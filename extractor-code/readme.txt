This product includes software developed by 5AM and the National Cancer Institute.

THIS SOFTWARE IS PROVIDED "AS IS," AND ANY EXPRESSED OR IMPLIED WARRANTIES, (INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE) ARE DISCLAIMED. IN NO EVENT SHALL THE NATIONAL CANCER INSTITUTE, 5AM SOLUTIONS, INC.
OR THEIR AFFILIATES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.

The extractor is a standalone java process used to extract data from client tissue banking software that
cannot be retrieved using the integration application.  For example, the extractor can be used to extract
data directly from the caTissue database because that data cannot be retrieved by the integration application
using the caTissue grid service.


Building the extractor
======================
1.  Make sure your JAVA_HOME and PATH variables are set to use Java 5 and maven 2.0.x.

2.  Set build properties in profiles.xml for the environment in which the extractor will be run.
    The following properties are needed to configure the extractor for caTissue.

        <period>1</period>
        <locationId>2</locationId>
        <databaseUrl>jdbc:mysql://localhost:3306/catissuecore</databaseUrl>
        <databaseUsername>catissue_core</databaseUsername>
        <databasePassword>catissue_core</databasePassword>
        <databaseDriver>com.mysql.jdbc.Driver</databaseDriver>
        <dynamicExtensionTable>DE_E_1984</dynamicExtensionTable>
        <priceNegotiableColumn>DE_AT_1988</priceNegotiableColumn>
        <minimumPriceColumn>DE_AT_1989</minimumPriceColumn>
        <maximumPriceColumn>DE_AT_1990</maximumPriceColumn>
        <storageTemperatureColumn>DE_AT_1989</storageTemperatureColumn>
        <specimenDensityColumn>DE_AT_1990</specimenDensityColumn>
        <timeLapseToProcessingColumn>DE_AT_1989</timeLapseToProcessingColumn>
        <preservationTypeColumn>DE_AT_3378</preservationTypeColumn>
        <specimenIdColumn>DYEXTN_AS_4_1970</specimenIdColumn>
        <integratorServiceUrl>http://127.0.0.1:8080/tissuelocator-integrator-web/CaTissueService</integratorServiceUrl>

3.  Build the extractor

        mvn clean install


Running the extractor
======================
1.  After building the extractor, move to the following directory:

        $PROJECT_HOME/target/deploy

2.  Run the extractor:

        java -jar tissuelocator-extractor-1.0.0-SNAPSHOT.jar CATISSUE

    The one argument is the type of extraction to perform.  Currently, the only valid value is CATISSUE.
    The application logs to target/log/tissuelocator-extractor.log, relative to where the process is started.
