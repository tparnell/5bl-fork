drop table if exists catissue_audit_event_log;
drop table if exists catissue_audit_event;
drop table if exists catissue_abstract_specimen;
drop table if exists catissue_specimen;
drop table if exists catissue_specimen_char;
drop table if exists catissue_specimen_coll_group;
drop table if exists catissue_coll_prot_reg;
drop table if exists catissue_collection_protocol;
drop table if exists catissue_specimen_protocol;
drop table if exists catissue_user;
drop table if exists catissue_address;
drop table if exists catissue_participant;
drop table if exists catissue_part_medical_id;
drop table if exists catissue_site;
drop table if exists dynamic_extension;

create table CATISSUE_AUDIT_EVENT (
   IDENTIFIER bigint not null,
   IP_ADDRESS varchar(20),
   EVENT_TIMESTAMP datetime,
   USER_ID bigint,
   COMMENTS varchar(1000),
   primary key (IDENTIFIER)
);

create table CATISSUE_AUDIT_EVENT_LOG (
   IDENTIFIER bigint not null,
   OBJECT_IDENTIFIER bigint,
   OBJECT_NAME varchar(50),
   EVENT_TYPE varchar(50),
   AUDIT_EVENT_ID bigint,
   primary key (IDENTIFIER)
);

CREATE TABLE CATISSUE_ABSTRACT_SPECIMEN (
    IDENTIFIER bigint NOT NULL,
    SPECIMEN_CLASS varchar(50) NOT NULL,
    SPECIMEN_TYPE varchar(50),
    LINEAGE varchar(50),
    PATHOLOGICAL_STATUS varchar(50),
    PARENT_SPECIMEN_ID bigint,
    SPECIMEN_CHARACTERISTICS_ID bigint,
    INITIAL_QUANTITY double,
    PRIMARY KEY  (IDENTIFIER)
);

CREATE TABLE CATISSUE_SPECIMEN (
    IDENTIFIER bigint NOT NULL,
    LABEL varchar(255),
    AVAILABLE boolean,
    BARCODE varchar(255),
    COMMENTS varchar(1000),
    ACTIVITY_STATUS varchar(50),
    SPECIMEN_COLLECTION_GROUP_ID bigint,
    REQ_SPECIMEN_ID bigint,
    AVAILABLE_QUANTITY double,
    CREATED_ON_DATE date,
    COLLECTION_STATUS varchar(50),
     PRIMARY KEY  (IDENTIFIER)
);

create table CATISSUE_SPECIMEN_CHAR (
   IDENTIFIER bigint not null,
   TISSUE_SITE varchar(150),
   TISSUE_SIDE varchar(50),
   primary key (IDENTIFIER)
);

CREATE TABLE catissue_specimen_coll_group (
    IDENTIFIER bigint NOT NULL,
    NAME varchar(255),
    BARCODE varchar(255),
    COMMENTS varchar(1000),
    ENCOUNTER_TIMESTAMP datetime,
    COLLECTION_PROTOCOL_REG_ID bigint,
    SURGICAL_PATHOLOGY_NUMBER varchar(50),
    COLLECTION_PROTOCOL_EVENT_ID bigint,
    COLLECTION_STATUS varchar(50),
    DATE_OFFSET integer,
    PRIMARY KEY  (IDENTIFIER)
);

create table CATISSUE_COLL_PROT_REG (
   IDENTIFIER bigint not null,
   PROTOCOL_PARTICIPANT_ID varchar(255),
   REGISTRATION_DATE date,
   PARTICIPANT_ID bigint,
   COLLECTION_PROTOCOL_ID bigint,
   ACTIVITY_STATUS varchar(50),
   CONSENT_SIGN_DATE datetime,
   CONSENT_DOC_URL varchar(1000),
   CONSENT_WITNESS bigint,
   BARCODE varchar(255),
   DATE_OFFSET integer,
   primary key (IDENTIFIER)
);

create table CATISSUE_COLLECTION_PROTOCOL (
   IDENTIFIER bigint not null,
   UNSIGNED_CONSENT_DOC_URL varchar(1000),
   ALIQUOT_IN_SAME_CONTAINER bit,
   CONSENTS_WAIVED bit,
   CP_TYPE varchar(50),
   PARENT_CP_ID bigint,
   SEQUENCE_NUMBER integer,
   STUDY_CALENDAR_EVENT_POINT double,
   primary key (IDENTIFIER)
);

create table CATISSUE_SPECIMEN_PROTOCOL (
   IDENTIFIER bigint not null,
   PRINCIPAL_INVESTIGATOR_ID bigint,
   TITLE varchar(255),
   SHORT_TITLE varchar(255),
   IRB_IDENTIFIER varchar(255),
   START_DATE date,
   END_DATE date,
   ENROLLMENT integer,
   DESCRIPTION_URL varchar(255),
   ACTIVITY_STATUS varchar(50),
   primary key (IDENTIFIER)
);

create table CATISSUE_USER (
   IDENTIFIER bigint not null,
   EMAIL_ADDRESS varchar(255),
   FIRST_NAME varchar(255),
   LAST_NAME varchar(255),
   LOGIN_NAME varchar(255),
   START_DATE date,
   ACTIVITY_STATUS varchar(50),
   DEPARTMENT_ID bigint,
   CANCER_RESEARCH_GROUP_ID bigint,
   INSTITUTION_ID bigint,
   ADDRESS_ID bigint,
   CSM_USER_ID bigint,
   STATUS_COMMENT varchar(1000),
   FIRST_TIME_LOGIN bit,
   primary key (IDENTIFIER)
);

create table CATISSUE_ADDRESS (
   IDENTIFIER bigint not null,
   STREET varchar(255),
   CITY varchar(50),
   STATE varchar(50),
   COUNTRY varchar(50),
   ZIPCODE varchar(30),
   PHONE_NUMBER varchar(50),
   FAX_NUMBER varchar(50),
   primary key (IDENTIFIER)
);

create table CATISSUE_PARTICIPANT (
   IDENTIFIER bigint not null,
   LAST_NAME varchar(255),
   FIRST_NAME varchar(255),
   MIDDLE_NAME varchar(255),
   BIRTH_DATE date,
   GENDER varchar(20),
   GENOTYPE varchar(50),
   ETHNICITY varchar(50),
   SOCIAL_SECURITY_NUMBER varchar(50),
   ACTIVITY_STATUS varchar(50),
   DEATH_DATE date,
   VITAL_STATUS varchar(50),
   primary key (IDENTIFIER)
);

create table CATISSUE_PART_MEDICAL_ID (
   IDENTIFIER bigint not null,
   MEDICAL_RECORD_NUMBER varchar(255),
   SITE_ID bigint,
   PARTICIPANT_ID bigint,
   primary key (IDENTIFIER)
);

create table CATISSUE_SITE (
    IDENTIFIER bigint not null,
    NAME varchar(255) not null,
    TYPE varchar(50),
    EMAIL_ADDRESS varchar(255),
    USER_ID bigint,
    ACTIVITY_STATUS varchar(50),
    ADDRESS_ID bigint,
    primary key (IDENTIFIER)
);

CREATE TABLE dynamic_extension (
    id bigint NOT NULL,
    negotiable boolean,
    minimum double,
    maximum double,
    temperature bigint,
    density bigint,
    time_lapse bigint,
    activity_status varchar(255),
    preservation_type varchar(255),
    PRIMARY KEY  (id)
);

alter table CATISSUE_AUDIT_EVENT add constraint FKACAF697A2206F20F foreign key (USER_ID) references CATISSUE_USER (IDENTIFIER);
alter table CATISSUE_AUDIT_EVENT_LOG add constraint FK8BB672DF77F0B904 foreign key (AUDIT_EVENT_ID) references CATISSUE_AUDIT_EVENT (IDENTIFIER);
alter table CATISSUE_ABSTRACT_SPECIMEN add constraint FK1674810456906F39 foreign key (SPECIMEN_CHARACTERISTICS_ID) references catissue_specimen_char (IDENTIFIER);
alter table CATISSUE_SPECIMEN add constraint FK1674810433BF33C5 foreign key (SPECIMEN_COLLECTION_GROUP_ID) references catissue_specimen_coll_group (IDENTIFIER);
alter table CATISSUE_SPECIMEN add constraint FK_PARENT_SPECIMEN foreign key (IDENTIFIER) references CATISSUE_ABSTRACT_SPECIMEN (IDENTIFIER);
alter table catissue_specimen_coll_group add constraint FKDEBAF1677E07C4AC foreign key (COLLECTION_PROTOCOL_REG_ID) references catissue_coll_prot_reg (IDENTIFIER);
alter table CATISSUE_COLL_PROT_REG add constraint FK5EB25F1387E5ADC7 foreign key (PARTICIPANT_ID) references CATISSUE_PARTICIPANT (IDENTIFIER);
alter table CATISSUE_COLL_PROT_REG add constraint FK5EB25F13A0FF79D4 foreign key (CONSENT_WITNESS) references CATISSUE_USER (IDENTIFIER);
alter table CATISSUE_COLL_PROT_REG add constraint FK5EB25F1348304401 foreign key (COLLECTION_PROTOCOL_ID) references CATISSUE_COLLECTION_PROTOCOL (IDENTIFIER);
alter table CATISSUE_COLLECTION_PROTOCOL add constraint FK32DC439DBC7298A9 foreign key (IDENTIFIER) references CATISSUE_SPECIMEN_PROTOCOL (IDENTIFIER);
alter table CATISSUE_COLLECTION_PROTOCOL add constraint FK32DC439DBC7298B9 foreign key (PARENT_CP_ID) references CATISSUE_COLLECTION_PROTOCOL (IDENTIFIER);
alter table CATISSUE_SPECIMEN_PROTOCOL add constraint FKB8481373870EB740 foreign key (PRINCIPAL_INVESTIGATOR_ID) references CATISSUE_USER (IDENTIFIER);
alter table CATISSUE_SITE add constraint FKB024C3436CD94566 foreign key (ADDRESS_ID) references CATISSUE_ADDRESS (IDENTIFIER);
alter table CATISSUE_SITE add constraint FKB024C3432206F20F foreign key (USER_ID) references CATISSUE_USER (IDENTIFIER);
alter table CATISSUE_USER add constraint FKB025CFC76CD94566 foreign key (ADDRESS_ID) references CATISSUE_ADDRESS (IDENTIFIER);
alter table CATISSUE_PART_MEDICAL_ID add constraint FK349E77F9A7F77D13 foreign key (SITE_ID) references CATISSUE_SITE (IDENTIFIER);
alter table CATISSUE_PART_MEDICAL_ID add constraint FK349E77F987E5ADC7 foreign key (PARTICIPANT_ID) references CATISSUE_PARTICIPANT (IDENTIFIER);
