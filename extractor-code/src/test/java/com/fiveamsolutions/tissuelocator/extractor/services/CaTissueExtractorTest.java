/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.extractor.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.extractor.data.CaTissueExtractionConfig;
import com.fiveamsolutions.tissuelocator.extractor.data.ExtractorType;
import com.fiveamsolutions.tissuelocator.extractor.service.ExtractorLauncher;
import com.fiveamsolutions.tissuelocator.extractor.test.AbstractTissueLocatorExtractorTest;
import com.fiveamsolutions.tissuelocator.extractor.test.MockCaTissueSpecimenService;
import com.fiveamsolutions.tissuelocator.extractor.util.TissueLocatorExtractorRegistry;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;

/**
 * @author ddasgupta
 *
 */
public class CaTissueExtractorTest extends AbstractTissueLocatorExtractorTest {

    private static final int NUM_SPECIMENS = 3;
    private static final long SPECIMEN_ID = 16;

    /**
     * set up the hsql database.
     * @throws Exception on error
     */
    @Before
    public void setUp() throws Exception {
        runSqlScript("catissue_schema.sql");
        runSqlScript("catissue_data.sql");
    }

    /**
     * run a sql script into the database.
     * @param scriptName the name of the script to run
     * @throws Exception on error
     */
    protected void runSqlScript(String scriptName) throws Exception {
        CaTissueExtractionConfig conf = CaTissueExtractionConfig.getInstance();
        String driver = conf.getDatabaseDriver();
        String url = conf.getDatabaseUrl();
        String username = conf.getDatabaseUsername();
        String password = conf.getDatabasePassword();
        Class.forName(driver).newInstance();
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement s = conn.createStatement();

        InputStream stream = ClassLoader.getSystemClassLoader().getResourceAsStream(scriptName);
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuffer query = new StringBuffer();
        String statement = br.readLine();
        while (statement != null) {
            query.append(statement);
            if (query.indexOf(";") >= 0) {
                s.executeUpdate(query.toString());
                query = new StringBuffer();
            }
            statement = br.readLine();
        }
        s.close();
        conn.close();
    }

    /**
     * Test the extractor.
     * @throws Exception on error
     */
    @Test
    public void extractorTest() throws Exception {
        new ExtractorLauncher();
        ExtractorType.CATISSUE.getExtractor().extractData();
        MockCaTissueSpecimenService service = (MockCaTissueSpecimenService)
            TissueLocatorExtractorRegistry.getServiceLocator().getCaTissueSpecimenService();
        assertNotNull(service.getLocationId());
        assertEquals(CaTissueExtractionConfig.getInstance().getLocationId(), service.getLocationId());
        SpecimenList specimens = service.getSpecimens();
        assertEquals(NUM_SPECIMENS, specimens.getSpecimens().size());
        for (Specimen s : specimens.getSpecimens()) {
            assertNotNull(s);
            assertNotNull(s.getId());
            assertNotNull(s.getSpecimenType());
            assertNotNull(s.getSpecimenType().getSpecimenClass());
            assertNotNull(s.getParticipant());
            assertNotNull(s.getParticipant().getExternalIdAssigner());
            assertNotNull(s.getParticipant().getExternalIdAssigner().getName());
            assertNotNull(s.getProtocol());
            assertNotNull(s.getProtocol().getContact());
            assertNotNull(s.getProtocol().getContact().getPhone());

            if (s.getId() != SPECIMEN_ID) {
                assertFalse(s.isPriceNegotiable());
                assertNotNull(s.getMinimumPrice());
                assertNotNull(s.getMaximumPrice());
            } else {
                assertTrue(s.isPriceNegotiable());
                assertNull(s.getMinimumPrice());
                assertNull(s.getMaximumPrice());
            }
        }
    }
}
