/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.extractor.data;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;


/**
 * @author ddasgupta
 */
public final class CaTissueExtractionConfig extends AbstractTissueBankExtractionConfig {

    private static final Logger LOG = Logger.getLogger(CaTissueExtractionConfig.class);

    private static final String PERIOD_KEY = "period";
    private static final String LOCATION_ID_KEY = "locationId";
    private static final String DATABASE_URL_KEY = "databaseUrl";
    private static final String DATABASE_USERNAME_KEY = "databaseUsername";
    private static final String DATABASE_PASSWORD_KEY = "databasePassword";
    private static final String DATABASE_DRIVER_KEY = "databaseDriver";
    private static final String DYNAMIC_EXTENSION_TABLE_KEY = "dynamicExtensionTable";
    private static final String PRICE_NEGOTIABLE_COLUMN_KEY = "priceNegotiableColumn";
    private static final String MINIMUM_PRICE_COLUMN_KEY = "minimumPriceColumn";
    private static final String MAXIMUM_PRICE_COLUMN_KEY = "maximumPriceColumn";
    private static final String SPECIMEN_ID_COLUMN_KEY = "specimenIdColumn";
    private static final String INTEGRATOR_SERVICE_URL_KEY = "integratorServiceUrl";

    private static CaTissueExtractionConfig instance = new CaTissueExtractionConfig();

    private CaTissueExtractionConfig() {
        setProperties(loadProperties("CaTissueExtractor.properties"));
    }

    /**
     * @return the singleton instance of this class.
     */
    public static CaTissueExtractionConfig getInstance() {
        return instance;
    }

    /**
     * @return the period (in days)
     */
    public int getPeriod() {
        return Integer.valueOf(getProperties().getProperty(PERIOD_KEY));
    }

    /**
     * @return the location id
     */
    public Long getLocationId() {
        return Long.valueOf(getProperties().getProperty(LOCATION_ID_KEY));
    }

    /**
     * @return the database url
     */
    public String getDatabaseUrl() {
        return getProperties().getProperty(DATABASE_URL_KEY);
    }

    /**
     * @return the database username
     */
    public String getDatabaseUsername() {
        return getProperties().getProperty(DATABASE_USERNAME_KEY);
    }

    /**
     * @return the database password
     */
    public String getDatabasePassword() {
        return getProperties().getProperty(DATABASE_PASSWORD_KEY);
    }

    /**
     * @return the database driver
     */
    public String getDatabaseDriver() {
        return getProperties().getProperty(DATABASE_DRIVER_KEY);
    }

    /**
     * @return the dynamic extension table
     */
    public String getDynamicExtensionTable() {
        return getProperties().getProperty(DYNAMIC_EXTENSION_TABLE_KEY);
    }

    /**
     * @return the price negotiable column
     */
    public String getPriceNegotiableColumn() {
        return getProperties().getProperty(PRICE_NEGOTIABLE_COLUMN_KEY);
    }

    /**
     * @return the minimum price column
     */
    public String getMinimumPriceColumn() {
        return getProperties().getProperty(MINIMUM_PRICE_COLUMN_KEY);
    }

    /**
     * @return the maximum price column
     */
    public String getMaximumPriceColumn() {
        return getProperties().getProperty(MAXIMUM_PRICE_COLUMN_KEY);
    }

    /**
     * @return the specimen id column
     */
    public String getSpecimenIdColumn() {
        return getProperties().getProperty(SPECIMEN_ID_COLUMN_KEY);
    }

    /**
     * @return the integrator service
     */
    public URL getIntegratorServiceUrl() {
        try {
            return new URL(getProperties().getProperty(INTEGRATOR_SERVICE_URL_KEY));
        } catch (MalformedURLException e) {
            LOG.error("error creating integrator service url", e);
        }
        return null;
    }
}
