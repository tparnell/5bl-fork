package org.cagrid.cbm.service.globus.resource;

import org.globus.wsrf.InvalidResourceKeyException;
import org.globus.wsrf.NoSuchResourceException;
import org.globus.wsrf.ResourceException;
import org.globus.wsrf.ResourceKey;


/** 
 * The implementation of this CBMResource type.
 * 
 * @created by Introduce Toolkit version 1.3
 * 
 */
public class CBMResource extends CBMResourceBase {

}
