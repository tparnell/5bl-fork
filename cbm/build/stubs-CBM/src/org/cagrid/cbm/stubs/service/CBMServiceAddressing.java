/**
 * CBMServiceAddressing.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2RC2 Apr 28, 2006 (12:42:00 EDT) WSDL2Java emitter.
 */

package org.cagrid.cbm.stubs.service;

public interface CBMServiceAddressing extends org.cagrid.cbm.stubs.service.CBMService {
    public org.cagrid.cbm.stubs.CBMPortType getCBMPortTypePort(org.apache.axis.message.addressing.EndpointReferenceType reference) throws javax.xml.rpc.ServiceException;


}
