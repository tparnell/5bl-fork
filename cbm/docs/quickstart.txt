The following guide is useful in troubleshooting registering with the grid service:
http://wiki.cagrid.org/display/knowledgebase/Troubleshoot+Index+Service+Registration

Summary of steps of the guide I used when setting up Demo and CI:

1) Changing the host name to register with the grid service
  -Edit JBoss: $JBOSS_HOME/server/default/deploy/wsrf.war/WEB-INF/etc/globus_wsrf_core/server-config.wsdd
  -Add the following line after <parameter name="sendXsiTypes"value="true"/>
    <parameter name="logicalHost"value="demo.5amsolutions.com"/> <parameter name="publishHostName"value="true"/>

2) Run JBoss without publishing your host name to the grid:
-Note: To remove an existing host from the grid service, see step 3 before performing the following.
-Edit JBoss: $JBOSS_HOME/server/default/deploy/wsrf.war/WEB-INF/etc/globus_wsrf_core/server-config.wsdd
  -Add or edit the following line after <parameter name="sendXsiTypes"value="true"/>
    <parameter name="publishHostName"value="false"/>


3) Unregister from the grid service
  -If you have already published your host with the grid service and wish to remove it, edit:
  $JBOSS_HOME/server/default/deploy/wsrf.war/WEB-INF/etc/cagrid_CBM/jndi-config.xml

  -Set the <performRegistration> parameter to false.
  -*IMPORTANT: Change the hostname by following step 1) so you don't unpublish demo from the grid. If you do this, just restart the jboss-4 on demo.

4) Training Service vs Production service
  -Currently Demo is registering with the training service.  To setup production, edit the following file:
  $JBOSS_HOME/server/default/deploy/wsrf.war/WEB-INF/etc/cagrid_<SERVICE NAME>/<SERVICE NAME>_registration.xml

  Training service:
  <wsa:Address>http://index.training.cagrid.org:8080/wsrf/services/DefaultIndexService</wsa:Address></ServiceGroupEPR>

  Production service:
  <wsa:Address>http://cagrid-index.nci.nih.gov:8080/wsrf/services/DefaultIndexService</wsa:Address></ServiceGroupEPR>

5) Query the grid service to show a list of published services:
   $GLOBUS_LOCATION/bin/wsrf-query -a -z none -s http://index.training.cagrid.org:8080/wsrf/services/DefaultIndexService | grep "<ns8:Address xmlns:ns8" | cut -d'>' -f2 | cut -d'<' -f1 | sort

   Query production:
   $GLOBUS_LOCATION/bin/wsrf-query -a -z none -s http://cagrid-index.nci.nih.gov:8080/wsrf/services/DefaultIndexService | grep "<ns8:Address xmlns:ns8" | cut -d'>' -f2 | cut -d'<' -f1 | sort
