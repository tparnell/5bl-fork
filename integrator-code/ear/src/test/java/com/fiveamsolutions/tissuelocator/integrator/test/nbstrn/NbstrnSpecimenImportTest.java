/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.test.nbstrn;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.integrator.test.AbstractSpecimenImportTest;

/**
 * @author gvaughn
 *
 */
public class NbstrnSpecimenImportTest extends AbstractSpecimenImportTest {

    private static final String[] XML_SPECIMEN = new String[]{
        "Argininemia (ARG)", "1 count", "Residual Dried Blood Spot",
        "30 hours", "2010",  "Female", "Black or African American",
        "Hispanic or Latino", "Breast", "-70 - - 80 (deep freeze)", "23" };
    private static final String[] XML_UPDATE_SPECIMEN = new String[]{
        "Histidinemia (HIS)", "2 micrograms", "Residual Dried Blood Spot", "40 months", "2009",
        "Male", "American Indian or Alaska Native", "Not Hispanic or Latino",
        "Formula", "-20 (Freezer)", "25" };
    private static final String[] CSV_SPECIMEN = new String[]{
        "Hearing Loss (HEAR)", "1 milligrams", "Residual Dried Blood Spot",
        "30 years", "2010",  "Female", "Black or African American",
        "Hispanic or Latino", "Breast", "-70 - - 80 (deep freeze)", "23" };
    private static final String[] CSV_UPDATE_SPECIMEN = new String[]{
        "Isovaleric acidemia (IVA)", "1 count", "20 hours", "2008",  "Residual Dried Blood Spot",
        "Male", "American Indian or Alaska Native", "Hispanic or Latino",
        "Formula", "-20 (Freezer)", "25" };
    private static final String[] TAB_SPECIMEN_1 = new String[]{
        "Hearing Loss (HEAR)", "1 count", "Residual Dried Blood Spot",
        "60 hours", "2010",  "Female", "Black or African American",
        "Hispanic or Latino", "Breast", "-70 - - 80 (deep freeze)", "23"  };
    private static final String[] TAB_SPECIMEN_2 = new String[]{
        "Isovaleric acidemia (IVA)", "1 count", "Residual Dried Blood Spot",
        "70 hours", "2010",  "Female", "Black or African American",
        "Hispanic or Latino", "Formula", "-20 (Freezer)", "25" };

    private static final int SPECIMEN_COUNT = 4;
    private static final String SPECIMEN_PATH = "xpath=//table[@id='specimen']/tbody/tr[%d]/td[1]/a";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "NbstrnSpecimenImportTest.sql";
    }

    /**
     * Test the round trip of the flat-file import.  Note that the specific Synchronization adapters are
     *  individually tested in the unit tests.
     * @throws Exception if error occurs
     */
    @Test
    public void testSpecimenImport() throws Exception {
        initializeConfigs();
        prepareSpecimen("nbstrnSpecimen.xml", FTP_XML_DIR);
        checkSpecimen(XML_SPECIMEN, "test external id", "30 hours");

        prepareSpecimen("nbstrnSpecimen2.xml", FTP_XML_DIR);
        checkSpecimen(XML_UPDATE_SPECIMEN, "test external id", "2 microgram");

        prepareSpecimen("nbstrnSpecimen.csv", FTP_CSV_DIR);
        checkSpecimen(CSV_SPECIMEN, "test external id 2", "1 milligram");

        prepareSpecimen("nbstrnSpecimen2.csv", FTP_CSV_DIR);
        checkSpecimen(CSV_UPDATE_SPECIMEN, "test external id 2", "20 hours");

        prepareSpecimen("nbstrnSpecimen_tab.txt", FTP_TAB_DIR);
        checkSpecimen(TAB_SPECIMEN_1, "test external id 3", "60 hours");
        checkSpecimen(TAB_SPECIMEN_2, "test external id 4", "70 hours");
        verifySpecimenCount();
    }

    private void checkSpecimen(String[] expectedValues, String externalId, String uniqueValue) {
        verifySpecimen(externalId, uniqueValue);
        checkSpecimenValues(expectedValues, false);
    }

    private void verifySpecimen(String externalId, String uniqueValue) {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Dried Blood Spot Administration");
        long totalTime = 0L;
        boolean specimenVisible = selenium.isTextPresent(externalId)
            && selenium.isTextPresent(uniqueValue);

        while (!specimenVisible && totalTime < MILLIS_PER_THREE_MIN) {
            totalTime += LONG_DELAY * 2;

            pause(LONG_DELAY);
            mouseOverAndPause("link=Administration");
            clickAndWait("link=Dried Blood Spot Administration");

            specimenVisible = selenium.isTextPresent(externalId)
                && selenium.isTextPresent(uniqueValue);
        }
        boolean pathExists = true;
        boolean specimenPath = false;
        int index = 1;
        while (pathExists && !specimenPath) {
            pathExists = selenium.isElementPresent(String.format(SPECIMEN_PATH, index));
            if (pathExists) {
               specimenPath =  selenium.getText(String.format("xpath=//table[@id='specimen']/tbody/tr[%d]/td[7]",
                       index)).equals(externalId);
            }
            index++;
        }
        assertTrue(specimenPath);
        String disease = selenium.getTable(String.format("specimen.%d.2", index - 1));
        String expectedHeader = disease.replaceAll("\\s+", "");
        clickAndWait(String.format("xpath=//table[@id='specimen']/tbody/tr[%d]/td[1]/a", index - 1));
        String actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
    }

    private void verifySpecimenCount() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Dried Blood Spot Administration");
        for (int i = 1; i <= SPECIMEN_COUNT; i++) {
            assertTrue(selenium.isElementPresent(String.format(
                    "xpath=//table[@id='specimen']/tbody/tr[%d]/td[1]/a", i)));
        }
        assertFalse(selenium.isElementPresent(String.format(
                "xpath=//table[@id='specimen']/tbody/tr[%d]/td[1]/a", SPECIMEN_COUNT + 1)));
    }
}
