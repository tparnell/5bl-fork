/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.test.abrc;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.integrator.test.AbstractSpecimenImportTest;

/**
 * @author gvaughn
 *
 */
public class AbrcSpecimenImportTest extends AbstractSpecimenImportTest {

    private static final String[] XML_SPECIMEN = new String[]{
        "Abdominal fibromatosis (disorder)", "1 milligrams", "Bile",
        "30 years", "2010", "test name", "$12.34 - $56.78", "Female", "Black or African American",
        "Hispanic or Latino", "Autopsy", "Gallbladder", "Fixed Formalin", "10", "35", "-70" };
    private static final String[] XML_UPDATE_SPECIMEN = new String[]{
        "Friction injury of tooth (disorder)", "2 micrograms", "CDNA", "40 months", "2009", "test name2",
        "$15.34 - $80.78", "Male", "American Indian or Alaska Native", "Not Hispanic or Latino",
        "Surgically Resected", "Salivary Gland", "Dried (slide)", "3", "44", "32" };
    private static final String[] CSV_SPECIMEN = new String[]{
        "Heart injury, closed (disorder)", "3 milliliters", "20 days", "2009", "test name", "Cells",
        "$100.75 - $201.98", "Female", "American Indian or Alaska Native", "Black or African American",
        "Hispanic or Latino", "Autopsy", "Gallbladder", "Fixed Formalin", "-70", "23" };
    private static final String[] CSV_UPDATE_SPECIMEN = new String[]{
        "Heat cramp (finding)", "4 milligrams", "20 days", "2008", "test name", "Bile",
        "$100.75 - $201.98", "Male", "American Indian or Alaska Native", "Hispanic or Latino",
        "Surgically Resected", "Heart", "Fixed Ethanol Slide", "-80", "24", "36" };
    private static final String[] TAB_SPECIMEN_1 = new String[]{
        "Abdominal fibromatosis (disorder)", "1 milligrams", "Cells",
        "30 years", "2010", "test name", "$12.34 - $56.78", "Female", "Black or African American",
        "Hispanic or Latino", "Autopsy", "Gallbladder", "Fixed Formalin", "10", "35", "-70" };
    private static final String[] TAB_SPECIMEN_2 = new String[]{
        "Hearing disorder (disorder)", "1 milligrams", "Adipose Tissue",
        "30 years", "2010", "test name", "$12.34 - $56.78", "Female", "Black or African American",
        "Hispanic or Latino", "Surgically Resected", "Aorta", "DMSO", "60", "32", "20" };

    /**
     * Test the round trip of the flat-file import.  Note that the specific Synchronization adapters are
     *  individually tested in the unit tests.
     * @throws Exception if error occurs
     */
    @Test
    public void testSpecimenImport() throws Exception {

        initializeConfigs();
        prepareSpecimen("specimen.xml", FTP_XML_DIR);
        checkSpecimenByClass("Fluid", XML_SPECIMEN);

        prepareSpecimen("specimen2.xml", FTP_XML_DIR);
        checkSpecimenByClass("Molecular", XML_UPDATE_SPECIMEN);

        prepareSpecimen("specimen.csv", FTP_CSV_DIR);
        checkSpecimenByClass("Cells", CSV_SPECIMEN);

        prepareSpecimen("specimen2.csv", FTP_CSV_DIR);
        checkSpecimenByClass("Fluid", CSV_UPDATE_SPECIMEN);

        prepareSpecimen("specimen_tab.txt", FTP_TAB_DIR);
        checkSpecimenByClass("Cells", TAB_SPECIMEN_1);
        checkSpecimenByClass("Tissue", TAB_SPECIMEN_2);

        verifySpecimenCounts(new int[]{1, 1, 1, 1});
    }
}
