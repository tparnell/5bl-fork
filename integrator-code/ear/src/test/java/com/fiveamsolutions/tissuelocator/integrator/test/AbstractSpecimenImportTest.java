/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.FtpFileImportHelper;

/**
 * @author bpickeral
 *
 */
public abstract class AbstractSpecimenImportTest extends AbstractTissueLocatorSeleniumTest {
    /**
     * Milliseconds per three minutes.
     */
    protected static final Long MILLIS_PER_THREE_MIN = 180000L;
    /**
     * Directory to which XML files are upload.
     */
    protected static final String FTP_XML_DIR = SeleniumTestProperties.getFTPRootDirectory();
    /**
     * Directory to which CSV files are upload.
     */
    protected static final String FTP_CSV_DIR = FTP_XML_DIR + "csv/";
    /**
     * Directory to which Tab-delimited files are upload.
     */
    protected static final String FTP_TAB_DIR = FTP_CSV_DIR + "tab/";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "SpecimenImportTest.sql";
    }

    /**
     * Run sql commands to initializae import configurations.
     * @throws Exception on error.
     */
    protected void initializeConfigs() throws Exception {
        runSqlCommand("update file_import_integration_config set rootDirectory='"
                + FTP_XML_DIR
                + "' where ftpSite='test ftp site' and"
                + " delimiter is null");
        runSqlCommand("update file_import_integration_config set rootDirectory='"
                + FTP_CSV_DIR
                + "' where ftpSite='test ftp site' and"
                + " delimiter='COMMA'");
        runSqlCommand("update file_import_integration_config set rootDirectory='"
                + FTP_TAB_DIR
                + "' where ftpSite='test ftp site' and"
                + " delimiter='TAB'");
        runSqlCommand("update file_import_integration_config set ftpSite='" + SeleniumTestProperties.getFTPSite()
                + "', username='" + SeleniumTestProperties.getFTPUser()
                + "', password='" + SeleniumTestProperties.getFTPPassword()
                + "' where ftpSite='test ftp site'");
        runSqlCommand("update integration_config set notification_email='"
                + SeleniumTestProperties.getFTPNotification() + "' where id in "
                + "(select config_id from file_import_integration_config where ftpsite='test ftp site')");
    }

    /**
     * Upload a specimen import file, launch adapter threads, and login to the app.
     * @param fileName File to upload.
     * @param uploadDir FTP directory to which to upload the file.
     * @throws Exception on error.
     */
    protected void prepareSpecimen(String fileName, String uploadDir) throws Exception {
        uploadFile(fileName, uploadDir);
        runSqlCommand("update integration_config set startTime = current_time + interval '5 seconds'");
        selenium.open("/tissuelocator-integrator-web/launchAdapterThreads.jsp");
        assertTrue(selenium.isTextPresent("Threads started"));
        login("test-user1@example.com", "tissueLocator1", true);
    }

    private void uploadFile(String fileName, String rootDirectory) throws IOException, URISyntaxException {
        FileImportIntegrationConfig config = new FileImportIntegrationConfig();
        config.setUsername(SeleniumTestProperties.getFTPUser());
        config.setPassword(SeleniumTestProperties.getFTPPassword());
        config.setFtpSite(SeleniumTestProperties.getFTPSite());
        config.setRootDirectory(rootDirectory);

        FtpFileImportHelper ftpHelper = new FtpFileImportHelper(config);
        try {
            if (!ftpHelper.connect()) {
                fail("Could not connect to ftp");
            }
            File file = new File(ClassLoader.getSystemResource(fileName).toURI());
            FileInputStream inputStream = new FileInputStream(file);

            ftpHelper.getClient().enterLocalPassiveMode();
            ftpHelper.getClient().storeFile(fileName, inputStream);

            inputStream.close();

        } finally {
            ftpHelper.disconnect();
        }
    }

    /**
     * Waits for a specimen to be imported into the system by searching
     * on specimen class. When the specimen appears, the details page
     * is checked for the expected values.
     * @param specimenClass Specimen class on which to search.
     * @param expectedValues Expected specimen values.
     */
    protected void checkSpecimenByClass(String specimenClass, String[] expectedValues) {
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        assertTrue(selenium.isTextPresent("Search for Biospecimens"));
        selenium.select("object.specimenType.name", "label=- All -");
        selenium.select("object.specimenType.specimenClass", "label=" + specimenClass);
        searchAndVerifyResult();
        checkSpecimenValues(expectedValues, true);

    }

    /**
     * Checks the specimen details page for the expected values.
     * @param values Expected values.
     * @param selectSpecimen If the specimen should be selected from the search page.
     */
    protected void checkSpecimenValues(String[] values, boolean selectSpecimen) {
        if (selectSpecimen) {
            clickAndWait("xpath=//table[@id='specimen']/tbody/tr/td[2]/a");
        }
        for (String value : values) {
            assertTrue(selenium.isTextPresent(value));
        }
    }

    /**
     * Searchs and verifies results.
     */
    protected void searchAndVerifyResult() {

        clickAndWait("btn_search");
        long totalTime = 0L;
        boolean resultPresent = selenium.isTextPresent("1-1 of 1 Result");

        while (!resultPresent && totalTime < MILLIS_PER_THREE_MIN) {
            totalTime += LONG_DELAY * 2;

            pause(LONG_DELAY);
            clickAndWait("btn_search");
            pause(LONG_DELAY);

            resultPresent = selenium.isTextPresent("1-1 of 1 Result");
        }
        assertFalse(selenium.isVisible("filters"));
        assertTrue(selenium.isVisible("criteria"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
    }

    /**
     * Verify specimen counts by class.
     * @param counts Counts for cells, fluid, molecular, tissue.
     */
    protected void verifySpecimenCounts(int[] counts) {
        selenium.open(CONTEXT_ROOT + "/updateHomePageCache.jsp");
        assertTrue(selenium.isTextPresent("Home Page Data Cache Updated"));
        clickAndWait("link=Home");
        int count = 0;
        assertTrue(selenium.isElementPresent("link=Cells (" + counts[count++] + ")"));
        assertTrue(selenium.isElementPresent("link=Fluid (" + counts[count++] + ")"));
        assertTrue(selenium.isElementPresent("link=Molecular (" + counts[count++] + ")"));
        assertTrue(selenium.isElementPresent("link=Tissue (" + counts[count++] + ")"));
    }
}
