/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package gov.nih.nci.cbm.domain.logicalmodel;

/**
 * Metadata describing the availability of information pertaining to specimens in a collection.
 * 
 * @version 1.0
 */
public class AnnotationAvailabilityProfile {

    /**
     * Are demographic information (e.g. age, sex, race, ethnicity) available?
     */
    private Boolean hasAdditionalPatientDemographics;

    /**
     * Are the exposure data (e.g. smoking, alcohol, drugs, chemicals) available?
     */
    private Boolean hasExposureHistory;

    /**
     * Are family history data available?
     */
    private Boolean hasFamilyHistory;

    /**
     * Are histopathologic data (e.g. stage, grade, histologic type, subtype) available?
     */
    private Boolean hasHistopathologicInformation;

    /**
     * Are lab data available?
     */
    private Boolean hasLabData;

    /**
     * Are longitudinal specimens (specimens collected from the same patient over time) available?
     */
    private Boolean hasLongitudinalSpecimens;

    /**
     * Are matched specimens (tumor and non-tumor from the same patient, tissue and blood from the same patient, for
     * example) available?
     */
    private Boolean hasMatchedSpecimens;

    /**
     * Is outcome information (effectiveness of treatment, side effects, etc) available?
     */
    private Boolean hasOutcomeInformation;

    /**
     * For the collection (for example, clinical trial group), are patients available for follow-up?
     */
    private Boolean hasParticipantsAvailableForFollowup;

    /**
     * Are treatment data (e.g. drug, schedule) available?
     */
    private Boolean hasTreatmentInformation;

    /**
     * @return the hasAdditionalPatientDemographics
     */
    public Boolean getHasAdditionalPatientDemographics() {
        return hasAdditionalPatientDemographics;
    }

    /**
     * @param hasAdditionalPatientDemographics the hasAdditionalPatientDemographics to set
     */
    public void setHasAdditionalPatientDemographics(Boolean hasAdditionalPatientDemographics) {
        this.hasAdditionalPatientDemographics = hasAdditionalPatientDemographics;
    }

    /**
     * @return the hasExposureHistory
     */
    public Boolean getHasExposureHistory() {
        return hasExposureHistory;
    }

    /**
     * @param hasExposureHistory the hasExposureHistory to set
     */
    public void setHasExposureHistory(Boolean hasExposureHistory) {
        this.hasExposureHistory = hasExposureHistory;
    }

    /**
     * @return the hasFamilyHistory
     */
    public Boolean getHasFamilyHistory() {
        return hasFamilyHistory;
    }

    /**
     * @param hasFamilyHistory the hasFamilyHistory to set
     */
    public void setHasFamilyHistory(Boolean hasFamilyHistory) {
        this.hasFamilyHistory = hasFamilyHistory;
    }

    /**
     * @return the hasHistopathologicInformation
     */
    public Boolean getHasHistopathologicInformation() {
        return hasHistopathologicInformation;
    }

    /**
     * @param hasHistopathologicInformation the hasHistopathologicInformation to set
     */
    public void setHasHistopathologicInformation(Boolean hasHistopathologicInformation) {
        this.hasHistopathologicInformation = hasHistopathologicInformation;
    }

    /**
     * @return the hasLabData
     */
    public Boolean getHasLabData() {
        return hasLabData;
    }

    /**
     * @param hasLabData the hasLabData to set
     */
    public void setHasLabData(Boolean hasLabData) {
        this.hasLabData = hasLabData;
    }

    /**
     * @return the hasLongitudinalSpecimens
     */
    public Boolean getHasLongitudinalSpecimens() {
        return hasLongitudinalSpecimens;
    }

    /**
     * @param hasLongitudinalSpecimens the hasLongitudinalSpecimens to set
     */
    public void setHasLongitudinalSpecimens(Boolean hasLongitudinalSpecimens) {
        this.hasLongitudinalSpecimens = hasLongitudinalSpecimens;
    }

    /**
     * @return the hasMatchedSpecimens
     */
    public Boolean getHasMatchedSpecimens() {
        return hasMatchedSpecimens;
    }

    /**
     * @param hasMatchedSpecimens the hasMatchedSpecimens to set
     */
    public void setHasMatchedSpecimens(Boolean hasMatchedSpecimens) {
        this.hasMatchedSpecimens = hasMatchedSpecimens;
    }

    /**
     * @return the hasOutcomeInformation
     */
    public Boolean getHasOutcomeInformation() {
        return hasOutcomeInformation;
    }

    /**
     * @param hasOutcomeInformation the hasOutcomeInformation to set
     */
    public void setHasOutcomeInformation(Boolean hasOutcomeInformation) {
        this.hasOutcomeInformation = hasOutcomeInformation;
    }

    /**
     * @return the hasParticipantsAvailableForFollowup
     */
    public Boolean getHasParticipantsAvailableForFollowup() {
        return hasParticipantsAvailableForFollowup;
    }

    /**
     * @param hasParticipantsAvailableForFollowup the hasParticipantsAvailableForFollowup to set
     */
    public void setHasParticipantsAvailableForFollowup(Boolean hasParticipantsAvailableForFollowup) {
        this.hasParticipantsAvailableForFollowup = hasParticipantsAvailableForFollowup;
    }

    /**
     * @return the hasTreatmentInformation
     */
    public Boolean getHasTreatmentInformation() {
        return hasTreatmentInformation;
    }

    /**
     * @param hasTreatmentInformation the hasTreatmentInformation to set
     */
    public void setHasTreatmentInformation(Boolean hasTreatmentInformation) {
        this.hasTreatmentInformation = hasTreatmentInformation;
    }
}