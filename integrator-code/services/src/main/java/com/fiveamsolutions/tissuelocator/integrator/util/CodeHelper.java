/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.util;

import java.util.HashMap;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;

/**
 * @author bpickeral
 *
 */
public class CodeHelper {

    private final Map<String, AdditionalPathologicFinding> diseaseCache =
        new HashMap<String, AdditionalPathologicFinding>();
    private final Map<String, SpecimenType> specimenTypeCache = new HashMap<String, SpecimenType>();

    /**
     * Replaces Code objects within a Specimen with the ABRC Code objects using the Translation service.
     * @param importedSpecimen Specimen object imported
     * @param institutionId id of the Institution associated with the Specimen
     * @param translationMap the map of translations for the location
     */
    public void setSystemCodes(Specimen importedSpecimen, Long institutionId,
            Map<String, Map<String, Map<String, String>>> translationMap) {
        setSpecimenType(importedSpecimen, institutionId, translationMap);
        setPathologicalCharacteristic(importedSpecimen, institutionId, translationMap);
    }

    /**
     * Replaces the imported SpecimenType object with the corresponding SpecimenType object in the DB.
     * @param importedSpecimen Specimen object imported
     * @param institutionId id of the Institution associated with the Specimen
     * @param translationMap the map of translations for the location
     */
    public void setSpecimenType(Specimen importedSpecimen, Long institutionId,
            Map<String, Map<String, Map<String, String>>> translationMap) {
        if (importedSpecimen.getSpecimenType() != null) {
            String name = importedSpecimen.getSpecimenType().getName();
            SpecimenType st = specimenTypeCache.get(name);
            if (st == null) {
                st = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService().getCode(name,
                                institutionId, SpecimenType.class, translationMap);
                specimenTypeCache.put(name, st);
            }
            importedSpecimen.setSpecimenType(st);
        }
    }

    /**
     * Replaces the imported AdditionalPathologicFinding object with the corresponding
     * AdditionalPathologicFinding object in the DB.
     * @param importedSpecimen Specimen object imported
     * @param institutionId id of the Institution associated with the Specimen
     * @param translationMap the map of translations for the location
     */
    public void setPathologicalCharacteristic(Specimen importedSpecimen, Long institutionId,
            Map<String, Map<String, Map<String, String>>> translationMap) {
        if (importedSpecimen.getPathologicalCharacteristic() != null) {
            String name = importedSpecimen.getPathologicalCharacteristic().getName();
            AdditionalPathologicFinding apf = diseaseCache.get(name);
            if (apf == null) {
                apf = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService().getCode(name,
                                institutionId, AdditionalPathologicFinding.class, translationMap);
                diseaseCache.put(name, apf);
            }
            importedSpecimen.setPathologicalCharacteristic(apf);
        }
    }
}
