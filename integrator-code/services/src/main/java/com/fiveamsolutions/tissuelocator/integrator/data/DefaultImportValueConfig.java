/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;

/**
 * A configuration for a default specimen value to be set if not included in an import data load.
 * 
 * @author gvaughn
 *
 */
@Entity
@Table(name = "default_import_value_config")
public class DefaultImportValueConfig implements PersistentObject, Auditable {

    private static final long serialVersionUID = 1L;
    private static final int MAX_LENGTH = 255;
    
    private Long id;
    private String propertyPath;
    private String propertyClassName;
    private String propertyValue;
    private DefaultImportValueType valueType;
    
    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * @return The path, relative to a Specimen, of the property this default value corresponds to.
     */
    @NotNull
    @Length(max = MAX_LENGTH)
    @Column(name = "property_path")
    public String getPropertyPath() {
        return propertyPath;
    }
    
    /**
     * @param path The path, relative to a Specimen, of the property this default value corresponds to.
     */
    public void setPropertyPath(String path) {
        this.propertyPath = path;
    }
    
    /**
     * @return The class name of the specimen property, if this is a nested object.
     */
    @Length(max = MAX_LENGTH)
    @Column(name = "property_class_name")
    public String getPropertyClassName() {
        return propertyClassName;
    }
    
    /**
     * @param propertyClassName The class name of the specimen property, if this is a nested object.
     */
    public void setPropertyClassName(String propertyClassName) {
        this.propertyClassName = propertyClassName;
    }
    
    /**
     * @return The default value, as a string.
     */
    @NotNull
    @Length(max = MAX_LENGTH)
    @Column(name = "property_value")
    public String getPropertyValue() {
        return propertyValue;
    }
    
    /**
     * @param propertyValue The default value, as a string.
     */
    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }
    
    /**
     * @return The value category this config falls under.
     */
    @NotNull
    @Enumerated(value = EnumType.STRING)
    @Column(name = "value_type")
    public DefaultImportValueType getValueType() {
        return valueType;
    }
    
    /**
     * @param valueType The value category this config falls under.
     */
    public void setValueType(DefaultImportValueType valueType) {
        this.valueType = valueType;
    }
    
    
}
