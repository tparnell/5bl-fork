/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.util.ThreadsyncedCache;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorHibernateUtil;

/**
 * This class handles the scheduling of importing specimens.  This should be used when a lot of 
 * specimens need to be imported at once and the queueing of the specimens to process happens much 
 * faster than the processing of those speciments.
 * @author smiller
 */
public class SpecimenProcessor {
    private static final int MAX_WAIT_TIME = 10000;
    private static final int SLEEP_AFTER_ADD_EXECUTION_TIME = 2000;
    private static final int BATCH_COUNTER = 1000;
    private static final Logger LOG = Logger.getLogger(SpecimenProcessor.class);
    
    private final ExecutorService executor;
    private final int maxThreads;
    private final FileImportBatch batch; 
    private final Institution institution;
    private final Map<String, Map<String, Map<String, String>>> translationMap;
    private final SpecimenSynchronizationAdapter adapter = new SpecimenSynchronizationAdapter();
    
    private List<Specimen> currentSpecimens;
    private final ThreadsyncedCache<CollectionProtocol> protocolCache = new ThreadsyncedCache<CollectionProtocol>();
    private final ThreadsyncedCache<Participant> participantCache = new ThreadsyncedCache<Participant>();
    
    /**
     * Build the processor.
     * @param maxThreads max number of threads to spawn at once.
     * @param batch the file import batch we are processing in this processor.
     * @param institution the insitution associated with all specimens in this processor.
     * @param translationMap map of translation values.
     */
    public SpecimenProcessor(int maxThreads, FileImportBatch batch, Institution institution,
            Map<String, Map<String, Map<String, String>>> translationMap) {
        this.maxThreads = maxThreads;
        this.batch = batch;
        this.institution = institution;
        this.translationMap = translationMap;
        executor = Executors.newFixedThreadPool(maxThreads);
        currentSpecimens = new ArrayList<Specimen>();
    }
    
    /**
     * Adds the specimen to the queue.
     * @param specimen the specimen.
     */
    public void addSpecimenToQueue(Specimen specimen) {
        currentSpecimens.add(specimen);
        if (currentSpecimens.size() == BATCH_COUNTER) {
            addExecution();
        }
    }

    private void addExecution() {
        // check for the queue of work growing faster than we are processing the work
        synchronized (batch) {
            while (((ThreadPoolExecutor) executor).getQueue().size() > maxThreads) {
                // there is enough work build up to fill an entire extra round of threads.  
                // Wait for a thread to finish.
                try {
                    LOG.debug("Delaying adding batch of parsed specimens to the queue, because the queue is full.");
                    batch.wait();
                } catch (InterruptedException e) {
                    LOG.error(e);
                }
            }
            
            LOG.debug("Adding a batch to the processing queue");
            SpecimenBatchProcessorRunner batchProcessor = new SpecimenBatchProcessorRunner(currentSpecimens);
            executor.execute(batchProcessor);
            currentSpecimens = new ArrayList<Specimen>();
            
            try {
                // This shouldn't be needed, but it gets around a timing issue caused by both of the first two threads 
                // created trying to create the default collection protocol over and over again.
                Thread.sleep(SLEEP_AFTER_ADD_EXECUTION_TIME);
            } catch (InterruptedException e) {
                LOG.error(e);
            }
        }
    }
    
    /**
     * Wait for completion.
     */
    public void waitForCompletion() {
        // first add any unsubmitted batches to the executor.
        if (!currentSpecimens.isEmpty()) {
            addExecution();
        }
        
        // now shutdown and wait for completion of the scheduled tasks.
        LOG.debug("Waiting for processing to complete, no more data to add to queue.");
        executor.shutdown();
        synchronized (batch) {
            while (!executor.isTerminated()) {
                try {
                    batch.wait(MAX_WAIT_TIME);
                } catch (InterruptedException e) {
                    LOG.error(e);
                }
            }
        }
    }
    
    /**
     * Class to process a single batch of speicmens.
     * @author smiller
     */
    class SpecimenBatchProcessorRunner implements Runnable {
        
        private final List<Specimen> specimens;
        
        /**
         * Create the batch proccessor.
         * @param specimens the specimens to process.
         */
        SpecimenBatchProcessorRunner(List<Specimen> specimens) {
            this.specimens = specimens;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void run() {
            LOG.debug("Starting processing of a batch of parsed specimens.");
            try {
                TissueLocatorIntegratorHibernateUtil.getHibernateHelper().openAndBindSession();
                if (!TissueLocatorIntegratorHibernateUtil.getHibernateHelper().isManagedSession()) {
                    TissueLocatorIntegratorHibernateUtil.getHibernateHelper().beginTransaction();
                }
                SpecimenBatchProcessor batchProcessor = new SpecimenBatchProcessor(protocolCache, participantCache);
                batchProcessor.processSpecimenBatch(batch, specimens, adapter, institution, translationMap);
            } finally {
                LOG.debug("Completed processing of a batch of parsed specimens.");
                synchronized (batch) {
                    batch.notifyAll();
                }
                TissueLocatorIntegratorHibernateUtil.getHibernateHelper().unbindAndCleanupSession();
            }
        }
    }
}