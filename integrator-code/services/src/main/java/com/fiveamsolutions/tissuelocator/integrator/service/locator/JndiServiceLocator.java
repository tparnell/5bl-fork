/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service.locator;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.util.JndiUtils;
import com.fiveamsolutions.tissuelocator.integrator.service.CaTissueGridService;
import com.fiveamsolutions.tissuelocator.integrator.service.CaTissueGridServiceImpl;
import com.fiveamsolutions.tissuelocator.integrator.service.DataExportConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.ImportBatchServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.service.CodeServiceRemote;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceRemote;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceRemote;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceRemote;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;
import com.fiveamsolutions.tissuelocator.service.extension.DynamicExtensionsServiceRemote;

/**
 * JNDI backed implementation of the service locator.
 *
 * @author smiller
 */
public class JndiServiceLocator implements ServiceLocator {

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public GenericServiceLocal<PersistentObject> getGenericService() {
        return (GenericServiceLocal) JndiUtils.lookup("tissuelocator-integrator/GenericServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    public DataExportConfigServiceLocal getDataExportConfigService() {
        return (DataExportConfigServiceLocal)
            JndiUtils.lookup("tissuelocator-integrator/DataExportConfigServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    public TissueBankIntegrationConfigServiceLocal getTissueBankIntegrationConfigService() {
        return (TissueBankIntegrationConfigServiceLocal)
            JndiUtils.lookup("tissuelocator-integrator/TissueBankIntegrationConfigServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    public ImportBatchServiceLocal getImportBatchService() {
        return (ImportBatchServiceLocal) JndiUtils.lookup("tissuelocator-integrator/ImportBatchServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    public SpecimenServiceRemote getSpecimenService() {
        return (SpecimenServiceRemote) JndiUtils.lookup("tissuelocator/SpecimenServiceRemoteBean/remote");
    }

    /**
     * {@inheritDoc}
     */
    public ParticipantServiceRemote getParticipantService() {
        return (ParticipantServiceRemote) JndiUtils.lookup("tissuelocator/ParticipantServiceRemoteBean/remote");
    }

    /**
     * {@inheritDoc}
     */
    public CollectionProtocolServiceRemote getCollectionProtocolService() {
        return (CollectionProtocolServiceRemote) JndiUtils.lookup(
                "tissuelocator/CollectionProtocolServiceRemoteBean/remote");
    }

    /**
     * {@inheritDoc}
     */
    public InstitutionServiceRemote getInstitutionService() {
        return (InstitutionServiceRemote) JndiUtils.lookup("tissuelocator/InstitutionServiceRemoteBean/remote");
    }

    /**
     * {@inheritDoc}
     */
    public CaTissueGridService getCaTissueGridService() {
        return new CaTissueGridServiceImpl();
    }

    /**
     * {@inheritDoc}
     */
    public CodeServiceRemote getCodeService() {
        return (CodeServiceRemote) JndiUtils.lookup("tissuelocator/CodeServiceRemoteBean/remote");
    }

    /**
     * {@inheritDoc}
     */
    public TranslationServiceLocal getTranslationService() {
        return (TranslationServiceLocal) JndiUtils.lookup("tissuelocator-integrator/TranslationServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    public DynamicExtensionsServiceRemote getDynamicExtensionsService() {
        return (DynamicExtensionsServiceRemote)
            JndiUtils.lookup("tissuelocator/DynamicExtensionsServiceRemoteBean/remote");
    }

}
