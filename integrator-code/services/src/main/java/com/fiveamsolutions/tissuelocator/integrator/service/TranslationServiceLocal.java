/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service;

import java.util.Map;

import javax.ejb.Local;

import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.Translation;

/**
 * @author ddasgupta
 *
 */
@Local
public interface TranslationServiceLocal extends GenericServiceLocal<Translation> {

    /**
     * Translate a code from the client coding system to the local coding system.
     * @param <T> the type of code to retrieve
     * @param clientValue the client code
     * @param locationId the id of the location for which the code is to be translated
     * @param codeType the type of code to retrieve
     * @param translationMap the map of translations for the location
     * @return the translated code
     */
    <T extends Code> T getCode(String clientValue, Long locationId, Class<T> codeType,
            Map<String, Map<String, Map<String, String>>> translationMap);

    /**
     * Translate a code from the client coding system to the local coding system.
     * @param <T> the type of code to retrieve
     * @param clientValue the client code
     * @param locationId the id of the location for which the code is to be translated
     * @param codeType the type of code to retrieve
     * @param translationMap the map of translations for the location
     * @return the translated code
     */
    @SuppressWarnings("rawtypes")
    <T extends Enum> T getEnum(String clientValue, Long locationId, Class<T> codeType,
            Map<String, Map<String, Map<String, String>>> translationMap);
    
    /**
     * Translate a controlled vocabulary custom property value to the local
     * vocabulary value.
     * @param clientValue Client value.
     * @param config integration config.
     * @param customPropertyName Field name of the custom property.
     * @param translationMap the map of translations for the location.
     * @return The translated vocabulary value.
     */
    String getCustomProperty(String clientValue, AbstractTissueBankIntegrationConfig config, String customPropertyName,
            Map<String, Map<String, Map<String, String>>> translationMap);
    
    /**
     * Translate a specimen column header value to the local header value.
     * @param clientValue Client value.
     * @param locationId the id of the location for which the header is to be translated
     * @param translationMap the map of translations for the location.
     * @return The translated column header value.
     */
    String getSpecimenColumn(String clientValue, Long locationId,
            Map<String, Map<String, Map<String, String>>> translationMap);

    /**
     * Get the raw translation for a field.
     * @param sourceValue the source code
     * @param codeSetName the code set
     * @param codeType the code set type
     * @return the translation object
     */
    Translation getTranslation(String sourceValue, String codeSetName, String codeType);

    /**
     * Get a map of translations for a particular location.
     * @param locationId the id of the location
     * @return a map from code set name -> code type -> source value -> translated value
     */
    Map<String, Map<String, Map<String, String>>> getTranslationMap(Long locationId);

}
