/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.dao;

import gov.nih.nci.cbm.domain.logicalmodel.ParticipantCollectionSummary;
import gov.nih.nci.cbm.domain.logicalmodel.Race;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author smiller
 *
 */
public class ParticipantCollectionSummaryDao extends AbstractCbmDao {
    
    private static long currentId = 1;
    
    private static final String SEARCH_FOR_MATCH_BASE = "select " 
        + "ParticipantCollectionSummary.participantCollectionSummaryID from "
        + "ParticipantCollectionSummary where ParticipantCollectionSummary.gender = ? and " 
        + "ParticipantCollectionSummary.ethnicity = ? and ParticipantCollectionSummary.registered_to = ? and " 
        + "? =  (select count(*) from JoinParticipantCollectionSummaryToRace, Race where "
        + "ParticipantCollectionSummary.participantCollectionSummaryID = "
        + "JoinParticipantCollectionSummaryToRace.participantCollectionSummaryID and "
        + "Race.raceId = JoinParticipantCollectionSummaryToRace.raceid) ";
    
    private static final String SEARCH_FOR_MATCH_RACE_CLAUSE = " and ? in (select Race.race from " 
        + "JoinParticipantCollectionSummaryToRace, Race where "
        + "ParticipantCollectionSummary.participantCollectionSummaryID = "
        + "JoinParticipantCollectionSummaryToRace.participantCollectionSummaryID "
        + "and Race.raceId = JoinParticipantCollectionSummaryToRace.raceid) ";
    
    private static final String SEARCH_FOR_MATCH_DIAGNOSIS_CLAUSE = "and ? in (select Diagnosis.diagnosisType from "
        + "JoinParticipantCollectionSummaryToDiagnosis, Diagnosis where "
        + "ParticipantCollectionSummary.participantCollectionSummaryID = "
        + "JoinParticipantCollectionSummaryToDiagnosis.participantCollectionSummaryID "
        + "and Diagnosis.diagnosisId = JoinParticipantCollectionSummaryToDiagnosis.diagnosisId) ";
    
    private static final int MIN_PARAM_COUNT = 4;
    
    private static final String INCREMENT_COUNT = "update ParticipantCollectionSummary set count = count + 1 where "
        + "participantCollectionSummaryID = ?";
    
    private static final String CREATE_PARTICIPANT = "insert into ParticipantCollectionSummary (count, ethnicity, "
        + "gender, participantCollectionSummaryID, registered_to) values (?, ?, ?, ?, ?)";
    
    private static final String CREATE_PARTICIPANT_TO_RACE_JOIN = "insert into JoinParticipantCollectionSummaryToRace " 
        + "(participantCollectionSummaryID, raceid) values (?, (select Race.raceId from Race where Race.race = ?))";
    
    private static final String CREATE_PARTICIPANT_TO_DIAGNOSIS_JOIN = "insert into " 
        + "JoinParticipantCollectionSummaryToDiagnosis (participantCollectionSummaryID, diagnosisId) values (?, " 
        + "(select Diagnosis.diagnosisID from Diagnosis where Diagnosis.diagnosisType = ?))"; 
    
    private static final String DELETE_PARTICIPANT = "delete from ParticipantCollectionSummary";
    
    private static final String DELETE_RACE_JOIN = "delete from JoinParticipantCollectionSummaryToRace";
    
    private static final String DELETE_DIAGNOSIS_JOIN = "delete from JoinParticipantCollectionSummaryToDiagnosis";
    
    
    /**
     * create the dao.
     * @param connection the connection to sue.
     */
    public ParticipantCollectionSummaryDao(Connection connection) {
        super(connection);
    }
    
    /**
     * delete all data saved.
     * @throws SQLException on error.
     */
    public void deleteAllParticipants() throws SQLException {
        executeUpdate(DELETE_RACE_JOIN, new Object[] {});
        executeUpdate(DELETE_DIAGNOSIS_JOIN, new Object[] {});
        executeUpdate(DELETE_PARTICIPANT, new Object[] {});
        
        CollectionProtocolDao cpDao = new CollectionProtocolDao(getConnection());
        cpDao.deleteAllProtocols();
        
        ParticipantCollectionSummaryDao.currentId = 1;
    }
    
    /**
     * Create the participant or increment its count if it already exists.
     * @param p the participant
     * @return the id.
     * @throws SQLException on error.
     */
    public long createOrIncrementParticipant(ParticipantCollectionSummary p) throws SQLException {
        Long id = getMatchingParticipant(p);
        if (id != null) {
            incrementCount(id);
        } else {
            id = createParticipant(p);
        }
        return id.longValue();
    }

    private Long getMatchingParticipant(ParticipantCollectionSummary p) throws SQLException {
        StringBuffer sql = new StringBuffer(SEARCH_FOR_MATCH_BASE);
        Object[] params = new Object[MIN_PARAM_COUNT + p.getRaces().size() + (p.getDiagnosis().isEmpty() ? 0 : 1)];
        int currentParam = 0;
        params[currentParam] = p.getGender();
        currentParam++;
        params[currentParam] = p.getEthnicity();
        currentParam++;
        params[currentParam] = p.getCollectionProtocol().getId();
        currentParam++;
        params[currentParam] = p.getRaces().size();
        currentParam++;
        
        if (!p.getDiagnosis().isEmpty()) {
            sql.append(SEARCH_FOR_MATCH_DIAGNOSIS_CLAUSE);
            params[currentParam] = p.getDiagnosis().iterator().next().getDiagnosisType(); 
            currentParam++;
        }
        
        for (Race r : p.getRaces()) {
            sql.append(SEARCH_FOR_MATCH_RACE_CLAUSE);
            params[currentParam] = r.getName();
            currentParam++;
        }
        
        @SuppressWarnings("rawtypes")
        List results = executePreparedQuery(sql.toString(), params);
        if (results.size() > 0) {
            return Long.valueOf(((Integer) results.get(0)).longValue());
        }
        return null;
    }
    
    private void incrementCount(Long id) throws SQLException {
        executeUpdate(INCREMENT_COUNT, new Object[] {id});
    }
    
    private Long createParticipant(ParticipantCollectionSummary p) throws SQLException {
        CollectionProtocolDao cpDao = new CollectionProtocolDao(getConnection());
        Long cpId = cpDao.createOrRetrieveProtocol(p.getCollectionProtocol());
        executeUpdate(CREATE_PARTICIPANT, new Object[] {1, p.getEthnicity(), p.getGender(), 
                ParticipantCollectionSummaryDao.currentId, cpId});
        
        if (!p.getDiagnosis().isEmpty()) {
            executeUpdate(CREATE_PARTICIPANT_TO_DIAGNOSIS_JOIN, new Object[] {
                ParticipantCollectionSummaryDao.currentId, 
                p.getDiagnosis().iterator().next().getDiagnosisType()});
        }
        
        for (Race r : p.getRaces()) {
            executeUpdate(CREATE_PARTICIPANT_TO_RACE_JOIN, new Object[] {
                    ParticipantCollectionSummaryDao.currentId, r.getName()});
        }
        
        ParticipantCollectionSummaryDao.currentId++;
        
        return ParticipantCollectionSummaryDao.currentId - 1;
    }

}
