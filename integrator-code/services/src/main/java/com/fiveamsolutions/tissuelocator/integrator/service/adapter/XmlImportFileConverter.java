/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;

/**
 * Converts an XML file to domain objects.
 * @author gvaughn
 *
 */
public class XmlImportFileConverter implements ImportFileConverter {

    private Unmarshaller um;
    private XMLStreamReader xmlReader;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FileImportBatch fileImportBatch, FtpFileImportHelper ftpHelper, 
            Map<String, Map<String, Map<String, String>>> translationMap) throws FileParseException {
        try {
            JAXBContext context = JAXBContext.newInstance(SpecimenList.class);
            um = context.createUnmarshaller();
            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            xmlReader =  xmlInputFactory.createXMLStreamReader(ftpHelper.downloadFile(fileImportBatch)); 
            xmlReader.next();
        } catch (JAXBException e) {
            throw new FileParseException("Exception initializing XML reader.", e);
        } catch (XMLStreamException e) {
            throw new FileParseException("Exception initializing XML reader.", e);
        }
        
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Specimen getNext() throws FileParseException {
        Specimen specimen = null;
        try {
            while (xmlReader.hasNext()) {
                if (xmlReader.getEventType() == XMLStreamReader.START_ELEMENT
                        && "specimen".equals(xmlReader.getLocalName())) {
                    specimen = (Specimen) um.unmarshal(xmlReader);
                    break;
                } else {
                    xmlReader.next();
                }
            }
        } catch (XMLStreamException e) {
            throw new FileParseException("Exception parsing XML file.", e);
        } catch (JAXBException e) {
            throw new FileParseException("Exception parsing XML file.", e);
        }
        return specimen;
    }
}
