/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.util.Collection;

/**
 * An exception resulting from a failed attempt to parse an individual row
 * of a CSV import file.
 * @author gvaughn
 *
 */
public class CsvParseException extends FileParseException {

    private static final long serialVersionUID = 1L;
    
    private final long row;
    private final Collection<InvalidCsvValue> invalidCsvValues;
    
    /**
     * A CSV parse exception with the specified invalid values.
     * @param invalidCsvValues Details of the values that caused the parse error.
     * @param row Row in the CSV file in which the error occurred, assuming a 1..n row numbering.
     */
    public CsvParseException(Collection<InvalidCsvValue> invalidCsvValues, long row) {
        super();
        this.row = row;
        this.invalidCsvValues = invalidCsvValues;
    }
    
    /**
     * @return the row
     */
    public long getRow() {
        return row;
    }

    /**
     * @return the invalidCsvValues
     */
    public Collection<InvalidCsvValue> getInvalidCsvValues() {
        return invalidCsvValues;
    }

    /**
     * Information about an invalid value in an individual cell of a
     * CSV import file.
     * @author gvaughn
     *
     */
    public static class InvalidCsvValue {
        
        private final String columnHeader;
        private final String invalidValue;
        private final Exception exception;
        
        /**
         * Information about an invalid value in an individual cell of a
         * CSV import file.
         * @param columnHeader The header of the column in which the error occurred.
         * @param invalidValue The value of the cell for which the error occurred.
         * @param exception The exception that was the cause of the error.
         */
        public InvalidCsvValue(String columnHeader, String invalidValue, Exception exception) {
            this.columnHeader = columnHeader;
            this.invalidValue = invalidValue;
            this.exception = exception;
        }

        /**
         * @return the columnHeader
         */
        public String getColumnHeader() {
            return columnHeader;
        }

        /**
         * @return the invalidValue
         */
        public String getInvalidValue() {
            return invalidValue;
        }

        /**
         * @return the exception
         */
        public Exception getException() {
            return exception;
        }
    }

}
