/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service;

import java.io.InputStream;
import java.io.StringReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.axis.types.URI.MalformedURIException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.integrator.data.CaTissueIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

import edu.wustl.catissuecore.domain.CellSpecimen;
import edu.wustl.catissuecore.domain.CollectionEventParameters;
import edu.wustl.catissuecore.domain.CollectionProtocol;
import edu.wustl.catissuecore.domain.FluidSpecimen;
import edu.wustl.catissuecore.domain.MolecularSpecimen;
import edu.wustl.catissuecore.domain.Participant;
import edu.wustl.catissuecore.domain.ParticipantMedicalIdentifier;
import edu.wustl.catissuecore.domain.Race;
import edu.wustl.catissuecore.domain.Site;
import edu.wustl.catissuecore.domain.Specimen;
import edu.wustl.catissuecore.domain.SpecimenCollectionGroup;
import edu.wustl.catissuecore.domain.TissueSpecimen;
import edu.wustl.catissuecore.domain.User;
import gov.nih.nci.cagrid.common.Utils;
import gov.nih.nci.cagrid.cqlquery.CQLQuery;
import gov.nih.nci.cagrid.cqlresultset.CQLQueryResults;
import gov.nih.nci.cagrid.data.client.DataServiceClient;
import gov.nih.nci.cagrid.data.utilities.CQLQueryResultsIterator;

/**
 * @author ddasgupta
 *
 */
@SuppressWarnings({ "PMD.SignatureDeclareThrowsException", "PMD.AvoidDuplicateLiterals", "rawtypes" })
public class CaTissueGridServiceImpl implements CaTissueGridService {

    private static final Logger LOG = Logger.getLogger(CaTissueGridServiceImpl.class);
    private static final String TYPE_REPLACEMENT = "%TYPE%";
    private static final String ID_REPLACEMENT = "%ID%";
    private static final String XML_CONFIG = "client-config.wsdd";

    private static final String SPECIMEN_QUERY = "<Target name=\"%TYPE%\">"
        + "<Attribute name=\"id\" value=\"" + ID_REPLACEMENT + "\" predicate=\"EQUAL_TO\"/>"
        + "</Target>";

    private static final String SCG_QUERY = "<Target name=\"edu.wustl.catissuecore.domain.SpecimenCollectionGroup\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollection\" name=\"edu.wustl.catissuecore.domain.Specimen\">"
        + "<Attribute name=\"id\"  value=\"" + ID_REPLACEMENT + "\" predicate=\"EQUAL_TO\"/>"
        + "</Association>"
        + "</Group>"
        + "</Target>";

    private static final String SCG_SITE_QUERY = "<Target name=\"edu.wustl.catissuecore.domain.Site\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"abstractSpecimenCollectionGroupCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.SpecimenCollectionGroup\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollection\" name=\"edu.wustl.catissuecore.domain.Specimen\">"
        + "<Attribute name=\"id\"  value=\"" + ID_REPLACEMENT + "\" predicate=\"EQUAL_TO\"/>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Target>";

    private static final String CEP_QUERY = "<Target name=\"edu.wustl.catissuecore.domain.CollectionEventParameters\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollectionGroup\" "
        + "name=\"edu.wustl.catissuecore.domain.SpecimenCollectionGroup\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollection\" name=\"edu.wustl.catissuecore.domain.Specimen\">"
        + "<Attribute name=\"id\"  value=\"" + ID_REPLACEMENT + "\" predicate=\"EQUAL_TO\"/>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Target>";

    private static final String PROTOCOL_QUERY = "<Target name=\"edu.wustl.catissuecore.domain.CollectionProtocol\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"collectionProtocolRegistrationCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.CollectionProtocolRegistration\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollectionGroupCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.SpecimenCollectionGroup\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollection\" name=\"edu.wustl.catissuecore.domain.Specimen\">"
        + "<Attribute name=\"id\"  value=\"" + ID_REPLACEMENT + "\" predicate=\"EQUAL_TO\"/>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Target>";

    private static final String PROTOCOL_SITE_QUERY = "<Target name=\"edu.wustl.catissuecore.domain.Site\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"collectionProtocolCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.CollectionProtocol\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"collectionProtocolRegistrationCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.CollectionProtocolRegistration\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollectionGroupCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.SpecimenCollectionGroup\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollection\" name=\"edu.wustl.catissuecore.domain.Specimen\">"
        + "<Attribute name=\"id\"  value=\"" + ID_REPLACEMENT + "\" predicate=\"EQUAL_TO\"/>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Target>";

    private static final String PROTOCOL_PI_QUERY = "<Target name=\"edu.wustl.catissuecore.domain.User\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"collectionProtocolCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.CollectionProtocol\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"collectionProtocolRegistrationCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.CollectionProtocolRegistration\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollectionGroupCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.SpecimenCollectionGroup\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollection\" name=\"edu.wustl.catissuecore.domain.Specimen\">"
        + "<Attribute name=\"id\"  value=\"" + ID_REPLACEMENT + "\" predicate=\"EQUAL_TO\"/>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Target>";

    private static final String PARTICIPANT_QUERY = "<Target name=\"edu.wustl.catissuecore.domain.Participant\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"collectionProtocolRegistrationCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.CollectionProtocolRegistration\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollectionGroupCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.SpecimenCollectionGroup\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollection\" name=\"edu.wustl.catissuecore.domain.Specimen\">"
        + "<Attribute name=\"id\"  value=\"" + ID_REPLACEMENT + "\" predicate=\"EQUAL_TO\"/>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Target>";

    private static final String PARTICIPANT_RACE_QUERY = "<Target name=\"edu.wustl.catissuecore.domain.Race\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"participant\" name=\"edu.wustl.catissuecore.domain.Participant\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"collectionProtocolRegistrationCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.CollectionProtocolRegistration\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollectionGroupCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.SpecimenCollectionGroup\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollection\" name=\"edu.wustl.catissuecore.domain.Specimen\">"
        + "<Attribute name=\"id\"  value=\"" + ID_REPLACEMENT + "\" predicate=\"EQUAL_TO\"/>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Target>";

    private static final String PARTICIPANT_ID_QUERY = "<Target "
        + "name=\"edu.wustl.catissuecore.domain.ParticipantMedicalIdentifier\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"participant\" name=\"edu.wustl.catissuecore.domain.Participant\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"collectionProtocolRegistrationCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.CollectionProtocolRegistration\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollectionGroupCollection\" "
        + "name=\"edu.wustl.catissuecore.domain.SpecimenCollectionGroup\">"
        + "<Group logicRelation=\"AND\">"
        + "<Attribute name=\"id\"  predicate=\"IS_NOT_NULL\"/>"
        + "<Association roleName=\"specimenCollection\" name=\"edu.wustl.catissuecore.domain.Specimen\">"
        + "<Attribute name=\"id\"  value=\"" + ID_REPLACEMENT + "\" predicate=\"EQUAL_TO\"/>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Association>"
        + "</Group>"
        + "</Target>";

    private static final Map<SpecimenClass, Class> SPECIMEN_CLASS_MAP;
    static {
        SPECIMEN_CLASS_MAP = new HashMap<SpecimenClass, Class>();
        SPECIMEN_CLASS_MAP.put(SpecimenClass.CELLS, CellSpecimen.class);
        SPECIMEN_CLASS_MAP.put(SpecimenClass.FLUID, FluidSpecimen.class);
        SPECIMEN_CLASS_MAP.put(SpecimenClass.MOLECULAR, MolecularSpecimen.class);
        SPECIMEN_CLASS_MAP.put(SpecimenClass.TISSUE, TissueSpecimen.class);
    }

    private DataServiceClient client;

    private synchronized DataServiceClient getClient(Long locationId) throws MalformedURIException, RemoteException {
        if (client == null) {
            TissueBankIntegrationConfigServiceLocal service =
                TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
            CaTissueIntegrationConfig config =
                (CaTissueIntegrationConfig) service.getByLocationId(locationId);
            client = new DataServiceClient(config.getGridServiceUrl());
        }
        return client;
    }

    private CQLQuery getQuery(String queryBase, Long specimenId) throws Exception {
        String fullQuery = "<CQLQuery xmlns=\"http://CQL.caBIG/1/gov.nih.nci.cagrid.CQLQuery\">"
            + StringUtils.replace(queryBase, ID_REPLACEMENT, String.valueOf(specimenId))
            + "</CQLQuery>";
        LOG.debug(fullQuery);
        return (CQLQuery) Utils.deserializeObject(new StringReader(fullQuery), CQLQuery.class);
    }

    private Object runQuery(CQLQuery query, Long locationId) throws RemoteException, MalformedURIException {
        CQLQueryResults cqlQueryResult = getClient(locationId).query(query);
        InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(XML_CONFIG);
        Iterator iter = new CQLQueryResultsIterator(cqlQueryResult, is);
        if (iter.hasNext()) {
            Object result = iter.next();
            LOG.debug(ToStringBuilder.reflectionToString(result));
            return result;
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public Specimen getSpecimen(Long specimenId, SpecimenClass specimenClass, Long locationId) throws Exception {
        String specimenQuery = StringUtils.replace(SPECIMEN_QUERY, TYPE_REPLACEMENT,
                SPECIMEN_CLASS_MAP.get(specimenClass).getName());
        return (Specimen) runQuery(getQuery(specimenQuery, specimenId), locationId);
    }

    /**
     * {@inheritDoc}
     */
    public SpecimenCollectionGroup getSpecimenCollectionGroup(Long specimenId, Long locationId) throws Exception {
        return (SpecimenCollectionGroup) runQuery(getQuery(SCG_QUERY, specimenId), locationId);
    }

    /**
     * {@inheritDoc}
     */
    public Site getSpecimenCollectionGroupSite(Long specimenId, Long locationId) throws Exception {
        return (Site) runQuery(getQuery(SCG_SITE_QUERY, specimenId), locationId);
    }

    /**
     * {@inheritDoc}
     */
    public CollectionEventParameters getCollectionEventParameters(Long specimenId, Long locationId) throws Exception {
        return (CollectionEventParameters) runQuery(getQuery(CEP_QUERY, specimenId), locationId);
    }

    /**
     * {@inheritDoc}
     */
    public CollectionProtocol getCollectionProtocol(Long specimenId, Long locationId) throws Exception {
        return (CollectionProtocol) runQuery(getQuery(PROTOCOL_QUERY, specimenId), locationId);
    }

    /**
     * {@inheritDoc}
     */
    public Site getCollectionProtocolSite(Long specimenId, Long locationId) throws Exception {
        return (Site) runQuery(getQuery(PROTOCOL_SITE_QUERY, specimenId), locationId);
    }

    /**
     * {@inheritDoc}
     */
    public User getCollectionProtocolPI(Long specimenId, Long locationId) throws Exception {
        return (User) runQuery(getQuery(PROTOCOL_PI_QUERY, specimenId), locationId);
    }

    /**
     * {@inheritDoc}
     */
    public Participant getParticipant(Long specimenId, Long locationId) throws Exception {
        return (Participant) runQuery(getQuery(PARTICIPANT_QUERY, specimenId), locationId);
    }

    /**
     * {@inheritDoc}
     */
    public ParticipantMedicalIdentifier getParticipantId(Long specimenId, Long locationId) throws Exception {
        return (ParticipantMedicalIdentifier) runQuery(getQuery(PARTICIPANT_ID_QUERY, specimenId), locationId);
    }

    /**
     * {@inheritDoc}
     */
    public Collection<Race> getParticipantRaces(Long specimenId, Long locationId) throws Exception {
        Collection<Race> races = new ArrayList<Race>();
        CQLQuery query = getQuery(PARTICIPANT_RACE_QUERY, specimenId);
        CQLQueryResults cqlQueryResult = getClient(locationId).query(query);
        InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(XML_CONFIG);
        Iterator iter = new CQLQueryResultsIterator(cqlQueryResult, is);
        while (iter.hasNext()) {
            Race race = (Race) iter.next();
            LOG.debug(ToStringBuilder.reflectionToString(race));
            races.add(race);
        }
        return races;
    }
}
