/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.integrator.data.FileImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.util.NotificationHelper;

/**
 * @author smiller
 */
public class FtpFileImportHelper {
    
    private static final Logger LOG = Logger.getLogger(FtpFileImportHelper.class);
    private static final String PROCESED_DIRECTORY = "processed";
    private static final int TIMEOUT = 30000;
    private static final int UPLOAD_WAIT_INCREMENT = 1000;
    
    private final FileImportIntegrationConfig config;
    private FTPClient client = null;
    
    /**
     * Create the helper.
     * @param config the config for the ftp server.
     */
    public FtpFileImportHelper(FileImportIntegrationConfig config) {
        this.config = config;
        
    }
    
    /**
     * Get the current client.
     * @return the client.
     */
    public FTPClient getClient() {
        return client;
    }

    /**
     * Connect to the ftp server.
     * @return true if the ftp server is ready for use, false otherwise.
     */
    public boolean connect() {
        try {
            client = new FTPClient();
            client.setDefaultTimeout(TIMEOUT);
            client.setDataTimeout(TIMEOUT);
            client.setConnectTimeout(TIMEOUT);  
            client.connect(config.getFtpSite());
            if (login()) {
                gotoRootDirectory();
                createProcesedDirectory();
            } else {
                disconnect();
            }
        } catch (Exception e) {
            disconnect();
            NotificationHelper.getInstance().sendErrorNotification(
                    "Error Connecting to ftp site: " + config.getFtpSite(), config.getNotificationEmail(), e);
        }
        
        return client != null;
    }
    
    private boolean login() throws IOException {
        if (!client.login(config.getUsername(), config.getPassword())) {
            NotificationHelper.getInstance().sendErrorNotification(
                    "Login Credentials not accepted for: " + config.getFtpSite(), config.getNotificationEmail());
            return false;
        }
        return true;
    }
    
    private void gotoRootDirectory() throws IOException {
        if (!StringUtils.isEmpty(config.getRootDirectory())) {
            client.changeWorkingDirectory(config.getRootDirectory());
        }
    }
    
    private void createProcesedDirectory() throws IOException {
        client.enterLocalPassiveMode();
        FTPFile[] ftpFiles = client.listFiles();
        boolean processedDirectoryExists = false;
        for (FTPFile ftpFile : ftpFiles) {
            if (ftpFile.isDirectory() && PROCESED_DIRECTORY.equals(ftpFile.getName())) {
                processedDirectoryExists = true;
            }
        }
        if (!processedDirectoryExists) {
            client.makeDirectory(PROCESED_DIRECTORY);
        }
    }
    
    /**
     * Use the existing connection to cycle through the files available, move them to the processed
     * dir, and create the batches. Note, this method must be called after a successful connect is called.
     * @return the list of batches.
     */
    public List<FileImportBatch> createImportBatches() {
        List<FileImportBatch> batches = new ArrayList<FileImportBatch>();
        
        try {
            FTPFile[] ftpFiles = client.listFiles();
            Map<Long, FTPFile> sortedMap = new TreeMap<Long, FTPFile>();
            
            // Sort files from oldest to newest
            for (FTPFile ftpFile : ftpFiles) {
                if (ftpFile.isFile()) {
                    sortedMap.put(ftpFile.getTimestamp().getTime().getTime(), ftpFile);
                }
            }
            
            // Process and store the files from oldest to newest
            for (FTPFile ftpFile : sortedMap.values()) {
                FileImportBatch batch = processFile(ftpFile);
                if (batch != null) {
                    batches.add(batch);
                }               
            }
        } catch (Exception e) {
            disconnect();
            NotificationHelper.getInstance().sendErrorNotification(
                    "Error Connecting to ftp site: " + config.getFtpSite(), config.getNotificationEmail(), e);
        }
        
        return batches;
    }
    
    private FileImportBatch processFile(FTPFile ftpFile) throws IOException {
        // Determine if the file is ready via consistent file size
        long size = ftpFile.getSize();
        boolean ready = false;
        int waitTime = 0;
        try {
            do {
                Thread.sleep(UPLOAD_WAIT_INCREMENT);
                FTPFile refreshedFile = client.listFiles(ftpFile.getName())[0];
                long newSize = refreshedFile.getSize();
                if (newSize == size) {
                    ready = true;
                } else {
                    size = newSize;
                    waitTime += UPLOAD_WAIT_INCREMENT;
                }
            } while (!ready && waitTime < TIMEOUT);
        } catch (InterruptedException e) {
            NotificationHelper.getInstance().sendErrorNotification(
                    "Error waiting for file upload: " + ftpFile.getName(), config.getNotificationEmail());
        }
        FileImportBatch fileBatch = null;
        if (ready) {
            fileBatch = new FileImportBatch();
            fileBatch.setStartTime(new Date());
            fileBatch.setFileName(generateFileName(ftpFile.getName()));
            if (!client.rename(ftpFile.getName(), fileBatch.getFileName())) {
                NotificationHelper.getInstance().sendErrorNotification(
                        "Error moving file: " + ftpFile.getName(), config.getNotificationEmail());
            }
        } else {
            LOG.warn("Timeout waiting for file to upload: " + ftpFile.getName());
        }
        
        return fileBatch;
    }
    
    private String generateFileName(String originalFileName) {
        StringBuffer fileName = new StringBuffer(PROCESED_DIRECTORY + "/");
        Date date = new Date();
        fileName.append(date.getTime());
        fileName.append(originalFileName);
        return fileName.toString();
    }
    
    /**
     * returns an input stream for the batch.  Note, a successful call to connect must preceed this one.
     * If null is returned an error was logged and no data is available.
     * @param batch the batch.
     * @return the input stream, or null if an error occurs.
     */
    public InputStream downloadFile(FileImportBatch batch) {
        try {
            return client.retrieveFileStream(batch.getFileName());
        } catch (IOException e) {
            NotificationHelper.getInstance().sendErrorNotification(
                    "Error downloading file from ftp site: " + batch.getFileName(), 
                    config.getNotificationEmail(), e);
            return null;
        }
    }
    
    /**
     * Disconnect from the ftp server.
     */
    public void disconnect() {
        if (client != null && client.isConnected()) {
            try {
                client.disconnect();
            } catch (Exception e) {
                NotificationHelper.getInstance().sendErrorNotification(
                        "Error Disconecting from ftp site: " + config.getFtpSite(), config.getNotificationEmail());
            }
        }
        
        client = null;
    }

}
