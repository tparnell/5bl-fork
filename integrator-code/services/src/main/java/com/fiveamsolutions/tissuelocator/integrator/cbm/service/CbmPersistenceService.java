/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.service;

import gov.nih.nci.cbm.domain.logicalmodel.SpecimenCollectionSummary;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.fiveamsolutions.tissuelocator.integrator.cbm.dao.SpecimenCollectionSummaryDao;
import com.fiveamsolutions.tissuelocator.integrator.data.CbmDataExportConfig;

/**
 * @author smiller
 *
 */
public class CbmPersistenceService {
    
    private final CbmDataExportConfig config;
    private Connection connection;
    
    /**
     * Create the service.
     * @param config the config.
     */
    public CbmPersistenceService(CbmDataExportConfig config) {
        this.config = config;
    }

    /**
     * connect to the database.
     * @throws ClassNotFoundException when driver is not found
     * @throws SQLException on error
     */
    public void connect() throws ClassNotFoundException, SQLException {
        if (connection == null) {
            Class.forName(config.getDbDriverClass());
            connection = DriverManager.getConnection(config.getDbUrl(), config.getDbUsername(), config.getDbPassword());
        }
    }
    
    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * start a transaction.  Requires an active connection.
     * @throws SQLException on error
     */
    public void beginTransaction() throws SQLException {
        if (connection != null) {
            this.connection.setAutoCommit(false);
        }
    }

    /**
     * commit the transaction.
     * @throws SQLException on error
     */
    public void commitTransaction() throws SQLException {
        if (connection != null) {
            this.connection.commit();
            this.connection.setAutoCommit(true);
        }
    }

    /**
     * disconnect from the db.
     * @throws SQLException on error
     */
    public void disconnect() throws SQLException {
        if (connection != null) {
            this.connection.close();
            this.connection = null;
        }
    }
    
    /**
     * Delete all the data that we have in cbm.
     * @throws SQLException on error.
     */
    public void deleteAllData() throws SQLException {
        SpecimenCollectionSummaryDao dao = new SpecimenCollectionSummaryDao(connection);
        dao.deleteAllSpecimens();
    }

    /**
     * Add the specimen to the cbm database.
     * @param specimen the specimen
     * @throws SQLException on error.
     */
    public void createOrUpdate(SpecimenCollectionSummary specimen) throws SQLException {
        SpecimenCollectionSummaryDao dao = new SpecimenCollectionSummaryDao(connection);
        dao.createOrIncrementSpecimen(specimen);
    }
}
