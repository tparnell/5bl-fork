/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.dao;

import gov.nih.nci.cbm.domain.logicalmodel.Institution;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author smiller
 *
 */
public class InstitutionDao extends AbstractCbmDao {
    private static final String CREATE_ORGANIZATION = "insert into Organization (organizationID, name) values (?, ?)";
    
    private static final String CREATE_INSTITUTION = "insert into Institution (institutionID, homepageURL) "
            + "values (?, ?)";

    private static final String SEARCH_BY_ID_SQL = "select institutionID from Institution where institutionID = ?";
    
    private static final String DELETE_ALL_ORG = "delete from Organization";
    
    private static final String DELETE_ALL_INST = "delete from Institution";
    
    /**
     * Create the dao.
     * @param connection the connection to use.
     */
    public InstitutionDao(Connection connection) {
        super(connection);
    }
    
    /**
     * delete all data saved.
     * @throws SQLException on error.
     */
    public void deleteAllInstitutions() throws SQLException {
        executeUpdate(DELETE_ALL_INST, new Object[] {});
        executeUpdate(DELETE_ALL_ORG, new Object[] {});
    }
    
    /**
     * creates or retrieves the inst.
     * @param inst the inst
     * @return the id
     * @throws SQLException on error.
     */
    public long createOrRetrieveInstitution(Institution inst) throws SQLException {
        Long id = getInstitutionById(inst.getId());
        if (id == null) {
            id = createInstitution(inst);
        }
        return id;
    }

    private Long createInstitution(Institution inst) throws SQLException {
        executeUpdate(CREATE_ORGANIZATION, new Object[] {inst.getId(), inst.getName()});
        executeUpdate(CREATE_INSTITUTION, new Object[] {inst.getId(), inst.getHomepageURL()});
        return inst.getId();
    }

    @SuppressWarnings("rawtypes")
    private Long getInstitutionById(Long id) throws SQLException {
        List results = executePreparedQuery(SEARCH_BY_ID_SQL, new Object[] {id});
        if (!results.isEmpty()) {
            return id;
        }
        return null;
    }
}