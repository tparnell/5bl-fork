/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.dao;

import gov.nih.nci.cbm.domain.logicalmodel.SpecimenCollectionSummary;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author smiller
 *
 */
public class SpecimenCollectionSummaryDao extends AbstractCbmDao {
    
    private static final String SEARCH_FOR_MATCH = "select " 
        + "SpecimenCollectionSummary.specimenCollectionSummaryID from "
        + "SpecimenCollectionSummary "
        + "where " 
        + "SpecimenCollectionSummary.patientAgeAtCollection = ? and "
        + "SpecimenCollectionSummary.specimenType = ? and " 
        + "SpecimenCollectionSummary.is_collected_from = ?";
    
    private static final String INCREMENT_COUNT = "update SpecimenCollectionSummary set count = count + 1 where "
        + "specimenCollectionSummaryID = ?";
    
    private static final String CREATE_PRESERVATION = "insert into Preservation (preservationType, "
        + "storageTemperatureInCentegrades, preservationID) values (?, ?, ?) ";
    
    private static final String CREATE_SPECIMEN = "insert into SpecimenCollectionSummary (anatomicSource, "
        + "count, patientAgeAtCollection, specimenType, specimenCollectionSummaryID, is_collected_from, undergoes) "
        + "values (?, ?, ?, ?, ?, ?, ?)";
    
    private static final String DELETE_PRESERVATION = "delete from Preservation";
    private static final String DELETE_SPECIMEN = "delete from SpecimenCollectionSummary";
    
    /**
     * create the dao.
     * @param connection the connection to sue.
     */
    public SpecimenCollectionSummaryDao(Connection connection) {
        super(connection);
    }
    
    /**
     * delete all data saved.
     * @throws SQLException on error.
     */
    public void deleteAllSpecimens() throws SQLException {
        executeUpdate(DELETE_PRESERVATION, new Object[] {});
        executeUpdate(DELETE_SPECIMEN, new Object[] {});
        
        ParticipantCollectionSummaryDao participantDao = new ParticipantCollectionSummaryDao(getConnection());
        participantDao.deleteAllParticipants();
    }
    
    /**
     * Create the specimen or increment its count if it already exists.
     * @param s the specimen
     * @return the id.
     * @throws SQLException on error.
     */
    public long createOrIncrementSpecimen(SpecimenCollectionSummary s) throws SQLException {
        ParticipantCollectionSummaryDao participantDao = new ParticipantCollectionSummaryDao(getConnection());
        
        // Note this is a possible bug, at the point we are this point in the translation, we don't know if we 
        // are adding the same patient, who happened to have multiple specimens, again or if we really have
        // two new participants.  We would have to track the map of 5bl particpant ids to cbm participant ids
        // to fix this bug.  That is out of scope for the initial version.
        Long participantId = participantDao.createOrIncrementParticipant(s.getParticipantCollectionSummary());
        
        Long id = getMatchingSpecimen(s, participantId);
        if (id != null) {
            incrementCount(id);
        } else {
            id = createSpecimen(s, participantId);
        }
        return id.longValue();
    }
    
    @SuppressWarnings("rawtypes")
    private Long getMatchingSpecimen(SpecimenCollectionSummary s, Long participantId) throws SQLException {
        String sql;
        List results;
        sql = SEARCH_FOR_MATCH;
        results = executePreparedQuery(sql, new Object[] {
                s.getPatientAgeAtCollection(), 
                s.getSpecimenType(), 
                participantId
        });
        if (!results.isEmpty()) {
            return Long.valueOf(((Integer) results.get(0)).longValue());
        }
        return null;
    }
    
    private void incrementCount(Long id) throws SQLException {
        executeUpdate(INCREMENT_COUNT, new Object[] {id});
    }
    
    private Long createSpecimen(SpecimenCollectionSummary s, Long participantId) throws SQLException {
        executeUpdate(CREATE_SPECIMEN, new Object[] {s.getAnatomicSource(), 1, s.getPatientAgeAtCollection(), 
                s.getSpecimenType(), s.getId(), participantId, s.getId()});
        
        executeUpdate(CREATE_PRESERVATION, new Object[] {s.getUndergoes().getPreservationType(),
                s.getUndergoes().getStorageTemperatureInCentegrades(), s.getId()});
        
        return s.getId();
    }
}
