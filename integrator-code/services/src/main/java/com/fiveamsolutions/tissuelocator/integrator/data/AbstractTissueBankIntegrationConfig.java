/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.data;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.validator.Length;
import org.hibernate.validator.Min;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.TissueBankAdapter;

/**
 * @author smiller
 *
 */
@Entity
@Table(name = "integration_config")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractTissueBankIntegrationConfig implements PersistentObject, Auditable {

    private static final long serialVersionUID = 1L;
    private static final int MAX_LENGTH = 254;
    private static final int MIN_NOTIFICATION_INTERVAL = 0;

    private Long id;
    private Long institutionId;
    private Date startTime;
    private long interval;
    private List<String> codeSets;
    private List<DefaultImportValueConfig> defaultImportValueConfigs;

    /** If the interval of time elapses without a data sync (days), the system should send out a notification email. */
    private int notificationInterval;
    private String notificationEmail;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get the adapter to use.
     * @return the adapter.
     */
    @Transient
    public abstract TissueBankAdapter getAdapter();

    /**
     * @return the institutionId
     */
    @NotNull
    @Column(unique = true)
    public Long getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(Long institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the startTime
     */
    @NotNull
    @Temporal(TemporalType.TIME)
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the interval
     */
    @NotNull
    public long getInterval() {
        return interval;
    }

    /**
     * @param interval the interval to set
     */
    public void setInterval(long interval) {
        this.interval = interval;
    }

    /**
     * @return the codeSets
     */
    @CollectionOfElements
    @JoinTable(name = "config_code_set", joinColumns = @JoinColumn(name = "config_id", nullable = false))
    @Column(name = "code_set_name", nullable = false)
    @IndexColumn(name = "index", nullable = false)
    public List<String> getCodeSets() {
        return this.codeSets;
    }

    /**
     * @param codeSets the codeSets to set
     */
    public void setCodeSets(List<String> codeSets) {
        this.codeSets = codeSets;
    }

    /**
     * @return the notificationInterval
     */
    @NotNull
    @Min(value = MIN_NOTIFICATION_INTERVAL)
    @Column(name = "notification_interval")
    public int getNotificationInterval() {
        return notificationInterval;
    }

    /**
     * @param notificationInterval the notificationInterval to set
     */
    public void setNotificationInterval(int notificationInterval) {
        this.notificationInterval = notificationInterval;
    }

    /**
     * @return the notificationEmail
     */
    @NotEmpty
    @Length(max = MAX_LENGTH)
    @Column(name = "notification_email")
    public String getNotificationEmail() {
        return notificationEmail;
    }

    /**
     * @param notificationEmail the notificationEmail to set
     */
    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    /**
     * @return A list of default value configurations.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @Valid
    @JoinTable(name = "default_import_value_configs",
                joinColumns = @JoinColumn(name = "integration_config_id"),
                inverseJoinColumns = @JoinColumn(name = "default_import_value_config_id"))
    @ForeignKey(name = "CONFIG_VALUE_FK",
                inverseName = "VALUE_CONFIG_FK")
    @Cascade(value = CascadeType.ALL)
    public List<DefaultImportValueConfig> getDefaultImportValueConfigs() {
        return defaultImportValueConfigs;
    }

    /**
     * @param defaultImportValueConfigs A list of default value configurations.
     */
    public void setDefaultImportValueConfigs(
            List<DefaultImportValueConfig> defaultImportValueConfigs) {
        this.defaultImportValueConfigs = defaultImportValueConfigs;
    }
    
}
