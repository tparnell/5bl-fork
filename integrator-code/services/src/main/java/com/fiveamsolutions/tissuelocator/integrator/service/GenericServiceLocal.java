/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service;

import java.util.List;

import javax.ejb.Local;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.service.GenericDataService;
import com.fiveamsolutions.nci.commons.service.GenericSearchService;

/**
 * Interface for the local Genreic Data Service.
 * @param <T> The type of the objects to be manipulated.
 * @author Scott Miller
 */
@Local
public interface GenericServiceLocal<T extends PersistentObject>
    extends GenericDataService, GenericSearchService<T, SearchCriteria<T>> {

    /**
     * Saves a persistent object to the database.
     * @param o the persistent object to save
     * @return the id of the newly saved object
     */
    Long savePersistentObject(T o);

    /**
     * deletes the given object from the database.
     * @param o the object to delete
     */
    void deletePersistentObject(T o);

    /**
     * Get all instances of a given type.
     * @param <TYPE> the type of the objects to retrieve
     * @param type the type of the objects to retrieve
     * @return a collection of all persisted instances of this type
     */
    <TYPE extends PersistentObject> List<TYPE> getAll(Class<TYPE> type);
}