/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.util.Map;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.Institution;

/**
 * Synchronizes an object from the 5bl core database with an imported object and calls the 5bl core
 *  remote service to save the object.
 * @author bpickeral
 * @param <T> PersistentObject to Synchronize
 */
public interface SynchronizationAdapter <T extends PersistentObject> {
    /**
     * Synchronizes an object from the 5bl core database with an imported object and calls the 5bl core
     *  remote service to save the object.
     * @param importedObject imported persistent object
     * @param preloadedObject the persistent object previously loaded, if any
     * @param preloadedDependencies a map of dependent persistent objects that have been preloaded.  The keys
     * of this map are the property names of the dependent objects, and the values are the objects themselves.
     * @param institution the Institution object retrieved from core
     * @param translationMap the map of code translations to be used during the synchronization
     * @return saved T object
     */
    T synchronize(T importedObject, T preloadedObject, Map<String, PersistentObject> preloadedDependencies,
            Institution institution, Map<String, Map<String, Map<String, String>>> translationMap);
}
