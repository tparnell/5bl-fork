/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.util;

import java.util.HashMap;
import java.util.Map;

/**
 * This cache allows for items to be added to a single cache, or all of the caches.  This intended use
 * for this class is to allow preloaded items that are retrieved from a database to go in to a single cache,
 * but to allow for newly created items that the other caches need to know about to go in to all caches.
 *
 * @author smiller
 * @param <T>  the class to cahce.
 */
public class ThreadsyncedCache<T> {
    private final Map<Object, Map<String, T>> caches = new HashMap<Object, Map<String, T>>();
    
    /**
     * Add the value to the cache.
     * @param cacheId the cache this method will ensure exists.
     * @param key the key
     * @param val the value
     */
    public synchronized void addToAllCaches(Object cacheId, String key, T val) {
        initCache(cacheId);
        
        for (Map<String, T> cache : caches.values()) {
            cache.put(key, val);
        }
    }
    
    /**
     * Add the value to the cache.
     * @param cacheId the id of the cache
     * @param key the key
     * @param val the value
     */
    public synchronized void addToCache(Object cacheId, String key, T val) {
        initCache(cacheId);
        
        caches.get(cacheId).put(key, val);
    }

    private void initCache(Object cacheId) {
        if (caches.get(cacheId) == null) {
            caches.put(cacheId, new HashMap<String, T>());
        }
    }
    
    /**
     * Get a value from the cache.
     * @param cacheId the id of the cache
     * @param key the key of the value in the cache
     * @return the value
     */
    public synchronized T getFromCache(Object cacheId, String key) {
        Map<String, T> cache = caches.get(cacheId);
        if (cache != null) {
            return cache.get(key);
        } else {
            return null;
        }
    }

    /**
     * clear the cache.
     * @param cacheId the cache id
     */
    public synchronized void clearCache(Object cacheId) {
        caches.remove(cacheId);
    }
}