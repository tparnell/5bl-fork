/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.data;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.tissuelocator.integrator.service.adapter.FileImportAdapter;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.ImportFileConverter;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.TissueBankAdapter;

/**
 * @author smiller
 *
 */
@Entity
@PrimaryKeyJoinColumn(name = "config_id")
@Table(name = "file_import_integration_config")
public class FileImportIntegrationConfig extends AbstractTissueBankIntegrationConfig {
    private static final long serialVersionUID = 1L;
    private static final int MAX_LENGTH = 254;
    private static final int FTP_LOGIN_LENGTH = 24;

    private String ftpSite;
    private String username;
    private String password;
    private String rootDirectory;
    private String fileConverterClass;
    private int numberOfProcessingThreads;
    private Delimiter delimiter;
    private Set<String> ignoredColumns = new HashSet<String>();

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public TissueBankAdapter getAdapter() {
        return new FileImportAdapter();
    }

    /**
     * @return the ftpSite
     */
    @Length(max = MAX_LENGTH)
    public String getFtpSite() {
        return ftpSite;
    }

    /**
     * @param ftpSite the ftpSite to set
     */
    public void setFtpSite(String ftpSite) {
        this.ftpSite = ftpSite;
    }

    /**
     * @return the username
     */
    @Length(max = FTP_LOGIN_LENGTH)
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    @Length(max = FTP_LOGIN_LENGTH)
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the rootDirectory
     */
    @Length(max = MAX_LENGTH)
    public String getRootDirectory() {
        return rootDirectory;
    }

    /**
     * @param rootDirectory the rootDirectory to set
     */
    public void setRootDirectory(String rootDirectory) {
        this.rootDirectory = rootDirectory;
    }

    /**
     * @return The ImportFileConverter class to be used with this configuration.
     */
    @NotEmpty
    @Length(max = MAX_LENGTH)
    @Column(name = "file_converter_class")
    public String getFileConvertorClass() {
        return fileConverterClass;
    }

    /**
     * @return the numberOfProcessingThreads
     */
    @NotNull
    @Column(name = "num_processing_threads")
    public int getNumberOfProcessingThreads() {
        return numberOfProcessingThreads;
    }

    /**
     * @param numberOfProcessingThreads the numberOfProcessingThreads to set
     */
    public void setNumberOfProcessingThreads(int numberOfProcessingThreads) {
        this.numberOfProcessingThreads = numberOfProcessingThreads;
    }

    /**
     * @param converterClass The ImportFileConverter class to be used with this configuration.
     */
    public void setFileConvertorClass(String converterClass) {
        fileConverterClass = converterClass;
    }

    /**
     * @return The delimiter character to be used for CSV file imports.
     */
    @Column(name = "delimiter")
    @Enumerated(value = EnumType.STRING)
    public Delimiter getDelimiter() {
        return delimiter;
    }

    /**
     * @param delimiter The delimiter character to be used for CSV file imports.
     */
    public void setDelimiter(Delimiter delimiter) {
        this.delimiter = delimiter;
    }


    /**
     * @return the ignoredColumns
     */
    @CollectionOfElements(fetch = FetchType.EAGER)
    @JoinTable(name = "config_ignored_column", joinColumns = @JoinColumn(name = "config_id", nullable = false))
    @Column(name = "ignored_column_name", nullable = false)
    @ForeignKey(name = "config_ignored_column_fk")
    public Set<String> getIgnoredColumns() {
        return ignoredColumns;
    }

    /**
     * @param ignoredColumns the ignoredColumns to set
     */
    public void setIgnoredColumns(Set<String> ignoredColumns) {
        this.ignoredColumns = ignoredColumns;
    }

    /**
     * Returns the delimiter character for this configuration.
     * @return the delimiter character for this configuration.
     */
    @Transient
    public char getDelimiterCharacter() {
        return getDelimiter().getDelimiterChar();
    }

    /**
     * @return The import file converter for this configuration.
     */
    @Transient
    public ImportFileConverter getImportFileConverter() {
        try {
            return (ImportFileConverter) Class.forName(getFileConvertorClass()).newInstance();
        } catch (InstantiationException e) {
            throw new IllegalArgumentException("Invalid converter class.", e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("Invalid converter class.", e);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("Invalid converter class.", e);
        }
    }

    /**
     * Delimiter characters.
     * @author gvaughn
     *
     */
    public enum Delimiter {
        /**
         * Comma delimiter.
         */
        COMMA(','),

        /**
         * Tab delimiter.
         */
        TAB('\t');

        private final char delimiterChar;

        private Delimiter(char delimiterChar) {
            this.delimiterChar = delimiterChar;
        }

        /**
         * @return The delimiter character.
         */
        public char getDelimiterChar() {
            return delimiterChar;
        }
    }
}
