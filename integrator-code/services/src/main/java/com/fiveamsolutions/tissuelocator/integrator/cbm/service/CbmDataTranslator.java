/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.cbm.service;

import gov.nih.nci.cbm.domain.logicalmodel.CollectionProtocol;
import gov.nih.nci.cbm.domain.logicalmodel.Diagnosis;
import gov.nih.nci.cbm.domain.logicalmodel.Institution;
import gov.nih.nci.cbm.domain.logicalmodel.ParticipantCollectionSummary;
import gov.nih.nci.cbm.domain.logicalmodel.Preservation;
import gov.nih.nci.cbm.domain.logicalmodel.Race;
import gov.nih.nci.cbm.domain.logicalmodel.SpecimenCollectionContact;
import gov.nih.nci.cbm.domain.logicalmodel.SpecimenCollectionSummary;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.data.Contact;
import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.integrator.data.Translation;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author smiller
 */
public class CbmDataTranslator {
    private static final int MONTHS_IN_YEAR = 12;
    
    private static final int SHORT_FIELD_MAX_LENGTH = 50;

    private final String codeSetName;
    private final TranslationServiceLocal translationService;

    /**
     * Creates the translator.
     * 
     * @param codeSetName the code set to use for the translations.
     */
    public CbmDataTranslator(String codeSetName) {
        this.translationService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        this.codeSetName = codeSetName;
    }

    /**
     * This translates the given 5bl specimen to the cbm representation of the same data.
     * 
     * @param source the 5bl specimen.
     * @return the cbm representation.
     */
    public SpecimenCollectionSummary translateSpecimen(Specimen source) {
        if (source == null) {
            return null;
        }

        SpecimenCollectionSummary dest = new SpecimenCollectionSummary();
        dest.setId(source.getId());

        // this may be updated when persisted if another record with the same values already exists.
        dest.setCount(1);

        // translate coded values
        dest.setPatientAgeAtCollection(Integer.toString(translateAge(source.getPatientAgeAtCollection(), 
                source.getPatientAgeAtCollectionUnits())));
        dest.setSpecimenType(getTranslation(source.getSpecimenType().getName(), SpecimenType.class.getSimpleName()));
        dest.setUndergoes(new Preservation());

        // translate linked objects
        dest.setParticipantCollectionSummary(translateParticipant(source.getParticipant(), 
                source.getPathologicalCharacteristic(), 
                source.getProtocol()));

        return dest;
    }

    private int translateAge(int patientAgeAtCollection, TimeUnits patientAgeAtCollectionUnits) {
        if (patientAgeAtCollectionUnits.equals(TimeUnits.YEARS)) {
            return patientAgeAtCollection;
        } else {
            int years = patientAgeAtCollection / MONTHS_IN_YEAR;
            if (patientAgeAtCollection % MONTHS_IN_YEAR > MONTHS_IN_YEAR / 2) {
                years++;
            }
            return years;
        }
    }

    private ParticipantCollectionSummary translateParticipant(Participant source, 
            AdditionalPathologicFinding additionalPathologicFinding, 
            com.fiveamsolutions.tissuelocator.data.CollectionProtocol protocol) {
        if (source == null) {
            return null;
        }
        ParticipantCollectionSummary dest = new ParticipantCollectionSummary();
        dest.setId(source.getId());
        
        // this may be updated when persisted if another record with the same values already exists.
        dest.setCount(1);
        
        // translate linked objects
        dest.setCollectionProtocol(translateProtocol(protocol));
        
        // need to get diagnsis codes from the linked specimens.
        dest.setDiagnosis(translateFinding(additionalPathologicFinding));
        
        // translate coded values.
        dest.setEthnicity(getTranslation(source.getEthnicity().name(), Ethnicity.class.getSimpleName()));
        dest.setGender(getTranslation(source.getGender().name(), Gender.class.getSimpleName()));
        dest.setRaces(translateRaces(source.getRaces()));
        
        return dest;
    }

    private CollectionProtocol translateProtocol(com.fiveamsolutions.tissuelocator.data.CollectionProtocol source) {
        if (source == null) {
            return null;
        }
        CollectionProtocol dest = new CollectionProtocol();
        dest.setId(source.getId());
        dest.setIdentifier(Long.toString(source.getId()));
        dest.setAnnotationAvailability(null);
        dest.setContact(translateContact(source.getContact()));
        dest.getContact().setId(source.getId());
        dest.setDateLastUpdated(null);
        dest.setEndDate(source.getEndDate());
        dest.setInstitution(translateInstitution(source.getInstitution()));
        dest.setName(source.getName());
        dest.setSpecimenAvailabilitySummary(null);
        dest.setStartDate(source.getStartDate());
        return dest;
    }

    private SpecimenCollectionContact translateContact(Contact source) {
        SpecimenCollectionContact dest = new SpecimenCollectionContact();
        if (source != null) {
            dest.setEmailAddress(cleanShortStringField(source.getEmail()));
            dest.setFirstName(cleanShortStringField(source.getFirstName()));
            dest.setLastName(cleanShortStringField(source.getLastName()));
            dest.setFullName(cleanShortStringField(source.getFirstName() + " " + source.getLastName()));
            dest.setPhone(cleanShortStringField(source.getPhone()));
        } else {
            dest.setEmailAddress("");
            dest.setFirstName("");
            dest.setLastName("");
            dest.setFullName("");
            dest.setPhone("");
        }
        return dest;
    }

    private Institution translateInstitution(com.fiveamsolutions.tissuelocator.data.Institution source) {
        Institution dest = new Institution();
        dest.setId(source.getId());
        dest.setName(source.getName());
        dest.setHomepageURL(cleanShortStringField(source.getHomepage()));
        return dest;
    }

    private Set<Diagnosis> translateFinding(AdditionalPathologicFinding source) {
        Set<Diagnosis> results = new HashSet<Diagnosis>();
        Diagnosis dest = new Diagnosis();
        dest.setDiagnosisType(getTranslation(source.getName(), AdditionalPathologicFinding.class.getSimpleName()));
        if (StringUtils.isNotEmpty(dest.getDiagnosisType())) {
            results.add(dest);
        }
        return results;
    }

    private Set<Race> translateRaces(List<com.fiveamsolutions.tissuelocator.data.Race> races) {
        Set<Race> dest = new HashSet<Race>();
        for (com.fiveamsolutions.tissuelocator.data.Race source : races) {
            Race r = new Race();
            r.setName(getTranslation(source.name(), 
                    com.fiveamsolutions.tissuelocator.data.Race.class.getSimpleName()));
            dest.add(r);
        }
        return dest;
    }
    
    private String getTranslation(String sourceValue, String codeType) {
        Translation t = translationService.getTranslation(sourceValue, this.codeSetName, codeType);
        if (t != null) {
            return t.getDestinationValue();
        }
        return null;
    }
    
    private String cleanShortStringField(String source) {
        return StringUtils.substring(StringUtils.defaultString(source), 0 , SHORT_FIELD_MAX_LENGTH);
    }
}