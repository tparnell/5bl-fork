create table config_ignored_column (
    config_id int8 not null,
    ignored_column_name varchar(255) not null
);
alter table config_ignored_column add constraint config_ignored_column_fk foreign key (config_id) references file_import_integration_config;
