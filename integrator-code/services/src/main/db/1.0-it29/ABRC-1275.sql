alter table export_config add column code_set varchar(254) not null;
alter table cbm_export_config add column db_driver_class varchar(254) not null;
alter table cbm_export_config add column db_url varchar(254) not null;
alter table cbm_export_config add column db_username varchar(254);
alter table cbm_export_config add column db_password varchar(254);