/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.service;

import static org.junit.Assert.*;
import gov.nih.nci.cbm.domain.logicalmodel.CollectionProtocol;
import gov.nih.nci.cbm.domain.logicalmodel.ParticipantCollectionSummary;
import gov.nih.nci.cbm.domain.logicalmodel.Race;
import gov.nih.nci.cbm.domain.logicalmodel.SpecimenCollectionContact;
import gov.nih.nci.cbm.domain.logicalmodel.SpecimenCollectionSummary;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.integrator.data.Translation;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceBean;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.test.CbmSpecimenCreationHelper;
import com.fiveamsolutions.tissuelocator.integrator.test.TestServiceLocator;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author smiller
 *
 */
public class CbmDataTranslatorTest {
    
    private static final int SHORT_FIELD_MAX_LENGTH = 50;
    private static final String FIFTY_CHARS = "12345678901234567890123456789012345678901234567890";

    private static final String CBM = "CBM";

    /**
     * Initialize the registry.
     */
    @Before
    public void setUpRegistry() {
        TestServiceLocator locator = new TestServiceLocator() {

            /**
             * {@inheritDoc}
             */
            @Override
            public TranslationServiceLocal getTranslationService() {
                return new TranslationServiceBean() {

                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    public Translation getTranslation(String sourceValue, String codeSetName, String codeType) {
                        Translation t = new Translation();
                        t.setCodeSetName(codeSetName);
                        t.setCodeType(codeType);
                        t.setDestinationValue(sourceValue + codeSetName);
                        t.setSourceValue(sourceValue);
                        return t;
                    }
                };
            }
        };
        TissueLocatorIntegratorRegistry.getInstance().setServiceLocator(locator);
    }
    
    /**
     * test the translator.
     */
    @Test
    public void testTranslation() {
        CbmSpecimenCreationHelper helper = new CbmSpecimenCreationHelper();
        Specimen source = helper.createSpecimen(1, null, null);
        
        CbmDataTranslator translator = new CbmDataTranslator(CBM);
        SpecimenCollectionSummary destination = translator.translateSpecimen(null);
        assertNull(destination);

        destination = translator.translateSpecimen(source);
        verifySpecimenFields(source, destination);
        verifyParticipantFields(source, destination.getParticipantCollectionSummary());
        verifyCollectionProtocolFields(source, destination.getParticipantCollectionSummary().getCollectionProtocol());
        
        source.setPatientAgeAtCollection(source.getPatientAgeAtCollection() - 1);
        destination = translator.translateSpecimen(source);
        assertEquals("1", destination.getPatientAgeAtCollection());
        
        source.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        destination = translator.translateSpecimen(source);
        assertEquals(Integer.toString(source.getPatientAgeAtCollection()), destination.getPatientAgeAtCollection());

        source.setProtocol(null);
        destination = translator.translateSpecimen(source);
        assertNull(destination.getParticipantCollectionSummary().getCollectionProtocol());
        
        source.setParticipant(null);
        destination = translator.translateSpecimen(source);
        assertNull(destination.getParticipantCollectionSummary());
    }
    
    /**
     * test the translator.
     */
    @Test
    public void testTranslationNullFields() {
        CbmSpecimenCreationHelper helper = new CbmSpecimenCreationHelper();
        Specimen source = helper.createSpecimen(1, null, null);
        source.getProtocol().setContact(null);
        
        CbmDataTranslator translator = new CbmDataTranslator(CBM);
        SpecimenCollectionSummary destination = translator.translateSpecimen(source);
        SpecimenCollectionContact contact = 
            destination.getParticipantCollectionSummary().getCollectionProtocol().getContact();
        assertEquals(null, destination.getAnatomicSource());
        assertEquals(null, destination.getUndergoes().getPreservationType());
        assertEquals("", contact.getEmailAddress());
        assertEquals("", contact.getFirstName());
        assertEquals("", contact.getLastName());
        assertEquals("", contact.getFullName());
        assertEquals("", contact.getPhone());
        
        destination = translator.translateSpecimen(source);
        assertEquals(null, destination.getUndergoes().getPreservationType());
        assertNull(destination.getUndergoes().getStorageTemperatureInCentegrades());
    }
    
    /**
     * test the translator.
     */
    @Test
    public void testTranslationLongFields() {
        CbmSpecimenCreationHelper helper = new CbmSpecimenCreationHelper();
        Specimen source = helper.createSpecimen(1, null, null);
        source.getProtocol().getContact().setEmail(FIFTY_CHARS + "test");
        source.getProtocol().getContact().setFirstName(FIFTY_CHARS + "test");
        source.getProtocol().getContact().setLastName(FIFTY_CHARS + "test");
        source.getProtocol().getContact().setPhone(FIFTY_CHARS + "test");
        source.getProtocol().getInstitution().setHomepage(FIFTY_CHARS + "test");
        
        CbmDataTranslator translator = new CbmDataTranslator(CBM);
        SpecimenCollectionSummary destination = translator.translateSpecimen(source);
        verifySpecimenFields(source, destination);
        verifyParticipantFields(source, destination.getParticipantCollectionSummary());
        verifyCollectionProtocolFields(source, destination.getParticipantCollectionSummary().getCollectionProtocol());
    }

    private void verifySpecimenFields(Specimen source, SpecimenCollectionSummary dest) {
        assertEquals(source.getId(), dest.getId());
        assertEquals(source.getSpecimenType().getName() + CBM, dest.getSpecimenType());
        assertEquals(1, dest.getCount().intValue());
        assertEquals("2", dest.getPatientAgeAtCollection());
    }

    private void verifyParticipantFields(Specimen source, ParticipantCollectionSummary dest) {
        assertEquals(source.getParticipant().getId(), dest.getId());
        assertEquals(1, dest.getCount().intValue());
        assertEquals(source.getParticipant().getEthnicity().name() + CBM, dest.getEthnicity());
        assertEquals(source.getParticipant().getGender().name() + CBM, dest.getGender());
        assertEquals(1, dest.getDiagnosis().size());
        assertEquals(source.getPathologicalCharacteristic().getName() + CBM, 
                dest.getDiagnosis().iterator().next().getDiagnosisType());
        assertEquals(source.getParticipant().getRaces().size(), dest.getRaces().size());
        for (Race destRace : dest.getRaces()) {
            boolean found = false;
            for (com.fiveamsolutions.tissuelocator.data.Race sourceRace : source.getParticipant().getRaces()) {
                if ((sourceRace.name() + CBM).equals(destRace.getName())) {
                    found = true;
                }
            }
            assertTrue(found);
        }
    }

    private void verifyCollectionProtocolFields(Specimen source, CollectionProtocol dest) {
        assertEquals(source.getProtocol().getName(), dest.getName());
        assertEquals(source.getProtocol().getId().toString(), dest.getIdentifier());
        assertEquals(source.getProtocol().getId(), dest.getId());
        assertEquals(null, dest.getAnnotationAvailability());
        
        assertEquals(source.getProtocol().getId(), dest.getContact().getId());
        assertEquals(cleanShortStringField(source.getProtocol().getContact().getEmail()), 
                dest.getContact().getEmailAddress());
        assertEquals(cleanShortStringField(source.getProtocol().getContact().getFirstName()), 
                dest.getContact().getFirstName());
        assertEquals(cleanShortStringField(source.getProtocol().getContact().getLastName()), 
                dest.getContact().getLastName());
        assertEquals(null, dest.getContact().getMiddleNameOrInitial());
        assertEquals(cleanShortStringField(source.getProtocol().getContact().getFirstName() + " " 
                + source.getProtocol().getContact().getLastName()), dest.getContact().getFullName());
        assertEquals(cleanShortStringField(source.getProtocol().getContact().getPhone()), 
            dest.getContact().getPhone());
        
        assertNull(dest.getDateLastUpdated());
        assertEquals(source.getProtocol().getEndDate(), dest.getEndDate());
        assertEquals(source.getProtocol().getStartDate(), dest.getStartDate());
        assertNull(dest.getSpecimenAvailabilitySummary());
        
        assertEquals(source.getProtocol().getInstitution().getId(), dest.getInstitution().getId());
        assertEquals(source.getProtocol().getInstitution().getName(), dest.getInstitution().getName());
        assertEquals(cleanShortStringField(source.getProtocol().getInstitution().getHomepage()), 
                dest.getInstitution().getHomepageURL());
    }
    
    private String cleanShortStringField(String source) {
        return StringUtils.substring(StringUtils.defaultString(source), 0 , SHORT_FIELD_MAX_LENGTH);
    }
}
