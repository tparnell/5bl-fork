/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.integrator.services.adapter.SpecimenHelper;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;

/**
 * @author bpickeral
 *
 */
public class MockSpecimenServiceRemote implements SpecimenServiceRemote {

    /**
     * The number of specimens.
     */
    public static final int SPECIMEN_COUNT = 20;

    /**
     * {@inheritDoc}
     */
    public Map<String, Specimen> getSpecimens(Long institutionId, Collection<String> extIds) {
        Map<String, Specimen> result = new HashMap<String, Specimen>();
        // If the externalId is equal to EXTERNAL_ID, we are testing the case that a Specimen exists
        // that we want to update, otherwise we are testing the case that a Specimen does not exist
        if (extIds != null && !extIds.isEmpty() && extIds.iterator().next() != null
                && extIds.iterator().next().equals(SpecimenHelper.EXTERNAL_ID)) {
            Specimen s = new Specimen();
            s.setId(1L);
            s.setExternalId(SpecimenHelper.EXTERNAL_ID);
            s.setExternalIdAssigner(new Institution());
            s.setParticipant(new Participant());
            s.setProtocol(new CollectionProtocol());
            result.put(SpecimenHelper.EXTERNAL_ID, s);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public Specimen importSpecimen(Specimen specimen) {
        // Return the specimen passed in so we can check that the attributes were copied correctly
        // into the empty Specimen object after getSpecimen(institution, EXTERNAL_ID) was called.
        return specimen;
    }

    /**
     * {@inheritDoc}
     */
    public List<Specimen> getAvailableSpecimens(int page, int pageSize) {
        CbmSpecimenCreationHelper helper = new CbmSpecimenCreationHelper();
        List<Specimen> results = new ArrayList<Specimen>();
        for (int i = 1; i <= pageSize && i + page * pageSize <= SPECIMEN_COUNT; i++) {
            Specimen s = helper.createSpecimen(i + page * pageSize, null, null);
            results.add(s);
        }
        return results;
    }
}
