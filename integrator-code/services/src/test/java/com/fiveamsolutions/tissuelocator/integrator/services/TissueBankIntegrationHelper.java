/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.DefaultImportValueConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.DefaultImportValueType;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.ImportBatch;

/**
 * @author bpickeral
 *
 */
public class TissueBankIntegrationHelper {

    private static final int SECONDS_IN_FUTURE = 20;
    private static final String XML_CONVERTOR_CLASS = 
        "com.fiveamsolutions.tissuelocator.integrator.service.adapter.XmlImportFileConverter";

    /**
     * Notification email to send to.
     */
    public static final String NOTIFICATION_EMAIL = "test@5amsolutions.com";

    /**
     * Creates an ImportBatch object.
     * @param config AbstractTissueBankIntegrationConfig used to connect to the FTP server.
     * @return ImportBatch object.
     */
    public static ImportBatch createBatch(AbstractTissueBankIntegrationConfig config) {
        ImportBatch batch = new ImportBatch();
        batch.setEndTime(new Date());
        batch.setStartTime(new Date());
        batch.setFailures(1);
        batch.setImports(1);
        batch.setConfig(config);
        return batch;
    }

    /**
     * Creates a FileImportIntegrationConfig object that should not execute when scheduled.
     * @param institutionId id of institution
     * @param interval time interval to call the batch
     * @return AbstractTissueBankIntegrationConfig object.
     */
    public static FileImportIntegrationConfig createConfig(Long institutionId,
            int interval) {
        return createConfig(institutionId, interval, getNonCurrentTime());
    }

    /**
     * Gets a time that is not equal to the current time. Useful when starting threads
     *  that should not start during unit tests.
     * @return Date object representing a time in a different hour than the current hour.
     */
    public static Date getNonCurrentTime() {
        Calendar firstExecution = Calendar.getInstance();
        // Set first execution time to 20 seconds in the future
        firstExecution.add(Calendar.SECOND, SECONDS_IN_FUTURE);
        return firstExecution.getTime();
    }

    /**
     * Creates a FileImportIntegrationConfig object.
     * @param institutionId id of institution
     * @param interval time interval to call the batch
     * @param startTime Start time of the import.
     * @return AbstractTissueBankIntegrationConfig object.
     */
    public static FileImportIntegrationConfig createConfig(Long institutionId,
            int interval, Date startTime) {
        FileImportIntegrationConfig config = new FileImportIntegrationConfig();
        config.setFtpSite("ftp://example.com/myfiles");
        config.setUsername("user");
        config.setPassword("password");
        config.setNotificationEmail(NOTIFICATION_EMAIL);
        config.setInstitutionId(institutionId);
        config.setInterval(interval);
        config.setStartTime(startTime);
        config.setNotificationInterval(interval);
        config.setCodeSets(new ArrayList<String>());
        config.getCodeSets().add("caTissue Base");
        config.setFileConvertorClass(XML_CONVERTOR_CLASS);
        config.setDefaultImportValueConfigs(new ArrayList<DefaultImportValueConfig>());
        config.getDefaultImportValueConfigs().add(createDefaultValueConfig());
        config.setNumberOfProcessingThreads(2);
        return config;
    }
    
    /**
     * @return A default value config.
     */
    public static DefaultImportValueConfig createDefaultValueConfig() {
        DefaultImportValueConfig config = new DefaultImportValueConfig();
        config.setPropertyPath("specimenType.name");
        config.setPropertyValue("tissue");
        config.setPropertyClassName(SpecimenType.class.getName());
        config.setValueType(DefaultImportValueType.OBJECT);
        return config;
    }
}
