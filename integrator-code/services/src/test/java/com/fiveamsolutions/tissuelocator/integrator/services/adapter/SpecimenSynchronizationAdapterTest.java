/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.integrator.data.CaTissueIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.Translation;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.SpecimenSynchronizationAdapter;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author bpickeral
 *
 */
public class SpecimenSynchronizationAdapterTest
        extends SynchronizationAdapterTest<SpecimenSynchronizationAdapter, Specimen> {

    private static final int MAX_AGE = 90;

    /**
     * test the synchronize method.
     */
    @Test
    public void testSynchronizeWithTooHighAge() {
        // Assert No Exceptions occur when creating a new object, since Mock methods are called
        // to store Specimen in core, there is nothing else to check for a newly created object
        Specimen importedObject = createNewImportedObject();
        importedObject.setPatientAgeAtCollection(MAX_AGE + 1);
        importedObject.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        Institution i = new Institution();
        i.setId(1L);
        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);
        Map<String, PersistentObject> dependencies = new HashMap<String, PersistentObject>();
        Specimen returned = getAdapter().synchronize(importedObject, null, dependencies, i, translationMap);
        assertEquals(MAX_AGE, returned.getPatientAgeAtCollection());
    }

    /**
     * Test price is set to negotiable if no prices are set.
     */
    @Test
    public void testNoPriceIncluded() {
        Specimen importedObject = createNewImportedObject();
        importedObject.setMinimumPrice(null);
        importedObject.setMaximumPrice(null);
        Institution i = new Institution();
        i.setId(1L);
        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);
        Map<String, PersistentObject> dependencies = new HashMap<String, PersistentObject>();
        Specimen returned = getAdapter().synchronize(importedObject, null, dependencies, i, translationMap);
        assertTrue(returned.isPriceNegotiable());
        
        importedObject = createNewImportedObject();
        dependencies = new HashMap<String, PersistentObject>();
        returned = getAdapter().synchronize(importedObject, null, dependencies, i, translationMap);
        assertFalse(returned.isPriceNegotiable());
        
        importedObject = createNewImportedObject();
        importedObject.setMinimumPrice(null);
        dependencies = new HashMap<String, PersistentObject>();
        returned = getAdapter().synchronize(importedObject, null, dependencies, i, translationMap);
        assertFalse(returned.isPriceNegotiable());
        assertNull(returned.getMinimumPrice());
        assertNotNull(returned.getMaximumPrice());
    }
    
    /**
     * Test the translation of custom properties.
     */
    @Test
    public void testTranslateCustomProperties() {
        Specimen importedObject = createNewImportedObject();
        importedObject.setMinimumPrice(null);
        importedObject.setMaximumPrice(null);
        Institution i = new Institution();
        i.setId(2L);
        createConfig();
        
        Map<String, Object> expected = new HashMap<String, Object>();
        expected.put("collectionType", SpecimenHelper.COLLECTION_TYPE);
        expected.put("anatomicSource", SpecimenHelper.ANATOMIC_SOURCE);
        expected.put("preservationType", SpecimenHelper.PRESERVATION_TYPE);
        expected.put("timeLapseToProcessing", SpecimenHelper.TIME_LAPSE_TO_PROCESSING);
        expected.put("specimenDensity", SpecimenHelper.SPECIMEN_DENSITY);
        expected.put("storageTemperature", SpecimenHelper.STORAGE_TEMPERATURE);
        
        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(2L);
        Map<String, PersistentObject> dependencies = new HashMap<String, PersistentObject>();
        Specimen returned = getAdapter().synchronize(importedObject, null, dependencies, i, translationMap);
        verifyCustomProperties(returned, expected);
        
        createCustomPropertyTranlsation(tService, "test collectionType", 
                SpecimenHelper.COLLECTION_TYPE, "collectionType");
        createCustomPropertyTranlsation(tService, "test anatomicSource", 
                SpecimenHelper.ANATOMIC_SOURCE, "anatomicSource");
        
        importedObject = createNewImportedObject();
        importedObject.setCustomProperty("collectionType", "test collectionType");
        importedObject.setCustomProperty("anatomicSource", "test anatomicSource");
        translationMap = tService.getTranslationMap(2L);
        returned = getAdapter().synchronize(importedObject, null, dependencies, i, translationMap);
        verifyCustomProperties(returned, expected);
        
        createCustomPropertyTranlsation(tService, "test preservationType", 
                SpecimenHelper.PRESERVATION_TYPE, "preservationType");
        importedObject = createNewImportedObject();
        importedObject.setCustomProperty("collectionType", "test collectionType");
        importedObject.setCustomProperty("anatomicSource", "test anatomicSource");
        importedObject.setCustomProperty("preservationType", "test preservationType");
        translationMap = tService.getTranslationMap(2L);
        returned = getAdapter().synchronize(importedObject, null, dependencies, i, translationMap);
        verifyCustomProperties(returned, expected);
    }
    
    private void verifyCustomProperties(Specimen specimen, Map<String, Object> expected) {
        for (String customProperty : specimen.getCustomProperties().keySet()) {
            assertEquals(specimen.getCustomProperty(customProperty),
                    expected.get(customProperty));
        }
    }
    
    private void createCustomPropertyTranlsation(TranslationServiceLocal service, 
            String source, String dest, String type) {
        Translation t = new Translation();
        t.setSourceValue(source);
        t.setDestinationValue(dest);
        t.setCodeSetName("custom properties");
        t.setCodeType(type);
        service.savePersistentObject(t);
    }
    
    private void createConfig() {
        CaTissueIntegrationConfig config = new CaTissueIntegrationConfig();
        config.setInstitutionId(2L);
        config.setStartTime(new Date());
        config.setGridServiceUrl("http://caTissue.com");
        List<String> codeSets = new ArrayList<String>();
        codeSets.add("custom properties");
        config.setCodeSets(codeSets);
        config.setNotificationEmail("email@example.com");
        config.setNotificationInterval(1);
        TissueBankIntegrationConfigServiceLocal configService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
        configService.savePersistentObject(config);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Specimen createExistingImportedObject() {
        Specimen existingSpecimen = SpecimenHelper.INSTANCE.createSpecimen();
        return existingSpecimen;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Specimen createNewImportedObject() {
        Specimen newSpecimen = SpecimenHelper.INSTANCE.createSpecimen();
        // Set the externalId to an unknown id so the MockSpecimenServiceRemote returns null (specimen does not exist
        newSpecimen.setExternalId("some other id");
        newSpecimen.setMinimumPrice(BigDecimal.ZERO);
        return newSpecimen;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkSynchronizeExisting(Specimen updatedObject, Specimen importedObject) {
        assertEquals(importedObject.getPatientAgeAtCollection(), updatedObject.getPatientAgeAtCollection());
        assertEquals(importedObject.getPatientAgeAtCollectionUnits(), updatedObject.getPatientAgeAtCollectionUnits());
        assertEquals(importedObject.getAvailableQuantity(), updatedObject.getAvailableQuantity());
        assertEquals(importedObject.getAvailableQuantityUnits(), updatedObject.getAvailableQuantityUnits());
        assertSame(importedObject.getPathologicalCharacteristic(), updatedObject.getPathologicalCharacteristic());
        assertSame(importedObject.getSpecimenType(), updatedObject.getSpecimenType());
        assertEquals(importedObject.getMinimumPrice(), updatedObject.getMinimumPrice());
        assertEquals(importedObject.getMaximumPrice(), updatedObject.getMaximumPrice());
        assertFalse(importedObject.isPriceNegotiable());
        assertEquals(importedObject.getStatus(), updatedObject.getStatus());
        assertEquals(updatedObject.getPreviousStatus(), updatedObject.getPreviousStatus());
        assertEquals(importedObject.getCreatedDate(), updatedObject.getCreatedDate());
        assertSame(importedObject.getParticipant(), updatedObject.getParticipant());
        assertSame(importedObject.getProtocol(), updatedObject.getProtocol());
        assertSame(importedObject.getCollectionYear(), updatedObject.getCollectionYear());
    }

    /**
     * Test the updateStatus method.
     */
    @Test
    public void testUpdateStatus() {
        SpecimenSynchronizationAdapter adapter = new SpecimenSynchronizationAdapter();
        Specimen newSpecimen = SpecimenHelper.INSTANCE.createSpecimen();
        // if the current status is UNDER_REVIEW and the provided status is AVAILABLE, then the status remains as
        // UNDER_REVIEW.
        newSpecimen.setStatus(SpecimenStatus.UNDER_REVIEW);
        adapter.updateSpecimenStatus(newSpecimen, SpecimenStatus.AVAILABLE);
        assertEquals(SpecimenStatus.UNDER_REVIEW, newSpecimen.getStatus());

        // if the current status is UNDER_REVIEW and the provided status is not AVAILABLE, change is allowed
        newSpecimen.setStatus(SpecimenStatus.UNDER_REVIEW);
        adapter.updateSpecimenStatus(newSpecimen, SpecimenStatus.UNAVAILABLE);
        assertEquals(SpecimenStatus.UNAVAILABLE, newSpecimen.getStatus());

        // if the current status is PENDING_SHIPMENT and the provided status is AVAILABLE, then the status remains as
        // PENDING_SHIPMENT.
        newSpecimen.setStatus(SpecimenStatus.PENDING_SHIPMENT);
        adapter.updateSpecimenStatus(newSpecimen, SpecimenStatus.AVAILABLE);
        assertEquals(SpecimenStatus.PENDING_SHIPMENT, newSpecimen.getStatus());

        // A Specimen's status should not be overwritten if the old status is shipped
        newSpecimen.setStatus(SpecimenStatus.SHIPPED);
        adapter.updateSpecimenStatus(newSpecimen, SpecimenStatus.DESTROYED);
        assertEquals(SpecimenStatus.SHIPPED, newSpecimen.getStatus());

        // A Specimen's status should not be overwritten if the old status is RETURNED
        newSpecimen.setStatus(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
        adapter.updateSpecimenStatus(newSpecimen, SpecimenStatus.AVAILABLE);
        assertEquals(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION, newSpecimen.getStatus());
        
        newSpecimen.setStatus(SpecimenStatus.RETURNED_TO_PARTICIPANT);
        adapter.updateSpecimenStatus(newSpecimen, SpecimenStatus.AVAILABLE);
        assertEquals(SpecimenStatus.RETURNED_TO_PARTICIPANT, newSpecimen.getStatus());

        // A Specimen's status should not be overwritten if the old status is DESTROYED
        newSpecimen.setStatus(SpecimenStatus.DESTROYED);
        adapter.updateSpecimenStatus(newSpecimen, SpecimenStatus.UNAVAILABLE);
        assertEquals(SpecimenStatus.DESTROYED, newSpecimen.getStatus());

    }

    /**
     * Test the validation of a new Specimen that does not currently exist in core.
     */
    @Test
    public void testValidateStatus() {
        SpecimenSynchronizationAdapter adapter = new SpecimenSynchronizationAdapter();
        for (SpecimenStatus currStatus : SpecimenStatus.values()) {
            // No Exception should be thrown for a Specimen with the status of available, unavailable, or destroyed
            if (SpecimenStatus.AVAILABLE.equals(currStatus)
                    || SpecimenStatus.UNAVAILABLE.equals(currStatus)
                    || SpecimenStatus.DESTROYED.equals(currStatus)) {
                adapter.validateStatus(currStatus);
            // An Exception should be thrown for any other value
            } else {
                try {
                    adapter.validateStatus(currStatus);
                    fail("IllegalArgumentException should have been thrown.");
                } catch (IllegalArgumentException iae) {
                    //Expected
                }
            }
        }
    }

}
