/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.dao;

import static org.junit.Assert.*;

import gov.nih.nci.cbm.domain.logicalmodel.Institution;
import gov.nih.nci.cbm.domain.logicalmodel.Organization;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

/**
 * @author smiller
 *
 */
public class InstitutionDaoTest extends AbstractCbmDaoTest {
    
    /**
     * test creating and deleting.
     * @throws Exception on error.
     */
    @Test
    public void testCreateAndDelete() throws Exception {
        Connection con = getService().getConnection();
        Statement s = con.createStatement();

        InstitutionDao dao = new InstitutionDao(con);
        dao.deleteAllInstitutions();
        verifyDeletion(s);
        
        Institution i = InstitutionDaoTest.getTestInstitution();
        dao.createOrRetrieveInstitution(i);
        
        verifyInstitution(s, i);
        verifyOrg(s, i);
        
        dao.createOrRetrieveInstitution(i);
        verifyInstitution(s, i);
        verifyOrg(s, i);
        
        dao.deleteAllInstitutions();
        verifyDeletion(s);
        s.close();
    }

    /**
     * verify.
     * @param s statement
     * @throws SQLException on error.
     */
    public static void verifyDeletion(Statement s) throws SQLException {
        ResultSet rs = s.executeQuery("select count(*) from Institution, Organization");
        rs.next();
        assertEquals(0, rs.getInt(1));
    }
    
    /**
     * verify.
     * @param s statement
     * @param i institution
     * @throws SQLException on error.
     */
    public static void verifyInstitution(Statement s, Institution i) throws SQLException {
        ResultSet rs = s.executeQuery("select homepageURL from Institution");
        assertTrue(rs.next());
        assertEquals(i.getHomepageURL(), rs.getString(1));
        assertFalse(rs.next());
        rs.close();
    }
    
    /**
     * verify.
     * @param s statement
     * @param i institution
     * @throws SQLException on error.
     */
    public static void verifyOrg(Statement s, Organization i) throws SQLException {
        ResultSet rs = s.executeQuery("select name from Organization");
        assertTrue(rs.next());
        assertEquals(i.getName(), rs.getString(1));
        assertFalse(rs.next());
        rs.close();
    }

    /**
     * get a test inst.
     * @return the inst.
     */
    public static Institution getTestInstitution() {
        Institution i = new Institution();
        i.setHomepageURL("http://testUrl.example.com/");
        i.setName("testName");
        i.setId(1L);
        return i;
    }

}
