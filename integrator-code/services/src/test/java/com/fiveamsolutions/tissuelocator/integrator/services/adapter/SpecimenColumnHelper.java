/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.services.adapter;

import java.util.HashMap;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.SpecimenColumn;

/**
 * @author gvaughn
 *
 */
public class SpecimenColumnHelper {

    private static final Map<SpecimenColumn, SpecimenValueRetriever> VALUE_RETRIEVER_MAP 
        = new HashMap<SpecimenColumn, SpecimenValueRetriever>();
    
    static {
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.EXERNAL_ID, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getExternalId();
            }           
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.AVAILABLE_QUANTITY, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getAvailableQuantity() != null ? specimen.getAvailableQuantity().toString()
                        : null;
            }           
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.AVAILABLE_QUANTITY_UNITS, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getAvailableQuantityUnits() != null ? specimen.getAvailableQuantityUnits().name()
                        : null;
            }           
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.PATHOLOGICAL_CHARACTERISTIC, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getPathologicalCharacteristic() != null 
                    ? specimen.getPathologicalCharacteristic().getName()
                    : null;
            }         
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.COLLECTION_YEAR, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getCollectionYear() != null ? Integer.toString(specimen.getCollectionYear())
                        : null;
            }         
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.PATIENT_AGE_AT_COLLECTION, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return Integer.toString(specimen.getPatientAgeAtCollection());
            }         
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.PATIENT_AGE_AT_COLLECTION_UNITS, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getPatientAgeAtCollectionUnits() != null 
                    ? specimen.getPatientAgeAtCollectionUnits().name()
                    : null;
            }         
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.SPECIMEN_TYPE, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getSpecimenType() != null ? specimen.getSpecimenType().getName()
                        : null;
            }         
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.MINIMUM_PRICE, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getMinimumPrice() != null ? specimen.getMinimumPrice().toString()
                        : null;
            }         
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.MAXIMUM_PRICE, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getMaximumPrice() != null ? specimen.getMaximumPrice().toString()
                        : null;
            }        
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.STATUS, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getStatus() != null ? specimen.getStatus().name()
                        : null;
            }        
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.PARTICIPANT_ID, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getParticipant() != null ? specimen.getParticipant().getExternalId()
                        : null;
            }        
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.GENDER, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return (specimen.getParticipant() != null && specimen.getParticipant().getGender() != null)
                    ? specimen.getParticipant().getGender().name() : null;
            }       
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.ETHNICITY, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return (specimen.getParticipant() != null && specimen.getParticipant().getEthnicity() != null)
                    ? specimen.getParticipant().getEthnicity().name() : null;
            }       
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.WHITE, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return getRace(specimen, Race.WHITE);
            }       
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.BLACK, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return getRace(specimen, Race.BLACK_OR_AFRICAN_AMERICAN);
            }       
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.ASIAN, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return getRace(specimen, Race.ASIAN);
            }       
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.NATIVE_AMERICAN, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return getRace(specimen, Race.NATIVE_AMERICAN);
            }       
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.ISLANDER, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return getRace(specimen, Race.ISLANDER);
            }       
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.RACE_NOT_REPORTED, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return getRace(specimen, Race.NOT_REPORTED);
            }       
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.RACE_UNKNOWN, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return getRace(specimen, Race.UNKNOWN);
            }       
        });
        VALUE_RETRIEVER_MAP.put(SpecimenColumn.COLLECTION_PROTOCOL, new SpecimenValueRetriever() {
            @Override
            public String getValue(Specimen specimen) {
                return specimen.getProtocol() != null ? specimen.getProtocol().getName()
                        : null;
            }       
        });
    }
    
    /**
     * Interface for retrieving specimen values as a string.
     * @author gvaughn
     *
     */
    static interface SpecimenValueRetriever {
        String getValue(Specimen specimen);
    }
    
    /**
     * Returns the specimen value corresponding to the given column, as a string.
     * @param specimen Specimen from which the value will be retrieved.
     * @param column Column for which the value will be retrieved.
     * @return The specimen value corresponding to the given column, as a string.
     */
    public static String getValue(Specimen specimen, SpecimenColumn column) {
        return VALUE_RETRIEVER_MAP.get(column).getValue(specimen);
    }
    
    private static String getRace(Specimen specimen, Race race) {
        return (specimen.getParticipant() != null && specimen.getParticipant().getRaces() != null)
        ? Boolean.toString(specimen.getParticipant().getRaces().contains(race)) 
        : Boolean.FALSE.toString();
    }
}
