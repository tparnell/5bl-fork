/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services.external;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.net.ftp.FTPFile;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.FtpFileImportHelper;
import com.fiveamsolutions.tissuelocator.integrator.services.adapter.SpecimenValidator;
import com.fiveamsolutions.tissuelocator.integrator.test.TestProperties;

/**
 * This external test is only executed by a project using the profiles.ci-external.xml.  Currently, you must setup
 *  an FTP server and place the file specimen.xml under the root directory.  This test currently only tests the
 *  connection to the ftp server and file download.  The file should be moved to the processed directory on the server
 *  and renamed to reflect the time it was processed.
 *  File: services/src/test/resources/specimen.xml
 *  Todo: ABRC-1372: Determine how to test FTP server from end to end.
 * @author bpickeral
 *  
 */
public class FtpFileImportHelperIntegrationTest {
    
    private FileImportIntegrationConfig getTestConfig() {
        FileImportIntegrationConfig config = new FileImportIntegrationConfig();
        config.setUsername(TestProperties.getFTPUser());
        config.setPassword(TestProperties.getFTPPassword());
        config.setFtpSite(TestProperties.getFTPSite());
        config.setRootDirectory(TestProperties.getFTPRootDirectory());
        return config;
    }
    
    /**
     * Test the connection.
     * @throws IOException 
     */
    @Test
    public void testValidConnect() throws IOException {
        FileImportIntegrationConfig config = getTestConfig();
        FtpFileImportHelper ftpHelper = new FtpFileImportHelper(config);
        assertTrue(ftpHelper.connect());
        String[] files = ftpHelper.getClient().listNames();
        assertTrue(Arrays.asList(files).contains("processed"));
    }
    
    /**
     * Test the connection.
     * @throws IOException 
     */
    @Test
    public void testInValidConnect() throws IOException {
        FileImportIntegrationConfig config = getTestConfig();
        config.setFtpSite("badurl.example.com");
        FtpFileImportHelper ftpHelper = new FtpFileImportHelper(config);
        assertFalse(ftpHelper.connect());
    }
    
    /**
     * Test the connection.
     * @throws IOException 
     */
    @Test
    public void testInValiPassword() throws IOException {
        FileImportIntegrationConfig config = getTestConfig();
        config.setPassword("invalidpassword");
        FtpFileImportHelper ftpHelper = new FtpFileImportHelper(config);
        assertFalse(ftpHelper.connect());
    }
    
    /**
     * Test Downloading and processing a file from the ftp.
     * @throws Exception on error
     */
    @Test
    public void downloadFilesTest() throws Exception {
        String uploadedFileName = null;
        try {
            uploadTestFile();
            
            FileImportIntegrationConfig config = getTestConfig();
            FtpFileImportHelper ftpHelper = new FtpFileImportHelper(config);
            if (!ftpHelper.connect()) {
                fail("Could not connect to ftp");
            }
            
            List<FileImportBatch> downloadedFiles = ftpHelper.createImportBatches();
            assertEquals(1, downloadedFiles.size());
            FileImportBatch importBatch = downloadedFiles.get(0);
            uploadedFileName = importBatch.getFileName();
            assertTrue(uploadedFileName.contains("specimen.xml"));
            
            SpecimenList specimenList = unmarshal(ftpHelper.downloadFile(importBatch));
            SpecimenValidator.INSTANCE.checkSpecimen(specimenList.getSpecimens().get(0));
            ftpHelper.disconnect();
        } finally {
            checkAndRemoveFiles(uploadedFileName);
        }
        
    }
    
    private SpecimenList unmarshal(InputStream s) throws JAXBException {
        Unmarshaller um = JAXBContext.newInstance(SpecimenList.class).createUnmarshaller();
        SpecimenList list = (SpecimenList) um.unmarshal(s);
        for (Specimen specimen : list.getSpecimens()) {
            specimen.setId(null);
        }
        return list;
    }

    
    private void uploadTestFile() throws Exception {
        FileImportIntegrationConfig config = getTestConfig();
        FtpFileImportHelper ftpHelper = new FtpFileImportHelper(config);
        if (!ftpHelper.connect()) {
            fail("Could not connect to ftp");
        }
        
        File file = new File(ClassLoader.getSystemResource("specimen.xml").toURI());
        FileInputStream inputStream = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        
        ftpHelper.getClient().enterLocalPassiveMode();
        OutputStream out = ftpHelper.getClient().storeFileStream("specimen.xml");
        String currLine;
        while ((currLine = reader.readLine()) != null) {
            out.write(currLine.getBytes());
        }
        out.flush();
        out.close();
        ftpHelper.disconnect();
    }
    
    private void checkAndRemoveFiles(String uploadedFile) throws Exception {
        FileImportIntegrationConfig config = getTestConfig();
        FtpFileImportHelper ftpHelper = new FtpFileImportHelper(config);
        ftpHelper.connect();
        assertTrue(ftpHelper.getClient().changeWorkingDirectory("processed"));
        ftpHelper.getClient().enterLocalPassiveMode();
        FTPFile[] ftpFiles = ftpHelper.getClient().listFiles();
        boolean processedFileExists = false;
        for (FTPFile ftpFile : ftpFiles) {
            if (!ftpFile.isDirectory() && uploadedFile.equals("processed/" + ftpFile.getName())) {
                processedFileExists = true;
                ftpHelper.getClient().deleteFile(ftpFile.getName());
            }
        }
        if (!processedFileExists) {
            fail("processed file does not exist");
        }
        ftpHelper.getClient().changeToParentDirectory();
        ftpHelper.getClient().removeDirectory("processed");
        ftpHelper.disconnect();
    }
}
