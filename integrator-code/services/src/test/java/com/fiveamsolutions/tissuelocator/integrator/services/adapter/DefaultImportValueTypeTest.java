/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.services.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.integrator.data.DefaultImportValueConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.DefaultImportValueType;

/**
 * @author gvaughn
 *
 */
public class DefaultImportValueTypeTest {

    /**
     * Tests the setValue method.
     * @throws Exception on error.
     */
    @Test
    public void testSetValue() throws Exception {
        DefaultImportValueConfig config = new DefaultImportValueConfig();
        config.setPropertyClassName(SpecimenType.class.getName());
        config.setPropertyPath("specimenType.name");
        config.setPropertyValue("tissue");
        
        Specimen specimen = new Specimen();
        
        DefaultImportValueType.OBJECT.setValue(specimen, config);
        assertNotNull(specimen.getSpecimenType());
        assertEquals("tissue", specimen.getSpecimenType().getName());
        
        specimen.setSpecimenType(new SpecimenType());
        DefaultImportValueType.OBJECT.setValue(specimen, config);
        assertNotNull(specimen.getSpecimenType());
        assertEquals("tissue", specimen.getSpecimenType().getName());
        
        specimen.getSpecimenType().setName("name");
        DefaultImportValueType.OBJECT.setValue(specimen, config);
        assertNotNull(specimen.getSpecimenType());
        assertEquals("name", specimen.getSpecimenType().getName());
        
        config = new DefaultImportValueConfig();
        config.setPropertyPath("availableQuantityUnits");
        config.setPropertyValue("COUNT");       
        specimen = new Specimen();
        
        DefaultImportValueType.ENUM.setValue(specimen, config);
        assertNotNull(specimen.getAvailableQuantityUnits());
        assertEquals(QuantityUnits.COUNT, specimen.getAvailableQuantityUnits());
        
        specimen.setAvailableQuantityUnits(QuantityUnits.G);
        DefaultImportValueType.ENUM.setValue(specimen, config);
        assertNotNull(specimen.getAvailableQuantityUnits());
        assertEquals(QuantityUnits.G, specimen.getAvailableQuantityUnits());
        
        config = new DefaultImportValueConfig();
        config.setPropertyPath("patientAgeAtCollection");
        config.setPropertyValue("33");       
        specimen = new Specimen();
        
        DefaultImportValueType.PRIMITIVE.setValue(specimen, config);
        assertTrue(Integer.parseInt("33") == specimen.getPatientAgeAtCollection());
        
        specimen.setPatientAgeAtCollection(1);
        DefaultImportValueType.PRIMITIVE.setValue(specimen, config);
        assertTrue(1 == specimen.getPatientAgeAtCollection());
        
        config.setPropertyPath("externalId");
        config.setPropertyValue("test id");       
        specimen = new Specimen();
        DefaultImportValueType.PRIMITIVE.setValue(specimen, config);
        assertEquals("test id", specimen.getExternalId());
        
        specimen.setExternalId("");
        DefaultImportValueType.PRIMITIVE.setValue(specimen, config);
        assertEquals("test id", specimen.getExternalId());
        
        specimen.setExternalId("new id");
        DefaultImportValueType.PRIMITIVE.setValue(specimen, config);
        assertEquals("new id", specimen.getExternalId());
        
        config.setPropertyPath("id");
        config.setPropertyValue("20");       
        specimen = new Specimen();
        DefaultImportValueType.PRIMITIVE.setValue(specimen, config);
        assertEquals(new Long("20"), specimen.getId());
        
        specimen.setId(0L);
        DefaultImportValueType.PRIMITIVE.setValue(specimen, config);
        assertEquals(new Long("20"), specimen.getId());
        
        specimen.setId(1L);
        DefaultImportValueType.PRIMITIVE.setValue(specimen, config);
        assertEquals(new Long("1"), specimen.getId());
    }
}
