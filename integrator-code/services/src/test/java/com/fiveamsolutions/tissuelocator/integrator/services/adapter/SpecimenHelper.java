/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services.adapter;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Contact;
import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;


/**
 * @author bpickeral
 *
 */
public final class SpecimenHelper {

    private static final int YEAR = 2010;

    /**
     * Instance of this class used for testing.
     */
    public static final SpecimenHelper INSTANCE = new SpecimenHelper();

    /** external id. */
    public static final String EXTERNAL_ID = "test external id";
    /** external id. */
    public static final String NAME = "test name";
    /** restrictions. */
    public static final String RESTRICTIONS = "restrictions";
    /** home page. */
    public static final String HOMEPAGE = "http://www.google.com";
    /** contact first name. */
    public static final String CONTACT_FIRST = "first";
    /** contact last name. */
    public static final String CONTACT_LAST = "last";
    /** contact email. */
    public static final String CONTACT_EMAIL = "email@email.com";
    /** contact phone number. */
    public static final String CONTACT_PHONE = "301-555-5555";
    /** payment instructions. */
    public static final String PAYMENT_INST = "payment inst";
    /** quantity. */
    public static final BigDecimal QUANTITY = new BigDecimal("1.0");
    /** age. */
    public static final int AGE = 30;
    /** minimum price. */
    public static final BigDecimal MIN_PRICE = new BigDecimal("12.34");
    /** maximum price. */
    public static final BigDecimal MAX_PRICE = new BigDecimal("56.78");
    /** Simple Date Format. */
    public static final SimpleDateFormat FORMAT = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
    /** date. */
    public static final String DATE = "01/01/1970";
    /** collection type. */
    public static final String COLLECTION_TYPE = "Autopsy";
    /** anatomic source. */
    public static final String ANATOMIC_SOURCE = "Gallbladder";
    /** preservation type. */
    public static final String PRESERVATION_TYPE = "Fixed Formalin";
    /** time lapse to processing. */
    public static final Integer TIME_LAPSE_TO_PROCESSING = Integer.valueOf(10);
    /** specimen density. */
    public static final Integer SPECIMEN_DENSITY = Integer.valueOf(35);
    /** storage temperature. */
    public static final Integer STORAGE_TEMPERATURE = Integer.valueOf(-70);

    /**
     * create a specimen.
     * @return a new specimen
     */
    public Specimen createSpecimen() {
        Participant participant = createParticipant();
        AdditionalPathologicFinding apf = createCode(AdditionalPathologicFinding.class);
        SpecimenType st = createCode(SpecimenType.class);
        Specimen specimen = new Specimen();
        specimen.setExternalId(EXTERNAL_ID);
        specimen.setPatientAgeAtCollection(AGE);
        specimen.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        specimen.setAvailableQuantity(QUANTITY);
        specimen.setAvailableQuantityUnits(QuantityUnits.MG);
        specimen.setPathologicalCharacteristic(apf);
        specimen.setSpecimenType(st);
        specimen.setParticipant(participant);
        specimen.setProtocol(createProtocol());
        specimen.setCollectionYear(YEAR);
        specimen.setMinimumPrice(MIN_PRICE);
        specimen.setMaximumPrice(MAX_PRICE);
        specimen.setPriceNegotiable(false);
        specimen.setStatus(SpecimenStatus.UNAVAILABLE);
        specimen.setPreviousStatus(SpecimenStatus.AVAILABLE);
        specimen.setCustomProperties(createCustomProperties());
        Institution i = new Institution();
        i.setId(1L);
        specimen.setExternalIdAssigner(i);

        return specimen;
    }

    /**
     * create a participant.
     * @return a new participant
     */
    public Participant createParticipant() {
        Participant participant = new Participant();
        participant.setEthnicity(Ethnicity.HISPANIC_OR_LATINO);
        participant.setRaces(new ArrayList<Race>());
        participant.getRaces().add(Race.BLACK_OR_AFRICAN_AMERICAN);
        participant.setExternalId(EXTERNAL_ID);
        participant.setGender(Gender.FEMALE);
        return participant;
    }

    private Contact createContact() {
        Contact contact = new Contact();
        contact.setFirstName(CONTACT_FIRST);
        contact.setLastName(CONTACT_LAST);
        contact.setEmail(CONTACT_EMAIL);
        contact.setPhone(CONTACT_PHONE);
        return contact;
    }

    private <T extends Code> T createCode(Class<T> codeType) {
        try {
            T code = codeType.newInstance();
            code.setName(codeType.getSimpleName() + "-active");
            code.setActive(true);
            if (codeType.isAssignableFrom(SpecimenType.class)) {
                ((SpecimenType) code).setSpecimenClass(SpecimenClass.TISSUE);
            }
            return code;
        } catch (IllegalAccessException e)  {
            return null;
        } catch (InstantiationException e) {
            return null;
        }
    }

    /**
     * create a collection protocol.
     * @return a new collection protocol
     */
    public CollectionProtocol createProtocol() {
        CollectionProtocol protocol = new CollectionProtocol();
        try {
            protocol.setEndDate(FORMAT.parse(DATE));
            protocol.setStartDate(FORMAT.parse(DATE));
        } catch (ParseException pe) {
            fail();
        }
        protocol.setName(NAME);
        protocol.setContact(createContact());
        return protocol;
    }

    /**
     * @return a map of custom properties
     */
    public Map<String, Object> createCustomProperties() {
        Map<String, Object> customProperties = new HashMap<String, Object>();
        customProperties.put("collectionType", COLLECTION_TYPE);
        customProperties.put("anatomicSource", ANATOMIC_SOURCE);
        customProperties.put("preservationType", PRESERVATION_TYPE);
        customProperties.put("timeLapseToProcessing", TIME_LAPSE_TO_PROCESSING);
        customProperties.put("specimenDensity", SPECIMEN_DENSITY);
        customProperties.put("storageTemperature", STORAGE_TEMPERATURE);
        return customProperties;
    }

}