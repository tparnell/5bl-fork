/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.integrator.data.CaTissueIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.ImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.Translation;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;
import com.fiveamsolutions.tissuelocator.integrator.service.CaTissueSpecimenServiceImpl;
import com.fiveamsolutions.tissuelocator.integrator.service.ImportBatchServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.ProcessSpecimens;
import com.fiveamsolutions.tissuelocator.integrator.service.ProcessSpecimensResponse;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author ddasgupta
 */
public class CaTissueSpecimenServiceTest extends AbstractHibernateTestCase {

    private static final Long ID = 171L;

    /**
     * Test caTissue specimen service.
     * @throws Exception on error
     */
    @Test
    public void testProcessSpecimens() throws Exception {
        setupTranslations();
        Specimen s = new Specimen();
        s.setId(ID);
        s.setSpecimenType(new SpecimenType());
        s.getSpecimenType().setSpecimenClass(SpecimenClass.CELLS);
        List<Specimen> specimens = new ArrayList<Specimen>();
        specimens.add(s);
        SpecimenList list = new SpecimenList();
        list.setSpecimens(specimens);

        ProcessSpecimens ps = new ProcessSpecimens();
        ps.setSpecimens(list);
        assertEquals(list, ps.getSpecimens());
        ps.setLocationId(ID);
        assertEquals(ID, ps.getLocationId());
        ProcessSpecimensResponse psr = new ProcessSpecimensResponse();
        ImportBatch ib = new ImportBatch();
        psr.setProcessSpecimensResult(ib);
        assertEquals(ib, psr.getProcessSpecimensResult());

        s.setId(0L);
        ib = new CaTissueSpecimenServiceImpl().processSpecimens(list, ID);
        testImportBatch(ib);
        assertNull(s.getExternalId());
        assertNull(s.getPathologicalCharacteristic());
        assertNotNull(s.getExternalIdAssigner());
        assertNull(s.getProtocol());
        assertNull(s.getParticipant());

        s.setId(1L);
        s.setSpecimenType(new SpecimenType());
        s.getSpecimenType().setSpecimenClass(SpecimenClass.CELLS);
        ib = new CaTissueSpecimenServiceImpl().processSpecimens(list, ID);
        testImportBatch(ib);
        assertNotNull(s.getExternalId());
        assertEquals("barcode", s.getExternalId());
        assertNotNull(s.getExternalIdAssigner());
        assertNotNull(s.getExternalIdAssigner().getId());
        assertEquals(ID, s.getExternalIdAssigner().getId());
        assertEquals(SpecimenStatus.AVAILABLE, s.getStatus());
        assertEquals("frozen tissue", s.getSpecimenType().getName());
        assertNotNull(s.getAvailableQuantity());
        assertEquals(QuantityUnits.CELLS, s.getAvailableQuantityUnits());
        assertNotNull(s.getPathologicalCharacteristic());
        assertEquals("disease", s.getPathologicalCharacteristic().getName());
        assertNotNull(s.getProtocol());
        assertEquals("protocol", s.getProtocol().getName());
        assertNotNull(s.getProtocol().getStartDate());
        assertNotNull(s.getProtocol().getEndDate());
        assertNotNull(s.getProtocol().getInstitution());
        assertNotNull(s.getProtocol().getInstitution().getId());
        assertEquals(ID, s.getProtocol().getInstitution().getId());
        assertNotNull(s.getParticipant());
        assertEquals(Gender.MALE, s.getParticipant().getGender());
        assertEquals(Ethnicity.HISPANIC_OR_LATINO, s.getParticipant().getEthnicity());
        assertEquals("MRN", s.getParticipant().getExternalId());
        assertNotNull(s.getParticipant().getRaces());
        assertTrue(s.getParticipant().getRaces().contains(Race.ASIAN));
        assertTrue(s.getParticipant().getRaces().contains(Race.BLACK_OR_AFRICAN_AMERICAN));
        assertTrue(s.getParticipant().getRaces().contains(Race.WHITE));
        assertNotNull(s.getParticipant().getExternalIdAssigner());
        assertNotNull(s.getParticipant().getExternalIdAssigner().getId());
        assertEquals(ID, s.getParticipant().getExternalIdAssigner().getId());
        assertEquals(0, s.getPatientAgeAtCollection());
        assertEquals(TimeUnits.MONTHS, s.getPatientAgeAtCollectionUnits());

        s.setId(2L);
        s.setSpecimenType(new SpecimenType());
        s.getSpecimenType().setSpecimenClass(SpecimenClass.CELLS);
        ib = new CaTissueSpecimenServiceImpl().processSpecimens(list, ID);
        testImportBatch(ib);
        testSpecimen(s);
        assertEquals(1, s.getPatientAgeAtCollection());
        assertEquals(TimeUnits.YEARS, s.getPatientAgeAtCollectionUnits());

        s.setId(ID);
        s.setSpecimenType(new SpecimenType());
        s.getSpecimenType().setSpecimenClass(SpecimenClass.CELLS);
        ib = new CaTissueSpecimenServiceImpl().processSpecimens(list, ID);
        testImportBatch(ib);
        testSpecimen(s);
        assertEquals(2, s.getPatientAgeAtCollection());
        assertEquals(TimeUnits.YEARS, s.getPatientAgeAtCollectionUnits());

        testNotification();
    }

    private void setupTranslations() {
        TissueBankIntegrationConfigServiceLocal service =
            TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
        CaTissueIntegrationConfig config = new CaTissueIntegrationConfig();
        config.setGridServiceUrl("http://localhost:8081/wsrf/services/cagrid/CaTissueSuite");
        config.setInstitutionId(ID);
        config.setStartTime(new Date());
        config.setCodeSets(new ArrayList<String>());
        config.getCodeSets().add("caTissue Base");
        config.setNotificationEmail(TissueBankIntegrationHelper.NOTIFICATION_EMAIL);
        config.setNotificationInterval(0);
        service.savePersistentObject(config);

        TranslationServiceLocal translationService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Translation t = new Translation();
        t.setSourceValue("Male Gender");
        t.setDestinationValue(Gender.MALE.name());
        t.setCodeType(Gender.class.getSimpleName());
        t.setCodeSetName("caTissue Base");
        translationService.savePersistentObject(t);

        t = new Translation();
        t.setSourceValue("Hispanic or Latino");
        t.setDestinationValue(Ethnicity.HISPANIC_OR_LATINO.name());
        t.setCodeType(Ethnicity.class.getSimpleName());
        t.setCodeSetName("caTissue Base");
        translationService.savePersistentObject(t);

        t = new Translation();
        t.setSourceValue("Asian");
        t.setDestinationValue(Race.ASIAN.name());
        t.setCodeType(Race.class.getSimpleName());
        t.setCodeSetName("caTissue Base");
        translationService.savePersistentObject(t);
        t = new Translation();
        t.setSourceValue("Black or African American");
        t.setDestinationValue(Race.BLACK_OR_AFRICAN_AMERICAN.name());
        t.setCodeType(Race.class.getSimpleName());
        t.setCodeSetName("caTissue Base");
        translationService.savePersistentObject(t);
        t = new Translation();
        t.setSourceValue("White");
        t.setDestinationValue(Race.WHITE.name());
        t.setCodeType(Race.class.getSimpleName());
        t.setCodeSetName("caTissue Base");
        translationService.savePersistentObject(t);
    }

    private void testImportBatch(ImportBatch ib) {
        assertNotNull(ib);
        assertNotNull(ib.getConfig());
        assertNotNull(ib.getStartTime());
        assertNotNull(ib.getEndTime());
        assertNotNull(ib.getImports());
        assertEquals(1, ib.getImports().intValue());
        assertNotNull(ib.getFailures());
        assertEquals(0, ib.getFailures().intValue());
    }

    private void testSpecimen(Specimen s) {
        assertNotNull(s.getExternalId());
        assertEquals("barcode", s.getExternalId());
        assertEquals(SpecimenStatus.UNAVAILABLE, s.getStatus());
        assertEquals("frozen tissue", s.getSpecimenType().getName());
        assertNotNull(s.getAvailableQuantity());
        assertEquals(QuantityUnits.CELLS, s.getAvailableQuantityUnits());
        assertNotNull(s.getPathologicalCharacteristic());
        assertEquals("disease", s.getPathologicalCharacteristic().getName());
        assertNotNull(s.getExternalIdAssigner());
        assertNotNull(s.getExternalIdAssigner().getId());
        assertEquals(ID, s.getExternalIdAssigner().getId());
        assertNotNull(s.getCollectionYear());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);
        assertEquals(cal.get(Calendar.YEAR), s.getCollectionYear().intValue());
        assertNotNull(s.getProtocol());
        assertEquals("protocol", s.getProtocol().getName());
        assertNotNull(s.getProtocol().getStartDate());
        assertNotNull(s.getProtocol().getEndDate());
        assertNotNull(s.getProtocol().getInstitution());
        assertNotNull(s.getProtocol().getInstitution().getId());
        assertEquals(ID, s.getProtocol().getInstitution().getId());
        assertNotNull(s.getParticipant());
        assertEquals(Gender.MALE, s.getParticipant().getGender());
        assertEquals(Ethnicity.HISPANIC_OR_LATINO, s.getParticipant().getEthnicity());
        assertEquals("MRN", s.getParticipant().getExternalId());
        assertNotNull(s.getParticipant().getRaces());
        assertTrue(s.getParticipant().getRaces().contains(Race.ASIAN));
        assertTrue(s.getParticipant().getRaces().contains(Race.BLACK_OR_AFRICAN_AMERICAN));
        assertTrue(s.getParticipant().getRaces().contains(Race.WHITE));
        assertNotNull(s.getParticipant().getExternalIdAssigner());
        assertNotNull(s.getParticipant().getExternalIdAssigner().getId());
        assertEquals(ID, s.getParticipant().getExternalIdAssigner().getId());
    }

    private void testNotification() throws Exception {
        ImportBatchServiceLocal batchService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getImportBatchService();
        int numBatches = batchService.getAll(ImportBatch.class).size();
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        new CaTissueSpecimenServiceImpl().processSpecimens(new SpecimenList(), ID);
        assertEquals(1, Mailbox.get(TissueBankIntegrationHelper.NOTIFICATION_EMAIL).size());
        testEmail(TissueBankIntegrationHelper.NOTIFICATION_EMAIL, "Specimen imports have stopped occurring",
                "It has been 0 days since the last Specimen import for institution 171.",
                "Please look into why the imports have stopped.");
        assertEquals(numBatches, batchService.getAll(ImportBatch.class).size());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * test the error notification.
     * @throws IOException on error
     * @throws MessagingException on error
     */
    @Test
    public void testError() throws MessagingException, IOException {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        setupTranslations();
        Specimen s = new Specimen();
        s.setId(-1L);
        s.setSpecimenType(new SpecimenType());
        s.getSpecimenType().setSpecimenClass(SpecimenClass.CELLS);
        s.setStatus(SpecimenStatus.PENDING_SHIPMENT);
        SpecimenList list = new SpecimenList();
        list.getSpecimens().add(s);

        ImportBatch ib = new CaTissueSpecimenServiceImpl().processSpecimens(list, ID);
        assertEquals(1, ib.getImports().intValue());
        assertEquals(1, ib.getFailures().intValue());
        assertFalse(Mailbox.get(TissueBankIntegrationHelper.NOTIFICATION_EMAIL).isEmpty());
        assertEquals(2, Mailbox.get(TissueBankIntegrationHelper.NOTIFICATION_EMAIL).size());
        testEmail(TissueBankIntegrationHelper.NOTIFICATION_EMAIL, "An Error occurred during Specimen synchronization",
                "with external id null and institution id 171",
                "The following error occurred during Specimen synchronization",
                "Please contact the System administrator.");
        testEmail(TissueBankIntegrationHelper.NOTIFICATION_EMAIL, "Specimen imports have stopped occurring",
                "It has been 0 days since the last Specimen import for institution 171.",
                "Please look into why the imports have stopped.");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

}
