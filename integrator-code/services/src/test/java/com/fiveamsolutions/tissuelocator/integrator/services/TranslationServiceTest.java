/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.CaTissueIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.Translation;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.SpecimenColumn;
import com.fiveamsolutions.tissuelocator.integrator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author ddasgupta
 *
 */
public class TranslationServiceTest extends AbstractHibernateTestCase {

    private static final String NOTIFICATION_EMAIL = "notification@email.com";
    private static final int CODE_SET_COUNT = 4;

    /**
     * Test the get code method of the translation service.
     * @throws IOException on error
     * @throws MessagingException on error
     */
    @Test
    public void testGetCode() throws MessagingException, IOException {
        MailUtils.setMailEnabled(true);
        setupConfig();
        setupTranslations();
        TranslationServiceLocal service = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = service.getTranslationMap(1L);
        assertNull(service.getCode(null, 1L, SpecimenType.class, translationMap));
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        assertNull(service.getCode("", 1L, SpecimenType.class, translationMap));
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        SpecimenType pt = service.getCode("frozen in dry ice", 1L, SpecimenType.class, translationMap);
        assertNotNull(pt);
        assertEquals("CO2 frozen", pt.getName());
        assertEquals(SpecimenType.class, pt.getClass());
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        pt = service.getCode("FROZEN IN DRY ICE", 1L, SpecimenType.class, translationMap);
        assertNotNull(pt);
        assertEquals("CO2 frozen", pt.getName());
        assertEquals(SpecimenType.class, pt.getClass());
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        pt = service.getCode("no translation needed", 1L, SpecimenType.class, translationMap);
        assertNotNull(pt);
        assertEquals("no translation needed", pt.getName());
        assertEquals(SpecimenType.class, pt.getClass());
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        assertNull(service.getCode("untranslated", 1L, SpecimenType.class, translationMap));
        assertFalse(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        assertEquals(1, Mailbox.get(NOTIFICATION_EMAIL).size());
        testEmail(NOTIFICATION_EMAIL, "An unmapped client code was encountered during Specimen synchronization",
                "could not be mapped", "untranslated", "invalid", "1", "SpecimenType", "custom",
                "caTissue default");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Test the get enum method of the translation service.
     * @throws IOException on error
     * @throws MessagingException on error
     */
    @Test
    public void testGetEnum() throws MessagingException, IOException {
        MailUtils.setMailEnabled(true);
        setupConfig();
        setupTranslations();
        TranslationServiceLocal service = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = service.getTranslationMap(1L);
        assertNull(service.getEnum(null, 1L, Gender.class, translationMap));
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        assertNull(service.getEnum("", 1L, Gender.class, translationMap));
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        Gender g = service.getEnum("Male Gender", 1L, Gender.class, translationMap);
        assertNotNull(g);
        assertEquals(Gender.MALE, g);
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        g = service.getEnum("MALE GENDER", 1L, Gender.class, translationMap);
        assertNotNull(g);
        assertEquals(Gender.MALE, g);
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        g = service.getEnum("MALE", 1L, Gender.class, translationMap);
        assertNotNull(g);
        assertEquals(Gender.MALE, g);
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        assertNull(service.getEnum("untranslated", 1L, Gender.class, translationMap));
        assertFalse(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        assertEquals(1, Mailbox.get(NOTIFICATION_EMAIL).size());
        testEmail(NOTIFICATION_EMAIL, "An unmapped client code was encountered during Specimen synchronization",
                "could not be mapped", "untranslated", "invalid", "1", "Gender", "custom", "caTissue default");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }
    
    /**
     * Test custom property translation.
     */
    @Test
    public void testGetCustomProperty() {
        AbstractTissueBankIntegrationConfig config = setupConfig();
        setupTranslations();
        TranslationServiceLocal service = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = service.getTranslationMap(1L);
        assertEquals("Pneumocyte", service.getCustomProperty("Lung cell", config, "anatomicSource", translationMap));
        assertEquals("Pneumocyte", service.getCustomProperty("lung Cell", config, "anatomicSource", translationMap));
        assertEquals("no translation", service.getCustomProperty("no translation", config, 
                "anatomicSource", translationMap));
        assertEquals("Lung cell", service.getCustomProperty("Lung cell", config, "noTranslation", translationMap));
    }
    
    /**
     * Test column header translation.
     */
    @Test
    public void testGetSpecimenColumn() {
        AbstractTissueBankIntegrationConfig config = setupConfig();
        setupTranslations();
        TranslationServiceLocal service = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = service.getTranslationMap(1L);
        assertEquals(SpecimenColumn.PATHOLOGICAL_CHARACTERISTIC.getColumnHeader(), 
                service.getSpecimenColumn("clientDisease", 
                config.getInstitutionId(), translationMap));
        assertEquals(SpecimenColumn.PATHOLOGICAL_CHARACTERISTIC.getColumnHeader(), 
                service.getSpecimenColumn("Clientdisease", 
                config.getInstitutionId(), translationMap));
        assertEquals("no translation", service.getSpecimenColumn("no translation", 
                config.getInstitutionId(), translationMap));
        assertEquals(SpecimenColumn.ETHNICITY.getColumnHeader(), 
                service.getSpecimenColumn(SpecimenColumn.ETHNICITY.getColumnHeader(), 
                config.getInstitutionId(), translationMap));
    }

    private AbstractTissueBankIntegrationConfig setupConfig() {
        CaTissueIntegrationConfig config = new CaTissueIntegrationConfig();
        config.setInstitutionId(1L);
        config.setStartTime(new Date());
        config.setGridServiceUrl("http://caTissue.com");
        List<String> codeSets = new ArrayList<String>();
        codeSets.add("custom");
        codeSets.add("custom properties");
        codeSets.add("caTissue default");
        codeSets.add("column test");
        codeSets.add("empty");
        config.setCodeSets(codeSets);
        config.setNotificationEmail(NOTIFICATION_EMAIL);
        config.setNotificationInterval(1);
        TissueBankIntegrationConfigServiceLocal configService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
        Long id = configService.savePersistentObject(config);
        config = configService.getPersistentObject(CaTissueIntegrationConfig.class, id);
        assertNotNull(config.getCodeSets());
        assertFalse(config.getCodeSets().isEmpty());
        assertEquals(CODE_SET_COUNT + 1, config.getCodeSets().size());
        assertFalse(StringUtils.isBlank(config.getNotificationEmail()));
        assertEquals(NOTIFICATION_EMAIL, config.getNotificationEmail());
        assertEquals(1, config.getNotificationInterval());
        return config;
    }

    private void setupTranslations() {
        TranslationServiceLocal service = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Translation t = new Translation();
        t.setSourceValue("frozen in dry ice");
        t.setDestinationValue("CO2 frozen");
        t.setCodeSetName("caTissue default");
        t.setCodeType(SpecimenType.class.getSimpleName());
        Long id = service.savePersistentObject(t);
        t = service.getPersistentObject(Translation.class, id);
        assertEquals("frozen in dry ice", t.getSourceValue());
        assertEquals("CO2 frozen", t.getDestinationValue());
        assertEquals("caTissue default", t.getCodeSetName());
        assertEquals(SpecimenType.class.getSimpleName(), t.getCodeType());

        t = new Translation();
        t.setSourceValue("Male Gender");
        t.setDestinationValue(Gender.MALE.name());
        t.setCodeSetName("custom");
        t.setCodeType(Gender.class.getSimpleName());
        id = service.savePersistentObject(t);
        t = service.getPersistentObject(Translation.class, id);
        assertEquals("Male Gender", t.getSourceValue());
        assertEquals(Gender.MALE.name(), t.getDestinationValue());
        assertEquals("custom", t.getCodeSetName());
        assertEquals(Gender.class.getSimpleName(), t.getCodeType());
        
        t = new Translation();
        t.setSourceValue("Lung cell");
        t.setDestinationValue("Pneumocyte");
        t.setCodeSetName("custom properties");
        t.setCodeType("anatomicSource");
        id = service.savePersistentObject(t);
        t = service.getPersistentObject(Translation.class, id);
        assertEquals("Lung cell", t.getSourceValue());
        assertEquals("Pneumocyte", t.getDestinationValue());
        assertEquals("custom properties", t.getCodeSetName());
        assertEquals("anatomicSource", t.getCodeType());

        t = new Translation();
        t.setSourceValue("untranslated");
        t.setDestinationValue("invalid");
        t.setCodeSetName("custom");
        t.setCodeType(SpecimenType.class.getSimpleName());
        id = service.savePersistentObject(t);
        t = service.getPersistentObject(Translation.class, id);
        assertEquals("untranslated", t.getSourceValue());
        assertEquals("invalid", t.getDestinationValue());
        assertEquals("custom", t.getCodeSetName());
        assertEquals(SpecimenType.class.getSimpleName(), t.getCodeType());

        t = new Translation();
        t.setSourceValue("untranslated");
        t.setDestinationValue("invalid");
        t.setCodeSetName("caTissue default");
        t.setCodeType(Gender.class.getSimpleName());
        id = service.savePersistentObject(t);
        t = service.getPersistentObject(Translation.class, id);
        assertEquals("untranslated", t.getSourceValue());
        assertEquals("invalid", t.getDestinationValue());
        assertEquals("caTissue default", t.getCodeSetName());
        assertEquals(Gender.class.getSimpleName(), t.getCodeType());
        
        t = new Translation();
        t.setSourceValue("clientDisease");
        t.setDestinationValue(SpecimenColumn.PATHOLOGICAL_CHARACTERISTIC.getColumnHeader());
        t.setCodeSetName("column test");
        t.setCodeType(SpecimenColumn.class.getSimpleName());
        id = service.savePersistentObject(t);
        t = service.getPersistentObject(Translation.class, id);
        assertEquals("clientDisease", t.getSourceValue());
        assertEquals(SpecimenColumn.PATHOLOGICAL_CHARACTERISTIC.getColumnHeader(), t.getDestinationValue());
        assertEquals("column test", t.getCodeSetName());
        assertEquals(SpecimenColumn.class.getSimpleName(), t.getCodeType());
    }

    /**
     * Test the get translation map method of the translation service.
     */
    @Test
    public void testGetTranslationMap() {
        MailUtils.setMailEnabled(true);
        setupConfig();
        setupTranslations();
        TranslationServiceLocal service = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = service.getTranslationMap(1L);
        assertNotNull(translationMap);
        assertEquals(CODE_SET_COUNT, translationMap.size());
        assertNull(translationMap.get("empty"));
        for (String codeSet : new String[] {"custom", "caTissue default"}) {
            assertNotNull(translationMap.get(codeSet));
            assertEquals(2, translationMap.get(codeSet).size());
            assertNull(translationMap.get(codeSet).get(Ethnicity.class.getSimpleName()));
            for (Class<?> clazz : new Class<?>[] {SpecimenType.class, Gender.class}) {
                assertNotNull(translationMap.get(codeSet).get(clazz.getSimpleName()));
                assertEquals(1, translationMap.get(codeSet).get(clazz.getSimpleName()).size());
            }
        }
        assertNotNull(translationMap.get("custom properties"));
        assertEquals(1, translationMap.get("custom properties").size());
    }

    /**
     * Test the get code method when there are no translations available.
     * @throws MessagingException on error
     */
    @Test
    public void testGetCodeNoTranslation() throws MessagingException {
        MailUtils.setMailEnabled(true);
        setupConfig();
        setupTranslations();
        TranslationServiceLocal service = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = service.getTranslationMap(1L);
        AdditionalPathologicFinding apf =
            service.getCode("no translation needed", 1L, AdditionalPathologicFinding.class, translationMap);
        assertNotNull(apf);
        assertEquals("no translation needed", apf.getName());
        assertEquals(AdditionalPathologicFinding.class, apf.getClass());
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Test the get enum method when there are no translations available.
     * @throws MessagingException on error
     */
    @Test
    public void testGetEnumNoTranslation() throws MessagingException {
        MailUtils.setMailEnabled(true);
        setupConfig();
        setupTranslations();
        TranslationServiceLocal service = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = service.getTranslationMap(1L);
        Ethnicity e  = service.getEnum(Ethnicity.UNKNOWN.name(), 1L, Ethnicity.class, translationMap);
        assertNotNull(e);
        assertEquals(Ethnicity.UNKNOWN, e);
        assertTrue(Mailbox.get(NOTIFICATION_EMAIL).isEmpty());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }
}
