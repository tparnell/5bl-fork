/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.dao;

import static org.junit.Assert.*;
import gov.nih.nci.cbm.domain.logicalmodel.Diagnosis;
import gov.nih.nci.cbm.domain.logicalmodel.ParticipantCollectionSummary;
import gov.nih.nci.cbm.domain.logicalmodel.Race;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import org.junit.Test;

/**
 * @author smiller
 *
 */
public class ParticipantCollectionSummaryDaoTest extends AbstractCbmDaoTest {
    
    private static final long PARTICIPANT_ID = 3L;
    private static final long RACE_ID_2 = 5L;
    private static final long DIAGNOSIS_ID = 10L;
    private static final int FIFTH_PARAM = 5;
    private static final int FOURTH_PARAM = 4;
    private static final int THIRD_PARAM = 3;
    
    /**
     * test creating and deleting.
     * @throws Exception on error.
     */
    @Test
    public void testCreateAndDelete() throws Exception {
        Connection con = getService().getConnection();
        Statement s = con.createStatement();

        ParticipantCollectionSummaryDao dao = new ParticipantCollectionSummaryDao(con);
        dao.deleteAllParticipants();
        ParticipantCollectionSummaryDaoTest.verifyDeletion(s);
        
        ParticipantCollectionSummary p = ParticipantCollectionSummaryDaoTest.getTestParticipant();
        dao.createOrIncrementParticipant(p);
        ParticipantCollectionSummaryDaoTest.verifyParticipant(s, p, 1);
        
        dao.createOrIncrementParticipant(p);
        ParticipantCollectionSummaryDaoTest.verifyParticipant(s, p, 2);
        
        dao.deleteAllParticipants();
        ParticipantCollectionSummaryDaoTest.verifyDeletion(s);
        
        p.getDiagnosis().clear();
        dao.createOrIncrementParticipant(p);
        dao.deleteAllParticipants();
        s.close();
    }
    
    /**
     * verify.
     * @param s statement
     * @param p participant
     * @param count the count
     * @throws SQLException on error.
     */
    public static void verifyParticipant(Statement s, ParticipantCollectionSummary p, int count) throws SQLException {
        CollectionProtocolDaoTest.verifyCollectionProtocol(s, p.getCollectionProtocol());
        
        ResultSet rs = s.executeQuery("select participantCollectionSummaryID, diagnosisId from "
                + "JoinParticipantCollectionSummaryToDiagnosis");
        assertTrue(rs.next());
        assertEquals(Long.valueOf(1), Long.valueOf(rs.getLong(1)));
        assertEquals(Long.valueOf(DIAGNOSIS_ID), Long.valueOf(rs.getLong(2)));
        assertFalse(rs.next());
        rs.close();
        
        rs = s.executeQuery("select participantCollectionSummaryID, raceid from "
                + "JoinParticipantCollectionSummaryToRace order by raceid");
        assertTrue(rs.next());
        assertEquals(Long.valueOf(1), Long.valueOf(rs.getLong(1)));
        assertEquals(Long.valueOf(1L), Long.valueOf(rs.getLong(2)));
        assertTrue(rs.next());
        assertEquals(Long.valueOf(1), Long.valueOf(rs.getLong(1)));
        assertEquals(Long.valueOf(RACE_ID_2), Long.valueOf(rs.getLong(2)));
        assertFalse(rs.next());
        rs.close();
        
        rs = s.executeQuery("select count, ethnicity, gender, participantCollectionSummaryID, registered_to from "
                + "ParticipantCollectionSummary");
        assertTrue(rs.next());
        assertEquals(count, rs.getInt(1));
        assertEquals(p.getEthnicity(), rs.getString(2));
        assertEquals(p.getGender(), rs.getString(THIRD_PARAM));
        assertEquals(Long.valueOf(1), Long.valueOf(rs.getLong(FOURTH_PARAM)));
        assertEquals(p.getCollectionProtocol().getId(), Long.valueOf(rs.getLong(FIFTH_PARAM)));
        assertEquals(p.getEthnicity(), rs.getString(2));
        assertFalse(rs.next());
        rs.close();
    }
    
    /**
     * verify.
     * @param s statement
     * @throws SQLException on error.
     */
    public static void verifyDeletion(Statement s) throws SQLException {
        CollectionProtocolDaoTest.verifyDeletion(s);
        
        ResultSet rs = s.executeQuery("select count(*) from ParticipantCollectionSummary, "
                + "JoinParticipantCollectionSummaryToRace, JoinParticipantCollectionSummaryToDiagnosis");
        rs.next();
        assertEquals(0, rs.getInt(1));
    }
    
    /**
     * get a test protocol.
     * @return the protocol .
     */
    public static ParticipantCollectionSummary getTestParticipant() {
        ParticipantCollectionSummary p = new ParticipantCollectionSummary();
        p.setId(PARTICIPANT_ID);
        p.setCollectionProtocol(CollectionProtocolDaoTest.getTestCollectionProtocol());
        p.setDiagnosis(new HashSet<Diagnosis>());
        Diagnosis d = new Diagnosis();
        d.setDiagnosisType("Achromatopsia");
        p.getDiagnosis().add(d);
        p.setEthnicity("Hispanic");
        p.setGender("Male");
        p.setRaces(new HashSet<Race>());
        Race r = new Race();
        r.setName("American Indian or Alaska Native");
        p.getRaces().add(r);
        r = new Race();
        r.setName("Native Hawaiian or other Pacific Islander");
        p.getRaces().add(r);
        return p;
    }

}
