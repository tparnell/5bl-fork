/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.util;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.validator.InvalidStateException;
import org.hibernate.validator.InvalidValue;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.CsvParseException;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.CsvParseException.InvalidCsvValue;
import com.fiveamsolutions.tissuelocator.integrator.services.TissueBankIntegrationHelper;
import com.fiveamsolutions.tissuelocator.integrator.test.AbstractHibernateTestCase;

import edu.wustl.catissuecore.domain.Specimen;

/**
 * @author bpickeral
 *
 */
public class NotificationHelperTest extends AbstractHibernateTestCase {
    private static final String MESSAGE = "Test error";
    private static final String EMAIL = "test@5amsolutions.com";

    /**
     * Test sending an notification email.
     * @throws Exception on error
     */
    @Test
    public void testSendErrorNotification() throws Exception {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        NotificationHelper.getInstance().sendErrorNotification(MESSAGE, EMAIL);
        assertEquals(1, Mailbox.get(EMAIL).size());
        testEmail(EMAIL, "An Error occurred during Specimen synchronization", MESSAGE,
                "The following error occurred during Specimen synchronization",
                "Please contact the System administrator.");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Test sending an notification email including an Exception stacktrace.
     * @throws Exception on error
     */
    @Test
    public void testSendErrorNotificationException() throws Exception {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        NotificationHelper.getInstance().sendErrorNotification(MESSAGE, EMAIL, new IllegalArgumentException());
        assertEquals(1, Mailbox.get(EMAIL).size());
        testEmail(EMAIL, "An Error occurred during Specimen synchronization", IllegalArgumentException.class.getName(),
                "The following error occurred during Specimen synchronization",
                "Please contact the System administrator.");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Test sending an notification email including an Exception stacktrace.
     * @throws Exception on error
     */
    @Test
    public void testSendValidationErrorNotificationException() throws Exception {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        InvalidValue iv = new InvalidValue("validation error", Specimen.class, "patientAge", -1, null);
        InvalidStateException ise = new InvalidStateException(new InvalidValue[] {iv});
        Exception e = new Exception(new IllegalArgumentException(ise));
        NotificationHelper.getInstance().sendErrorNotification(MESSAGE, EMAIL, e);
        assertEquals(1, Mailbox.get(EMAIL).size());
        testEmail(EMAIL, "An Error occurred during Specimen synchronization", Exception.class.getName(),
                "The following error occurred during Specimen synchronization",
                "Validation Errors:", "patientAge = -1: validation error",
                "Caused by: " + IllegalArgumentException.class.getName(),
                "Caused by: " + InvalidStateException.class.getName(),
                "validation failed for: " + Specimen.class.getName(),
                "Please contact the System administrator.");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }
    
    /**
     * Test sending a CSV parse error message notification.
     * @throws Exception on error.
     */
    @Test
    public void testSendCsvParseErrorNotification() throws Exception {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        InvalidCsvValue iv = new InvalidCsvValue("minimumPrice", "one dollar", new NumberFormatException());
        List<InvalidCsvValue> invalidValues = new ArrayList<CsvParseException.InvalidCsvValue>();
        invalidValues.add(iv);
        CsvParseException e = new CsvParseException(invalidValues, 2L);
        NotificationHelper.getInstance().sendErrorNotification(MESSAGE, EMAIL, e);
        assertEquals(1, Mailbox.get(EMAIL).size());
        testEmail(EMAIL, "An Error occurred during Specimen synchronization", NumberFormatException.class.getName(),
                "The following error occurred during Specimen synchronization",
                "CSV Parse Errors: Row 2", 
                "Column: minimumPrice",
                "Value: one dollar",
                "Please contact the System administrator.");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Test calculateDaysSinceLastSynchronization.
     */
    @Test
    public void testCalculateDaysSinceLastSynchronization() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        assertEquals("1", NotificationHelper.getInstance().calculateDaysSinceLastSynchronization(cal.getTime()));
    }

    /**
     * Test sendSpecimenIntervalNotification.
     * @throws Exception on error
     */
    @Test
    public void testSendSpecimenIntervalNotification() throws Exception {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        FileImportIntegrationConfig config = TissueBankIntegrationHelper.createConfig(1L, 0, new Date());
        NotificationHelper.getInstance().sendSpecimenIntervalNotification(config, new Date());
        assertEquals(1, Mailbox.get(EMAIL).size());
        testEmail(EMAIL, "Specimen imports have stopped occurring",
                "It has been 0 days since the last Specimen import for institution 1",
                "Please look into why the imports have stopped.");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }
}
