/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.SynchronizationAdapter;
import com.fiveamsolutions.tissuelocator.integrator.services.TissueBankIntegrationHelper;
import com.fiveamsolutions.tissuelocator.integrator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 *
 * @author bpickeral
 *
 * @param <A> the type of the adapter
 * @param <T> the type of the persistent object the adapter works with
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public abstract class SynchronizationAdapterTest <A extends SynchronizationAdapter, T extends PersistentObject>
extends AbstractHibernateTestCase {

    /**
     * @return a new imported object
     */
    public abstract T createNewImportedObject();

    /**
     * @return an existing imported object
     */
    public abstract T createExistingImportedObject();

    private A adapter;
    private final Class<A> aClass;
    {
        ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
        final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        if (actualTypeArguments[0] instanceof Class) {
            aClass = (Class<A>) actualTypeArguments[0];
        } else {
            aClass = (Class<A>) ((ParameterizedType) actualTypeArguments[0]).getRawType();
        }
    }

    /**
     * initialize the adapter.
     * @throws Exception on error
     */
    @Before
    public void initAdapter() throws Exception {
        TissueBankIntegrationConfigServiceLocal service =
            TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
        AbstractTissueBankIntegrationConfig config =
            TissueBankIntegrationHelper.createConfig(1L, 1, new Date());
        service.savePersistentObject(config);
        adapter = aClass.newInstance();
    }

    /**
     * test the synchronize method.
     */
    @Test
    public void testSynchronize() {
        // Assert No Exceptions occur when creating a new object, since Mock methods are called
        // to store Specimen in core, there is nothing else to check for a newly created object
        T importedObject = createNewImportedObject();
        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);
        Institution i = new Institution();
        i.setId(1L);
        Map<String, PersistentObject> dependencies = new HashMap<String, PersistentObject>();
        T updatedObject = (T) adapter.synchronize(importedObject, null, dependencies, i, translationMap);
        assertNotNull(updatedObject);
        assertEquals(updatedObject, importedObject);
    }

    /**
     * check synchronization with an existing object.
     * @param updatedObject the updated object
     * @param objectCopiedFrom the object the properties were copied from
     */
    public abstract void checkSynchronizeExisting(T updatedObject, T objectCopiedFrom);

    /**
     * test the synchronize method with an existing object.
     */
    @Test
    public void testSynchronizeExisting() {
        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);
        Institution i = new Institution();
        i.setId(1L);
        T importedObject = createExistingImportedObject();
        Map<String, PersistentObject> dependencies = new HashMap<String, PersistentObject>();
        T updatedObject = (T) adapter.synchronize(importedObject, null, dependencies, i, translationMap);
        // Check that all variables were copied to the object to be updated
        checkSynchronizeExisting(updatedObject, importedObject);

        // Null Test
        assertNull("Adapter must handle null case.", adapter.synchronize(null, null, null, i, translationMap));
    }

    /**
     * get the adapter.
     * @return the adapter
     */
    public A getAdapter() {
        return adapter;
    }
}
