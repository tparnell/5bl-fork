/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Date;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.services.TissueBankIntegrationHelper;
import com.fiveamsolutions.tissuelocator.integrator.test.AbstractHibernateTestCase;

/**
 * @author bpickeral
 *
 */
public class CodeHelperTest extends AbstractHibernateTestCase {
    private static final String SPECIMEN_TYPE = "specimen type";
    private static final String APF = "Abdominal fibromatosis";
    private TissueBankIntegrationConfigServiceLocal service = null;

    /**
     * Setup the config object.
     */
    @Before
    public void setup() {
        service = TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
        AbstractTissueBankIntegrationConfig config =
            TissueBankIntegrationHelper.createConfig(1L, 1, new Date());
        service.savePersistentObject(config);
    }

    /**
     * Test setting the SpecimenType.
     */
    @Test
    public void testSetSpecimenType() {
        Specimen specimen = new Specimen();
        SpecimenType specimenType = new SpecimenType();
        specimenType.setName(SPECIMEN_TYPE);
        specimen.setSpecimenType(specimenType);
        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);

        CodeHelper ch = new CodeHelper();
        ch.setSystemCodes(specimen, 1L, translationMap);
        assertNotNull(specimen.getSpecimenType());
        assertEquals(SPECIMEN_TYPE, specimen.getSpecimenType().getName());
        ch.setSystemCodes(specimen, 1L, translationMap);
        assertNotNull(specimen.getSpecimenType());
        assertEquals(SPECIMEN_TYPE, specimen.getSpecimenType().getName());
        assertNull(specimen.getPathologicalCharacteristic());
    }

    /**
     * Test setting the PathologicalCharacteristic.
     */
    @Test
    public void testSetDisease() {
        Specimen specimen = new Specimen();
        AdditionalPathologicFinding apf = new AdditionalPathologicFinding();
        apf.setName(APF);
        specimen.setPathologicalCharacteristic(apf);

        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);
        CodeHelper ch = new CodeHelper();
        ch.setSystemCodes(specimen, 1L, translationMap);
        assertNotNull(specimen.getPathologicalCharacteristic());
        assertEquals(APF, specimen.getPathologicalCharacteristic().getName());
        ch.setSystemCodes(specimen, 1L, translationMap);
        assertNotNull(specimen.getPathologicalCharacteristic());
        assertEquals(APF, specimen.getPathologicalCharacteristic().getName());
        assertNull(specimen.getSpecimenType());
    }

    /**
     * Test setting the AnatomicSource.
     */
    @Test
    public void testSetAnatomicSource() {
        Specimen specimen = new Specimen();

        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);
        new CodeHelper().setSystemCodes(specimen, 1L, translationMap);
        assertNull(specimen.getPathologicalCharacteristic());
        assertNull(specimen.getSpecimenType());
    }

    /**
     * Test setting the PreservationType.
     */
    @Test
    public void testSetPreservationType() {
        Specimen specimen = new Specimen();

        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);
        new CodeHelper().setSystemCodes(specimen, 1L, translationMap);
        assertNull(specimen.getSpecimenType());
        assertNull(specimen.getPathologicalCharacteristic());

        new CodeHelper().setSystemCodes(specimen, 1L, translationMap);
        assertNull(specimen.getSpecimenType());
        assertNull(specimen.getPathologicalCharacteristic());
    }
}
