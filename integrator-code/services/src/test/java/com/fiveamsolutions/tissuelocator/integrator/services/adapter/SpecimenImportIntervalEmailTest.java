/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services.adapter;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.mail.MessagingException;
import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.ImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.service.ImportBatchServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.services.TissueBankIntegrationHelper;
import com.fiveamsolutions.tissuelocator.integrator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author smiller
 *
 */
public class SpecimenImportIntervalEmailTest extends AbstractHibernateTestCase {
    private AbstractTissueBankIntegrationConfig config;
    /** Mock service will return empty list of files when this id is used. **/
    private static final long INSTITUTION_ID = 3456L;
    private static TissueBankIntegrationConfigServiceLocal service;
    private static final int INTERVAL = 600000;

    /**
     * Before method.
     */
    @Before
    public void before() {
        service = TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
        config = TissueBankIntegrationHelper.createConfig(INSTITUTION_ID, 1, new Date());
        Long id = service.savePersistentObject(config);
        config = service.getPersistentObject(AbstractTissueBankIntegrationConfig.class, id);
    }

    /**
     * Test that notification is sent if no specimen is saved within interval.
     * @throws JAXBException on JAXB error
     * @throws ParseException if error occurs parsing
     * @throws MessagingException If error occurs when sending email
     * @throws IOException if IO Exception occurs
     */
    @Test
    public void checkIntervalTest() throws JAXBException, ParseException, IOException, MessagingException {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        ImportBatchServiceLocal importBatchService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getImportBatchService();
        importBatchService.savePersistentObject(createImportBatch());
        assertEquals(1, importBatchService.getAll(ImportBatch.class).size());
        importBatchService.checkSpecimenImportInterval(config);
        assertEquals(1, Mailbox.get(TissueBankIntegrationHelper.NOTIFICATION_EMAIL).size());
        testEmail(TissueBankIntegrationHelper.NOTIFICATION_EMAIL, "Specimen imports have stopped occurring",
                "It has been 2 days since the last Specimen import for institution 3456.",
                "Please look into why the imports have stopped.");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    private ImportBatch createImportBatch() {
        ImportBatch ib = new ImportBatch();
        ib.setStartTime(new Date());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        cal.add(Calendar.DATE, -1);
        ib.setEndTime(cal.getTime());
        ib.setConfig(config);
        ib.setImports(1);
        ib.setFailures(1);
        return ib;
    }

    /**
     * Test that notification is not sent if specimen is saved within interval.
     * @throws JAXBException on JAXB error
     * @throws ParseException if error occurs parsing
     * @throws MessagingException If error occurs when sending email
     * @throws IOException if IO Exception occurs
     */
    @Test
    public void testNotificationNotSent() throws JAXBException, ParseException, IOException, MessagingException {
        // Check email not sent when specimen saved inside of interval
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        ((FileImportIntegrationConfig) config).setNotificationInterval(INTERVAL);
        Long id = service.savePersistentObject(config);
        config = service.getPersistentObject(AbstractTissueBankIntegrationConfig.class, id);
        ImportBatchServiceLocal importBatchService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getImportBatchService();
        importBatchService.savePersistentObject(createImportBatch());
        assertEquals(1, importBatchService.getAll(ImportBatch.class).size());
        importBatchService.checkSpecimenImportInterval(config);
        assertEquals(0, Mailbox.get(TissueBankIntegrationHelper.NOTIFICATION_EMAIL).size());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Test that notification is not sent if specimen is saved within interval.
     * @throws JAXBException on JAXB error
     * @throws ParseException if error occurs parsing
     * @throws MessagingException If error occurs when sending email
     * @throws IOException if IO Exception occurs
     */
    @Test
    public void testNotificationNotSentFirst() throws JAXBException, ParseException, IOException, MessagingException {
        // Check email not sent when there are no previous imports
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        TissueLocatorIntegratorRegistry.getServiceLocator().getImportBatchService().checkSpecimenImportInterval(config);
        assertEquals(0, Mailbox.get(TissueBankIntegrationHelper.NOTIFICATION_EMAIL).size());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }
}
