/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceBean;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceBean;
import com.google.inject.Injector;

/**
 * Acts a bit like a container, setting up @EJB references by hand.
 */
public final class EjbTestHelper {

    private static Injector injector;

    /**
     * @param injector the injector to set
     */
    public static void setInjector(Injector injector) {
        EjbTestHelper.injector = injector;
    }

    /**
     * get the generic service bean.
     * @param <T> the class
     * @param clazz the class
     * @return the service
     */
    @SuppressWarnings("unchecked")
    public static <T extends PersistentObject> GenericServiceBean<T> getGenericServiceBean(Class<T> clazz) {
        return injector.getInstance(GenericServiceBean.class);
    }

    /**
     * get the user service.
     * @return the user service
     */
    public static TissueLocatorUserServiceBean getTissueLocatorUserServiceBean() {
        TissueLocatorUserServiceBean serviceBean = injector.getInstance(TissueLocatorUserServiceBean.class);
        serviceBean.setApplicationSettingService(injector.getInstance(ApplicationSettingServiceBean.class));
        return serviceBean;
    }

    /**
     * get the user service.
     * @return the user service
     */
    public static TissueLocatorUserServiceBean getTissueLocatorUserServiceBeanWithMockLoad() {
        TissueLocatorUserServiceBean userService = new TissueLocatorUserServiceBean() {

            /**
             * {@inheritDoc}
             */
            @SuppressWarnings("unchecked")
            @Override
            public TissueLocatorUser getPersistentObject(@SuppressWarnings("rawtypes") Class toClass, Long id) {
                TissueLocatorUser user = new TissueLocatorUser();
                user.setId(id);
                return user;
            }
        };
        injector.injectMembers(userService);
        return userService;
    }
}
