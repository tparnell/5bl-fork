/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.util.OutstandingSpecimenRequestRequirements;
import com.fiveamsolutions.tissuelocator.util.RequestProcessingConfiguration;

/**
 * @author smiller
 *
 */
public class SpecimenRequestStatusAnalyzerTest {

    private static final int REVIEWER_ASSIGNMENT_PERIOD = 1;

    /**
     * test the analyze request status method.
     */
    @Test
    public void testAnalyzeRequestStatus() {
        RequestProcessingConfiguration config = new RequestProcessingConfiguration();
        config.setReviewerAssignmentPeriod(REVIEWER_ASSIGNMENT_PERIOD);
        SpecimenRequestStatusAnalyzer analyzer = new SpecimenRequestStatusAnalyzer(config);

        Institution i1 = new Institution();
        i1.setName("institution 1");
        SpecimenRequestReviewVote vote1 = new SpecimenRequestReviewVote();
        vote1.setInstitution(i1);
        vote1.setUser(new TissueLocatorUser());

        Institution i2 = new Institution();
        i2.setName("institution 2");
        SpecimenRequestReviewVote vote2 = new SpecimenRequestReviewVote();
        vote2.setInstitution(i2);

        SpecimenRequest request = new SpecimenRequest();
        request.getConsortiumReviews().add(vote1);
        request.getConsortiumReviews().add(vote2);
        request.setUpdatedDate(new Date());

        //reviewer assignment period not over
        OutstandingSpecimenRequestRequirements issues = analyzer.analyzeRequestStatus(request);
        assertNotNull(issues);
        assertNotNull(issues.getOutstandingReviewAssignments());
        assertTrue(issues.getOutstandingReviewAssignments().isEmpty());

        //reviewer assignment period over, i2 unassigned
        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, -REVIEWER_ASSIGNMENT_PERIOD - 1);
        request.setUpdatedDate(cal.getTime());
        issues = analyzer.analyzeRequestStatus(request);
        assertNotNull(issues);
        assertNotNull(issues.getOutstandingReviewAssignments());
        assertFalse(issues.getOutstandingReviewAssignments().isEmpty());
        assertEquals(1, issues.getOutstandingReviewAssignments().size());
        assertEquals(i2, issues.getOutstandingReviewAssignments().iterator().next());
    }
}
