/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.test.SpecimenPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.AdditionalPathologicFindingUseCountHandler;

/**
 * @author gvaughn
 *
 */
public class AdditionalPathologicFindingUseCountHandlerTest extends AbstractHibernateTestCase {

    /**
     * Test the updateAdditionalPathologicFindings method.
     */
    @Test
    public void testUpdateAdditionalPathologicFindings() {
        AdditionalPathologicFindingUseCountHandler handler = new AdditionalPathologicFindingUseCountHandler();
        AdditionalPathologicFinding apf1 = SpecimenPersistenceHelper.createCode(AdditionalPathologicFinding.class, 0);
        AdditionalPathologicFinding apf2 = SpecimenPersistenceHelper.createCode(AdditionalPathologicFinding.class, 1);
        
        //same status (avaialable), same apf, no change in count
        apf1.setUseCount(1L);
        Specimen specimen = new Specimen();
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        specimen.setPreviousStatus(SpecimenStatus.AVAILABLE);
        specimen.setPathologicalCharacteristic(apf1);
        specimen.setPreviousPathologicalCharacteristic(apf1);
        handler.updateAdditionalPathologicFindings(specimen);
        assertEquals(new Long(1L), specimen.getPathologicalCharacteristic().getUseCount());
        assertEquals(new Long(1L), specimen.getPreviousPathologicalCharacteristic().getUseCount());
        
        //same status (unavaialable), same apf, no change in count
        apf1.setUseCount(1L);
        specimen = new Specimen();
        specimen.setStatus(SpecimenStatus.DESTROYED);
        specimen.setPreviousStatus(SpecimenStatus.UNAVAILABLE);
        specimen.setPathologicalCharacteristic(apf1);
        specimen.setPreviousPathologicalCharacteristic(apf1);
        handler.updateAdditionalPathologicFindings(specimen);
        assertEquals(new Long(1L), specimen.getPathologicalCharacteristic().getUseCount());
        assertEquals(new Long(1L), specimen.getPreviousPathologicalCharacteristic().getUseCount());
        
        //same status (avaialable), diff apf
        apf1.setUseCount(1L);
        apf2.setUseCount(0L);
        specimen = new Specimen();
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        specimen.setPreviousStatus(SpecimenStatus.AVAILABLE);
        specimen.setPathologicalCharacteristic(apf2);
        specimen.setPreviousPathologicalCharacteristic(apf1);
        handler.updateAdditionalPathologicFindings(specimen);
        assertEquals(new Long(1L), specimen.getPathologicalCharacteristic().getUseCount());
        assertEquals(new Long(0L), specimen.getPreviousPathologicalCharacteristic().getUseCount());
        
        //same status (unavaialable), new apf, no change in count
        apf1.setUseCount(0L);
        apf2.setUseCount(1L);
        specimen = new Specimen();
        specimen.setStatus(SpecimenStatus.DESTROYED);
        specimen.setPreviousStatus(SpecimenStatus.UNAVAILABLE);
        specimen.setPathologicalCharacteristic(apf2);
        specimen.setPreviousPathologicalCharacteristic(apf1);
        handler.updateAdditionalPathologicFindings(specimen);
        assertEquals(new Long(1L), specimen.getPathologicalCharacteristic().getUseCount());
        assertEquals(new Long(0L), specimen.getPreviousPathologicalCharacteristic().getUseCount());
        
        //new status (avaialable), same apf
        apf1.setUseCount(0L);
        specimen = new Specimen();
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        specimen.setPreviousStatus(SpecimenStatus.UNAVAILABLE);
        specimen.setPathologicalCharacteristic(apf1);
        specimen.setPreviousPathologicalCharacteristic(apf1);
        handler.updateAdditionalPathologicFindings(specimen);
        assertEquals(new Long(1L), specimen.getPathologicalCharacteristic().getUseCount());
        assertEquals(new Long(1L), specimen.getPreviousPathologicalCharacteristic().getUseCount());
        
        //new status (unavaialable), same apf
        apf1.setUseCount(1L);
        specimen = new Specimen();
        specimen.setStatus(SpecimenStatus.UNAVAILABLE);
        specimen.setPreviousStatus(SpecimenStatus.AVAILABLE);
        specimen.setPathologicalCharacteristic(apf1);
        specimen.setPreviousPathologicalCharacteristic(apf1);
        handler.updateAdditionalPathologicFindings(specimen);
        assertEquals(new Long(0L), specimen.getPathologicalCharacteristic().getUseCount());
        assertEquals(new Long(0L), specimen.getPreviousPathologicalCharacteristic().getUseCount());
        
        //new status (avaialable), diff apf
        apf1.setUseCount(0L);
        apf2.setUseCount(0L);
        specimen = new Specimen();
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        specimen.setPreviousStatus(SpecimenStatus.UNAVAILABLE);
        specimen.setPathologicalCharacteristic(apf2);
        specimen.setPreviousPathologicalCharacteristic(apf1);
        handler.updateAdditionalPathologicFindings(specimen);
        assertEquals(new Long(1L), specimen.getPathologicalCharacteristic().getUseCount());
        assertEquals(new Long(0L), specimen.getPreviousPathologicalCharacteristic().getUseCount());
        
        //new status (unavaialable), diff apf
        apf1.setUseCount(1L);
        apf2.setUseCount(0L);
        specimen = new Specimen();
        specimen.setStatus(SpecimenStatus.UNAVAILABLE);
        specimen.setPreviousStatus(SpecimenStatus.AVAILABLE);
        specimen.setPathologicalCharacteristic(apf2);
        specimen.setPreviousPathologicalCharacteristic(apf1);
        handler.updateAdditionalPathologicFindings(specimen);
        assertEquals(new Long(0L), specimen.getPathologicalCharacteristic().getUseCount());
        assertEquals(new Long(0L), specimen.getPreviousPathologicalCharacteristic().getUseCount());
        
        //new status (avaialable), new apf
        apf2.setUseCount(0L);
        specimen = new Specimen();
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        specimen.setPreviousStatus(null);
        specimen.setPathologicalCharacteristic(apf2);
        specimen.setPreviousPathologicalCharacteristic(null);
        handler.updateAdditionalPathologicFindings(specimen);
        assertEquals(new Long(1L), specimen.getPathologicalCharacteristic().getUseCount());
        
        //new status (unavaialable), new apf
        apf2.setUseCount(0L);
        specimen = new Specimen();
        specimen.setStatus(SpecimenStatus.UNAVAILABLE);
        specimen.setPreviousStatus(null);
        specimen.setPathologicalCharacteristic(apf2);
        specimen.setPreviousPathologicalCharacteristic(null);
        handler.updateAdditionalPathologicFindings(specimen);
        assertEquals(new Long(0L), specimen.getPathologicalCharacteristic().getUseCount());
    }
}
