/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Contact;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.validation.RequireConsortiumContactInfoValidator;

/**
 * @author smiller
 *
 */
public class RequiredConsortiumContactValidatorTest {
    
    /**
     * Test the validator.
     */
    @Test
    public void testValidator() {
        RequireConsortiumContactInfoValidator validator = new RequireConsortiumContactInfoValidator();
        validator.initialize(null);
        assertFalse(validator.isValid(null));
        assertFalse(validator.isValid(new TissueLocatorUser()));
        
        Institution obj = new Institution();
        obj.setConsortiumMember(true);
        Contact c = new Contact();
        obj.setContact(c);
        assertFalse(validator.isValid(obj));

        c.setEmail("test email");
        c.setFirstName("fname");
        c.setLastName("lname");
        c.setPhone("phone");
        c.setPhoneExtension("extension");
        assertTrue(validator.isValid(obj));
        
        String tmp = c.getEmail();
        c.setEmail("");
        assertFalse(validator.isValid(obj));
        c.setEmail(tmp);
        
        tmp = c.getFirstName();
        c.setFirstName(null);
        assertFalse(validator.isValid(obj));
        c.setFirstName(tmp);
        
        tmp = c.getLastName();
        c.setLastName(null);
        assertFalse(validator.isValid(obj));
        c.setLastName(tmp);
        
        tmp = c.getPhone();
        c.setPhone(null);
        assertFalse(validator.isValid(obj));
        c.setPhone(tmp);
        
        obj.setContact(null);
        assertFalse(validator.isValid(obj));
        
        obj.setConsortiumMember(false);
        assertTrue(validator.isValid(obj));
    }
}
