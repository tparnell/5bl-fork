/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.PropertyValueException;
import org.hibernate.validator.InvalidStateException;
import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.data.validation.ConsistentInstitutionsValidator;
import com.fiveamsolutions.tissuelocator.data.validation.PastYearValidator;
import com.fiveamsolutions.tissuelocator.data.validation.RequiredSpecimenPriceValidator;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenPriceRelationshipValidator;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.test.SpecimenPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.NameCountPair;
import com.fiveamsolutions.tissuelocator.util.ParticipantReturnRequestData;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
public class SpecimenPersistenceTest extends AbstractPersistenceTest<Specimen> {

    private static final int PAGE_SIZE = 10;
    private static final int MAX_AGE_YEARS = 90;
    private static int codeSuffix = 1;

    /**
     * {@inheritDoc}
     */
    @Override
    public GenericServiceLocal<Specimen> getService() {
        return TissueLocatorRegistry.getServiceLocator().getSpecimenService();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Specimen[] getValidObjects() {
        codeSuffix++;
        AdditionalPathologicFinding apf =
            SpecimenPersistenceHelper.createCode(AdditionalPathologicFinding.class, codeSuffix);
        SpecimenType st = SpecimenPersistenceHelper.createCode(SpecimenType.class, codeSuffix);
        Institution i = EjbTestHelper.getGenericServiceBean(Institution.class).getPersistentObject(Institution.class,
                getCurrentUser().getInstitution().getId());

        Participant participant = SpecimenPersistenceHelper.createAndStoreParticipant(i, codeSuffix);

        CollectionProtocol protocol = SpecimenPersistenceHelper.createAndStoreProtocol(i, codeSuffix);

        Specimen s1 = new Specimen();
        s1.setExternalId("ABRC-1234" + codeSuffix);
        s1.setPatientAgeAtCollection(MAX_AGE_YEARS);
        s1.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        assertTrue(s1.isPatientAgeAtCollectionAtHippaMax());
        s1.setPatientAgeAtCollection(1);
        assertFalse(s1.isPatientAgeAtCollectionAtHippaMax());
        assertNotNull(TimeUnits.YEARS.getResourceKey());
        s1.setAvailableQuantity(new BigDecimal("1.0"));
        s1.setAvailableQuantityUnits(QuantityUnits.MG);
        assertNotNull(QuantityUnits.MG.getResourceKey());
        s1.setPathologicalCharacteristic(apf);
        s1.setSpecimenType(st);
        s1.setExternalIdAssigner(i);
        s1.setParticipant(participant);
        s1.setProtocol(protocol);
        s1.setCollectionYear(Calendar.getInstance().get(Calendar.YEAR));
        s1.setMinimumPrice(new BigDecimal("12.34"));
        s1.setMaximumPrice(new BigDecimal("56.78"));
        s1.setPriceNegotiable(false);
        s1.setStatus(SpecimenStatus.UNAVAILABLE);

        Specimen s2 = new Specimen();
        s2.setExternalId("ABRC-3456" + codeSuffix);
        s2.setPatientAgeAtCollection(2);
        s2.setPatientAgeAtCollectionUnits(TimeUnits.MONTHS);
        s2.setPathologicalCharacteristic(apf);
        s2.setSpecimenType(st);
        s2.setExternalIdAssigner(i);
        s2.setParticipant(participant);
        s2.setProtocol(protocol);
        s2.setPriceNegotiable(true);
        s2.setStatus(SpecimenStatus.AVAILABLE);
        return new Specimen[] {s1, s2};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(Specimen expected, Specimen actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
    }

    /**
     * Test search.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        Specimen example = new Specimen();
        example.setExternalId("test");
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        SearchCriteria<Specimen> sc = new TissueLocatorAnnotatedBeanSearchCriteria<Specimen>(example);
        PageSortParams<Specimen> psp =
              new PageSortParams<Specimen>(PAGE_SIZE, 0, SpecimenSortCriterion.EXTERNAL_ID, true);
        List<Specimen> results = service.search(sc, psp);
        assertEquals(0, results.size());
        example.setExternalId("1234");
        results = service.search(sc, psp);
        assertEquals(1, results.size());
        assertEquals("ABRC-1234" + codeSuffix, results.get(0).getExternalId());
        Specimen s1 = results.get(0);
        example.setExternalId("3456");
        results = service.search(sc, psp);
        assertEquals(1, results.size());
        assertEquals("ABRC-3456" + codeSuffix, results.get(0).getExternalId());
        Specimen s2 = results.get(0);
        example.setExternalId("ABRC");
        results = service.search(sc, psp);
        assertEquals(2, results.size());
        assertTrue(results.get(0).getExternalId().compareTo(results.get(1).getExternalId()) > 0);
        example.setExternalId("abrc");
        results = service.search(sc, psp);
        assertEquals(2, results.size());
        assertTrue(results.get(0).getExternalId().compareTo(results.get(1).getExternalId()) > 0);

        example.setExternalId(null);
        example.setId(s1.getId());
        results = service.search(sc, psp);
        assertEquals(1, results.size());
        assertEquals(s1.getId(), results.get(0).getId());
        example.setId(s2.getId());
        results = service.search(sc, psp);
        assertEquals(1, results.size());
        assertEquals(s2.getId(), results.get(0).getId());

        // Units alone should not affect results
        example.setId(null);
        example.setAvailableQuantityUnits(QuantityUnits.MG);
        results = service.search(sc, psp);
        assertEquals(2, results.size());
        example.setAvailableQuantityUnits(QuantityUnits.UG);
        results = service.search(sc, psp);
        assertEquals(2, results.size());

        example.setAvailableQuantityUnits(null);
        example.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        results = service.search(sc, psp);
        assertEquals(2, results.size());
        example.setPatientAgeAtCollectionUnits(TimeUnits.MONTHS);
        results = service.search(sc, psp);
        assertEquals(2, results.size());
        example.setPatientAgeAtCollectionUnits(TimeUnits.HOURS);
        results = service.search(sc, psp);
        assertEquals(2, results.size());

        example.setPatientAgeAtCollectionUnits(null);
        example.setParticipant(new Participant());
        example.getParticipant().setGender(Gender.FEMALE);
        results = service.search(sc, psp);
        assertEquals(2, results.size());
        example.getParticipant().setGender(Gender.MALE);
        results = service.search(sc, psp);
        assertEquals(0, results.size());

        example.getParticipant().setGender(null);
        example.getParticipant().setEthnicity(Ethnicity.HISPANIC_OR_LATINO);
        results = service.search(sc, psp);
        assertEquals(2, results.size());
        example.getParticipant().setEthnicity(Ethnicity.NOT_REPORTED);
        results = service.search(sc, psp);
        assertEquals(0, results.size());

        example.setParticipant(null);
        example.setPathologicalCharacteristic(new AdditionalPathologicFinding());
        String pathCharName = s1.getPathologicalCharacteristic().getName();
        example.getPathologicalCharacteristic().setName(pathCharName);
        results = service.search(sc, psp);
        assertEquals(2, results.size());
        example.getPathologicalCharacteristic().setName(pathCharName.toUpperCase());
        results = service.search(sc, psp);
        assertEquals(2, results.size());
        example.getPathologicalCharacteristic().setName(pathCharName.toLowerCase());
        results = service.search(sc, psp);
        assertEquals(2, results.size());
        example.getPathologicalCharacteristic().setName(pathCharName.substring(2, pathCharName.length() - 2));
        results = service.search(sc, psp);
        assertEquals(2, results.size());

        example.setPathologicalCharacteristic(null);
        example.setSpecimenType(new SpecimenType());
        example.getSpecimenType().setName(s1.getSpecimenType().getName());
        results = service.search(sc, psp);
        assertEquals(2, results.size());

        example.getSpecimenType().setName(null);
        example.getSpecimenType().setSpecimenClass(s1.getSpecimenType().getSpecimenClass());
        results = service.search(sc, psp);
        assertEquals(2, results.size());

        example.setSpecimenType(s1.getSpecimenType());
        results = service.search(sc, psp);
        assertEquals(2, results.size());

        for (SpecimenSortCriterion sort : SpecimenSortCriterion.values()) {
            assertEquals(2, service.search(sc, new PageSortParams<Specimen>(PAGE_SIZE, 0, sort, false)).size());
        }
    }

    /**
     * test hash code and equals methods.
     */
    @Test
    public void testHashCodeAndEquals() {
        String name = "test";
        Institution inst = new Institution();
        inst.setName(name);
        Institution inst2 = new Institution();

        String externalId = "test";
        Specimen spec = new Specimen();
        spec.setExternalId(externalId);
        assertFalse(spec.equals(null));
        assertFalse(spec.equals(new Participant()));
        assertTrue(spec.equals(spec));

        Specimen spec2 = new Specimen();
        assertFalse(spec.equals(spec2));
        assertFalse(spec2.equals(spec));
        assertNotSame(spec.hashCode(), spec2.hashCode());

        spec2.setExternalId(externalId);
        assertTrue(spec.equals(spec2));
        assertTrue(spec2.equals(spec));
        assertEquals(spec.hashCode(), spec2.hashCode());

        spec.setExternalIdAssigner(inst);
        assertFalse(spec.equals(spec2));
        assertFalse(spec2.equals(spec));
        assertNotSame(spec.hashCode(), spec2.hashCode());

        spec2.setExternalIdAssigner(inst2);
        assertFalse(spec.equals(spec2));
        assertFalse(spec2.equals(spec));
        assertNotSame(spec.hashCode(), spec2.hashCode());

        inst2.setName(name);
        assertTrue(spec.equals(spec2));
        assertTrue(spec2.equals(spec));
        assertEquals(spec.hashCode(), spec2.hashCode());
        assertTrue(spec.equals(spec));
    }

    /**
     * test the consistent institutions validator.
     */
    @Test
    public void testConsistentInstitutionsValidator() {
        ConsistentInstitutionsValidator validator = new ConsistentInstitutionsValidator();
        validator.initialize(null);
        validator.apply(null);

        Institution i = getCurrentUser().getInstitution();
        Specimen spec = new Specimen();
        spec.setExternalIdAssigner(i);
        CollectionProtocol prot = new CollectionProtocol();
        prot.setInstitution(i);
        spec.setProtocol(prot);
        Participant part = new Participant();
        part.setExternalIdAssigner(i);
        spec.setParticipant(part);
        assertTrue(validator.isValid(spec));

        Institution i2 = createUserSkipRole().getInstitution();
        spec.setExternalIdAssigner(i2);
        assertFalse(validator.isValid(spec));
        spec.setExternalIdAssigner(i);
        prot.setInstitution(i2);
        assertFalse(validator.isValid(spec));
        prot.setInstitution(i);
        part.setExternalIdAssigner(i2);
        assertFalse(validator.isValid(spec));

        part.setExternalIdAssigner(i);
        spec.setExternalIdAssigner(null);
        assertFalse(validator.isValid(spec));
        spec.setExternalIdAssigner(i);
        prot.setInstitution(null);
        assertFalse(validator.isValid(spec));
        prot.setInstitution(i);
        part.setExternalIdAssigner(null);
        assertFalse(validator.isValid(spec));

        part.setExternalIdAssigner(i);
        spec.setExternalIdAssigner(null);
        assertFalse(validator.isValid(spec));
        spec.setExternalIdAssigner(i);
        spec.setProtocol(null);
        assertFalse(validator.isValid(spec));
        spec.setProtocol(prot);
        spec.setParticipant(null);
        assertFalse(validator.isValid(spec));

        assertFalse(validator.isValid(new Institution()));
    }


    /**
     * Tests the required specimen price validator.
     */
    @Test
    public void testRequiredSpecimenPriceValidator() {
        RequiredSpecimenPriceValidator validator = new RequiredSpecimenPriceValidator();
        validator.initialize(null);
        validator.apply(null);

        Specimen s = new Specimen();
        assertFalse(validator.isValid(s));
        s.setMaximumPrice(new BigDecimal("1.0"));
        assertFalse(validator.isValid(s));
        s.setMinimumPrice(new BigDecimal("1.0"));
        s.setMaximumPrice(null);
        assertFalse(validator.isValid(s));
        s.setMaximumPrice(new BigDecimal("1.0"));
        assertTrue(validator.isValid(s));

        s.setPriceNegotiable(true);
        s.setMaximumPrice(null);
        s.setMinimumPrice(null);
        assertTrue(validator.isValid(s));
        s.setMaximumPrice(new BigDecimal("1.0"));
        assertFalse(validator.isValid(s));
        s.setMinimumPrice(new BigDecimal("1.0"));
        s.setMaximumPrice(null);
        assertFalse(validator.isValid(s));
        s.setMaximumPrice(new BigDecimal("1.0"));
        assertFalse(validator.isValid(s));

        assertFalse(validator.isValid(new Institution()));
    }

    /**
     * Tests the specimen price relationship validator.
     */
    @Test
    public void testSpecimenPriceRelationshipValidator() {
        SpecimenPriceRelationshipValidator validator = new SpecimenPriceRelationshipValidator();
        validator.initialize(null);
        validator.apply(null);

        Specimen s = new Specimen();
        assertTrue(validator.isValid(s));

        s.setMinimumPrice(new BigDecimal("1.0"));
        assertTrue(validator.isValid(s));
        s.setMinimumPrice(null);
        s.setMaximumPrice(new BigDecimal("2.0"));
        assertFalse(validator.isValid(s));

        s.setMinimumPrice(new BigDecimal("1.0"));
        assertTrue(validator.isValid(s));

        s.setMinimumPrice(new BigDecimal("2.0"));
        assertTrue(validator.isValid(s));

        s.setMinimumPrice(new BigDecimal("3.0"));
        assertFalse(validator.isValid(s));

        assertFalse(validator.isValid(new Institution()));
    }

    /**
     * test the past year validator throws exceptions when needed.
     */
    @Test(expected = InvalidStateException.class)
    public void testPastYearValidator() {
        assertTrue(new PastYearValidator().isValid(null));
        Specimen s = getValidObjects()[0];
        s.setCollectionYear(Calendar.getInstance().get(Calendar.YEAR) + 1);
        getService().savePersistentObject(s);
    }

    /**
     * test the getCountsByClass service method.
     */
    @Test
    public void testGetCountsByClass() {
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        Map<SpecimenClass, Long> result = service.getCountsByClass();
        assertNotNull(result);
        assertTrue(result.isEmpty());
        testSaveRetrieve();
        result = service.getCountsByClass();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertFalse(result.containsKey(SpecimenClass.CELLS));
        assertNull(result.get(SpecimenClass.CELLS));
        assertFalse(result.containsKey(SpecimenClass.FLUID));
        assertNull(result.get(SpecimenClass.FLUID));
        assertFalse(result.containsKey(SpecimenClass.MOLECULAR));
        assertNull(result.get(SpecimenClass.MOLECULAR));
        assertTrue(result.containsKey(SpecimenClass.TISSUE));
        assertNotNull(result.get(SpecimenClass.TISSUE));
        assertEquals(1, result.get(SpecimenClass.TISSUE).intValue());
    }

    /**
     * test the getCountsByPathologicalCharacteristic service method.
     */
    @Test
    public void testGetCountsByPathologicalCharacteristic() {
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        Map<String, Long[]> result = service.getCountsByPathologicalCharacteristic();
        assertNotNull(result);
        assertTrue(result.isEmpty());
        testSaveRetrieve();
        Specimen specimen = service.getAll(Specimen.class).get(0);
        String apfName = specimen.getPathologicalCharacteristic().getName();
        Long apfId = specimen.getPathologicalCharacteristic().getId();
        result = service.getCountsByPathologicalCharacteristic();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertTrue(result.containsKey(apfName));
        assertNotNull(result.get(apfName));
        assertEquals(1, result.get(apfName)[1].intValue());
        assertEquals(apfId.intValue(), result.get(apfName)[0].intValue());

        specimen = service.getAll(Specimen.class).get(0);
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        Long specId = service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, specId);
        result = service.getCountsByPathologicalCharacteristic();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertTrue(result.containsKey(apfName));
        assertNotNull(result.get(apfName));
        assertEquals(2, result.get(apfName)[1].intValue());
        assertEquals(apfId.intValue(), result.get(apfName)[0].intValue());

        AdditionalPathologicFinding apf2 =
            SpecimenPersistenceHelper.createCode(AdditionalPathologicFinding.class, ++codeSuffix);
        specimen.setPathologicalCharacteristic(apf2);
        service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, specId);
        result = service.getCountsByPathologicalCharacteristic();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
        assertTrue(result.containsKey(apfName));
        assertNotNull(result.get(apfName));
        assertEquals(1, result.get(apfName)[1].intValue());
        assertEquals(apfId.intValue(), result.get(apfName)[0].intValue());
        assertTrue(result.containsKey(apf2.getName()));
        assertNotNull(result.get(apf2.getName()));
        assertEquals(1, result.get(apf2.getName())[1].intValue());
        assertEquals(apf2.getId().intValue(), result.get(apf2.getName())[0].intValue());
        Iterator<String> resultIter = result.keySet().iterator();
        assertEquals(apfName, resultIter.next());
        assertEquals(apf2.getName(), resultIter.next());

        AdditionalPathologicFinding normalCode = getNormalDiseaseCode();
        Specimen[] normalSpecs = getValidObjects();
        for (Specimen normalSpec : normalSpecs) {
            normalSpec.setStatus(SpecimenStatus.AVAILABLE);
            normalSpec.setPathologicalCharacteristic(normalCode);
            service.savePersistentObject(normalSpec);
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        result = service.getCountsByPathologicalCharacteristic();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2 + 1, result.size());
        assertTrue(result.containsKey(apfName));
        assertNotNull(result.get(apfName));
        assertEquals(1, result.get(apfName)[1].intValue());
        assertEquals(apfId.intValue(), result.get(apfName)[0].intValue());
        assertTrue(result.containsKey(apf2.getName()));
        assertNotNull(result.get(apf2.getName()));
        assertEquals(1, result.get(apf2.getName())[1].intValue());
        assertEquals(apf2.getId().intValue(), result.get(apf2.getName())[0].intValue());
        assertTrue(result.containsKey(normalCode.getName()));
        assertNotNull(result.get(normalCode.getName()));
        assertEquals(2, result.get(normalCode.getName())[1].intValue());
        assertEquals(normalCode.getId().intValue(), result.get(normalCode.getName())[0].intValue());
        resultIter = result.keySet().iterator();
        assertEquals(normalCode.getName(), resultIter.next());
        assertEquals(apfName, resultIter.next());
        assertEquals(apf2.getName(), resultIter.next());
    }

    /**
     * test the getPathologicalCharacteristic service method.
     */
    @Test
    public void testGetPathologicalCharacteristic() {
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        Map<String, Long> result = service.getPathologicalCharacteristics();
        assertNotNull(result);
        assertTrue(result.isEmpty());
        testSaveRetrieve();
        Specimen specimen = service.getAll(Specimen.class).get(1);
        AdditionalPathologicFinding apf = specimen.getPathologicalCharacteristic();
        String apfName = apf.getName();
        Long apfId = apf.getId();
        result = service.getPathologicalCharacteristics();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertTrue(result.containsKey(apfName));
        assertNotNull(result.get(apfName));
        assertEquals(apfId, result.get(apfName));

        specimen = service.getAll(Specimen.class).get(0);
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        Long specId = service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, specId);
        result = service.getPathologicalCharacteristics();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertTrue(result.containsKey(apfName));
        assertNotNull(result.get(apfName));
        assertEquals(apfId, result.get(apfName));

        AdditionalPathologicFinding apf2 =
            SpecimenPersistenceHelper.createCode(AdditionalPathologicFinding.class, ++codeSuffix);
        specimen.setPathologicalCharacteristic(apf2);
        service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, specId);
        result = service.getPathologicalCharacteristics();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
        assertTrue(result.containsKey(apfName));
        assertNotNull(result.get(apfName));
        assertEquals(apfId, result.get(apfName));
        assertTrue(result.containsKey(apf2.getName()));
        assertNotNull(result.get(apf2.getName()));
        assertEquals(apf2.getId(), result.get(apf2.getName()));
        Iterator<String> resultIter = result.keySet().iterator();
        assertEquals(apf.getName(), resultIter.next());
        assertEquals(apf2.getName(), resultIter.next());

        AdditionalPathologicFinding normalCode = getNormalDiseaseCode();
        Specimen[] normalSpecs = getValidObjects();
        for (Specimen normalSpec : normalSpecs) {
            normalSpec.setStatus(SpecimenStatus.AVAILABLE);
            normalSpec.setPathologicalCharacteristic(normalCode);
            service.savePersistentObject(normalSpec);
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        result = service.getPathologicalCharacteristics();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2 + 1, result.size());
        assertTrue(result.containsKey(apfName));
        assertNotNull(result.get(apfName));
        assertEquals(apfId, result.get(apfName));
        assertTrue(result.containsKey(apf2.getName()));
        assertNotNull(result.get(apf2.getName()));
        assertEquals(apf2.getId(), result.get(apf2.getName()));
        assertTrue(result.containsKey(normalCode.getName()));
        assertNotNull(result.get(normalCode.getName()));
        assertEquals(normalCode.getId(), result.get(normalCode.getName()));
        resultIter = result.keySet().iterator();
        assertEquals(normalCode.getName(), resultIter.next());
        assertEquals(apf.getName(), resultIter.next());
        assertEquals(apf2.getName(), resultIter.next());
    }

    private static AdditionalPathologicFinding getNormalDiseaseCode() {
        CodeServiceLocal codeService = TissueLocatorRegistry.getServiceLocator().getCodeService();
        AdditionalPathologicFinding code = new AdditionalPathologicFinding();
        code.setName(new EmailHelper().getResourceBundle().getString("specimen.search.normal.name"));
        code.setValue(code.getName());
        code.setActive(true);
        code.setUseCount(0L);
        Long codeId = codeService.savePersistentObject(code);
        return codeService.getPersistentObject(AdditionalPathologicFinding.class, codeId);
    }

    /**
     * Test pathologicol characteristic use updates.
     */
    // CHECKSTYLE:OFF - max method length
    @Test
    public void testUpdateAdditionalPathologicFindings() {
    // CHECKSTYLE:ON
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        CodeServiceLocal codeService = TissueLocatorRegistry.getServiceLocator().getCodeService();
        Specimen[] specimens = getValidObjects();
        AdditionalPathologicFinding apf1 = SpecimenPersistenceHelper
            .createCode(AdditionalPathologicFinding.class, codeSuffix++);
        AdditionalPathologicFinding apf2 = SpecimenPersistenceHelper
            .createCode(AdditionalPathologicFinding.class, codeSuffix++);
        apf1.setUseCount(0L);
        apf2.setUseCount(0L);
        long apf1Count = 0;
        long apf2Count = 0;

        Specimen specimen = specimens[0];

        // cycle through possible state changes and test expected results

        // newly available, new apf
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        specimen.setPathologicalCharacteristic(apf1);
        specimen.setPreviousPathologicalCharacteristic(null);
        specimen.setPreviousStatus(null);
        apf1Count++;
        Long id = service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        assertEquals(apf1, specimen.getPathologicalCharacteristic());
        assertEquals(apf1, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());

        // newly unavail, same apf
        specimen.setStatus(SpecimenStatus.UNAVAILABLE);
        assertEquals(SpecimenStatus.AVAILABLE, specimen.getPreviousStatus());
        apf1Count--;
        service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        assertEquals(apf1, specimen.getPathologicalCharacteristic());
        assertEquals(apf1, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());

        // same status unavail, same apf
        assertEquals(SpecimenStatus.UNAVAILABLE, specimen.getStatus());
        assertEquals(SpecimenStatus.UNAVAILABLE, specimen.getPreviousStatus());
        specimen.setExternalId("update");
        service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        assertEquals(apf1, specimen.getPathologicalCharacteristic());
        assertEquals(apf1, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());

        // same status unavail, diff apf
        assertEquals(SpecimenStatus.UNAVAILABLE, specimen.getStatus());
        assertEquals(SpecimenStatus.UNAVAILABLE, specimen.getPreviousStatus());
        specimen.setPathologicalCharacteristic(apf2);
        service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        apf2 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf2.getId());
        assertEquals(apf2, specimen.getPathologicalCharacteristic());
        assertEquals(apf2, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());
        assertEquals(new Long(apf2Count), apf2.getUseCount());

        // newly avail, same apf
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        assertEquals(SpecimenStatus.UNAVAILABLE, specimen.getPreviousStatus());
        apf2Count++;
        service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        apf2 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf2.getId());
        assertEquals(apf2, specimen.getPathologicalCharacteristic());
        assertEquals(apf2, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());
        assertEquals(new Long(apf2Count), apf2.getUseCount());

        // same status avail, same apf
        assertEquals(SpecimenStatus.AVAILABLE, specimen.getStatus());
        assertEquals(SpecimenStatus.AVAILABLE, specimen.getPreviousStatus());
        specimen.setExternalId("update2");
        service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        apf2 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf2.getId());
        assertEquals(apf2, specimen.getPathologicalCharacteristic());
        assertEquals(apf2, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());
        assertEquals(new Long(apf2Count), apf2.getUseCount());

        // same status avail, diff apf
        assertEquals(SpecimenStatus.AVAILABLE, specimen.getStatus());
        assertEquals(SpecimenStatus.AVAILABLE, specimen.getPreviousStatus());
        specimen.setPathologicalCharacteristic(apf1);
        apf1Count++;
        apf2Count--;
        service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        apf2 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf2.getId());
        assertEquals(apf1, specimen.getPathologicalCharacteristic());
        assertEquals(apf1, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());
        assertEquals(new Long(apf2Count), apf2.getUseCount());

        specimen = specimens[1];

        // newly unavailable, new apf
        specimen.setStatus(SpecimenStatus.UNAVAILABLE);
        specimen.setPreviousStatus(null);
        specimen.setPathologicalCharacteristic(apf1);
        specimen.setPreviousPathologicalCharacteristic(null);
        id = service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        apf2 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf2.getId());
        assertEquals(apf1, specimen.getPathologicalCharacteristic());
        assertEquals(apf1, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());
        assertEquals(new Long(apf2Count), apf2.getUseCount());

        // newly avail, diff apf
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        assertEquals(SpecimenStatus.UNAVAILABLE, specimen.getPreviousStatus());
        specimen.setPathologicalCharacteristic(apf2);
        apf2Count++;
        id = service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        apf2 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf2.getId());
        assertEquals(apf2, specimen.getPathologicalCharacteristic());
        assertEquals(apf2, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());
        assertEquals(new Long(apf2Count), apf2.getUseCount());

        // newly unavail, diff apf
        specimen.setStatus(SpecimenStatus.UNAVAILABLE);
        assertEquals(SpecimenStatus.AVAILABLE, specimen.getPreviousStatus());
        specimen.setPathologicalCharacteristic(apf1);
        apf2Count--;
        id = service.savePersistentObject(specimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        apf2 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf2.getId());
        assertEquals(apf1, specimen.getPathologicalCharacteristic());
        assertEquals(apf1, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());
        assertEquals(new Long(apf2Count), apf2.getUseCount());


        // invalid specimen does not update use count
        specimen.setStatus(SpecimenStatus.AVAILABLE);
        specimen.setPathologicalCharacteristic(apf2);
        specimen.setExternalId(null);
        try {
            service.savePersistentObject(specimen);
            fail("Exception expected");
        } catch (PropertyValueException e) {
            // expected
        }
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        specimen = service.getPersistentObject(Specimen.class, id);
        apf1 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf1.getId());
        apf2 = codeService.getPersistentObject(AdditionalPathologicFinding.class, apf2.getId());
        assertEquals(apf1, specimen.getPathologicalCharacteristic());
        assertEquals(apf1, specimen.getPreviousPathologicalCharacteristic());
        assertEquals(new Long(apf1Count), apf1.getUseCount());
        assertEquals(new Long(apf2Count), apf2.getUseCount());
    }

    /**
     * tests the getInstitutions service method.
     */
    @Test
    public void testGetInstitutions() {
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        Map<String, Long> result = service.getInstitutions();
        assertNotNull(result);
        assertTrue(result.isEmpty());
        testSaveRetrieve();
        Institution institution = service.getAll(Specimen.class).get(0).getExternalIdAssigner();
        result = service.getInstitutions();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertTrue(result.containsKey(institution.getName()));
        assertNotNull(result.get(institution.getName()));
        assertEquals(institution.getId(), result.get(institution.getName()));
    }

    /**
     * test the mapping between request statuses and votes.
     */
    @Test
    public void testVoteStatusMapping() {
        assertEquals(RequestStatus.APPROVED, Vote.APPROVE.getStatus());
        assertEquals(RequestStatus.DENIED, Vote.DENY.getStatus());
        assertEquals(RequestStatus.PENDING_REVISION, Vote.REVISE.getStatus());
        assertEquals(Vote.APPROVE, RequestStatus.getVote(RequestStatus.APPROVED));
        assertEquals(Vote.DENY, RequestStatus.getVote(RequestStatus.DENIED));
        assertEquals(Vote.REVISE, RequestStatus.getVote(RequestStatus.PENDING_REVISION));
        assertNull(RequestStatus.getVote(RequestStatus.PARTIALLY_APPROVED));
        assertNull(RequestStatus.getVote(RequestStatus.PENDING));
        assertNull(RequestStatus.getVote(RequestStatus.PENDING_FINAL_DECISION));
    }

    /**
     * test available quantity validation.
     */
    @Test
    public void testValidQuantity() {
        Specimen s = getValidObjects()[0];
        assertTrue(s.isValidQuantity());
        s.setAvailableQuantity(null);
        assertFalse(s.isValidQuantity());
        s.setAvailableQuantity(new BigDecimal("2.0"));
        s.setAvailableQuantityUnits(null);
        assertFalse(s.isValidQuantity());
        s.setAvailableQuantity(null);
        assertTrue(s.isValidQuantity());
    }

    /**
     * Test consent withdrawn date validation.
     */
    @Test
    public void testValidConsentWithdrawnDate() {
        Specimen s = getValidObjects()[0];
        assertTrue(s.isConsentWithdrawnDateValid());
        Long id = 0L;
        try {
            id = getService().savePersistentObject(s);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
        } catch (Exception e) {
            fail("Specimen consent withdrawn is valid.");
        }
        s = getService().getPersistentObject(Specimen.class, id);
        s.setConsentWithdrawnDate(new Date());
        assertFalse(s.isConsentWithdrawnDateValid());
        try {
            getService().savePersistentObject(s);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
            fail("Invalid date.");
        } catch (Exception e) {
            // expected
        }
        s = getService().getPersistentObject(Specimen.class, id);
        s.setConsentWithdrawn(true);
        assertTrue(s.isConsentWithdrawnDateValid());
        try {
            getService().savePersistentObject(s);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
        } catch (Exception e) {
            fail("Specimen consent withdrawn is valid.");
        }
        s = getService().getPersistentObject(Specimen.class, id);
        s.setConsentWithdrawnDate(null);
        assertFalse(s.isConsentWithdrawnDateValid());
        try {
            getService().savePersistentObject(s);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
            fail("Invalid date.");
        } catch (Exception e) {
            // expected
        }
        s = getService().getPersistentObject(Specimen.class, id);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 1);
        s.setConsentWithdrawnDate(cal.getTime());
        try {
            getService().savePersistentObject(s);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
            fail("Invalid date.");
        } catch (Exception e) {
            // expected
        }
    }

    /**
     * Test is consent can be withdrawn.
     */
    @Test
    public void testIsConsentWithdrawable() {
        Specimen s = getValidObjects()[0];
        List<SpecimenStatus> invalid = Arrays.asList(new SpecimenStatus[]{SpecimenStatus.DESTROYED});
        for (SpecimenStatus status : SpecimenStatus.values()) {
            s.setStatus(status);
            assertNotSame(invalid.contains(status), s.isConsentWithdrawable());
        }
    }

    /**
     * Test the withdrawal of consent for a participant.
     * @throws Exception on error.
     */
    @Test
    public void testWithdrawConsentForParticipant() throws Exception {
        // Destroyed specimens should not be updated
        Specimen[] specimens = getValidObjects();
        specimens[0].setStatus(SpecimenStatus.DESTROYED);
        specimens[1].setStatus(SpecimenStatus.DESTROYED);
        assertFalse(specimens[0].isConsentWithdrawn());
        assertFalse(specimens[1].isConsentWithdrawn());
        assertNull(specimens[0].getConsentWithdrawnDate());
        assertNull(specimens[1].getConsentWithdrawnDate());
        Long id1 = getService().savePersistentObject(specimens[0]);
        Long id2 = getService().savePersistentObject(specimens[1]);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        Specimen s1 = getService().getPersistentObject(Specimen.class, id1);
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        service.withdrawConsentForParticipant(s1.getParticipant());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        Specimen s2 = getService().getPersistentObject(Specimen.class, id2);
        assertFalse(s1.isConsentWithdrawn());
        assertFalse(s2.isConsentWithdrawn());
        assertNull(s1.getConsentWithdrawnDate());
        assertNull(s2.getConsentWithdrawnDate());

        // only specimens not previously withdrawn should be updated
        specimens = getValidObjects();
        s1 = specimens[0];
        s2 = specimens[1];
        s1.setStatus(SpecimenStatus.AVAILABLE);
        s2.setStatus(SpecimenStatus.UNAVAILABLE);
        s2.setConsentWithdrawn(true);
        s2.setConsentWithdrawnDate(new Date());
        Date oldDate = s2.getConsentWithdrawnDate();
        id1 = getService().savePersistentObject(s1);
        id2 = getService().savePersistentObject(s2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        s2 = getService().getPersistentObject(Specimen.class, id2);
        service.withdrawConsentForParticipant(s1.getParticipant());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        s2 = getService().getPersistentObject(Specimen.class, id2);
        assertTrue(s1.isConsentWithdrawn());
        assertTrue(s2.isConsentWithdrawn());
        assertEquals(SpecimenStatus.UNAVAILABLE, s1.getStatus());
        assertEquals(SpecimenStatus.UNAVAILABLE, s2.getStatus());
        assertNotNull(s1.getConsentWithdrawnDate());
        assertEquals(oldDate, s2.getConsentWithdrawnDate());

        //participant with no specimens
        service.withdrawConsentForParticipant(SpecimenPersistenceHelper
                .createAndStoreParticipant(s1.getExternalIdAssigner(), ++codeSuffix));

        // both should be updated, shipped status should remain
        SpecimenRequest[] requests = new SpecimenRequestPersistenceTest().getValidObjects();
        List<SpecimenRequestLineItem> lineItems = new ArrayList<SpecimenRequestLineItem>();
        lineItems.addAll(requests[0].getLineItems());
        s1 = lineItems.get(0).getSpecimen();
        s2 = lineItems.get(1).getSpecimen();
        s1.setStatus(SpecimenStatus.AVAILABLE);
        s2.setStatus(SpecimenStatus.AVAILABLE);
        id1 = getService().savePersistentObject(s1);
        id2 = getService().savePersistentObject(s2);
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        createUserInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER, s1.getExternalIdAssigner());
        SpecimenRequestServiceLocal requestService = TissueLocatorRegistry.getServiceLocator()
            .getSpecimenRequestService();
        UsernameHolder.setUser(requests[0].getRequestor().getUsername());
        requestService.saveRequest(requests[0], 0, 0, 0, 0);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        s2 = getService().getPersistentObject(Specimen.class, id2);
        s2.setStatus(SpecimenStatus.PENDING_SHIPMENT);
        getService().savePersistentObject(s2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s2 = getService().getPersistentObject(Specimen.class, id2);
        s2.setStatus(SpecimenStatus.SHIPPED);
        getService().savePersistentObject(s2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        service.withdrawConsentForParticipant(s1.getParticipant());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        s2 = getService().getPersistentObject(Specimen.class, id2);
        assertTrue(s1.isConsentWithdrawn());
        assertTrue(s2.isConsentWithdrawn());
        assertEquals(SpecimenStatus.UNAVAILABLE, s1.getStatus());
        assertEquals(SpecimenStatus.SHIPPED, s2.getStatus());
        assertNotNull(s1.getConsentWithdrawnDate());
        assertNotNull(s2.getConsentWithdrawnDate());
    }

    /**
     * Test specimen consent withdrawal.
     * @throws Exception on error.
     */
    @Test
    public void testWithdrawConsentForSpecimen() throws Exception {
        Specimen[] specimens = getValidObjects();
        specimens[0].setStatus(SpecimenStatus.AVAILABLE);
        Long id1 = getService().savePersistentObject(specimens[0]);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        Specimen s1 = getService().getPersistentObject(Specimen.class, id1);
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        service.withdrawConsent(s1);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        assertTrue(s1.isConsentWithdrawn());
        assertNotNull(s1.getConsentWithdrawnDate());
        assertEquals(SpecimenStatus.UNAVAILABLE, s1.getStatus());

        specimens[1].setStatus(SpecimenStatus.DESTROYED);
        Long id2 = getService().savePersistentObject(specimens[1]);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        Specimen s2 = getService().getPersistentObject(Specimen.class, id2);
        assertFalse(s2.isConsentWithdrawn());
        assertNull(s2.getConsentWithdrawnDate());

        Date oldDate = s1.getConsentWithdrawnDate();
        try {
            service.withdrawConsent(s1);
        } catch (IllegalStateException e) {
            // do nothing, expected
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        assertEquals(oldDate, s1.getConsentWithdrawnDate());

        SpecimenRequest[] requests = new SpecimenRequestPersistenceTest().getValidObjects();
        List<SpecimenRequestLineItem> lineItems = new ArrayList<SpecimenRequestLineItem>();
        lineItems.addAll(requests[0].getLineItems());
        s1 = lineItems.get(0).getSpecimen();
        s2 = lineItems.get(1).getSpecimen();
        s1.setStatus(SpecimenStatus.AVAILABLE);
        s2.setStatus(SpecimenStatus.AVAILABLE);
        id1 = getService().savePersistentObject(s1);
        id2 = getService().savePersistentObject(s2);
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.LINE_ITEM_REVIEW);
        createUserInRoles(s1.getExternalIdAssigner(), Role.LINE_ITEM_REVIEW_VOTER, Role.TISSUE_REQUEST_ADMIN);
        createUserInRole(Role.SHIPMENT_ADMIN, s1.getExternalIdAssigner());
        SpecimenRequestServiceLocal requestService = TissueLocatorRegistry.getServiceLocator()
            .getSpecimenRequestService();
        UsernameHolder.setUser(requests[0].getRequestor().getUsername());
        Long reqId = requestService.saveRequest(requests[0], 0, 0, 0, 0);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        SpecimenRequest request = getService().getPersistentObject(SpecimenRequest.class, reqId);

        s1 = getService().getPersistentObject(Specimen.class, id1);
        s2 = getService().getPersistentObject(Specimen.class, id2);
        Shipment order = new Shipment();
        order.setStatus(ShipmentStatus.PENDING);
        order.setRequest(request);
        order.setSender(request.getRequestor());
        order.setSendingInstitution(s2.getExternalIdAssigner());
        order.setShippingMethod(ShippingMethod.OTHER);
        order.setStatus(ShipmentStatus.PENDING);
        order.setTrackingNumber("1234");
        order.setShippingPrice(new BigDecimal("12.34"));
        order.setFees(new BigDecimal("12.34"));
        order.setUpdatedDate(new Date());
        order.setRecipient(request.getShipment().getRecipient());
        SpecimenRequestLineItem li = new SpecimenRequestLineItem();
        li.setSpecimen(s2);
        li.setQuantity(new BigDecimal("1.0"));
        li.setQuantityUnits(QuantityUnits.COUNT);
        order.getLineItems().add(li);
        TissueLocatorHibernateUtil.getCurrentSession().save(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        s2 = getService().getPersistentObject(Specimen.class, id2);
        s2.setStatus(SpecimenStatus.PENDING_SHIPMENT);
        getService().savePersistentObject(s2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        service.withdrawConsentForParticipant(s1.getParticipant());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        s2 = getService().getPersistentObject(Specimen.class, id2);
        assertTrue(s1.isConsentWithdrawn());
        assertTrue(s2.isConsentWithdrawn());
        assertEquals(SpecimenStatus.UNAVAILABLE, s1.getStatus());
        assertEquals(SpecimenStatus.UNAVAILABLE, s2.getStatus());
        assertNotNull(s1.getConsentWithdrawnDate());
        assertNotNull(s2.getConsentWithdrawnDate());
    }

    /**
     * Tests the return to source institution method.
     */
    @Test
    public void testReturnToSourceInstitution() {
        Specimen[] specimens = getValidObjects();
        specimens[0].setStatus(SpecimenStatus.SHIPPED);
        Long id1 = getService().savePersistentObject(specimens[0]);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        Specimen s1 = getService().getPersistentObject(Specimen.class, id1);
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        service.returnToSourceInstitution(s1);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        assertEquals(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION, s1.getStatus());

        s1.setStatus(SpecimenStatus.RETURNED_TO_PARTICIPANT);
        getService().savePersistentObject(s1);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        try {
            service.returnToSourceInstitution(s1);
        } catch (IllegalStateException e) {
            // expected
        }
        assertEquals(SpecimenStatus.RETURNED_TO_PARTICIPANT, s1.getStatus());

        s1.setPreviousStatus(SpecimenStatus.SHIPPED);
        s1.setStatus(SpecimenStatus.DESTROYED);
        getService().savePersistentObject(s1);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s1 = getService().getPersistentObject(Specimen.class, id1);
        try {
            service.returnToSourceInstitution(s1);
        } catch (IllegalStateException e) {
            // expected
        }
        assertEquals(SpecimenStatus.DESTROYED, s1.getStatus());
    }

    /**
     * Test specimen status transition handling.
     */
    @Test
    public void testStatusTransitionHistory() {
        Specimen[] specimens = getValidObjects();
        Specimen s = specimens[0];
        int statusCount = -1;
        s.setStatus(SpecimenStatus.AVAILABLE);
        Long id = getService().savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s = getService().getPersistentObject(Specimen.class, id);
        assertEquals(++statusCount, s.getStatusTransitionHistory().size());

        s.setStatus(SpecimenStatus.PENDING_SHIPMENT);
        getService().savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s = getService().getPersistentObject(Specimen.class, id);
        assertEquals(++statusCount, s.getStatusTransitionHistory().size());
        SpecimenStatusTransition t = s.getStatusTransitionHistory().get(statusCount - 1);
        t = s.getStatusTransitionHistory().get(statusCount - 1);
        assertEquals(SpecimenStatus.PENDING_SHIPMENT, t.getNewStatus());
        assertEquals(SpecimenStatus.AVAILABLE, t.getPreviousStatus());
        assertNotNull(t.getTransitionDate());
        assertNotNull(t.getSystemTransitionDate());

        s.setStatus(SpecimenStatus.SHIPPED);
        Date date = new Date();
        s.setStatusTransitionDate(date);
        getService().savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s = getService().getPersistentObject(Specimen.class, id);
        assertEquals(++statusCount, s.getStatusTransitionHistory().size());
        t = s.getStatusTransitionHistory().get(statusCount - 1);
        assertEquals(SpecimenStatus.SHIPPED, t.getNewStatus());
        assertEquals(SpecimenStatus.PENDING_SHIPMENT, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());
        assertNotNull(t.getSystemTransitionDate());
        assertNotSame(date, t.getSystemTransitionDate());

        // Edit a previously saved date
        date = new Date();
        t.setTransitionDate(date);
        getService().savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s = getService().getPersistentObject(Specimen.class, id);
        assertEquals(statusCount, s.getStatusTransitionHistory().size());
        assertEquals(SpecimenStatus.SHIPPED, t.getNewStatus());
        assertEquals(SpecimenStatus.PENDING_SHIPMENT, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());

        s.setExternalId("new_external_id");
        getService().savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s = getService().getPersistentObject(Specimen.class, id);
        assertEquals(statusCount, s.getStatusTransitionHistory().size());
        assertEquals(SpecimenStatus.SHIPPED, t.getNewStatus());
        assertEquals(SpecimenStatus.PENDING_SHIPMENT, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());
    }

    /**
     * test for custom properties.
     */
    @Test
    public void testCustomProperties() {
        Specimen specimen = new Specimen();
        assertNotNull(specimen.getCustomProperties());
        String field = "field";
        assertNull(specimen.getCustomProperty(field));
        specimen.setCustomProperty(field, "value");
        assertNotNull(specimen.getCustomProperty(field));
        specimen.setCustomProperties(null);
        assertNull(specimen.getCustomProperties());
     }

    /**
     * test xml marshaling of a specimen.
     * @throws JAXBException on error
     */
    @Test
    public void testXmlMarshaling() throws JAXBException {
        Specimen specimen = getValidObjects()[1];
        specimen.setExternalId("ABRC-6789");

        CollectionProtocol protocol = specimen.getProtocol();
        protocol.setName("Test Name");
        protocol.setStartDate(null);
        protocol.setEndDate(null);
        specimen.setProtocol(protocol);

        Participant participant = specimen.getParticipant();
        participant.setExternalId("External Id 123");
        specimen.setParticipant(participant);

        SpecimenType specimenType = specimen.getSpecimenType();
        specimenType.setName("SpecimenType-active");
        specimen.setSpecimenType(specimenType);

        AdditionalPathologicFinding finding = specimen.getPathologicalCharacteristic();
        finding.setName("AdditionalPathologicFinding-active");
        specimen.setPathologicalCharacteristic(finding);

        specimen.setCustomProperty("collectionType", "Autopsy");

        JAXBContext context = JAXBContext.newInstance(Specimen.class);
        Marshaller marshaller = context.createMarshaller();
        StringWriter writer = new StringWriter();
        marshaller.marshal(specimen, writer);
        assertEquals(writer.toString().trim(), getSpecimenXml(specimen.getCreatedDate()).trim());
}

    /**
     * test xml unmarshaling of a specimen.
     * @throws JAXBException on error
     */
    @Test
    public void textXmlUnmarshaling() throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Specimen.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        Specimen specimen = (Specimen) unmarshaller
            .unmarshal(new StreamSource(new StringReader(getSpecimenXml(new Date()))));
        String collectionType = (String) specimen.getCustomProperty("collectionType");
        assertEquals(collectionType, "Autopsy");
    }

    private String getSpecimenXml(Date createdDate) {
        StringBuilder dateString =  new StringBuilder(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ",
                Locale.getDefault()).format(createdDate));
        dateString.insert(dateString.length() - 2, ':');
        StringBuilder sb = new StringBuilder()
        .append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>")
        .append("<specimen>")
        .append("<externalId>ABRC-6789</externalId>")
        .append("<patientAgeAtCollection>2</patientAgeAtCollection>")
        .append("<patientAgeAtCollectionUnits>MONTHS</patientAgeAtCollectionUnits>")
        .append("<protocol>")
        .append("<name>Test Name</name>")
        .append("</protocol>")
        .append("<participant>")
        .append("<ethnicity>HISPANIC_OR_LATINO</ethnicity>")
        .append("<race>WHITE</race>")
        .append("<race>BLACK_OR_AFRICAN_AMERICAN</race>")
        .append("<gender>FEMALE</gender>")
        .append("<externalId>External Id 123</externalId>")
        .append("</participant>")
        .append("<pathologicalCharacteristic>")
        .append("<name>AdditionalPathologicFinding-active</name>")
        .append("<active>true</active>")
        .append("</pathologicalCharacteristic>")
        .append("<specimenType>")
        .append("<name>SpecimenType-active</name>")
        .append("<active>true</active>")
        .append("<specimenClass>TISSUE</specimenClass>")
        .append("</specimenType>")
        .append("<priceNegotiable>true</priceNegotiable>")
        .append("<createdDate>" + dateString.toString() + "</createdDate>")
        .append("<status>AVAILABLE</status>")
        .append("<consentWithdrawn>false</consentWithdrawn>")
        .append("<customProperties>")
        .append("<customProperty name=\"collectionType\">Autopsy</customProperty>")
        .append("</customProperties>")
        .append("</specimen>");
        return sb.toString();
    }

    /**
     * Test the get top requested specimen types method.
     * @throws Exception on error
     */
    @Test
    public void testGetTopRequestedSpecimenTypes() throws Exception {
        Date today = new Date();
        Date tomorrow = DateUtils.addDays(new Date(), 1);
        Date future = DateUtils.addDays(new Date(), 2);
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        List<NameCountPair> result = service.getTopRequestedSpecimenTypes(today, future);
        assertNotNull(result);
        assertTrue(result.isEmpty());
        SpecimenRequest request = new SpecimenRequestPersistenceTest().getValidObjects()[0];
        request.setRequestedDate(tomorrow);
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            lineItem.getSpecimen().setStatus(SpecimenStatus.AVAILABLE);
        }
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        createUserInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER,
                request.getLineItems().iterator().next().getSpecimen().getExternalIdAssigner());
        UsernameHolder.setUser(request.getRequestor().getUsername());
        SpecimenRequestServiceLocal requestService = TissueLocatorRegistry.getServiceLocator()
            .getSpecimenRequestService();
        Long id = requestService.savePersistentObject(request);
        request = requestService.getPersistentObject(SpecimenRequest.class, id);
        result = service.getTopRequestedSpecimenTypes(today, future);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertEquals(2, result.get(0).getCount().intValue());
        Specimen[] newSpecs = getValidObjects();
        newSpecs[0].setSpecimenType(request.getLineItems().iterator().next().getSpecimen().getSpecimenType());
        for (Specimen s : newSpecs) {
            service.savePersistentObject(s);
            SpecimenRequestLineItem lineItem = new SpecimenRequestLineItem();
            lineItem.setNote("test note");
            lineItem.setQuantity(new BigDecimal(1));
            lineItem.setQuantityUnits(QuantityUnits.ML);
            lineItem.setSpecimen(s);
            request.getLineItems().add(lineItem);
        }
        requestService.savePersistentObject(request);
        request = requestService.getPersistentObject(SpecimenRequest.class, id);
        result = service.getTopRequestedSpecimenTypes(today, future);
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
        assertEquals(2 + 1, result.get(0).getCount().intValue());
        assertEquals(1, result.get(1).getCount().intValue());
        for (int i = 0; i <= 2; i++) {
            newSpecs = getValidObjects();
            for (Specimen s : newSpecs) {
                service.savePersistentObject(s);
                SpecimenRequestLineItem lineItem = new SpecimenRequestLineItem();
                lineItem.setNote("test note");
                lineItem.setQuantity(new BigDecimal(1));
                lineItem.setQuantityUnits(QuantityUnits.ML);
                lineItem.setSpecimen(s);
                request.getLineItems().add(lineItem);
            }
        }
        requestService.savePersistentObject(request);
        request = requestService.getPersistentObject(SpecimenRequest.class, id);
        result = service.getTopRequestedSpecimenTypes(today, future);
        verifyCounts(result);
        result = service.getTopRequestedSpecimenTypes(today, tomorrow);
        verifyCounts(result);
        result = service.getTopRequestedSpecimenTypes(tomorrow, future);
        verifyCounts(result);
        result = service.getTopRequestedSpecimenTypes(tomorrow, tomorrow);
        verifyCounts(result);
        result = service.getTopRequestedSpecimenTypes(today, today);
        assertTrue(result.isEmpty());
        result = service.getTopRequestedSpecimenTypes(future, future);
        assertTrue(result.isEmpty());
    }

    private void verifyCounts(List<NameCountPair> result) {
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2 + 1, result.size());
        assertEquals(2 + 1, result.get(0).getCount().intValue());
        assertEquals(2, result.get(1).getCount().intValue());
        assertEquals(2, result.get(2).getCount().intValue());
    }

    /**
     * Test last updated date after specimen values have been changed and persisted.
     *
     * @throws InterruptedException if the current thread is interrupted
     */
    @Test
    public void testLastUpdatedDate() throws InterruptedException {
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        Long id = service.savePersistentObject(getValidObjects()[0]);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        Specimen s = getService().getPersistentObject(Specimen.class, id);
        assertEquals(s.getCreatedDate(), s.getLastUpdatedDate());
        s.setMaximumPrice(s.getMinimumPrice());
        // CHECKSTYLE:OFF
        Thread.sleep(1000);
        // CHECKSTYLE:ON
        service.savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s = getService().getPersistentObject(Specimen.class, id);
        assertTrue(s.getLastUpdatedDate().after(s.getCreatedDate()));
    }

    /**
     * Test return request counts by participant.
     */
    // CHECKSTYLE:OFF - max method length
    @Test
    public void testReturnRequestDataByParticipant() {
    // CHECKSTYLE:ON
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        Calendar start = Calendar.getInstance();
        start.add(Calendar.DATE, -1);
        Calendar end = Calendar.getInstance();
        assertTrue(service.getReturnRequestDataByParticipant(start.getTime(), end.getTime()).isEmpty());

        // no return requests
        Specimen[] specimens = getValidObjects();
        Long id1 = service.savePersistentObject(specimens[0]);
        specimens[1].setStatus(SpecimenStatus.UNAVAILABLE);
        Long id2 = service.savePersistentObject(specimens[1]);
        assertTrue(service.getReturnRequestDataByParticipant(start.getTime(), end.getTime()).isEmpty());

        // one return request within range
        Specimen s = getService().getPersistentObject(Specimen.class, id1);
        s.setConsentWithdrawn(true);
        s.setConsentWithdrawnDate(new Date());
        service.savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        end = Calendar.getInstance();
        Map<Long, ParticipantReturnRequestData> counts =
            service.getReturnRequestDataByParticipant(start.getTime(), end.getTime());
        assertEquals(1, counts.size());
        Participant p1 = s.getParticipant();
        assertTrue(counts.containsKey(p1.getId()));
        ParticipantReturnRequestData data = counts.get(p1.getId());
        assertEquals(1, data.getReturnsRequested());
        assertEquals(1, data.getUnfulfilledReturnRequests());
        assertEquals(0, data.getReturnsFulfilled());
        assertEquals(p1.getExternalId(), data.getExternalId());
        assertEquals(p1.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());

        // fulfill request after date range, should still count as unfulfilled
        s = getService().getPersistentObject(Specimen.class, id1);
        s.setStatus(SpecimenStatus.RETURNED_TO_PARTICIPANT);
        Calendar transitionDate = Calendar.getInstance();
        transitionDate.add(Calendar.DATE, 1);
        s.setStatusTransitionDate(transitionDate.getTime());
        service.savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        end = Calendar.getInstance();
        counts =
            service.getReturnRequestDataByParticipant(start.getTime(), end.getTime());
        assertEquals(1, counts.size());
        assertTrue(counts.containsKey(p1.getId()));
        data = counts.get(p1.getId());
        assertEquals(1, data.getReturnsRequested());
        assertEquals(1, data.getUnfulfilledReturnRequests());
        assertEquals(0, data.getReturnsFulfilled());
        assertEquals(p1.getExternalId(), data.getExternalId());

        // fulfill request within date range, should count as fulfilled
        s = getService().getPersistentObject(Specimen.class, id1);
        s.getStatusTransitionHistory().get(s.getStatusTransitionHistory().size() - 1)
            .setTransitionDate(new Date());
        service.savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        end = Calendar.getInstance();
        counts =
            service.getReturnRequestDataByParticipant(start.getTime(), end.getTime());
        assertEquals(1, counts.size());
        assertTrue(counts.containsKey(p1.getId()));
        data = counts.get(p1.getId());
        assertEquals(1, data.getReturnsRequested());
        assertEquals(0, data.getUnfulfilledReturnRequests());
        assertEquals(1, data.getReturnsFulfilled());
        assertEquals(p1.getExternalId(), data.getExternalId());
        assertEquals(p1.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());

        // requests prior to date range are not counted in return requests
        s = getService().getPersistentObject(Specimen.class, id2);
        s.setConsentWithdrawn(true);
        Calendar withdrawnDate = Calendar.getInstance();
        // CHECKSTYLE:OFF
        withdrawnDate.add(Calendar.DATE, -2);
        // CHECKSTYLE:ON
        s.setConsentWithdrawnDate(withdrawnDate.getTime());
        service.savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        end = Calendar.getInstance();
        counts =
            service.getReturnRequestDataByParticipant(start.getTime(), end.getTime());
        assertEquals(1, counts.size());
        p1 = s.getParticipant();
        assertTrue(counts.containsKey(p1.getId()));
        data = counts.get(p1.getId());
        assertEquals(1, data.getReturnsRequested());
        // this return request was unfulfilled by the end date, so it should be counted
        assertEquals(1, data.getUnfulfilledReturnRequests());
        assertEquals(1, data.getReturnsFulfilled());
        assertEquals(p1.getExternalId(), data.getExternalId());
        assertEquals(p1.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());

        // multiple requests for participant are counted
        start.add(Calendar.DATE, -1);
        end = Calendar.getInstance();
        counts =
            service.getReturnRequestDataByParticipant(start.getTime(), end.getTime());
        assertEquals(1, counts.size());
        p1 = s.getParticipant();
        assertTrue(counts.containsKey(p1.getId()));
        data = counts.get(p1.getId());
        assertEquals(2, data.getReturnsRequested());
        assertEquals(1, data.getUnfulfilledReturnRequests());
        assertEquals(1, data.getReturnsFulfilled());
        assertEquals(p1.getExternalId(), data.getExternalId());
        assertEquals(p1.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());

        // multiple fulfilled requests for participant are counted
        s = getService().getPersistentObject(Specimen.class, id2);
        s.setStatus(SpecimenStatus.RETURNED_TO_PARTICIPANT);
        s.setStatusTransitionDate(new Date());
        service.savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        end = Calendar.getInstance();
        counts =
            service.getReturnRequestDataByParticipant(start.getTime(), end.getTime());
        assertEquals(1, counts.size());
        p1 = s.getParticipant();
        assertTrue(counts.containsKey(p1.getId()));
        data = counts.get(p1.getId());
        assertEquals(2, data.getReturnsRequested());
        assertEquals(0, data.getUnfulfilledReturnRequests());
        assertEquals(2, data.getReturnsFulfilled());
        assertEquals(p1.getExternalId(), data.getExternalId());
        assertEquals(p1.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());

        // add another participant
        specimens = getValidObjects();
        specimens[0].setConsentWithdrawn(true);
        specimens[0].setConsentWithdrawnDate(new Date());
        id1 = service.savePersistentObject(specimens[0]);
        id2 = service.savePersistentObject(specimens[1]);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        s = getService().getPersistentObject(Specimen.class, id1);
        end = Calendar.getInstance();
        counts =
            service.getReturnRequestDataByParticipant(start.getTime(), end.getTime());
        assertEquals(2, counts.size());
        // original counts are still correct
        data = counts.get(p1.getId());
        assertEquals(2, data.getReturnsRequested());
        assertEquals(0, data.getUnfulfilledReturnRequests());
        assertEquals(2, data.getReturnsFulfilled());
        assertEquals(p1.getExternalId(), data.getExternalId());
        assertEquals(p1.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());
        Participant p2 = s.getParticipant();
        // new counts are correct
        assertTrue(counts.containsKey(p2.getId()));
        data = counts.get(p2.getId());
        assertEquals(1, data.getReturnsRequested());
        assertEquals(1, data.getUnfulfilledReturnRequests());
        assertEquals(0, data.getReturnsFulfilled());
        assertEquals(p2.getExternalId(), data.getExternalId());
        assertEquals(p2.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());

        // request for p2 is out of range, but unfulfilled
        withdrawnDate.add(Calendar.DATE, -1);
        s.setConsentWithdrawnDate(withdrawnDate.getTime());
        service.savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        end = Calendar.getInstance();
        counts =
            service.getReturnRequestDataByParticipant(start.getTime(), end.getTime());
        assertEquals(2, counts.size());
        // original counts are still correct
        data = counts.get(p1.getId());
        assertEquals(2, data.getReturnsRequested());
        assertEquals(0, data.getUnfulfilledReturnRequests());
        assertEquals(2, data.getReturnsFulfilled());
        assertEquals(p1.getExternalId(), data.getExternalId());
        assertEquals(p1.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());
        // new counts are correct
        assertTrue(counts.containsKey(p2.getId()));
        data = counts.get(p2.getId());
        assertEquals(0, data.getReturnsRequested());
        assertEquals(1, data.getUnfulfilledReturnRequests());
        assertEquals(0, data.getReturnsFulfilled());
        assertEquals(p2.getExternalId(), data.getExternalId());
        assertEquals(p2.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());

        // request for p2 is fulfilled
        s = getService().getPersistentObject(Specimen.class, id1);
        s.setStatus(SpecimenStatus.RETURNED_TO_PARTICIPANT);
        s.setStatusTransitionDate(new Date());
        service.savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        end = Calendar.getInstance();
        counts =
            service.getReturnRequestDataByParticipant(start.getTime(), end.getTime());
        assertEquals(2, counts.size());
        // original counts are still correct
        data = counts.get(p1.getId());
        assertEquals(2, data.getReturnsRequested());
        assertEquals(0, data.getUnfulfilledReturnRequests());
        assertEquals(p1.getExternalId(), data.getExternalId());
        assertEquals(p1.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());
        // new counts are correct
        assertTrue(counts.containsKey(p2.getId()));
        data = counts.get(p2.getId());
        assertEquals(0, data.getReturnsRequested());
        assertEquals(0, data.getUnfulfilledReturnRequests());
        assertEquals(1, data.getReturnsFulfilled());
        assertEquals(p2.getExternalId(), data.getExternalId());
        assertEquals(p2.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());

        // request for p2 is fulfilled prior to date range, so no data
        s = getService().getPersistentObject(Specimen.class, id1);
        transitionDate = Calendar.getInstance();
        // CHECKSTYLE:OFF
        transitionDate.add(Calendar.DATE, -3);
        // CHECKSTYLE:ON
        s.getStatusTransitionHistory().get(s.getStatusTransitionHistory().size() - 1)
            .setTransitionDate(transitionDate.getTime());
        service.savePersistentObject(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        end = Calendar.getInstance();
        counts =
            service.getReturnRequestDataByParticipant(start.getTime(), end.getTime());
        assertEquals(1, counts.size());
        // original counts are still correct
        data = counts.get(p1.getId());
        assertEquals(2, data.getReturnsRequested());
        assertEquals(0, data.getUnfulfilledReturnRequests());
        assertEquals(p1.getExternalId(), data.getExternalId());
        assertEquals(p1.getExternalIdAssigner().getId(), data.getExternalIdAssignerId());

        // date range prior to return requests
        // CHECKSTYLE:OFF
        start.add(Calendar.DATE, -5);
        end.add(Calendar.DATE, -4);
        // CHECKSTYLE:ON
        assertTrue(service.getReturnRequestDataByParticipant(start.getTime(), end.getTime()).isEmpty());
    }

}