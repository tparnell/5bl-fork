/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.config.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.category.DisplayOption;
import com.fiveamsolutions.tissuelocator.data.config.category.SearchResultFieldDisplaySetting;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.preferences.SearchResultFieldDisplayUserPreference;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author cgoina
 *
 */
public class SearchResultDisplaySettingsServiceTest extends AbstractHibernateTestCase {

    private static final int NUMBER_OF_DEFAULT_FIELDS_FOR_TESTING = 5;
    private static final int NUMBER_OF_USER_FIELDS_FOR_TESTING = 3;

    /**
     * search result field display setting comparator.
     * @author cgoina
     *
     */
    private static class SearchResultFieldDisplaySettingComparator implements
            Comparator<SearchResultFieldDisplaySetting> {

        /**
         * The precondition for this comparator is that the id is set.
         *
         * {@inheritDoc}
         */
        @Override
        public int compare(SearchResultFieldDisplaySetting f1, SearchResultFieldDisplaySetting f2) {
            return f1.getId().compareTo(f2.getId());
        }

    }

    private TissueLocatorUser testUser;
    private SearchResultDisplaySettingsServiceLocal searchDisplaySettingService;
    private List<SearchResultFieldDisplaySetting> testDefaultSettings;

    /**
     * Set up test.
     */
    @Before
    public void setUpTest() {
        searchDisplaySettingService = new SearchResultDisplaySettingsServiceBean();
        testUser = createUser(true, null);
        TissueLocatorHibernateUtil.getCurrentSession().save(testUser);
        testDefaultSettings = prepareDefaultDisplaySettings(NUMBER_OF_DEFAULT_FIELDS_FOR_TESTING);
        prepareUserDisplaySettings(NUMBER_OF_USER_FIELDS_FOR_TESTING);
    }

    /**
     * Test object retrieval.
     */
    @Test
    public void testSaveRetrieve() {
        List<SearchResultFieldDisplaySetting> expectedSettings = 
            createDisplaySettings(NUMBER_OF_DEFAULT_FIELDS_FOR_TESTING);
        List<SearchResultFieldDisplaySetting> retrievedSettings = saveAndRetrieveDisplaySettings(expectedSettings);
        for (int i = 0; i < expectedSettings.size(); i++) {
            assertTrue(EqualsBuilder.reflectionEquals(expectedSettings.get(i), retrievedSettings.get(i)));
        }
    }
    
    /**
     * test getting search result display settings by category.
     */
    @Test
    public void testGetDefaultSearchResultDisplaySettingsByCategory() {
        List<SearchResultFieldDisplaySetting> retrievedData = searchDisplaySettingService
                .getDefaultSearchResultDisplaySettings();
        assertNotNull(retrievedData);
        assertTrue(retrievedData.size() > 0);
    }

    /**
     * test getting search result field setting properties.
     */
    @Test
    public void testSearchResultFieldSettingProperties() {
        SearchResultFieldDisplaySetting srfds = new SearchResultFieldDisplaySetting();
        srfds.setDisplayFlagAsBoolean(false);
        assertEquals(DisplayOption.DISABLED, srfds.getDisplayFlag());
        srfds.setDisplayFlagAsBoolean(true);
        assertEquals(DisplayOption.ENABLED, srfds.getDisplayFlag());
        srfds.setDisplayFlag(null);
        assertEquals(DisplayOption.NOT_APPLICABLE, srfds.getDisplayFlag());
        srfds.setDisplayFlag(DisplayOption.ENABLED);
        assertTrue(srfds.isDisplayFlagAsBoolean());
        srfds.setDisplayFlag(DisplayOption.DISABLED);
        assertFalse(srfds.isDisplayFlagAsBoolean());
        srfds.setDisplayFlag(DisplayOption.NOT_APPLICABLE);
        assertFalse(srfds.isDisplayFlagAsBoolean());
        srfds.setFieldCategory(null);
        assertNull(srfds.getFieldCategoryName());
        UiSearchFieldCategory searchCategory = new UiSearchFieldCategory();
        srfds.setFieldCategory(searchCategory);
        assertNull(srfds.getFieldCategoryName());
        searchCategory.setCategoryName("Test Category");
        assertEquals(searchCategory.getCategoryName(), srfds.getFieldCategoryName());
    }

    /**
     * test get user search result display settings.
     */
    @Test
    public void testGetUserSearchResultDisplaySettings() {
        List<SearchResultFieldDisplaySetting> retrievedData = searchDisplaySettingService
                .getUserSearchResultDisplaySettings(UsernameHolder.getUser());
        assertEquals(NUMBER_OF_DEFAULT_FIELDS_FOR_TESTING, retrievedData.size());
        for (int i = 0; i < retrievedData.size(); i++) {
            if (i < NUMBER_OF_USER_FIELDS_FOR_TESTING && i % 2 == 0 || i >= NUMBER_OF_USER_FIELDS_FOR_TESTING) {
                assertEquals(DisplayOption.ENABLED, retrievedData.get(i).getDisplayFlag());
            } else {
                assertEquals(DisplayOption.DISABLED, retrievedData.get(i).getDisplayFlag());
            }
        }
        ArrayList<SearchResultFieldDisplaySetting> userSettingsToChange =
            new ArrayList<SearchResultFieldDisplaySetting>();
        for (int i = 0; i < NUMBER_OF_USER_FIELDS_FOR_TESTING; i++) {
            SearchResultFieldDisplaySetting currentSetting = retrievedData.get(i);
            currentSetting.setDisplayFlagAsBoolean(!currentSetting.isDisplayFlagAsBoolean());
            userSettingsToChange.add(currentSetting);
        }
        searchDisplaySettingService.storeUserSearchResultDisplaySettings(testUser, userSettingsToChange);
        retrievedData = searchDisplaySettingService
                .getUserSearchResultDisplaySettings(UsernameHolder.getUser());
        for (int i = 0; i < retrievedData.size(); i++) {
            if (i < NUMBER_OF_USER_FIELDS_FOR_TESTING && i % 2 == 0) {
                assertEquals(DisplayOption.DISABLED, retrievedData.get(i).getDisplayFlag());
            } else {
                assertEquals(DisplayOption.ENABLED, retrievedData.get(i).getDisplayFlag());
            }
        }
        userSettingsToChange.clear();
        for (int i = NUMBER_OF_USER_FIELDS_FOR_TESTING; i < NUMBER_OF_DEFAULT_FIELDS_FOR_TESTING; i++) {
            SearchResultFieldDisplaySetting currentSetting = retrievedData.get(i);
            currentSetting.setDisplayFlag(DisplayOption.DISABLED);
            userSettingsToChange.add(currentSetting);
        }
        searchDisplaySettingService.storeUserSearchResultDisplaySettings(testUser, userSettingsToChange);
        retrievedData = searchDisplaySettingService
                .getUserSearchResultDisplaySettings(UsernameHolder.getUser());
        for (int i = 0; i < retrievedData.size(); i++) {
            if (i < NUMBER_OF_USER_FIELDS_FOR_TESTING && i % 2 == 1) {
                assertEquals(DisplayOption.ENABLED, retrievedData.get(i).getDisplayFlag());
            } else {
                assertEquals(DisplayOption.DISABLED, retrievedData.get(i).getDisplayFlag());
            }
        }
    }

    private List<SearchResultFieldDisplaySetting> prepareDefaultDisplaySettings(int nFields) {
        List<SearchResultFieldDisplaySetting> searchResultSettings = 
            saveAndRetrieveDisplaySettings(createDisplaySettings(nFields));        
        Collections.sort(searchResultSettings, new SearchResultFieldDisplaySettingComparator());
        return searchResultSettings;
    }
    
    private List<SearchResultFieldDisplaySetting> saveAndRetrieveDisplaySettings(
            List<SearchResultFieldDisplaySetting> searchResultSettings) {
        List<SearchResultFieldDisplaySetting> savedSettings = new ArrayList<SearchResultFieldDisplaySetting>();
        for (SearchResultFieldDisplaySetting setting : searchResultSettings) {
            Long id = searchDisplaySettingService.savePersistentObject(setting);
            savedSettings.add(searchDisplaySettingService.getPersistentObject(
                    SearchResultFieldDisplaySetting.class, id));
        }
        return savedSettings;
    }
    
    private List<SearchResultFieldDisplaySetting> createDisplaySettings(int nFields) {
        List<SearchResultFieldDisplaySetting> searchResultSettings = new ArrayList<SearchResultFieldDisplaySetting>();
        for (int i = 0; i < nFields; i++) {
            String fieldName = "f" + String.valueOf(i + 1);
            SearchResultFieldDisplaySetting fieldSetting = new SearchResultFieldDisplaySetting();
            fieldSetting.setDisplayFlag(DisplayOption.ENABLED);
            assertTrue(fieldSetting.isDisplayFlagAsBoolean());
            fieldSetting.setFieldConfig(createAndPersistGenericFieldConfig(fieldName));
            fieldSetting.setFieldCategory(createAndPersistSearchCategory());
            assertNotNull(fieldSetting.getFieldCategoryName());
            searchResultSettings.add(fieldSetting);
        }
        return searchResultSettings;
    }
    
    private UiSearchFieldCategory createAndPersistSearchCategory() {
        UiSearchFieldCategory category = new UiSearchFieldCategoryServiceTest()
            .createUiSearchFieldCategories(1).get(0);
        UiSearchFieldCategoryServiceLocal service = new UiSearchFieldCategoryServiceBean();
        Long id = service.savePersistentObject(category);
        return service.getPersistentObject(UiSearchFieldCategory.class, id);
    }
    private List<SearchResultFieldDisplayUserPreference> prepareUserDisplaySettings(int nUserSettings) {
        if (nUserSettings > testDefaultSettings.size()) {
            nUserSettings = testDefaultSettings.size();
        }
        List<SearchResultFieldDisplayUserPreference> userDisplayPreferences =
            new ArrayList<SearchResultFieldDisplayUserPreference>();
        GenericServiceLocal<PersistentObject> userDisplayPreferencesService = TissueLocatorRegistry.getServiceLocator()
                .getGenericService();

        for (int i = 0; i < nUserSettings; i++) {
            SearchResultFieldDisplayUserPreference userDisplayPreference = new SearchResultFieldDisplayUserPreference();
            userDisplayPreference.setUser(testUser);
            userDisplayPreference.setFieldDisplaySetting(testDefaultSettings.get(i));
            if (i % 2 == 0) {
                userDisplayPreference.setDisplayFlag(DisplayOption.ENABLED);
            } else {
                userDisplayPreference.setDisplayFlag(DisplayOption.DISABLED);
            }
            userDisplayPreferencesService.savePersistentObject(userDisplayPreference);
            userDisplayPreferences.add(userDisplayPreference);
        }
        return userDisplayPreferences;
    }

}
