/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.IrbApprovalStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.Study;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.validation.study.StudyIrbApprovalStatusValidator;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;

/**
 * @author ddasgupta
 *
 */
public class SpecimenRequestIrbValidationTest extends AbstractHibernateTestCase {

    /**
     * Tests irb approval status validator.
     */
    @Test
    public void testIrbApprovalStatusValidator() {
        StudyIrbApprovalStatusValidator validator = new StudyIrbApprovalStatusValidator();
        Study study = new Study();
        assertTrue(validator.isValid(study));
                
        study.setIrbApprovalStatus(IrbApprovalStatus.NOT_APPROVED);
        assertTrue(validator.isValid(study));
        
        study.setIrbApprovalStatus(IrbApprovalStatus.EXEMPT);
        assertFalse(validator.isValid(study));
        
        study.setIrbExemptionLetter(new TissueLocatorFile(new byte[]{}, "filename", "contenttype"));
        assertFalse(validator.isValid(study));
        
        study.setIrbName("irb name");
        assertTrue(validator.isValid(study));
        
        study.setIrbName(null);
        study.setIrbApprovalStatus(IrbApprovalStatus.APPROVED);
        assertFalse(validator.isValid(study));
        
        study.setIrbName("irb name");
        assertFalse(validator.isValid(study));
        
        study.setIrbApprovalExpirationDate(new Date());
        assertFalse(validator.isValid(study));
        
        study.setIrbApprovalLetter(new TissueLocatorFile(new byte[]{}, "filename", "contenttype"));
        assertTrue(validator.isValid(study));
        
        study.setIrbApprovalNumber("irb approval number");
        assertTrue(validator.isValid(study));
        
        study.setIrbApprovalLetter(null);
        assertTrue(validator.isValid(study));
        
        study.setIrbApprovalLetter(new TissueLocatorFile());
        study.setIrbApprovalNumber(null);
        assertTrue(validator.isValid(study));
    }

    /**
     * tests that past irb approval expiration date is not allowed.
     */
    @Test
    public void testIrbApprovalExpirationDateInPast() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);

        SpecimenRequest request = new SpecimenRequest();
        Study study = new Study();
        study.setIrbApprovalStatus(IrbApprovalStatus.APPROVED);
        study.setIrbApprovalNumber("1234");
        study.setIrbApprovalExpirationDate(cal.getTime());
        study.setIrbApprovalLetter(null);
        study.setIrbExemptionLetter(null);
        request.setStudy(study);

        assertFalse(request.isStudyIrbApprovalExpirationDateValid());
    }

    /**
     * tests that irb approval expiration date must be in the future.
     */
    @Test
    public void testIrbApprovalExpirationDateInFuture() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 1);

        SpecimenRequest request = new SpecimenRequest();
        Study study = new Study();
        study.setIrbApprovalStatus(IrbApprovalStatus.APPROVED);
        study.setIrbApprovalNumber("1234");
        study.setIrbApprovalExpirationDate(cal.getTime());
        study.setIrbApprovalLetter(null);
        study.setIrbExemptionLetter(null);
        request.setStudy(study);

        assertTrue(request.isStudyIrbApprovalExpirationDateValid());
    }
}
