/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.data.security.AccountStatus;

/**
 * TissueLocatorUser unit tests.
 * 
 * @author jstephens
 */
public class TissueLocatorUserTest {

    /**
     * When previous status is pending and status is inactive the user is deletable. 
     * 
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     */
    @Test
    public void pendingToInactiveIsDeletable() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        TissueLocatorUser user = new TissueLocatorUser();
        user.setId(1L);
        setPreviousStatus(user, AccountStatus.PENDING);
        user.setStatus(AccountStatus.INACTIVE);
        assertFalse(user.isDeletable());
    }
    
    /**
     * When previous status is pending and status is active the user is not deletable. 
     * 
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     */
    @Test
    public void pendingToActiveIsNotDeletable() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        TissueLocatorUser user = new TissueLocatorUser();
        user.setId(1L);
        setPreviousStatus(user, AccountStatus.PENDING);
        user.setStatus(AccountStatus.ACTIVE);
        assertFalse(user.isDeletable());
    }
    
    /**
     * When previous status is non-pending the user is not deletable. 
     * 
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     */
    @Test
    public void nonpendingPreviousStatusIsNotDeletable() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        TissueLocatorUser user = new TissueLocatorUser();
        user.setId(1L);
        setPreviousStatus(user, AccountStatus.INACTIVE);
        user.setStatus(AccountStatus.ACTIVE);
        assertFalse(user.isDeletable());
    }

    /**
     * An unpersisted user is not deletable. 
     * 
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     */
    @Test
    public void unpersistedUserIsNotDeletable() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        TissueLocatorUser user = new TissueLocatorUser();
        setPreviousStatus(user, AccountStatus.PENDING);
        user.setStatus(AccountStatus.ACTIVE);
        assertFalse(user.isDeletable());
    }
    
    /**
     * When the previous status has not been set the user is not deletable. 
     * 
     * @throws InvocationTargetException if the underlying method throws an exception.
     * @throws IllegalAccessException if the underlying method is inaccessible.
     * @throws NoSuchMethodException if a matching method is not found.
     */
    @Test
    public void nullPreviousStatusIsNotDeletable() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        TissueLocatorUser user = new TissueLocatorUser();
        user.setId(1L);
        user.setStatus(AccountStatus.ACTIVE);
        assertFalse(user.isDeletable());
    }    
    
    /**
     * Test the isDeactivationStatusTransitionCommentValid method.
     */
    @Test
    public void testDeactivationStatusTransitionCommentIsValid() {
        TissueLocatorUser user = new TissueLocatorUser();
        assertTrue(user.isDeactivationStatusTransitionCommentValid());
        for (AccountStatus status : AccountStatus.values()) {
            user.setPreviousStatus(null);
            user.setStatus(status);
            assertTrue(user.isDeactivationStatusTransitionCommentValid());
        }
        
        for (AccountStatus previous : AccountStatus.values()) {
            for (AccountStatus status : AccountStatus.values()) {
                user.setPreviousStatus(previous);
                user.setStatus(status);
                boolean invalid = previous.equals(AccountStatus.ACTIVE) && status.equals(AccountStatus.INACTIVE);
                assertEquals(!invalid, user.isDeactivationStatusTransitionCommentValid());
            }
        }
        
        user.setStatusTransitionComment("deactivation reason");
        for (AccountStatus previous : AccountStatus.values()) {
            for (AccountStatus status : AccountStatus.values()) {
                user.setPreviousStatus(previous);
                user.setStatus(status);
                assertTrue(user.isDeactivationStatusTransitionCommentValid());
            }
        }        
    }
    
    /**
     * Test the isDenialStatusTransitionCommentValid method.
     */
    @Test
    public void testDenialStatusTransitionCommentIsValid() {
        TissueLocatorUser user = new TissueLocatorUser();
        assertTrue(user.isDeactivationStatusTransitionCommentValid());
        for (AccountStatus status : AccountStatus.values()) {
            user.setPreviousStatus(null);
            user.setStatus(status);
            assertTrue(user.isDenialStatusTransitionCommentValid());
        }
        
        for (AccountStatus previous : AccountStatus.values()) {
            for (AccountStatus status : AccountStatus.values()) {
                user.setPreviousStatus(previous);
                user.setStatus(status);
                boolean invalid = previous.equals(AccountStatus.PENDING) && status.equals(AccountStatus.INACTIVE);
                assertEquals(!invalid, user.isDenialStatusTransitionCommentValid());
            }
        }
        
        user.setStatusTransitionComment("denial reason");
        for (AccountStatus previous : AccountStatus.values()) {
            for (AccountStatus status : AccountStatus.values()) {
                user.setPreviousStatus(previous);
                user.setStatus(status);
                assertTrue(user.isDenialStatusTransitionCommentValid());
            }
        }        
    }
    
    private void setPreviousStatus(TissueLocatorUser user, AccountStatus status) throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        Method method = AbstractUser.class
            .getDeclaredMethod("setPreviousStatus", AccountStatus.class);
        method.setAccessible(true);
        method.invoke(user, status);
    }

}
