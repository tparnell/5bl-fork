/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.RequestReviewReport;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.util.UsageReport;

/**
 * @author ddasgupta
 *
 */
public class ReportServiceTest extends AbstractHibernateTestCase {

    /**
     * tests generation of the usage report.
     */
    @Test
    public void testUsageReport() {
        ApplicationSettingsServiceTest ast = new ApplicationSettingsServiceTest();
        ast.createServiceForTest();
        ast.testGetNewRequestCountCondition();
        ReportServiceLocal service = TissueLocatorRegistry.getServiceLocator().getReportService();
        Date start = DateUtils.addDays(new Date(), 2);
        Date end = DateUtils.addDays(start, 2);
        UsageReport report = service.getUsageReport(start, end);
        assertNotNull(report);
        assertNotNull(report.getTopSpecimenTypes());
        assertEquals(0, report.getShippedOrderCount());
        assertEquals(0, report.getNewUserCount());
        assertEquals(0, report.getRestrictedNewRequestCount());
        assertEquals(0, report.getUnrestrictedNewRequestCount());
        assertEquals(0, report.getTotalReturnsRequested());
        assertEquals(0, report.getTotalReturnsFulfilled());
        assertEquals(0, report.getTotalUnfulfilledReturnRequests());
        assertNotNull(report.getParticipantReturnRequestMap());
    }

    /**
     * tests generation of the usage report.
     */
    @Test
    public void testRequestReviewReport() {
        ApplicationSettingsServiceTest ast = new ApplicationSettingsServiceTest();
        ast.createServiceForTest();
        ast.testGetNewRequestCountCondition();
        ReportServiceLocal service = TissueLocatorRegistry.getServiceLocator().getReportService();
        Date start = DateUtils.addDays(new Date(), 2);
        Date end = DateUtils.addDays(start, 2);
        RequestReviewReport report = service.getRequestReviewReport(start, end, 2);
        assertNotNull(report);
        assertNotNull(report.getScientificReviewMap());
        assertNotNull(report.getInstitutionalReviewMap());
        assertNotNull(report.getLateScientificReviewMap());
        assertNotNull(report.getLateInstitutionalReviewMap());
    }

    /**
     * tests generation of a jasper report.
     * @throws Exception on error
     */
    @Test
    public void testJasperReport() throws Exception {
        ReportServiceLocal service = TissueLocatorRegistry.getServiceLocator().getReportService();
        String report = new File(ClassLoader.getSystemResource("newUsers.jasper").toURI()).toString();
        JasperPrint print = service.runJasperReport(report, new HashMap<String, Object>());
        assertNotNull(print);
        print = service.runJasperReport(report, null);
        assertNotNull(print);
    }

    /**
     * tests correct exception is thrown when a report file is not found.
     * @throws Exception on error
     */
    @Test(expected = IllegalArgumentException.class)
    public void testMissingJasperReport() throws Exception {
        ReportServiceLocal service = TissueLocatorRegistry.getServiceLocator().getReportService();
        String report = new File(ClassLoader.getSystemResource("newUsers.jasper").toURI()).toString();
        service.runJasperReport(report.replaceAll("newUsers", "invalid"), new HashMap<String, Object>());
        fail();
    }

    /**
     * tests generation of the new user jasper report with the custom data source.
     * @throws Exception on error
     */
    @Test
    public void testNewUserReport() throws Exception {
        ReportServiceLocal service = TissueLocatorRegistry.getServiceLocator().getReportService();
        String report = new File(ClassLoader.getSystemResource("newUsers.jasper").toURI()).toString();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", new Date(0));
        params.put("endDate", new Date());
        JasperPrint print = service.runNewUserReport(report, params);
        assertNotNull(print);
        assertNotNull(print.getPages());
        assertEquals(1, print.getPages().size());
    }
}
