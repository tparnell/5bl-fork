/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data;

import static com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus.APPROVED;
import static com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus.OUT_OF_DATE;
import static com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus.PENDING_REVIEW;
import static com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus.REJECTED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.hibernate.Session;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SignedMaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.InstitutionSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.SignedMaterialTransferAgreementSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.test.InstitutionPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
public class SignedMtaPersistenceTest extends AbstractPersistenceTest<SignedMaterialTransferAgreement> {

    private Date version2;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public SignedMaterialTransferAgreement[] getValidObjects() {
        GenericServiceLocal<PersistentObject> service = TissueLocatorRegistry.getServiceLocator().getGenericService();
        TissueLocatorUser uploader = getCurrentUser();
        InstitutionPersistenceHelper helper = InstitutionPersistenceHelper.getInstance();
        InstitutionType type = helper.createAndSaveDefaultType();
        Institution i = helper.createInstitution(type, "institution name", true);
        Long id = service.savePersistentObject(i);
        i = service.getPersistentObject(Institution.class, id);

        MaterialTransferAgreement mta1 = new MtaPersistenceTest().getValidObjects()[0];
        Long mtaId = service.savePersistentObject(mta1);
        mta1 = service.getPersistentObject(MaterialTransferAgreement.class, mtaId);
        MaterialTransferAgreement mta2 = new MtaPersistenceTest().getValidObjects()[1];
        mtaId = service.savePersistentObject(mta2);
        mta2 = service.getPersistentObject(MaterialTransferAgreement.class, mtaId);
        version2 = mta2.getVersion();

        SignedMaterialTransferAgreement[] mtas = new SignedMaterialTransferAgreement[2];
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);

        SignedMaterialTransferAgreement mta = new SignedMaterialTransferAgreement();
        mta.setDocument(new TissueLocatorFile(new byte[] {1}, "mta1.txt", "text/plain"));
        mta.setSendingInstitution(i);
        mta.setOriginalMta(mta1);
        mta.setStatus(OUT_OF_DATE);
        mta.setUploadTime(cal.getTime());
        mta.setUploader(uploader);
        mtas[0] = mta;

        cal.add(Calendar.YEAR, 1);
        mta = new SignedMaterialTransferAgreement();
        mta.setDocument(new TissueLocatorFile(new byte[] {2}, "mta2.txt", "text/plain"));
        mta.setOriginalMta(mta2);
        mta.setSendingInstitution(i);
        mta.setStatus(PENDING_REVIEW);
        mta.setUploadTime(cal.getTime());
        mta.setUploader(uploader);
        mtas[1] = mta;

        return mtas;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(SignedMaterialTransferAgreement expected,
            SignedMaterialTransferAgreement actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
    }

    /**
     * test searching for signed mtas by status.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        SignedMaterialTransferAgreement example = new SignedMaterialTransferAgreement();
        example.setStatus(PENDING_REVIEW);
        assertTrue(StringUtils.isNotBlank(example.getStatus().getResourceKey()));
        SearchCriteria<SignedMaterialTransferAgreement> sc =
            new TissueLocatorAnnotatedBeanSearchCriteria<SignedMaterialTransferAgreement>(example);
        SignedMaterialTransferAgreementServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        List<SignedMaterialTransferAgreement> results = service.search(sc);
        assertEquals(1, results.size());
        example.setStatus(OUT_OF_DATE);
        results = service.search(sc);
        assertEquals(1, results.size());
        example.setStatus(APPROVED);
        results = service.search(sc);
        assertEquals(0, results.size());

        example.setStatus(OUT_OF_DATE);
        SignedMaterialTransferAgreementSortCriterion[] sorts = SignedMaterialTransferAgreementSortCriterion.values();
        for (SignedMaterialTransferAgreementSortCriterion sort : sorts) {
            PageSortParams<SignedMaterialTransferAgreement> psp =
                new PageSortParams<SignedMaterialTransferAgreement>(PAGE_SIZE, 0, sort, false);
            assertEquals(1, service.search(sc, psp).size());
        }
    }

    /**
     * test the ordering of the signed sender mta collection on institution.
     */
    @Test
    public void testSignedSenderMtaOrder() {
        testSaveRetrieve();
        GenericServiceLocal<PersistentObject> service = TissueLocatorRegistry.getServiceLocator().getGenericService();
        SignedMaterialTransferAgreement saved = service.getAll(SignedMaterialTransferAgreement.class).get(0);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        Institution i = service.getPersistentObject(Institution.class, saved.getSendingInstitution().getId());
        assertNotNull(i.getSignedSenderMtas());
        assertEquals(2, i.getSignedSenderMtas().size());
        Calendar cal = Calendar.getInstance();
        int currentYear = cal.get(Calendar.YEAR);
        cal.setTime(i.getSignedSenderMtas().get(0).getUploadTime());
        assertEquals(currentYear, cal.get(Calendar.YEAR));
        cal.setTime(i.getSignedSenderMtas().get(1).getUploadTime());
        assertEquals(currentYear - 1, cal.get(Calendar.YEAR));
    }

    /**
     * Test the get current signed sender mta method on institution.
     */
    @Test
    public void testGetCurrentSignedSenderMta() {
        testSaveRetrieve();
        Institution i = new Institution();
        assertNull(i.getCurrentSignedSenderMta());
        GenericServiceLocal<PersistentObject> service = TissueLocatorRegistry.getServiceLocator().getGenericService();
        SignedMaterialTransferAgreement saved = service.getAll(SignedMaterialTransferAgreement.class).get(0);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        i = service.getPersistentObject(Institution.class, saved.getSendingInstitution().getId());
        assertNotNull(i.getSignedSenderMtas());
        assertEquals(2, i.getSignedSenderMtas().size());
        SignedMaterialTransferAgreement current = i.getSignedSenderMtas().get(0);
        assertNotNull(i.getCurrentSignedSenderMta());
        assertEquals(current.getId(), i.getCurrentSignedSenderMta().getId());
        assertEquals(PENDING_REVIEW, i.getCurrentSignedSenderMta().getStatus());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        current.setSendingInstitution(service.getPersistentObject(Institution.class,
                current.getSendingInstitution().getId()));
        current.setStatus(APPROVED);
        service.savePersistentObject(current);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        i = service.getPersistentObject(Institution.class, saved.getSendingInstitution().getId());
        assertNotNull(i.getSignedSenderMtas());
        assertEquals(2, i.getSignedSenderMtas().size());
        assertNotNull(i.getCurrentSignedSenderMta());
        assertEquals(current.getId(), i.getCurrentSignedSenderMta().getId());
        assertEquals(APPROVED, i.getCurrentSignedSenderMta().getStatus());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        current.setSendingInstitution(service.getPersistentObject(Institution.class,
                current.getSendingInstitution().getId()));
        current.setStatus(REJECTED);
        service.savePersistentObject(current);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        i = service.getPersistentObject(Institution.class, saved.getSendingInstitution().getId());
        assertNotNull(i.getSignedSenderMtas());
        assertEquals(2, i.getSignedSenderMtas().size());
        SignedMaterialTransferAgreement old = i.getSignedSenderMtas().get(1);
        assertNotNull(i.getCurrentSignedSenderMta());
        assertEquals(old.getId(), i.getCurrentSignedSenderMta().getId());
        assertEquals(OUT_OF_DATE, i.getCurrentSignedSenderMta().getStatus());
    }

    /**
     * test the ordering of the signed recipient mta collection on institution.
     */
    @Test
    public void testSignedRecipientMtaOrder() {
        testSaveRetrieve();
        GenericServiceLocal<PersistentObject> service = TissueLocatorRegistry.getServiceLocator().getGenericService();
        for (SignedMaterialTransferAgreement mta : service.getAll(SignedMaterialTransferAgreement.class)) {
            mta.setReceivingInstitution(mta.getSendingInstitution());
            mta.setSendingInstitution(null);
            service.savePersistentObject(mta);
        }
        SignedMaterialTransferAgreement saved = service.getAll(SignedMaterialTransferAgreement.class).get(0);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        Institution i = service.getPersistentObject(Institution.class, saved.getReceivingInstitution().getId());
        assertNotNull(i.getSignedRecipientMtas());
        assertEquals(2, i.getSignedRecipientMtas().size());
        Calendar cal = Calendar.getInstance();
        int currentYear = cal.get(Calendar.YEAR);
        cal.setTime(i.getSignedRecipientMtas().get(0).getUploadTime());
        assertEquals(currentYear, cal.get(Calendar.YEAR));
        cal.setTime(i.getSignedRecipientMtas().get(1).getUploadTime());
        assertEquals(currentYear - 1, cal.get(Calendar.YEAR));
    }

    /**
     * Test the get current signed recipient mta method on institution.
     */
    @Test
    public void testGetCurrentSignedRecipientMta() {
        testSaveRetrieve();
        Institution i = new Institution();
        assertNull(i.getCurrentSignedRecipientMta());
        GenericServiceLocal<PersistentObject> service = TissueLocatorRegistry.getServiceLocator().getGenericService();
        for (SignedMaterialTransferAgreement mta : service.getAll(SignedMaterialTransferAgreement.class)) {
            mta.setReceivingInstitution(mta.getSendingInstitution());
            mta.setSendingInstitution(null);
            service.savePersistentObject(mta);
        }
        SignedMaterialTransferAgreement saved = service.getAll(SignedMaterialTransferAgreement.class).get(0);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        i = service.getPersistentObject(Institution.class, saved.getReceivingInstitution().getId());
        assertNotNull(i.getSignedRecipientMtas());
        assertEquals(2, i.getSignedRecipientMtas().size());
        SignedMaterialTransferAgreement current = i.getSignedRecipientMtas().get(0);
        assertNotNull(i.getCurrentSignedRecipientMta());
        assertEquals(current.getId(), i.getCurrentSignedRecipientMta().getId());
        assertEquals(PENDING_REVIEW,
                i.getCurrentSignedRecipientMta().getStatus());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        current.setReceivingInstitution(service.getPersistentObject(Institution.class,
                current.getReceivingInstitution().getId()));
        current.setStatus(APPROVED);
        service.savePersistentObject(current);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        i = service.getPersistentObject(Institution.class, saved.getReceivingInstitution().getId());
        assertNotNull(i.getSignedRecipientMtas());
        assertEquals(2, i.getSignedRecipientMtas().size());
        assertNotNull(i.getCurrentSignedRecipientMta());
        assertEquals(current.getId(), i.getCurrentSignedRecipientMta().getId());
        assertEquals(APPROVED, i.getCurrentSignedRecipientMta().getStatus());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        current.setReceivingInstitution(service.getPersistentObject(Institution.class,
                current.getReceivingInstitution().getId()));
        current.setStatus(REJECTED);
        service.savePersistentObject(current);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        i = service.getPersistentObject(Institution.class, saved.getReceivingInstitution().getId());
        assertNotNull(i.getSignedRecipientMtas());
        assertEquals(2, i.getSignedRecipientMtas().size());
        SignedMaterialTransferAgreement old = i.getSignedRecipientMtas().get(1);
        assertNotNull(i.getCurrentSignedRecipientMta());
        assertEquals(old.getId(), i.getCurrentSignedRecipientMta().getId());
        assertEquals(OUT_OF_DATE, i.getCurrentSignedRecipientMta().getStatus());
    }

    /**
     * test the custom hibernate validation.
     */
    @Test
    public void testValidation() {
        SignedMaterialTransferAgreement mta = new SignedMaterialTransferAgreement();
        assertFalse(mta.areInstitutionsValid());
        mta.setSendingInstitution(new Institution());
        assertTrue(mta.areInstitutionsValid());
        mta.setReceivingInstitution(new Institution());
        assertFalse(mta.areInstitutionsValid());
        mta.setSendingInstitution(null);
        assertTrue(mta.areInstitutionsValid());
    }

    /**
     * test the custom hibernate validation.
     */
    @Test
    public void testInstitutionSearch() {
        InstitutionServiceLocal service = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        InstitutionSearchCriteria isc = new InstitutionSearchCriteria(new Institution(), null);
        List<Institution> results = service.search(isc);
        assertNotNull(results);
        assertEquals(0, results.size());
        assertEquals(0, service.count(isc));
        testSaveRetrieve();
        results = service.search(isc);
        assertNotNull(results);
        assertEquals(0, results.size());
        assertEquals(0, service.count(isc));
        isc = new InstitutionSearchCriteria(new Institution(), version2);
        results = service.search(isc);
        assertNotNull(results);
        assertEquals(0, results.size());
        assertEquals(0, service.count(isc));

        GenericServiceLocal<PersistentObject> gsl = TissueLocatorRegistry.getServiceLocator().getGenericService();
        for (SignedMaterialTransferAgreement mta : service.getAll(SignedMaterialTransferAgreement.class)) {
            mta.setReceivingInstitution(mta.getSendingInstitution());
            mta.setSendingInstitution(null);
            gsl.savePersistentObject(mta);
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        results = service.search(isc);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(1, service.count(isc));

        SignedMaterialTransferAgreement current = results.get(0).getSignedRecipientMtas().get(0);
        SignedMaterialTransferAgreement old = results.get(0).getSignedRecipientMtas().get(1);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        old.setSendingInstitution(service.getPersistentObject(Institution.class,
                old.getReceivingInstitution().getId()));
        old.setReceivingInstitution(null);
        gsl.savePersistentObject(old);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        results = service.search(isc);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(1, service.count(isc));
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        old.setReceivingInstitution(service.getPersistentObject(Institution.class,
                old.getSendingInstitution().getId()));
        old.setSendingInstitution(null);
        gsl.savePersistentObject(old);
        current.setReceivingInstitution(service.getPersistentObject(Institution.class,
                current.getReceivingInstitution().getId()));
        current.setStatus(REJECTED);
        gsl.savePersistentObject(current);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        results = service.search(isc);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(1, service.count(isc));
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        old.setSendingInstitution(service.getPersistentObject(Institution.class,
                old.getReceivingInstitution().getId()));
        old.setReceivingInstitution(null);
        gsl.savePersistentObject(old);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        results = service.search(isc);
        assertNotNull(results);
        assertEquals(0, results.size());
        assertEquals(0, service.count(isc));

        MaterialTransferAgreement currentMta = current.getOriginalMta();
        MaterialTransferAgreement oldMta = old.getOriginalMta();
        old.setReceivingInstitution(service.getPersistentObject(Institution.class,
                old.getSendingInstitution().getId()));
        old.setSendingInstitution(null);
        old.setOriginalMta(currentMta);
        old.setStatus(APPROVED);
        gsl.savePersistentObject(old);
        current.setReceivingInstitution(service.getPersistentObject(Institution.class,
                current.getReceivingInstitution().getId()));
        current.setStatus(PENDING_REVIEW);
        gsl.savePersistentObject(current);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        results = service.search(isc);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(1, service.count(isc));
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        old.setReceivingInstitution(service.getPersistentObject(Institution.class,
                old.getReceivingInstitution().getId()));
        old.setOriginalMta(oldMta);
        old.setStatus(OUT_OF_DATE);
        gsl.savePersistentObject(old);
        current.setReceivingInstitution(service.getPersistentObject(Institution.class,
                current.getReceivingInstitution().getId()));
        current.setOriginalMta(oldMta);
        current.setStatus(OUT_OF_DATE);
        gsl.savePersistentObject(current);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        results = service.search(isc);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(1, service.count(isc));
    }

    /**
     * tests the save signed mta method.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testSaveSignedSenderMta() throws MessagingException, IOException {
        testSaveSignedMta(getValidObjects());
    }

    /**
     * tests the save signed mta method.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testSaveSignedRecipientMta() throws MessagingException, IOException {
        SignedMaterialTransferAgreement[] mtas = getValidObjects();
        for (SignedMaterialTransferAgreement mta : mtas) {
            mta.setReceivingInstitution(mta.getSendingInstitution());
            mta.setSendingInstitution(null);
        }
        testSaveSignedMta(mtas);
    }

    private void testSaveSignedMta(SignedMaterialTransferAgreement[] mtas) throws MessagingException, IOException {
        MailUtils.setMailEnabled(true);
        TissueLocatorUser mtaAdmin = createUserInRole(Role.MTA_ADMINISTRATOR, getCurrentUser().getInstitution());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        SignedMaterialTransferAgreementServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        for (SignedMaterialTransferAgreement mta : mtas) {
            Session s = TissueLocatorHibernateUtil.getCurrentSession();
            if (mta.getSendingInstitution() != null) {
                mta.setSendingInstitution((Institution) s.load(Institution.class,
                        mta.getSendingInstitution().getId()));
            } else {
                mta.setReceivingInstitution((Institution) s.load(Institution.class,
                        mta.getReceivingInstitution().getId()));
            }
            Long id = service.saveSignedMaterialTransferAgreement(mta);
            assertNotNull(id);
            assertNotNull(service.getPersistentObject(SignedMaterialTransferAgreement.class, id));
            assertTrue(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
            assertFalse(Mailbox.get(mtaAdmin.getEmail()).isEmpty());
            assertEquals(1, Mailbox.get(mtaAdmin.getEmail()).size());
            testEmail(mtaAdmin.getEmail(), "MTA awaiting your review", "An MTA has been submitted for your review",
                    "MTA verification page", "review and verify the MTA");
            Mailbox.clearAll();
        }
        MailUtils.setMailEnabled(false);
    }

    /**
     * tests the review signed mta method.
     */
    @Test
    public void testReviewSignedMta() {
        testSaveRetrieve();
        SignedMaterialTransferAgreementServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        Institution original = null;
        for (SignedMaterialTransferAgreement mta : service.getAll(SignedMaterialTransferAgreement.class)) {
            mta.setReceivingInstitution(mta.getSendingInstitution());
            mta.setSendingInstitution(null);
            service.savePersistentObject(mta);
            original = mta.getReceivingInstitution();
        }
        Long mta1Id = service.getAll(SignedMaterialTransferAgreement.class).get(1).getId();
        Long mta2Id = service.getAll(SignedMaterialTransferAgreement.class).get(0).getId();

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta1Id, mta2Id, APPROVED, PENDING_REVIEW, null, true, false);
        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta2Id, mta1Id, REJECTED, PENDING_REVIEW, null, true, false);
        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta1Id, mta2Id, APPROVED, PENDING_REVIEW, null, true, true);
        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta2Id, mta1Id, APPROVED, OUT_OF_DATE, null, true, true);

        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta1Id, mta2Id, REJECTED, PENDING_REVIEW, null, false, false);
        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta2Id, mta1Id, REJECTED, PENDING_REVIEW, null, false, false);
        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta1Id, mta2Id, REJECTED, PENDING_REVIEW, null, false, true);
        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta2Id, mta1Id, REJECTED, OUT_OF_DATE, null, false, true);

        resetData(original, OUT_OF_DATE);
        reviewMtaHelper(mta1Id, mta2Id, APPROVED, OUT_OF_DATE, null, true, false);
        resetData(original, OUT_OF_DATE);
        reviewMtaHelper(mta2Id, mta1Id, APPROVED, OUT_OF_DATE, null, true, false);
        resetData(original, OUT_OF_DATE);
        reviewMtaHelper(mta1Id, mta2Id, APPROVED, OUT_OF_DATE, null, true, true);
        resetData(original, OUT_OF_DATE);
        reviewMtaHelper(mta2Id, mta1Id, APPROVED, OUT_OF_DATE, null, true, true);

        Institution currentUserInst = getCurrentUser().getInstitution();
        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta1Id, mta2Id, APPROVED, PENDING_REVIEW, currentUserInst, true, false);
        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta2Id, mta1Id, APPROVED, PENDING_REVIEW, currentUserInst, true, false);
        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta1Id, mta2Id, APPROVED, PENDING_REVIEW, currentUserInst, true, true);
        resetData(original, PENDING_REVIEW);
        reviewMtaHelper(mta2Id, mta1Id, APPROVED, PENDING_REVIEW, currentUserInst, true, true);
    }


    /**
     * tests the update signed mta and deprecated signed mta method.
     */
    @Test
    public void testUpdateAndDeprecateSignedMta() {
        testSaveRetrieve();
        SignedMaterialTransferAgreementServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        for (SignedMaterialTransferAgreement mta : service.getAll(SignedMaterialTransferAgreement.class)) {
            mta.setReceivingInstitution(mta.getSendingInstitution());
            mta.setSendingInstitution(null);
            service.savePersistentObject(mta);
        }
        Long mta1Id = service.getAll(SignedMaterialTransferAgreement.class).get(1).getId();
        Long mta2Id = service.getAll(SignedMaterialTransferAgreement.class).get(0).getId();

        SignedMaterialTransferAgreement testMta =
            service.getPersistentObject(SignedMaterialTransferAgreement.class, mta1Id);
        SignedMaterialTransferAgreement otherMta =
            service.getPersistentObject(SignedMaterialTransferAgreement.class, mta2Id);

        testMta.setStatus(APPROVED);
        otherMta.setStatus(APPROVED);
        service.updateSignedMta(testMta, otherMta);
        assertEquals(APPROVED, testMta.getStatus());
        assertEquals(OUT_OF_DATE, otherMta.getStatus());

        service.updateSignedMta(testMta, null);
        assertEquals(APPROVED, testMta.getStatus());

        service.deprecateSignedMta(null);
        testMta.setStatus(APPROVED);
        service.deprecateSignedMta(testMta);
        assertEquals(OUT_OF_DATE, testMta.getStatus());
        service.deprecateSignedMta(testMta);
        assertEquals(OUT_OF_DATE, testMta.getStatus());
    }

    private void reviewMtaHelper(Long testMtaId, Long otherMtaId,
            SignedMaterialTransferAgreementStatus testMtaExpectedStatus,
            SignedMaterialTransferAgreementStatus otherMtaExpectedStatus, Institution testInstitution,
            boolean valid, boolean replaceExisting) {
        SignedMaterialTransferAgreementServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        InstitutionServiceLocal instService = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        SignedMaterialTransferAgreement testMta =
            service.getPersistentObject(SignedMaterialTransferAgreement.class, testMtaId);
        Institution i = testInstitution;
        if (i == null) {
            i = testMta.getReceivingInstitution();
        }
        i = instService.getPersistentObject(Institution.class, i.getId());
        i.setMtaContact(new Person());
        i.getMtaContact().setFirstName("fname");
        i.getMtaContact().setLastName("lname");
        i.getMtaContact().setEmail("fname@lname.com");
        i.getMtaContact().setAddress(getAddress());
        i.getMtaContact().setOrganization(i);
        service.reviewSignedMta(testMta, i, valid, replaceExisting);
        testMta = service.getPersistentObject(SignedMaterialTransferAgreement.class, testMtaId);
        assertEquals(testMtaExpectedStatus, testMta.getStatus());
        assertEquals(i.getId(), testMta.getReceivingInstitution().getId());
        i = instService.getPersistentObject(Institution.class, i.getId());
        assertNotNull(i.getMtaContact());
        assertEquals("fname", i.getMtaContact().getFirstName());
        assertEquals("lname", i.getMtaContact().getLastName());
        assertEquals("fname@lname.com", i.getMtaContact().getEmail());
        assertNotNull(i.getMtaContact().getAddress());
        assertEquals("123 Main Street", i.getMtaContact().getAddress().getLine1());
        assertEquals("Apt 4", i.getMtaContact().getAddress().getLine2());
        assertEquals("City", i.getMtaContact().getAddress().getCity());
        assertEquals(State.MARYLAND, i.getMtaContact().getAddress().getState());
        assertEquals("12345", i.getMtaContact().getAddress().getZip());
        assertEquals(Country.UNITED_STATES, i.getMtaContact().getAddress().getCountry());
        assertEquals("1234567890", i.getMtaContact().getAddress().getPhone());
        assertEquals("1234", i.getMtaContact().getAddress().getPhoneExtension());
        assertEquals("0987654321", i.getMtaContact().getAddress().getFax());
        assertEquals(i.getId(), i.getMtaContact().getOrganization().getId());

        SignedMaterialTransferAgreement otherMta =
            service.getPersistentObject(SignedMaterialTransferAgreement.class, otherMtaId);
        assertEquals(otherMtaExpectedStatus, otherMta.getStatus());
    }

    private void resetData(Institution original, SignedMaterialTransferAgreementStatus status) {

        SignedMaterialTransferAgreementServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        InstitutionServiceLocal instService = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        Long instId = null;
        for (SignedMaterialTransferAgreement mta : service.getAll(SignedMaterialTransferAgreement.class)) {
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
            mta.setStatus(status);
            mta.setReceivingInstitution(instService.getPersistentObject(Institution.class, original.getId()));
            service.savePersistentObject(mta);
            instId = mta.getReceivingInstitution().getId();
        }
        Institution i = instService.getPersistentObject(Institution.class, instId);
        i.setMtaContact(null);
        instService.savePersistentObject(i);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
    }
}
