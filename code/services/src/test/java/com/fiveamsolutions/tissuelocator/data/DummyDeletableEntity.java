/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.util.Collection;
import java.util.Collections;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fiveamsolutions.nci.commons.data.persistent.Deletable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;

/**
 * @author ddasgupta
 *
 */
@Entity
public class DummyDeletableEntity implements PersistentObject, InstitutionRestricted, Deletable {

    private static final long serialVersionUID = -116368008420632041L;
    private Long id;
    private Institution institution;
    private boolean deletable;
    

    /**
     * get the id.
     * @return the id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the institution
     */
    @ManyToOne
    public Institution getInstitution() {
        return this.institution;
    }

    /**
     * @param institution the insitution
     */
    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    /**
     * @return the collection of related institutions.
     */
    @Transient
    public Collection<Institution> getRelatedInstitutions() {
        return Collections.singleton(getInstitution());
    }

    /**
     * is this deletable.
     * @return true or fales
     */
    public boolean isDeletable() {
        return this.deletable;
    }

    /**
     * @param deletable true or false
     */
    public void setDeletable(boolean deletable) {
        this.deletable = deletable;
    }
}
