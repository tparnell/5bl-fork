/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.validator;

import static org.junit.Assert.assertEquals;

import org.hibernate.validator.InvalidStateException;
import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.security.AccountStatus;
import com.fiveamsolutions.nci.commons.data.security.Password;
import com.fiveamsolutions.nci.commons.data.security.PasswordType;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * Account status change validator unit tests.
 *
 * @author jstephens
 */
public class AccountStatusChangeValidatorTest extends AbstractHibernateTestCase {

    /**
     * Test active to inactive transition.
     */
    @Test
    public void activeToInactiveIsValid() {
        Long id = updateAccountStatus(AccountStatus.ACTIVE, AccountStatus.INACTIVE);
        TissueLocatorUser user = getTissueLocatorUser(id);
        assertEquals(user.getStatus(), AccountStatus.INACTIVE);
    }

    /**
     * Test inactive to active transition.
     */
    @Test
    public void inactiveToActiveIsValid() {
        Long id = updateAccountStatus(AccountStatus.INACTIVE, AccountStatus.ACTIVE);
        TissueLocatorUser user = getTissueLocatorUser(id);
        assertEquals(user.getStatus(), AccountStatus.ACTIVE);
    }

    /**
     * Test pending to active transition.
     */
    @Test
    public void pendingToActiveIsValid() {
        Long id = updateAccountStatus(AccountStatus.PENDING, AccountStatus.ACTIVE);
        TissueLocatorUser user = getTissueLocatorUser(id);
        assertEquals(user.getStatus(), AccountStatus.ACTIVE);
    }

    /**
     * Test pending to inactive transition.
     */
    @Test
    public void pendingToInactiveIsValid() {
        Long id = updateAccountStatus(AccountStatus.PENDING, AccountStatus.INACTIVE);
        TissueLocatorUser user = getTissueLocatorUser(id);
        assertEquals(user.getStatus(), AccountStatus.INACTIVE);
    }

    /**
     * Test active to pending transition.
     */
    @Test(expected = InvalidStateException.class)
    public void activeToPendingIsInvalid() {
        updateAccountStatus(AccountStatus.ACTIVE, AccountStatus.PENDING);
    }

    /**
     * Test inactive to pending transition.
     */
    @Test(expected = InvalidStateException.class)
    public void inactiveToPendingIsInvalid() {
        updateAccountStatus(AccountStatus.INACTIVE, AccountStatus.PENDING);
    }

    private TissueLocatorUser createTissueLocatorUser(AccountStatus status) {
        InstitutionType institutionType = new InstitutionType();
        institutionType.setName("Test Institution");
        TissueLocatorHibernateUtil.getCurrentSession().save(institutionType);

        Institution institution = new Institution();
        institution.setName("Institution");
        institution.setType(institutionType);
        TissueLocatorHibernateUtil.getCurrentSession().save(institution);

        TissueLocatorUser user = new TissueLocatorUser();
        user.setFirstName("First");
        user.setLastName("Last");
        user.setEmail("email@address.com");
        user.setUsername("email@address.com");
        user.setAddress(getAddress());
        user.setTitle("Title");
        user.setDegree("Degree");
        user.setDepartment("Department");
        user.setResearchArea("Research Area");
        user.setStatus(status);
        Password password = new Password();
        password.setValue("Password123");
        password.setType(PasswordType.PLAINTEXT);
        user.setPassword(password);
        user.setInstitution(institution);
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(user);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        return getTissueLocatorUser(id);
    }

    private TissueLocatorUser getTissueLocatorUser(Long id) {
        return (TissueLocatorUser) TissueLocatorHibernateUtil
            .getCurrentSession().load(TissueLocatorUser.class, id);
    }

    private Long updateAccountStatus(AccountStatus status1, AccountStatus status2) {
        TissueLocatorUser user = createTissueLocatorUser(status1);
        user.setStatus(status2);
        user.setStatusTransitionComment(AccountStatus.INACTIVE.equals(status2) ? "test" : null);
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(user);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        return id;
    }

}
