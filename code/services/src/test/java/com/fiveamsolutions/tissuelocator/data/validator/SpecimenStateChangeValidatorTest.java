/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data.validator;

import static org.junit.Assert.assertFalse;

import org.hibernate.validator.InvalidStateException;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenPersistenceTest;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimentStateChangeValidator;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author smiller
 *
 */
public class SpecimenStateChangeValidatorTest extends AbstractHibernateTestCase {
    
    private Specimen createTestSpecimen(SpecimenStatus initialStatus) {
        Specimen s = new SpecimenPersistenceTest().getValidObjects()[0];
        s.setStatus(initialStatus);
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        return (Specimen) TissueLocatorHibernateUtil.getCurrentSession().load(Specimen.class, id);
    }
    
    private void runTest(SpecimenStatus s1, SpecimenStatus s2) {
        Specimen s = createTestSpecimen(s1);
        s.setStatus(s2);
        TissueLocatorHibernateUtil.getCurrentSession().save(s);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
    }
    
    /**
     * test the validator on a non specimen.
     */
    @Test
    public void testNonSpeciment() {
        assertFalse(new SpecimentStateChangeValidator().isValid(new String()));
    }
    
    /**
     * Test changing from the available status.
     */
    @Test
    public void testAvailableToAvailable() {
        runTest(SpecimenStatus.AVAILABLE, SpecimenStatus.AVAILABLE);
    }
    
    /**
     * Test changing from the available status.
     */
    @Test
    public void testAvailableToUnavailable() {
        runTest(SpecimenStatus.AVAILABLE, SpecimenStatus.UNAVAILABLE);
    }
    
    /**
     * Test changing from the available status.
     */
    @Test
    public void testAvailableToUnderReview() {
        runTest(SpecimenStatus.AVAILABLE, SpecimenStatus.UNDER_REVIEW);
    }
    
    /**
     * Test changing from the available status.
     */
    @Test
    public void testAvailableToPendingShipment() {
        runTest(SpecimenStatus.AVAILABLE, SpecimenStatus.PENDING_SHIPMENT);
    }
    
    /**
     * Test changing from the available status.
     */
    @Test(expected = InvalidStateException.class)
    public void testAvailableToReturned() {
        runTest(SpecimenStatus.AVAILABLE, SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
    }
    
    /**
     * Test changing from the available status.
     */
    @Test(expected = InvalidStateException.class)
    public void testAvailableToShipped() {
        runTest(SpecimenStatus.AVAILABLE, SpecimenStatus.SHIPPED);
    }
    
    /**
     * Test changing from the available status.
     */
    @Test
    public void testAvailableToDestroyed() {
        runTest(SpecimenStatus.AVAILABLE, SpecimenStatus.DESTROYED);
    }
    
    /**
     * Test changing from the under review status.
     */
    @Test
    public void testUnderReviewToUnderReview() {
        runTest(SpecimenStatus.UNDER_REVIEW, SpecimenStatus.UNDER_REVIEW);
    }
    
    /**
     * Test changing from the under review status.
     */
    @Test
    public void testUnderReviewToAvailable() {
        runTest(SpecimenStatus.UNDER_REVIEW, SpecimenStatus.AVAILABLE);
    }
    
    /**
     * Test changing from the under review status.
     */
    @Test
    public void testUnderReviewToPendingShipment() {
        runTest(SpecimenStatus.UNDER_REVIEW, SpecimenStatus.PENDING_SHIPMENT);
    }
    
    /**
     * Test changing from the under review status.
     */
    @Test
    public void testUnderReviewToUnavailable() {
        runTest(SpecimenStatus.UNDER_REVIEW, SpecimenStatus.UNAVAILABLE);
    }
    
    /**
     * Test changing from the under review status.
     */
    @Test(expected = InvalidStateException.class)
    public void testUnderReviewToShipped() {
        runTest(SpecimenStatus.UNDER_REVIEW, SpecimenStatus.SHIPPED);
    }
    
    /**
     * Test changing from the under review status.
     */
    @Test(expected = InvalidStateException.class)
    public void testUnderReviewToReturned() {
        runTest(SpecimenStatus.UNDER_REVIEW, SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
    }
    
    /**
     * Test changing from the under review status.
     */
    @Test
    public void testUnderReviewToDestroyed() {
        runTest(SpecimenStatus.UNDER_REVIEW, SpecimenStatus.DESTROYED);
    }
    
    /**
     * Test changing from the pending shipment status.
     */
    @Test
    public void testPendingShipmentToPendingShipment() {
        runTest(SpecimenStatus.PENDING_SHIPMENT, SpecimenStatus.PENDING_SHIPMENT);
    }
    
    /**
     * Test changing from the pending shipment status.
     */
    @Test
    public void testPendingShipmentToAvailable() {
        runTest(SpecimenStatus.PENDING_SHIPMENT, SpecimenStatus.AVAILABLE);
    }
    
    /**
     * Test changing from the pending shipment status.
     */
    @Test(expected = InvalidStateException.class)
    public void testPendingShipmentToUnderReview() {
        runTest(SpecimenStatus.PENDING_SHIPMENT, SpecimenStatus.UNDER_REVIEW);
    }
    
    /**
     * Test changing from the pending shipment status.
     */
    @Test
    public void testPendingShipmentToUnavailable() {
        runTest(SpecimenStatus.PENDING_SHIPMENT, SpecimenStatus.UNAVAILABLE);
    }
    
    /**
     * Test changing from the pending shipment status.
     */
    @Test
    public void testPendingShipmentToShipped() {
        runTest(SpecimenStatus.PENDING_SHIPMENT, SpecimenStatus.SHIPPED);
    }
    
    /**
     * Test changing from the pending shipment status.
     */
    @Test(expected = InvalidStateException.class)
    public void testPendingShipmentToReturned() {
        runTest(SpecimenStatus.PENDING_SHIPMENT, SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
    }
    
    /**
     * Test changing from the pending shipment status.
     */
    @Test
    public void testPendingShipmentToDestroyed() {
        runTest(SpecimenStatus.PENDING_SHIPMENT, SpecimenStatus.DESTROYED);
    }
    
    /**
     * Test changing from the unavailable status.
     */
    @Test
    public void testUnavailableToUnavailable() {
        runTest(SpecimenStatus.UNAVAILABLE, SpecimenStatus.UNAVAILABLE);
    }
    
    /**
     * Test changing from the unavailable status.
     */
    @Test
    public void testUnavailableToAvailable() {
        runTest(SpecimenStatus.UNAVAILABLE, SpecimenStatus.AVAILABLE);
    }
    
    /**
     * Test changing from the unavailable status.
     */
    @Test(expected = InvalidStateException.class)
    public void testUnavailableToUnderReview() {
        runTest(SpecimenStatus.UNAVAILABLE, SpecimenStatus.UNDER_REVIEW);
    }
    
    /**
     * Test changing from the unavailable status.
     */
    @Test(expected = InvalidStateException.class)
    public void testUnavailableToPendingShipment() {
        runTest(SpecimenStatus.UNAVAILABLE, SpecimenStatus.PENDING_SHIPMENT);
    }
    
    /**
     * Test changing from the unavailable status.
     */
    @Test(expected = InvalidStateException.class)
    public void testUnavailableToShipped() {
        runTest(SpecimenStatus.UNAVAILABLE, SpecimenStatus.SHIPPED);
    }
    
    /**
     * Test changing from the unavailable status.
     */
    @Test(expected = InvalidStateException.class)
    public void testUnavailableToReturned() {
        runTest(SpecimenStatus.UNAVAILABLE, SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
    }
    
    /**
     * Test changing from the unavailable status.
     */
    @Test
    public void testUnavailableToDestroyed() {
        runTest(SpecimenStatus.UNAVAILABLE, SpecimenStatus.DESTROYED);
    }
    
    /**
     * Test changing from the shipped status.
     */
    @Test(expected = InvalidStateException.class)
    public void testShippedToAvailable() {
        runTest(SpecimenStatus.SHIPPED, SpecimenStatus.AVAILABLE);
    }
    
    /**
     * Test changing from the shipped status.
     */
    @Test(expected = InvalidStateException.class)
    public void testShippedToUnderReview() {
        runTest(SpecimenStatus.SHIPPED, SpecimenStatus.UNDER_REVIEW);
    }
    
    /**
     * Test changing from the shipped status.
     */
    @Test(expected = InvalidStateException.class)
    public void testShippedToPendingShipment() {
        runTest(SpecimenStatus.SHIPPED, SpecimenStatus.PENDING_SHIPMENT);
    }
    
    /**
     * Test changing from the shipped status.
     */
    @Test(expected = InvalidStateException.class)
    public void testShippedToUnavailable() {
        runTest(SpecimenStatus.SHIPPED, SpecimenStatus.UNAVAILABLE);
    }
    
    /**
     * Test changing from the shipped status.
     */
    @Test
    public void testShippedToShipped() {
        runTest(SpecimenStatus.SHIPPED, SpecimenStatus.SHIPPED);
    }
    
    /**
     * Test changing from the shipped status.
     */
    @Test
    public void testShippedToReturned() {
        runTest(SpecimenStatus.SHIPPED, SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
    }
    
    /**
     * Test changing from the shipped status.
     */
    @Test
    public void testShippedToDestroyed() {
        runTest(SpecimenStatus.SHIPPED, SpecimenStatus.DESTROYED);
    }
    
    /**
     * Test changing from the Returned status.
     */
    @Test(expected = InvalidStateException.class)
    public void testReturnedToAvailable() {
        runTest(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION, SpecimenStatus.AVAILABLE);
    }
    
    /**
     * Test changing from the Returned status.
     */
    @Test(expected = InvalidStateException.class)
    public void testReturnedToUnderReview() {
        runTest(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION, SpecimenStatus.UNDER_REVIEW);
    }
    
    /**
     * Test changing from the Returned status.
     */
    @Test(expected = InvalidStateException.class)
    public void testReturnedToPendingShipment() {
        runTest(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION, SpecimenStatus.PENDING_SHIPMENT);
    }
    
    /**
     * Test changing from the Returned status.
     */
    @Test(expected = InvalidStateException.class)
    public void testReturnedToUnavailable() {
        runTest(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION, SpecimenStatus.UNAVAILABLE);
    }
    
    /**
     * Test changing from the Returned status.
     */
    @Test(expected = InvalidStateException.class)
    public void testReturnedToShipped() {
        runTest(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION, SpecimenStatus.SHIPPED);
    }
    
    /**
     * Test changing from the Returned status.
     */
    @Test
    public void testReturnedToReturned() {
        runTest(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION, SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
    }
    
    /**
     * Test changing from the Returned status.
     */
    @Test
    public void testReturnedToDestroyed() {
        runTest(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION, SpecimenStatus.DESTROYED);
    }
    
    /**
     * Test changing from the Destroyed status.
     */
    @Test(expected = InvalidStateException.class)
    public void testDestroyedToAvailable() {
        runTest(SpecimenStatus.DESTROYED, SpecimenStatus.AVAILABLE);
    }
    
    /**
     * Test changing from the Destroyed status.
     */
    @Test(expected = InvalidStateException.class)
    public void testDestroyedToUnderReview() {
        runTest(SpecimenStatus.DESTROYED, SpecimenStatus.UNDER_REVIEW);
    }
    
    /**
     * Test changing from the Destroyed status.
     */
    @Test(expected = InvalidStateException.class)
    public void testDestroyedToPendingShipment() {
        runTest(SpecimenStatus.DESTROYED, SpecimenStatus.PENDING_SHIPMENT);
    }
    
    /**
     * Test changing from the Destroyed status.
     */
    @Test(expected = InvalidStateException.class)
    public void testDestroyedToUnavailable() {
        runTest(SpecimenStatus.DESTROYED, SpecimenStatus.UNAVAILABLE);
    }
    
    /**
     * Test changing from the Destroyed status.
     */
    @Test(expected = InvalidStateException.class)
    public void testDestroyedToShipped() {
        runTest(SpecimenStatus.DESTROYED, SpecimenStatus.SHIPPED);
    }
    
    /**
     * Test changing from the Destroyed status.
     */
    @Test(expected = InvalidStateException.class)
    public void testDestroyedToReturned() {
        runTest(SpecimenStatus.DESTROYED, SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
    }
    
    /**
     * Test changing from the Destroyed status.
     */
    @Test
    public void testDestroyedToDestroyed() {
        runTest(SpecimenStatus.DESTROYED, SpecimenStatus.DESTROYED);
    }
}
