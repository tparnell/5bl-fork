/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.ApplicationSetting;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceBean;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.AggregateLineItemSeparator;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * Tests the separation of aggregate line items into
 * general and specific line items.
 * @author gvaughn
 *
 */
public class AggregateLineItemSeparatorTest extends AbstractHibernateTestCase {

    private static final String GENERAL_POPULATION_SORT_REGEX = "(No Criteria Specified)|(Birth Year)[^;]*";

    /**
     * Test line item separation.
     */
    @Test
    public void testLineItemSeparation() {

        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("general_population_sort_regex");
        setting.setValue(GENERAL_POPULATION_SORT_REGEX);

        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        List<AggregateSpecimenRequestLineItem> items = new ArrayList<AggregateSpecimenRequestLineItem>();
        AggregateSpecimenRequestLineItem[]  generalLineItems = createGeneralLineItems();
        AggregateSpecimenRequestLineItem[] specificLineItems = createSpecificLineItems();
        items.addAll(Arrays.asList(generalLineItems));
        items.addAll(Arrays.asList(specificLineItems));

        ApplicationSettingServiceLocal service = new ApplicationSettingServiceBean();
        AggregateLineItemSeparator separator = new AggregateLineItemSeparator(items, service);
        assertEquals(separator.getGeneralLineItems().size(), generalLineItems.length);
        assertEquals(separator.getSpecificLineItems().size(), specificLineItems.length);
        assertTrue(separator.getGeneralLineItems().containsAll(Arrays.asList(generalLineItems)));
        assertTrue(separator.getSpecificLineItems().containsAll(Arrays.asList(specificLineItems)));

        Collections.shuffle(items);
        separator = new AggregateLineItemSeparator(items, service);
        assertEquals(separator.getGeneralLineItems().size(), generalLineItems.length);
        assertEquals(separator.getSpecificLineItems().size(), specificLineItems.length);
        assertTrue(separator.getGeneralLineItems().containsAll(Arrays.asList(generalLineItems)));
        assertTrue(separator.getSpecificLineItems().containsAll(Arrays.asList(specificLineItems)));
    }

    private AggregateSpecimenRequestLineItem[] createGeneralLineItems() {
        return new AggregateSpecimenRequestLineItem[]{
            createUniqueLineItem("No Criteria Specified"),
            createUniqueLineItem("Birth Year: 1908"),
            createUniqueLineItem("Birth Year: 1908 - 1918"),
        };
    }

    private AggregateSpecimenRequestLineItem[] createSpecificLineItems() {
        return new AggregateSpecimenRequestLineItem[]{
                createUniqueLineItem("Condition: Cancer"),
                createUniqueLineItem("Condition: Cancer; Birth Year: 1908"),
                createUniqueLineItem("Birth Year: 1908; disease"),
                createUniqueLineItem("Condition: Cancer; Birth Year: 1908; disease"),
                createUniqueLineItem(""),
                createUniqueLineItem("invalid")
        };
    }

    private AggregateSpecimenRequestLineItem createUniqueLineItem(String criteria) {
        return createUniqueLineItem("test institution", criteria);
    }

    private AggregateSpecimenRequestLineItem createUniqueLineItem(String instName, String criteria) {
        AggregateSpecimenRequestLineItem lineItem = new AggregateSpecimenRequestLineItem();
        lineItem.setInstitution(new Institution());
        lineItem.getInstitution().setName(instName);
        lineItem.setCriteria(criteria);
        lineItem.setQuantity(1);
        lineItem.setNote("note");
        return lineItem;
    }

    /**
     * Test sorting of general aggregate line items in the separator.
     */
    @Test
    public void testGeneralLineItemSorting() {
        List<AggregateSpecimenRequestLineItem> items = new ArrayList<AggregateSpecimenRequestLineItem>();
        items.add(createUniqueLineItem("bTestInst", "No Criteria Specified"));
        items.add(createUniqueLineItem("aTestInst", "No Criteria Specified"));
        items.add(createUniqueLineItem("bTestInst", "Birth Year: 1908"));
        ApplicationSettingServiceLocal service = new ApplicationSettingServiceBean();
        AggregateLineItemSeparator separator = new AggregateLineItemSeparator(items, service);
        List<AggregateSpecimenRequestLineItem> sorted = separator.getGeneralLineItems();
        assertEquals("aTestInst", sorted.get(0).getInstitution().getName());
        assertEquals("No Criteria Specified", sorted.get(0).getCriteria());
        assertEquals("bTestInst", sorted.get(1).getInstitution().getName());
        assertEquals("Birth Year: 1908", sorted.get(1).getCriteria());
        assertEquals("bTestInst", sorted.get(2).getInstitution().getName());
        assertEquals("No Criteria Specified", sorted.get(2).getCriteria());
    }

    /**
     * Test sorting of specific aggregate line items in the separator.
     */
    @Test
    public void testSpecificLineItemSorting() {
        List<AggregateSpecimenRequestLineItem> items = new ArrayList<AggregateSpecimenRequestLineItem>();
        items.add(createUniqueLineItem("bTestInst", "Condition: Cancer"));
        items.add(createUniqueLineItem("aTestInst", "Condition: Cancer"));
        items.add(createUniqueLineItem("bTestInst", "Condition: Cancer; Birth Year: 1908; disease"));
        ApplicationSettingServiceLocal service = new ApplicationSettingServiceBean();
        AggregateLineItemSeparator separator = new AggregateLineItemSeparator(items, service);
        List<AggregateSpecimenRequestLineItem> sorted = separator.getSpecificLineItems();
        assertEquals("aTestInst", sorted.get(0).getInstitution().getName());
        assertEquals("Condition: Cancer", sorted.get(0).getCriteria());
        assertEquals("bTestInst", sorted.get(1).getInstitution().getName());
        assertEquals("Condition: Cancer", sorted.get(1).getCriteria());
        assertEquals("bTestInst", sorted.get(2).getInstitution().getName());
        assertEquals("Condition: Cancer; Birth Year: 1908; disease", sorted.get(2).getCriteria());
    }
}
