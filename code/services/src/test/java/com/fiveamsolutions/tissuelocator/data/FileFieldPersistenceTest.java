/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;

import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsHibernateHelper;
import com.fiveamsolutions.nci.commons.validator.ValidationError;
import com.fiveamsolutions.tissuelocator.data.extension.FileDynamicFieldDefinition;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
public class FileFieldPersistenceTest extends AbstractPersistenceTest<FileDynamicFieldDefinition> {

    /**
     * {@inheritDoc}
     */
    @Override
    public FileDynamicFieldDefinition[] getValidObjects() {
        FileDynamicFieldDefinition[] definitions = new FileDynamicFieldDefinition[2];

        FileDynamicFieldDefinition definition = new FileDynamicFieldDefinition();
        definition.setEntityClassName(SpecimenRequest.class.getName());
        definition.setReferencedEntityClassName(LobHolder.class.getName());
        definition.setFieldDisplayName("file field 1");
        definition.setFieldName("file1");
        definition.setContentTypeFieldName("file1ContentType");
        definition.setFileNameFieldName("file1FileName");
        definition.setNullable(true);
        definition.setSearchable(false);
        definitions[0] = definition;

        definition = new FileDynamicFieldDefinition();
        definition.setEntityClassName(SpecimenRequest.class.getName());
        definition.setReferencedEntityClassName(LobHolder.class.getName());
        definition.setFieldDisplayName("file field 2");
        definition.setFieldName("file2");
        definition.setContentTypeFieldName("file2ContentType");
        definition.setFileNameFieldName("file2FileName");
        definition.setNullable(false);
        definition.setSearchable(false);
        definitions[1] = definition;

        return definitions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(FileDynamicFieldDefinition expected, FileDynamicFieldDefinition actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
        assertTrue(CollectionUtils.isEqualCollection(expected.getFieldNames(), actual.getFieldNames()));
    }

    /**
     * test validation.
     */
    @Test
    public void testValidation() {
        FileDynamicFieldDefinition[] definitions = getValidObjects();
        FileDynamicFieldDefinition d1 = definitions[0];
        FileDynamicFieldDefinition d2 = definitions[1];

        SpecimenRequest request = new SpecimenRequest();
        request.getCustomProperties().put(d2.getFieldName(), new File("path"));
        request.getCustomProperties().put(d2.getContentTypeFieldName(), "content type");
        request.getCustomProperties().put(d2.getFileNameFieldName(), "file name");

        Collection<ValidationError> errors = d1.validate(request);
        assertTrue(errors.isEmpty());
        errors = d2.validate(request);
        assertTrue(errors.isEmpty());

        request.getCustomProperties().put(d2.getFieldName(), null);
        errors = d1.validate(request);
        assertTrue(errors.isEmpty());
        errors = d2.validate(request);
        assertFalse(errors.isEmpty());

        request.getCustomProperties().put(d2.getFieldName(), new File("path"));
        request.getCustomProperties().put(d2.getContentTypeFieldName(), null);
        errors = d1.validate(request);
        assertTrue(errors.isEmpty());
        errors = d2.validate(request);
        assertFalse(errors.isEmpty());

        request.getCustomProperties().put(d2.getContentTypeFieldName(), "content type");
        request.getCustomProperties().put(d2.getFileNameFieldName(), null);
        errors = d1.validate(request);
        assertTrue(errors.isEmpty());
        errors = d2.validate(request);
        assertFalse(errors.isEmpty());
    }

    /**
     * test persistence.
     */
    @Test
    public void testPersistence() {
        DynamicExtensionsHibernateHelper hh = TissueLocatorHibernateUtil.getHibernateHelper();
        FileDynamicFieldDefinition d2 = getValidObjects()[1];
        d2.addToConfiguration(hh.getConfiguration());
        hh.initialize();

        SpecimenRequest request = new SpecimenRequestPersistenceTest().getValidObjects()[0];
        Long lobId = (Long) hh.getCurrentSession().save(new LobHolder(new byte[] {1}));
        request.getCustomProperties().put(d2.getFieldName(), hh.getCurrentSession().load(LobHolder.class, lobId));
        request.getCustomProperties().put(d2.getContentTypeFieldName(), "content type");
        request.getCustomProperties().put(d2.getFileNameFieldName(), "file name");

        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        Long id = service.savePersistentObject(request);
        SpecimenRequest retrieved = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(retrieved);
        assertNotNull(retrieved.getCustomProperties().get(d2.getFieldName()));
        assertEquals(lobId, ((LobHolder) request.getCustomProperties().get(d2.getFieldName())).getId());
        assertNotNull(retrieved.getCustomProperties().get(d2.getContentTypeFieldName()));
        assertEquals("content type", request.getCustomProperties().get(d2.getContentTypeFieldName()));
        assertNotNull(retrieved.getCustomProperties().get(d2.getFileNameFieldName()));
        assertEquals("file name", request.getCustomProperties().get(d2.getFileNameFieldName()));
    }
}

