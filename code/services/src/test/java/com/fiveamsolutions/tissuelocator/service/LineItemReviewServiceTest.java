/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;

import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.tissuelocator.data.ReviewComment;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.test.AbstractSpecimenRequestTest;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author cgoina
 *
 */
public class LineItemReviewServiceTest extends AbstractSpecimenRequestTest {

    /**
     * Tests that appropriate emails are sent when an internal comment is added to a request.
     *
     * @throws MessagingException on error
     * @throws IOException on testEmail error
     */
    @Test
    public void testAddReviewComment() throws MessagingException, IOException {

        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);

        TissueLocatorUser commenter = null;
        Set<TissueLocatorUser> otherStateAdmins = new HashSet<TissueLocatorUser>();
        for (SpecimenRequestLineItem item : request.getLineItems()) {
            // create an institution administrator
            TissueLocatorUser institutionAdmin = createUserInRole(Role.REVIEW_COMMENTER, item.getSpecimen()
                    .getExternalIdAssigner());
            // make this user also a request administrator so that it had permissions on a tissue request
            institutionAdmin.getRoles().add(getApplicationRoleByName(Role.TISSUE_REQUEST_ADMIN, true));
            if (commenter == null) {
                commenter = institutionAdmin;
            } else {
                otherStateAdmins.add(institutionAdmin);
            }
        }
        ReviewComment comment = new ReviewComment();
        comment.setUser(commenter);
        comment.setComment("Test comment to be added to a request");
        comment.setDate(new Date());

        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        LineItemReviewServiceLocal service = TissueLocatorRegistry.getServiceLocator()
                .getLineItemReviewService();
        service.addReviewComment(request, comment);

        assertEquals(0, Mailbox.get(commenter.getEmail()).size());
        for (TissueLocatorUser otherStateAdmin : otherStateAdmins) {
            assertEquals(1, Mailbox.get(otherStateAdmin.getEmail()).size());
            testEmail(otherStateAdmin.getEmail(), "comment", "comment");
        }

        MailUtils.setMailEnabled(false);
    }
}
