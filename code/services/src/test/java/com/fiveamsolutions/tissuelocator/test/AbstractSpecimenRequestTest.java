/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.time.DateUtils;

import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.nci.commons.data.security.UserGroup;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.AbstractPersistenceTest;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.FundingStatus;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.IrbApprovalStatus;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.PrincipalInvestigator;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.ShippingInformation;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenPersistenceTest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestPersistenceTestHelper;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.Study;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.data.YesNoResponse;
import com.fiveamsolutions.tissuelocator.data.code.FundingSource;
import com.fiveamsolutions.tissuelocator.service.LineItemReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
public abstract class AbstractSpecimenRequestTest extends AbstractPersistenceTest<SpecimenRequest> {

    /**
     * three.
     */
    protected static final int THREE = 3;

    /**
     * biospecimen price.
     */
    protected static final double PRICE = 75.5;

    /**
     * request review period.
     */
    protected static final int REVIEW_PERIOD = 10;

    /**
     * request voting period.
     */
    public static final int VOTING_PERIOD = 15;

    /**
     * minimum percentage of available votes.
     */
    protected static final int MIN_AVAILABLE = 60;

    /**
     * minimum percentage of winning votes.
     */
    protected static final int MIN_WINNING = 65;

    /**
     * line item count for the background thread test.
     */
    protected static final int BG_TEST_LINE_ITEM_COUNT = 3;

    /**
     * reviewer assignment period.
     */
    protected static final int REVIEWER_ASSIGNMENT_PERIOD = 1;

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenRequestServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
    }

    /**
     * @return the line item review service.
     */
    protected LineItemReviewServiceLocal getLineItemReviewService() {
        return TissueLocatorRegistry.getServiceLocator().getLineItemReviewService();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenRequest[] getValidObjects() {
        SpecimenRequest request1 = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        SpecimenRequest request2 = createRequest(false, SpecimenStatus.UNDER_REVIEW);
        return new SpecimenRequest[] {request1, request2};
    }

    /**
     * create a request.
     * @param includeOptional whether to include optional properties
     * @param status new status of specimen to set
     * @return a new specimen request
     */
    protected SpecimenRequest createRequest(boolean includeOptional, SpecimenStatus status) {
        SpecimenRequestPersistenceTestHelper.createRequiredApplicationSettings();
        FundingSource fs = SpecimenRequestPersistenceTestHelper.createFundingSource();

        SpecimenRequest request = new SpecimenRequest();
        TissueLocatorUser u = getCurrentUser();

        Study study = new Study();
        study.setIrbName("irb");
        study.setIrbRegistrationNumber("1234");
        study.setIrbApprovalNumber("1234");
        study.setIrbApprovalExpirationDate(DateUtils.addDays(new Date(), VOTING_PERIOD));
        study.setProtocolTitle("protocol");
        study.setStudyAbstract("abstract");
        study.setIrbApprovalStatus(IrbApprovalStatus.APPROVED);
        study.setFundingStatus(FundingStatus.FUNDED);
        study.setFundingSources(new ArrayList<FundingSource>());
        study.getFundingSources().add(fs);
        EjbTestHelper.getGenericServiceBean(Study.class).savePersistentObject(study);

        PrincipalInvestigator pi = new PrincipalInvestigator();
        pi.setFirstName("pi");
        pi.setLastName("pi");
        pi.setOrganization(u.getInstitution());
        pi.setResume(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));
        pi.setEmail("pi@example.com");
        pi.setAddress(getAddress());
        pi.setCertified(YesNoResponse.YES);
        pi.setInvestigated(YesNoResponse.NO);
        EjbTestHelper.getGenericServiceBean(PrincipalInvestigator.class).savePersistentObject(pi);

        ShippingInformation shipment = new ShippingInformation();
        Person recipient = new Person();
        recipient.setFirstName("recipient");
        recipient.setLastName("recipient");
        recipient.setOrganization(u.getInstitution());
        recipient.setEmail(u.getEmail());
        recipient.setAddress(getAddress());
        EjbTestHelper.getGenericServiceBean(Person.class).savePersistentObject(recipient);
        shipment.setRecipient(recipient);
        EjbTestHelper.getGenericServiceBean(ShippingInformation.class).savePersistentObject(shipment);

        if (includeOptional) {
            request.setLineItems(new HashSet<SpecimenRequestLineItem>());
            request.setRevisionComment("revision comment");
            request.setProspectiveCollectionNotes("prospective collection notes");
            request.setGeneralComment("general comment");
            SpecimenPersistenceTest specimenTest = new SpecimenPersistenceTest();
            Specimen[] specimens = specimenTest.getValidObjects();
            for (Specimen s : specimens) {
                s.setStatus(status);
                TissueLocatorRegistry.getServiceLocator().getSpecimenService().savePersistentObject(s);
            }

            SpecimenRequestLineItem lineItem = new SpecimenRequestLineItem();
            lineItem.setNote("test note");
            lineItem.setQuantity(new BigDecimal(1));
            lineItem.setQuantityUnits(QuantityUnits.ML);
            lineItem.setSpecimen(specimens[0]);
            request.getLineItems().add(lineItem);

            lineItem = new SpecimenRequestLineItem();
            lineItem.setQuantity(new BigDecimal(1));
            lineItem.setQuantityUnits(QuantityUnits.ML);
            lineItem.setSpecimen(specimens[1]);
            lineItem.setFinalPrice(new BigDecimal(PRICE));
            request.getLineItems().add(lineItem);

            request.setAggregateLineItems(new HashSet<AggregateSpecimenRequestLineItem>());
            AggregateSpecimenRequestLineItem aggregateLineItem = new AggregateSpecimenRequestLineItem();
            aggregateLineItem.setNote("test note 1");
            aggregateLineItem.setQuantity(1);
            aggregateLineItem.setCriteria("criteria 1");
            aggregateLineItem.setInstitution(specimens[0].getExternalIdAssigner());
            request.getAggregateLineItems().add(aggregateLineItem);

            aggregateLineItem = new AggregateSpecimenRequestLineItem();
            aggregateLineItem.setNote("test note 1");
            aggregateLineItem.setQuantity(2);
            aggregateLineItem.setCriteria("criteria 2");
            aggregateLineItem.setInstitution(specimens[1].getExternalIdAssigner());
            request.getAggregateLineItems().add(aggregateLineItem);

            request.setReviewers(new HashSet<Institution>());
            request.getReviewers().add(getCurrentUser().getInstitution());

            pi.getAddress().setLine2("line 2");
            pi.getAddress().setFax("0987654321");
            recipient.getAddress().setLine2("line 2");
            recipient.getAddress().setFax("0987654321");

            study.setPreliminary(Boolean.TRUE);
            study.setFundingSources(new ArrayList<FundingSource>());
            study.getFundingSources().add(fs);
            study.setProtocolDocument(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));
            study.setIrbExemptionLetter(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));

            shipment.setInstructions("instructions");
            shipment.setBillingRecipient(recipient);
        }

        request.setRequestor(u);
        request.setStatus(RequestStatus.PENDING);
        assertNotNull(request.getStatus().getResourceKey());
        assertFalse(Arrays.asList(RequestStatus.getSubmittedValues()).contains(RequestStatus.DRAFT));
        request.setUpdatedDate(new Date());
        request.setStudy(study);
        request.setInvestigator(pi);
        request.setShipment(shipment);
        return request;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(SpecimenRequest expected, SpecimenRequest actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
        assertNotNull(actual.getOrderLineItems());
        assertTrue(actual.getOrderLineItems().isEmpty());
    }

    /**
     * create an institutional reviewer user.
     * @param inst the institution for the user
     * @return an institutional reviewer user in the given institution
     */
    protected TissueLocatorUser createInstitutionalReviewer(Institution inst) {
        return createUserInRoles(inst, Role.INSTITUTIONAL_REVIEW_VOTER, Role.TISSUE_REQUEST_ADMIN);
    }

    /**
     * create a line item reviewer user.
     * @param inst the institution for the user
     * @return a line item reviewer user in the given institution
     */
    protected TissueLocatorUser createLineItemReviewer(Institution inst) {
        return createUserInRoles(inst, Role.LINE_ITEM_REVIEW_VOTER, Role.TISSUE_REQUEST_ADMIN);
    }

    /**
     * create a consortium reviewer user.
     * @param inst the institution for the user
     * @return a consortium reviewer user in the given institution
     */
    protected TissueLocatorUser createConsortiumReviewer(Institution inst) {
        return createUserInRoles(inst, Role.CONSORTIUM_REVIEW_VOTER, Role.TISSUE_REQUEST_ADMIN);
    }

    /**
     * create a reviewer assigner user.
     * @param inst the institution for the user
     * @return a reviewer assigner user in the given institution
     */
    protected TissueLocatorUser createReviewerAssigner(Institution inst) {
        return createUserInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER, inst);
    }

    /**
     * create a lead reviewer user.
     * @param inst the institution for the user
     * @return a lead reviewer user in the given institution
     */
    protected TissueLocatorUser createLeadReviewer(Institution inst) {
        return createUserInRole(Role.LEAD_REVIEWER, inst);
    }

    /**
     * create a tissue tech user.
     * @param inst the institution for the user
     * @return a tissue tech user in the given institution
     */
    public TissueLocatorUser createTissueTechnician(Institution inst) {
        return createUserInRole(Role.SHIPMENT_ADMIN, inst);
    }

    /**
     * Create a review decision notifier.
     * @param inst the institution for the user
     * @return a review decision notifier in the given institution
     */
    protected TissueLocatorUser createReviewDecisionNotifier(Institution inst) {
        return createUserInRole(Role.REVIEW_DECISION_NOTIFIER, inst);
    }

    /**
     * create the consortium review group.
     * @param consortiumReviewRole the consortium review role to assign to the group
     * @return the consortium review group
     */
    protected UserGroup createConsortiumReviewGroup(ApplicationRole consortiumReviewRole) {
        UserGroup ug = new UserGroup();
        ug.setName("test group");
        ug.getRoles().add(consortiumReviewRole);
        TissueLocatorHibernateUtil.getCurrentSession().save(ug);
        return ug;
    }

    /**
     * create the consortium review role.
     * @return the consortium review role.
     */
    protected ApplicationRole createConsortiumReviewRole() {
        ApplicationRole consortiumReviewRole = new ApplicationRole();
        consortiumReviewRole.setName(Role.CONSORTIUM_REVIEW_VOTER.getName());
        TissueLocatorHibernateUtil.getCurrentSession().save(consortiumReviewRole);
        return consortiumReviewRole;
    }

    /**
     * create 3 member review committee, one assigned role directly, two in group assigned to role.
     * @return the review committee
     */
    protected TissueLocatorUser[] getReviewCommittee() {
        ApplicationRole reviewRole = createConsortiumReviewRole();
        UserGroup ug = createConsortiumReviewGroup(reviewRole);
        TissueLocatorUser reviewer1 = createUserSkipRole();
        reviewer1.getGroups().add(ug);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(reviewer1);
        TissueLocatorUser reviewer2 = createUserSkipRole();
        reviewer2.getGroups().add(ug);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(reviewer2);
        TissueLocatorUser reviewer3 = createUserSkipRole();
        reviewer3.getRoles().add(reviewRole);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(reviewer3);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        return new TissueLocatorUser[] {reviewer1, reviewer2, reviewer3};
    }

    /**
     * Create a reviewer.
     * @param instToConsortiumReviewerMap map to which reviewer will be added.
     * @param ug user group.
     * @return the reviewer.
     */
    protected TissueLocatorUser createReviewer(HashMap<Institution, TissueLocatorUser>
        instToConsortiumReviewerMap, UserGroup ug) {
        TissueLocatorUser reviewer1 = createUserSkipRole();
        instToConsortiumReviewerMap.put(reviewer1.getInstitution(), reviewer1);
        reviewer1.getGroups().add(ug);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(reviewer1);
        return reviewer1;
    }

    /**
     * Cast a line item review vote.
     * @param request specimen request.
     * @param lineItemReviewer reviewer.
     * @param vote vote to be cast.
     * @throws MessagingException on error.
     */
    protected void castLineItemReviewVote(SpecimenRequest request, TissueLocatorUser lineItemReviewer, Vote vote)
            throws MessagingException {
        for (AggregateSpecimenRequestLineItem lineItem : request.getAggregateLineItems()) {
            if (lineItem.getInstitution().getId().equals(lineItemReviewer.getInstitution().getId())) {
                lineItem.getVote().setVote(vote);
                lineItem.getVote().setComment("comment");
                UsernameHolder.setUser(lineItemReviewer.getUsername());
                getLineItemReviewService().addLineItemReviews(request);
                break;
            }
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
    }

    /**
     * Add aggregate line items for line item review.
     * @param request specimen request.
     * @param institution institution.
     */
    protected void addAggregateLineItemForLineItemReview(SpecimenRequest request, Institution institution) {
        AggregateSpecimenRequestLineItem lineItem = new AggregateSpecimenRequestLineItem();
        lineItem.setInstitution(institution);
        lineItem.setQuantity(1);
        lineItem.setCriteria("criteria");
        request.getAggregateLineItems().add(lineItem);
    }

    /**
     * Add specimens for institutional review.
     * @param request specimen request.
     * @param idAssigner is assigner.
     * @param templateSpecimen specimen.
     */
    protected void addSpecimenForInstitutionalReview(SpecimenRequest request, TissueLocatorUser idAssigner,
            Specimen templateSpecimen) {
        UsernameHolder.setUser(idAssigner.getUsername());
        Specimen newSpecimen = SpecimenRequestPersistenceTestHelper.createNewSpecimen(request, idAssigner,
                templateSpecimen);
        newSpecimen.setStatus(SpecimenStatus.AVAILABLE);
        TissueLocatorRegistry.getServiceLocator().getSpecimenService().savePersistentObject(newSpecimen);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
    }

    /**
     * Set all specimens to status available.
     * @param lineItems Set of SpecimenRequestLineItem
     */
    public void setSpecimensAvailable(Set<SpecimenRequestLineItem> lineItems) {
        for (SpecimenRequestLineItem lineItem : lineItems) {
            lineItem.getSpecimen().setStatus(SpecimenStatus.AVAILABLE);
            TissueLocatorRegistry.getServiceLocator().
                getSpecimenService().savePersistentObject(lineItem.getSpecimen());
        }
    }
}
