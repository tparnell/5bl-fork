/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewComment;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.ShipmentStatus;
import com.fiveamsolutions.tissuelocator.data.ShippingMethod;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestPersistenceTest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestPersistenceTestHelper;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenReceiptQuality;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenRequestSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenRequestSortCriterion;
import com.fiveamsolutions.tissuelocator.test.AbstractSpecimenRequestTest;
import com.fiveamsolutions.tissuelocator.test.InstitutionPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.util.UserActionAnalyzer;

/**
 * @author ddasgupta
 *
 */
public class SpecimenRequestSearchTest extends AbstractSpecimenRequestTest {


    /**
     * test search.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        SpecimenRequest example = new SpecimenRequest();
        example.setRequestor(getCurrentUser());
        example.setStatus(RequestStatus.PENDING);
        SearchCriteria<SpecimenRequest> sc = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        PageSortParams<SpecimenRequest> psp = new PageSortParams<SpecimenRequest>(PAGE_SIZE, 0,
                SpecimenRequestSortCriterion.ID, true);
        List<SpecimenRequest> results = getService().search(sc, psp);
        assertEquals(2, results.size());
        assertEquals(2, getService().count(sc));
        assertTrue(results.get(0).getId().compareTo(results.get(1).getId()) > 0);
        example.setStatus(RequestStatus.APPROVED);
        results = getService().search(sc, psp);
        assertEquals(0, results.size());
        assertEquals(0, getService().count(sc));
    }

    /**
     * test search for requests needing reviewer assignment.
     */
    @Test
    public void testAssignReviewerSearch() {
        SpecimenRequest example = new SpecimenRequest();
        example.setRequestor(getCurrentUser());
        example.setStatus(RequestStatus.PENDING);
        verifyAssignReviewerSearch(example, 0, 0, null);

        SpecimenRequest request = getNewlySubmittedRequest();
        for (SpecimenRequestReviewVote consVote : request.getConsortiumReviews()) {
            consVote.setUser(null);
        }
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyAssignReviewerSearch(example, 1, 1, id);

        example.setStatus(RequestStatus.APPROVED);
        verifyAssignReviewerSearch(example, 0, 0, id);

        example.setStatus(null);
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.APPROVED);
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyAssignReviewerSearch(example, 0, 0, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING);
        for (SpecimenRequestReviewVote consVote : request.getConsortiumReviews()) {
            consVote.setUser(getCurrentUser());
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyAssignReviewerSearch(example, 0, 0, id);

        TissueLocatorUser alternate = createUserSkipRole();
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        for (SpecimenRequestReviewVote consVote : request.getConsortiumReviews()) {
            consVote.setUser(null);
            consVote.setInstitution(alternate.getInstitution());
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyAssignReviewerSearch(example, 0, 1, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.getReviewers().clear();
        for (SpecimenRequestReviewVote consVote : request.getConsortiumReviews()) {
            consVote.setUser(null);
            consVote.setInstitution(getCurrentUser().getInstitution());
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyAssignReviewerSearch(example, 0, 0, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.getReviewers().add(getCurrentUser().getInstitution());
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyAssignReviewerSearch(example, 1, 0, id);

        SpecimenRequest request2 = getNewlySubmittedRequest();
        for (SpecimenRequestReviewVote consVote : request2.getConsortiumReviews()) {
            consVote.setUser(null);
        }
        Long id2 = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyAssignReviewerSearch(example, 2, 1, id2);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        for (SpecimenRequestReviewVote consVote : request.getConsortiumReviews()) {
            consVote.setUser(getCurrentUser());
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        request2 = getService().getPersistentObject(SpecimenRequest.class, id2);
        for (SpecimenRequestReviewVote consVote : request2.getConsortiumReviews()) {
            consVote.setUser(getCurrentUser());
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyAssignReviewerSearch(example, 0, 0, id);
    }

    private void verifyAssignReviewerSearch(SpecimenRequest example, int fullCount, int fixedCount, Long id) {
        SearchCriteria<SpecimenRequest> scFull = new SpecimenRequestSearchCriteria(example, true, false, false,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        verifySearch(scFull, fullCount, id);
        SearchCriteria<SpecimenRequest> scFixed = new SpecimenRequestSearchCriteria(example, true, false, false,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        verifySearch(scFixed, fixedCount, id);
        SearchCriteria<SpecimenRequest> scInst = new SpecimenRequestSearchCriteria(example, true, false, false,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.LINE_ITEM_REVIEW);
        verifySearch(scInst, 0, id);
    }

    private void verifySearch(SearchCriteria<SpecimenRequest> criteria, int count, Long id) {
        List<SpecimenRequest> results = getService().search(criteria);
        assertEquals(count, results.size());
        assertEquals(count, getService().count(criteria));
        if (count == 1) {
            assertEquals(id, results.get(0).getId());
        }
    }

    /**
     * test search for requests needing lead review.
     */
    @Test
    public void testLeadReviewerSearch() {
        SpecimenRequest example = new SpecimenRequest();
        example.setRequestor(getCurrentUser());
        example.setStatus(RequestStatus.PENDING);
        verifyLeadReviewerSearch(example, 0, null);

        SpecimenRequest request = getNewlySubmittedRequest();
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLeadReviewerSearch(example, 1, id);
        TissueLocatorUser alternate = createUserSkipRole();
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);

        example.setStatus(RequestStatus.APPROVED);
        verifyLeadReviewerSearch(example, 0, id);

        example.setStatus(null);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.APPROVED);
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLeadReviewerSearch(example, 0, id);

        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING);
        for (SpecimenRequestReviewVote consVote : request.getConsortiumReviews()) {
            consVote.setUser(null);
            consVote.setInstitution(alternate.getInstitution());
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLeadReviewerSearch(example, 0, id);

        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        for (SpecimenRequestReviewVote consVote : request.getConsortiumReviews()) {
            consVote.setUser(getCurrentUser());
            consVote.setInstitution(getCurrentUser().getInstitution());
        }
        request.getReviewers().clear();
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLeadReviewerSearch(example, 0, id);

        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.getReviewers().add(getCurrentUser().getInstitution());
        request.getComments().add(getReviewComment(getCurrentUser(), new Date()));
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLeadReviewerSearch(example, 0, id);

        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.getComments().add(getReviewComment(getCurrentUser(), new Date()));
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLeadReviewerSearch(example, 0, id);

        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.getComments().clear();
        request.getReviewers().clear();
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        request = getNewlySubmittedRequest();
        request.getComments().add(getReviewComment(getCurrentUser(), new Date()));
        TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLeadReviewerSearch(example, 0, id);
    }

    private void verifyLeadReviewerSearch(SpecimenRequest example, int fullCount, Long id) {
        SearchCriteria<SpecimenRequest> scFull = new SpecimenRequestSearchCriteria(example, false, true, false,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        verifySearch(scFull, fullCount, id);
        SearchCriteria<SpecimenRequest> scFixed = new SpecimenRequestSearchCriteria(example, false, true, false,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        verifySearch(scFixed, 0, id);
        SearchCriteria<SpecimenRequest> scInst = new SpecimenRequestSearchCriteria(example, false, true, false,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.LINE_ITEM_REVIEW);
        verifySearch(scInst, 0, id);
    }

    /**
     * test search for requests needing consortium review.
     */
    @Test
    public void testConsortiumReviewSearch() {
        SpecimenRequest example = new SpecimenRequest();
        example.setRequestor(getCurrentUser());
        example.setStatus(RequestStatus.PENDING);
        verifyConsortiumReviewerSearch(example, 0, 0, null);

        SpecimenRequest request = getNewlySubmittedRequest();
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyConsortiumReviewerSearch(example, 1, 1, id);

        example.setStatus(RequestStatus.APPROVED);
        verifyConsortiumReviewerSearch(example, 0, 0, id);

        example.setStatus(null);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.APPROVED);
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyConsortiumReviewerSearch(example, 0, 0, id);

        TissueLocatorUser alternate = createUserSkipRole();
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING);
        for (SpecimenRequestReviewVote consVote : request.getConsortiumReviews()) {
            consVote.setUser(alternate);
            consVote.setInstitution(alternate.getInstitution());
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyConsortiumReviewerSearch(example, 0, 0, id);

        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        for (SpecimenRequestReviewVote consVote : request.getConsortiumReviews()) {
            consVote.setUser(getCurrentUser());
            consVote.setInstitution(getCurrentUser().getInstitution());
            consVote.setVote(Vote.APPROVE);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyConsortiumReviewerSearch(example, 0, 0, id);
    }

    private void verifyConsortiumReviewerSearch(SpecimenRequest example, int fullCount, int fixedCount, Long id) {
        SearchCriteria<SpecimenRequest> scFull = new SpecimenRequestSearchCriteria(example, false, false, true,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        verifySearch(scFull, fullCount, id);
        SearchCriteria<SpecimenRequest> scFixed = new SpecimenRequestSearchCriteria(example, false, false, true,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        verifySearch(scFixed, fixedCount, id);
        SearchCriteria<SpecimenRequest> scInst = new SpecimenRequestSearchCriteria(example, false, false, true,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.LINE_ITEM_REVIEW);
        verifySearch(scInst, 0, id);
    }

    /**
     * test search for requests needing institutional review.
     */
    @Test
    public void testInstitutionalReviewSearch() {
        SpecimenRequest example = new SpecimenRequest();
        example.setRequestor(getCurrentUser());
        example.setStatus(RequestStatus.PENDING);
        verifyInstReviewerSearch(example, 0, null);

        SpecimenRequest request = getNewlySubmittedRequest();
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyInstReviewerSearch(example, 1, id);

        example.setStatus(RequestStatus.APPROVED);
        verifyInstReviewerSearch(example, 0, id);

        example.setStatus(null);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.APPROVED);
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyInstReviewerSearch(example, 0, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING);
        for (SpecimenRequestReviewVote instVote : request.getInstitutionalReviews()) {
            instVote.setVote(Vote.APPROVE);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyInstReviewerSearch(example, 0, id);

        TissueLocatorUser alternate = createUserSkipRole();
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        for (SpecimenRequestReviewVote instVote : request.getInstitutionalReviews()) {
            instVote.setVote(null);
            instVote.setUser(alternate);
            instVote.setInstitution(alternate.getInstitution());
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyInstReviewerSearch(example, 0, id);
    }

    private void verifyInstReviewerSearch(SpecimenRequest example, int fullCount, Long id) {
        SearchCriteria<SpecimenRequest> scFull = new SpecimenRequestSearchCriteria(example, false, false, false,
                true, false, false, false, false, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        verifySearch(scFull, fullCount, id);
        SearchCriteria<SpecimenRequest> scFixed = new SpecimenRequestSearchCriteria(example, false, false, false,
                true, false, false, false, false, false, getCurrentUser(), ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        verifySearch(scFixed, 0, id);
        SearchCriteria<SpecimenRequest> scInst = new SpecimenRequestSearchCriteria(example, false, false, false,
                true, false, false, false, false, false, getCurrentUser(), ReviewProcess.LINE_ITEM_REVIEW);
        verifySearch(scInst, 0, id);
    }

    /**
     * test search for requests needing line item review.
     */
    @Test
    public void testLineItemReviewSearch() {
        SpecimenRequest example = new SpecimenRequest();
        example.setStatus(RequestStatus.PENDING);
        verifyLineItemReviewerSearch(example, 0, null);

        SpecimenRequest request = getNewlySubmittedRequest();
        request.getConsortiumReviews().clear();
        request.getInstitutionalReviews().clear();
        for (AggregateSpecimenRequestLineItem lineItem : request.getAggregateLineItems()) {
            lineItem.setQuantity(1);
            lineItem.setCriteria("criteria");
            lineItem.setInstitution(getCurrentUser().getInstitution());
            lineItem.setVote(new SpecimenRequestReviewVote());
            lineItem.getVote().setInstitution(getCurrentUser().getInstitution());
            lineItem.getVote().setDate(new Date());
        }
        request.setRequestor(createUser(true, getCurrentUser().getInstitution()));
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLineItemReviewerSearch(example, 1, id);

        example.setStatus(RequestStatus.APPROVED);
        verifyLineItemReviewerSearch(example, 0, id);

        example.setStatus(null);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.APPROVED);
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLineItemReviewerSearch(example, 0, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING);
        for (AggregateSpecimenRequestLineItem lineItem : request.getAggregateLineItems()) {
            lineItem.getVote().setVote(Vote.APPROVE);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLineItemReviewerSearch(example, 0, id);

        TissueLocatorUser alternate = createUserSkipRole();
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        for (AggregateSpecimenRequestLineItem lineItem : request.getAggregateLineItems()) {
            lineItem.getVote().setVote(null);
            lineItem.getVote().setUser(alternate);
            lineItem.getVote().setInstitution(alternate.getInstitution());
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyLineItemReviewerSearch(example, 0, id);
    }

    private void verifyLineItemReviewerSearch(SpecimenRequest example, int instCount, Long id) {
        SearchCriteria<SpecimenRequest> scFull = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, true, false, false, false, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        verifySearch(scFull, 0, id);
        SearchCriteria<SpecimenRequest> scFixed = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, true, false, false, false, false, getCurrentUser(), ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        verifySearch(scFixed, 0, id);
        SearchCriteria<SpecimenRequest> scInst = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, true, false, false, false, false, getCurrentUser(), ReviewProcess.LINE_ITEM_REVIEW);
        verifySearch(scInst, instCount, id);
    }

    /**
     * test search for requests needing final comment.
     */
    @Test
    public void testFinalCommentSearch() {
        SpecimenRequest example = new SpecimenRequest();
        example.setRequestor(getCurrentUser());
        example.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        verifyFinalComment(example, 0, 0, null);

        SpecimenRequest request = getNewlySubmittedRequest();
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyFinalComment(example, 1, 1, id);

        example.setStatus(RequestStatus.APPROVED);
        verifyFinalComment(example, 0, 0, id);

        example.setStatus(null);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.APPROVED);
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyFinalComment(example, 0, 0, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.setExternalComment("new external comment");
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyFinalComment(example, 0, 0, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.setExternalComment(null);
        request.getReviewers().clear();
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyFinalComment(example, 0, 1, id);
    }

    private void verifyFinalComment(SpecimenRequest example, int fullCount, int fixedCount, Long id) {
        SearchCriteria<SpecimenRequest> scFull = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, true, false, false, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        verifySearch(scFull, fullCount, id);
        SearchCriteria<SpecimenRequest> scFixed = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, true, false, false, false, getCurrentUser(), ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        verifySearch(scFixed, fixedCount, id);
        SearchCriteria<SpecimenRequest> scInst = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, true, false, false, false, getCurrentUser(), ReviewProcess.LINE_ITEM_REVIEW);
        verifySearch(scInst, 0, id);
    }

    /**
     * test specimen request requiring reviewer notification search.
     */
    @Test
    public void testReviewerNotificationSearch() {
        SpecimenRequest example = new SpecimenRequest();
        example.setRequestor(getCurrentUser());
        example.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        verifyReviwerNotificationSearch(example, 0, null);

        SpecimenRequest request = getNewlySubmittedRequest();
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyReviwerNotificationSearch(example, 0, id);

        example.setStatus(RequestStatus.APPROVED);
        verifyReviwerNotificationSearch(example, 0, id);

        example.setStatus(null);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.APPROVED);
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyReviwerNotificationSearch(example, 0, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.setExternalComment("new external comment");
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyReviwerNotificationSearch(example, 0, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.setExternalComment(null);
        request.setFinalVote(RequestStatus.APPROVED);
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyReviwerNotificationSearch(example, 0, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.setExternalComment("new external comment");
        request.setFinalVote(RequestStatus.APPROVED);
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyReviwerNotificationSearch(example, 1, id);
    }

    private void verifyReviwerNotificationSearch(SpecimenRequest example, int fixedCount, Long id) {
        SearchCriteria<SpecimenRequest> scFull = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, true, false, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        verifySearch(scFull, 0, id);
        SearchCriteria<SpecimenRequest> scFixed = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, true, false, false, getCurrentUser(), ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        verifySearch(scFixed, fixedCount, id);
        SearchCriteria<SpecimenRequest> scInst = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, true, false, false, getCurrentUser(), ReviewProcess.LINE_ITEM_REVIEW);
        verifySearch(scInst, 0, id);
    }

    /**
     * test specimen request needs quality update search.
     */
    @Test
    public void testNeedsQualityUpdate() {
        SpecimenRequest request = getRequestWithOrder();
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        Shipment order = request.getOrders().iterator().next();
        Long orderId = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        SpecimenRequest example = new SpecimenRequest();
        example.setRequestor(getCurrentUser());
        verifyQualityUpdateSearch(example, 0, null);

        order = getService().getPersistentObject(Shipment.class, orderId);
        order.setStatus(ShipmentStatus.SHIPPED);
        TissueLocatorHibernateUtil.getCurrentSession().update(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyQualityUpdateSearch(example, 1, id);

        order = getService().getPersistentObject(Shipment.class, orderId);
        order.setStatus(ShipmentStatus.RECEIVED);
        TissueLocatorHibernateUtil.getCurrentSession().update(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyQualityUpdateSearch(example, 1, id);

        example = new SpecimenRequest();
        example.setRequestor(getCurrentUser());
        SpecimenReceiptQuality quality = SpecimenRequestPersistenceTestHelper.createReceiptQuality();
        verifyQualitySearchWithReceiptQuality(example, null, id, orderId, 1);
        verifyQualitySearchWithReceiptQuality(example, quality, id, orderId, 0);
    }

    private void verifyQualitySearchWithReceiptQuality(SpecimenRequest example, SpecimenReceiptQuality quality,
            Long requestId, Long orderId, int count) {
        Shipment order = getService().getPersistentObject(Shipment.class, orderId);
        Set<SpecimenRequestLineItem> lineItems = new HashSet<SpecimenRequestLineItem>();
        lineItems.addAll(order.getLineItems());
        Set<AggregateSpecimenRequestLineItem> aggregateLineItems = new HashSet<AggregateSpecimenRequestLineItem>();
        aggregateLineItems.addAll(order.getAggregateLineItems());

        // test with no aggregates

        order.getAggregateLineItems().clear();
        for (SpecimenRequestLineItem li : order.getLineItems()) {
            li.setReceiptQuality(quality);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyQualityUpdateSearch(example, count, requestId);

        // test with aggregates, no value
        order = getService().getPersistentObject(Shipment.class, orderId);
        order.getAggregateLineItems().addAll(aggregateLineItems);
        for (AggregateSpecimenRequestLineItem li : order.getAggregateLineItems()) {
            li.setReceiptQuality(null);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyQualityUpdateSearch(example, 1, requestId);

        // test with no line items
        order = getService().getPersistentObject(Shipment.class, orderId);
        order.getLineItems().clear();
        for (AggregateSpecimenRequestLineItem li : order.getAggregateLineItems()) {
            li.setReceiptQuality(quality);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyQualityUpdateSearch(example, count, requestId);

        // test with line items, no value
        order = getService().getPersistentObject(Shipment.class, orderId);
        for (SpecimenRequestLineItem li : lineItems) {
            order.getLineItems().add(getService().getPersistentObject(SpecimenRequestLineItem.class, li.getId()));
        }
        for (SpecimenRequestLineItem li : order.getLineItems()) {
            li.setReceiptQuality(null);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyQualityUpdateSearch(example, 1, requestId);

        // test with aggregates and line items
        order = getService().getPersistentObject(Shipment.class, orderId);
        for (SpecimenRequestLineItem li : order.getLineItems()) {
            li.setReceiptQuality(quality);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyQualityUpdateSearch(example, count, requestId);
    }

    private void verifyQualityUpdateSearch(SpecimenRequest example, int count, Long id) {
        SearchCriteria<SpecimenRequest> scFull = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, false, true, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        verifySearch(scFull, count, id);
        SearchCriteria<SpecimenRequest> scFixed = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, false, true, false, getCurrentUser(), ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        verifySearch(scFixed, count, id);
        SearchCriteria<SpecimenRequest> scInst = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, false, true, false, getCurrentUser(), ReviewProcess.LINE_ITEM_REVIEW);
        verifySearch(scInst, count, id);
    }

    /**
     * test institution restriction search.
     */
    @Test
    public void testInstitutionRestriction() {
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution inst = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test 1", true);
        InstitutionServiceLocal instService = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        Long instId = instService.savePersistentObject(inst);

        SpecimenRequest request = getRequestWithOrder();
        request.setRequestor(createUser(true, getCurrentUser().getInstitution()));
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        Shipment order = request.getOrders().iterator().next();
        TissueLocatorHibernateUtil.getCurrentSession().save(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        inst = instService.getPersistentObject(Institution.class, instId);

        SpecimenRequest example = new SpecimenRequest();
        verifyInstitutionRestrictionSearch(example, 1, id);

        request = (SpecimenRequest) TissueLocatorHibernateUtil.getCurrentSession().get(SpecimenRequest.class, id);
        for (Shipment shipment : request.getOrders()) {
            shipment.setSendingInstitution(inst);
            TissueLocatorHibernateUtil.getCurrentSession().update(shipment);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyInstitutionRestrictionSearch(example, 1, id);

        request = (SpecimenRequest) TissueLocatorHibernateUtil.getCurrentSession().get(SpecimenRequest.class, id);
        for (AggregateSpecimenRequestLineItem li : request.getAggregateLineItems()) {
            li.setInstitution(inst);
            TissueLocatorHibernateUtil.getCurrentSession().update(li);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyInstitutionRestrictionSearch(example, 0, id);

        request = (SpecimenRequest) TissueLocatorHibernateUtil.getCurrentSession().get(SpecimenRequest.class, id);
        for (Shipment shipment : request.getOrders()) {
            shipment.setSendingInstitution(getCurrentUser().getInstitution());
            TissueLocatorHibernateUtil.getCurrentSession().update(shipment);
        }
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyInstitutionRestrictionSearch(example, 1, id);
    }

    private void verifyInstitutionRestrictionSearch(SpecimenRequest example, int count, Long id) {
        SearchCriteria<SpecimenRequest> scFull = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, false, false, true, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        verifySearch(scFull, 0, id);
        SearchCriteria<SpecimenRequest> scFixed = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, false, false, true, getCurrentUser(), ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        verifySearch(scFixed, 0, id);
        SearchCriteria<SpecimenRequest> scInst = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, false, false, true, getCurrentUser(), ReviewProcess.LINE_ITEM_REVIEW);
        verifySearch(scInst, count, id);
    }

    /**
     * test specimen request search.
     */
    @Test
    public void testSpecimenRequestSearchCriteria() {
        SpecimenRequest request = getNewlySubmittedRequest();
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        SpecimenRequest example = new SpecimenRequest();
        example.setRequestor(getCurrentUser());
        example.setStatus(RequestStatus.PENDING);
        SearchCriteria<SpecimenRequest> sc = new SpecimenRequestSearchCriteria(example, true, true, true,
                true, false, true, false, false, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        List<SpecimenRequest> results = getService().search(sc);
        assertEquals(1, results.size());
        assertEquals(1, getService().count(sc));
        assertEquals(id, results.get(0).getId());

        request = getNewlySubmittedRequest();
        for (SpecimenRequestReviewVote consVote : request.getConsortiumReviews()) {
            consVote.setUser(null);
        }
        id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        results = getService().search(sc);
        assertEquals(2, results.size());
        assertEquals(2, getService().count(sc));
    }

    /**
     * tests the needs reviewer assignment method.
     */
    @Test
    public void testNeedsReviewerAssignment() {
        UserActionAnalyzer fullConsortiumAnalyzer = ReviewProcess.FULL_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        UserActionAnalyzer fixedConsortiumAnalyzer = ReviewProcess.FIXED_CONSORTIUM_REVIEW.getUserActionAnalyzer();

        SpecimenRequest request = getNewlySubmittedRequest();
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            vote.setUser(null);
        }
        TissueLocatorUser alternateUser = createUserSkipRole();
        assertTrue(fullConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));
        assertTrue(fixedConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertTrue(fixedConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));

        request.setStatus(RequestStatus.APPROVED);
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));

        request.setStatus(RequestStatus.PENDING);
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            vote.setUser(getCurrentUser());
        }
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));

        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.getReviewers().clear();
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));

        request.getReviewers().add(getCurrentUser().getInstitution());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            vote.setUser(null);
        }
        assertTrue(fullConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));

        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            vote.setInstitution(alternateUser.getInstitution());
        }
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));

        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            vote.setUser(getCurrentUser());
        }
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsReviewerAssignment(alternateUser, request));

        UserActionAnalyzer lineItemAnalyzer = ReviewProcess.LINE_ITEM_REVIEW.getUserActionAnalyzer();
        assertFalse(lineItemAnalyzer.needsReviewerAssignment(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsReviewerAssignment(alternateUser, request));
    }

    /**
     * tests the needs lead review method.
     */
    @Test
    public void testNeedsLeadReview() {
        UserActionAnalyzer fullConsortiumAnalyzer = ReviewProcess.FULL_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        SpecimenRequest request = getNewlySubmittedRequest();
        TissueLocatorUser alternateUser = createUserSkipRole();
        SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
        vote.setUser(alternateUser);
        vote.setDate(new Date());
        vote.setInstitution(alternateUser.getInstitution());
        request.getConsortiumReviews().add(vote);
        vote = new SpecimenRequestReviewVote();
        vote.setDate(new Date());
        vote.setInstitution(alternateUser.getInstitution());
        request.getConsortiumReviews().add(vote);
        assertTrue(fullConsortiumAnalyzer.needsLeadReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsLeadReview(alternateUser, request));

        request.setStatus(RequestStatus.APPROVED);
        assertFalse(fullConsortiumAnalyzer.needsLeadReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsLeadReview(alternateUser, request));

        request.setStatus(RequestStatus.PENDING);
        ReviewComment rc = new ReviewComment();
        rc.setUser(getCurrentUser());
        rc.setComment("comment");
        rc.setDate(new Date());
        request.getComments().add(rc);
        assertFalse(fullConsortiumAnalyzer.needsLeadReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsLeadReview(alternateUser, request));

        UserActionAnalyzer fixedConsortiumAnalyzer = ReviewProcess.FIXED_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        assertFalse(fixedConsortiumAnalyzer.needsLeadReview(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsLeadReview(alternateUser, request));

        UserActionAnalyzer lineItemAnalyzer = ReviewProcess.LINE_ITEM_REVIEW.getUserActionAnalyzer();
        assertFalse(lineItemAnalyzer.needsLeadReview(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsLeadReview(alternateUser, request));
    }

    /**
     * tests the needs consortium review method.
     */
    @Test
    public void testNeedsConsortiumReview() {
        UserActionAnalyzer fullConsortiumAnalyzer = ReviewProcess.FULL_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        UserActionAnalyzer fixedConsortiumAnalyzer = ReviewProcess.FIXED_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        SpecimenRequest request = getNewlySubmittedRequest();
        TissueLocatorUser alternateUser = createUserSkipRole();
        assertTrue(fullConsortiumAnalyzer.needsConsortiumReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsConsortiumReview(alternateUser, request));
        assertTrue(fixedConsortiumAnalyzer.needsConsortiumReview(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsConsortiumReview(alternateUser, request));

        request.setStatus(RequestStatus.APPROVED);
        assertFalse(fullConsortiumAnalyzer.needsConsortiumReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsConsortiumReview(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsConsortiumReview(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsConsortiumReview(alternateUser, request));

        request.setStatus(RequestStatus.PENDING);
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            vote.setVote(Vote.APPROVE);
        }
        assertFalse(fullConsortiumAnalyzer.needsConsortiumReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsConsortiumReview(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsConsortiumReview(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsConsortiumReview(alternateUser, request));

        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            vote.setUser(null);
            vote.setVote(null);
        }
        assertFalse(fullConsortiumAnalyzer.needsConsortiumReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsConsortiumReview(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsConsortiumReview(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsConsortiumReview(alternateUser, request));

        UserActionAnalyzer lineItemAnalyzer = ReviewProcess.LINE_ITEM_REVIEW.getUserActionAnalyzer();
        assertFalse(lineItemAnalyzer.needsConsortiumReview(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsConsortiumReview(alternateUser, request));
    }


    /**
     * tests the needs institutional review method.
     */
    @Test
    public void testNeedsInstitutionalReview() {
        UserActionAnalyzer fullConsortiumAnalyzer = ReviewProcess.FULL_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        UserActionAnalyzer lineItemAnalyzer = ReviewProcess.LINE_ITEM_REVIEW.getUserActionAnalyzer();

        SpecimenRequest request = getNewlySubmittedRequest();
        TissueLocatorUser alternateUser = createUserSkipRole();
        assertTrue(fullConsortiumAnalyzer.needsInstitutionalReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsInstitutionalReview(alternateUser, request));
        assertTrue(lineItemAnalyzer.needsInstitutionalReview(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsInstitutionalReview(alternateUser, request));

        request.setStatus(RequestStatus.APPROVED);
        assertFalse(fullConsortiumAnalyzer.needsInstitutionalReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsInstitutionalReview(alternateUser, request));
        assertFalse(lineItemAnalyzer.needsInstitutionalReview(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsInstitutionalReview(alternateUser, request));

        request.setStatus(RequestStatus.PENDING);
        for (SpecimenRequestReviewVote vote : request.getInstitutionalReviews()) {
            vote.setVote(Vote.APPROVE);
        }
        assertFalse(fullConsortiumAnalyzer.needsInstitutionalReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsInstitutionalReview(alternateUser, request));
        assertFalse(lineItemAnalyzer.needsInstitutionalReview(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsInstitutionalReview(alternateUser, request));

        UserActionAnalyzer fixedConsortiumAnalyzer = ReviewProcess.FIXED_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        assertFalse(fixedConsortiumAnalyzer.needsInstitutionalReview(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsInstitutionalReview(alternateUser, request));
    }

    /**
     * tests the needs line item review method.
     */
    @Test
    public void testNeedsLineItemReview() {
        SpecimenRequest request = getNewlySubmittedRequest();
        TissueLocatorUser alternateUser = createUserSkipRole();
        request.getConsortiumReviews().clear();
        request.getInstitutionalReviews().clear();
        AggregateSpecimenRequestLineItem li = new AggregateSpecimenRequestLineItem();
        li.setInstitution(getCurrentUser().getInstitution());
        li.setVote(new SpecimenRequestReviewVote());
        li.getVote().setInstitution(getCurrentUser().getInstitution());
        li.getVote().setDate(new Date());
        li.getVote().setVote(Vote.APPROVE);
        request.getAggregateLineItems().add(li);

        UserActionAnalyzer lineItemAnalyzer = ReviewProcess.LINE_ITEM_REVIEW.getUserActionAnalyzer();
        assertFalse(lineItemAnalyzer.needsLineItemReview(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsLineItemReview(alternateUser, request));

        request.setStatus(RequestStatus.APPROVED);
        assertFalse(lineItemAnalyzer.needsLineItemReview(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsLineItemReview(alternateUser, request));

        request.setStatus(RequestStatus.PENDING);
        li.getVote().setVote(null);
        assertTrue(lineItemAnalyzer.needsLineItemReview(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsLineItemReview(alternateUser, request));

        li.setVote(null);
        assertFalse(lineItemAnalyzer.needsLineItemReview(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsLineItemReview(alternateUser, request));

        UserActionAnalyzer fixedConsortiumAnalyzer = ReviewProcess.FIXED_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        assertFalse(fixedConsortiumAnalyzer.needsLineItemReview(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsLineItemReview(alternateUser, request));

        UserActionAnalyzer fullConsortiumAnalyzer = ReviewProcess.FULL_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        assertFalse(fullConsortiumAnalyzer.needsLineItemReview(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsLineItemReview(alternateUser, request));
    }

    /**
     * tests the needs lead review method.
     */
    @Test
    public void testNeedsFinalComment() {
        UserActionAnalyzer fullConsortiumAnalyzer = ReviewProcess.FULL_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        UserActionAnalyzer fixedConsortiumAnalyzer = ReviewProcess.FIXED_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        SpecimenRequest request = getNewlySubmittedRequest();
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        TissueLocatorUser alternateUser = createUserSkipRole();
        SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
        vote.setUser(alternateUser);
        vote.setDate(new Date());
        vote.setInstitution(alternateUser.getInstitution());
        request.getConsortiumReviews().add(vote);
        vote = new SpecimenRequestReviewVote();
        vote.setDate(new Date());
        vote.setInstitution(alternateUser.getInstitution());
        request.getConsortiumReviews().add(vote);
        assertTrue(fullConsortiumAnalyzer.needsFinalComment(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsFinalComment(alternateUser, request));
        assertTrue(fixedConsortiumAnalyzer.needsFinalComment(getCurrentUser(), request));
        assertTrue(fixedConsortiumAnalyzer.needsFinalComment(alternateUser, request));

        request.setStatus(RequestStatus.APPROVED);
        assertFalse(fullConsortiumAnalyzer.needsFinalComment(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsFinalComment(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsFinalComment(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsFinalComment(alternateUser, request));

        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.setExternalComment("external comment");
        assertFalse(fullConsortiumAnalyzer.needsFinalComment(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsFinalComment(alternateUser, request));
        assertFalse(fixedConsortiumAnalyzer.needsFinalComment(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsFinalComment(alternateUser, request));

        UserActionAnalyzer lineItemAnalyzer = ReviewProcess.LINE_ITEM_REVIEW.getUserActionAnalyzer();
        assertFalse(lineItemAnalyzer.needsFinalComment(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsFinalComment(alternateUser, request));
    }

    /**
     * tests the needs researcher notification method.
     */
    @Test
    public void testNeedsResearcherNotification() {
        UserActionAnalyzer fixedConsortiumAnalyzer = ReviewProcess.FIXED_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        SpecimenRequest request = getNewlySubmittedRequest();
        TissueLocatorUser alternateUser = createUserSkipRole();
        assertFalse(fixedConsortiumAnalyzer.needsResearcherNotification(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsResearcherNotification(alternateUser, request));

        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        assertFalse(fixedConsortiumAnalyzer.needsResearcherNotification(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsResearcherNotification(alternateUser, request));

        request.setFinalVote(RequestStatus.APPROVED);
        assertFalse(fixedConsortiumAnalyzer.needsResearcherNotification(getCurrentUser(), request));
        assertFalse(fixedConsortiumAnalyzer.needsResearcherNotification(alternateUser, request));

        request.setExternalComment("final comment");
        assertTrue(fixedConsortiumAnalyzer.needsResearcherNotification(getCurrentUser(), request));
        assertTrue(fixedConsortiumAnalyzer.needsResearcherNotification(alternateUser, request));

        UserActionAnalyzer fullConsortiumAnalyzer = ReviewProcess.FULL_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        assertFalse(fullConsortiumAnalyzer.needsResearcherNotification(getCurrentUser(), request));
        assertFalse(fullConsortiumAnalyzer.needsResearcherNotification(alternateUser, request));

        UserActionAnalyzer lineItemAnalyzer = ReviewProcess.LINE_ITEM_REVIEW.getUserActionAnalyzer();
        assertFalse(lineItemAnalyzer.needsResearcherNotification(getCurrentUser(), request));
        assertFalse(lineItemAnalyzer.needsResearcherNotification(alternateUser, request));
    }

    /**
     * test draft restriction search.
     */
    @Test
    public void testDraftRestriction() {
        SpecimenRequest example = new SpecimenRequest();
        verifyDraftRestrictionSearch(example, 0, null);

        SpecimenRequest request = getNewlySubmittedRequest();
        request.setStatus(RequestStatus.DRAFT);
        request.setRequestor(createUser(true, getCurrentUser().getInstitution()));
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        verifyDraftRestrictionSearch(example, 0, id);

        example.setRequestor(request.getRequestor());
        verifyDraftRestrictionSearch(example, 1, id);

        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PENDING);
        TissueLocatorHibernateUtil.getCurrentSession().update(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        example.setRequestor(null);
        verifyDraftRestrictionSearch(example, 1, id);

        example.setRequestor(request.getRequestor());
        verifyDraftRestrictionSearch(example, 1, id);
    }

    private void verifyDraftRestrictionSearch(SpecimenRequest example, int count, Long id) {
        SearchCriteria<SpecimenRequest> scFull = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.FULL_CONSORTIUM_REVIEW);
        verifySearch(scFull, count, id);
        SearchCriteria<SpecimenRequest> scFixed = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        verifySearch(scFixed, count, id);
        SearchCriteria<SpecimenRequest> scInst = new SpecimenRequestSearchCriteria(example, false, false, false,
                false, false, false, false, false, false, getCurrentUser(), ReviewProcess.LINE_ITEM_REVIEW);
        verifySearch(scInst, count, id);
    }

    private SpecimenRequest getNewlySubmittedRequest() {
        SpecimenRequest request = new SpecimenRequestPersistenceTest().getValidObjects()[0];
        request.getReviewers().clear();
        request.getReviewers().add(getCurrentUser().getInstitution());
        request.getConsortiumReviews().clear();
        SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
        vote.setUser(getCurrentUser());
        vote.setDate(new Date());
        vote.setInstitution(getCurrentUser().getInstitution());
        request.getConsortiumReviews().add(vote);
        vote = new SpecimenRequestReviewVote();
        vote.setUser(getCurrentUser());
        vote.setDate(new Date());
        vote.setInstitution(getCurrentUser().getInstitution());
        request.getInstitutionalReviews().add(vote);
        return request;
    }

    private SpecimenRequest getRequestWithOrder() {
        SpecimenRequest request = getNewlySubmittedRequest();
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.setStatus(RequestStatus.PARTIALLY_PROCESSED);

        Shipment order = new Shipment();
        order.setStatus(ShipmentStatus.PENDING);
        order.setRequest(request);
        order.setSender(request.getRequestor());
        order.setSendingInstitution(request.getRequestor().getInstitution());
        order.setShippingMethod(ShippingMethod.OTHER);
        order.setStatus(ShipmentStatus.PENDING);
        order.setTrackingNumber("1234");
        order.setShippingPrice(new BigDecimal("12.34"));
        order.setFees(new BigDecimal("12.34"));
        order.setUpdatedDate(new Date());
        order.setRecipient(request.getShipment().getRecipient());
        order.getLineItems().addAll(request.getLineItems());
        order.getAggregateLineItems().addAll(request.getAggregateLineItems());

        for (SpecimenRequestLineItem li : order.getLineItems()) {
            li.getSpecimen().setStatus(SpecimenStatus.PENDING_SHIPMENT);
        }

        order.setProcessingUser(getCurrentUser());
        request.getOrders().add(order);
        request.getLineItems().clear();
        TissueLocatorHibernateUtil.getCurrentSession().save(order);
        TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        return request;
    }

    private ReviewComment getReviewComment(TissueLocatorUser reviewer, Date date) {
        ReviewComment rc = new ReviewComment();
        rc.setComment("review comment");
        rc.setUser(reviewer);
        rc.setDate(date);
        return rc;
    }
}
