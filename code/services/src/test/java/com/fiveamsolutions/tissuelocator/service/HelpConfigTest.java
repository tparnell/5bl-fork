/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.config.help.FieldHelpConfig;
import com.fiveamsolutions.tissuelocator.data.config.help.HelpConfig;
import com.fiveamsolutions.tissuelocator.data.config.help.HelpTextLink;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author gvaughn
 *
 */
public class HelpConfigTest extends AbstractHibernateTestCase {

    private static final String[] FIELD_NAMES = new String[]{"principalInvestigator.resume", "name"};
    private static final String[] ENTITY_NAMES = new String[]{"com.fiveamsolutions.tissuelocator.data.SpecimenRequest",
        "com.fiveamsolutions.tissuelocator.data.TissueLocatorUser"};

    /**
     * Test field help configuration retrieval.
     */
    @Test
    public void testFieldHelpConfigRetrieval() {
        for (String entityClassName : ENTITY_NAMES) {
            createAndSaveFieldConfigs(entityClassName);
        }

        for (String entityClassName : ENTITY_NAMES) {
            HelpConfigServiceLocal service = TissueLocatorRegistry.getServiceLocator().getHelpConfigService();
            List<FieldHelpConfig> configs = service.getFieldHelpConfigs(entityClassName);
            assertEquals(FIELD_NAMES.length, configs.size());
            for (int i = 0; i < configs.size(); i++) {
                FieldHelpConfig config = configs.get(i);
                assertEquals(FIELD_NAMES[i], config.getFieldName());
                assertEquals(FIELD_NAMES[i] + " help", config.getHelpText());
            }

        }
    }

    /**
     * Test the help configuration object.
     */
    @Test
    public void testHelpConfig() {
        for (String entityClassName : ENTITY_NAMES) {
            createAndSaveFieldConfigs(entityClassName);

            HelpConfig config = new HelpConfig(entityClassName);
            for (String field : FIELD_NAMES) {
                assertNotNull(config.getFieldHelpConfig(field));
                FieldHelpConfig fieldConfig = config.getFieldHelpConfig(field);
                assertEquals(field, fieldConfig.getFieldName());
                assertEquals(field + " help", fieldConfig.getHelpText());
            }

            assertNull(config.getFieldHelpConfig("invalid"));
        }
        // this part is for branch coverage
        HelpConfig entityHelpConfig = new HelpConfig(ENTITY_NAMES[0]);
        assertNotNull(entityHelpConfig.getFieldHelpConfig(FIELD_NAMES[0]));
    }

    /**
     * Test field help configuration link replacement.
     */
    @Test
    public void testLinkReplacement() {
        createAndSaveFieldConfigWithLinks(ENTITY_NAMES[0]);
        HelpConfigServiceLocal service = TissueLocatorRegistry.getServiceLocator().getHelpConfigService();
        List<FieldHelpConfig> configs = service.getFieldHelpConfigs(ENTITY_NAMES[0]);
        assertEquals(1, configs.size());
        for (int i = 0; i < configs.size(); i++) {
            FieldHelpConfig config = configs.get(i);
            assertEquals(FIELD_NAMES[0], config.getFieldName());
            assertEquals(FIELD_NAMES[0] + " help {0} {1}", config.getHelpText());
            assertEquals(2, config.getHelpTextLinks().size());
            assertEquals("Link1", config.getHelpTextLinks().get(0)
                    .getLinkText());
            assertEquals("Link2", config.getHelpTextLinks().get(1)
                    .getLinkText());
            assertEquals("www.example.com", config.getHelpTextLinks().get(0)
                    .getLinkHref());
            assertEquals("www.google.com", config.getHelpTextLinks().get(1)
                    .getLinkHref());
            List<String> replacements = new ArrayList<String>();
            replacements.add("<a href=\"www.example.com\">Link1</a>");
            replacements.add("<a href=\"www.google.com\">Link2</a>");
            assertEquals(
                    FIELD_NAMES[0] + " help <a href=\"www.example.com\">Link1</a> "
                    + "<a href=\"www.google.com\">Link2</a>",
                    config.getHelpTextWithReplacements(replacements));
        }
    }

    private void createAndSaveFieldConfigs(String entityClassName) {
        HelpConfigServiceLocal service = TissueLocatorRegistry.getServiceLocator().getHelpConfigService();
        for (String field : FIELD_NAMES) {
            FieldHelpConfig config = new FieldHelpConfig();
            config.setEntityClassName(entityClassName);
            config.setFieldName(field);
            config.setHelpText(field + " help");
            service.savePersistentObject(config);
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
    }

    private void createAndSaveFieldConfigWithLinks(String entityClassName) {
        FieldHelpConfig config = new FieldHelpConfig();
        config.setEntityClassName(entityClassName);
        config.setFieldName(FIELD_NAMES[0]);
        config.setHelpText(FIELD_NAMES[0] + " help {0} {1}");
        HelpTextLink link1 = new HelpTextLink();
        link1.setLinkText("Link1");
        link1.setLinkHref("www.example.com");
        HelpTextLink link2 = new HelpTextLink();
        link2.setLinkText("Link2");
        link2.setLinkHref("www.google.com");
        config.setHelpTextLinks(new ArrayList<HelpTextLink>());
        config.getHelpTextLinks().add(link1);
        config.getHelpTextLinks().add(link2);
        HelpConfigServiceLocal service = TissueLocatorRegistry.getServiceLocator().getHelpConfigService();
        service.savePersistentObject(config);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
    }
}
