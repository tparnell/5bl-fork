/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.mail.MessagingException;

import org.apache.commons.lang.ObjectUtils;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.ApplicationSetting;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestPersistenceTestHelper;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceBean;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractSpecimenRequestTest;
import com.fiveamsolutions.tissuelocator.util.RequestProcessingConfiguration;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
public class FixedConsortiumReviewRequestProcessorTest extends AbstractSpecimenRequestTest {

    /**
     * Tests that appropriate emails are sent in the case of a late reviewer assignment for
     * the fixed consortium review process.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testLateReviewerAssignment() throws MessagingException, IOException {
        RequestProcessingConfiguration config = new RequestProcessingConfiguration();
        config.setVotingPeriod(VOTING_PERIOD);
        config.setReviewerAssignmentPeriod(REVIEWER_ASSIGNMENT_PERIOD);
        config.setReviewerAssignmentLateEmail(getEmail("reviewerAssignmentLate"));
        TissueLocatorUser instReviewer = createReviewerAssigner(null);
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        TissueLocatorUser reviewer = createInstitutionalReviewer(null);

        request.getConsortiumReviews().clear();
        SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
        vote.setUser(reviewer);
        vote.setInstitution(reviewer.getInstitution());
        request.getConsortiumReviews().add(vote);
        vote = new SpecimenRequestReviewVote();
        reviewer = createInstitutionalReviewer(null);
        vote.setUser(reviewer);
        vote.setInstitution(reviewer.getInstitution());
        request.getConsortiumReviews().add(vote);

        UsernameHolder.setUser(request.getRequestor().getUsername());
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        SpecimenRequestProcessingServiceLocal processingService =
            TissueLocatorRegistry.getServiceLocator().getSpecimenRequestProcessingService();
        Long id = service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertTrue(Mailbox.get(instReviewer.getEmail()).isEmpty());
        Mailbox.clearAll();

        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, -REVIEWER_ASSIGNMENT_PERIOD - 1);
        request.setUpdatedDate(cal.getTime());
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertTrue(Mailbox.get(instReviewer.getEmail()).isEmpty());
        Mailbox.clearAll();

        request.setUpdatedDate(cal.getTime());
        request.getConsortiumReviews().iterator().next().setUser(null);
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertFalse(Mailbox.get(instReviewer.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(instReviewer.getEmail()).size());
        testEmail(instReviewer.getEmail(), "immediate action", "assign");
        Mailbox.clearAll();

        request.setUpdatedDate(cal.getTime());
        Iterator<SpecimenRequestReviewVote> voteIter = request.getConsortiumReviews().iterator();
        voteIter.next().setUser(getCurrentUser());
        voteIter.next().setUser(null);
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertFalse(Mailbox.get(instReviewer.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(instReviewer.getEmail()).size());
        testEmail(instReviewer.getEmail(), "immediate action", "assign");
        Mailbox.clearAll();

        request.setUpdatedDate(cal.getTime());
        request.getConsortiumReviews().iterator().next().setUser(null);
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertFalse(Mailbox.get(instReviewer.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(instReviewer.getEmail()).size());
        testEmail(instReviewer.getEmail(), "immediate action", "assign");
        Mailbox.clearAll();

        request.setUpdatedDate(cal.getTime());
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        ReviewProcess.FIXED_CONSORTIUM_REVIEW.getRequestProcessor().processPendingRequest(request, config);
        assertTrue(Mailbox.get(instReviewer.getEmail()).isEmpty());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Tests request processing.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testRequestProcessing() throws MessagingException, IOException {
        RequestProcessingConfiguration config = new RequestProcessingConfiguration();
        config.setMinPercentAvailableVotes(MIN_AVAILABLE);
        config.setVotingPeriod(VOTING_PERIOD);
        config.setReviewerAssignmentPeriod(REVIEWER_ASSIGNMENT_PERIOD);
        config.setVoteLateEmail(getEmail("voteLate"));
        config.setVoteFinalizedEmail(getEmail("voteFinalized"));
        config.setReviewerAssignmentLateEmail(getEmail("reviewerAssignmentLate"));
        config.setRequestPendingReleaseEmail(getEmail("coordinator"));
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FIXED_CONSORTIUM_REVIEW);

        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        TissueLocatorUser assigner = createReviewerAssigner(null);
        TissueLocatorUser leadReviewer = createLeadReviewer(null);
        TissueLocatorUser reviewer1 = createConsortiumReviewer(null);
        TissueLocatorUser reviewer2 = createConsortiumReviewer(null);
        TissueLocatorUser decisionNotifier = createReviewDecisionNotifier(null);
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        request.getConsortiumReviews().clear();
        SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
        vote.setUser(reviewer1);
        vote.setInstitution(reviewer1.getInstitution());
        request.getConsortiumReviews().add(vote);
        vote = new SpecimenRequestReviewVote();
        vote.setUser(reviewer2);
        vote.setInstitution(reviewer2.getInstitution());
        request.getConsortiumReviews().add(vote);
        vote = new SpecimenRequestReviewVote();
        vote.setUser(null);
        vote.setInstitution(assigner.getInstitution());
        request.getConsortiumReviews().add(vote);

        UsernameHolder.setUser(request.getRequestor().getUsername());
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        SpecimenRequestProcessingServiceLocal processingService =
            TissueLocatorRegistry.getServiceLocator().getSpecimenRequestProcessingService();
        Long id = service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertTrue(Mailbox.get(assigner.getEmail()).isEmpty());
        assertTrue(Mailbox.get(leadReviewer.getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewer1.getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewer2.getEmail()).isEmpty());
        assertTrue(Mailbox.get(decisionNotifier.getEmail()).isEmpty());
        Mailbox.clearAll();

        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, -VOTING_PERIOD - 1);
        request.setUpdatedDate(cal.getTime());
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertFalse(Mailbox.get(assigner.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assigner.getEmail()).size());
        testEmail(assigner.getEmail(), "immediate action", "assign");
        assertTrue(Mailbox.get(leadReviewer.getEmail()).isEmpty());
        assertFalse(Mailbox.get(reviewer1.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewer1.getEmail()).size());
        testEmail(reviewer1.getEmail(), "Please vote", "vote today");
        assertFalse(Mailbox.get(reviewer2.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewer2.getEmail()).size());
        testEmail(reviewer2.getEmail(), "Please vote", "vote today");
        assertTrue(Mailbox.get(decisionNotifier.getEmail()).isEmpty());
        Mailbox.clearAll();

        request.setUpdatedDate(cal.getTime());
        for (SpecimenRequestReviewVote savedVote : request.getConsortiumReviews()) {
            if (ObjectUtils.equals(savedVote.getUser(), reviewer1)) {
                savedVote.setVote(Vote.APPROVE);
            }
        }
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertFalse(Mailbox.get(assigner.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assigner.getEmail()).size());
        testEmail(assigner.getEmail(), "immediate action", "assign");
        assertTrue(Mailbox.get(leadReviewer.getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewer1.getEmail()).isEmpty());
        assertFalse(Mailbox.get(reviewer2.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewer2.getEmail()).size());
        testEmail(reviewer2.getEmail(), "Please vote", "vote today");
        assertTrue(Mailbox.get(decisionNotifier.getEmail()).isEmpty());
        Mailbox.clearAll();

        request.setUpdatedDate(cal.getTime());
        for (SpecimenRequestReviewVote savedVote : request.getConsortiumReviews()) {
            if (ObjectUtils.equals(savedVote.getUser(), reviewer1)) {
                savedVote.setVote(Vote.DENY);
            } else if (ObjectUtils.equals(savedVote.getUser(), reviewer2)) {
                savedVote.setVote(Vote.REVISE);
            }
        }
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertTrue(Mailbox.get(assigner.getEmail()).isEmpty());
        assertFalse(Mailbox.get(leadReviewer.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(leadReviewer.getEmail()).size());
        testEmail(leadReviewer.getEmail(), "awaiting", "final comment");
        assertTrue(Mailbox.get(reviewer1.getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewer2.getEmail()).isEmpty());
        assertTrue(Mailbox.get(decisionNotifier.getEmail()).isEmpty());
        Mailbox.clearAll();

        for (SpecimenRequestReviewVote savedVote : request.getConsortiumReviews()) {
           savedVote.setVote(Vote.APPROVE);
        }
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.setExternalComment("test comment");
        request.setFinalVote(RequestStatus.APPROVED);
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertTrue(Mailbox.get(assigner.getEmail()).isEmpty());
        assertTrue(Mailbox.get(leadReviewer.getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewer1.getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewer2.getEmail()).isEmpty());
        assertFalse(Mailbox.get(decisionNotifier.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(decisionNotifier.getEmail()).size());
        testEmail(decisionNotifier.getEmail(), "Request", "release");
        Mailbox.clearAll();

        MailUtils.setMailEnabled(false);
    }

    /**
     * test the save request method for NFCR.
     * @throws MessagingException on error sending email
     * @throws IOException on error
     */
    @Test
    public void testSaveRequest() throws MessagingException, IOException {
        ApplicationSettingServiceLocal asService = new ApplicationSettingServiceBean();
        ApplicationSetting as = new ApplicationSetting();
        as.setName("number_reviewers");
        as.setValue("2");
        asService.savePersistentObject(as);
        as = new ApplicationSetting();
        as.setName("display_prospective_collection");
        as.setValue("true");
        asService.savePersistentObject(as);

        TissueLocatorUser reviewerAssigner = createReviewerAssigner(getCurrentUser().getInstitution());
        TissueLocatorUser coordinator = createReviewDecisionNotifier(getCurrentUser().getInstitution());
        TissueLocatorUser[] reviewers = getReviewCommittee();

        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FIXED_CONSORTIUM_REVIEW);
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        request.getConsortiumReviews().clear();
        request.getInstitutionalReviews().clear();
        request.getReviewers().clear();
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            lineItem.getSpecimen().setStatus(SpecimenStatus.AVAILABLE);
            TissueLocatorRegistry.getServiceLocator().
                getSpecimenService().savePersistentObject(lineItem.getSpecimen());
        }
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        UsernameHolder.setUser(request.getRequestor().getUsername());
        Long id = service.saveRequest(request, 2, 1, 1, 2);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(request);
        assertFalse(request.getConsortiumReviews().isEmpty());
        assertEquals(2, request.getConsortiumReviews().size());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            assertNotNull(vote.getId());
            assertNotNull(vote.getInstitution());
            assertNotNull(vote.getDate());
            assertNull(vote.getUser());
            assertNull(vote.getVote());
            assertNull(vote.getComment());
        }
        assertTrue(request.getInstitutionalReviews().isEmpty());
        assertTrue(request.getReviewers().isEmpty());
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            assertEquals(lineItem.getSpecimen().getStatus(), SpecimenStatus.UNDER_REVIEW);
        }
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertFalse(Mailbox.get(reviewerAssigner.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewerAssigner.getEmail()).size());
        testEmail(reviewerAssigner.getEmail(), "is awaiting your action", "assign");
        assertFalse(Mailbox.get(coordinator.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(coordinator.getEmail()).size());
        testEmail(coordinator.getEmail(), "request", "request", "submitted");
        assertTrue(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[1].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        Mailbox.clearAll();

        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            lineItem.getSpecimen().setStatus(SpecimenStatus.AVAILABLE);
            TissueLocatorRegistry.getServiceLocator().
                getSpecimenService().savePersistentObject(lineItem.getSpecimen());
        }
        id = service.saveRequest(request, 2, 1, 1, 2);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(request);
        assertFalse(request.getConsortiumReviews().isEmpty());
        assertEquals(2, request.getConsortiumReviews().size());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            assertNotNull(vote.getId());
            assertNotNull(vote.getInstitution());
            assertNotNull(vote.getDate());
            assertNull(vote.getUser());
            assertNull(vote.getVote());
            assertNull(vote.getComment());
        }
        assertTrue(request.getInstitutionalReviews().isEmpty());
        assertTrue(request.getReviewers().isEmpty());
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            assertEquals(lineItem.getSpecimen().getStatus(), SpecimenStatus.UNDER_REVIEW);
        }
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertFalse(Mailbox.get(reviewerAssigner.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewerAssigner.getEmail()).size());
        testEmail(reviewerAssigner.getEmail(), "Revised", "assign");
        assertFalse(Mailbox.get(coordinator.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(coordinator.getEmail()).size());
        testEmail(coordinator.getEmail(), "Revised", "review");
        assertTrue(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[1].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        Mailbox.clearAll();

        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            lineItem.getSpecimen().setStatus(SpecimenStatus.AVAILABLE);
            TissueLocatorRegistry.getServiceLocator().
                getSpecimenService().savePersistentObject(lineItem.getSpecimen());
        }
        Iterator<SpecimenRequestReviewVote> voteIter = request.getConsortiumReviews().iterator();
        voteIter.next().setUser(reviewers[0]);
        voteIter.next().setUser(reviewers[1]);
        id = service.saveRequest(request, 2, 1, 1, 2);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(request);
        assertFalse(request.getConsortiumReviews().isEmpty());
        assertEquals(2, request.getConsortiumReviews().size());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            assertNotNull(vote.getId());
            assertNotNull(vote.getInstitution());
            assertNotNull(vote.getDate());
            assertNotNull(vote.getUser());
            assertNull(vote.getVote());
            assertNull(vote.getComment());
        }
        assertTrue(request.getInstitutionalReviews().isEmpty());
        assertTrue(request.getReviewers().isEmpty());
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            assertEquals(lineItem.getSpecimen().getStatus(), SpecimenStatus.UNDER_REVIEW);
        }
        assertFalse(Mailbox.get(reviewerAssigner.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewerAssigner.getEmail()).size());
        testEmail(reviewerAssigner.getEmail(), "revis", "Scientific Reviewer");
        assertFalse(Mailbox.get(coordinator.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(coordinator.getEmail()).size());
        testEmail(coordinator.getEmail(), "Revised", "review");
        assertFalse(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewers[0].getEmail()).size());
        testEmail(reviewers[0].getEmail(), "Please vote", "resubmitted with revisions", "change your vote");
        assertFalse(Mailbox.get(reviewers[1].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewers[1].getEmail()).size());
        testEmail(reviewers[1].getEmail(), "Please vote", "resubmitted with revisions", "change your vote");
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * tests the save final vote method.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testSaveFinalVote() throws MessagingException, IOException {
        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        TissueLocatorUser notifier = createReviewDecisionNotifier(null);
        request.setFinalVote(RequestStatus.APPROVED);
        request.setExternalComment("approved");
        UsernameHolder.setUser(request.getRequestor().getUsername());
        getReviewService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        getReviewService().saveFinalVote(request);
        assertFalse(Mailbox.get(notifier.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(notifier.getEmail()).size());
        testEmail(notifier.getEmail(), "Request", "release");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * tests the decline reviewer method.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testDeclineReviewer() throws MessagingException, IOException {
        FixedConsortiumReviewServiceLocal service = getReviewService();
        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        request.getConsortiumReviews().clear();
        SpecimenRequestReviewVote review = SpecimenRequestPersistenceTestHelper.getVote(getCurrentUser());
        review.setVote(Vote.APPROVE);
        review.setComment("comment");
        request.getConsortiumReviews().add(review);
        Long id = service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        SpecimenRequestReviewVote savedReview = request.getConsortiumReviews().iterator().next();
        assertNotNull(savedReview);
        assertNotNull(savedReview.getInstitution());
        assertNotNull(savedReview.getUser());
        assertNotNull(savedReview.getVote());
        assertNotNull(savedReview.getComment());

        TissueLocatorUser assigner = createReviewerAssigner(getCurrentUser().getInstitution());
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        service.declineReview(request, savedReview, 2, 1);
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = service.getPersistentObject(SpecimenRequest.class, id);
        savedReview = request.getConsortiumReviews().iterator().next();
        assertNotNull(savedReview);
        assertNotNull(savedReview.getInstitution());
        assertNull(savedReview.getUser());
        assertNull(savedReview.getVote());
        assertNull(savedReview.getComment());

        assertFalse(Mailbox.get(assigner.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assigner.getEmail()).size());
        testEmail(assigner.getEmail(), "", "decline", "assign a new scientific reviewer");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * tests the assign reviewer method called by the NFCR specimen request review action.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testAssignReviewerSimple() throws MessagingException, IOException {
        FixedConsortiumReviewServiceLocal service = getReviewService();
        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        request.getConsortiumReviews().clear();
        SpecimenRequestReviewVote review = SpecimenRequestPersistenceTestHelper.getVote(getCurrentUser());
        review.setUser(null);
        request.getConsortiumReviews().add(review);
        Long id = service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        TissueLocatorUser assignee1 = createUserSkipRole();
        TissueLocatorUser assignee2 = createUserSkipRole();
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        Date oldUpdatedDate = request.getUpdatedDate();
        request.setUpdatedDate(new Date());
        service.assignRequest(request, VOTING_PERIOD);
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertTrue(Mailbox.get(assignee1.getEmail()).isEmpty());
        assertTrue(Mailbox.get(assignee2.getEmail()).isEmpty());
        assertTrue(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        Mailbox.clearAll();
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        oldUpdatedDate = request.getUpdatedDate();
        request.setUpdatedDate(new Date());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            vote.setUser(assignee1);
            vote.setInstitution(assignee1.getInstitution());
        }
        service.assignRequest(request, VOTING_PERIOD);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertFalse(Mailbox.get(assignee1.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assignee1.getEmail()).size());
        testEmail(assignee1.getEmail(), "request", "vote");
        assertTrue(Mailbox.get(assignee2.getEmail()).isEmpty());
        assertTrue(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        Mailbox.clearAll();
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        oldUpdatedDate = request.getUpdatedDate();
        request.setUpdatedDate(new Date());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            vote.setUser(assignee2);
            vote.setInstitution(assignee2.getInstitution());
        }
        service.assignRequest(request, VOTING_PERIOD);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertTrue(Mailbox.get(assignee1.getEmail()).isEmpty());
        assertFalse(Mailbox.get(assignee2.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assignee2.getEmail()).size());
        testEmail(assignee2.getEmail(), "request", "vote");
        assertTrue(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    private FixedConsortiumReviewServiceLocal getReviewService() {
        return TissueLocatorRegistry.getServiceLocator().getFixedConsortiumReviewService();
    }
}
