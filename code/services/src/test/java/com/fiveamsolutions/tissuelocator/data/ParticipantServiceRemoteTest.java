/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.service.ParticipantServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceRemote;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.test.SpecimenPersistenceHelper;
import com.fiveamsolutions.tissuelocator.test.TestServiceLocator;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author bpickeral
 *
 */
public class ParticipantServiceRemoteTest extends AbstractHibernateTestCase {
    private final TestServiceLocator test = new TestServiceLocator(getGuiceInjector());
    private final ParticipantServiceRemote participantService =
        test.getParticipantServiceRemote();

    /**
     * test the get participant method.
     */
    @Test
    public void getParticipants() {
        Institution i = EjbTestHelper.getGenericServiceBean(Institution.class).getPersistentObject(Institution.class,
                getCurrentUser().getInstitution().getId());
        ParticipantServiceLocal localService = TissueLocatorRegistry.getServiceLocator().getParticipantService();
        Long participantId = localService.savePersistentObject(SpecimenPersistenceHelper.createParticipant(i, 1));
        Participant participant = localService.getPersistentObject(Participant.class, participantId);
        Map<String, Participant> retrieved =
            participantService.getParticipants(i.getId(), Collections.singleton(participant.getExternalId()));
        assertNotNull(retrieved);
        assertEquals(1, retrieved.size());
        assertEquals("external id 1231", retrieved.keySet().iterator().next());
        assertNotNull(retrieved.values().iterator().next());
        assertEquals("external id 1231", retrieved.values().iterator().next().getExternalId());
        assertEquals(Ethnicity.HISPANIC_OR_LATINO, retrieved.values().iterator().next().getEthnicity());
        assertEquals(2, retrieved.values().iterator().next().getRaces().size());
        assertEquals(Gender.FEMALE, retrieved.values().iterator().next().getGender());
        assertEquals(i.getId(), retrieved.values().iterator().next().getExternalIdAssigner().getId());

        retrieved = participantService.getParticipants(i.getId(), new ArrayList<String>());
        assertNotNull(retrieved);
        assertTrue(retrieved.isEmpty());
    }
}
