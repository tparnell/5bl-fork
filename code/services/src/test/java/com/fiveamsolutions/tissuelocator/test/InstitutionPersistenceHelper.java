/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test;

import java.util.Date;

import com.fiveamsolutions.tissuelocator.data.Address;
import com.fiveamsolutions.tissuelocator.data.Contact;
import com.fiveamsolutions.tissuelocator.data.Country;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.State;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;

/**
 * @author bpickeral
 *
 */
public class InstitutionPersistenceHelper {
    private static final InstitutionPersistenceHelper INSTANCE = new InstitutionPersistenceHelper();

    /**
     * Get instance of InstitutionPersistenceHelper.
     * @return InstitutionPersistenceHelper institution
     */
    public static InstitutionPersistenceHelper getInstance() {
        return INSTANCE;
    }

    /**
     * Creates an institution and saves an institution type to the db.
     * @param type institution type
     * @param name Name of institution
     * @param consortiumMember set as consortium member
     * @return Institution object
     */
    public Institution createInstitution(InstitutionType type, String name, boolean consortiumMember) {
        Institution object = new Institution();
        object.setName(name);
        object.setHomepage("http://www.google.com");
        object.setInstitutionProfileUrl("http://www.5amsolutions.com");
        object.setType(type);
        object.setSpecimenUseRestrictions("test restrictions");
        if (consortiumMember) {
            object.setLastReviewAssignment(new Date());
            setAsConsortiumMember(object);
        } else {
            object.setConsortiumMember(false);
            object.setSpecimenUseRestrictions("test restrictions");
        }
        return object;
    }

    /**
     * @param institution Institution to set as Consortium member.
     */
    public void setAsConsortiumMember(Institution institution) {
        institution.setConsortiumMember(true);
        institution.setBillingAddress(new Address());
        institution.getBillingAddress().setLine1("123 Main Street");
        institution.getBillingAddress().setLine2("apartment 3");
        institution.getBillingAddress().setCity("City");
        institution.getBillingAddress().setState(State.MARYLAND);
        institution.getBillingAddress().setZip("12345");
        institution.getBillingAddress().setCountry(Country.UNITED_STATES);
        institution.getBillingAddress().setPhone("1234567890");
        institution.getBillingAddress().setPhoneExtension("1234");
        institution.getBillingAddress().setFax("0987654321");
        institution.setContact(new Contact());
        institution.getContact().setFirstName("fname");
        institution.getContact().setLastName("lname");
        institution.getContact().setEmail("fname@lname.com");
        institution.getContact().setPhone("1234567890");
        institution.getContact().setPhoneExtension("1234");
        institution.setMtaContact(new Person());
        institution.getMtaContact().setFirstName("fname");
        institution.getMtaContact().setLastName("lname");
        institution.getMtaContact().setEmail("fname@lname.com");
        institution.getMtaContact().setAddress(new Address());
        institution.getMtaContact().getAddress().setLine1("line 1");
        institution.getMtaContact().getAddress().setLine2("line 2");
        institution.getMtaContact().getAddress().setCity("city");
        institution.getMtaContact().getAddress().setState(State.ALABAMA);
        institution.getMtaContact().getAddress().setZip("12345");
        institution.getMtaContact().getAddress().setCountry(Country.UNITED_STATES);
        institution.getMtaContact().getAddress().setPhone("1234567890");
        institution.getMtaContact().getAddress().setPhoneExtension("1234");
        institution.getMtaContact().getAddress().setFax("1234567890");
        institution.getMtaContact().setOrganization(institution);
        institution.setPaymentInstructions("test instructions");
    }

    /**
     * Creates and saves default institution type.
     * @return InstitutionType object saved
     */
    public InstitutionType createAndSaveDefaultType() {
        GenericServiceLocal<InstitutionType> typeService = EjbTestHelper.getGenericServiceBean(InstitutionType.class);
        InstitutionType type = new InstitutionType();
        type.setName("test institute type");
        Long typeId = typeService.savePersistentObject(type);
        type = typeService.getPersistentObject(InstitutionType.class, typeId);
        return type;
    }
}
