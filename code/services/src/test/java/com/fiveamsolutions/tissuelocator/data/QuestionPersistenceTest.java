/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.support.Question;
import com.fiveamsolutions.tissuelocator.data.support.QuestionResponse;
import com.fiveamsolutions.tissuelocator.data.support.QuestionStatus;
import com.fiveamsolutions.tissuelocator.data.support.QuestionStatusTransition;
import com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceBean;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.QuestionSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.support.QuestionServiceBean;
import com.fiveamsolutions.tissuelocator.service.support.QuestionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceBean;
import com.fiveamsolutions.tissuelocator.test.InstitutionPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author ddasgupta
 *
 */
public class QuestionPersistenceTest extends AbstractPersistenceTest<Question> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Question[] getValidObjects() {
        return new Question[] {createQuestion(false), createQuestion(true)};
    }

    private Question createQuestion(boolean includeOptional) {
        Question question = new Question();
        TissueLocatorUser u = getCurrentUser();
        question.setRequestor(u);

        Person investigator = new Person();
        investigator.setFirstName("investigator");
        investigator.setLastName("investigator");
        investigator.setOrganization(u.getInstitution());
        investigator.setEmail(u.getEmail());
        investigator.setAddress(getAddress());
        new GenericServiceBean<Person>().savePersistentObject(investigator);
        question.setInvestigator(investigator);

        question.setResume(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));
        question.setQuestionText("question text");
        question.setStatus(QuestionStatus.PENDING);

        if (includeOptional) {
            question.setProtocolDocument(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));
            question.setStatus(QuestionStatus.FULLY_RESPONDED);

            QuestionResponse qr = new QuestionResponse();
            qr.setInstitution(getCurrentUser().getInstitution());
            qr.setStatus(SupportRequestStatus.PENDING);
            qr.setQuestion(question);
            question.setResponses(Collections.singletonList(qr));
        }
        return question;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(Question expected, Question actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
        assertEquals(expected.getResponses().size(), actual.getResponses().size());
    }

    /**
     * Verify that a question can be saved by the requestor.
     */
    @Test
    public void testSaveByRequestor() {
        GenericServiceBean<Question> service = new GenericServiceBean<Question>();
        service.setUserService(new TissueLocatorUserServiceBean());
        Question question = getValidObjects()[0];
        Long id = service.savePersistentObject(question);
        assertNotNull(id);
    }

    /**
     * Verify that a question can be saved by a question responder.
     */
    @Test
    public void testSaveByResponder() {
        GenericServiceBean<Question> service = new GenericServiceBean<Question>();
        service.setUserService(new TissueLocatorUserServiceBean());
        Question question = getValidObjects()[1];
        TissueLocatorUser responder = createUserInRole(Role.QUESTION_RESPONDER, getCurrentUser().getInstitution());
        UsernameHolder.setUser(responder.getUsername());
        Long id = service.savePersistentObject(question);
        assertNotNull(id);
    }

    /**
     * Verify that a question can be saved by a question administrator.
     */
    @Test
    public void testSaveByAdministrator() {
        GenericServiceBean<Question> service = new GenericServiceBean<Question>();
        service.setUserService(new TissueLocatorUserServiceBean());
        Question question = getValidObjects()[1];
        TissueLocatorUser admin = createUserInRole(Role.QUESTION_ADMINISTRATOR, getCurrentUser().getInstitution());
        UsernameHolder.setUser(admin.getUsername());
        Long id = service.savePersistentObject(question);
        assertNotNull(id);
    }

    /**
     * Verify that a question cannot be saved by an inaccessible user
     * (e.g., not the responder, admin or requestor).
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSaveByInccessibleUser() {
        GenericServiceBean<Question> service = new GenericServiceBean<Question>();
        service.setUserService(new TissueLocatorUserServiceBean());
        Question question = getValidObjects()[1];
        TissueLocatorUser admin = createUserSkipRole();
        UsernameHolder.setUser(admin.getUsername());
        service.savePersistentObject(question);
    }

    /**
     * Test question status transition handling.
     */
    @Test
    public void testStatusTransitionHistory() {
        Question[] questions = getValidObjects();
        Question question = questions[0];
        int statusCount = -1;
        question.setStatus(QuestionStatus.PENDING);
        assertNotNull(question.getStatus().getAdministratorResourceKey());
        assertNotNull(question.getStatus().getResearcherResourceKey());
        Long id = getService().savePersistentObject(question);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        question = getService().getPersistentObject(Question.class, id);
        assertEquals(++statusCount, question.getStatusTransitionHistory().size());

        question.setStatus(QuestionStatus.FULLY_RESPONDED);
        getService().savePersistentObject(question);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        question = getService().getPersistentObject(Question.class, id);
        assertEquals(++statusCount, question.getStatusTransitionHistory().size());
        QuestionStatusTransition t = question.getStatusTransitionHistory().get(statusCount - 1);
        t = question.getStatusTransitionHistory().get(statusCount - 1);
        assertEquals(QuestionStatus.FULLY_RESPONDED, t.getNewStatus());
        assertEquals(QuestionStatus.PENDING, t.getPreviousStatus());
        assertNotNull(t.getTransitionDate());
        assertNotNull(t.getSystemTransitionDate());

        question.setStatus(QuestionStatus.PARTIALLY_RESPONDED);
        Date date = new Date();
        question.setStatusTransitionDate(date);
        getService().savePersistentObject(question);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        question = getService().getPersistentObject(Question.class, id);
        assertEquals(++statusCount, question.getStatusTransitionHistory().size());
        t = question.getStatusTransitionHistory().get(statusCount - 1);
        assertEquals(QuestionStatus.PARTIALLY_RESPONDED, t.getNewStatus());
        assertEquals(QuestionStatus.FULLY_RESPONDED, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());
        assertNotNull(t.getSystemTransitionDate());
        assertNotSame(date, t.getSystemTransitionDate());

        // Edit a previously saved date
        date = new Date();
        t.setTransitionDate(date);
        getService().savePersistentObject(question);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        question = getService().getPersistentObject(Question.class, id);
        assertEquals(statusCount, question.getStatusTransitionHistory().size());
        assertEquals(QuestionStatus.PARTIALLY_RESPONDED, t.getNewStatus());
        assertEquals(QuestionStatus.FULLY_RESPONDED, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());

        getService().savePersistentObject(question);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        question = getService().getPersistentObject(Question.class, id);
        assertEquals(statusCount, question.getStatusTransitionHistory().size());
        assertEquals(QuestionStatus.PARTIALLY_RESPONDED, t.getNewStatus());
        assertEquals(QuestionStatus.FULLY_RESPONDED, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());
    }

    /**
     * test searching for questions by status and requestor.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        Question example = new Question();
        SearchCriteria<Question> sc = new TissueLocatorAnnotatedBeanSearchCriteria<Question>(example);
        QuestionServiceBean service = new QuestionServiceBean();
        service.setUserService(new TissueLocatorUserServiceBean());
        int total = service.count(sc);

        example.setStatus(QuestionStatus.PENDING);
        List<Question> results = service.search(sc);
        assertEquals(1, results.size());
        example.setStatus(QuestionStatus.FULLY_RESPONDED);
        results = service.search(sc);
        assertEquals(1, results.size());
        example.setStatus(QuestionStatus.PARTIALLY_RESPONDED);
        results = service.search(sc);
        assertEquals(0, results.size());

        example.setStatus(null);
        example.setRequestor(getCurrentUser());
        results = service.search(sc);
        assertEquals(total, results.size());
        example.setRequestor(createUserSkipRole());
        results = service.search(sc);
        assertEquals(0, results.size());

        example.setRequestor(null);
        QuestionSortCriterion[] sorts = QuestionSortCriterion.values();
        for (QuestionSortCriterion sort : sorts) {
            PageSortParams<Question> psp =
                new PageSortParams<Question>(PAGE_SIZE, 0, sort, false);
            assertEquals(total, service.search(sc, psp).size());
        }
    }

    /**
     * test the get responding institution names method.
     */
    @Test
    public void testGetRespondingInstitutionNames() {
        Question question = new Question();
        Institution i1 = new Institution();
        i1.setName("institution 1");
        QuestionResponse qr1 = new QuestionResponse();
        qr1.setInstitution(i1);
        question.getResponses().add(qr1);
        Institution i2 = new Institution();
        i2.setName("institution 2");
        QuestionResponse qr2 = new QuestionResponse();
        qr2.setInstitution(i2);
        question.getResponses().add(qr2);
        assertEquals("institution 1, institution 2", question.getRespondingInstitutionNames());
    }

    /**
     * test the save new question method.
     * @throws Exception on error
     */
    @Test
    public void testSaveNewQuestion() throws Exception {
        MailUtils.setMailEnabled(true);
        InstitutionServiceLocal instService = new InstitutionServiceBean();
        InstitutionPersistenceHelper helper = InstitutionPersistenceHelper.getInstance();
        InstitutionType it = getCurrentUser().getInstitution().getType();
        Institution a = helper.createInstitution(it, "a institution", false);
        Long aId = instService.savePersistentObject(a);
        Institution b = helper.createInstitution(it, "b institution", false);
        Long bId = instService.savePersistentObject(b);
        Institution c = helper.createInstitution(it, "c institution", false);
        Long cId = instService.savePersistentObject(c);
        Institution d = helper.createInstitution(it, "d institution", false);
        Long dId = instService.savePersistentObject(d);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        a = instService.getInstitution(aId);
        b = instService.getInstitution(bId);
        c = instService.getInstitution(cId);
        d = instService.getInstitution(dId);

        TissueLocatorUser admin = createUserInRole(Role.QUESTION_ADMINISTRATOR, getCurrentUser().getInstitution());
        TissueLocatorUser aResponder = createUserInRole(Role.QUESTION_RESPONDER, a);
        TissueLocatorUser bResponder = createUserInRole(Role.QUESTION_RESPONDER, b);
        TissueLocatorUser cResponder = createUserInRole(Role.QUESTION_RESPONDER, c);
        TissueLocatorUser dResponder = createUserInRole(Role.QUESTION_RESPONDER, d);
        TissueLocatorUser altCResponder = createUserInRole(Role.QUESTION_RESPONDER, c);

        Question question = createQuestion(false);
        assertTrue(question.getResponses().isEmpty());
        List<Institution> responders = new ArrayList<Institution>();
        responders.add(c);
        responders.add(a);
        responders.add(b);

        Long id = getService().saveQuestion(question, responders);
        assertNotNull(id);
        Question saved = getService().getPersistentObject(Question.class, id);
        assertNotNull(saved.getId());
        assertNotNull(saved.getResponses());
        assertFalse(saved.getResponses().isEmpty());
        assertEquals(responders.size(), saved.getResponses().size());
        verifyInstitution(saved, 0, a, aResponder, id);
        verifyInstitution(saved, 1, b, bResponder, id);
        verifyInstitution(saved, 2, c, cResponder, id);
        verifyInstitution(saved, 2, c, altCResponder, id);

        assertFalse(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(getCurrentUser().getEmail()).size());
        testEmail(getCurrentUser().getEmail(), "has been submitted", "You have successfully submitted a question",
                "To view your question, click", id.toString(), getCurrentUser().getFirstName(),
                getCurrentUser().getLastName(), "support@");

        assertFalse(Mailbox.get(admin.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(admin.getEmail()).size());
        testEmail(admin.getEmail(), "has been submitted", "has been submitted",
                "response at your earliest convenience", id.toString(), getCurrentUser().getFirstName(),
                getCurrentUser().getLastName());
        assertTrue(Mailbox.get(dResponder.getEmail()).isEmpty());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    private void verifyInstitution(Question question, int responseIndex, Institution responderInst,
            TissueLocatorUser responder, Long id) throws Exception {
        QuestionResponse response = question.getResponses().get(responseIndex);
        assertNotNull(response.getInstitution());
        assertEquals(responderInst.getName(), response.getInstitution().getName());
        assertEquals(SupportRequestStatus.PENDING, response.getStatus());
        assertNotNull(response.getQuestion());
        assertFalse(Mailbox.get(responder.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(responder.getEmail()).size());
        testEmail(responder.getEmail(), "is awaiting your action", "has been submitted to your",
                "response at your earliest convenience", id.toString(), getCurrentUser().getFirstName(),
                getCurrentUser().getLastName(), response.getId().toString());
    }

    /**
     * Test submitting a response to a question, updating the overall question status,
     * and sending notification emails.
     * @throws Exception on error
     */
    @Test
    public void testAddAdminResponse() throws Exception {
        MailUtils.setMailEnabled(true);
        Question q = getValidObjects()[1];
        q.setResponses(new ArrayList<QuestionResponse>());
        Date questionUpdatedDate = q.getLastUpdatedDate();
        QuestionResponse qr = new QuestionResponsePersistenceTest().getValidObjects()[2];
        qr.setResponse("offline response notes");
        qr.setResponder(null);
        qr.setInstitution(null);
        Date responseUpdatedDate = qr.getLastUpdatedDate();

        Long id = getService().addAdminResponse(q, qr);
        assertNotNull(id);
        q = getService().getPersistentObject(Question.class, id);
        assertNotSame(questionUpdatedDate, q.getLastUpdatedDate());
        assertNotNull(q.getResponses());
        assertFalse(q.getResponses().isEmpty());
        assertEquals(1, q.getResponses().size());
        QuestionResponse response = q.getResponses().get(0);
        assertNotNull(response.getResponder());
        assertNotNull(response.getInstitution());
        assertEquals(getCurrentUser().getId(), response.getResponder().getId());
        assertNotSame(responseUpdatedDate, response.getLastUpdatedDate());

        assertFalse(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(getCurrentUser().getEmail()).size());
        testEmail(getCurrentUser().getEmail(), "has been updated",
                "Administrator has responded to your Question Request", "review your question and responses",
                id.toString(), "offline response notes");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * test the has admin response method.
     */
    @Test
    public void testHasAdminResponse() {
        Question q = getValidObjects()[1];
        for (QuestionResponse response : q.getResponses()) {
            response.setResponder(null);
        }
        assertFalse(q.hasAdminResponse());
        for (QuestionResponse response : q.getResponses()) {
            response.setResponder(getCurrentUser());
        }
        assertFalse(q.hasAdminResponse());
        TissueLocatorUser admin = createUserInRole(Role.QUESTION_ADMINISTRATOR, getCurrentUser().getInstitution());
        for (QuestionResponse response : q.getResponses()) {
            response.setResponder(admin);
        }
        assertTrue(q.hasAdminResponse());
    }

    /**
     * @return the service
     */
    @Override
    public QuestionServiceLocal getService() {
        QuestionServiceBean service = new QuestionServiceBean();
        service.setUserService(new TissueLocatorUserServiceBean());
        return service;
    }

}
