/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.hibernate.validator.InvalidStateException;
import org.hibernate.validator.InvalidValue;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.test.SpecimenPersistenceHelper;
import com.fiveamsolutions.tissuelocator.test.TestServiceLocator;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author bpickeral
 *
 */
public class SpecimenServiceRemoteTest extends AbstractHibernateTestCase {
    private final TestServiceLocator test = new TestServiceLocator(getGuiceInjector());
    private final SpecimenServiceRemote specimenService =
        test.getSpecimenServiceRemote();
    private static int codeSuffix = 1;

    private static final int AGE = 17;

    /**
     * test get available specimen.
     */
    @Test
    public void testGetAvailableSpecimens() {
        List<Specimen> results = specimenService.getAvailableSpecimens(0, 1);
        assertEquals(0, results.size());

        Specimen s1 = createFirstSpecimen();
        TissueLocatorHibernateUtil.getCurrentSession().save(s1);
        codeSuffix++;
        results = specimenService.getAvailableSpecimens(0, 1);
        assertEquals(0, results.size());

        Specimen s2 = createSecondSpecimen();
        TissueLocatorHibernateUtil.getCurrentSession().save(s2);
        codeSuffix++;
        results = specimenService.getAvailableSpecimens(0, 1);
        assertEquals(1, results.size());
        results = specimenService.getAvailableSpecimens(1, 1);
        assertEquals(0, results.size());

        Specimen s3 = createSecondSpecimen();
        TissueLocatorHibernateUtil.getCurrentSession().save(s3);
        codeSuffix++;
        results = specimenService.getAvailableSpecimens(0, 1);
        assertEquals(1, results.size());
        results = specimenService.getAvailableSpecimens(1, 1);
        assertEquals(1, results.size());
        results = specimenService.getAvailableSpecimens(2, 1);
        assertEquals(0, results.size());
    }

    /**
     * test the get remote specimen service.
     */
    @Test
    public void specimenServiceRemoteTest() {
        // Test save specimen
        Specimen s1 = createFirstSpecimen();
        codeSuffix++;
        Specimen s2 = createSecondSpecimen();

        Specimen imported1 = specimenService.importSpecimen(s1);
        assertNotNull(imported1);
        assertNotNull(imported1.getParticipant().getId());
        assertNotNull(imported1.getProtocol().getId());
        Specimen imported2 = specimenService.importSpecimen(s2);
        assertNotNull(imported2);
        assertNotNull(imported2.getParticipant().getId());
        assertNotNull(imported2.getProtocol().getId());

        // Test get Specimen
        Map<String, Specimen> result1 =
            specimenService.getSpecimens(s1.getExternalIdAssigner().getId(), Collections.singleton(s1.getExternalId()));
        assertNotNull(result1);
        assertEquals(1, result1.size());
        assertEquals(s1.getExternalId(), result1.keySet().iterator().next());
        assertEquals(s1.getProtocol().getId(), result1.values().iterator().next().getProtocol().getId());
        assertEquals(s1.getExternalIdAssigner().getId(),
                result1.values().iterator().next().getExternalIdAssigner().getId());
        checkSpecimenObjects(result1.values().iterator().next(), codeSuffix - 1);
        Map<String, Specimen> result2 =
            specimenService.getSpecimens(s1.getExternalIdAssigner().getId(), Collections.singleton(s2.getExternalId()));
        assertNotNull(result2);
        assertEquals(1, result2.size());
        assertEquals(s2.getProtocol().getId(), result2.values().iterator().next().getProtocol().getId());
        assertEquals(s2.getExternalIdAssigner().getId(),
                result2.values().iterator().next().getExternalIdAssigner().getId());
        checkSpecimenObjects(result2.values().iterator().next(), codeSuffix);

        List<String> extIds = Arrays.asList(s1.getExternalId(), s2.getExternalId());
        Map<String, Specimen> result3 = specimenService.getSpecimens(s1.getExternalIdAssigner().getId(), extIds);
        assertNotNull(result3);
        assertTrue(result3.keySet().contains(s1.getExternalId()));
        assertTrue(result3.keySet().contains(s2.getExternalId()));
        assertTrue(result3.values().contains(s1));
        assertTrue(result3.values().contains(s2));

        result3 = specimenService.getSpecimens(s1.getExternalIdAssigner().getId(), new ArrayList<String>());
        assertNotNull(result3);
        assertTrue(result3.isEmpty());

        // Test update specimen
        codeSuffix++;

        Specimen s3 = createThirdSpecimen();
        updateSpecimen(result1.values().iterator().next(), s3);
        Specimen imported3 = specimenService.importSpecimen(result1.values().iterator().next());
        assertNotNull(imported3);
        assertNotNull(imported3.getParticipant().getId());
        assertNotNull(imported3.getProtocol().getId());
        Specimen updatedSpecimen = null;
        try {
            updatedSpecimen = specimenService.getSpecimens(s1.getExternalIdAssigner().getId(),
                    Collections.singleton(s1.getExternalId())).values().iterator().next();
        } catch (InvalidStateException ise) {
            StringBuffer buff = new StringBuffer();
            for (InvalidValue value : ise.getInvalidValues()) {
                buff.append(value.getMessage() + ". ");
            }
            assertEquals("", buff.toString());
        }

        assertTrue(s3.equals(updatedSpecimen));
        checkUpdatedSpecimen(updatedSpecimen);
    }

    private Specimen createFirstSpecimen() {
        Specimen s1 = new Specimen();
        s1.setExternalId("ABRC-1234" + codeSuffix);
        s1.setPatientAgeAtCollection(1);
        s1.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        s1.setAvailableQuantity(new BigDecimal("1.0"));
        s1.setAvailableQuantityUnits(QuantityUnits.MG);
        s1.setCollectionYear(Calendar.getInstance().get(Calendar.YEAR));

        s1.setMinimumPrice(new BigDecimal("12.34"));
        s1.setMaximumPrice(new BigDecimal("56.78"));
        s1.setPriceNegotiable(false);
        s1.setStatus(SpecimenStatus.UNAVAILABLE);
        setupSpecimenObjects(s1);
        return s1;
    }

    private void setupSpecimenObjects(Specimen specimen) {
        AdditionalPathologicFinding apf =
            SpecimenPersistenceHelper.createCode(AdditionalPathologicFinding.class, codeSuffix);
        SpecimenType st = SpecimenPersistenceHelper.createCode(SpecimenType.class, codeSuffix);
        Institution i = EjbTestHelper.getGenericServiceBean(Institution.class).getPersistentObject(Institution.class,
                getCurrentUser().getInstitution().getId());

        Participant participant = SpecimenPersistenceHelper.createAndStoreParticipant(i, codeSuffix);

        CollectionProtocol protocol = SpecimenPersistenceHelper.createAndStoreProtocol(i, codeSuffix);

        specimen.setPathologicalCharacteristic(apf);
        specimen.setSpecimenType(st);
        specimen.setExternalIdAssigner(i);
        specimen.setParticipant(participant);
        specimen.setProtocol(protocol);
    }

    private Specimen createSecondSpecimen() {
        Specimen s2 = new Specimen();
        s2.setExternalId("ABRC-3456" + codeSuffix);
        s2.setPatientAgeAtCollection(2);
        s2.setPatientAgeAtCollectionUnits(TimeUnits.MONTHS);
        s2.setAvailableQuantity(new BigDecimal("2.0"));
        s2.setAvailableQuantityUnits(QuantityUnits.ML);
        s2.setPriceNegotiable(true);
        s2.setStatus(SpecimenStatus.AVAILABLE);
        setupSpecimenObjects(s2);
        return s2;
    }
    private Specimen createThirdSpecimen() {
        Specimen s3 = new Specimen();
        // Set s3's externalId to s1's external id in order to overwrite s1 on update
        s3.setExternalId("ABRC-1234" + (codeSuffix - 2));
        s3.setPatientAgeAtCollection(AGE);
        s3.setPatientAgeAtCollectionUnits(TimeUnits.MONTHS);
        s3.setAvailableQuantity(new BigDecimal("3.0"));
        s3.setAvailableQuantityUnits(QuantityUnits.CELL_COUNT);
        s3.setCollectionYear(Calendar.getInstance().get(Calendar.YEAR) - 1);
        s3.setPriceNegotiable(true);
        s3.setStatus(SpecimenStatus.AVAILABLE);
        setupSpecimenObjects(s3);
        return s3;
    }

    private void updateSpecimen(Specimen storedSpecimen, Specimen importedSpecimen) {
        storedSpecimen.setAvailableQuantity(importedSpecimen.getAvailableQuantity());
        storedSpecimen.setAvailableQuantityUnits(importedSpecimen.getAvailableQuantityUnits());
        storedSpecimen.setExternalIdAssigner(importedSpecimen.getExternalIdAssigner());
        storedSpecimen.setMaximumPrice(importedSpecimen.getMaximumPrice());
        storedSpecimen.setMinimumPrice(importedSpecimen.getMinimumPrice());
        storedSpecimen.setParticipant(importedSpecimen.getParticipant());
        storedSpecimen.setPathologicalCharacteristic(importedSpecimen.getPathologicalCharacteristic());
        storedSpecimen.setPatientAgeAtCollection(importedSpecimen.getPatientAgeAtCollection());
        storedSpecimen.setPatientAgeAtCollectionUnits(importedSpecimen.getPatientAgeAtCollectionUnits());
        storedSpecimen.setCollectionYear(importedSpecimen.getCollectionYear());
        storedSpecimen.setPreviousStatus(importedSpecimen.getPreviousStatus());
        storedSpecimen.setPriceNegotiable(importedSpecimen.isPriceNegotiable());
        storedSpecimen.setProtocol(importedSpecimen.getProtocol());
        storedSpecimen.setSpecimenType(importedSpecimen.getSpecimenType());
        storedSpecimen.setStatus(importedSpecimen.getStatus());
    }

    private void checkUpdatedSpecimen(Specimen updatedSpecimen) {
        assertEquals("ABRC-1234" + (codeSuffix - 2), updatedSpecimen.getExternalId());
        assertEquals(AGE, updatedSpecimen.getPatientAgeAtCollection());
        assertEquals(TimeUnits.MONTHS, updatedSpecimen.getPatientAgeAtCollectionUnits());
        assertEquals(new BigDecimal("3.0"), updatedSpecimen.getAvailableQuantity());
        assertEquals(QuantityUnits.CELL_COUNT, updatedSpecimen.getAvailableQuantityUnits());
        assertTrue(updatedSpecimen.isPriceNegotiable());
        assertNull(updatedSpecimen.getMinimumPrice());
        assertNull(updatedSpecimen.getMaximumPrice());
        assertEquals(SpecimenStatus.AVAILABLE, updatedSpecimen.getStatus());
        checkSpecimenObjects(updatedSpecimen, codeSuffix);
    }

    private void checkSpecimenObjects(Specimen specimen, int suffix) {
        assertEquals(SpecimenPersistenceHelper.generateCodeName(AdditionalPathologicFinding.class, suffix),
                specimen.getPathologicalCharacteristic().getName());
        assertEquals(SpecimenPersistenceHelper.generateCodeName(SpecimenType.class, suffix),
                specimen.getSpecimenType().getName());
        assertEquals("test name" + suffix, specimen.getProtocol().getName());
        checkParticipant(specimen.getParticipant(), suffix, specimen.getExternalIdAssigner());
    }

    private void checkParticipant(Participant participant, int suffix, Institution i) {
        assertEquals(Ethnicity.HISPANIC_OR_LATINO, participant.getEthnicity());
        assertEquals(2, participant.getRaces().size());
        assertEquals("external id 123" + suffix, participant.getExternalId());
        assertEquals(Gender.FEMALE, participant.getGender());
        assertEquals(i.getId(), participant.getExternalIdAssigner().getId());
    }

    /**
     * test the get remote specimen service with null a null participant and protocol.
     */
    @Test(expected = NullPointerException.class)
    public void testImportSpecimenException() {
        Specimen s = createFirstSpecimen();
        s.setParticipant(null);
        s.setProtocol(null);
        specimenService.importSpecimen(s);
    }
}
