/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.BooleanDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.DecimalDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.ExtendableEntity;
import com.fiveamsolutions.dynamicextensions.IntegerDynamicFieldDefinition;

/**
 * SearchParameterTypeConverter unit tests.
 *
 * @author jstephens
 */
public class SearchParameterTypeConverterTest {

    private SearchParameterTypeConverter converter;
    private List<AbstractDynamicFieldDefinition> definitions;

    //CHECKSTYLE:OFF

    /**
     * Initialize the tests.
     */
    @Before
    public void setUpTest() {
        createDynamicFieldDefinitions();
        converter = new SearchParameterTypeConverter(TestEntity.class);
    }

    /**
     * Tests converting object to boolean.
     */
    @Test
    public void shouldConvertObjectToBoolean()
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = converter.convertValue("booleanField", "false");
        assertTrue(convertedValue instanceof Boolean);
    }

    /**
     * Tests converting object to date in RFC3339 format.
     */
    @Test
    public void shouldConvertObjectToDateRFC3339Format()
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = converter.convertValue("dateField", "1970-01-01T00:00:00");
        assertTrue(convertedValue instanceof Date);
    }

    /**
     * Tests converting object to date in short format.
     */
    @Test
    public void shouldConvertObjectToDateShortFormat()
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = converter.convertValue("dateField", "1/1/70");
        assertTrue(convertedValue instanceof Date);
    }

    /**
     * Tests converting object to decimal.
     */
    @Test
    public void shouldConvertObjectToDecimal()
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = converter.convertValue("decimalField", "0.0");
        assertTrue(convertedValue instanceof BigDecimal);
    }

    /**
     * Tests converting object to double.
     */
    @Test
    public void shouldConvertObjectToDouble()
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = converter.convertValue("doubleField", "0.0");
        assertTrue(convertedValue instanceof Double);
    }

    /**
     * Tests converting object to float.
     */
    @Test
    public void shouldConvertObjectToFloat()
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = converter.convertValue("floatField", "0.0");
        assertTrue(convertedValue instanceof Float);
    }

    /**
     * Tests converting object to integer.
     */
    @Test
    public void shouldConvertObjectToInteger()
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = converter.convertValue("integerField", "0");
        assertTrue(convertedValue instanceof Integer);
    }

    /**
     * Tests converting object to long.
     */
    @Test
    public void shouldConvertStringToLong()
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = converter.convertValue("longField", "0");
        assertTrue(convertedValue instanceof Long);
    }

    /**
     * Tests converting dynamic extension to boolean.
     */
    @Test
    public void shouldConvertDynamicExtensionToBoolean()
            throws NoSuchFieldException, ClassNotFoundException {
        converter = new SearchParameterTypeConverter(TestEntity.class, definitions);
        Object convertedValue = converter.convertValue("booleanDynamicField", "false");
        assertTrue(convertedValue instanceof Boolean);
    }

    /**
     * Tests converting dynamic extension to decimal.
     */
    @Test
    public void shouldConvertDynamicExtensionToDecimal()
            throws NoSuchFieldException, ClassNotFoundException {
        converter = new SearchParameterTypeConverter(TestEntity.class, definitions);
        Object convertedValue = converter.convertValue("decimalDynamicField", "0.0");
        assertTrue(convertedValue instanceof BigDecimal);
    }

    /**
     * Tests converting dynamic extension to integer.
     */
    @Test
    public void shouldConvertDynamicExtensionToInteger()
            throws NoSuchFieldException, ClassNotFoundException {
        converter = new SearchParameterTypeConverter(TestEntity.class, definitions);
        Object convertedValue = converter.convertValue("integerDynamicField", "0");
        assertTrue(convertedValue instanceof Integer);
    }

    /**
     * Tests converting a nested property to long.
     */
    @Test
    public void shouldConvertNestedPropertyToLong()
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = converter.convertValue("nestedEntity.id", "0");
        assertTrue(convertedValue instanceof Long);
    }

    /**
     * Tests that an empty parameter value is not converted.
     */
    @Test
    public void shouldNotConvertEmptyParameterValue()
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = converter.convertValue("decimalField", "");
        assertEquals(StringUtils.EMPTY, convertedValue);
    }

    /**
     * Tests that an unknown field throws an exception.
     */
    @Test(expected = NoSuchFieldException.class)
    public void shouldThrowExceptionForUnknownField()
            throws NoSuchFieldException, ClassNotFoundException {
        converter.convertValue("unknownField", "0");
    }

    private void createDynamicFieldDefinitions() {
        definitions = new ArrayList<AbstractDynamicFieldDefinition>();
        BooleanDynamicFieldDefinition bfd = new BooleanDynamicFieldDefinition();
        bfd.setEntityClassName(TestEntity.class.getName());
        bfd.setFieldName("booleanDynamicField");
        bfd.setFieldDisplayName("Boolean Dynamic Field");
        definitions.add(bfd);

        DecimalDynamicFieldDefinition dfd = new DecimalDynamicFieldDefinition();
        dfd.setEntityClassName(TestEntity.class.getName());
        dfd.setFieldName("decimalDynamicField");
        dfd.setFieldDisplayName("Decimal Dynamic Field");
        definitions.add(dfd);

        IntegerDynamicFieldDefinition ifd = new IntegerDynamicFieldDefinition();
        ifd.setEntityClassName(TestEntity.class.getName());
        ifd.setFieldName("integerDynamicField");
        ifd.setFieldDisplayName("Integer Dynamic Field");
        definitions.add(ifd);
    }

    @SuppressWarnings({ "PMD.UnusedPrivateField", "unused" })
    public class TestEntity implements ExtendableEntity {

        private Boolean booleanField;
        private Date dateField;
        private BigDecimal decimalField;
        private Double doubleField;
        private Float floatField;
        private Integer integerField;
        private Long longField;
        private Map<String, Object> customProperties;
        private NestedEntity nestedEntity;

        @Override
        public Object getCustomProperty(String name) {
            return customProperties.get(name);
        }

        @Override
        public void setCustomProperty(String name, Object value) {
            customProperties.put(name, value);
        }

    }

    @SuppressWarnings({ "PMD.UnusedPrivateField", "unused" })
    public class NestedEntity {

        private Long id;

    }

    //CHECKSTYLE:ON

}