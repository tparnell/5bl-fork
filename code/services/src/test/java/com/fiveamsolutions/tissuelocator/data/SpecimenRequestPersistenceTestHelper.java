/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import com.fiveamsolutions.tissuelocator.data.code.FundingSource;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenReceiptQuality;
import com.fiveamsolutions.tissuelocator.service.CodeServiceBean;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceBean;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractSpecimenRequestTest;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author gvaughn
 *
 */
public class SpecimenRequestPersistenceTestHelper {

    /**
     * Get a new request vote.
     * @param user Vote user.
     * @return a new request vote.
     */
    public static SpecimenRequestReviewVote getVote(TissueLocatorUser user) {
        SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
        vote.setDate(new Date());
        vote.setInstitution(user.getInstitution());
        vote.setUser(user);
        return vote;
    }

    /**
     * Populate a request vote.
     * @param vote Vote to be populated.
     * @param voteValue Vote value.
     * @param user Vote user.
     */
    public static void setVote(SpecimenRequestReviewVote vote, Vote voteValue, TissueLocatorUser user) {
        vote.setDate(new Date());
        vote.setVote(voteValue);
        vote.setComment("test comment");
        vote.setUser(user);
    }

    /**
     * Create a new specimen and add it to a request.
     * @param request Request to which the specimen will be added.
     * @param user User whose institution will be used to populate the specimen.
     * @param specimen Template specimen.
     * @return The new specimen.
     */
    public static Specimen createNewSpecimen(SpecimenRequest request, TissueLocatorUser user, Specimen specimen) {
        Specimen s1 = new Specimen();
        s1.setExternalId("ABRC-12348");
        s1.setPatientAgeAtCollection(1);
        s1.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        assertNotNull(TimeUnits.YEARS.getResourceKey());
        s1.setAvailableQuantity(new BigDecimal("1.0"));
        s1.setAvailableQuantityUnits(QuantityUnits.MG);
        assertNotNull(QuantityUnits.MG.getResourceKey());
        s1.setPathologicalCharacteristic(specimen.getPathologicalCharacteristic());
        s1.setSpecimenType(specimen.getSpecimenType());
        s1.setExternalIdAssigner(user.getInstitution());
        s1.setParticipant(new Participant());
        s1.getParticipant().setExternalId("0987");
        s1.getParticipant().setExternalIdAssigner(user.getInstitution());
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(s1.getParticipant());
        s1.setProtocol(new CollectionProtocol());
        s1.getProtocol().setName("testProt3");
        s1.getProtocol().setInstitution(user.getInstitution());
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(s1.getProtocol());
        s1.setCollectionYear(specimen.getCollectionYear());
        s1.setMinimumPrice(new BigDecimal("12.34"));
        s1.setMaximumPrice(new BigDecimal("56.78"));
        s1.setPriceNegotiable(false);
        s1.setStatus(SpecimenStatus.UNDER_REVIEW);

        SpecimenRequestLineItem li = new SpecimenRequestLineItem();
        li.setSpecimen(s1);
        li.setQuantity(new BigDecimal(1));
        li.setQuantityUnits(QuantityUnits.MG);
        request.getLineItems().add(li);
        return s1;
    }

    /**
     * Get a new review comment.
     * @param reviewer Comment user.
     * @param date Comment date.
     * @return a new review comment.
     */
    public static ReviewComment getReviewComment(TissueLocatorUser reviewer, Date date) {
        ReviewComment rc = new ReviewComment();
        rc.setComment("review comment");
        rc.setUser(reviewer);
        rc.setDate(date);
        return rc;
    }

    /**
     * Set the prospective collection display setting.
     * @param setting the prospective collection display setting.
     */
    public static void createProspectiveCollectionApplicationSetting(boolean setting) {
        ApplicationSetting as = new ApplicationSetting();
        as.setName("display_prospective_collection");
        as.setValue(String.valueOf(setting));
        new ApplicationSettingServiceBean().savePersistentObject(as);
    }

    /**
     * create the review process application setting.
     * @param process the value of the application setting.
     */
    public static void createReviewProcessSetting(ReviewProcess process) {
        ApplicationSetting as = new ApplicationSetting();
        as.setName("review_process");
        as.setValue(process.name());
        new ApplicationSettingServiceBean().savePersistentObject(as);
    }

    /**
     * Reset request votes.
     * @param request Request.
     * @param service Request service.
     * @param partialApproval If request should be partially approved.
     * @return Updated request.
     */
    public static SpecimenRequest resetRequest(SpecimenRequest request, SpecimenRequestServiceLocal service,
            boolean partialApproval) {
        // revote - everyone approves so shipments should be created
        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, -AbstractSpecimenRequestTest.VOTING_PERIOD - 1);
        request.setUpdatedDate(cal.getTime());
        request.setStatus(RequestStatus.PENDING);
        for (SpecimenRequestReviewVote consortiumVote : request.getConsortiumReviews()) {
            consortiumVote.setDate(new Date());
            consortiumVote.setVote(Vote.APPROVE);
        }
        if (partialApproval) {
            request.getInstitutionalReviews().iterator().next().setVote(Vote.DENY);
        }
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            lineItem.getSpecimen().setStatus(SpecimenStatus.UNDER_REVIEW);
        }
        service.savePersistentObject(request);

        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        return request;
    }

    /**
     * Creates application settings required for request persistence.
     */
    public static void createRequiredApplicationSettings() {
        ApplicationSettingServiceLocal service = new ApplicationSettingServiceBean();
        try {
            service.isMtaCertificationRequired();
        } catch (Exception e) {
            createMtaCertificationSetting(false);
        }
        try {
            service.isIrbApprovalRequired();
        } catch (Exception e) {
            createStudyIrbApprovalRequiredSetting(false);
        }
        try {
            service.getMinimunRequiredFundingStatus();
        } catch (Exception e) {
            createMinimumFundingStatusSetting(FundingStatus.NOT_FUNDED);
        }
        try {
            service.isProtocolDocumentRequired();
        } catch (Exception e) {
            createProtocolDocumentRequiredSetting(false);
        }
    }

    /**
     * Create the minimum required funding status setting.
     * @param status the minimum funding status to set.
     */
    public static void createMinimumFundingStatusSetting(FundingStatus status) {
        ApplicationSettingServiceLocal service = new ApplicationSettingServiceBean();
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("minimum_required_funding_status");
        setting.setValue(status.toString());
        service.savePersistentObject(setting);
    }

    /**
     * Create an mta certification application setting.
     * @param required whether certification is required.
     */
    public static void createMtaCertificationSetting(boolean required) {
        ApplicationSettingServiceLocal service = new ApplicationSettingServiceBean();
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("mta_certification_required");
        setting.setValue(required ? "true" : "false");
        service.savePersistentObject(setting);
    }

    /**
     * Create an irb approval required application setting.
     * @param required whether approval is required.
     */
    public static void createStudyIrbApprovalRequiredSetting(boolean required) {
        ApplicationSettingServiceLocal service = new ApplicationSettingServiceBean();
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("irb_approval_required");
        setting.setValue(required ? "true" : "false");
        service.savePersistentObject(setting);
    }

    /**
     * Create the protocol document required application setting.
     * @param required whether the protocol document is required.
     */
    public static void createProtocolDocumentRequiredSetting(boolean required) {
        ApplicationSettingServiceLocal service = new ApplicationSettingServiceBean();
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("protocol_document_required");
        setting.setValue(required ? "true" : "false");
        service.savePersistentObject(setting);
    }

    /**
     * Create a funding source code.
     * @return a funding source code
     */
    public static FundingSource createFundingSource() {
        FundingSource fs = new FundingSource();
        fs.setName("Other");
        fs.setActive(true);
        CodeServiceLocal service = new CodeServiceBean();
        Long id = service.savePersistentObject(fs);
        return service.getPersistentObject(FundingSource.class, id);
    }

    /**
     * Create a specimen receipt quality code.
     * @return a specimen receipt quality code
     */
    public static SpecimenReceiptQuality createReceiptQuality() {
        SpecimenReceiptQuality srq = new SpecimenReceiptQuality();
        srq.setName("Acceptable");
        srq.setActive(true);
        CodeServiceLocal service = new CodeServiceBean();
        Long id = service.savePersistentObject(srq);
        return service.getPersistentObject(SpecimenReceiptQuality.class, id);
    }
}
