/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.config.category;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.category.SpecimenExternalIdAssignerDecider;
import com.fiveamsolutions.tissuelocator.data.config.category.SpecimenExternalIdDecider;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.test.InstitutionPersistenceHelper;

/**
 * @author gvaughn
 *
 */
public class DisplayableDeciderTest extends AbstractHibernateTestCase {

    /**
     * Test the external id decider.
     */
    @Test
    public void testSpecimenExternalIdDecider() {
        SpecimenExternalIdDecider decider = new SpecimenExternalIdDecider();
        Specimen specimen = new Specimen();
        UsernameHolder.setUser(null);
        assertFalse(decider.isFieldDisplayed(specimen, null));

        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution inst = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test", false);
        TissueLocatorUser user = createUserInRole(Role.TISSUE_LOCATOR_USER, inst);
        assertFalse(decider.isFieldDisplayed(specimen, user));

        user = createUserInRole(Role.SPECIMEN_ADMIN, inst);
        UsernameHolder.setUser(user.getEmail());
        assertTrue(decider.isFieldDisplayed(specimen, user));
    }

    /**
     * Test the external id assigner decider.
     */
    @Test
    public void testSpecimenExternalIdAssignerDecider() {
        SpecimenExternalIdAssignerDecider decider = new SpecimenExternalIdAssignerDecider();
        Specimen specimen = new Specimen();
        UsernameHolder.setUser(null);
        assertFalse(decider.isFieldDisplayed(specimen, null));

        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution inst = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test", false);
        TissueLocatorUser user = createUserInRole(Role.TISSUE_LOCATOR_USER, inst);
        UsernameHolder.setUser(user.getEmail());
        assertFalse(decider.isFieldDisplayed(specimen, user));

        user = createUserInRole(Role.SPECIMEN_LOCATION_VIEWER_PRE_APPROVE, inst);
        UsernameHolder.setUser(user.getEmail());
        assertTrue(decider.isFieldDisplayed(specimen, user));

        user = createUserInRole(Role.SPECIMEN_LOCATION_VIEWER_POST_APPROVE, inst);
        UsernameHolder.setUser(user.getEmail());
        assertFalse(decider.isFieldDisplayed(specimen, user));

        specimen.setLineItems(new ArrayList<SpecimenRequestLineItem>());
        specimen.getLineItems().add(new SpecimenRequestLineItem());
        specimen.getLineItems().add(new SpecimenRequestLineItem());
        assertFalse(decider.isFieldDisplayed(specimen, user));

        SpecimenRequest request = new SpecimenRequest();
        request.setStatus(RequestStatus.PENDING);
        specimen.getLineItems().get(1).setRequests(new ArrayList<SpecimenRequest>());
        specimen.getLineItems().get(1).getRequests().add(request);
        assertFalse(decider.isFieldDisplayed(specimen, user));

        request.setStatus(RequestStatus.APPROVED);
        assertTrue(decider.isFieldDisplayed(specimen, user));
    }
}
