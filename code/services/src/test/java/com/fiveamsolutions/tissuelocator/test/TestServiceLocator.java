/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.service.CodeServiceBean;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceBean;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceLocal;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceRemote;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceRemoteBean;
import com.fiveamsolutions.tissuelocator.service.FixedConsortiumReviewServiceBean;
import com.fiveamsolutions.tissuelocator.service.FixedConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.FullConsortiumReviewServiceBean;
import com.fiveamsolutions.tissuelocator.service.FullConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.HelpConfigServiceBean;
import com.fiveamsolutions.tissuelocator.service.HelpConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceBean;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceRemote;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceRemoteBean;
import com.fiveamsolutions.tissuelocator.service.LineItemReviewServiceBean;
import com.fiveamsolutions.tissuelocator.service.LineItemReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceBean;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceBean;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceRemote;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceRemoteBean;
import com.fiveamsolutions.tissuelocator.service.ReportServiceBean;
import com.fiveamsolutions.tissuelocator.service.ReportServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceBean;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SignedMaterialTransferAgreementServiceBean;
import com.fiveamsolutions.tissuelocator.service.SignedMaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestProcessingServiceBean;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestProcessingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceBean;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceBean;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemoteBean;
import com.fiveamsolutions.tissuelocator.service.locator.ServiceLocator;
import com.google.inject.Injector;

/**
 * @author smiller
 *
 */
public class TestServiceLocator implements ServiceLocator {

    private final Injector injector;

    /**
     * ctor.
     * @param injector the guice injector to use.
     */
    public TestServiceLocator(Injector injector) {
        this.injector = injector;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CodeServiceLocal getCodeService() {
        return injector.getInstance(CodeServiceBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectionProtocolServiceLocal getCollectionProtocolService() {
        return injector.getInstance(CollectionProtocolServiceBean.class);
    }

    /**
     * {@inheritDoc}
     */
    public CollectionProtocolServiceRemote getCollectionProtocolServiceRemote() {
        CollectionProtocolServiceRemoteBean serviceBean =
            injector.getInstance(CollectionProtocolServiceRemoteBean.class);
        serviceBean.setProtocolService(getCollectionProtocolService());
        return serviceBean;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public GenericServiceLocal<PersistentObject> getGenericService() {
        return injector.getInstance(GenericServiceBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InstitutionServiceLocal getInstitutionService() {
        InstitutionServiceBean institutionService = injector.getInstance(InstitutionServiceBean.class);
        institutionService.setProtocolService(getCollectionProtocolService());
        return institutionService;
    }

    /**
     * {@inheritDoc}
     */
    public InstitutionServiceRemote getInstitutionServiceRemote() {
        InstitutionServiceRemoteBean institutionService =
            injector.getInstance(InstitutionServiceRemoteBean.class);
        institutionService.setInstitutionService(injector.getInstance(InstitutionServiceBean.class));
        return institutionService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ParticipantServiceLocal getParticipantService() {
        return injector.getInstance(ParticipantServiceBean.class);
    }

    /**
     * {@inheritDoc}
     */
    public ParticipantServiceRemote getParticipantServiceRemote() {
        ParticipantServiceRemoteBean serviceBean = injector.getInstance(ParticipantServiceRemoteBean.class);
        serviceBean.setParticipantService(getParticipantService());
        return serviceBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShipmentServiceLocal getShipmentService() {
        ShipmentServiceBean bean = injector.getInstance(ShipmentServiceBean.class);
        bean.setSpecimenService(getSpecimenService());
        return bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenRequestProcessingServiceLocal getSpecimenRequestProcessingService() {
        SpecimenRequestProcessingServiceBean service = injector.getInstance(SpecimenRequestProcessingServiceBean.class);
        service.setSpecimenRequestService(getSpecimenRequestService());
        return service;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenRequestServiceLocal getSpecimenRequestService() {
        SpecimenRequestServiceBean service = injector.getInstance(SpecimenRequestServiceBean.class);
        populateDependencies(service);
        return service;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenServiceLocal getSpecimenService() {
        return injector.getInstance(SpecimenServiceBean.class);
    }

    /**
     * {@inheritDoc}
     */
    public SpecimenServiceRemote getSpecimenServiceRemote() {
        SpecimenServiceRemoteBean serviceBean = injector.getInstance(SpecimenServiceRemoteBean.class);
        serviceBean.setSpecimenService(getSpecimenService());
        return serviceBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FixedConsortiumReviewServiceLocal getFixedConsortiumReviewService() {
        FixedConsortiumReviewServiceBean service = injector.getInstance(FixedConsortiumReviewServiceBean.class);
        populateDependencies(service);
        return service;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FullConsortiumReviewServiceLocal getFullConsortiumReviewService() {
        FullConsortiumReviewServiceBean service = injector.getInstance(FullConsortiumReviewServiceBean.class);
        populateDependencies(service);
        return service;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LineItemReviewServiceLocal getLineItemReviewService() {
        LineItemReviewServiceBean service = injector.getInstance(LineItemReviewServiceBean.class);
        populateDependencies(service);
        return service;
    }

    private void populateDependencies(SpecimenRequestServiceBean service) {
        service.setShipmentService(getShipmentService());
        service.setInstitutionService(getInstitutionService());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MaterialTransferAgreementServiceLocal getMaterialTransferAgreementService() {
        MaterialTransferAgreementServiceBean service = injector.getInstance(MaterialTransferAgreementServiceBean.class);
        service.setInstitutionService(getInstitutionService());
        return service;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SignedMaterialTransferAgreementServiceLocal getSignedMaterialTransferAgreementService() {
        SignedMaterialTransferAgreementServiceBean service =
            injector.getInstance(SignedMaterialTransferAgreementServiceBean.class);
        service.setInstitutionService(getInstitutionService());
        return service;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReportServiceLocal getReportService() {
        ReportServiceBean service = injector.getInstance(ReportServiceBean.class);
        service.setInstitutionService(getInstitutionService());
        service.setRequestService(getSpecimenRequestService());
        service.setShipmentService(getShipmentService());
        service.setSpecimenService(getSpecimenService());
        return service;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HelpConfigServiceLocal getHelpConfigService() {
        return injector.getInstance(HelpConfigServiceBean.class);
    }

}
