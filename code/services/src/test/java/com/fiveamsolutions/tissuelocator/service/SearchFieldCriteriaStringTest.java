/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Address;
import com.fiveamsolutions.tissuelocator.data.Country;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.AutocompleteConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.CheckboxAutocompleteCompositeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.CheckboxConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.DataType;
import com.fiveamsolutions.tissuelocator.data.config.search.RangeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.RangeSelectCompositeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectDynamicExtensionConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SimpleSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.TextConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.TextSelectCompositeConfig;
import com.fiveamsolutions.tissuelocator.service.search.RangeSearchParameterAccessor;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.test.InstitutionPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
public class SearchFieldCriteriaStringTest extends AbstractHibernateTestCase {

    /**
     * test criteria string generation for text fields.
     */
    @Test
    public void testTextFieldCriteriaString() {
        TextConfig config = new TextConfig();
        populateSimpleConfig(config, 1L);
        config.getFieldConfig().setSearchFieldName("externalId");
        config.getFieldConfig().setSearchFieldDisplayName("External Id");
        Specimen s = new Specimen();
        assertTrue(StringUtils.isBlank(config.getCriteriaString(s, null)));
        s.setExternalId("external id");
        assertFalse(StringUtils.isBlank(config.getCriteriaString(s, null)));
        assertEquals("External Id: external id", config.getCriteriaString(s, null));
        config.setObjectProperty(true);
        assertEquals("External Id: external id", config.getCriteriaString(getWrapper(s), null));
    }

    /**
     * test criteria string generation for select fields with codes as their options.
     */
    @Test
    public void testCodeSelectFieldCriteriaString() {
        SelectConfig config = new SelectConfig();
        populateSimpleConfig(config, 1L);
        config.getFieldConfig().setSearchFieldName("specimenType");
        config.getFieldConfig().setSearchFieldDisplayName("Specimen Type");
        config.setListType(DataType.CODE);
        config.setListClass(SpecimenType.class.getName());
        Specimen s = new Specimen();
        assertTrue(StringUtils.isBlank(config.getCriteriaString(s, null)));
        SpecimenType as = new SpecimenType();
        as.setName("arm");
        as.setValue("arm code");
        as.setSpecimenClass(SpecimenClass.TISSUE);
        s.setSpecimenType(as);
        assertFalse(StringUtils.isBlank(config.getCriteriaString(s, null)));
        assertEquals("Specimen Type: arm", config.getCriteriaString(s, null));
        config.setObjectProperty(true);
        assertEquals("Specimen Type: arm", config.getCriteriaString(getWrapper(s), null));
        config.getFieldConfig().setSearchFieldName("specimenType.name");
        assertEquals("Specimen Type: arm", config.getCriteriaString(getWrapper(s), null));

        config.setMultiSelect(true);
        config.getFieldConfig().setSearchFieldName("params");
        config.setObjectProperty(false);
        SpecimenType t = new SpecimenType();
        t.setName("leg");
        t.setValue("leg code");
        t.setSpecimenClass(SpecimenClass.TISSUE);
        CodeServiceLocal service = TissueLocatorRegistry.getServiceLocator().getCodeService();
        SpecimenWrapper wrapper = new SpecimenWrapper();
        service.savePersistentObject(as);
        service.savePersistentObject(t);
        wrapper.getParams().add("arm");
        wrapper.getParams().add("leg");
        assertFalse(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));
        assertEquals("Specimen Type: arm, leg", config.getCriteriaString(wrapper, null));
    }

    /**
     * test criteria string generation for select fields with enums defining options.
     */
    @Test
    public void testEnumSelectFieldCriteriaString() {
        SelectConfig config = new SelectConfig();
        populateSimpleConfig(config, 1L);
        config.getFieldConfig().setSearchFieldName("availableQuantityUnits");
        config.getFieldConfig().setSearchFieldDisplayName("Available Quantity Units");
        config.setListType(DataType.ENUM);
        config.setListClass(QuantityUnits.class.getName());
        Specimen s = new Specimen();
        assertTrue(StringUtils.isBlank(config.getCriteriaString(s, null)));
        s.setAvailableQuantityUnits(QuantityUnits.CELLS);
        assertFalse(StringUtils.isBlank(config.getCriteriaString(s, null)));
        assertEquals("Available Quantity Units: cells", config.getCriteriaString(s, null));
        config.setObjectProperty(true);
        assertEquals("Available Quantity Units: cells", config.getCriteriaString(getWrapper(s), null));

        SpecimenWrapper wrapper = new SpecimenWrapper();
        wrapper.setObject(s);
        config.setMultiSelect(true);
        config.getFieldConfig().setSearchFieldName("params");
        config.setObjectProperty(false);
        wrapper.getParams().add(QuantityUnits.CELLS);
        wrapper.getParams().add(QuantityUnits.COUNT);
        assertFalse(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));
        assertEquals("Available Quantity Units: cells, count", config.getCriteriaString(wrapper, null));
    }

    /**
     * test criteria string generation for select fields with enums without a resource key property defining options.
     */
    @Test
    public void testEnumNoResourceKeySelectFieldCriteriaString() {
        SelectConfig config = new SelectConfig();
        populateSimpleConfig(config, 1L);
        config.getFieldConfig().setSearchFieldName("externalIdAssigner.billingAddress.country");
        config.getFieldConfig().setSearchFieldDisplayName("Country");
        config.setListType(DataType.ENUM);
        config.setListClass(Country.class.getName());
        Specimen s = new Specimen();
        s.setExternalIdAssigner(new Institution());
        s.getExternalIdAssigner().setBillingAddress(new Address());
        assertTrue(StringUtils.isBlank(config.getCriteriaString(s, null)));
        s.getExternalIdAssigner().getBillingAddress().setCountry(Country.AFGHANISTAN);
        assertFalse(StringUtils.isBlank(config.getCriteriaString(s, null)));
        assertEquals("Country: AFGHANISTAN", config.getCriteriaString(s, null));
        config.setObjectProperty(true);
        assertEquals("Country: AFGHANISTAN", config.getCriteriaString(getWrapper(s), null));

        SpecimenWrapper wrapper = new SpecimenWrapper();
        wrapper.setObject(s);
        config.setMultiSelect(true);
        config.getFieldConfig().setSearchFieldName("params");
        config.setObjectProperty(false);
        wrapper.getParams().add(Country.AFGHANISTAN);
        wrapper.getParams().add(Country.ALGERIA);
        assertFalse(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));
        assertEquals("Country: AFGHANISTAN, ALGERIA", config.getCriteriaString(wrapper, null));
    }

    /**
     * test criteria string generation for select fields with an institution list defining options.
     */
    @Test
    public void testInstitutionSelectFieldCriteriaString() {
        SelectConfig config = new SelectConfig();
        populateSimpleConfig(config, 1L);
        config.getFieldConfig().setSearchFieldName("externalIdAssigner");
        config.getFieldConfig().setSearchFieldDisplayName("External Id Assigner");
        config.setListType(DataType.INSTITUTION);
        config.setListClass(Institution.class.getName());
        Specimen s = new Specimen();
        assertTrue(StringUtils.isBlank(config.getCriteriaString(s, null)));
        Institution i = new Institution();
        i.setId(1L);
        i.setName("institution name");
        s.setExternalIdAssigner(i);
        assertFalse(StringUtils.isBlank(config.getCriteriaString(s, null)));
        assertEquals("External Id Assigner: institution name", config.getCriteriaString(s, null));
        config.setObjectProperty(true);
        assertEquals("External Id Assigner: institution name", config.getCriteriaString(getWrapper(s), null));

        SpecimenWrapper wrapper = new SpecimenWrapper();
        wrapper.setObject(s);
        config.setMultiSelect(true);
        config.getFieldConfig().setSearchFieldName("params");
        config.setObjectProperty(false);
        InstitutionServiceLocal service = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        InstitutionPersistenceHelper helper = InstitutionPersistenceHelper.getInstance();
        InstitutionType type = helper.createAndSaveDefaultType();
        i = helper.createInstitution(type, "institution name", true);
        Institution i2 = helper.createInstitution(type, "institution 2 name", true);
        wrapper.getParams().add(service.savePersistentObject(i));
        wrapper.getParams().add(service.savePersistentObject(i2));
        assertFalse(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));
        assertEquals("External Id Assigner: institution name, institution 2 name", 
                config.getCriteriaString(wrapper, null));
    }

    /**
     * test criteria string generation for range fields.
     */
    @Test
    public void testRangeFieldCriteriaString() {
        RangeConfig config = new RangeConfig();
        populateSimpleConfig(config, 1L);
        config.getFieldConfig().setSearchFieldName("timeLapseToProcessing");
        config.getFieldConfig().setSearchFieldDisplayName("Time Lapse");

        Map<String, Object> rangeSearchParameters = new HashMap<String, Object>();
        SpecimenWrapper wrapper = getWrapper(new Specimen());
        wrapper.setRangeSearchParameters(rangeSearchParameters);
        assertTrue(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));

        rangeSearchParameters.put(config.getMinSearchFieldName(), 1);
        assertEquals("Time Lapse: greater than or equal to 1", config.getCriteriaString(wrapper, null));

        rangeSearchParameters.put(config.getMinSearchFieldName(), 2);
        rangeSearchParameters.put(config.getMaxSearchFieldName(), 2);
        assertEquals("Time Lapse: 2", config.getCriteriaString(wrapper, null));

        rangeSearchParameters.put(config.getMinSearchFieldName(), null);
        rangeSearchParameters.put(config.getMaxSearchFieldName(), 2);
        assertEquals("Time Lapse: less than or equal to 2", config.getCriteriaString(wrapper, null));

        rangeSearchParameters.put(config.getMinSearchFieldName(), 1);
        assertEquals("Time Lapse: 1 - 2", config.getCriteriaString(wrapper, null));

        config.setObjectProperty(true);
        assertEquals("Time Lapse: 1 - 2", config.getCriteriaString(wrapper, null));
    }

    /**
     * test criteria string generation for select dynamic extension fields.
     */
    @Test
    public void testSelectDynamicExtensionFieldCriteriaString() {
        TextConfig config = new TextConfig();
        String mapName = "customProperties";
        populateSimpleConfig(config, 1L);
        config.getFieldConfig().setSearchFieldName("storageCondition");
        config.getFieldConfig().setSearchFieldDisplayName("DBS Specimen Storage Condition");
        Specimen s = new Specimen();
        assertTrue(StringUtils.isBlank(config.getCriteriaString(s, mapName)));
        s.setCustomProperty("storageCondition", "Refrigerated");
        assertFalse(StringUtils.isBlank(config.getCriteriaString(s, mapName)));
        assertEquals("DBS Specimen Storage Condition: Refrigerated", config.getCriteriaString(s, mapName));
        config.setObjectProperty(true);
        assertEquals("DBS Specimen Storage Condition: Refrigerated", config.getCriteriaString(getWrapper(s), mapName));
    }

    /**
     * Test the criteria string generation.
     */
    @Test
    public void testMultiSelectDEFieldCriteriaString() {
        SelectDynamicExtensionConfig c = new SelectDynamicExtensionConfig();
        c.setMultiSelect(true);
        c.setFieldConfig(new GenericFieldConfig());
        c.getFieldConfig().setSearchFieldName("param");
        c.setObjectProperty(false);
        c.getFieldConfig().setSearchFieldDisplayName("label");

        TestObject obj = new TestObject();
        List<String> param = new ArrayList<String>();
        param.add("value1");
        param.add("value2");
        obj.setParam(param);
        String criteria = c.getCriteriaString(obj, null);
        assertEquals("label: value1, value2", criteria);

        obj.setParam("value1");
        c.setMultiSelect(false);
        criteria = c.getCriteriaString(obj, null);
        assertEquals("label: value1", criteria);

        obj.setParam(null);
        assertTrue(StringUtils.isEmpty(c.getCriteriaString(obj, null)));
    }

    /**
     * @author smiller
     */
    public class TestObject {

        private Object param;

        /**
         * get the param.
         *
         * @return the param
         */
        public Object getParam() {
            return param;
        }

        /**
         * @param param the param to set
         */
        public void setParam(Object param) {
            this.param = param;
        }
    }

    /**
     * test criteria string generation for text select fields.
     */
    @Test
    public void testTextSelectFieldCriteriaString() {
        TextConfig textConfig = new TextConfig();
        populateSimpleConfig(textConfig, 1L);
        textConfig.getFieldConfig().setSearchFieldName("availableQuantity");
        textConfig.getFieldConfig().setSearchFieldDisplayName("Available Quantity");
        SelectConfig selectConfig = new SelectConfig();
        populateSimpleConfig(selectConfig, 1L);
        selectConfig.getFieldConfig().setSearchFieldName("availableQuantityUnits");
        selectConfig.getFieldConfig().setSearchFieldDisplayName("");
        selectConfig.setListType(DataType.ENUM);
        selectConfig.setListClass(QuantityUnits.class.getName());
        TextSelectCompositeConfig config = new TextSelectCompositeConfig();
        config.setTextConfig(textConfig);
        config.setSelectConfig(selectConfig);
        Specimen s = new Specimen();
        assertTrue(StringUtils.isBlank(config.getCriteriaString(s, null)));
        s.setAvailableQuantity(new BigDecimal("100"));
        assertFalse(StringUtils.isBlank(config.getCriteriaString(s, null)));
        assertEquals("Available Quantity: 100", config.getCriteriaString(s, null));
        s.setAvailableQuantity(null);
        s.setAvailableQuantityUnits(QuantityUnits.MG);
        assertEquals("Available Quantity: milligrams", config.getCriteriaString(s, null));
        s.setAvailableQuantity(new BigDecimal("100"));
        assertEquals("Available Quantity: 100 milligrams", config.getCriteriaString(s, null));
        textConfig.setObjectProperty(true);
        selectConfig.setObjectProperty(true);
        assertEquals("Available Quantity: 100 milligrams", config.getCriteriaString(getWrapper(s), null));

        config.getSelectConfig().setShowEmptyOption(false);
        config.getSelectConfig().setObjectProperty(false);
        s.setAvailableQuantityUnits(QuantityUnits.MG);
        s.setAvailableQuantity(null);
        assertTrue(StringUtils.isBlank(config.getCriteriaString(s, null)));
    }

    /**
     * test criteria string generation for range select fields.
     */
    @Test
    public void testRangeSelectFieldCriteriaString() {
        RangeConfig rangeConfig = new RangeConfig();
        populateSimpleConfig(rangeConfig, 1L);
        rangeConfig.getFieldConfig().setSearchFieldDisplayName("Patient's Age at Collection");
        rangeConfig.getFieldConfig().setSearchFieldName("patientAgeAtCollection");
        rangeConfig.setSearchConditionClass("TimeUnitRangeSearchCondition");
        rangeConfig.setObjectProperty(false);

        SelectConfig selectConfig = new SelectConfig();
        populateSimpleConfig(selectConfig, 1L);
        selectConfig.getFieldConfig().setSearchFieldName("patientAgeAtCollectionUnits");
        selectConfig.getFieldConfig().setSearchFieldDisplayName("");
        selectConfig.setListType(DataType.ENUM);
        selectConfig.setListClass(TimeUnits.class.getName());
        selectConfig.setObjectProperty(true);

        RangeSelectCompositeConfig config = new RangeSelectCompositeConfig();
        config.setRangeConfig(rangeConfig);
        config.setSelectConfig(selectConfig);
        assertEquals(rangeConfig.getSearchFieldDisplayName(), config.getSearchFieldDisplayName());
        assertEquals(rangeConfig.getSearchFieldName(), config.getSearchFieldName());
        assertEquals(rangeConfig.getSearchConditionClass(), config.getSearchConditionClass());

        Map<String, Object> rangeSearchParameters = new HashMap<String, Object>();
        Specimen s = new Specimen();
        s.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        SpecimenWrapper wrapper = new SpecimenWrapper();
        wrapper.setObject(s);
        wrapper.setRangeSearchParameters(rangeSearchParameters);
        assertEquals("Patient's Age at Collection: years", config.getCriteriaString(wrapper, null));

        rangeSearchParameters.put(rangeConfig.getMinSearchFieldName(), null);
        rangeSearchParameters.put(rangeConfig.getMaxSearchFieldName(), null);
        assertEquals("Patient's Age at Collection: years", config.getCriteriaString(wrapper, null));

        rangeSearchParameters.put(rangeConfig.getMinSearchFieldName(), 1);
        rangeSearchParameters.put(rangeConfig.getMaxSearchFieldName(), 2);
        assertEquals("Patient's Age at Collection: 1 - 2 years", config.getCriteriaString(wrapper, null));

        config.getSelectConfig().setShowEmptyOption(false);
        rangeSearchParameters.put(rangeConfig.getMinSearchFieldName(), null);
        rangeSearchParameters.put(rangeConfig.getMaxSearchFieldName(), null);
        assertTrue(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));
        
        config.getSelectConfig().setShowEmptyOption(true);
        wrapper.getObject().setPatientAgeAtCollectionUnits(null);
        rangeSearchParameters.put(rangeConfig.getMinSearchFieldName(), null);
        rangeSearchParameters.put(rangeConfig.getMaxSearchFieldName(), null);
        assertTrue(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));
    }

    /**
     * test criteria string generation for checkbox fields.
     */
    @Test
    public void testCheckboxFieldCriteriaString() {
        CheckboxConfig config = new CheckboxConfig();
        populateSimpleConfig(config, 1L);
        config.getFieldConfig().setSearchFieldName("booleanProperty");
        config.getFieldConfig().setSearchFieldDisplayName("Boolean Property");
        SpecimenWrapper wrapper = getWrapper(new Specimen());
        assertTrue(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));
        wrapper.setBooleanProperty(Boolean.FALSE);
        assertFalse(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));
        assertEquals("Boolean Property: No", config.getCriteriaString(wrapper, null));
        wrapper.setBooleanProperty(Boolean.TRUE);
        assertFalse(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));
        assertEquals("Boolean Property: Yes", config.getCriteriaString(wrapper, null));
        config.getFieldConfig().setSearchFieldName("object");
        assertTrue(StringUtils.isBlank(config.getCriteriaString(wrapper, null)));
    }

    /**
     * test criteria string generation for autocomplete fields.
     */
    @Test
    public void testAutocompleteFieldCriteriaString() {
        AutocompleteConfig config = new AutocompleteConfig();
        populateSimpleConfig(config, 1L);
        config.getFieldConfig().setSearchFieldName("specimenType");
        config.getFieldConfig().setSearchFieldDisplayName("Specimen Type");
        Specimen s = new Specimen();
        assertTrue(StringUtils.isBlank(config.getCriteriaString(s, null)));
        SpecimenType as = new SpecimenType();
        as.setId(1L);
        as.setName("arm");
        as.setValue("arm code");
        s.setSpecimenType(as);
        assertFalse(StringUtils.isBlank(config.getCriteriaString(s, null)));
        assertEquals("Specimen Type: arm", config.getCriteriaString(s, null));
        config.setObjectProperty(true);
        assertEquals("Specimen Type: arm", config.getCriteriaString(getWrapper(s), null));
        config.getFieldConfig().setSearchFieldName("specimenType.name");
        assertEquals("Specimen Type: arm", config.getCriteriaString(getWrapper(s), null));
    }

    /**
     * test criteria string generation for checkbox autocomplete fields.
     */
    @Test
    public void testCheckboxAutocompleteFieldCriteriaString() {
        CheckboxConfig checkboxConfig = new CheckboxConfig();
        populateSimpleConfig(checkboxConfig, 1L);
        checkboxConfig.getFieldConfig().setSearchFieldName("normalSample");
        checkboxConfig.getFieldConfig().setSearchFieldDisplayName("Normal Sample");
        AutocompleteConfig autoConfig = new AutocompleteConfig();
        populateSimpleConfig(autoConfig, 1L);
        autoConfig.getFieldConfig().setSearchFieldName("pathologicalCharacteristic");
        autoConfig.getFieldConfig().setSearchFieldDisplayName("Disease");
        CheckboxAutocompleteCompositeConfig config = new CheckboxAutocompleteCompositeConfig();
        config.setCheckboxConfig(checkboxConfig);
        config.setAutocompleteConfig(autoConfig);

        Specimen s = new Specimen();
        assertTrue(StringUtils.isBlank(autoConfig.getCriteriaString(s, null)));
        AdditionalPathologicFinding apf = new AdditionalPathologicFinding();
        apf.setId(1L);
        apf.setName("Cancer");
        apf.setValue("cancerCode");
        s.setPathologicalCharacteristic(apf);
        assertFalse(StringUtils.isBlank(config.getCriteriaString(s, null)));
        assertEquals("Disease: Cancer", config.getCriteriaString(s, null));
        config.getCheckboxConfig().setObjectProperty(true);
        config.getAutocompleteConfig().setObjectProperty(true);
        assertEquals("Disease: Cancer", config.getCriteriaString(getWrapper(s), null));
    }

    /**
     * test criteria string generation for fields that point to invalid properties.
     */
    @Test
    public void testInvalidConfiguration() {
        TextConfig config = new TextConfig();
        populateSimpleConfig(config, 1L);
        config.getFieldConfig().setSearchFieldName("invalid");
        config.getFieldConfig().setSearchFieldDisplayName("Invalid Field");
        assertTrue(StringUtils.isBlank(config.getCriteriaString(new Specimen(), null)));
    }

    private void populateSimpleConfig(SimpleSearchFieldConfig config, Long id) {
        config.setFieldConfig(new GenericFieldConfig());
        config.setObjectProperty(false);
        config.setErrorProperty("error property");
        config.setId(id);
    }

    private SpecimenWrapper getWrapper(Specimen s) {
        SpecimenWrapper wrapper = new SpecimenWrapper();
        wrapper.setObject(s);
        assertNotNull(wrapper.getObject());
        assertEquals(s, wrapper.getObject());
        return wrapper;
    }

    /**
     * Class that wraps a specimen.
     * @author ddasgupta
     */
    public class SpecimenWrapper implements RangeSearchParameterAccessor {

        private Specimen object;
        private Boolean booleanProperty;
        private Map<String, Object> rangeSearchParameters = new HashMap<String, Object>();
        private Collection<Object> params = new ArrayList<Object>();

        /**
         * @return the params
         */
        public Collection<Object> getParams() {
            return params;
        }

        /**
         * @param params the params to set
         */
        public void setParams(Collection<Object> params) {
            this.params = params;
        }

        /**
         * @return the object
         */
        public Specimen getObject() {
            return this.object;
        }

        /**
         * @param object the object to set
         */
        public void setObject(Specimen object) {
            this.object = object;
        }

        /**
         * @return the booleanProperty
         */
        public Boolean getBooleanProperty() {
            return this.booleanProperty;
        }

        /**
         * @param booleanProperty the booleanProperty to set
         */
        public void setBooleanProperty(Boolean booleanProperty) {
            this.booleanProperty = booleanProperty;
        }

        /**
         * Gets the map of range search parameters.
         *
         * @return a map of range search parameters.
         */
        public Map<String, Object> getRangeSearchParameters() {
            return this.rangeSearchParameters;
        }

        /**
         * Sets the map of range search parameters.
         *
         * @param rangeSearchParameters a map of range search parameters.
         */
        public void setRangeSearchParameters(Map<String, Object> rangeSearchParameters) {
            this.rangeSearchParameters = rangeSearchParameters;
        }

    }

}