/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.validation.SinglePropertyUniqueConstraintValidator;
import com.fiveamsolutions.tissuelocator.data.validation.UniqueConstraintValidator;
import com.fiveamsolutions.tissuelocator.data.validation.UniqueConstraintValidatorHelper;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;

/**
 * Most functionality of the validator is tested by persisting objects in the other tests, but some
 * error cases need to be tested separately.
 * @author smiller
 */
public class UniqueConstraintValidatorTest extends AbstractHibernateTestCase {

    /**
     * Tests validating a non persistent object.
     */
    @Test
    public void testNotAPersistentObject() {
        UniqueConstraintValidatorHelper validator = new UniqueConstraintValidatorHelper();
        assertFalse(validator.isValid(new String()));
    }

    /**
     * Tests validating a non supported field.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedOperation() {
        UniqueConstraintValidatorHelper validator = new UniqueConstraintValidatorHelper();
        validator.setPropertyNames(new String[] {"unsupported"});
        A a = new A();
        a.setUnsupported(new B());
        assertFalse(validator.isValid(a));
    }

    /**
     * Tests validating a non existent field.
     */
    @Test(expected = RuntimeException.class)
    public void testNonExistentField() {
        UniqueConstraintValidatorHelper validator = new UniqueConstraintValidatorHelper();
        validator.setPropertyNames(new String[] {"c"});
        assertFalse(validator.isValid(new A()));
    }

    /**
     * Tests validating an error field.
     */
    @Test(expected = RuntimeException.class)
    public void testErrorField() {
        UniqueConstraintValidatorHelper validator = new UniqueConstraintValidatorHelper();
        validator.setPropertyNames(new String[] {"errorField"});
        assertFalse(validator.isValid(new A()));
    }

    /**
     * Tests validating a non hidden field.
     */
    @Test(expected = RuntimeException.class)
    public void testHiddenField() {
        UniqueConstraintValidatorHelper validator = new UniqueConstraintValidatorHelper();
        validator.setPropertyNames(new String[] {"hiddenField"});
        assertFalse(validator.isValid(new A()));
    }

    /**
     * Tests validating a null field.
     */
    @Test
    public void testNullField() {
        UniqueConstraintValidatorHelper validator = new UniqueConstraintValidatorHelper();
        validator.setPropertyNames(new String[] {"id"});
        assertTrue(validator.isValid(new A()));
    }

    /**
     * Tests using the validator helper in a unique constraint validator.
     */
    @Test
    public void testUniqueConstraintValidator() {
        UniqueConstraintValidator validator = new UniqueConstraintValidator();
        assertFalse(validator.isValid(new String()));
    }

    /**
     * Tests using the validator helper in a single property unique constraint validator.
     */
    @Test
    public void testSinglePropertyUniqueConstraintValidator() {
        SinglePropertyUniqueConstraintValidator validator = new SinglePropertyUniqueConstraintValidator();
        assertFalse(validator.isValid(new String()));
    }

    /**
     * test class.
     * @author smiller
     */
    public class A implements PersistentObject {
        private static final long serialVersionUID = 1L;
        private Long id;
        private B unsupported;

        private String hiddenField;

        /**
         * @return the id
         */
        public Long getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(Long id) {
            this.id = id;
        }

        /**
         * @return the unsupported
         */
        public B getUnsupported() {
            return unsupported;
        }

        /**
         * @param unsupported the unsupported to set
         */
        public void setUnsupported(B unsupported) {
            this.unsupported = unsupported;
        }

        /**
         * @return an excpetion
         */
        public String getErrorField() {
            throw new UnsupportedOperationException();
        }

        /**
         * @param errorField the errorField to set
         */
        public void setErrorField(String errorField) {
            // do nothing
        }

        /**
         * @return the hiddenField
         */
        private String getHiddenField() {
            return hiddenField;
        }

        /**
         * @param hiddenField the hiddenField to set
         */
        private void setHiddenField(String hiddenField) {
            this.hiddenField = hiddenField;
        }

        /**
         * to avoid eclipse warnings.
         */
        public void useHiddenField() {
            getHiddenField();
            setHiddenField("test");
        }
    }

    /**
     * test class.
     * @author smiller
     */
    public class B implements Serializable {

        private static final long serialVersionUID = 1L;
        private String name;

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }
    }
}
