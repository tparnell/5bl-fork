/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.validator.InvalidStateException;
import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.Institution.InstitutionComparator;
import com.fiveamsolutions.tissuelocator.data.code.FundingSource;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceLocal;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.InstitutionSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.test.InstitutionPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author smiller
 *
 */
public class InstitutionPersitenceTest extends AbstractPersistenceTest<Institution> {

    private static final int INSTITUTION_COUNT = 4;

    /**
     * {@inheritDoc}
     */
    @Override
    public InstitutionServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getInstitutionService();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Institution[] getValidObjects() {
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();

        Institution[] objects = new Institution[2];
        objects[0] = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test institute1", false);
        objects[1] = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test institute2", true);
        return objects;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(Institution expected, Institution actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual, new String[] {"contact"}));
        if (expected.getContact() == null) {
            assertNull(actual.getContact());
        } else {
            assertTrue(EqualsBuilder.reflectionEquals(expected.getContact(), actual.getContact()));
        }
    }

    /**
     * Test the url constraint on the institution.
     */
    @Test
    public void testContactConstraint() {
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();

        GenericServiceLocal<Institution> service = getService();
        Institution institution = new Institution();
        institution.setName("test institute");
        institution.setHomepage("htt://www.google.com");
        institution.setType(type);
        InstitutionPersistenceHelper.getInstance().setAsConsortiumMember(institution);
        institution.setContact(new Contact());
        institution.getContact().setEmail("test@test.com");

        try {
            service.savePersistentObject(institution);
            fail("expected error");
        } catch (InvalidStateException e) {
            assertTrue(e.getInvalidValues()[0].getMessage().contains("must provide contact"));
        }
    }

    /**
     * Test the contact constraint on the institution.
     */
    @Test
    public void testUrlConstraint() {
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();

        GenericServiceLocal<Institution> service = getService();
        Institution institution = new Institution();
        institution.setName("test institute");
        institution.setHomepage("www.google.com");
        institution.setType(type);

        try {
            service.savePersistentObject(institution);
        } catch (InvalidStateException e) {
            assertEquals(e.getInvalidValues()[0].getPropertyName(), "homepage");
        }
    }

    /**
     * Test the contact constraint on the institution.
     */
    @Test
    public void testFailedAddressConstraint() {
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();

        GenericServiceLocal<Institution> service = getService();
        Institution institution = new Institution();
        institution.setName("test institute");
        institution.setType(type);
        InstitutionPersistenceHelper.getInstance().setAsConsortiumMember(institution);
        institution.setBillingAddress(null);

        try {
            service.savePersistentObject(institution);
        } catch (InvalidStateException e) {
            assertEquals(e.getInvalidValues()[0].getPropertyName(), "billingAddressValid");
        }
    }

    /**
     * test that institutions with duplicate names can not be saved.
     */
    @Test
    public void testUniqueConstraint() {
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution i1 = new Institution();
        i1.setName("test name");
        i1.setType(type);

        InstitutionServiceLocal service = getService();
        service.savePersistentObject(i1);

        Institution i2 = new Institution();
        i2.setName("test name");
        i2.setType(type);

        try {
            service.savePersistentObject(i2);
        } catch (InvalidStateException e) {
            assertTrue(e.getInvalidValues()[0].getMessage().contains("already exists"));
        }
    }

    /**
     * test that institutions with a DUNS/EIN Number can be saved.
     */
    @Test
    public void testDunsEinSave() {
        final String validNineDigitNumber = "123456789";

        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution i1 = new Institution();
        i1.setName("test institute");
        i1.setType(type);
        i1.setDunsEINType(DunsEin.DUNS);
        i1.setDunsEINNumber(validNineDigitNumber);
        InstitutionServiceLocal service = getService();
        Long institutionId = service.savePersistentObject(i1);
        Institution i2 = service.getInstitution(institutionId);
        assertNotNull(i2);
        assertEquals(i2.getDunsEINNumber(), validNineDigitNumber);
        assertEquals(i2.getDunsEINType(), DunsEin.DUNS);
    }

    /**
     * test that institutions with a DUNS/EIN Number too long can not be saved.
     */
    @Test(expected = InvalidStateException.class)
    public void testDunsEinTooLongConstraint() {
        final String tenDigitNumber = "0123456789";
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution i1 = new Institution();
        i1.setName("test institute");
        i1.setType(type);
        i1.setDunsEINType(DunsEin.DUNS);
        i1.setDunsEINNumber(tenDigitNumber);
        InstitutionServiceLocal service = getService();
        service.savePersistentObject(i1);
    }

    /**
     * test that institutions with invalid DUNS/EIN Numbers can not be saved.
     */
    @Test(expected = InvalidStateException.class)
    public void testDunsEinInvalidNumberConstraint() {
        final String nineCharacterString = "abcdefghi";
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution i1 = new Institution();
        i1.setName("test institute");
        i1.setType(type);
        i1.setDunsEINType(DunsEin.DUNS);
        i1.setDunsEINNumber(nineCharacterString);
        InstitutionServiceLocal service = getService();
        service.savePersistentObject(i1);
    }

    /**
     * test that institutions with DUNS/EIN Number without a type can not be saved.
     */
    @Test(expected = InvalidStateException.class)
    public void testDunsEinConditionalRequiredConstraint() {
        final String nineCharacterString = "123456789";
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution i1 = new Institution();
        i1.setName("test institute");
        i1.setType(type);
        i1.setDunsEINNumber(nineCharacterString);
        InstitutionServiceLocal service = getService();
        service.savePersistentObject(i1);
    }

    /**
     * test search.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        Institution example = new Institution();
        example.setName("test");
        InstitutionServiceLocal service = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        SearchCriteria<Institution> sc = new TissueLocatorAnnotatedBeanSearchCriteria<Institution>(example);
        PageSortParams<Institution> psp =
              new PageSortParams<Institution>(PAGE_SIZE, 0, InstitutionSortCriterion.NAME, true);
        List<Institution> results = service.search(sc, psp);
        assertEquals(2, results.size());
        assertTrue(results.get(0).getName().compareTo(results.get(1).getName()) > 0);

        example.setConsortiumMember(true);
        results = service.search(sc, psp);
        assertEquals(1, results.size());
        Institution cm = results.get(0);

        example.setConsortiumMember(false);
        results = service.search(sc, psp);
        assertEquals(1, results.size());
        assertNotSame(cm, results.get(0));
    }

    /**
     * test hash code and equals methods.
     */
    @Test
    public void testHashCodeAndEquals() {
        String name = "test";
        Institution inst = new Institution();
        inst.setName(name);
        assertFalse(inst.equals(null));
        assertFalse(inst.equals(new Participant()));
        assertTrue(inst.equals(inst));

        Institution inst2 = new Institution();
        assertFalse(inst.equals(inst2));
        assertFalse(inst2.equals(inst));
        assertNotSame(inst.hashCode(), inst2.hashCode());

        inst2.setName(name);
        assertTrue(inst.equals(inst2));
        assertTrue(inst2.equals(inst));
        assertEquals(inst.hashCode(), inst2.hashCode());
    }

    /**
     * test the getReviewers method.
     */
    @Test
    public void testGetReviewers() {
        InstitutionServiceLocal service = getService();
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Calendar cal = Calendar.getInstance();
        for (Institution inst : service.getAll(Institution.class)) {
            inst.setConsortiumMember(false);
            TissueLocatorHibernateUtil.getCurrentSession().update(inst);
        }

        Contact c = new Contact();
        c.setFirstName("fname");
        c.setLastName("lname");
        c.setEmail("fname@lname.com");
        c.setPhone("1234567890");
        c.setPhoneExtension("1234");

        Institution i1 = new Institution();
        i1.setName("test name 1");
        i1.setType(type);
        i1.setContact(c);
        i1.setConsortiumMember(false);
        i1.setLastReviewAssignment(null);
        service.savePersistentObject(i1);

        Institution i2 = new Institution();
        i2.setName("test name 2");
        i2.setType(type);
        i2.setContact(c);
        i2.setConsortiumMember(false);
        i2.setLastReviewAssignment(cal.getTime());
        service.savePersistentObject(i2);

        Institution i3 = new Institution();
        i3.setName("test name 3");
        i3.setType(type);
        i3.setContact(c);
        InstitutionPersistenceHelper.getInstance().setAsConsortiumMember(i3);
        i3.setLastReviewAssignment(null);
        service.savePersistentObject(i3);
        cal.add(Calendar.DATE, 1);

        Institution i4 = new Institution();
        i4.setName("test name 4");
        i4.setType(type);
        i4.setContact(c);
        InstitutionPersistenceHelper.getInstance().setAsConsortiumMember(i4);
        i4.setLastReviewAssignment(cal.getTime());
        service.savePersistentObject(i4);
        cal.add(Calendar.DATE, 1);

        Institution i5 = new Institution();
        i5.setName("test name 5");
        i5.setType(type);
        i5.setContact(c);
        InstitutionPersistenceHelper.getInstance().setAsConsortiumMember(i5);
        i5.setLastReviewAssignment(cal.getTime());
        service.savePersistentObject(i5);
        cal.add(Calendar.DATE, 1);

        Institution i6 = new Institution();
        i6.setName("test name 6");
        i6.setType(type);
        i6.setContact(c);
        InstitutionPersistenceHelper.getInstance().setAsConsortiumMember(i6);
        i6.setLastReviewAssignment(cal.getTime());
        service.savePersistentObject(i6);
        Institution[] consortium = {i3, i4, i5, i6};

        List<Institution> reviewers = service.getReviewers(1);
        assertNotNull(reviewers);
        assertFalse(reviewers.isEmpty());
        assertEquals(1, reviewers.size());
        for (int i = 0; i < reviewers.size(); i++) {
            assertTrue(reviewers.contains(consortium[i]));
            assertTrue(reviewers.get(i).equals(consortium[i]));
        }
        assertFalse(reviewers.contains(i1));
        assertFalse(reviewers.contains(i2));

        reviewers = service.getReviewers(2);
        assertNotNull(reviewers);
        assertFalse(reviewers.isEmpty());
        assertEquals(2, reviewers.size());
        for (int i = 0; i < reviewers.size(); i++) {
            assertTrue(reviewers.contains(consortium[i]));
            assertTrue(reviewers.get(i).equals(consortium[i]));
        }
        assertFalse(reviewers.contains(i1));
        assertFalse(reviewers.contains(i2));

        reviewers = service.getReviewers(INSTITUTION_COUNT * 2);
        assertNotNull(reviewers);
        assertFalse(reviewers.isEmpty());
        assertEquals(INSTITUTION_COUNT, reviewers.size());
        for (int i = 0; i < reviewers.size(); i++) {
            assertTrue(reviewers.contains(consortium[i]));
            assertTrue(reviewers.get(i).equals(consortium[i]));
        }
        assertFalse(reviewers.contains(i1));
        assertFalse(reviewers.contains(i2));
    }

    /**
     * test the comparator.
     */
    @Test
    public void testComparator() {

        InstitutionComparator comparator = new InstitutionComparator();

        Institution i1 = new Institution();
        i1.setName("albany medical college");
        Institution i2 = new Institution();
        i2.setName("arizona biomedical research");

        assertTrue(comparator.compare(i1, i2) < 0);
        assertTrue(comparator.compare(i2, i1) > 0);
        assertTrue(comparator.compare(i1, i1) == 0);

        i2.setName(null);
        assertTrue(comparator.compare(i1, i2) > 0);
        assertTrue(comparator.compare(i2, i1) < 0);
        assertTrue(comparator.compare(i2, i2) == 0);


    }

    /**
     * test the get scientific review vote counts method.
     */
    @Test
    public void testGetScientificReviewVoteCounts() {
        InstitutionServiceLocal service = getService();
        Date today = new Date();
        Date tomorrow = DateUtils.addDays(new Date(), 1);
        Date future = DateUtils.addDays(new Date(), 2);
        Map<String, Map<Vote, Long>> results = service.getScientificReviewVoteCounts(today, future);
        verifyEmptyResults(results, 1);

        testSaveRetrieve();
        GenericServiceBean<SpecimenRequest> requestService =
            EjbTestHelper.getGenericServiceBean(SpecimenRequest.class);
        Long id = requestService.savePersistentObject(createRequest());
        SpecimenRequest request = requestService.getPersistentObject(SpecimenRequest.class, id);
        for (Institution i : service.getAll(Institution.class)) {
            request.getConsortiumReviews().add(getVote(Vote.APPROVE, i));
            request.getConsortiumReviews().add(getVote(Vote.DENY, i));
            request.getConsortiumReviews().add(getVote(Vote.DENY, i));
        }
        requestService.savePersistentObject(request);

        results = service.getScientificReviewVoteCounts(today, future);
        verifyFullResults(results);
        results = service.getInstitutionalReviewVoteCounts(today, future);
        verifyEmptyResults(results, 2);
        results = service.getScientificReviewVoteCounts(today, today);
        verifyEmptyResults(results, 2);
        results = service.getScientificReviewVoteCounts(future, future);
        verifyEmptyResults(results, 2);
        results = service.getScientificReviewVoteCounts(today, tomorrow);
        verifyFullResults(results);
        results = service.getScientificReviewVoteCounts(tomorrow, future);
        verifyFullResults(results);
        clearVotes();
        results = service.getScientificReviewVoteCounts(today, future);
        verifyEmptyResults(results, 2);
    }

    /**
     * test the get institutional review vote counts method.
     */
    @Test
    public void testGetInstitutionalReviewVoteCounts() {
        InstitutionServiceLocal service = getService();
        Date today = new Date();
        Date tomorrow = DateUtils.addDays(new Date(), 1);
        Date future = DateUtils.addDays(new Date(), 2);
        Map<String, Map<Vote, Long>> results = service.getInstitutionalReviewVoteCounts(today, future);
        verifyEmptyResults(results, 1);

        testSaveRetrieve();
        GenericServiceBean<SpecimenRequest> requestService =
            EjbTestHelper.getGenericServiceBean(SpecimenRequest.class);
        Long id = requestService.savePersistentObject(createRequest());
        SpecimenRequest request = requestService.getPersistentObject(SpecimenRequest.class, id);
        for (Institution i : service.getAll(Institution.class)) {
            request.getInstitutionalReviews().add(getVote(Vote.APPROVE, i));
            request.getInstitutionalReviews().add(getVote(Vote.DENY, i));
            request.getInstitutionalReviews().add(getVote(Vote.DENY, i));
        }
        requestService.savePersistentObject(request);

        results = service.getInstitutionalReviewVoteCounts(today, future);
        verifyFullResults(results);
        results = service.getScientificReviewVoteCounts(today, future);
        verifyEmptyResults(results, 2);
        results = service.getInstitutionalReviewVoteCounts(today, today);
        verifyEmptyResults(results, 2);
        results = service.getInstitutionalReviewVoteCounts(future, future);
        verifyEmptyResults(results, 2);
        results = service.getInstitutionalReviewVoteCounts(today, tomorrow);
        verifyFullResults(results);
        results = service.getInstitutionalReviewVoteCounts(tomorrow, future);
        verifyFullResults(results);
        clearVotes();
        results = service.getInstitutionalReviewVoteCounts(today, future);
        verifyEmptyResults(results, 2);
    }

    /**
     * test the get scientific review rejection comments method.
     */
    @Test
    public void testGetScientificReviewRejectionComments() {
        InstitutionServiceLocal service = getService();
        Date today = new Date();
        Date tomorrow = DateUtils.addDays(new Date(), 1);
        Date future = DateUtils.addDays(new Date(), 2);

        testSaveRetrieve();
        List<Institution> institutions = service.getAll(Institution.class);
        for (Institution i : institutions) {
            assertTrue(service.getScientificReviewRejectionComments(today, tomorrow,
                    i.getName()).isEmpty());
        }
        GenericServiceBean<SpecimenRequest> requestService =
            EjbTestHelper.getGenericServiceBean(SpecimenRequest.class);
        Long id = requestService.savePersistentObject(createRequest());
        for (Institution i : institutions) {
            assertTrue(service.getScientificReviewRejectionComments(today, tomorrow,
                    i.getName()).isEmpty());
        }

        SpecimenRequest request = requestService.getPersistentObject(SpecimenRequest.class, id);
        for (Institution i : institutions) {
            request.getConsortiumReviews().add(getVote(Vote.APPROVE, i));
            request.getConsortiumReviews().add(getVote(Vote.DENY, i));
            request.getConsortiumReviews().add(getVote(Vote.DENY, i));
        }
        requestService.savePersistentObject(request);
        for (Institution i : institutions) {
            Map<Long, List<String>> results = service.getScientificReviewRejectionComments(today,
                    future, i.getName());
            assertEquals(1, results.size());
            assertTrue(results.containsKey(id));
            assertEquals(2, results.get(id).size());
            String c = "comment: " + i.getName() + ": " + Vote.DENY.name();
            assertEquals(c, results.get(id).get(0));
            assertEquals(c, results.get(id).get(1));
        }

        Long id2 = requestService.savePersistentObject(createRequest());
        request = requestService.getPersistentObject(SpecimenRequest.class, id2);
        for (Institution i : institutions) {
            request.getConsortiumReviews().add(getVote(Vote.APPROVE, i));
            request.getConsortiumReviews().add(getVote(Vote.APPROVE, i));
            request.getConsortiumReviews().add(getVote(Vote.DENY, i));
        }
        requestService.savePersistentObject(request);
        for (Institution i : institutions) {
            Map<Long, List<String>> results = service.getScientificReviewRejectionComments(today,
                    future, i.getName());
            assertEquals(2, results.size());
            assertTrue(results.containsKey(id));
            assertTrue(results.containsKey(id2));
            assertEquals(2, results.get(id).size());
            String c = "comment: " + i.getName() + ": " + Vote.DENY.name();
            assertEquals(c, results.get(id).get(0));
            assertEquals(c, results.get(id).get(1));
            assertEquals(1, results.get(id2).size());
            assertEquals(c, results.get(id2).get(0));
        }

        for (Institution i : institutions) {
            assertTrue(service.getScientificReviewRejectionComments(future, future,
                    i.getName()).isEmpty());
        }
    }

    private SpecimenRequest createRequest() {
        SpecimenRequestPersistenceTestHelper.createRequiredApplicationSettings();
        FundingSource fs = SpecimenRequestPersistenceTestHelper.createFundingSource();

        SpecimenRequest request = new SpecimenRequest();
        TissueLocatorUser u = getCurrentUser();

        Study study = new Study();
        study.setIrbApprovalStatus(IrbApprovalStatus.APPROVED);
        study.setIrbName("irb");
        study.setIrbRegistrationNumber("1234");
        study.setIrbApprovalNumber("1234");
        study.setIrbApprovalExpirationDate(DateUtils.addDays(new Date(), 2));
        study.setProtocolTitle("protocol");
        study.setStudyAbstract("abstract");
        study.setFundingStatus(FundingStatus.FUNDED);
        study.setFundingSources(new ArrayList<FundingSource>());
        study.getFundingSources().add(fs);
        EjbTestHelper.getGenericServiceBean(Study.class).savePersistentObject(study);

        PrincipalInvestigator pi = new PrincipalInvestigator();
        pi.setFirstName("pi");
        pi.setLastName("pi");
        pi.setOrganization(u.getInstitution());
        pi.setResume(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));
        pi.setEmail("pi@example.com");
        pi.setAddress(getAddress());
        pi.setCertified(YesNoResponse.YES);
        pi.setInvestigated(YesNoResponse.NO);
        EjbTestHelper.getGenericServiceBean(PrincipalInvestigator.class).savePersistentObject(pi);

        ShippingInformation shipment = new ShippingInformation();
        Person recipient = new Person();
        recipient.setFirstName("recipient");
        recipient.setLastName("recipient");
        recipient.setOrganization(u.getInstitution());
        recipient.setEmail(u.getEmail());
        recipient.setAddress(getAddress());
        EjbTestHelper.getGenericServiceBean(Person.class).savePersistentObject(recipient);
        shipment.setRecipient(recipient);
        EjbTestHelper.getGenericServiceBean(ShippingInformation.class).savePersistentObject(shipment);

        request.setRequestor(u);
        request.setStatus(RequestStatus.PENDING);
        assertNotNull(request.getStatus().getResourceKey());
        request.setUpdatedDate(new Date());
        request.setStudy(study);
        request.setInvestigator(pi);
        request.setShipment(shipment);
        return request;
    }

    private SpecimenRequestReviewVote getVote(Vote decision, Institution i) {
        SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
        vote.setDate(DateUtils.addDays(new Date(), 1));
        vote.setVote(decision);
        vote.setInstitution(i);
        vote.setComment("comment: " + i.getName() + ": " + decision.name());
        EjbTestHelper.getGenericServiceBean(SpecimenRequestReviewVote.class).savePersistentObject(vote);
        return vote;
    }

    private void verifyEmptyResults(Map<String, Map<Vote, Long>> results, int resultCount) {
        assertNotNull(results);
        assertEquals(resultCount, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        for (Map<Vote, Long> voteCounts : results.values()) {
            assertEquals(Vote.values().length, voteCounts.size());
            for (Long voteCount : voteCounts.values()) {
                assertEquals(0, voteCount.intValue());
            }
        }
    }

    private void verifyFullResults(Map<String, Map<Vote, Long>> results) {
        assertNotNull(results);
        assertFalse(results.isEmpty());
        assertEquals(getService().getAll(Institution.class).size(), results.size());
        for (Map<Vote, Long> voteCounts : results.values()) {
            assertEquals(Vote.values().length, voteCounts.size());
            assertEquals(1, voteCounts.get(Vote.APPROVE).intValue());
            assertEquals(2, voteCounts.get(Vote.DENY).intValue());
            assertEquals(0, voteCounts.get(Vote.REVISE).intValue());
        }
    }

    private void clearVotes() {
        GenericServiceBean<SpecimenRequestReviewVote> voteService =
            EjbTestHelper.getGenericServiceBean(SpecimenRequestReviewVote.class);
        for (SpecimenRequestReviewVote vote : voteService.getAll(SpecimenRequestReviewVote.class)) {
            vote.setVote(null);
            voteService.savePersistentObject(vote);
        }
    }
    /**
     * test the save new institution method.
     */
    @Test
    public void testSaveNewInstitution() {
        InstitutionServiceLocal instService = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        CollectionProtocolServiceLocal protocolService =
            TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        int baseCount = instService.getAll(Institution.class).size();
        Institution inst = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test 1", false);
        Long instId = instService.saveNewInstitution(inst);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertNotNull(instId);
        assertEquals(baseCount + 1, instService.getAll(Institution.class).size());
        assertEquals(0, protocolService.getAll(CollectionProtocol.class).size());

        inst = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test 2", true);
        instId = instService.saveNewInstitution(inst);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertNotNull(instId);
        assertEquals(baseCount + 2, instService.getAll(Institution.class).size());
        assertEquals(1, protocolService.getAll(CollectionProtocol.class).size());
    }

    /**
     * Test retrieval by name.
     */
    @Test
    public void testGetByName() {
        testSaveRetrieve();
        List<Institution> institutions = getService().getAll(Institution.class);
        for (Institution inst : institutions) {
            Institution retrieved = getService().getInstitutionByName(inst.getName());
            verifyObjectRetrieval(inst, retrieved);
        }
        assertNull(getService().getInstitutionByName("invalid"));
    }
}
