/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.config.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSection;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;

/**
 * User interface section service integration test.
 *
 * @author jstephens
 */
public class UiSectionServiceTest extends AbstractHibernateTestCase {

    private static final int NUMBER_OF_UI_SECTIONS = 3;
    private static final int NUMBER_OF_UI_DYNAMIC_FIELD_CATEGORIES = 3;
    private static final int NUMBER_OF_UI_SEARCH_FIELD_CATEGORIES = 3;

    /**
     * Set up the tests.
     */
    @Before
    public void setUpTests() {
        saveUiSections();
    }

    /**
     * Tests the persistence and retrieval of a single section.
     */
    @Test
    public void testUiSectionPersistence() {
        UiSection expectedUiSection = new UiSection();
        expectedUiSection.setSectionName(UiSection.class.getSimpleName());
        expectedUiSection.setEntityClassName(Class.class.getName());
        Long id = getService().savePersistentObject(expectedUiSection);
        UiSection returnedUiSection = getService().getPersistentObject(UiSection.class, id);
        assertTrue(EqualsBuilder.reflectionEquals(expectedUiSection, returnedUiSection));
    }

    /**
     * Test to ensure sections are returned in order.
     */
    @Test
    public void testGetUiSectionsReturnsuiCategoriesInOrder() {
        List<UiSection> expectedUiSections = createUiSections(NUMBER_OF_UI_SECTIONS);
        List<UiSection> returnedUiSections = getService().getUiSections(Specimen.class);
        assertEquals(expectedUiSections.get(1).getSectionName(), returnedUiSections.get(0).getSectionName());
        assertEquals(expectedUiSections.get(2).getSectionName(), returnedUiSections.get(1).getSectionName());
        assertEquals(expectedUiSections.get(0).getSectionName(), returnedUiSections.get(2).getSectionName());
    }

    /**
     * Test to ensure search field categories are returned in order.
     */
    @Test
    public void testGetUiSectionsReturnsUiSearchFieldCategoriesInOrder() {
        List<UiSearchFieldCategory> expectedUiCategories =
            createUiSearchFieldCategories(NUMBER_OF_UI_SEARCH_FIELD_CATEGORIES);
        List<UiSection> uiSections = getService().getUiSections(Specimen.class);
        List<UiSearchFieldCategory> returnedUiCategories = uiSections.get(0).getUiSearchFieldCategories();
        assertEquals(expectedUiCategories.get(1).getCategoryName(), returnedUiCategories.get(0).getCategoryName());
        assertEquals(expectedUiCategories.get(2).getCategoryName(), returnedUiCategories.get(1).getCategoryName());
        assertEquals(expectedUiCategories.get(0).getCategoryName(), returnedUiCategories.get(2).getCategoryName());
    }

    /**
     * Test to ensure dynamic field categories are returned in order.
     */
    @Test
    public void testGetUiSectionsReturnsUiDynamicFieldCategoriesInOrder() {
        List<UiDynamicFieldCategory> expectedUiCategories =
            createUiDynamicFieldCategories(NUMBER_OF_UI_DYNAMIC_FIELD_CATEGORIES);
        List<UiSection> uiSections = getService().getUiSections(Specimen.class);
        List<UiDynamicFieldCategory> returnedUiCategories = uiSections.get(2).getUiDynamicFieldCategories();
        assertEquals(expectedUiCategories.get(1).getCategoryName(), returnedUiCategories.get(0).getCategoryName());
        assertEquals(expectedUiCategories.get(2).getCategoryName(), returnedUiCategories.get(1).getCategoryName());
        assertEquals(expectedUiCategories.get(0).getCategoryName(), returnedUiCategories.get(2).getCategoryName());
    }

    private void saveUiSections() {
        List<UiSection> uiCategories = createUiSections(NUMBER_OF_UI_SECTIONS);
        List<UiDynamicFieldCategory> dfc = createUiDynamicFieldCategories(NUMBER_OF_UI_DYNAMIC_FIELD_CATEGORIES);
        uiCategories.get(0).getUiDynamicFieldCategories().add(dfc.get(1));
        uiCategories.get(0).getUiDynamicFieldCategories().add(dfc.get(2));
        uiCategories.get(0).getUiDynamicFieldCategories().add(dfc.get(0));
        List<UiSearchFieldCategory> sfc = createUiSearchFieldCategories(NUMBER_OF_UI_SEARCH_FIELD_CATEGORIES);
        uiCategories.get(1).getUiSearchFieldCategories().add(sfc.get(1));
        uiCategories.get(1).getUiSearchFieldCategories().add(sfc.get(2));
        uiCategories.get(1).getUiSearchFieldCategories().add(sfc.get(0));
        getService().savePersistentObject(uiCategories.get(1));
        getService().savePersistentObject(uiCategories.get(2));
        getService().savePersistentObject(uiCategories.get(0));
    }

    private UiSectionServiceLocal getService() {
        return new UiSectionServiceBean();
    }

    private List<UiSection> createUiSections(int numberOfSections) {
        List<UiSection> sections = new ArrayList<UiSection>();
        for (int count = 1; count <= numberOfSections; count++) {
            UiSection section = new UiSection();
            section.setSectionName(String.format("Section %d", count));
            section.setEntityClassName(Specimen.class.getName());
            sections.add(section);
        }

        return sections;
    }

    private List<UiDynamicFieldCategory> createUiDynamicFieldCategories(int numberOfCategories) {
        List<UiDynamicFieldCategory> categories = new ArrayList<UiDynamicFieldCategory>();
        for (int count = 1; count <= numberOfCategories; count++) {
            UiDynamicFieldCategory category = new UiDynamicFieldCategory();
            category.setCategoryName(String.format("Dynamic Field Category %d", count));
            category.setViewable(Boolean.FALSE);
            category.setEntityClassName(Class.class.getName());
            categories.add(category);
        }

        return categories;
    }

    private List<UiSearchFieldCategory> createUiSearchFieldCategories(int numberOfCategories) {
        List<UiSearchFieldCategory> categories = new ArrayList<UiSearchFieldCategory>();
        for (int count = 1; count <= numberOfCategories; count++) {
            UiSearchFieldCategory category = new UiSearchFieldCategory();
            category.setCategoryName(String.format("Search Field Category %d", count));
            category.setViewable(Boolean.TRUE);
            category.setEntityClassName(Class.class.getName());
            category.setSearchSetType(SearchSetType.ADVANCED.name());
            categories.add(category);
        }

        return categories;
    }

}