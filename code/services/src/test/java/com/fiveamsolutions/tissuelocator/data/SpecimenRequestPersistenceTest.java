//CHECKSTYLE:OFF file length
/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//CHECKSTYLE:ON
package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.validator.InvalidStateException;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.nci.commons.data.security.UserGroup;
import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.validation.FundingStatusCheckValidator;
import com.fiveamsolutions.tissuelocator.data.validation.StudyIrbApprovalRequiredValidator;
import com.fiveamsolutions.tissuelocator.service.FullConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.LineItemReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestProcessingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceBean;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.reminder.ScheduledReminderServiceBean;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceBean;
import com.fiveamsolutions.tissuelocator.test.AbstractSpecimenRequestTest;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.util.RequestProcessingConfiguration;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * This class includes tests of general specimen request persistence, the specimen request service,
 * and the full consortium review request processor.
 * @author smiller
 *
 */

public class SpecimenRequestPersistenceTest extends AbstractSpecimenRequestTest {

    private static final int NUM_INSTITUTIONAL_REVIEWERS = 3;
    private static final int GRACE_PERIOD = 30;

    /**
     * test for comments.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void addComment() throws MessagingException, IOException {
        FullConsortiumReviewServiceLocal service = getReviewService();
        TissueLocatorUser user1 = getCurrentUser();
        TissueLocatorUser user2 = createUserInRole(Role.TISSUE_REQUEST_ADMIN, user1.getInstitution());
        TissueLocatorUser user3 = createUserSkipRole();
        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        UsernameHolder.setUser(user2.getUsername());
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        request.getReviewers().clear();
        user1 = EjbTestHelper.getTissueLocatorUserServiceBean().getByUsername(user1.getUsername());
        request.getReviewers().add(user1.getInstitution());
        request.getConsortiumReviews().clear();
        request.getConsortiumReviews().add(SpecimenRequestPersistenceTestHelper.getVote(user1));
        request.getConsortiumReviews().add(SpecimenRequestPersistenceTestHelper.getVote(user2));
        SpecimenRequestReviewVote vote = SpecimenRequestPersistenceTestHelper.getVote(user3);
        vote.setUser(null);
        request.getConsortiumReviews().add(vote);
        TissueLocatorHibernateUtil.getCurrentSession().save(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        assertEquals(0, request.getComments().size());
        assertEquals(THREE, request.getConsortiumReviews().size());
        assertEquals(1, request.getReviewers().size());

        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        //comment by lead reviewer
        request.getComments().add(SpecimenRequestPersistenceTestHelper.getReviewComment(user1, new Date()));
        Date oldUpdatedDate = request.getUpdatedDate();
        request.setUpdatedDate(new Date());
        UsernameHolder.setUser(user1.getUsername());
        service.addComment(request, 2);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(1, request.getComments().size());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertTrue(Mailbox.get(user1.getEmail()).isEmpty());
        assertFalse(Mailbox.get(user2.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(user2.getEmail()).size());
        testEmail(user2.getEmail(), "awaiting your action", "entered a review recommendation");
        assertTrue(Mailbox.get(user3.getEmail()).isEmpty());
        Mailbox.clearAll();

        //comment by consortium voter that is not lead reviewer
        request.getComments().add(SpecimenRequestPersistenceTestHelper.getReviewComment(user2, new Date()));
        oldUpdatedDate = request.getUpdatedDate();
        request.setUpdatedDate(new Date());
        UsernameHolder.setUser(request.getRequestor().getUsername());
        service.addComment(request, 2);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(2, request.getComments().size());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertTrue(Mailbox.get(user1.getEmail()).isEmpty());
        assertTrue(Mailbox.get(user3.getEmail()).isEmpty());
        Mailbox.clearAll();

        //comment by non consortium voter
        request.getComments().add(SpecimenRequestPersistenceTestHelper.getReviewComment(user3, new Date()));
        oldUpdatedDate = request.getUpdatedDate();
        request.setUpdatedDate(new Date());
        UsernameHolder.setUser(request.getRequestor().getUsername());
        service.addComment(request, 2);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(THREE, request.getComments().size());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertTrue(Mailbox.get(user1.getEmail()).isEmpty());
        assertTrue(Mailbox.get(user3.getEmail()).isEmpty());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * test the is valid method.
     */
    @Test
    public void testIsValid() {
        SpecimenRequestLineItem li = new SpecimenRequestLineItem();
        assertTrue(li.isValidQuantity());
        li.setQuantity(new BigDecimal(1L));
        assertTrue(li.isValidQuantity());
        li.setSpecimen(new Specimen());
        assertTrue(li.isValidQuantity());
        li.getSpecimen().setAvailableQuantity(new BigDecimal(0L));
        assertTrue(!li.isValidQuantity());
        li.getSpecimen().setAvailableQuantity(new BigDecimal(1L));
        assertTrue(li.isValidQuantity());
        li.setQuantity(new BigDecimal(2L));
        assertFalse(li.isValidQuantity());
    }

    private void verifyVotes(Set<SpecimenRequestReviewVote> votes, Vote vote) {
        for (SpecimenRequestReviewVote srrv : votes) {
            assertEquals(vote, srrv.getVote());
        }
    }

    /**
     * test the processPendingRequests method.
     *
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testProcessPendingRequestsFullApproval() throws MessagingException, IOException {
        testProcessPendingRequests(false);
    }

    /**
     * test the processPendingRequests method.
     *
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testProcessPendingRequestsPartialApproval() throws MessagingException, IOException {
        testProcessPendingRequests(true);
    }

    /**
     * Test institutional review voting for clients with no scientific review process.
     * @throws MessagingException on error.
     * @throws IOException on error.
     */
    @Test
    public void testAddInstitutionalReview() throws MessagingException, IOException {
        SpecimenRequestPersistenceTestHelper.createProspectiveCollectionApplicationSetting(true);
        RequestProcessingConfiguration config = new RequestProcessingConfiguration();
        config.setInstitutionalAdminEmail(getEmail("instAdmin"));
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.LINE_ITEM_REVIEW);

        // Test the voting functionality for different combinations of votes
        verifyAddLineItemReview(new Vote[]{Vote.APPROVE, Vote.APPROVE, Vote.APPROVE},
                new RequestStatus[]{RequestStatus.PENDING, RequestStatus.PENDING, RequestStatus.APPROVED});
        verifyAddLineItemReview(new Vote[]{Vote.APPROVE, Vote.DENY, Vote.APPROVE},
                new RequestStatus[]{RequestStatus.PENDING, RequestStatus.PENDING,
                RequestStatus.PARTIALLY_APPROVED});
        verifyAddLineItemReview(new Vote[]{Vote.DENY, Vote.DENY, Vote.APPROVE},
                new RequestStatus[]{RequestStatus.PENDING, RequestStatus.PENDING,
                RequestStatus.PARTIALLY_APPROVED});
        verifyAddLineItemReview(new Vote[]{Vote.DENY, Vote.DENY, Vote.DENY},
                new RequestStatus[]{RequestStatus.PENDING, RequestStatus.PENDING,
                RequestStatus.DENIED});
    }

    /**
     * Test request status updates for the institutional review process.
     * @throws MessagingException on error.
     * @throws IOException on error.
     */
    @Test
    public void testStatusForInstitutionalReview() throws MessagingException, IOException {
        SpecimenRequestPersistenceTestHelper
                .createReviewProcessSetting(ReviewProcess.LINE_ITEM_REVIEW);

        updateRequestStatusWithShipment(new Vote[]{Vote.APPROVE},
                new RequestStatus[]{RequestStatus.PENDING});
        updateRequestStatusWithShipment(new Vote[]{Vote.APPROVE, Vote.DENY},
                new RequestStatus[]{RequestStatus.PENDING});
        updateRequestStatusWithShipment(new Vote[]{Vote.APPROVE, Vote.APPROVE},
                new RequestStatus[]{RequestStatus.PENDING, RequestStatus.PENDING});
        updateRequestStatusWithShipment(new Vote[]{Vote.APPROVE, Vote.APPROVE, Vote.DENY},
                new RequestStatus[]{RequestStatus.PARTIALLY_PROCESSED, RequestStatus.FULLY_PROCESSED});
        updateRequestStatusWithShipment(new Vote[]{Vote.APPROVE, Vote.APPROVE, Vote.APPROVE},
                new RequestStatus[]{RequestStatus.PARTIALLY_PROCESSED, RequestStatus.PARTIALLY_PROCESSED,
                RequestStatus.FULLY_PROCESSED});
    }

    private void updateRequestStatusWithShipment(Vote[] votes,
            RequestStatus[] expectedStatus) throws MessagingException, IOException {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        List<TissueLocatorUser> users = new ArrayList<TissueLocatorUser>();
        List<TissueLocatorUser> lineItemReviewers = new ArrayList<TissueLocatorUser>();
        SpecimenRequest request = prepareRequestForLineItemReview(users, lineItemReviewers,
                new ArrayList<TissueLocatorUser>());
        assertEquals(RequestStatus.PENDING, request.getStatus());
        for (int i = 0; i < votes.length; i++) {
            castLineItemReviewVote(request, lineItemReviewers.get(i), votes[i]);
            request = getService().getPersistentObject(SpecimenRequest.class,
                    request.getId());
        }
        verifyRequestStatusWithShipment(request, expectedStatus);
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    private void verifyRequestStatusWithShipment(SpecimenRequest request, RequestStatus[] expectedStatus)
        throws MessagingException {
        ShipmentServiceLocal service = TissueLocatorRegistry.getServiceLocator().getShipmentService();
        UsernameHolder.setUser(request.getRequestor().getUsername());
        List<Shipment> orders = new ArrayList<Shipment>();
        orders.addAll(request.getOrders());
        for (int i = 0; i < expectedStatus.length; i++) {
            RequestStatus currentStatus = request.getStatus();
            Shipment order = orders.get(i);
            order.setStatus(ShipmentStatus.PENDING);
            service.saveOrder(order);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            request = getService().getPersistentObject(SpecimenRequest.class, request.getId());
            order = getService().getPersistentObject(Shipment.class, order.getId());
            assertEquals(currentStatus, request.getStatus());
            order.setShippingMethod(ShippingMethod.DHL);
            order.setTrackingNumber("tracking number");
            service.markOrderAsShipped(order);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            request = getService().getPersistentObject(SpecimenRequest.class, request.getId());
            assertEquals(expectedStatus[i], request.getStatus());
        }
        Shipment order = orders.get(0);
        order = getService().getPersistentObject(Shipment.class, order.getId());
        service.cancelOrder(order);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        request = getService().getPersistentObject(SpecimenRequest.class, request.getId());
        assertEquals(expectedStatus[expectedStatus.length - 1], request.getStatus());
    }

    private void verifyAddLineItemReview(Vote[] votes, RequestStatus[] expectedStatus)
        throws MessagingException, IOException {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        List<TissueLocatorUser> users = new ArrayList<TissueLocatorUser>();
        List<TissueLocatorUser> reviewers = new ArrayList<TissueLocatorUser>();
        List<TissueLocatorUser> tissueTechs = new ArrayList<TissueLocatorUser>();
        SpecimenRequest request = prepareRequestForLineItemReview(users, reviewers, tissueTechs);

        assertEquals(reviewers.size(), request.getAggregateLineItems().size());
        for (TissueLocatorUser reviewer : reviewers) {
            assertFalse(Mailbox.get(reviewer.getEmail()).isEmpty());
        }
        int numOrders = 0;
        Mailbox.clearAll();

        for (int i = 0; i < reviewers.size(); i++) {
            confirmLineItemReviewVote(request, reviewers.get(i), tissueTechs.get(i),
                    votes[i], expectedStatus[i], (votes[i].equals(Vote.DENY) ? numOrders : ++numOrders));
            request = getService().getPersistentObject(SpecimenRequest.class, request.getId());
        }
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    private SpecimenRequest prepareRequestForLineItemReview(List<TissueLocatorUser> users,
            List<TissueLocatorUser> reviewers, List<TissueLocatorUser> tissueTechs)
            throws MessagingException, IOException {
        for (int i = 0; i < NUM_INSTITUTIONAL_REVIEWERS; i++) {
            users.add(createUserSkipRole());
        }
        SpecimenRequest request = createRequest(true, SpecimenStatus.AVAILABLE);
        for (int i = 0; i < NUM_INSTITUTIONAL_REVIEWERS; i++) {
            reviewers.add(createLineItemReviewer(users.get(i).getInstitution()));
        }
        request.getLineItems().clear();
        request.getAggregateLineItems().clear();
        for (TissueLocatorUser reviewer : reviewers) {
            addAggregateLineItemForLineItemReview(request, reviewer.getInstitution());
        }

        UsernameHolder.setUser(request.getRequestor().getUsername());
        getService().saveRequest(request, 0, 0, 0, 0);
        TissueLocatorHibernateUtil.getCurrentSession().flush();

        for (int i = 0; i < NUM_INSTITUTIONAL_REVIEWERS; i++) {
            tissueTechs.add(createTissueTechnician(reviewers.get(i).getInstitution()));
        }

        for (AggregateSpecimenRequestLineItem li : request.getAggregateLineItems()) {
            assertNotNull(li.getVote());
            assertNotNull(li.getVote().getDate());
            assertNotNull(li.getVote().getInstitution());
        }
        for (int i = 0; i < NUM_INSTITUTIONAL_REVIEWERS; i++) {
            assertEquals(1, Mailbox.get(reviewers.get(i).getEmail()).size());
        }

        return request;
    }

    private void confirmLineItemReviewVote(SpecimenRequest request, TissueLocatorUser reviewer,
            TissueLocatorUser tissueTech, Vote vote, RequestStatus expectedStatus, int expectedOrders)
            throws MessagingException {
        LineItemReviewServiceLocal service = getLineItemReviewService();
        int expectedOrderAggregateLineItems = request.getOrderAggregateLineItems().size();
        if (vote.equals(Vote.APPROVE)) {
            for (AggregateSpecimenRequestLineItem li : request.getAggregateLineItems()) {
                if (li.getInstitution().equals(reviewer.getInstitution())) {
                    expectedOrderAggregateLineItems++;
                }
            }
        }

        castLineItemReviewVote(request, reviewer, vote);
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        assertEquals(vote.equals(Vote.DENY) ? 0 : 1, Mailbox.get(tissueTech.getEmail()).size());

        if (vote.equals(Vote.APPROVE)) {
            int aggregateCount = 0;
            for (AggregateSpecimenRequestLineItem lineItem : request.getAggregateLineItems()) {
                if (lineItem.getInstitution().equals(reviewer.getInstitution())) {
                    aggregateCount++;
                }
            }
            assertEquals(0, aggregateCount);
        }
        assertEquals(Mailbox.get(request.getRequestor().getEmail()).size(), 1);
        assertEquals(expectedStatus, request.getStatus());
        assertEquals(expectedOrders, request.getOrders().size());
        assertEquals(expectedOrderAggregateLineItems, request.getOrderAggregateLineItems().size());
        Mailbox.clearAll();
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
    }

    /**
     * Test the getUnreviewedRequests method.
     * @throws AddressException on error.
     */
    @Test
    public void testGetUnreviewedRequests() throws AddressException {

        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        List<TissueLocatorUser> users = new ArrayList<TissueLocatorUser>();
        for (int i = 0; i < NUM_INSTITUTIONAL_REVIEWERS; i++) {
            users.add(createUserSkipRole());
        }
        List<TissueLocatorUser> lineItemReviewers = new ArrayList<TissueLocatorUser>();
        for (int i = 0; i < NUM_INSTITUTIONAL_REVIEWERS; i++) {
            lineItemReviewers.add(createLineItemReviewer(users.get(i).getInstitution()));
        }

        List<SpecimenRequest> requests = new ArrayList<SpecimenRequest>();
        for (int i = 0; i < lineItemReviewers.size(); i++) {
            SpecimenRequest request = createRequest(true, SpecimenStatus.AVAILABLE);
            request.getAggregateLineItems().clear();
            requests.add(request);
            for (int j = 0; j < lineItemReviewers.size(); j++) {
                AggregateSpecimenRequestLineItem li = new AggregateSpecimenRequestLineItem();
                li.setCriteria("criteria");
                li.setQuantity(i + j);
                li.setInstitution(lineItemReviewers.get(j).getInstitution());
                li.setVote(SpecimenRequestPersistenceTestHelper.getVote(lineItemReviewers.get(j)));
                request.getAggregateLineItems().add(li);
            }
            UsernameHolder.setUser(requests.get(i).getRequestor().getUsername());
            service.savePersistentObject(requests.get(i));
            TissueLocatorHibernateUtil.getCurrentSession().flush();
        }

        // confirm all requests fall initially within the grace period
        assertEquals(0, service.getUnreviewedRequests(GRACE_PERIOD).size());

        // confirm requests will be returned after the grace period
        for (int i = 0; i < requests.size(); i++) {
            requests.get(i).setRequestedDate(DateUtils.addDays(requests.get(i).getRequestedDate(),
                    (GRACE_PERIOD + 1) * -1));
            service.savePersistentObject(requests.get(i));
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            assertEquals(i + 1, service.getUnreviewedRequests(GRACE_PERIOD).size());
        }

        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        // confirm emails are sent
        ScheduledReminderServiceBean reminderService = new ScheduledReminderServiceBean();
        reminderService.setRequestService(new SpecimenRequestServiceBean());
        reminderService.setUserService(new TissueLocatorUserServiceBean());
        reminderService.sendReminderEmails(0, GRACE_PERIOD, 0, 0);
        for (TissueLocatorUser reviewer : lineItemReviewers) {
            assertEquals(requests.size(), Mailbox.get(reviewer.getEmail()).size());
        }

        // confirm emails will not be resent
        assertEquals(0, service.getUnreviewedRequests(GRACE_PERIOD).size());

        // reset to reminder conditions
        for (SpecimenRequest request : requests) {
            request.setNextReminderDate(null);
            request.setRequestedDate(DateUtils.addDays(request.getRequestedDate(),
                    (GRACE_PERIOD + 1) * -1));
            service.savePersistentObject(request);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
        }

        // confirm voting prevents reminders
        for (int i = 0; i < requests.size(); i++) {
            List<AggregateSpecimenRequestLineItem> lineItems =
                new ArrayList<AggregateSpecimenRequestLineItem>(requests.get(i).getAggregateLineItems());
            for (int j = 0; j < NUM_INSTITUTIONAL_REVIEWERS; j++) {
                assertEquals(requests.size() - i, service.getUnreviewedRequests(GRACE_PERIOD).size());
                lineItems.get(j).getVote().setVote(Vote.APPROVE);
                service.savePersistentObject(requests.get(i));
                TissueLocatorHibernateUtil.getCurrentSession().flush();
            }
        }
        assertEquals(0, service.getUnreviewedRequests(GRACE_PERIOD).size());
    }

    // CHECKSTYLE:OFF � method too long
    private void testProcessPendingRequests(boolean partialApproval) throws MessagingException, IOException {
    // CHECKSTYLE:ON
        SpecimenRequestPersistenceTestHelper.createProspectiveCollectionApplicationSetting(false);
        RequestProcessingConfiguration config = new RequestProcessingConfiguration();
        config.setReviewPeriod(REVIEW_PERIOD);
        config.setVotingPeriod(VOTING_PERIOD);
        config.setMinPercentAvailableVotes(MIN_AVAILABLE);
        config.setMinPercentWinningVotes(MIN_WINNING);
        config.setReviewLateEmail(getEmail("reviewLate"));
        config.setVoteLateEmail(getEmail("vote"));
        config.setDeadlockEmail(getEmail("deadlock"));
        config.setDeadlockInstAdminEmail(getEmail("deadlockInstAdmin"));
        config.setInstitutionalAdminEmail(getEmail("instAdmin"));
        config.setVoteFinalizedEmail(getEmail("voteFinalized"));
        config.setVoteFinalizedAssignmentEmail(getEmail("voteFinalizedAssignment"));
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);

        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        // create consortium reviewer role
        ApplicationRole consortiumReviewRole = createConsortiumReviewRole();

        // map of institutions to a user in that institution
        HashMap<Institution, TissueLocatorUser> instToConsortiumReviewerMap =
            new HashMap<Institution, TissueLocatorUser>();

        // create 3 member review committee, one assigned role directly, two in group assigned to role
        int numConsortiumInstitutions = 1;
        UserGroup ug = createConsortiumReviewGroup(consortiumReviewRole);

        TissueLocatorUser reviewer4 = createReviewer(instToConsortiumReviewerMap, ug);
        numConsortiumInstitutions++;

        TissueLocatorUser reviewer1 = createReviewer(instToConsortiumReviewerMap, ug);
        numConsortiumInstitutions++;

        TissueLocatorUser reviewer2 = createReviewer(instToConsortiumReviewerMap, ug);
        numConsortiumInstitutions++;

        TissueLocatorUser reviewer3 = createUserSkipRole();
        numConsortiumInstitutions++;
        instToConsortiumReviewerMap.put(reviewer3.getInstitution(), reviewer3);
        reviewer3.getRoles().add(consortiumReviewRole);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(reviewer3);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorUser[] reviewers = {reviewer1, reviewer2, reviewer3, reviewer4};

        // map of institutions to a user in that institution
        HashMap<Institution, TissueLocatorUser> instToInstReviewerMap =
            new HashMap<Institution, TissueLocatorUser>();

        // create an institutional reviewer for existing line items.
        SpecimenRequest request = createRequest(true, SpecimenStatus.AVAILABLE);
        Specimen specimen = request.getLineItems().iterator().next().getSpecimen();
        TissueLocatorUser instReviewer1 = createInstitutionalReviewer(specimen.getExternalIdAssigner());
        instToInstReviewerMap.put(instReviewer1.getInstitution(), instReviewer1);

        // create a new line item and associated institutional reviewer
        Specimen s1 = SpecimenRequestPersistenceTestHelper.createNewSpecimen(request, reviewer1, specimen);
        UsernameHolder.setUser(request.getRequestor().getUsername());
        TissueLocatorRegistry.getServiceLocator().getSpecimenService().savePersistentObject(s1);

        TissueLocatorUser instReviewer2 = createInstitutionalReviewer(s1.getExternalIdAssigner());
        instToInstReviewerMap.put(instReviewer2.getInstitution(), instReviewer2);

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorUser assigner1 = createReviewerAssigner(reviewers[0].getInstitution());
        TissueLocatorUser assigner2 = createReviewerAssigner(reviewers[1].getInstitution());
        TissueLocatorUser assigner3 = createReviewerAssigner(reviewers[2].getInstitution());
        TissueLocatorUser assigner4 = createReviewerAssigner(reviewers[THREE].getInstitution());
        TissueLocatorUser[] assigners = {assigner1, assigner2, assigner3, assigner4};

        TissueLocatorUser tissueTech1 = createTissueTechnician(reviewers[0].getInstitution());
        TissueLocatorUser tissueTech2 = createTissueTechnician(reviewers[1].getInstitution());
        TissueLocatorUser tissueTech3 = createTissueTechnician(reviewers[2].getInstitution());
        TissueLocatorUser tissueTech4 = createTissueTechnician(reviewers[THREE].getInstitution());
        TissueLocatorUser[] tissueTechs = {tissueTech1, tissueTech2, tissueTech3, tissueTech4};

        // save off all users so we are ready for the rest of the work
        TissueLocatorHibernateUtil.getCurrentSession().flush();

        UsernameHolder.setUser(request.getRequestor().getUsername());
        // save the specimen and verify votes are set up
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        SpecimenRequestProcessingServiceLocal processingService =
            TissueLocatorRegistry.getServiceLocator().getSpecimenRequestProcessingService();
        setSpecimensAvailable(request.getLineItems());
        Long id = service.saveRequest(request, 2, 1, 1, 2);
        Mailbox.clearAll();
        request = service.getPersistentObject(SpecimenRequest.class, id);
        verifyVoteCounts(numConsortiumInstitutions, request);
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            assertEquals(lineItem.getSpecimen().getStatus(), SpecimenStatus.UNDER_REVIEW);
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();

        // set up the reviewers
        request.getReviewers().clear();
        assertTrue(request.getReviewers().isEmpty());
        request.getReviewers().add(reviewer1.getInstitution());
        request.getReviewers().add(reviewer2.getInstitution());
        request.getComments().clear();

        Map<Institution, TissueLocatorUser> reviewerMap = new HashMap<Institution, TissueLocatorUser>();
        for (TissueLocatorUser reviewer : reviewers) {
            reviewerMap.put(reviewer.getInstitution(), reviewer);
        }
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            vote.setUser(reviewerMap.get(vote.getInstitution()));
        }

        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);

        //verify the scientific review part of the voting process
        verifyReview(request, reviewers, config, assigners);

        // voting period not done yet - no change to status or updated date, and no emails sent
        Date oldUpdatedDate = request.getUpdatedDate();
        RequestStatus oldStatus = request.getStatus();
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        verifyVoteCounts(numConsortiumInstitutions, request);
        verifyVotes(request.getConsortiumReviews(), null);
        verifyVotes(request.getInstitutionalReviews(), null);
        assertEquals(oldStatus, request.getStatus());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[1].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(instReviewer1.getEmail()).isEmpty());
        assertTrue(Mailbox.get(instReviewer2.getEmail()).isEmpty());
        Mailbox.clearAll();

        // voting period done, but no votes yet - no change to status or updated date, and reviewers get emails
        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, -VOTING_PERIOD - 1);
        request.setUpdatedDate(cal.getTime());
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        oldUpdatedDate = request.getUpdatedDate();
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        verifyVoteCounts(numConsortiumInstitutions, request);
        verifyVotes(request.getConsortiumReviews(), null);
        verifyVotes(request.getInstitutionalReviews(), null);
        assertEquals(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(oldStatus, request.getStatus());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertEquals(0, Mailbox.get(request.getRequestor().getEmail()).size());
        assertEquals(1, Mailbox.get(reviewers[0].getEmail()).size());
        testEmail(reviewers[0].getEmail(), "vote", "vote today");
        assertEquals(1, Mailbox.get(reviewers[1].getEmail()).size());
        testEmail(reviewers[1].getEmail(), "vote", "vote today");
        assertEquals(1, Mailbox.get(reviewers[2].getEmail()).size());
        testEmail(reviewers[2].getEmail(), "vote", "vote today");
        assertEquals(1, Mailbox.get(instReviewer1.getEmail()).size());
        testEmail(instReviewer1.getEmail(), "vote", "vote today");
        assertEquals(1, Mailbox.get(instReviewer2.getEmail()).size());
        testEmail(instReviewer2.getEmail(), "vote", "vote today");
        assertFalse(Mailbox.get(assigners[0].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assigners[0].getEmail()).size());
        testEmail(assigners[0].getEmail(), "awaiting immediate action", "is late and has received the following email");
        assertFalse(Mailbox.get(assigners[1].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assigners[1].getEmail()).size());
        testEmail(assigners[1].getEmail(), "awaiting immediate action", "is late and has received the following email");
        assertFalse(Mailbox.get(assigners[2].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assigners[2].getEmail()).size());
        testEmail(assigners[2].getEmail(), "awaiting immediate action", "is late and has received the following email");
        Mailbox.clearAll();

        // voting period done, reviewers[0] voted, still not enough
        // no change to status or updated date, and reviewers[1] and reviewers[2] get email
        SpecimenRequestReviewVote vote = request.getConsortiumReviews().iterator().next();
        SpecimenRequestPersistenceTestHelper.setVote(vote, Vote.DENY,
                instToConsortiumReviewerMap.get(vote.getInstitution()));
        SpecimenRequestReviewVote institutionVote = request.getInstitutionalReviews().iterator().next();
        SpecimenRequestPersistenceTestHelper.setVote(institutionVote,
                Vote.DENY, instToInstReviewerMap.get(institutionVote.getInstitution()));
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        verifyVoteCounts(numConsortiumInstitutions, request);
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertEquals(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(oldStatus, request.getStatus());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertEquals(0, Mailbox.get(request.getRequestor().getEmail()).size());
        for (TissueLocatorUser reviewer : instToConsortiumReviewerMap.values()) {
            if (reviewer.getInstitution().equals(vote.getInstitution())) {
                assertEquals(0, Mailbox.get(reviewer.getEmail()).size());
            } else {
                assertEquals(1, Mailbox.get(reviewer.getEmail()).size());
                testEmail(reviewer.getEmail(), "vote", "vote today");
            }
        }
        for (TissueLocatorUser assigner : assigners) {
            if (assigner.getInstitution().equals(vote.getInstitution())) {
                assertEquals(0, Mailbox.get(assigner.getEmail()).size());
            } else {
                assertEquals(1, Mailbox.get(assigner.getEmail()).size());
                testEmail(assigner.getEmail(), "awaiting immediate action",
                        "is late and has received the following email");
            }
        }
        for (TissueLocatorUser reviewer : instToInstReviewerMap.values()) {
            if (reviewer.getInstitution().equals(institutionVote.getInstitution())) {
                assertEquals(0, Mailbox.get(reviewer.getEmail()).size());
            } else {
                assertEquals(1, Mailbox.get(reviewer.getEmail()).size());
                testEmail(reviewer.getEmail(), "vote", "vote today");
            }
        }
        Mailbox.clearAll();


        //Voting period done, reviewers (consortium and institutional) all voted but deadlocked occured
        //All reviewers and assigners get an email

        for (SpecimenRequestReviewVote instVote : request.getInstitutionalReviews()) {
            SpecimenRequestPersistenceTestHelper.setVote(instVote, Vote.APPROVE,
                    instToInstReviewerMap.get(instVote.getInstitution()));
        }

        Iterator<SpecimenRequestReviewVote> votes = request.getConsortiumReviews().iterator();
        SpecimenRequestReviewVote vote1 = votes.next();
        SpecimenRequestPersistenceTestHelper.setVote(vote1, Vote.APPROVE,
                instToConsortiumReviewerMap.get(vote1.getInstitution()));

        SpecimenRequestReviewVote vote2 = votes.next();
        SpecimenRequestPersistenceTestHelper.setVote(vote2, Vote.APPROVE,
                instToConsortiumReviewerMap.get(vote2.getInstitution()));

        SpecimenRequestReviewVote vote3 = votes.next();
        SpecimenRequestPersistenceTestHelper.setVote(vote3, Vote.DENY,
                instToConsortiumReviewerMap.get(vote3.getInstitution()));

        SpecimenRequestReviewVote vote4 = votes.next();
        SpecimenRequestPersistenceTestHelper.setVote(vote4, Vote.DENY,
                instToConsortiumReviewerMap.get(vote4.getInstitution()));

        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        verifyVoteCounts(numConsortiumInstitutions, request);
        oldUpdatedDate = request.getUpdatedDate();
        oldStatus = request.getStatus();
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, id);

        assertEquals(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(oldStatus, request.getStatus());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());

        for (TissueLocatorUser reviewer : reviewers) {
            assertEquals(1, Mailbox.get(reviewer.getEmail()).size());
            testEmail(reviewer.getEmail(), "requires your action", "recast your vote to resolve the deadlock");
        }

        for (TissueLocatorUser assigner : assigners) {
            assertEquals(1, Mailbox.get(assigner.getEmail()).size());
            testEmail(assigner.getEmail(), "awaiting immediate action", "received the following email");
        }

        Mailbox.clearAll();


        //now all consortium reviewers vote the same, so enough votes available
        //inst review done previously, so voting is closed and the lead reviewers are emailed.
       for (SpecimenRequestReviewVote consortiumVote : request.getConsortiumReviews()) {
            if (consortiumVote.getVote() == null) {
                SpecimenRequestPersistenceTestHelper.setVote(consortiumVote, Vote.DENY,
                        instToConsortiumReviewerMap.get(consortiumVote.getInstitution()));
            } else if (consortiumVote.getVote() != Vote.DENY) {
                consortiumVote.setDate(new Date());
                consortiumVote.setVote(Vote.DENY);
            }
        }
        request.setExternalComment("externalComment");
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        verifyVoteCounts(numConsortiumInstitutions, request);
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertNotSame(oldStatus, request.getStatus());
        assertEquals(RequestStatus.PENDING_FINAL_DECISION, request.getStatus());
        assertEquals(RequestStatus.DENIED, request.getFinalVote());
        assertFalse(request.getLineItems().isEmpty());
        assertTrue(StringUtils.isBlank(request.getExternalComment()));
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        verifyVoteFinalizedEmails(reviewers, assigners);
        assertTrue(Mailbox.get(instReviewer1.getEmail()).isEmpty());
        assertTrue(Mailbox.get(instReviewer2.getEmail()).isEmpty());
        Mailbox.clearAll();

        //clear comments so lead review is not done but vote would still pass
        //lead reviewers and reviewer assigners are not emailed about their late review
        //lead reviewers are emailed because request is updated
        request.getComments().clear();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, -VOTING_PERIOD - 1);
        request.setUpdatedDate(cal.getTime());
        request.setStatus(RequestStatus.PENDING);
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertTrue(request.getComments().isEmpty());
        oldUpdatedDate = request.getUpdatedDate();
        oldStatus = request.getStatus();
        verifyVoteCounts(numConsortiumInstitutions, request);
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertNotSame(oldStatus, request.getStatus());
        assertEquals(RequestStatus.PENDING_FINAL_DECISION, request.getStatus());
        assertEquals(RequestStatus.DENIED, request.getFinalVote());
        assertFalse(request.getLineItems().isEmpty());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        verifyVoteFinalizedEmails(reviewers, assigners);
        assertTrue(Mailbox.get(instReviewer1.getEmail()).isEmpty());
        assertTrue(Mailbox.get(instReviewer2.getEmail()).isEmpty());
        Mailbox.clearAll();

        //now request is pending final decision.  run again to verify that the lead reviewers
        //and the reviewer assigners are emailed the vote finalized reminder
        oldUpdatedDate = request.getUpdatedDate();
        oldStatus = request.getStatus();
        verifyVoteCounts(numConsortiumInstitutions, request);
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertEquals(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(oldStatus, request.getStatus());
        assertEquals(RequestStatus.PENDING_FINAL_DECISION, request.getStatus());
        assertEquals(RequestStatus.DENIED, request.getFinalVote());
        assertFalse(request.getLineItems().isEmpty());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        verifyVoteFinalizedEmails(reviewers, assigners);
        assertTrue(Mailbox.get(instReviewer1.getEmail()).isEmpty());
        assertTrue(Mailbox.get(instReviewer2.getEmail()).isEmpty());
        Mailbox.clearAll();

        verifyFinalize(request, reviewers, assigners, tissueTechs, config, partialApproval);
        MailUtils.setMailEnabled(false);
    }

    private void verifyVoteFinalizedEmails(TissueLocatorUser[] reviewers, TissueLocatorUser[] assigners)
        throws MessagingException, IOException {
        assertEquals(1, Mailbox.get(reviewers[0].getEmail()).size());
        testEmail(reviewers[0].getEmail(), "awaiting", "final comment");
        assertEquals(1, Mailbox.get(reviewers[1].getEmail()).size());
        testEmail(reviewers[1].getEmail(), "awaiting", "final comment");
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[THREE].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assigners[0].getEmail()).size());
        testEmail(assigners[0].getEmail(), "awaiting immediate action", "is late and has received the following email");
        assertEquals(1, Mailbox.get(assigners[1].getEmail()).size());
        testEmail(assigners[1].getEmail(), "awaiting immediate action", "is late and has received the following email");
        assertTrue(Mailbox.get(assigners[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(assigners[THREE].getEmail()).isEmpty());
    }

    private void verifyVoteCounts(int numConsortiumInstitutions, SpecimenRequest request) {
        assertEquals(numConsortiumInstitutions, request.getConsortiumReviews().size());
        assertEquals(2, request.getInstitutionalReviews().size());
        assertEquals(BG_TEST_LINE_ITEM_COUNT, request.getLineItems().size());
    }

    private void verifyReview(SpecimenRequest request, TissueLocatorUser[] reviewers,
            RequestProcessingConfiguration config, TissueLocatorUser[] assigners)
            throws MessagingException, IOException {
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        SpecimenRequestProcessingServiceLocal processingService =
            TissueLocatorRegistry.getServiceLocator().getSpecimenRequestProcessingService();

        //no comments, so review is not done.  review period is not over, so no emails sent
        Date oldUpdatedDate = request.getUpdatedDate();
        RequestStatus oldStatus = request.getStatus();
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        assertEquals(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(oldStatus, request.getStatus());
        assertFalse(request.getLineItems().isEmpty());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[1].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[THREE].getEmail()).isEmpty());
        Mailbox.clearAll();

        //reviewers[0] comments, but review is still not done (reviewers[1] still needs to comment)
        //review period is over, so reviewers[1] is emailed
        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, -REVIEW_PERIOD / 2);
        request.getComments().add(SpecimenRequestPersistenceTestHelper.getReviewComment(reviewers[0], cal.getTime()));
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, -REVIEW_PERIOD - 1);
        request.setUpdatedDate(cal.getTime());
        service.savePersistentObject(request);
        oldUpdatedDate = request.getUpdatedDate();
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        assertEquals(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(oldStatus, request.getStatus());
        assertFalse(request.getLineItems().isEmpty());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewers[1].getEmail()).size());
        testEmail(reviewers[1].getEmail(), "please review", "requires your immediate review");
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[THREE].getEmail()).isEmpty());
        assertTrue(Mailbox.get(assigners[0].getEmail()).isEmpty());
        assertFalse(Mailbox.get(assigners[1].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assigners[1].getEmail()).size());
        testEmail(assigners[1].getEmail(), "awaiting immediate action", "is late and has received the following email");
        assertTrue(Mailbox.get(assigners[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(assigners[THREE].getEmail()).isEmpty());
        Mailbox.clearAll();

        //reviewers[1], reviewers[2] and reviewers[3] comment after the review period, so review done.  No emails sent.
        request.getComments().add(SpecimenRequestPersistenceTestHelper.getReviewComment(reviewers[1], new Date()));
        request.getComments().add(SpecimenRequestPersistenceTestHelper.getReviewComment(reviewers[2], new Date()));
        request.getComments().add(SpecimenRequestPersistenceTestHelper.getReviewComment(reviewers[THREE], new Date()));
        service.savePersistentObject(request);
        oldUpdatedDate = request.getUpdatedDate();
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        assertEquals(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(oldStatus, request.getStatus());
        assertFalse(request.getLineItems().isEmpty());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[1].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[THREE].getEmail()).isEmpty());
        assertTrue(Mailbox.get(assigners[0].getEmail()).isEmpty());
        assertTrue(Mailbox.get(assigners[1].getEmail()).isEmpty());
        assertTrue(Mailbox.get(assigners[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(assigners[THREE].getEmail()).isEmpty());
        Mailbox.clearAll();
    }

    private void verifyFinalize(SpecimenRequest request, TissueLocatorUser[] reviewers, TissueLocatorUser[] assigners,
            TissueLocatorUser[] tissueTechs, RequestProcessingConfiguration config, boolean partialApproval)
        throws MessagingException, IOException {
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        SpecimenRequestProcessingServiceLocal processingService =
            TissueLocatorRegistry.getServiceLocator().getSpecimenRequestProcessingService();
        request = SpecimenRequestPersistenceTestHelper.resetRequest(request, service, partialApproval);
        request.getReviewers().remove(reviewers[0].getInstitution());
        TissueLocatorUser previousReviewer = null;
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getInstitution().equals(reviewers[1].getInstitution())) {
                previousReviewer = vote.getUser();
                vote.setUser(null);
                vote.setVote(null);
            } else {
                vote.setVote(Vote.DENY);
            }
        }
        for (SpecimenRequestReviewVote vote : request.getInstitutionalReviews()) {
            vote.setVote(Vote.DENY);
        }
        service.savePersistentObject(request);
        Date oldUpdatedDate = request.getUpdatedDate();
        RequestStatus oldStatus = request.getStatus();
        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        assertEquals(RequestStatus.PENDING_FINAL_DECISION, request.getStatus());
        assertEquals(RequestStatus.DENIED, request.getFinalVote());
        assertTrue(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[1].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[THREE].getEmail()).isEmpty());
        assertTrue(Mailbox.get(tissueTechs[0].getEmail()).isEmpty());
        assertTrue(Mailbox.get(tissueTechs[1].getEmail()).isEmpty());
        assertTrue(Mailbox.get(tissueTechs[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(tissueTechs[THREE].getEmail()).isEmpty());
        assertTrue(Mailbox.get(assigners[0].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(assigners[1].getEmail()).size());
        testEmail(assigners[1].getEmail(), "awaiting your immediate action", "Assign a Lead Reviewer");
        assertTrue(Mailbox.get(assigners[2].getEmail()).isEmpty());
        assertTrue(Mailbox.get(assigners[THREE].getEmail()).isEmpty());
        Mailbox.clearAll();
        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        request.setExternalComment("external comment");
        service.finalizeVote(request, true);
        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertNotSame(oldStatus, request.getStatus());
        assertEquals(RequestStatus.DENIED, request.getStatus());
        assertFalse(StringUtils.isBlank(request.getExternalComment()));
        assertEquals(RequestStatus.DENIED, request.getFinalVote());
        assertFalse(request.getLineItems().isEmpty());
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            assertEquals(lineItem.getSpecimen().getStatus(), SpecimenStatus.AVAILABLE);
        }
        assertNotNull(request.getOrderLineItems());
        assertTrue(request.getOrderLineItems().isEmpty());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        assertFalse(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(request.getRequestor().getEmail()).size());
        testEmail(request.getRequestor().getEmail(), "has been updated", "update", "has been made");
        for (int i = 0; i <= THREE; i++) {
            assertTrue(Mailbox.get(reviewers[i].getEmail()).isEmpty());
            assertTrue(Mailbox.get(assigners[i].getEmail()).isEmpty());
        }
        Mailbox.clearAll();

        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        request.getReviewers().add(reviewers[0].getInstitution());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getInstitution().equals(reviewers[1].getInstitution())) {
                vote.setUser(previousReviewer);
            }
            vote.setVote(Vote.APPROVE);
        }
        for (SpecimenRequestReviewVote vote : request.getInstitutionalReviews()) {
            vote.setVote(Vote.APPROVE);
        }
        service.savePersistentObject(request);
        request = SpecimenRequestPersistenceTestHelper.resetRequest(request, service, partialApproval);
        oldUpdatedDate = request.getUpdatedDate();
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            assertEquals(lineItem.getSpecimen().getStatus(), SpecimenStatus.UNDER_REVIEW);
        }

        processingService.processPendingRequests(config);
        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        request.setExternalComment("finalizing vote");
        service.finalizeVote(request, true);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = service.getPersistentObject(SpecimenRequest.class, request.getId());
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(partialApproval ? RequestStatus.PARTIALLY_APPROVED : RequestStatus.APPROVED,
                request.getStatus());
        assertEquals(partialApproval ? RequestStatus.PARTIALLY_APPROVED : RequestStatus.APPROVED,
                request.getFinalVote());
        if (partialApproval) {
            assertTrue(request.getLineItems().size() < BG_TEST_LINE_ITEM_COUNT);
            for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
                assertEquals(lineItem.getSpecimen().getStatus(), SpecimenStatus.AVAILABLE);
            }
        } else {
            assertTrue(request.getLineItems().isEmpty());
        }

        Set<Shipment> shipments = new HashSet<Shipment>(
                TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class));
        assertFalse(shipments.isEmpty());
        assertEquals(partialApproval ? 1 : 2, shipments.size());
        for (Shipment s : shipments) {
            if (reviewers[0].getInstitution().equals(s.getSendingInstitution())) {
                assertEquals(1, s.getLineItems().size());
            } else {
                assertEquals(2, s.getLineItems().size());
            }
            assertEquals(request, s.getRequest());
            assertNotNull(s.getRequest().getOrderLineItems());
            assertFalse(s.getRequest().getOrderLineItems().isEmpty());
            assertEquals(ShipmentStatus.PENDING, s.getStatus());
            assertEquals(request.getShipment().getRecipient().getFirstName(), s.getRecipient().getFirstName());
            assertEquals(request.getShipment().getRecipient().getLastName(), s.getRecipient().getLastName());
            assertEquals(request.getShipment().getRecipient().getEmail(), s.getRecipient().getEmail());
            assertEquals(request.getShipment().getRecipient().getOrganization(), s.getRecipient().getOrganization());
            assertTrue(EqualsBuilder.reflectionEquals(request.getShipment().getRecipient().getAddress(),
                    s.getRecipient().getAddress(), new String[] {"id"}));
            assertEquals(request.getShipment().getBillingRecipient().getFirstName(),
                    s.getBillingRecipient().getFirstName());
            assertEquals(request.getShipment().getBillingRecipient().getLastName(),
                    s.getBillingRecipient().getLastName());
            assertEquals(request.getShipment().getBillingRecipient().getEmail(), s.getBillingRecipient().getEmail());
            assertEquals(request.getShipment().getBillingRecipient().getOrganization(),
                    s.getBillingRecipient().getOrganization());
            assertTrue(EqualsBuilder.reflectionEquals(request.getShipment().getBillingRecipient().getAddress(),
                    s.getBillingRecipient().getAddress(), new String[] {"id"}));
            for (SpecimenRequestLineItem li : s.getLineItems()) {
                assertEquals(li.getSpecimen().getStatus(), SpecimenStatus.PENDING_SHIPMENT);
            }
            for (TissueLocatorUser tech : tissueTechs) {
                if (tech.getInstitution().equals(s.getSendingInstitution())) {
                    assertFalse(Mailbox.get(tech.getEmail()).isEmpty());
                    assertEquals(1, Mailbox.get(tech.getEmail()).size());
                    testEmail(tech.getEmail(), "awaiting immediate action", "Order Fulfillment Request");
                }
            }
        }

        assertEquals(1, Mailbox.get(request.getRequestor().getEmail()).size());
        testEmail(request.getRequestor().getEmail(), "has been updated", "update", "has been made");
        verifyVoteFinalizedEmails(reviewers, assigners);
        Mailbox.clearAll();
    }

    /**
     * Test the deletion of a draft request.
     */
    @Test
    public void testDeleteRequest() {
        SpecimenRequest request = createRequest(false, SpecimenStatus.AVAILABLE);
        request.setStatus(RequestStatus.DRAFT);

        Long id = getService().saveDraftRequest(request);
        request = getService().getPersistentObject(SpecimenRequest.class, id);

        getService().deleteRequest(request);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        assertNull(request);
    }

    /**
     * Tests the deletion of a request that cannot be deleted.
     */
    @Test
    public void testDeleteRequestException() {
        SpecimenRequest request = createRequest(false, SpecimenStatus.AVAILABLE);
        request.setStatus(RequestStatus.PENDING_REVISION);

        Long id = getService().savePersistentObject(request);
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        try {
            getService().deleteRequest(request);
            fail("Exception should be thrown - request is not a draft.");
        } catch (IllegalArgumentException e) {
            // expected
        }

        request = createRequest(false, SpecimenStatus.AVAILABLE);
        request.setStatus(RequestStatus.DRAFT);
        try {
            getService().deleteRequest(request);
            fail("Exception should be thrown - request has not been persisted.");
        } catch (IllegalArgumentException e) {
            // expected
        }
    }

    /**
     * tests the assign reviewer method called by the ABRC specimen request review action.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testAssignReviewer() throws MessagingException, IOException {
        FullConsortiumReviewServiceLocal service = getReviewService();
        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        Long id = service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        TissueLocatorUser newUser = createUserSkipRole();
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        Date oldUpdatedDate = request.getUpdatedDate();
        request.setUpdatedDate(new Date());
        service.assignRequest(request, newUser, 2, 1);
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertFalse(Mailbox.get(newUser.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(newUser.getEmail()).size());
        testEmail(newUser.getEmail(), "", "vote");
        assertTrue(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        Mailbox.clearAll();

        oldUpdatedDate = request.getUpdatedDate();
        request.setUpdatedDate(new Date());
        service.assignRequest(request, getCurrentUser(), 2, 1);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertFalse(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(getCurrentUser().getEmail()).size());
        testEmail(getCurrentUser().getEmail(), "", "review");
        assertTrue(Mailbox.get(newUser.getEmail()).isEmpty());
        Mailbox.clearAll();

        oldUpdatedDate = request.getUpdatedDate();
        request.setUpdatedDate(new Date());
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        service.assignRequest(request, getCurrentUser(), 2, 1);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertEquals(RequestStatus.PENDING_FINAL_DECISION, request.getStatus());
        assertFalse(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(getCurrentUser().getEmail()).size());
        testEmail(getCurrentUser().getEmail(), "awaiting", "final comment");
        assertTrue(Mailbox.get(newUser.getEmail()).isEmpty());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * test the analyze request status part of the specimen request processing for ABRC.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testAnalyzeRequestStatus() throws MessagingException, IOException {
        RequestProcessingConfiguration config = new RequestProcessingConfiguration();
        config.setReviewPeriod(REVIEW_PERIOD);
        config.setReviewerAssignmentPeriod(REVIEWER_ASSIGNMENT_PERIOD);
        config.setReviewerAssignmentLateEmail(getEmail("reviewerAssignmentLate"));
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        TissueLocatorUser instReviewer = createReviewerAssigner(getCurrentUser().getInstitution());
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        request.getConsortiumReviews().clear();
        request.getConsortiumReviews().add(SpecimenRequestPersistenceTestHelper.getVote(getCurrentUser()));
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        SpecimenRequestProcessingServiceLocal processingService =
            TissueLocatorRegistry.getServiceLocator().getSpecimenRequestProcessingService();
        UsernameHolder.setUser(request.getRequestor().getUsername());
        Long id = service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertTrue(Mailbox.get(instReviewer.getEmail()).isEmpty());
        Mailbox.clearAll();

        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, -REVIEWER_ASSIGNMENT_PERIOD - 1);
        request.setUpdatedDate(cal.getTime());
        request.getConsortiumReviews().iterator().next().setUser(null);
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        processingService.processPendingRequests(config);
        assertFalse(Mailbox.get(instReviewer.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(instReviewer.getEmail()).size());
        testEmail(instReviewer.getEmail(), "awaiting your immediate action", "assign", "scientific reviewer");
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Tests make specimen in shipment destroyed.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testDestroyedSpecimen() throws MessagingException, IOException {
        testRemovedSpecimen(SpecimenStatus.DESTROYED);
    }

    /**
     * Tests make specimen in shipment unavailable.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testUnavailableSpecimen() throws MessagingException, IOException {
        testRemovedSpecimen(SpecimenStatus.UNAVAILABLE);
    }

    private void testRemovedSpecimen(SpecimenStatus status) throws MessagingException, IOException {
        SpecimenRequest request = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        getService().savePersistentObject(request);

        TissueLocatorUser consortiumReviewer = createUser(true, getCurrentUser().getInstitution());
        request.getConsortiumReviews().add(SpecimenRequestPersistenceTestHelper.getVote(consortiumReviewer));

        getService().savePersistentObject(request);

        TissueLocatorUser instReviewer = createUser(true, getCurrentUser().getInstitution());
        request.getInstitutionalReviews().add(SpecimenRequestPersistenceTestHelper.getVote(instReviewer));
        getService().savePersistentObject(request);

        int lineItemCount = request.getLineItems().size();
        Iterator<SpecimenRequestLineItem> it = request.getLineItems().iterator();
        Specimen s2 = it.next().getSpecimen();
        Specimen s = it.next().getSpecimen();

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        s = TissueLocatorRegistry.getServiceLocator().
            getSpecimenService().getPersistentObject(Specimen.class, s.getId());
        s.setStatus(status);
        TissueLocatorRegistry.getServiceLocator().getSpecimenService().savePersistentObject(s);

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, request.getId());
        assertEquals(lineItemCount - 1, request.getLineItems().size());

        assertEquals(1, Mailbox.get(request.getRequestor().getEmail()).size());
        testEmail(request.getRequestor().getEmail(), "has been updated", "was in the request, but has been removed");
        assertEquals(1, Mailbox.get(consortiumReviewer.getEmail()).size());
        testEmail(consortiumReviewer.getEmail(), "has been updated",
                "was in the request, but has been removed as it is no longer available.",
                "This information is being provided to you in order to aid in your review.");
        assertEquals(1, Mailbox.get(instReviewer.getEmail()).size());
        testEmail(instReviewer.getEmail(), "has been updated",
                "was in the request, but has been removed as it is no longer available.",
                "This information is being provided to you in order to aid in your review.");

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        Mailbox.clearAll();

        s2 = TissueLocatorRegistry.getServiceLocator().
        getSpecimenService().getPersistentObject(Specimen.class, s2.getId());
        s2.setStatus(status);
        TissueLocatorRegistry.getServiceLocator().getSpecimenService().savePersistentObject(s2);

        request = getService().getPersistentObject(SpecimenRequest.class, request.getId());
        assertEquals(0, request.getLineItems().size());

        assertEquals(1, Mailbox.get(request.getRequestor().getEmail()).size());
        testEmail(request.getRequestor().getEmail(), "has been updated", "was in the request, but has been removed");
        assertEquals(1, Mailbox.get(consortiumReviewer.getEmail()).size());
        testEmail(consortiumReviewer.getEmail(), "has been updated",
                "was in the request, but has been removed as it is no longer available.",
                "This request no longer contains any",
                "It is suggested that you vote for the requester to revise the request",
                "can be selected.");
        assertEquals(1, Mailbox.get(instReviewer.getEmail()).size());
        testEmail(instReviewer.getEmail(), "has been updated",
                "was in the request, but has been removed as it is no longer available.",
                "This request no longer contains any",
                "review is no longer needed");

        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Tests the validation of specimen availability.
     * @throws Exception on saveRequest() error.
     */
    @Test
    public void testSpecimenAvailability() throws Exception {
        // set up necessary conditions
        SpecimenRequestPersistenceTestHelper.createProspectiveCollectionApplicationSetting(false);
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        createReviewerAssigner(getCurrentUser().getInstitution());
        for (Institution i : TissueLocatorRegistry.getServiceLocator().
                getInstitutionService().getAll(Institution.class)) {
            i.setLastReviewAssignment(null);
            TissueLocatorHibernateUtil.getCurrentSession().update(i);
        }
        getCurrentUser().getInstitution().setLastReviewAssignment(new Date());
        TissueLocatorHibernateUtil.getCurrentSession().update(getCurrentUser().getInstitution());

        // new request with available specimens
        SpecimenRequest request1 = createRequest(true, SpecimenStatus.AVAILABLE);
        UsernameHolder.setUser(request1.getRequestor().getUsername());
        try {
            getService().saveRequest(request1, 2, 1, 1, 0);
        } catch (IllegalStateException e) {
            fail("Exception should not be thrown - new request with available specimens");
        }

        // new request with requested specimens
        SpecimenRequest request2 = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        try {
            getService().saveRequest(request2, 2, 1, 1, 0);
            fail("Exception should be thrown, specimens unavailable.");
        } catch (IllegalStateException e) {
            // expected
        }

        // draft request with available specimens
        SpecimenRequest draft1 = createRequest(true, SpecimenStatus.AVAILABLE);
        try {
            Long id = getService().saveDraftRequest(draft1);
            draft1 = getService().getPersistentObject(SpecimenRequest.class, id);
            getService().saveRequest(draft1, 2, 1, 1, 0);
        } catch (IllegalStateException e) {
            fail("Exception should not be thrown - draft request with available specimens");
        }

        // draft with requested specimens
        SpecimenRequest draft2 = createRequest(true, SpecimenStatus.AVAILABLE);
        getService().saveDraftRequest(draft2);
        SpecimenRequest request3 = createRequest(true, SpecimenStatus.AVAILABLE);
        request3.getLineItems().clear();
        for (SpecimenRequestLineItem lineItem : draft2.getLineItems()) {
            SpecimenRequestLineItem li = new SpecimenRequestLineItem();
            li.setSpecimen(lineItem.getSpecimen());
            li.setQuantity(new BigDecimal(1));
            li.setQuantityUnits(QuantityUnits.MG);
            request3.getLineItems().add(li);
        }
        getService().saveRequest(request3, 2, 1, 1, 0);
        try {
            getService().saveRequest(draft2, 2, 1, 1, 0);
            fail("Exception should have been thrown - specimens already requested.");
        } catch (IllegalStateException e) {
            // expected
        }

        // revision request
        request3.setStatus(RequestStatus.PENDING_REVISION);
        request3.setExternalComment("revise");
        try {
            getService().saveRequest(request3, 2, 1, 1, 0);
        } catch (IllegalStateException e) {
            fail("Exception should not be thrown - specimens requested for this request.");
        }

        // revision, saved then submitted
        request3.setStatus(RequestStatus.PENDING_REVISION);
        request3.setPreviousStatus(RequestStatus.PENDING_REVISION);
        request3.setRevisionComment("revising");
        getService().saveDraftRequest(request3);
        try {
            getService().saveRequest(request3, 2, 1, 1, 0);
        } catch (IllegalStateException e) {
            fail("Exception should not be thrown - specimens requested for this request.");
        }
    }

    /**
     * Tests irb approval validation.
     */
    @Test
    public void testIrbApprovalValidation() {
        SpecimenRequest request = createRequest(false, SpecimenStatus.AVAILABLE);
        Study study = request.getStudy();
        study.setIrbName(null);
        study.setIrbApprovalExpirationDate(null);
        study.setIrbApprovalNumber(null);
        study.setIrbApprovalLetter(null);
        study.setIrbExemptionLetter(null);
        study = request.getStudy();
        study.setIrbApprovalStatus(IrbApprovalStatus.NOT_APPROVED);
        request = verifyNoIrbException(request);
        study = request.getStudy();
        study.setIrbApprovalStatus(IrbApprovalStatus.EXEMPT);
        verifyIrbException(request);
        study.setIrbExemptionLetter(new TissueLocatorFile(new byte[]{}, "filename", "contenttype"));
        verifyIrbException(request);
        study.setIrbName("irb name");
        request = verifyNoIrbException(request);
        study = request.getStudy();
        study.setIrbName(null);
        study.setIrbApprovalStatus(IrbApprovalStatus.APPROVED);
        verifyIrbException(request);
        study.setIrbName("irb name");
        verifyIrbException(request);
        study.setIrbApprovalExpirationDate(new Date());
        verifyIrbException(request);
        study.setIrbApprovalLetter(new TissueLocatorFile(new byte[]{}, "filename", "contenttype"));
        request = verifyNoIrbException(request);
        request.getStudy().setIrbApprovalNumber("irb approval number");
        request = verifyNoIrbException(request);
        request.getStudy().setIrbApprovalLetter(null);
        request = verifyNoIrbException(request);
        study.setIrbApprovalLetter(new TissueLocatorFile());
        study.setIrbApprovalNumber(null);
        verifyNoIrbException(request);
    }

    /**
     * Tests irb approval requirement validation.
     */
    @Test
    public void testIrbApprovalRequiredValidation() {
        StudyIrbApprovalRequiredValidator.reset();
        SpecimenRequestPersistenceTestHelper.createStudyIrbApprovalRequiredSetting(true);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        SpecimenRequest request = createRequest(false, SpecimenStatus.AVAILABLE);
        // verify a request for an exempt study
        request.getStudy().setIrbApprovalStatus(IrbApprovalStatus.EXEMPT);
        request.getStudy().setIrbExemptionLetter(new TissueLocatorFile(new byte[]{}, "filename", "contenttype"));
        request.getStudy().setIrbName("irb name");
        request = verifyNoIrbException(request);
        // set study IRB as not approved
        request.getStudy().setIrbApprovalStatus(IrbApprovalStatus.NOT_APPROVED);
        verifyIrbException(request);
        // set IRB as approved
        request.getStudy().setIrbApprovalStatus(IrbApprovalStatus.APPROVED);
        request.getStudy().setIrbApprovalExpirationDate(new Date());
        request.getStudy().setIrbExemptionLetter(null);
        verifyNoIrbException(request);
        StudyIrbApprovalRequiredValidator.reset();
    }

    private SpecimenRequest verifyNoIrbException(SpecimenRequest request) {
        try {
            Long requestId = getService().savePersistentObject(request);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
            return getService().getPersistentObject(SpecimenRequest.class, requestId);
        } catch (InvalidStateException e) {
            fail("Valid study irb approval, unexpected exception.");
        }
        return null;
    }

    private void verifyIrbException(SpecimenRequest request) {
        try {
            getService().savePersistentObject(request);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
            fail("Invalid study irb approval.");
        } catch (InvalidStateException e) {
            // expected
        }
    }

    /**
     * Tests Funding Status Validation
     * when the minimum funding status is NOT_FUNDED.
     */
    @Test
    public void testNotFundedMinFundingStatusValidation() {
        SpecimenRequestPersistenceTestHelper
            .createMinimumFundingStatusSetting(FundingStatus.NOT_FUNDED); //minimum funding status
        SpecimenRequest request = createRequest(false, SpecimenStatus.AVAILABLE);
        request.getStudy().setFundingStatus(FundingStatus.NOT_FUNDED);
        request = verifyNoFundingStatusException(request);
        request.getStudy().setFundingStatus(FundingStatus.PENDING);
        request = verifyNoFundingStatusException(request);
        request.getStudy().setFundingStatus(FundingStatus.FUNDED);
        request = verifyNoFundingStatusException(request);
        request.getStudy().setFundingStatus(null);
        verifyFundingStatusException(request);
    }

    /**
     * Tests Funding Status Validation
     * when the minimum funding status is PENDING.
     */
    @Test
    public void testPendingMinFundingStatusValidation() {
       // DisableableUtil.resetThreadLocal();
        FundingStatusCheckValidator.reset();
        SpecimenRequestPersistenceTestHelper.createMinimumFundingStatusSetting(FundingStatus.PENDING);
        SpecimenRequest request = createRequest(false, SpecimenStatus.AVAILABLE);
        request.getStudy().setFundingStatus(FundingStatus.PENDING);
        request = verifyNoFundingStatusException(request);
        request.getStudy().setFundingStatus(FundingStatus.FUNDED);
        request = verifyNoFundingStatusException(request);
        request.getStudy().setFundingStatus(FundingStatus.NOT_FUNDED);
        verifyFundingStatusException(request);
        request.getStudy().setFundingStatus(null);
        verifyFundingStatusException(request);
    }

    /**
     * Tests Funding Status Validation
     * when the minimum funding status is FUNDED.
     */
    @Test
    public void testFundedMinFundingStatusValidation() {
        FundingStatusCheckValidator.reset();
        SpecimenRequestPersistenceTestHelper.createMinimumFundingStatusSetting(FundingStatus.FUNDED);
        SpecimenRequest request = createRequest(false, SpecimenStatus.AVAILABLE);
        request.getStudy().setFundingStatus(FundingStatus.FUNDED);
        request = verifyNoFundingStatusException(request);
        request.getStudy().setFundingStatus(FundingStatus.NOT_FUNDED);
        verifyFundingStatusException(request);
        request.getStudy().setFundingStatus(FundingStatus.PENDING);
        verifyFundingStatusException(request);
        request.getStudy().setFundingStatus(null);
        verifyFundingStatusException(request);
    }


    private SpecimenRequest verifyNoFundingStatusException(SpecimenRequest request) {
        try {
            Long requestId = getService().savePersistentObject(request);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
            return getService().getPersistentObject(SpecimenRequest.class, requestId);
        } catch (InvalidStateException e) {
            fail("Invalid Funding status relationship, unexpected exception.");
        }
        return null;
    }

    private void verifyFundingStatusException(SpecimenRequest request) {
        try {
            getService().savePersistentObject(request);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
            fail("Invaid funding status.");
        } catch (InvalidStateException e) {
            //expected
        }
    }

    /**
     *  Test required mta certification validation.
     */
    @Test
    public void testMtaCertificationRequiredValidation() {
        SpecimenRequestPersistenceTestHelper.createMtaCertificationSetting(true);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        SpecimenRequest request = createRequest(false, SpecimenStatus.AVAILABLE);
        try {
            getService().savePersistentObject(request);
        } catch (InvalidStateException e) {
            // expected, no certification
        }
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = createRequest(false, SpecimenStatus.AVAILABLE);
        request.setMtaCertification("");
        try {
            getService().savePersistentObject(request);
        } catch (InvalidStateException e) {
            // expected, empty certification
        }
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = createRequest(false, SpecimenStatus.AVAILABLE);
        request.setMtaCertification("GAV");
        getService().savePersistentObject(request);
    }

    /**
     *  Test no required mta certification validation.
     */
    @Test
    public void testNoMtaCertificationRequiredValidation() {
        SpecimenRequestPersistenceTestHelper.createMtaCertificationSetting(false);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        SpecimenRequest request = createRequest(false, SpecimenStatus.AVAILABLE);
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = createRequest(false, SpecimenStatus.AVAILABLE);
        request.setMtaCertification("");
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = createRequest(false, SpecimenStatus.AVAILABLE);
        request.setMtaCertification("GAV");
        getService().savePersistentObject(request);
    }

    /**
     * Tests the retrieval of requests with particular specimens.
     */
    @Test
    public void testGetRequestWithSpecimen() {
        testGetRequestWithSpecimenHelper(RequestStatus.PENDING, true);
        testGetRequestWithSpecimenHelper(RequestStatus.PENDING_FINAL_DECISION, true);
        testGetRequestWithSpecimenHelper(RequestStatus.PENDING_REVISION, true);
        testGetRequestWithSpecimenHelper(RequestStatus.APPROVED, false);
        testGetRequestWithSpecimenHelper(RequestStatus.PARTIALLY_APPROVED, false);
        testGetRequestWithSpecimenHelper(RequestStatus.DENIED, false);
        testGetRequestWithSpecimenHelper(RequestStatus.PARTIALLY_PROCESSED, false);
        testGetRequestWithSpecimenHelper(RequestStatus.FULLY_PROCESSED, false);
    }

    private void testGetRequestWithSpecimenHelper(RequestStatus status, boolean foundResult) {
        SpecimenRequest sr = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        sr.setStatus(status);
        Long id = getService().savePersistentObject(sr);
        SpecimenRequest request = getService().getPersistentObject(SpecimenRequest.class, id);
        sr = createRequest(true, SpecimenStatus.UNDER_REVIEW);
        sr.setStatus(status);
        getService().savePersistentObject(sr);
        sr = createRequest(false, SpecimenStatus.UNDER_REVIEW);
        sr.setStatus(status);
        getService().savePersistentObject(sr);
        for (SpecimenRequestLineItem li : request.getLineItems()) {
            SpecimenRequest foundRequest = getService().getRequestWithSpecimen(li.getSpecimen());
            assertEquals(foundResult, foundRequest != null);
            if (foundResult) {
                assertEquals(request.getId(), foundRequest.getId());
            }
        }
    }

    /**
     * determine if comment is valid.
     */
    @Test
    public void testIsCommentValid() {
        SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
        assertTrue(vote.isCommentValid());

        vote.setVote(Vote.APPROVE);
        assertTrue(vote.isCommentValid());

        vote.setVote(Vote.DENY);
        assertFalse(vote.isCommentValid());

        vote.setComment("test");
        assertTrue(vote.isCommentValid());
    }

    /**
     * determine if the pi investigation comment is valid.
     */
    @Test(expected = InvalidStateException.class)
    public void testIsPIInvestigationCommentValid() {
        SpecimenRequest request = getValidObjects()[0];
        assertFalse(StringUtils.isBlank(request.getInvestigator().getCertified().getResourceKey()));
        assertFalse(StringUtils.isBlank(request.getInvestigator().getInvestigated().getResourceKey()));
        assertTrue(request.getInvestigator().isInvestigationCommentValid());
        request.getInvestigator().setInvestigationComment("comment");
        assertTrue(request.getInvestigator().isInvestigationCommentValid());
        request.getInvestigator().setInvestigated(YesNoResponse.YES);
        assertTrue(request.getInvestigator().isInvestigationCommentValid());
        request.getInvestigator().setInvestigationComment(null);
        assertFalse(request.getInvestigator().isInvestigationCommentValid());
        request.getInvestigator().setInvestigated(YesNoResponse.NO);
        assertTrue(request.getInvestigator().isInvestigationCommentAllowed());
        request.getInvestigator().setInvestigationComment("comment");
        assertFalse(request.getInvestigator().isInvestigationCommentAllowed());
        getService().savePersistentObject(request);
    }

    /**
     * Test the finalizeVote method where no shipments are to be created and no emails are to
     * be sent to the tissue techs.
     * @throws MessagingException on error
     */
    @Test
    public void testFinalizeVoteNoShipments() throws MessagingException {
        MailUtils.setMailEnabled(true);
        SpecimenRequest request = createRequest(true, SpecimenStatus.AVAILABLE);
        Long requestId = getService().savePersistentObject(request);
        List<TissueLocatorUser> tissueTechs = new ArrayList<TissueLocatorUser>();
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            tissueTechs.add(createReviewerAssigner(lineItem.getSpecimen().getExternalIdAssigner()));
        }
        UsernameHolder.setUser(request.getRequestor().getUsername());
        request = getService().getPersistentObject(SpecimenRequest.class, requestId);
        assertEquals(RequestStatus.PENDING, request.getStatus());
        assertNull(request.getFinalVote());
        request.setFinalVote(RequestStatus.APPROVED);
        Date oldUpdatedDate = request.getUpdatedDate();
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        request = getService().getPersistentObject(SpecimenRequest.class, requestId);
        getService().finalizeVote(request, false);
        request = getService().getPersistentObject(SpecimenRequest.class, requestId);
        assertNotNull(request.getFinalVote());
        assertEquals(RequestStatus.APPROVED, request.getFinalVote());
        assertEquals(RequestStatus.APPROVED, request.getStatus());
        assertNotSame(oldUpdatedDate, request.getUpdatedDate());
        assertNotNull(request.getOrders());
        assertTrue(request.getOrders().isEmpty());
        assertTrue(TissueLocatorRegistry.getServiceLocator().getShipmentService().getAll(Shipment.class).isEmpty());
        for (TissueLocatorUser tissueTech : tissueTechs) {
            assertTrue(Mailbox.get(tissueTech.getEmail()).isEmpty());
        }
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * test for custom properties.
     */
    @Test
    public void testCustomProperties() {
        SpecimenRequest request = new SpecimenRequest();
        assertNotNull(request.getCustomProperties());
        String field = "field";
        assertNull(request.getCustomProperty(field));
        request.setCustomProperty(field, "value");
        assertNotNull(request.getCustomProperty(field));
        request.setCustomProperties(null);
        assertNull(request.getCustomProperties());
    }

    private FullConsortiumReviewServiceLocal getReviewService() {
        return TissueLocatorRegistry.getServiceLocator().getFullConsortiumReviewService();
    }

    /**
     * test the get new request count method.
     */
    @Test
    public void testGetNewRequestCount() {
        testSaveRetrieve();
        Date requestedDate = new Date();
        Date before = DateUtils.addDays(requestedDate, -1);
        Date after = DateUtils.addDays(requestedDate, 1);
        SpecimenRequestServiceLocal service = getService();
        assertEquals(2, service.getNewRequestCount(before, after, null));
        assertEquals(2, service.getNewRequestCount(before, requestedDate, null));
        assertEquals(2, service.getNewRequestCount(requestedDate, after, null));
        assertEquals(2, service.getNewRequestCount(requestedDate, requestedDate, null));
        assertEquals(0, service.getNewRequestCount(before, before, null));
        assertEquals(0, service.getNewRequestCount(after, after, null));

        assertEquals(2, service.getNewRequestCount(before, after, "1 = 1"));
        assertEquals(2, service.getNewRequestCount(before, requestedDate, "1 = 1"));
        assertEquals(2, service.getNewRequestCount(requestedDate, after, "1 = 1"));
        assertEquals(2, service.getNewRequestCount(requestedDate, requestedDate, "1 = 1"));
        assertEquals(0, service.getNewRequestCount(before, before, "1 = 1"));
        assertEquals(0, service.getNewRequestCount(after, after, "1 = 1"));

        String cond = "request.requestor.address.state = '" + getCurrentUser().getAddress().getState().name() + "'";
        assertEquals(2, service.getNewRequestCount(before, after, cond));
        assertEquals(2, service.getNewRequestCount(before, requestedDate, cond));
        assertEquals(2, service.getNewRequestCount(requestedDate, after, cond));
        assertEquals(2, service.getNewRequestCount(requestedDate, requestedDate, cond));
        assertEquals(0, service.getNewRequestCount(before, before, cond));
        assertEquals(0, service.getNewRequestCount(after, after, cond));

        cond = "request.requestor.address.state = '" + State.GUAM.name() + "'";
        assertEquals(0, service.getNewRequestCount(before, after, cond));
        assertEquals(0, service.getNewRequestCount(before, requestedDate, cond));
        assertEquals(0, service.getNewRequestCount(requestedDate, after, cond));
        assertEquals(0, service.getNewRequestCount(requestedDate, requestedDate, cond));
        assertEquals(0, service.getNewRequestCount(before, before, cond));
        assertEquals(0, service.getNewRequestCount(after, after, cond));
    }

    /**
     * test the get late scientific review counts method.
     */
    @Test
    public void testGetLateScientificReviewCounts() {
        SpecimenRequestServiceLocal service = getService();
        Date requestedDate = DateUtils.addDays(new Date(), -1);
        requestedDate = DateUtils.addDays(requestedDate, -1);
        Date before = DateUtils.addDays(requestedDate, -1);
        Date after = DateUtils.addDays(requestedDate, 1);
        Map<String, Integer> results = service.getLateScientificReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        testSaveRetrieve();
        results = service.getLateScientificReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        for (SpecimenRequest request : service.getAll(SpecimenRequest.class)) {
            request.setRequestedDate(requestedDate);
            SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
            vote.setDate(DateUtils.addDays(requestedDate, 2));
            vote.setVote(Vote.APPROVE);
            vote.setInstitution(getCurrentUser().getInstitution());
            vote.setComment("comment");
            EjbTestHelper.getGenericServiceBean(SpecimenRequestReviewVote.class).savePersistentObject(vote);
            request.getConsortiumReviews().add(vote);
            service.savePersistentObject(request);
        }
        results = service.getLateScientificReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
        for (SpecimenRequest request : service.getAll(SpecimenRequest.class)) {
            for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
                vote.setDate(requestedDate);
                EjbTestHelper.getGenericServiceBean(SpecimenRequestReviewVote.class).savePersistentObject(vote);
            }
            service.savePersistentObject(request);
        }
        results = service.getLateScientificReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        for (SpecimenRequest request : service.getAll(SpecimenRequest.class)) {
            for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
                vote.setVote(null);
                EjbTestHelper.getGenericServiceBean(SpecimenRequestReviewVote.class).savePersistentObject(vote);
            }
            service.savePersistentObject(request);
        }
        results = service.getLateScientificReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
        for (SpecimenRequest request : service.getAll(SpecimenRequest.class)) {
            request.setRequestedDate(DateUtils.addDays(requestedDate, 2));
            service.savePersistentObject(request);
        }
        results = service.getLateScientificReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        for (SpecimenRequest request : service.getAll(SpecimenRequest.class)) {
            request.setRequestedDate(requestedDate);
            for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
                vote.setDate(DateUtils.addDays(requestedDate, 2));
                vote.setVote(Vote.APPROVE);
                EjbTestHelper.getGenericServiceBean(SpecimenRequestReviewVote.class).savePersistentObject(vote);
            }
            service.savePersistentObject(request);
        }
        results = service.getLateScientificReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
        results = service.getLateScientificReviewCounts(before, requestedDate, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
        results = service.getLateScientificReviewCounts(requestedDate, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
        results = service.getLateScientificReviewCounts(before, before, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        results = service.getLateScientificReviewCounts(after, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        results = service.getLateScientificReviewCounts(requestedDate, requestedDate, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
    }

    /**
     * test the get late institutional review counts method.
     */
    @Test
    public void testGetLateInstitutionalReviewCounts() {
        SpecimenRequestServiceLocal service = getService();
        Date requestedDate = DateUtils.addDays(new Date(), -1);
        requestedDate = DateUtils.addDays(requestedDate, -1);
        Date before = DateUtils.addDays(requestedDate, -1);
        Date after = DateUtils.addDays(requestedDate, 1);
        Map<String, Integer> results = service.getLateInstitutionalReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        testSaveRetrieve();
        results = service.getLateInstitutionalReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        for (SpecimenRequest request : service.getAll(SpecimenRequest.class)) {
            request.setRequestedDate(requestedDate);
            SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
            vote.setDate(DateUtils.addDays(requestedDate, 2));
            vote.setVote(Vote.APPROVE);
            vote.setInstitution(getCurrentUser().getInstitution());
            vote.setComment("comment");
            EjbTestHelper.getGenericServiceBean(SpecimenRequestReviewVote.class).savePersistentObject(vote);
            request.getInstitutionalReviews().add(vote);
            service.savePersistentObject(request);
        }
        results = service.getLateInstitutionalReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
        for (SpecimenRequest request : service.getAll(SpecimenRequest.class)) {
            for (SpecimenRequestReviewVote vote : request.getInstitutionalReviews()) {
                vote.setDate(requestedDate);
                EjbTestHelper.getGenericServiceBean(SpecimenRequestReviewVote.class).savePersistentObject(vote);
            }
            service.savePersistentObject(request);
        }
        results = service.getLateInstitutionalReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        for (SpecimenRequest request : service.getAll(SpecimenRequest.class)) {
            for (SpecimenRequestReviewVote vote : request.getInstitutionalReviews()) {
                vote.setVote(null);
                EjbTestHelper.getGenericServiceBean(SpecimenRequestReviewVote.class).savePersistentObject(vote);
            }
            service.savePersistentObject(request);
        }
        results = service.getLateInstitutionalReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
        for (SpecimenRequest request : service.getAll(SpecimenRequest.class)) {
            request.setRequestedDate(DateUtils.addDays(requestedDate, 2));
            service.savePersistentObject(request);
        }
        results = service.getLateInstitutionalReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        for (SpecimenRequest request : service.getAll(SpecimenRequest.class)) {
            request.setRequestedDate(requestedDate);
            for (SpecimenRequestReviewVote vote : request.getInstitutionalReviews()) {
                vote.setDate(DateUtils.addDays(requestedDate, 2));
                vote.setVote(Vote.APPROVE);
                EjbTestHelper.getGenericServiceBean(SpecimenRequestReviewVote.class).savePersistentObject(vote);
            }
            service.savePersistentObject(request);
        }
        results = service.getLateInstitutionalReviewCounts(before, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
        results = service.getLateInstitutionalReviewCounts(before, requestedDate, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
        results = service.getLateInstitutionalReviewCounts(requestedDate, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
        results = service.getLateInstitutionalReviewCounts(before, before, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        results = service.getLateInstitutionalReviewCounts(after, after, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(0, results.values().iterator().next().intValue());
        results = service.getLateInstitutionalReviewCounts(requestedDate, requestedDate, 1);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(getCurrentUser().getInstitution().getName(), results.keySet().iterator().next());
        assertEquals(2, results.values().iterator().next().intValue());
    }

    /**
     * Test request status transition handling.
     */
    @Test
    public void testStatusTransitionHistory() {
        SpecimenRequest request = createRequest(true, SpecimenStatus.AVAILABLE);
        int statusCount = -1;
        request.setStatus(RequestStatus.DRAFT);
        Long id = getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        assertEquals(++statusCount, request.getStatusTransitionHistory().size());

        request.setStatus(RequestStatus.PENDING);
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        assertEquals(++statusCount, request.getStatusTransitionHistory().size());
        RequestStatusTransition t = request.getStatusTransitionHistory().get(statusCount - 1);
        assertEquals(RequestStatus.PENDING, t.getNewStatus());
        assertEquals(RequestStatus.DRAFT, t.getPreviousStatus());
        assertNotNull(t.getTransitionDate());
        assertNotNull(t.getSystemTransitionDate());

        request.setStatus(RequestStatus.APPROVED);
        Date date = new Date();
        request.setStatusTransitionDate(date);
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        assertEquals(++statusCount, request.getStatusTransitionHistory().size());
        t = request.getStatusTransitionHistory().get(statusCount - 1);
        assertEquals(RequestStatus.APPROVED, t.getNewStatus());
        assertEquals(RequestStatus.PENDING, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());
        assertNotNull(t.getSystemTransitionDate());
        assertNotSame(date, t.getSystemTransitionDate());

        // Edit a previously saved date
        date = new Date();
        t.setTransitionDate(date);
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        assertEquals(statusCount, request.getStatusTransitionHistory().size());
        assertEquals(RequestStatus.APPROVED, t.getNewStatus());
        assertEquals(RequestStatus.PENDING, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());

        request.setExternalComment("new_external_comment");
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SpecimenRequest.class, id);
        assertEquals(statusCount, request.getStatusTransitionHistory().size());
        assertEquals(RequestStatus.APPROVED, t.getNewStatus());
        assertEquals(RequestStatus.PENDING, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());

        request.getConsortiumReviews().add(SpecimenRequestPersistenceTestHelper.getVote(request.getRequestor()));
        request.getConsortiumReviews().add(SpecimenRequestPersistenceTestHelper.getVote(request.getRequestor()));
        request.setExternalComment("comment");
        try {
            getService().savePersistentObject(request);
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            fail("Exception expected.");
        } catch (InvalidStateException e) {
            // expected
        }
        assertEquals(statusCount, request.getStatusTransitionHistory().size());
        assertEquals(RequestStatus.APPROVED, t.getNewStatus());
        assertEquals(RequestStatus.PENDING, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());
    }

    /**
     * Test vote validation.
     */
    @Test
    public void testVoteValidation() {
        SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
        assertTrue(vote.isVoteAssociationValid());
        vote.getConsortiumReviewRequests().add(new SpecimenRequest());
        assertTrue(vote.isVoteAssociationValid());
        vote.getConsortiumReviewRequests().add(new SpecimenRequest());
        assertFalse(vote.isVoteAssociationValid());
        vote.getInstitutionalReviewRequests().add(new SpecimenRequest());
        assertFalse(vote.isVoteAssociationValid());
        vote.getConsortiumReviewRequests().clear();
        assertTrue(vote.isVoteAssociationValid());
        vote.getInstitutionalReviewRequests().add(new SpecimenRequest());
        assertFalse(vote.isVoteAssociationValid());
        vote.getAggregateLineItems().add(new AggregateSpecimenRequestLineItem());
        assertFalse(vote.isVoteAssociationValid());
        vote.getInstitutionalReviewRequests().clear();
        assertTrue(vote.isVoteAssociationValid());
        vote.getAggregateLineItems().add(new AggregateSpecimenRequestLineItem());
        assertFalse(vote.isVoteAssociationValid());

        vote.getConsortiumReviewRequests().clear();
        vote.getConsortiumReviewRequests().add(new SpecimenRequest());
        vote.getInstitutionalReviewRequests().clear();
        vote.getInstitutionalReviewRequests().add(new SpecimenRequest());
        vote.getAggregateLineItems().clear();
        vote.getAggregateLineItems().add(new AggregateSpecimenRequestLineItem());
        assertFalse(vote.isVoteAssociationValid());
    }


    /**
     * Test the separation of aggregate line items between reviewed and un-reviewed.
     */
    @Test
    public void testAggregateLineItemSeparation() {
        SpecimenRequest request = new SpecimenRequest();
        SpecimenRequestReviewVote vote1 = new SpecimenRequestReviewVote();
        AggregateSpecimenRequestLineItem lineItem1 = new AggregateSpecimenRequestLineItem();
        lineItem1.setVote(vote1);
        request.getAggregateLineItems().add(lineItem1);
        SpecimenRequestReviewVote vote2 = new SpecimenRequestReviewVote();
        vote2.setVote(Vote.APPROVE);
        AggregateSpecimenRequestLineItem lineItem2 = new AggregateSpecimenRequestLineItem();
        lineItem2.setVote(vote2);
        request.getAggregateLineItems().add(lineItem2);
        SpecimenRequestReviewVote vote3 = new SpecimenRequestReviewVote();
        vote3.setVote(Vote.DENY);
        AggregateSpecimenRequestLineItem lineItem3 = new AggregateSpecimenRequestLineItem();
        lineItem3.setVote(vote3);
        request.getAggregateLineItems().add(lineItem3);
        AggregateSpecimenRequestLineItem lineItem4 = new AggregateSpecimenRequestLineItem();
        request.getAggregateLineItems().add(lineItem4);
        assertNotNull(request.getUnreviewedAggregateLineItems());
        assertFalse(request.getUnreviewedAggregateLineItems().isEmpty());
        assertEquals(2, request.getUnreviewedAggregateLineItems().size());
        assertTrue(request.getUnreviewedAggregateLineItems().contains(lineItem1));
        assertTrue(request.getUnreviewedAggregateLineItems().contains(lineItem4));
        assertNotNull(request.getReviewedAggregateLineItems());
        assertFalse(request.getReviewedAggregateLineItems().isEmpty());
        assertEquals(2, request.getReviewedAggregateLineItems().size());
        assertTrue(request.getReviewedAggregateLineItems().contains(lineItem2));
        assertTrue(request.getReviewedAggregateLineItems().contains(lineItem3));
    }

    /**
     * Test the copy constructors.
     */
    @Test
    public void testCopyConstructors() {
        TissueLocatorUser current = getCurrentUser();
        PrincipalInvestigator pi = new PrincipalInvestigator(current);
        assertNotNull(pi);
        assertNull(pi.getResume());
        assertEquals(current.getFirstName(), pi.getFirstName());
        assertEquals(current.getLastName(), pi.getLastName());
        assertEquals(current.getEmail(), pi.getEmail());
        assertEquals(current.getInstitution(), pi.getOrganization());
        assertEquals(current.getInstitution().getId(), pi.getOrganization().getId());
        assertTrue(EqualsBuilder.reflectionEquals(current.getAddress(), pi.getAddress(), new String[] {"id"}));

        current.setResume(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));
        TissueLocatorHibernateUtil.getCurrentSession().save(current);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        current = getCurrentUser();
        pi = new PrincipalInvestigator(current);
        assertNotNull(pi.getResume());
        assertEquals(current.getResume().getContentType(), pi.getResume().getContentType());
        assertEquals(current.getResume().getName(), pi.getResume().getName());
        assertNotNull(current.getResume().getLob());
        assertNotNull(pi.getResume().getLob());
        assertEquals(current.getResume().getLob().getId(), pi.getResume().getLob().getId());
    }
}
