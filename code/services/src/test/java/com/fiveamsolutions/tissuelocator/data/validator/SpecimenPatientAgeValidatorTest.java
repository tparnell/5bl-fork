/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenPatientAgeValidator;

/**
 * @author akong
 */
public class SpecimenPatientAgeValidatorTest {
        
    private Specimen createTestSpecimen(int patientAgeAtCollection, TimeUnits patientAgeAtCollectionUnits) {
        Specimen s = new Specimen();
        s.setPatientAgeAtCollection(patientAgeAtCollection);
        s.setPatientAgeAtCollectionUnits(patientAgeAtCollectionUnits);
        return s;
    }
    
    /**
     * Tests the validator.
     */
    @Test
    public void testValidator() {
        SpecimenPatientAgeValidator validator = new SpecimenPatientAgeValidator();
        validator.initialize(null);
          
        assertFalse(validator.isValid(null));
               
        int maxAge = SpecimenPatientAgeValidator.getHippaMaxAge(TimeUnits.YEARS);
        TimeUnits ageUnit = TimeUnits.YEARS;
        assertTrue(validator.isValid(createTestSpecimen(maxAge, ageUnit)));
        assertTrue(validator.isValid(createTestSpecimen(maxAge - 1, ageUnit)));
        assertFalse(validator.isValid(createTestSpecimen(maxAge + 1, ageUnit))); 
        
        maxAge = SpecimenPatientAgeValidator.getHippaMaxAge(TimeUnits.MONTHS);
        ageUnit = TimeUnits.MONTHS;
        assertTrue(validator.isValid(createTestSpecimen(maxAge, ageUnit)));
        assertTrue(validator.isValid(createTestSpecimen(maxAge - 1, ageUnit)));
        assertFalse(validator.isValid(createTestSpecimen(maxAge + 1, ageUnit)));       
        
        maxAge = SpecimenPatientAgeValidator.getHippaMaxAge(TimeUnits.DAYS);
        ageUnit = TimeUnits.DAYS;
        assertTrue(validator.isValid(createTestSpecimen(maxAge, ageUnit)));
        assertTrue(validator.isValid(createTestSpecimen(maxAge - 1, ageUnit)));
        assertFalse(validator.isValid(createTestSpecimen(maxAge + 1, ageUnit))); 
        
        maxAge = SpecimenPatientAgeValidator.getHippaMaxAge(TimeUnits.HOURS);
        ageUnit = TimeUnits.HOURS;
        assertTrue(validator.isValid(createTestSpecimen(maxAge, ageUnit)));
        assertTrue(validator.isValid(createTestSpecimen(maxAge - 1, ageUnit)));
        assertFalse(validator.isValid(createTestSpecimen(maxAge + 1, ageUnit))); 
    }

}
