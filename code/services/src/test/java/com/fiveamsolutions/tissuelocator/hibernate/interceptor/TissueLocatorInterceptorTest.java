/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.hibernate.interceptor;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

import java.util.Date;

import org.junit.Test;

import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsHibernateHelper;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.FileFieldPersistenceTest;
import com.fiveamsolutions.tissuelocator.data.IrbApprovalStatus;
import com.fiveamsolutions.tissuelocator.data.LobHolder;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.ShipmentPersistenceTest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestPersistenceTest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.extension.FileDynamicFieldDefinition;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author gvaughn
 *
 */
public class TissueLocatorInterceptorTest extends AbstractHibernateTestCase {
    
    /**
     * Tests orphan deletion for non-collection properties.
     */
    @Test
    public void testDeleteOrphans() {
        
        Shipment shipment = new ShipmentPersistenceTest().getValidObjects()[0];
        GenericServiceLocal<PersistentObject> genericService = 
            TissueLocatorRegistry.getServiceLocator().getGenericService();
        Long id = genericService.savePersistentObject(shipment);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = genericService.getPersistentObject(Shipment.class, id);
        Long lobId = shipment.getMtaAmendment().getLob().getId();
        assertNotNull(genericService.getPersistentObject(LobHolder.class, lobId));
        shipment.setMtaAmendment(new TissueLocatorFile(new byte[]{1}, "mtaAmendment", "contentType"));
        genericService.savePersistentObject(shipment);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = genericService.getPersistentObject(Shipment.class, id);
        assertNull(genericService.getPersistentObject(LobHolder.class, lobId));
        lobId = shipment.getMtaAmendment().getLob().getId();
        shipment.setMtaAmendment(null);
        genericService.savePersistentObject(shipment);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = genericService.getPersistentObject(Shipment.class, id);
        assertNull(genericService.getPersistentObject(LobHolder.class, lobId));
        
        SpecimenRequest request = new SpecimenRequestPersistenceTest().getValidObjects()[0];
        FileDynamicFieldDefinition def = new FileFieldPersistenceTest().getValidObjects()[1];
        
        lobId = (Long) TissueLocatorHibernateUtil.getHibernateHelper().
            getCurrentSession().save(new LobHolder(new byte[] {1}));
        request.getCustomProperties().put(def.getFieldName(), 
                TissueLocatorHibernateUtil.getHibernateHelper().getCurrentSession().load(LobHolder.class, lobId));
        request.getCustomProperties().put(def.getContentTypeFieldName(), "content type");
        request.getCustomProperties().put(def.getFileNameFieldName(), "file name");
        request.getStudy().setIrbApprovalLetter(new TissueLocatorFile(new byte[]{1}, "irbApproval", "text/plain"));
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        id = service.savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = service.getPersistentObject(SpecimenRequest.class, id);
        lobId = request.getStudy().getIrbApprovalLetter().getLob().getId();   
        
        assertNotNull(genericService.getPersistentObject(LobHolder.class, lobId));
        request.getStudy().setIrbApprovalLetter(new TissueLocatorFile(new byte[]{1}, "irbApproval", "text/plain"));
        service.savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNull(genericService.getPersistentObject(LobHolder.class, lobId));
        lobId = request.getStudy().getIrbApprovalLetter().getLob().getId();
        request.getStudy().setIrbApprovalLetter(null);
        service.savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertNull(genericService.getPersistentObject(LobHolder.class, lobId));
        
        request = service.getPersistentObject(SpecimenRequest.class, id);
        request.getStudy().setIrbApprovalExpirationDate(null);
        request.getStudy().setIrbApprovalNumber(null);
        request.getStudy().setIrbApprovalStatus(IrbApprovalStatus.EXEMPT);
        request.getStudy().setIrbExemptionLetter(new TissueLocatorFile(new byte[]{1}, "irbExemption", "text/plain"));
        service.savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = service.getPersistentObject(SpecimenRequest.class, id);
        lobId = request.getStudy().getIrbExemptionLetter().getLob().getId();
        assertNotNull(genericService.getPersistentObject(LobHolder.class, lobId));
        request.getStudy().setIrbExemptionLetter(new TissueLocatorFile(new byte[]{1}, "irbExemption", "text/plain"));
        service.savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertNull(genericService.getPersistentObject(LobHolder.class, lobId));
        request = service.getPersistentObject(SpecimenRequest.class, id);
        lobId = request.getStudy().getIrbExemptionLetter().getLob().getId();
        request.getStudy().setIrbExemptionLetter(null);
        request.getStudy().setIrbApprovalStatus(IrbApprovalStatus.NOT_APPROVED);
        service.savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertNull(genericService.getPersistentObject(LobHolder.class, lobId));
        
        request = service.getPersistentObject(SpecimenRequest.class, id);
        lobId = request.getInvestigator().getResume().getLob().getId();
        assertNotNull(genericService.getPersistentObject(LobHolder.class, lobId));
        request.getInvestigator().setResume(new TissueLocatorFile(new byte[]{1}, "irbExemption", "text/plain"));
        service.savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertNull(genericService.getPersistentObject(LobHolder.class, lobId));
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(request.getInvestigator().getResume());
        
        DynamicExtensionsHibernateHelper hh = TissueLocatorHibernateUtil.getHibernateHelper();
        def = new FileFieldPersistenceTest().getValidObjects()[1];
        def.addToConfiguration(hh.getConfiguration());

        lobId = (Long) TissueLocatorHibernateUtil.getHibernateHelper().
        getCurrentSession().save(new LobHolder(new byte[] {1}));
        request.getCustomProperties().put(def.getFieldName(), 
                TissueLocatorHibernateUtil.getHibernateHelper().getCurrentSession().load(LobHolder.class, lobId));
        request.getCustomProperties().put(def.getContentTypeFieldName(), "content type");
        request.getCustomProperties().put(def.getFileNameFieldName(), "file name");
        service.savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(genericService.getPersistentObject(LobHolder.class, lobId));
        Long oldLob = lobId;
        lobId = (Long) TissueLocatorHibernateUtil.getHibernateHelper().
        getCurrentSession().save(new LobHolder(new byte[] {1}));
        request.getCustomProperties().put(def.getFieldName(), 
                TissueLocatorHibernateUtil.getHibernateHelper().getCurrentSession().load(LobHolder.class, lobId));
        request.getCustomProperties().put(def.getContentTypeFieldName(), "content type");
        request.getCustomProperties().put(def.getFileNameFieldName(), "file name");
        request.setUpdatedDate(new Date());
        service.savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotSame(oldLob, request.getCustomProperty(def.getFieldName()));
    }
    
}
