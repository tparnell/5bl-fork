/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Assert;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorPropertyUtil;

/**
 * @author gvaughn
 *
 */
public class TissueLocatorPropertyUtilTest {

    /**
     * Test the getObjectProperty method.
     * @throws Exception on error.
     */
    @Test
    public void testGetObjectProperty() throws Exception {
        assertNull(TissueLocatorPropertyUtil.getObjectProperty(null, ""));
        Specimen testSpecimen = new Specimen();
        Institution testInstition = new Institution();
        testInstition.setId(1L);
        testSpecimen.setExternalIdAssigner(testInstition);
        final String testCustomPropertyName = "test";
        final String testCustomPropertyValue = "this is the test field value";
        testSpecimen.getCustomProperties().put(testCustomPropertyName, testCustomPropertyValue);
        assertNotNull(TissueLocatorPropertyUtil.getObjectProperty(testSpecimen, "externalIdAssigner"));
        assertEquals(testInstition.getId(), 
                TissueLocatorPropertyUtil.getObjectProperty(testSpecimen, "externalIdAssigner.id"));
        Object retrievedCustomPropertyValue = TissueLocatorPropertyUtil.getObjectProperty(testSpecimen, "test");
        assertNotNull(retrievedCustomPropertyValue);
        assertEquals(retrievedCustomPropertyValue, testCustomPropertyValue);
        testSpecimen.setExternalIdAssigner(null);
        assertNull(TissueLocatorPropertyUtil.getObjectProperty(testSpecimen, "externalIdAssigner"));
        assertNull(TissueLocatorPropertyUtil.getObjectProperty(testSpecimen, "externalIdAssigner.id"));
        try {
            assertNull(TissueLocatorPropertyUtil.getObjectProperty(testSpecimen, "externalIdAssigner.jd"));
            testSpecimen.setExternalIdAssigner(testInstition);
            TissueLocatorPropertyUtil.getObjectProperty(testSpecimen, "externalIdAssigner.jd");
            Assert.fail();
        } catch (Exception e) {
            assertNotNull(e);
        }
    }
}
