create table generic_field_config (
    id int8 not null,
    field_name varchar(255) not null unique,
    field_display_name varchar(255) not null,
    search_field_name varchar(255) unique,
    search_field_display_name varchar(255),
    required_role_id int8,
    primary key (id)
);
alter table generic_field_config add constraint generic_field_config_role_fk foreign key (required_role_id) references ApplicationRole;

-- core fields
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'availableQuantity', 'minimumAvailableQuantity', 'Available Quantity', 'Minimum Available Quantity', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'availableQuantityUnits', 'availableQuantityUnits', 'Quantity units', 'Quantity units', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'collectionYear', 'collectionYear', 'Collection Year', 'Collection Year (yyyy)', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'externalId', 'externalId', 'External ID (Record ID)', 'External ID (Record ID)', (select id from applicationrole where name = 'externalIdViewer'));
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'externalIdAssigner.name', 'externalIdAssigner.id', 'Institution', 'Institution', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'id', 'id', 'Biospecimen ID', 'Biospecimen ID', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'minimumPrice', 'minimumPrice', 'Fee', 'Fee', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'normalSample', 'normalSample', 'Normal Sample', 'Normal Sample', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'participant.ethnicity', 'participant.ethnicity', 'Ethnicity', 'Ethnicity', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'participant.gender', 'participant.gender', 'Gender', 'Sex', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'participant.races', 'participant.races', 'Race', 'Race', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'pathologicalCharacteristic', 'pathologicalCharacteristic', 'Disease', 'Disease', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'patientAgeAtCollection', 'patientAgeAtCollection', 'Collection Age', 'Patient''s Age at Collection', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'patientAgeAtCollectionUnits', 'patientAgeAtCollectionUnits', 'Patient age units', 'Patient age units', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'protocol.name', 'protocol.name', 'Collection Protocol', 'Collection Protocol', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'specimenType', 'specimenType.name', 'Type', 'Type', null);
insert into generic_field_config (id, field_name, search_field_name, field_display_name, search_field_display_name, required_role_id) values ((select nextval('hibernate_sequence')), 'specimenType.specimenClass', 'specimenType.specimenClass', 'Class', 'Class', null);

-- update related tables
alter table search_result_field_display_setting add column generic_field_config_id int8;
alter table search_result_field_display_setting add constraint result_display_setting_field_config_fk foreign key (generic_field_config_id) references generic_field_config;

alter table ui_search_category_field add column generic_field_config_id int8;
alter table ui_search_category_field add constraint search_category_field_config_fk foreign key (generic_field_config_id) references generic_field_config;

alter table ui_dynamic_category_field add column generic_field_config_id int8;
alter table ui_dynamic_category_field add constraint dynamic_category_field_config_fk foreign key (generic_field_config_id) references generic_field_config;

alter table autocomplete_config add column generic_field_config_id int8;
alter table autocomplete_config add constraint autocomplete_config_field_config_fk foreign key (generic_field_config_id) references generic_field_config;

alter table checkbox_config add column generic_field_config_id int8;
alter table checkbox_config add constraint checkbox_config_field_config_fk foreign key (generic_field_config_id) references generic_field_config;

alter table range_config add column generic_field_config_id int8;
alter table range_config add constraint range_config_field_config_fk foreign key (generic_field_config_id) references generic_field_config;

alter table select_config add column generic_field_config_id int8;
alter table select_config add constraint select_config_field_config_fk foreign key (generic_field_config_id) references generic_field_config;
alter table select_config add column multi_select_value_type varchar(255);
alter table select_config add column multi_select_collection bool;

alter table select_dynamic_extension_config add column generic_field_config_id int8;
alter table select_dynamic_extension_config add constraint select_dynamic_extension_config_field_config_fk foreign key (generic_field_config_id) references generic_field_config;
alter table select_dynamic_extension_config add column multi_select_value_type varchar(255);

alter table text_config add column generic_field_config_id int8;
alter table text_config add constraint text_config_field_config_fk foreign key (generic_field_config_id) references generic_field_config;