create table question (
    id int8 not null,
    requestor_id int8 not null,
    investigator_id int8 not null,
    resume_content_type varchar(255) not null,
    resume_name varchar(255) not null,
    resume_lob_id int8 not null,
    protocol_document_content_type varchar(255),
    protocol_document_name varchar(255),
    protocol_document_lob_id int8,
    question_text varchar(3999) not null,
    status varchar(255) not null,
    created_date timestamp not null,
    last_updated_date timestamp not null,
    primary key (id)
);
alter table question add constraint FKBA823BE6B55088D0 foreign key (resume_lob_id) references lob_holder;
alter table question add constraint question_investigator_fk foreign key (investigator_id) references person;
alter table question add constraint FKBA823BE66C4BEC7B foreign key (protocol_document_lob_id) references lob_holder;
alter table question add constraint question_requestor_fk foreign key (requestor_id) references tissue_locator_user;
create index question_status_idx on question (status);
create index question_requestor_idx on question (requestor_id);
create index question_investigator_idx on question (investigator_id);

create table question_status_transition (
    id int8 not null,
    new_status varchar(255) not null,
    previous_status varchar(255) not null,
    system_transition_date timestamp not null,
    transition_date timestamp not null,
    primary key (id)
);

create table question_status_transition_history (
    question_id int8 not null,
    status_transition_id int8 not null,
    primary key (question_id, status_transition_id)
);
alter table question_status_transition_history add constraint TRANSITION_QUESTION_FK foreign key (status_transition_id) references question_status_transition;
alter table question_status_transition_history add constraint QUESTION_TRANSITION_FK foreign key (question_id) references question;

create table question_response (
    id int8 not null,
    question_id int8 not null,
    institution_id int8 not null,
    responder_id int8,
    response varchar(3999),
    status varchar(255) not null,
    created_date timestamp not null,
    last_updated_date timestamp not null,
    primary key (id),
    unique (question_id, institution_id)
);
alter table question_response add constraint question_response_responder_fk foreign key (responder_id) references tissue_locator_user;
alter table question_response add constraint question_response_institution_fk foreign key (institution_id) references institution;
alter table question_response add constraint question_response_question_fk foreign key (question_id) references question;
create index question_response_institution_idx on question_response (institution_id);
create index question_response_question_idx on question_response (question_id);
create index question_response_status_idx on question_response (status);
create index question_response_responder_idx on question_response (responder_id);

create table question_response_status_transition (
    id int8 not null,
    new_status varchar(255) not null,
    previous_status varchar(255) not null,
    system_transition_date timestamp not null,
    transition_date timestamp not null,
    primary key (id)
);

create table question_response_status_transition_history (
    response_id int8 not null,
    status_transition_id int8 not null,
    primary key (response_id, status_transition_id)
);
alter table question_response_status_transition_history add constraint TRANSITION_QUESTION_RESPONSE_FK foreign key (status_transition_id) references question_response_status_transition;
alter table question_response_status_transition_history add constraint QUESTION_RESPONSE_TRANSITION_FK foreign key (response_id) references question_response;

insert into applicationrole (id, name) values (32, 'questionAsker');
insert into applicationrole (id, name) values (33, 'questionResponder');
insert into applicationrole (id, name) values (34, 'questionAdministrator');
