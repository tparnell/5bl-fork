alter table shipping_information add column billing_recipient_id int8;
alter table shipping_information add constraint shipping_information_billing_recipient_fk foreign key (billing_recipient_id) references person;

alter table shipment add column billing_recipient_id int8;
alter table shipment add constraint shipment_billing_recipient_fk foreign key (billing_recipient_id) references person;

insert into application_setting (id, name, value) values ((select max(id) + 1 from application_setting), 'display_shipment_billing_recipient', 'false');