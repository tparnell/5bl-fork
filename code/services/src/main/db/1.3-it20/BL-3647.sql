alter table support_letter_request add column intended_submission_date date;

insert into field_help_config (id, entity_class_name, field_name, help_text) values (
    nextval('hibernate_sequence'),
    'com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest',
    'intendedSubmissionDate',
    'Date letter is needed for institution or for submission to funding source.'
);
