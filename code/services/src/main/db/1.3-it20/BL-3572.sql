alter table material_transfer_agreement add column version_date date;
update material_transfer_agreement set version_date = '2011-08-28' where id = (select max(id) from material_transfer_agreement);
update material_transfer_agreement set version_date = '2011-08-01' where version_date is null;
alter table material_transfer_agreement drop column version;
alter table material_transfer_agreement rename column version_date to version;
alter table material_transfer_agreement alter column version set not null;
create index mta_version_idx on material_transfer_agreement (version);