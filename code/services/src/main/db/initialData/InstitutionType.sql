insert into institution_type (id, name) values (1, 'Academic');
insert into institution_type (id, name) values (2, 'For Profit');
insert into institution_type (id, name) values (3, 'Government');
insert into institution_type (id, name) values (4, 'Individual');
insert into institution_type (id, name) values (5, 'Non-profit');
