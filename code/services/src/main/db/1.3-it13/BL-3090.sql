-- Separate the ui_category_field table into 2 distinct tables. Migration in client-specific scripts.

create table ui_dynamic_category_field (
    id                          INT8            NOT NULL,
    field_name                  VARCHAR(255)    NOT NULL,
    ui_dynamic_category_id      INT8            NOT NULL,
    field_type                  varchar(255)    NOT NULL,
    display_name                varchar(255),
    displayable_decider_class   varchar(255),
    PRIMARY KEY (id)
);

create table ui_search_category_field (
    id                          INT8            NOT NULL,
    field_name                  VARCHAR(255)    NOT NULL,
    ui_search_category_id       INT8            NOT NULL,
    PRIMARY KEY (id)
);

alter table ui_dynamic_category_field add constraint dynamic_field_category_fk foreign key (ui_dynamic_category_id) references ui_dynamic_field_category;
alter table ui_search_category_field add constraint search_field_category_fk foreign key (ui_search_category_id) references ui_search_field_category;