alter table specimen add column consent_withdrawn bool not null default false;
alter table specimen add column consent_withdrawn_date date;
alter table specimen add column returned_to_source_institution_date date;