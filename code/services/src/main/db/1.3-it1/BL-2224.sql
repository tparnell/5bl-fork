
alter table select_config add column show_empty_option bool;
alter table select_dynamic_extension_config add column show_empty_option bool;
update select_config set show_empty_option = true;
update select_dynamic_extension_config set show_empty_option = true;