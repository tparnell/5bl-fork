alter table aggregate_specimen_request_line_item add column vote_id int8;
create index aggregate_line_item_vote_idx on aggregate_specimen_request_line_item (vote_id);
alter table aggregate_specimen_request_line_item add constraint aggregate_line_item_vote_fk foreign key (vote_id) references specimen_request_review_vote;

insert into applicationrole (id, name) values (28, 'lineItemReviewVoter');
