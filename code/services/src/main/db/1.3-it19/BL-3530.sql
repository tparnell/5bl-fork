alter table support_letter_request add column next_reminder_date timestamp;

insert into application_setting (id, name, value) values ((select max(id) + 1 from application_setting), 'support_letter_request_review_grace_period', '0');