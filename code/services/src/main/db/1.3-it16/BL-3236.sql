create table funding_source (
    id int8 not null,
    active bool not null,
    name varchar(254) not null,
    value varchar(254),
    primary key (id)
);
CREATE UNIQUE INDEX funding_source_name_lower_unique_key on funding_source(lower(name));
CREATE INDEX funding_source_active_idx ON funding_source(active);
CREATE INDEX funding_source_name_idx ON funding_source(lower(name));

create table study_funding_source (
    study_id int8 not null,
    funding_source_id int8 not null,
    primary key (study_id, funding_source_id)
);
alter table study_funding_source add constraint FUNDING_SOURCE_STUDY_FK foreign key (funding_source_id) references funding_source;
alter table study_funding_source add constraint STUDY_FUNDING_SOURCE_FK foreign key (study_id) references study;
