CREATE OR REPLACE FUNCTION convert_bool_to_string(bool) RETURNS varchar(255) as $$ begin CASE WHEN $1=false THEN return 'NOT_FUNDED'; WHEN $1=true THEN return 'FUNDED'; END CASE; END $$ language 'plpgsql';
ALTER TABLE study ALTER COLUMN currently_funded TYPE varchar(255) USING convert_bool_to_string(currently_funded);
ALTER TABLE study ALTER COLUMN currently_funded DROP default;
ALTER TABLE study ALTER COLUMN currently_funded DROP not null;
INSERT into application_setting (id, name, value) values (22, 'minimum_required_funding_status', 'NOT_FUNDED');
drop function convert_bool_to_string(bool);
alter table study rename column currently_funded to funding_status;
