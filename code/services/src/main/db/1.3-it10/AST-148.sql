create table specimen_search (id int8 not null, description varchar(254), name varchar(254) not null, criteriaData_id int8 not null, owner_id int8, primary key (id));
alter table specimen_search add constraint FKE43B66BF1EC72FF4 foreign key (criteriaData_id) references lob_holder;
alter table specimen_search add constraint search_owner_fk foreign key (owner_id) references tissue_locator_user;
