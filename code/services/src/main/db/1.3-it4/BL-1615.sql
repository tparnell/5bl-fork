alter table tissue_locator_user add column creation_date timestamp default now();
update tissue_locator_user set creation_date = updateddate where creation_date is null;
alter table tissue_locator_user alter column creation_date set not null;
insert into application_setting (id, name, value) values (20, 'new_request_count_condition', '1 = 1');
insert into applicationrole (id, name) values (27, 'reportViewer');
