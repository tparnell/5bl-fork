create or replace function create_irb_approval_blob_for_non_human_subject_studies() returns bigint as $$ declare lid bigint;approvalLetterLob oid; begin select lo_creat(-1) into approvalLetterLob;lid := nextval('hibernate_sequence'); insert into lob_holder (id, data) values (lid,approvalLetterLob); return lid; end; $$ language plpgsql;

update study set
irb_approval_status='APPROVED',
irb_approval_expiration_date=current_date,
irb_approval_content_type='application/text',
irb_approval_name='Approval Letter.txt',
irb_approval_number='IRB1234',
irb_name='University IRB',
irb_registration_number='1212',
irb_approval_lob_id=create_irb_approval_blob_for_non_human_subject_studies()
where irb_approval_status='NON_HUMAN_SUBJECT';

drop function create_irb_approval_blob_for_non_human_subject_studies();
