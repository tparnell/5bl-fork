alter table specimen_search drop constraint FKE43B66BF1EC72FF4;
alter table specimen_search drop constraint search_owner_fk;
alter table specimen_search rename column criteriaData_id to criteria_data_id;
alter table specimen_search rename to saved_specimen_search;
alter table saved_specimen_search add constraint saved_specimen_search_lob_holder_fk foreign key (criteria_data_id) references lob_holder;
alter table saved_specimen_search add constraint saved_specimen_search_tissue_locator_user_fk foreign key (owner_id) references tissue_locator_user;
create unique index saved_specimen_search_tissue_locator_user_key on saved_specimen_search(lower(name), owner_id);
