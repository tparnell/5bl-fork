create table status_transition (id int8 not null, previous_status varchar(255) not null, new_status varchar(255) not null, transition_date timestamp not null, system_transition_date timestamp not null, primary key (id));
create table specimen_status_transition_history (specimen_id int8 not null, status_transition_id int8 not null, primary key (specimen_id, status_transition_id));
alter table specimen_status_transition_history add constraint SPECIMEN_TRANSITION_FK foreign key (specimen_id) references specimen;
alter table specimen_status_transition_history add constraint TRANSITION_SPECIMEN_FK foreign key (status_transition_id) references status_transition;

alter table specimen add column created_date timestamp not null default now();
