ALTER TABLE specimen_type DROP CONSTRAINT specimen_type_name_key;
CREATE UNIQUE INDEX specimen_type_name_lower_uinque_key on specimen_type (lower(name));

ALTER TABLE additional_pathologic_finding DROP CONSTRAINT additional_pathologic_finding_name_key;
CREATE UNIQUE INDEX additional_pathologic_finding_name_lower_unique_key on additional_pathologic_finding (lower(name));
