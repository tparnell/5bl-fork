CREATE TABLE ui_dynamic_field_category (
    id                  INT8            NOT NULL    PRIMARY KEY,
    category_name       VARCHAR(255)    NOT NULL,
    entity_class_name   VARCHAR(255)    NOT NULL,
    viewable            BOOLEAN         DEFAULT TRUE
);

CREATE TABLE ui_category_field (
    id              INT8            NOT NULL,
    field_name      VARCHAR(255)    NOT NULL,
    ui_category_id  INT8            NOT NULL,
    PRIMARY KEY (id, ui_category_id)
);
