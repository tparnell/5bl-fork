CREATE TABLE ui_search_field_category (
    id                  INT8            NOT NULL    PRIMARY KEY,
    category_name       VARCHAR(255)    NOT NULL,
    action_class_name   VARCHAR(255)    NOT NULL,
    search_set_type     VARCHAR(255)    NOT NULL    DEFAULT 'ADVANCED',
    viewable            BOOLEAN         NOT NULL    DEFAULT TRUE
);
