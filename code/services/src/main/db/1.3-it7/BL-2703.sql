create table field_help_config(
    id int8 not null, 
    entity_class_name varchar(255) not null, 
    field_name varchar(255) not null, 
    help_text varchar(1000), 
    primary key(id));
create table help_text_link(
    id int8 not null, 
    link_text varchar(255) not null, 
    link_href varchar(255) not null, 
    primary key (id));
create table help_text_links(
    field_help_config_id int8 not null, 
    help_text_link_id int8 not null, 
    primary key(field_help_config_id, help_text_link_id));
alter table help_text_links add constraint HELP_CONFIG_ID foreign key (field_help_config_id) references field_help_config;
alter table help_text_links add constraint HELP_LINK_ID foreign key (help_text_link_id) references help_text_link;