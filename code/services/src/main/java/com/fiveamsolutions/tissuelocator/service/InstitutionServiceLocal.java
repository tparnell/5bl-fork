/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Vote;

/**
 * @author smiller
 *
 */
@Local
public interface InstitutionServiceLocal extends GenericServiceLocal<Institution> {

    /**
     * Get the institutions that have not reviewed a request the longest.
     * @param numberReviewers the number of institutions to retrieve
     * @return a list of institutions that have not reviewed a request the longest
     */
    List<Institution> getReviewers(int numberReviewers);

    /**
     * return the instiutions that are consortium members.
     * @return the members.
     */
    List<Institution> getConsortiumMembers();

    /**
     * API to get an Institution.
     * @param institutionId id of the Institution
     * @return Institution object
     */
    Institution getInstitution(Long institutionId);
    
    /**
     * Retrieve an institution by name.
     * @param name Institution name.
     * @return The institution with the given name, or null if none exists.
     */
    Institution getInstitutionByName(String name);

    /**
     * For each consortium member institution and possible vote, get the number of requests
     * for which the institution has submitted a scientific review with the given vote within
     * a date range.  The search will include both end points of the date range.
     * @param startDate the start date of the date range
     * @param endDate the end date of the date range
     * @return a two level map of counts.  The outer map separates the results by institution.
     * The inner map separates the results for an institution by vote.
     */
    Map<String, Map<Vote, Long>> getScientificReviewVoteCounts(Date startDate, Date endDate);

    /**
     * For each consortium member institution and possible vote, get the number of requests
     * for which the institution has submitted an institutional review with the given vote
     * within a date range.  The search will include both end points of the date range.
     * @param startDate the start date of the date range
     * @param endDate the end date of the date range
     * @return a two level map of counts.  The outer map separates the results by institution.
     * The inner map separates the results for an institution by vote.
     */
    Map<String, Map<Vote, Long>> getInstitutionalReviewVoteCounts(Date startDate, Date endDate);
    
    /**
     * Returns the comments for each denied scientific review vote cast within the given date
     * range for the given institution.
     * @param startDate the start date of the date range.
     * @param endDate the end date of the date range.
     * @param institutionName name of the institution.
     * @return A mapping of request id to a list of rejection comments.
     */
    Map<Long, List<String>> getScientificReviewRejectionComments(Date startDate, Date endDate, String institutionName);

    /**
     * Save a new institution.
     * @param institution the Institution to save
     * @return the id of the new institution
     */
    Long saveNewInstitution(Institution institution);
}
