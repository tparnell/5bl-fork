/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.tissuelocator.data.Address;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.ShipmentStatus;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.util.Email;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.RequestProcessingConfiguration;
import com.fiveamsolutions.tissuelocator.util.RequestProcessor;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.google.inject.Inject;

/**
 * @author ddasgupta
 */
@Stateless
@SuppressWarnings({ "PMD.TooManyMethods", "PMD.ExcessiveClassLength", "PMD.AvoidDuplicateLiterals" })
public class SpecimenRequestServiceBean extends GenericServiceBean<SpecimenRequest> implements
        SpecimenRequestServiceLocal {

    /**
     * Exception message when save on a request occurs without prospective collection notes or selected specimens.
     */
    public static final String SPECIMEN_OR_NOTES_NOT_SELECTED =
        "Request cannot be saved without prospective collection notes or selected specimens.";

    /**
     * Exception message when save on a request occurs without selected specimens.
     */
    public static final String SPECIMEN_NOT_SELECTED =
        "Request cannot be saved without selected specimens.";

    /**
     * Exception message when save on specimen occurs with status other than available.
     */
    public static final String SPECIMEN_NOT_AVAILABLE = "Specimen can not be saved with status other than available.";

    private static final Logger LOG = Logger.getLogger(SpecimenRequestServiceBean.class);
    private static final String REPLACEMENT_ONE = "{1}";
    private static final String REPLACEMENT_TWO = "{2}";
    private static final String REPLACEMENT_THREE = "{3}";
    private static final String REPLACEMENT_FOUR = "{4}";

    private final EmailHelper emailHelper = new EmailHelper();

    @EJB
    private ShipmentServiceLocal shipmentService;

    @EJB
    private InstitutionServiceLocal institutionService;

    private ApplicationSettingServiceLocal applicationSettingService;

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void processPendingRequest(SpecimenRequest request, RequestProcessingConfiguration config)
        throws MessagingException {

        RequestProcessor processor = getApplicationSettingService().getReviewProcess().getRequestProcessor();
        processor.setInstitutionService(getInstitutionService());
        processor.setUserService(getUserService());
        processor.setApplicationSettingService(getApplicationSettingService());
        Collection<Email> emails = processor.processPendingRequest(request, config);
        if (RequestStatus.PENDING_FINAL_DECISION.equals(request.getStatus())) {
            savePersistentObject(request);
        }
        for (Email email : emails) {
            sendEmail(email);
        }
    }

    private void sendEmail(Email message) throws MessagingException {
        Email email = new Email();
        email.setRecipient(message.getRecipient());
        String idString = String.valueOf(message.getRequest().getId());
        String textReplace = StringUtils.defaultString(message.getTextReplacement());
        String htmlReplace = StringUtils.defaultString(message.getHtmlReplacement());
        String assignedUserReplace = StringUtils.defaultString(message.getAssignedUser());
        String leadReviewerReplace = StringUtils.defaultString(message.getLeadReviewer());
        email.setSubject(message.getSubject().replace(REPLACEMENT_ONE, idString)
                    .replace(REPLACEMENT_TWO, textReplace));
        email.setHtml(message.getHtml().replace(REPLACEMENT_ONE, idString).replace(REPLACEMENT_TWO, htmlReplace)
                    .replace(REPLACEMENT_THREE, assignedUserReplace).replace(REPLACEMENT_FOUR, leadReviewerReplace));
        email.setText(message.getText().replace(REPLACEMENT_ONE, idString).replace(REPLACEMENT_TWO, textReplace)
                    .replace(REPLACEMENT_THREE, assignedUserReplace).replace(REPLACEMENT_FOUR, leadReviewerReplace));
        email.send();
    }

    private void updateLineItemStatus(SpecimenRequest request) {
        if (RequestStatus.DENIED.equals(request.getStatus())) {
            // set the availability to true if the request is denied
            setAllSpecimensToAvailable(request);
        } else if (RequestStatus.PARTIALLY_APPROVED.equals(request.getStatus())) {
            // for request that are approved, but had some inst veto's exercised we need to free up the
            // denied line items
            setVetoedRequestsToAvailable(request);
        }
    }

    private void setAllSpecimensToAvailable(SpecimenRequest request) {
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            lineItem.getSpecimen().setStatus(SpecimenStatus.AVAILABLE);
        }
    }

    private void setVetoedRequestsToAvailable(SpecimenRequest request) {
        // create set of institutions that failed review
        Set<Institution> failedReview = getFailedInstitutionalReviews(request);

        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            if (failedReview.contains(lineItem.getSpecimen().getExternalIdAssigner())) {
                lineItem.getSpecimen().setStatus(SpecimenStatus.AVAILABLE);
            }
        }
    }

    private Map<Institution, Long> createShipments(SpecimenRequest request) throws MessagingException {
        Map<Institution, Long> shipmentIds = new HashMap<Institution, Long>();
        if (!(RequestStatus.APPROVED.equals(request.getStatus())
                || RequestStatus.PARTIALLY_APPROVED.equals(request.getStatus()))) {
            return shipmentIds;
        }

        // sort the line items by institution
        Map<Institution, Set<SpecimenRequestLineItem>> lineItemMap = getLineItemMap(request);
        Map<Institution, Set<AggregateSpecimenRequestLineItem>> aggregateLineItemMap =
            getAggregateLineItemMap(request);

        // create set of institutions that failed review
        Set<Institution> failedReview = getFailedInstitutionalReviews(request);

        Set<Institution> institutions = new HashSet<Institution>();
        institutions.addAll(lineItemMap.keySet());
        institutions.addAll(aggregateLineItemMap.keySet());

        //create a shipment for every institution;
        for (Institution inst : institutions) {
            if (!failedReview.contains(inst)) {
                Long shipmentId = createShipment(request, inst, lineItemMap.get(inst),
                        aggregateLineItemMap.get(inst));
                removeLineItems(request, inst, lineItemMap, aggregateLineItemMap);
                shipmentIds.put(inst, shipmentId);
            }
        }
        return shipmentIds;
    }

    private void removeLineItems(SpecimenRequest request, Institution institution,
            Map<Institution, Set<SpecimenRequestLineItem>> lineItemMap, Map<Institution,
            Set<AggregateSpecimenRequestLineItem>> aggregateLineItemMap) {
        if (lineItemMap.get(institution) != null) {
            request.getLineItems().removeAll(lineItemMap.get(institution));
        }
        if (aggregateLineItemMap.get(institution) != null) {
            request.getAggregateLineItems().removeAll(aggregateLineItemMap.get(institution));
        }
    }

    private Set<Institution> getFailedInstitutionalReviews(SpecimenRequest request) {
        Set<Institution> failedReview = new HashSet<Institution>();
        for (SpecimenRequestReviewVote vote : request.getInstitutionalReviews()) {
            if (Vote.DENY.equals(vote.getVote())) {
                failedReview.add(vote.getInstitution());
            }
        }
        return failedReview;
    }

    /**
     * @param request the request
     * @return a map of institutions to requested line items from that institution.
     */
    protected Map<Institution, Set<SpecimenRequestLineItem>> getLineItemMap(SpecimenRequest request) {
        Map<Institution, Set<SpecimenRequestLineItem>> lineItemMap =
            new HashMap<Institution, Set<SpecimenRequestLineItem>>();
        for (SpecimenRequestLineItem item : request.getLineItems()) {
            Set<SpecimenRequestLineItem> items = lineItemMap.get(item.getSpecimen().getExternalIdAssigner());
            if (items == null) {
                items = new HashSet<SpecimenRequestLineItem>();
                lineItemMap.put(item.getSpecimen().getExternalIdAssigner(), items);
            }
            items.add(item);
        }
        return lineItemMap;
    }

    /**
     * @param request specimen request.
     * @return map of institutions to aggregate line items for that institution.
     */
    protected Map<Institution, Set<AggregateSpecimenRequestLineItem>> getAggregateLineItemMap(SpecimenRequest request) {
        Map<Institution, Set<AggregateSpecimenRequestLineItem>> lineItemMap =
            new HashMap<Institution, Set<AggregateSpecimenRequestLineItem>>();
        for (AggregateSpecimenRequestLineItem item : request.getAggregateLineItems()) {
            Set<AggregateSpecimenRequestLineItem> items = lineItemMap.get(item.getInstitution());
            if (items == null) {
                items = new HashSet<AggregateSpecimenRequestLineItem>();
                lineItemMap.put(item.getInstitution(), items);
            }
            items.add(item);
        }
        return lineItemMap;
    }

    /**
     * create a shipment.
     * @param request the request to create a shipment for
     * @param i the shipping institutions
     * @param lis the line items to be included in the shipment
     * @param aggregateLineItems aggregate line items to be included in the shipment
     * @return a shipment for the given request from the given institution with the given line items
     */
    protected Long createShipment(SpecimenRequest request, Institution i, Set<SpecimenRequestLineItem> lis,
            Set<AggregateSpecimenRequestLineItem> aggregateLineItems) {
        Shipment s = new Shipment();
        s.setSendingInstitution(i);
        if (lis != null) {
            s.getLineItems().addAll(lis);
            for (SpecimenRequestLineItem li : s.getLineItems()) {
                li.getSpecimen().setStatus(SpecimenStatus.PENDING_SHIPMENT);
            }
        }
        if (aggregateLineItems != null) {
            s.getAggregateLineItems().addAll(aggregateLineItems);
        }

        s.setRequest(request);
        s.setStatus(ShipmentStatus.PENDING);
        s.setUpdatedDate(new Date());
        
        s.setRecipient(copyRecipient(request.getShipment().getRecipient()));
        s.setBillingRecipient(copyRecipient(request.getShipment().getBillingRecipient()));
        s.setShippingInstructions(request.getShipment().getInstructions());
        request.getOrders().add(s);
        return getShipmentService().savePersistentObject(s);
    }
    
    private Person copyRecipient(Person src) {
        if (src == null) {
            return null;
        }
        Person recipient = new Person();
        recipient.setFirstName(src.getFirstName());
        recipient.setLastName(src.getLastName());
        recipient.setEmail(src.getEmail());
        recipient.setOrganization(src.getOrganization());
        recipient.setAddress(new Address());
        Address srcAddress = src.getAddress();
        recipient.getAddress().setCity(srcAddress.getCity());
        recipient.getAddress().setCountry(srcAddress.getCountry());
        recipient.getAddress().setFax(srcAddress.getFax());
        recipient.getAddress().setLine1(srcAddress.getLine1());
        recipient.getAddress().setLine2(srcAddress.getLine2());
        recipient.getAddress().setPhone(srcAddress.getPhone());
        recipient.getAddress().setPhoneExtension(srcAddress.getPhoneExtension());
        recipient.getAddress().setState(srcAddress.getState());
        recipient.getAddress().setZip(srcAddress.getZip());
        return recipient;
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void processPendingDecisionRequest(SpecimenRequest request, RequestProcessingConfiguration config)
        throws MessagingException {
        RequestProcessor processor = getApplicationSettingService().getReviewProcess().getRequestProcessor();
        processor.setInstitutionService(getInstitutionService());
        processor.setUserService(getUserService());
        processor.setApplicationSettingService(getApplicationSettingService());
        Collection<Email> emails = processor.processPendingDecisionRequest(request, config);
        for (Email email : emails) {
            sendEmail(email);
        }
    }

    /**
     * {@inheritDoc}
     */
    public Long saveDraftRequest(SpecimenRequest request) {
        verifySpecimensAvailable(request);
        request.setFinalVote(null);
        // If specimens have been added to a request revision, they
        // must be marked as under review immediately to prevent
        // multiple requests
        if (request.getStatus() == RequestStatus.PENDING_REVISION) {
            for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
                lineItem.getSpecimen().setStatus(SpecimenStatus.UNDER_REVIEW);
            }
        }
        savePersistentObject(request);
        if (request.getPreviousStatus() == null) {
            request.setPreviousStatus(RequestStatus.DRAFT);
        }
        return request.getId();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings({ "PMD.ExcessiveParameterList", "PMD.ExcessiveMethodLength" })
    public Long saveRequest(SpecimenRequest request, int votingPeriod, int reviewPeriod,
            int reviewerAssignmentPeriod, int numReviewers) throws MessagingException {
        verifySpecimensSelected(request);
        verifySpecimensAvailable(request);
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            lineItem.getSpecimen().setStatus(SpecimenStatus.UNDER_REVIEW);
        }
        request.setStatus(RequestStatus.PENDING);
        request.setRequestedDate(new Date());
        request.setFinalVote(null);

        boolean isNew = request.getId() == null || RequestStatus.DRAFT.equals(request.getPreviousStatus());
        Long requestId = savePersistentObject(request);
        SpecimenRequest requestAfterSave = getPersistentObject(SpecimenRequest.class, requestId);

        String idString = requestId.toString();
        SimpleDateFormat sdf = emailHelper.getDefaultDateFormat();
        String reviewDeadline = sdf.format(addDaysToUpdatedDate(requestAfterSave, reviewPeriod));
        String votingDeadline = sdf.format(addDaysToUpdatedDate(requestAfterSave, votingPeriod));
        String reviewerAssigmentDeadline = sdf.format(addDaysToUpdatedDate(requestAfterSave, reviewerAssignmentPeriod));
        String format = emailHelper.getResourceBundle().getString("default.datetime.format");
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(format, Locale.getDefault());
        String currentTime = dateTimeFormat.format(new Date());
        String[] args = {idString, votingDeadline, reviewDeadline, reviewerAssigmentDeadline, currentTime,
                REPLACEMENT_TWO, REPLACEMENT_THREE, REPLACEMENT_FOUR};

        Email emailBase = emailHelper.getEmail("specimenRequest.email.newRequest", args);
        Email leadReviewerContent = emailHelper.getEmail("specimenRequest.email.leadReview", args);
        Email instApprovalContent = emailHelper.getEmail("specimenRequest.email.institutionalReview", args);
        Email reviewerNotAssignedBase = new Email();
        Email revisionEmail = new Email();
        Email investigatorEmail = null;

        if (!isNew) {
            emailBase = emailHelper.getEmail("specimenRequest.email.revisedRequest", args);
            leadReviewerContent = emailHelper.getEmail("specimenRequest.email.leadReviewRevision", args);
            instApprovalContent = emailHelper.getEmail("specimenRequest.email.institutionalReviewRevision", args);
            reviewerNotAssignedBase = emailHelper.getEmail("specimenRequest.email.revisedRequestNoReviewer", args);
            revisionEmail = emailHelper.getEmail("specimenRequest.email.revision", args);
        } else {
            String[] investigatorEmailArgs = {idString, request.getInvestigator().getFirstName(),
                    request.getInvestigator().getLastName()};
            investigatorEmail = emailHelper.getEmail("specimenRequest.email.newRequest.investigatorNotification",
                    investigatorEmailArgs);
            investigatorEmail.setRecipient(request.getInvestigator().getEmail());
            investigatorEmail.setRequest(requestAfterSave);
        }

        RequestProcessor processor = getApplicationSettingService().getReviewProcess().getRequestProcessor();
        processor.setInstitutionService(getInstitutionService());
        processor.setUserService(getUserService());
        processor.setApplicationSettingService(getApplicationSettingService());
        Collection<Email> emails = processor.initializeRequest(requestAfterSave, emailBase, leadReviewerContent,
                instApprovalContent, revisionEmail, reviewerNotAssignedBase, numReviewers, isNew);
        
        if (null != investigatorEmail) {
            emails.add(investigatorEmail);
        }
        
        savePersistentObject(requestAfterSave);

        for (Email email : emails) {
            sendEmail(email);
        }
        sendMtaAdminEmails(idString, isNew);

        return requestAfterSave.getId();
    }

    private void verifySpecimensSelected(SpecimenRequest request) {
        if (request.getLineItems().isEmpty() && request.getAggregateLineItems().isEmpty()) {
            if (applicationSettingService.isDisplayProspectiveCollection()) {
                if (StringUtils.isBlank(request.getProspectiveCollectionNotes())) {
                    throw new IllegalStateException(SPECIMEN_OR_NOTES_NOT_SELECTED);
                }
            } else {
                throw new IllegalStateException(SPECIMEN_NOT_SELECTED);
            }
        }
    }

    private void verifySpecimensAvailable(SpecimenRequest request) {
        Set<SpecimenRequestLineItem> itemsToRemove = new HashSet<SpecimenRequestLineItem>();
        // Only check specimen availability for new and draft requests - specimens
        // will have been marked as unavailable for revised requests
        if (request.getId() == null || request.getPreviousStatus() == RequestStatus.DRAFT) {
            for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
                Specimen currSpecimen = lineItem.getSpecimen();
                Specimen s = TissueLocatorRegistry.getServiceLocator()
                        .getSpecimenService().getPersistentObject(Specimen.class,
                                currSpecimen.getId());
                lineItem.setSpecimen(s);
                if (!s.getStatus().equals(SpecimenStatus.AVAILABLE)) {
                    itemsToRemove.add(lineItem);
                }
            }
            if (!itemsToRemove.isEmpty()) {
                request.getLineItems().removeAll(itemsToRemove);
                savePersistentObject(request);
                throw new IllegalStateException(SPECIMEN_NOT_AVAILABLE);
            }
        }
    }

    private void sendMtaAdminEmails(String idString, boolean isNew) {
        if (getApplicationSettingService().isMtaCertificationRequired() && isNew) {
            Set<AbstractUser> mtaAdmins = getUserService().getUsersInRole(Role.MTA_ADMINISTRATOR.getName());
            List<String> emails = new ArrayList<String>();
            for (AbstractUser mtaAdmin : mtaAdmins) {
                emails.add(mtaAdmin.getEmail());
            }
            emailHelper.sendEmail("mta.email.request", emails.toArray(new String[emails.size()]),
                    new String[] {idString});
        }
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteRequest(SpecimenRequest request) {
        SpecimenRequest requestToDelete = getPersistentObject(SpecimenRequest.class, request.getId());
        deletePersistentObject(requestToDelete);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Long savePersistentObject(SpecimenRequest o) {
        /*
         * Note: if passing a previously persisted object not
         * associated with the current persistence context, the
         * updated object must be loaded from persistence after
         * calling this method.
         * */
        if (o.getId() == null) {
            return super.savePersistentObject(o);
        }
        onSave(o);
        TissueLocatorHibernateUtil.getCurrentSession().merge(o);
        return o.getId();
    }


    /**
     * add a number of days to the updated date of a request.
     * @param request the request
     * @param days the number of days to add
     * @return a date representing the updated date of a given request plus an additional number of days.
     */
    protected Date addDaysToUpdatedDate(SpecimenRequest request, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }



    /**
     * {@inheritDoc}
     */
    public void finalizeVote(SpecimenRequest request, boolean createShipments)
        throws MessagingException {
        LOG.info("updating request " + request.getId() + " to status " + request.getFinalVote().name());
        request.setStatus(request.getFinalVote());
        request.setUpdatedDate(new Date());
        updateLineItemStatus(request);
        Map<Institution, Long> shipmentIds = null;
        if (createShipments) {
            shipmentIds = createShipments(request);
        }
        savePersistentObject(request);

        String[] args = new String[] {String.valueOf(request.getId())};
        emailHelper.sendEmail("specimenRequest.email.updated", new String[] {request.getRequestor().getEmail()}, args);
        if (createShipments) {
            sendTissueTechEmails(shipmentIds);
        }
    }

    /**
     * send emails to tissue techs notifying them of new shipments.
     * @param shipmentIds the ids of the new shipments, separated by institution
     * @throws MessagingException on error
     */
    protected void sendTissueTechEmails(Map<Institution, Long> shipmentIds) throws MessagingException {
        Set<AbstractUser> tissueTechs = getUserService().getUsersInRole(Role.SHIPMENT_ADMIN.getName());
        for (AbstractUser user : tissueTechs) {
            TissueLocatorUser tech = (TissueLocatorUser) user;
            if (shipmentIds.containsKey(tech.getInstitution())) {
                String[] args = new String[] {String.valueOf(shipmentIds.get(tech.getInstitution()))};
                emailHelper.sendEmail("specimenRequest.email.tissueTech", new String[] {tech.getEmail()}, args);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @SuppressWarnings("unchecked")
    public SpecimenRequest getRequestWithSpecimen(Specimen specimen) {
        String queryString =
            "select request from " + SpecimenRequestLineItem.class.getName() + " srli join "
           + "srli.requests as request where srli.specimen.id = :specimenId and request.status in (:statuses)";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setLong("specimenId", specimen.getId());
        RequestStatus[] statuses = {RequestStatus.PENDING, RequestStatus.PENDING_FINAL_DECISION,
                RequestStatus.PENDING_REVISION};
        q.setParameterList("statuses", statuses);
        q.setMaxResults(1);
        List<SpecimenRequest> requests = q.list();
        return requests.isEmpty() ? null : requests.get(0);
    }

    /**
     * @return the shipmentService
     */
    public ShipmentServiceLocal getShipmentService() {
        return shipmentService;
    }

    /**
     * @param shipmentService the shipmentService to set
     */
    public void setShipmentService(ShipmentServiceLocal shipmentService) {
        this.shipmentService = shipmentService;
    }

    /**
     * @return the institutionService
     */
    public InstitutionServiceLocal getInstitutionService() {
        return institutionService;
    }

    /**
     * @param institutionService the institutionService to set
     */
    public void setInstitutionService(InstitutionServiceLocal institutionService) {
        this.institutionService = institutionService;
    }

    /**
     * @return the applicationSettingService
     */
    public ApplicationSettingServiceLocal getApplicationSettingService() {
        return applicationSettingService;
    }

    /**
     * @param applicationSettingService the applicationSettingService to set
     */
    @Inject
    public void setApplicationSettingService(ApplicationSettingServiceLocal applicationSettingService) {
        this.applicationSettingService = applicationSettingService;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<SpecimenRequest> getUnreviewedRequests(int lineItemReviewGracePeriod) {
        Date requestedDate = DateUtils.addDays(new Date(), -1 * lineItemReviewGracePeriod);
        Criteria crit = TissueLocatorHibernateUtil.getCurrentSession().createCriteria(SpecimenRequest.class)
        .add(Restrictions.lt("requestedDate", requestedDate))
        .add(Restrictions.or(Restrictions.isNull("nextReminderDate"), Restrictions.lt("nextReminderDate", new Date())))
        .createCriteria("aggregateLineItems")
        .createCriteria("vote")
        .add(Restrictions.isNull("vote"));

        return crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    /**
     * {@inheritDoc}
     */
    public int getNewRequestCount(Date startDate, Date endDate, String additionalCondition) {
        Query q = getNewRequestQuery("count(*)", startDate, endDate, additionalCondition);
        return ((Long) q.uniqueResult()).intValue();
    }

    @SuppressWarnings("PMD.UseStringBufferForStringAppends")
    private Query getNewRequestQuery(String select, Date startDate, Date endDate, String additionalCondition) {
        String queryString = "select " + select
            + " from " + SpecimenRequest.class.getName() + " request "
            + " where request.requestedDate >= :from and request.requestedDate < :to ";
        if (StringUtils.isNotBlank(additionalCondition)) {
            queryString += " and " + additionalCondition;
        }
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        q.setDate("from", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        q.setDate("to", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        return q;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public Map<String, Integer> getLateScientificReviewCounts(Date startDate, Date endDate, int votingPeriod) {
        Map<String, Integer> results = getEmptyMap();
        Query q = getNewRequestQuery("request", startDate, endDate, null);
        List<SpecimenRequest> newRequests = q.list();
        for (SpecimenRequest newRequest : newRequests) {
            for (SpecimenRequestReviewVote vote : newRequest.getConsortiumReviews()) {
                incrementLateVoteCount(vote, results, newRequest.getRequestedDate(), votingPeriod);
            }
        }
        return results;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public Map<String, Integer> getLateInstitutionalReviewCounts(Date startDate, Date endDate, int votingPeriod) {
        Map<String, Integer> results = getEmptyMap();
        Query q = getNewRequestQuery("request", startDate, endDate, null);
        List<SpecimenRequest> newRequests = q.list();
        for (SpecimenRequest newRequest : newRequests) {
            for (SpecimenRequestReviewVote vote : newRequest.getInstitutionalReviews()) {
                incrementLateVoteCount(vote, results, newRequest.getRequestedDate(), votingPeriod);
            }
        }
        return results;
    }

    private Map<String, Integer> getEmptyMap() {
        Map<String, Integer> emptyMap = new HashMap<String, Integer>();
        for (Institution i : getInstitutionService().getConsortiumMembers()) {
            emptyMap.put(i.getName(), 0);
        }
        return emptyMap;
    }

    private void incrementLateVoteCount(SpecimenRequestReviewVote vote, Map<String, Integer> results,
            Date requestDate, int votingPeriod) {
        Date endDate = vote.getDate();
        if (vote.getVote() == null) {
            endDate = new Date();
        }
        Date votingPeriodEndDate = DateUtils.addDays(requestDate, votingPeriod);
        if (votingPeriodEndDate.before(endDate)) {
            String instName = vote.getInstitution().getName();
            results.put(instName, results.get(instName).intValue() + 1);
        }
    }
}
