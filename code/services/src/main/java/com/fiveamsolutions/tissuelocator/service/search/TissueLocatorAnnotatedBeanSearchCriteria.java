/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.search;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsConfigurator;
import com.fiveamsolutions.dynamicextensions.search.AbstractDynamicExtensionsSearchCriteria;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.SearchableUtils;
import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.support.Question;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author ddasgupta
 * @param <T> the class of the example object used to generate the query.
 */
public class TissueLocatorAnnotatedBeanSearchCriteria<T extends PersistentObject>
    extends AbstractDynamicExtensionsSearchCriteria<T> {

    private static final long serialVersionUID = 1L;
    @SuppressWarnings({ "rawtypes" })
    private static final Set<Class> NO_CRITERIA_NEEDED = new HashSet<Class>();
    static {
        NO_CRITERIA_NEEDED.add(TissueLocatorUser.class);
        NO_CRITERIA_NEEDED.add(Institution.class);
        NO_CRITERIA_NEEDED.add(CollectionProtocol.class);
        NO_CRITERIA_NEEDED.add(Participant.class);
        NO_CRITERIA_NEEDED.add(Specimen.class);
        NO_CRITERIA_NEEDED.add(SpecimenRequest.class);
        NO_CRITERIA_NEEDED.add(SupportLetterRequest.class);
        NO_CRITERIA_NEEDED.add(Question.class);
    }

    /**
     * @param example the example object
     */
    public TissueLocatorAnnotatedBeanSearchCriteria(T example) {
        super(example);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Session getSession() {
       return TissueLocatorHibernateUtil.getCurrentSession();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasOneCriterionSpecified() {
        if (NO_CRITERIA_NEEDED.contains(getCriteria().getClass())) {
            return true;
        }
        return super.hasOneCriterionSpecified();
    }

    /**
     * get the appropriate conjunction for the where clause based on whether or not it already has conditions.
     * @param whereClause the where clause
     * @return the appropriate conjunction
     */
    protected String whereOrAnd(StringBuffer whereClause) {
        if (StringUtils.isBlank(whereClause.toString())) {
            return SearchableUtils.WHERE;
        }
        return SearchableUtils.AND;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DynamicExtensionsConfigurator getConfigurator() {
        return TissueLocatorHibernateUtil.getDynamicExtensionConfigurator();
    }
}
