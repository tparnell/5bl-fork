/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.query.JRHibernateQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRLoader;

import org.hibernate.Session;

import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.report.JasperReportDataSource;
import com.fiveamsolutions.tissuelocator.service.report.ReportDataProvider;
import com.fiveamsolutions.tissuelocator.service.report.ScrollingDataSource;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.ParticipantReturnRequestData;
import com.fiveamsolutions.tissuelocator.util.RequestReviewReport;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.UsageReport;
import com.google.inject.Inject;

/**
 * @author ddasgupta
 *
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ReportServiceBean implements ReportServiceLocal {

    @EJB
    private InstitutionServiceLocal institutionService;

    @EJB
    private SpecimenServiceLocal specimenService;

    @EJB
    private ShipmentServiceLocal shipmentService;

    @EJB
    private TissueLocatorUserServiceLocal userService;

    private ApplicationSettingServiceLocal appSettingService;

    @EJB
    private SpecimenRequestServiceLocal requestService;

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public UsageReport getUsageReport(Date startDate, Date endDate) {
        UsageReport report = new UsageReport();
        report.setTopSpecimenTypes(getSpecimenService().getTopRequestedSpecimenTypes(startDate, endDate));
        report.setShippedOrderCount(getShipmentService().getShippedOrderCount(startDate, endDate));
        report.setNewUserCount(getUserService().getNewUserCount(startDate, endDate));
        report.setRestrictedNewRequestCount(getRequestService().getNewRequestCount(startDate, endDate,
                getAppSettingService().getNewRequestCountCondition()));
        report.setUnrestrictedNewRequestCount(getRequestService().getNewRequestCount(startDate, endDate, null));
        Map<Long, ParticipantReturnRequestData> returnData =
            getSpecimenService().getReturnRequestDataByParticipant(startDate, endDate);
        int totalReturnsRequested = 0;
        int totalReturnsFulfilled = 0;
        int totalUnfulfilledReturns = 0;
        for (ParticipantReturnRequestData data : returnData.values()) {
            totalReturnsRequested += data.getReturnsRequested();
            totalReturnsFulfilled += data.getReturnsFulfilled();
            totalUnfulfilledReturns += data.getUnfulfilledReturnRequests();
        }
        report.setTotalReturnsRequested(totalReturnsRequested);
        report.setTotalReturnsFulfilled(totalReturnsFulfilled);
        report.setTotalUnfulfilledReturnRequests(totalUnfulfilledReturns);
        report.setParticipantReturnRequestMap(returnData);
        return report;
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public RequestReviewReport getRequestReviewReport(Date startDate, Date endDate, int votingPeriod) {
        RequestReviewReport report = new RequestReviewReport();
        report.setScientificReviewMap(getInstitutionService().getScientificReviewVoteCounts(startDate, endDate));
        report.setInstitutionalReviewMap(getInstitutionService().getInstitutionalReviewVoteCounts(startDate, endDate));
        report.setLateScientificReviewMap(getRequestService().getLateScientificReviewCounts(startDate, endDate,
                votingPeriod));
        report.setLateInstitutionalReviewMap(getRequestService().getLateInstitutionalReviewCounts(startDate, endDate,
                votingPeriod));
        return report;
    }

    /**
     * {@inheritDoc}
     */
    public JasperPrint runJasperReport(String reportName, Map<String, Object> params) throws JRException {
        return JasperFillManager.fillReport(getReport(reportName), getParameters(params));
    }

    private JasperReport getReport(String reportName) throws JRException {
        File reportFile = new File(reportName);
        if (!reportFile.exists()) {
            throw new IllegalArgumentException("File not found. The report design must be compiled first.");
        }

        return (JasperReport) JRLoader.loadObject(reportFile.getPath());
    }

    private Map<String, Object> getParameters(Map<String, Object> params) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        Session session = TissueLocatorHibernateUtil.getCurrentSession();
        parameters.put(JRHibernateQueryExecuterFactory.PARAMETER_HIBERNATE_SESSION, session);
        if (params != null) {
            parameters.putAll(params);
        }
        return parameters;
    }

    private JRDataSource getDataSource(String queryString, Map<String, Object> queryParams) {
        ReportDataProvider provider = new ReportDataProvider(queryString, queryParams);
        ScrollingDataSource scrollingDataSource = new ScrollingDataSource(provider);
        return new JasperReportDataSource(scrollingDataSource);
    }

    /**
     * {@inheritDoc}
     */
    public JasperPrint runNewUserReport(String reportName, Map<String, Object> params) throws JRException {
        String queryString = "select user from " + TissueLocatorUser.class.getName()
            + " user where user.creationDate >= :startDate and user.creationDate < :endDate "
            + " order by lastname, firstName";
        Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("startDate", params.get("startDate"));
        queryParams.put("endDate", params.get("endDate"));
        return JasperFillManager.fillReport(getReport(reportName), getParameters(params),
                getDataSource(queryString, queryParams));
    }

    /**
     * @return the institutionService
     */
    public InstitutionServiceLocal getInstitutionService() {
        return institutionService;
    }

    /**
     * @param institutionService the institutionService to set
     */
    public void setInstitutionService(InstitutionServiceLocal institutionService) {
        this.institutionService = institutionService;
    }

    /**
     * @return the specimenService
     */
    public SpecimenServiceLocal getSpecimenService() {
        return specimenService;
    }

    /**
     * @param specimenService the specimenService to set
     */
    public void setSpecimenService(SpecimenServiceLocal specimenService) {
        this.specimenService = specimenService;
    }

    /**
     * @return the shipmentService
     */
    public ShipmentServiceLocal getShipmentService() {
        return shipmentService;
    }

    /**
     * @param shipmentService the shipmentService to set
     */
    public void setShipmentService(ShipmentServiceLocal shipmentService) {
        this.shipmentService = shipmentService;
    }

    /**
     * @return the userService
     */
    public TissueLocatorUserServiceLocal getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    @Inject
    public void setUserService(TissueLocatorUserServiceLocal userService) {
        this.userService = userService;
    }

    /**
     * @return the appSettingService
     */
    public ApplicationSettingServiceLocal getAppSettingService() {
        return appSettingService;
    }

    /**
     * @param appSettingService the appSettingService to set
     */
    @Inject
    public void setAppSettingService(ApplicationSettingServiceLocal appSettingService) {
        this.appSettingService = appSettingService;
    }

    /**
     * @return the requestService
     */
    public SpecimenRequestServiceLocal getRequestService() {
        return requestService;
    }

    /**
     * @param requestService the requestService to set
     */
    public void setRequestService(SpecimenRequestServiceLocal requestService) {
        this.requestService = requestService;
    }
}
