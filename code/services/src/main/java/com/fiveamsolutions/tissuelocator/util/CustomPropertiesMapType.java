/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * Map of custom properties that can be persisted to XML.
 * 
 * @author jstephens
 */
@XmlAccessorType(XmlAccessType.NONE)
public class CustomPropertiesMapType {

    @XmlElement(name = "customProperty")
    private final List<MapTypeEntry> entryList = new ArrayList<MapTypeEntry>();
    
    /**
     * Associates the value with the key in this map.
     * 
     * @param key the key with which the value is to be associated.
     * @param value the value to be associated with the key.
     */
    public void put(String key, String value) {
        MapTypeEntry entry = new MapTypeEntry();
        entry.key = key.trim();
        entry.value = value.trim();
        entryList.add(entry);
    }
    
    /**
     * Returns an unmodifiable view of the map entries.
     * 
     * @return an unmodifiable view of the map entries.
     */
    public List<MapTypeEntry> getEntryList() {
        return Collections.unmodifiableList(entryList);
    }
    
    /**
     * A custom property map type entry.
     */
    @XmlAccessorType(XmlAccessType.NONE)
    public static class MapTypeEntry {
        
        @XmlAttribute(name = "name")
        private String key;
        
        @XmlValue
        private String value;
        
        /**
         * Returns the entry key.
         * 
         * @return the entry key.
         */
        public String getKey() {
            return key;
        }
        
        /**
         * Returns the entry value.
         * 
         * @return the entry value.
         */
        public String getValue() {
            return value;
        }
        
    }
    
}
