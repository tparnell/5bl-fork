/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.util;

import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;

/**
 * @author ddasgupta
 *
 */
public interface UserActionAnalyzer {

    /**
     * determines whether a given request needs a reviewer assignment from a given user.
     * @param user the user potentially performing the reviewer assignment
     * @param request the request potentially needing the reviewer assignment
     * @return an indication of whether this request needs a reviewer assignment from a given user.
     */
    boolean needsReviewerAssignment(TissueLocatorUser user, SpecimenRequest request);

    /**
     * determines whether a given request needs a lead review from a given user.
     * @param user the user potentially performing the lead review
     * @param request the request potentially needing the lead review
     * @return an indication of whether this request needs a lead review from a given user.
     */
    boolean needsLeadReview(TissueLocatorUser user, SpecimenRequest request);

    /**
     * determines whether a given request needs a consortium review from a given user.
     * @param user the user potentially performing the consortium review
     * @param request the request potentially needing the consortium review
     * @return an indication of whether this request needs a consortium review from a given user.
     */
    boolean needsConsortiumReview(TissueLocatorUser user, SpecimenRequest request);

    /**
     * determines whether a given request needs a institutional review from a given user.
     * @param user the user potentially performing the institutional review
     * @param request the request potentially needing the institutional review
     * @return an indication of whether this request needs a institutional review from a given user.
     */
    boolean needsInstitutionalReview(TissueLocatorUser user, SpecimenRequest request);

    /**
     * determines whether a given request needs line item review from a given user.
     * @param user the user potentially performing the line item review
     * @param request the request potentially needing the line item review
     * @return an indication of whether this request needs a line item review from a given user.
     */
    boolean needsLineItemReview(TissueLocatorUser user, SpecimenRequest request);

    /**
     * determines whether a given request needs a final comment from a given user.
     * @param user the user potentially performing the final comment
     * @param request the request potentially needing the final comment
     * @return an indication of whether this request needs a final comment from a given user.
     */
    boolean needsFinalComment(TissueLocatorUser user, SpecimenRequest request);

    /**
     * determines whether a given request needs a researcher notification from a given user.
     * @param user the user potentially performing the researcher notification
     * @param request the request potentially needing the researcher notification
     * @return an indication of whether this request needs a researcher notification from a given user.
     */
    boolean needsResearcherNotification(TissueLocatorUser user, SpecimenRequest request);
}
