/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import javax.ejb.Local;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;

/**
 * @author ddasgupta
 *
 */
@Local
public interface SignedMaterialTransferAgreementServiceLocal
    extends GenericServiceLocal<SignedMaterialTransferAgreement> {
    
    /**
     * Sets up a new unsigned material transfer agreement.  This method does the following:
     * 1.  Save the new MTA to the database
     * 2.  Notifies Consortium Administrators that a new signed MTA needs to be reviewed
     * @param mta the mta to save
     * @return the id of the new MTA
     */
    Long saveSignedMaterialTransferAgreement(SignedMaterialTransferAgreement mta);

    /**
     * Review a signed MTA.
     * @param signedMta the signed MTA to review
     * @param institution the institution to which the signed MTA is assigned
     * @param valid whether the signed MTA is valid
     * @param replaceExisting whether the signed MTA should replace the existing MTA for the institution
     */
    void reviewSignedMta(SignedMaterialTransferAgreement signedMta, Institution institution, boolean valid,
            boolean replaceExisting);
    
    /**
     * Updates a new signed material transfer agreement.  This method does the following:
     * 1.  Sets the old MTA of the institution to the out of date state 
     * 2.  Save the new MTA to the database
     * @param newMta the mta to save
     * @param currentMta the current mta to replace
     * @return the id of the new MTA
     */
    Long updateSignedMta(SignedMaterialTransferAgreement newMta, SignedMaterialTransferAgreement currentMta);
    
    /**
     * Deprecates the signed MTA by setting its status to OUT_OF_DATE.
     * @param signedMta the signed MTA to deprecate
     */
    void deprecateSignedMta(SignedMaterialTransferAgreement signedMta);      

}
