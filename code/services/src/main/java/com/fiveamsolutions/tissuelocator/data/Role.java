/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

/**
 * @author ddasgupta
 *
 */
public enum Role {

    /**
     * tissue locator user.
     */
    TISSUE_LOCATOR_USER("tissuelocatoruser"),

    /**
     * administrator.
     */
    ADMINISTRATOR("administrator"),

    /**
     * cross institution user.
     */
    CROSS_INSTITUTION("crossinstitution"),

    /**
     * specimen administrator.
     */
    SPECIMEN_ADMIN("specimenAdmin"),

    /**
     * institution administrator.
     */
    INSTITIUTION_ADMIN("institutionAdmin"),

    /**
     * participant administrator.
     */
    PARTICIPANT_ADMIN("participantAdmin"),

    /**
     * protocol administrator.
     */
    PROTOCOL_ADMIN("protocolAdmin"),

    /**
     * user administrator.
     */
    USER_ADMIN("userAdmin"),

    /**
     * super administrator.
     */
    SUPER_ADMIN("superAdmin"),

    /**
     * tissue request administrator.
     */
    TISSUE_REQUEST_ADMIN("tissueRequestAdmin"),

    /**
     * review commenter.
     */
    REVIEW_COMMENTER("reviewCommenter"),

    /**
     * consortium review voter.
     */
    CONSORTIUM_REVIEW_VOTER("consortiumReviewVoter"),

    /**
     * shipment administrator.
     */
    SHIPMENT_ADMIN("shipmentAdmin"),

    /**
     * institutional review voter.
     */
    INSTITUTIONAL_REVIEW_VOTER("instiutionalReviewVoter"),

    /**
     * line item review voter.
     */
    LINE_ITEM_REVIEW_VOTER("lineItemReviewVoter"),

    /**
     * user that can see specimen institutions in their cart.
     */
    CART_INSTITUTION_VISIBLE("cartInstitutionVisible"),

    /**
     * scientific reviewer assigner.
     */
    SCIENTIFIC_REVIEWER_ASSIGNER("scientificReviewerAssigner"),

    /**
     * user that can view prices.
     */
    PRICE_VIEWER("priceViewer"),

    /**
     * lead reviewer (currently only used for NFCR).
     */
    LEAD_REVIEWER("leadReviewer"),


    /**
     * Revuiew decision notifier (currently only used for NFCR).
     */
    REVIEW_DECISION_NOTIFIER("reviewDecisionNotifier"),

    /**
     * User who can see eternal id of specimens.
     */
    EXTERNAL_ID_REVIEWER("externalIdViewer"),

    /**
     * User who can see where a specimen comes from prior to
     * a request being approved.
     */
    SPECIMEN_LOCATION_VIEWER_PRE_APPROVE("preApprovalSpecimenLocViewer"),

    /**
     * User who can see where a specimen comes from after
     * a request has been approved.
     */
    SPECIMEN_LOCATION_VIEWER_POST_APPROVE("postApprovalSpecimenLocViewer"),

    /**
     * User who can appoval pending user accounts.
     */
    USER_ACCOUNT_APPROVER("userAccountApprover"),

    /**
     * User who can administer material transfer agreements.
     */
    MTA_ADMINISTRATOR("mtaAdministrator"),

    /**
     * User who can view signed mtas.
     */
    SIGNED_MTA_VIEWER("signedMtaViewer"),

    /**
     * A user who can view and maintain MTA information on the institution administration page.
     */
    INSTITUTION_MTA_ADMINISTRATOR("institutionMtaAdmin"),

    /**
     * A user who can view reports.
     */
    REPORT_VIEWER("reportViewer"),

    /**
     * A user who can edit the institution profile url on the institution administration page.
     */
    INSTITUTION_PROFILE_URL_ADMINISTRATOR("institutionProfileUrlAdministrator"),

    /**
     * A user who can submit requests for letters of support.
     */
    SUPPORT_LETTER_REQUESTOR("supportLetterRequestor"),

    /**
     * A user who can review submitted requests for letters of support.
     */
    SUPPORT_LETTER_REQUEST_REVIEWER("supportLetterRequestReviewer"),

    /**
     * A user who can submit questions to institutions.
     */
    QUESTION_ASKER("questionAsker"),

    /**
     * A user who can respond to questions from investigators.
     */
    QUESTION_RESPONDER("questionResponder"),

    /**
     * A user who can review and optionally respond to questions from investigators.
     */
    QUESTION_ADMINISTRATOR("questionAdministrator");


    private final String name;

    /**
     * @param name
     */
    private Role(String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
}
