/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.config.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;

/**
 * @author gvaughn
 *
 */
public enum MultiSelectValueType {

    /**
     * Long data type.
     */
    LONG(new ValueConvertor() {

        public List<Object> getValues(Collection<Object> values) {
            List<Object> convertedValues = new ArrayList<Object>();
            for (Object value : values) {
                convertedValues.add(Long.valueOf(value.toString()));
            }
            return convertedValues;
        }

    }),

    /**
     * Gender enum type.
     */
    GENDER(new ValueConvertor() {

        public List<Object> getValues(Collection<Object> values) {
            List<Object> convertedValues = new ArrayList<Object>();
            for (Object value : values) {
                if (StringUtils.isNotBlank(value.toString())) {
                    convertedValues.add(Gender.valueOf(StringUtils.trimToNull(value.toString())));
                }
            }
            return convertedValues;
        }

    }),

    /**
     * Ethnicity enum type.
     */
    ETHNICITY(new ValueConvertor() {

        public List<Object> getValues(Collection<Object> values) {
            List<Object> convertedValues = new ArrayList<Object>();
            for (Object value : values) {
                if (StringUtils.isNotBlank(value.toString())) {
                    convertedValues.add(Ethnicity.valueOf(StringUtils.trimToNull(value.toString())));
                }
            }
            return convertedValues;
        }

    }),

    /**
     * Race enum type.
     */
    RACE(new ValueConvertor() {
        public List<Object> getValues(Collection<Object> values) {
            List<Object> convertedValues = new ArrayList<Object>();
            for (Object value : values) {
                if (StringUtils.isNotBlank(value.toString())) {
                    convertedValues.add(Race.valueOf(StringUtils.trimToNull(value.toString())));
                }
            }
            return convertedValues;
        }
    }),
    
    /**
     * String value type.
     */
    STRING(new ValueConvertor() {
        public List<Object> getValues(Collection<Object> values) {
            List<Object> convertedValues = new ArrayList<Object>();
            for (Object value : values) {
                if (StringUtils.isNotBlank(value.toString())) {
                    convertedValues.add(StringUtils.trimToNull(value.toString()));
                }
            }
            return convertedValues;
        }
    }),

    /**
     * SpecimenClass enum type.
     */
    SPECIMENCLASS(new ValueConvertor() {
        public List<Object> getValues(Collection<Object> values) {
            List<Object> convertedValues = new ArrayList<Object>();
            for (Object value : values) {
                if (StringUtils.isNotBlank(value.toString())) {
                    convertedValues.add(SpecimenClass.valueOf(StringUtils.trimToNull(value.toString())));
                }
            }
            return convertedValues;
        }
    });

    /**
     * Converts input values to the appropriate type for a multi-select search.
     * @author gvaughn
     *
     */
    public interface ValueConvertor {

        /**
         * Returns a collection of values of the appropriate type.
         * @param values Input values to be converted.
         * @return a collection of values of the appropriate type.
         */
        List<Object> getValues(Collection<Object> values);
    }

    private ValueConvertor valueConvertor;

    MultiSelectValueType(ValueConvertor valueConvertor) {
        this.valueConvertor = valueConvertor;
    }

    /**
     * Returns a collection of values of the appropriate type.
     * @param values Input values to be converted.
     * @return a collection of values of the appropriate type.
     */
    public List<Object> getValues(Collection<Object> values) {
        return valueConvertor.getValues(values);
    }
}
