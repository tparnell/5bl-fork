/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.QuantityUnits;

/**
 * @author gvaughn
 *
 */
public class QuantityConverter {

    private static final Map<QuantityUnits, Map<QuantityUnits, QuantityConversion>> CONVERSION_MAP
        = new HashMap<QuantityUnits, Map<QuantityUnits, QuantityConversion>>();
    
    private static final BigDecimal MASS_CONVERSION_FACTOR = new BigDecimal(1000);
    private static final BigDecimal MASS_CONVERSION_FACTOR_SQ = new BigDecimal(1000000);
    private static final BigDecimal OZ_ML_CONVERSION_FACTOR = new BigDecimal("29.5735296");
    private static final int SCALE = 1000;
    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_DOWN;
    
    static {
        // Grams
        Map<QuantityUnits, QuantityConversion> conversionMap = new HashMap<QuantityUnits, QuantityConversion>();
        conversionMap.put(QuantityUnits.MG, new QuantityConversion() {
            public BigDecimal convert(BigDecimal quantityValue) {
                return quantityValue.multiply(MASS_CONVERSION_FACTOR);
            }
        });
        conversionMap.put(QuantityUnits.UG, new QuantityConversion() {
            public BigDecimal convert(BigDecimal quantityValue) {
                return quantityValue.multiply(MASS_CONVERSION_FACTOR_SQ);
            }
        });
        CONVERSION_MAP.put(QuantityUnits.G, conversionMap);
        
        // Milligrams
        conversionMap = new HashMap<QuantityUnits, QuantityConversion>();
        conversionMap.put(QuantityUnits.UG, new QuantityConversion() {
            public BigDecimal convert(BigDecimal quantityValue) {
                return quantityValue.multiply(MASS_CONVERSION_FACTOR);
            }
        });
        conversionMap.put(QuantityUnits.G, new QuantityConversion() {
            public BigDecimal convert(BigDecimal quantityValue) {
                return quantityValue.divide(MASS_CONVERSION_FACTOR);
            }
        });
        CONVERSION_MAP.put(QuantityUnits.MG, conversionMap);
        
        // Micrograms
        conversionMap = new HashMap<QuantityUnits, QuantityConversion>();
        conversionMap.put(QuantityUnits.MG, new QuantityConversion() {
            public BigDecimal convert(BigDecimal quantityValue) {
                return quantityValue.divide(MASS_CONVERSION_FACTOR);
            }
        });
        conversionMap.put(QuantityUnits.G, new QuantityConversion() {
            public BigDecimal convert(BigDecimal quantityValue) {
                return quantityValue.divide(MASS_CONVERSION_FACTOR_SQ);
            }
        });
        CONVERSION_MAP.put(QuantityUnits.UG, conversionMap);
        
        // Ounces
        conversionMap = new HashMap<QuantityUnits, QuantityConversion>();
        conversionMap.put(QuantityUnits.ML, new QuantityConversion() {
            public BigDecimal convert(BigDecimal quantityValue) {
                return quantityValue.multiply(OZ_ML_CONVERSION_FACTOR);
            }
        });
        CONVERSION_MAP.put(QuantityUnits.OZ, conversionMap);
        
        //Milliliters
        conversionMap = new HashMap<QuantityUnits, QuantityConversion>();
        conversionMap.put(QuantityUnits.OZ, new QuantityConversion() {
            public BigDecimal convert(BigDecimal quantityValue) {
                return quantityValue.divide(OZ_ML_CONVERSION_FACTOR, SCALE, ROUNDING_MODE);
            }
        });
        CONVERSION_MAP.put(QuantityUnits.ML, conversionMap);
    }
    
    /**
     * Performs a quantity conversion with units implied.
     */
    interface QuantityConversion {
        
        /**
         * Performs a quantity conversion with units implied.
         * @param quantityValue Quantity value to be converted.
         * @return The converted quantity value.
         */
        BigDecimal convert(BigDecimal quantityValue);
    }
    
    /**
     * If possible, converts the quantity from the given units to the indicated units.
     * If no conversion is possible, return null;
     * @param quantityValue Quantity to be converted.
     * @param currentUnits Units in which the given quantity is represented.
     * @param targetUnits Units to which the quantity will be converted.
     * @return The given quantity converted to the indicated units, or null if no conversion is possible.
     */
    public static BigDecimal convert(BigDecimal quantityValue, QuantityUnits currentUnits, 
            QuantityUnits targetUnits) {
        if (currentUnits.equals(targetUnits)) {
            return quantityValue;
        }
        if (CONVERSION_MAP.containsKey(currentUnits)
                && CONVERSION_MAP.get(currentUnits).containsKey(targetUnits)) {
            return CONVERSION_MAP.get(currentUnits).get(targetUnits).convert(quantityValue);
        }
        return null;
    }
    
    /**
     * Returns a mapping of quantity units to values for all possible
     * representations of the given quantity and units, including the quantity
     * and units given.
     * @param quantityValue Quantity to be converted.
     * @param currentUnits Units in which the given quantity is represented.
     * @return All possible quantity values mapped to the corresponding units.
     */
    public static Map<QuantityUnits, BigDecimal> getAllValues(BigDecimal quantityValue, QuantityUnits currentUnits) {
        Map<QuantityUnits, BigDecimal> values = new HashMap<QuantityUnits, BigDecimal>();
        if (!CONVERSION_MAP.containsKey(currentUnits)) {
            values.put(currentUnits, quantityValue);
        } else {
            for (QuantityUnits units : CONVERSION_MAP.keySet()) {
                values.put(units, convert(quantityValue, currentUnits, units));
            }
        }
        return values;
    }
}
