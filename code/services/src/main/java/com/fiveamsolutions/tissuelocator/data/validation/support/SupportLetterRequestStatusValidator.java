/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.validation.support;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.validator.AbstractMultipleCriteriaValidator;
import com.fiveamsolutions.nci.commons.validator.ValidationError;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus;

/**
 * Validates fields related to a support letter request's status.
 * @author ddasgupta
 *
 */
public class SupportLetterRequestStatusValidator
    extends AbstractMultipleCriteriaValidator<SupportLetterRequestStatus> {

    private static final long serialVersionUID = -877160142814415432L;
    private static final String RESPONSE_FIELD_NAME = "response";
    private static final String RESPONSE_MESSAGE_KEY = "{validator.supportLetterRequest.response.required}";
    private static final String LETTER_FIELD_NAME = "letter";
    private static final String LETTER_MESSAGE_KEY = "{validator.supportLetterRequest.letter.required}";
    private static final String OFFLINE_NOTES_FIELD_NAME = "offlineResponseNotes";
    private static final String OFFLINE_NOTES_MESSAGE_KEY = "{validator.supportLetterRequest.offlineNotes.required}";

    /**
     * {@inheritDoc}
     */
    @Override
    protected Collection<ValidationError> validateObject(Object value) {
        Collection<ValidationError> errors = new ArrayList<ValidationError>();
        if (value instanceof SupportLetterRequest) {
            SupportLetterRequest request = (SupportLetterRequest) value;
            validateRespondedStatus(request, errors);
            validateRespondedOfflineStatus(request, errors);
        }

        return errors;
    }

    private void validateRespondedStatus(SupportLetterRequest request, Collection<ValidationError> errors) {
        if (SupportRequestStatus.RESPONDED.equals(request.getStatus())) {
            if (StringUtils.isBlank(request.getResponse())) {
                ValidationError ve = new ValidationError(RESPONSE_FIELD_NAME, RESPONSE_MESSAGE_KEY);
                errors.add(ve);
            }
            if (request.getLetter() == null) {
                ValidationError ve = new ValidationError(LETTER_FIELD_NAME, LETTER_MESSAGE_KEY);
                errors.add(ve);
            }
        }
    }

    private void validateRespondedOfflineStatus(SupportLetterRequest request, Collection<ValidationError> errors) {
        if (SupportRequestStatus.RESPONDED_OFFLINE.equals(request.getStatus())
                && StringUtils.isBlank(request.getOfflineResponseNotes())) {
            ValidationError ve = new ValidationError(OFFLINE_NOTES_FIELD_NAME, OFFLINE_NOTES_MESSAGE_KEY);
            errors.add(ve);
        }
    }
}
