/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.mail.MessagingException;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;

/**
 * @author ddasgupta
 *
 */
@Stateless
public class FixedConsortiumReviewServiceBean extends SpecimenRequestServiceBean implements
        FixedConsortiumReviewServiceLocal {

    /**
     * {@inheritDoc}
     */
    public void assignRequest(SpecimenRequest request, int votingPeriod) throws MessagingException {
        List<String> emails = new ArrayList<String>();
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null) {
                vote.setInstitution(vote.getUser().getInstitution());
                emails.add(vote.getUser().getEmail());
            }
        }
        savePersistentObject(request);

        EmailHelper emailHelper = new EmailHelper();
        String idString = String.valueOf(request.getId());
        String votingDeadline = emailHelper.getDefaultDateFormat().format(addDaysToUpdatedDate(request, votingPeriod));
        String[] args = new String[] {idString, votingDeadline};
        emailHelper.sendEmail("specimenRequest.email.vote", emails.toArray(new String[emails.size()]), args);
    }

    /**
     * {@inheritDoc}
     */
    public void declineReview(SpecimenRequest request, SpecimenRequestReviewVote declinedReview,
            int votingPeriod, int reviewerAssignmentPeriod) throws MessagingException {
        TissueLocatorUser oldReviewer = declinedReview.getUser();
        declinedReview.setUser(null);
        declinedReview.setVote(null);
        declinedReview.setComment(null);
        savePersistentObject(request);

        EmailHelper emailHelper = new EmailHelper();
        SimpleDateFormat sdf = emailHelper.getDefaultDateFormat();
        String votingDeadline = sdf.format(addDaysToUpdatedDate(request, votingPeriod));
        String reviewerAssigmentDeadline = sdf.format(addDaysToUpdatedDate(request, reviewerAssignmentPeriod));
        String idString = String.valueOf(request.getId());
        String oldReviewerName = oldReviewer.getFirstName() + " " + oldReviewer.getLastName();
        String[] args = new String[] {idString, oldReviewerName, votingDeadline, reviewerAssigmentDeadline};
        emailHelper.sendEmail("specimenRequest.email.declineReview",
                getEmailsInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER), args);
    }
    /**
     * {@inheritDoc}
     */
    public void saveFinalVote(SpecimenRequest request) throws MessagingException {
        savePersistentObject(request);
        String[] args = new String[] {String.valueOf(request.getId())};
        new EmailHelper().sendEmail("specimenRequest.email.coordinator",
                getEmailsInRole(Role.REVIEW_DECISION_NOTIFIER), args);
    }

    private String[] getEmailsInRole(Role role) {
        List<String> emails = new ArrayList<String>();
        Set<AbstractUser> assigners = getUserService().getUsersInRole(role.getName());
        for (AbstractUser assigner : assigners) {
            emails.add(assigner.getEmail());
        }
        return emails.toArray(new String[emails.size()]);
    }
}
