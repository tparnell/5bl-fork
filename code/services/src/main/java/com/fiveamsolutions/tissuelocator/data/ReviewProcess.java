/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import com.fiveamsolutions.tissuelocator.util.FixedConsortiumReviewRequestProcessor;
import com.fiveamsolutions.tissuelocator.util.FixedConsortiumReviewUserActionAnalyzer;
import com.fiveamsolutions.tissuelocator.util.FullConsortiumReviewRequestProcessor;
import com.fiveamsolutions.tissuelocator.util.FullConsortiumReviewUserActionAnalyzer;
import com.fiveamsolutions.tissuelocator.util.LeastProgressedStatusUpdater;
import com.fiveamsolutions.tissuelocator.util.LineItemReviewRequestProcessor;
import com.fiveamsolutions.tissuelocator.util.LineItemReviewUserActionAnalyzer;
import com.fiveamsolutions.tissuelocator.util.RequestProcessor;
import com.fiveamsolutions.tissuelocator.util.RequestStatusUpdater;
import com.fiveamsolutions.tissuelocator.util.ShipmentStatusUpdater;
import com.fiveamsolutions.tissuelocator.util.UserActionAnalyzer;

/**
 * @author ddasgupta
 *
 */
public enum ReviewProcess {

    /**
     * Review process including consortium and institutional reviews.
     * Originally developed for Arizona Biomedical Research Commission.
     */
    FULL_CONSORTIUM_REVIEW(new FullConsortiumReviewRequestProcessor(), "fullConsortium",
            new FullConsortiumReviewUserActionAnalyzer(), new ShipmentStatusUpdater()),

    /**
     * Review process involving a fixed number of consortium reviewers.
     * Originally developed for National Foundation for Cancer Research.
     */
    FIXED_CONSORTIUM_REVIEW(new FixedConsortiumReviewRequestProcessor(), "fixedConsortium",
            new FixedConsortiumReviewUserActionAnalyzer(), new ShipmentStatusUpdater()),

    /**
     * Review process including only line item reviews.
     * Originally developed for ACMG - Newborn Screening Translation Research Network.
     */
    LINE_ITEM_REVIEW(new LineItemReviewRequestProcessor(), "lineItem",
            new LineItemReviewUserActionAnalyzer(), new LeastProgressedStatusUpdater());

    private final RequestProcessor requestProcessor;
    private final String namespace;
    private final UserActionAnalyzer userActionAnalyzer;
    private final RequestStatusUpdater requestStatusUpdater;

    /**
     * @param requestProcessor
     */
    private ReviewProcess(RequestProcessor requestProcessor, String namespace, UserActionAnalyzer userActionAnalyzer,
            RequestStatusUpdater requestStatusUpdater) {
        this.requestProcessor = requestProcessor;
        this.namespace = namespace;
        this.userActionAnalyzer = userActionAnalyzer;
        this.requestStatusUpdater = requestStatusUpdater;
    }

    /**
     * @return the requestProcessor
     */
    public RequestProcessor getRequestProcessor() {
        return requestProcessor;
    }

    /**
     * @return the namespace
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * @return the userActionAnalyzer
     */
    public UserActionAnalyzer getUserActionAnalyzer() {
        return userActionAnalyzer;
    }

    /**
     * @return the requestStatusUpdater
     */
    public RequestStatusUpdater getRequestStatusUpdater() {
        return requestStatusUpdater;
    }

}
