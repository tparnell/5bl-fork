/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.util.RequestProcessingConfiguration;

/**
 * @author smiller
 *
 */
@SuppressWarnings("PMD.TooManyMethods")
public class SpecimenRequestVoteAnalyzer {

    private static final Logger LOG = Logger.getLogger(SpecimenRequestVoteAnalyzer.class);
    private static final double PERCENT_MULTIPLIER = 100.0;

    private final RequestProcessingConfiguration config;

    /**
     * Constructor.
     * @param config the config
     */
    public SpecimenRequestVoteAnalyzer(RequestProcessingConfiguration config) {
        this.config = config;
    }

    /**
     * counts up the votes for a request.
     * @param request the request.
     * @return null for no new status, the status if changed.
     * @throws NotEnoughVotesException on error.
     */
    public RequestStatus tabulateVotes(SpecimenRequest request) throws NotEnoughVotesException {
        RequestStatus newStatus = null;
        if (failedAllInstitutionalReviews(request)) {
            newStatus = RequestStatus.DENIED;
        } else {
            if (!isVotingPeriodDone(request)) {
                LOG.info("voting period not complete for request " + request.getId());
                return null;
            }

            checkEnoughVotes(request);

            newStatus = getStatus(request);
        }

        return newStatus;
    }

    private boolean failedAllInstitutionalReviews(SpecimenRequest request) {
        if (request.getInstitutionalReviews().isEmpty()) {
            return false;
        }
        for (SpecimenRequestReviewVote review : request.getInstitutionalReviews()) {
            if (review.getVote() == null || Vote.APPROVE.equals(review.getVote())) {
                return false;
            }
        }
        return true;
    }

    private Date getVotingPeriodCloseDate(SpecimenRequest request) {
        SpecimenRequestVoteEndDateCalculator calculator = new SpecimenRequestVoteEndDateCalculator();
        return calculator.getVotingPeriodEndDate(request.getUpdatedDate(), config.getVotingPeriod());
    }

    private boolean isVotingPeriodDone(SpecimenRequest request) {
        return getVotingPeriodCloseDate(request).before(new Date());
    }

    private void checkEnoughVotes(SpecimenRequest request) throws NotEnoughVotesException {
        // determine if there were enough scientific votes
        int numVotes = 0;
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getVote() != null) {
                numVotes++;
            }
        }
        LOG.trace("number of votes: " + numVotes);
        LOG.trace("committee size: " + request.getConsortiumReviews().size());
        LOG.trace("required percentage available votes: " + config.getMinPercentAvailableVotes());
        double percentAvailableVotes = PERCENT_MULTIPLIER
            * (double) numVotes / (double) request.getConsortiumReviews().size();
        boolean enoughScientificVotes = percentAvailableVotes >= config.getMinPercentAvailableVotes();

        Collection<Institution> institutionsNeedingToVote =
            SpecimenRequestVoteAnalyzer.getInsitutionsNeedingToPerformInstitutionalReview(request);
        if (!enoughScientificVotes || !institutionsNeedingToVote.isEmpty()) {
            LOG.info("not enough votes for request " + request.getId());
            throw new NotEnoughVotesException(enoughScientificVotes, institutionsNeedingToVote.isEmpty(), false,
                    config.getVoteLateEmail());
        }
    }

    /**
     * get the list of institutions that need to perform institutional review.
     * @param request the request
     * @return the institutions
     */
    public static Set<Institution> getInsitutionsNeedingToPerformInstitutionalReview(SpecimenRequest request) {
        // determine if all inst votes are done
        @SuppressWarnings("unchecked")
        Collection<Institution> institutionsNeedingToVoteCollection = CollectionUtils.collect(request.getLineItems(),
                new Transformer() {
                    /**
                     * {@inheritDoc}
                     */
                    public Object transform(Object input) {
                        SpecimenRequestLineItem li = (SpecimenRequestLineItem) input;
                        return li.getSpecimen().getExternalIdAssigner();
                    }
        });
        HashSet<Institution> institutionsNeedingToVote = new HashSet<Institution>();
        institutionsNeedingToVote.addAll(institutionsNeedingToVoteCollection);
        for (SpecimenRequestReviewVote review : request.getInstitutionalReviews()) {
            if (null != review.getVote()) {
                institutionsNeedingToVote.remove(review.getInstitution());
            }
        }
        return institutionsNeedingToVote;
    }

    private RequestStatus getStatus(SpecimenRequest request) throws NotEnoughVotesException {
        Map<Vote, Integer> voteMap = countVotes(request);
        Vote winner = determinWinner(voteMap);

        LOG.trace("winner: " + winner.name());
        LOG.trace("winner votes: " + voteMap.get(winner).intValue());
        LOG.trace("total votes: " + request.getConsortiumReviews().size());
        LOG.trace("required percentage winning votes: " + config.getMinPercentWinningVotes());

        //verify that we have enough of a consensus to accept the winning vote
        int numVotes = 0;
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getVote() != null) {
                numVotes++;
            }
        }
        
        double percentWinningVotes = PERCENT_MULTIPLIER
            * (double) voteMap.get(winner).intValue() / (double) numVotes;
        if (percentWinningVotes < config.getMinPercentWinningVotes()) {
            LOG.info("winner determined to be " + winner.name() + " but it does not have enough votes yet");
            throw new NotEnoughVotesException(false, true, true, config.getDeadlockEmail());
        }

        return checkForPartialApproval(request, winner);
    }

    private Map<Vote, Integer> countVotes(SpecimenRequest request) {
        // create a map to hold all the vote counts, with each vote count initialized to 0
        Map<Vote, Integer> voteMap = new HashMap<Vote, Integer>();
        for (Vote vote : Vote.values()) {
            voteMap.put(vote, Integer.valueOf(0));
        }
        //count the votes
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getVote() != null) {
                voteMap.put(vote.getVote(), Integer.valueOf(voteMap.get(vote.getVote()) + 1));
            }
        }
        return voteMap;
    }

    private Vote determinWinner(Map<Vote, Integer> voteMap) {
        // determine which vote has the highest count
        // note that in this implementation, if multiple votes have the same count, the first one encountered
        // when iterating through the map will be used.
        Vote winner = Vote.values()[0];
        for (Map.Entry<Vote, Integer> count : voteMap.entrySet()) {
            LOG.trace("vote count for " + count.getKey().name() + ": " + count.getValue().intValue());
            if (count.getValue().intValue() > voteMap.get(winner).intValue()) {
                winner = count.getKey();
            }
        }
        return winner;
    }

    private RequestStatus checkForPartialApproval(SpecimenRequest request, Vote winner) {
        if (winner.getStatus().equals(RequestStatus.APPROVED)) {
            // check for partial approval
            for (SpecimenRequestReviewVote review : request.getInstitutionalReviews()) {
                if (Vote.DENY.equals(review.getVote())) {
                    return RequestStatus.PARTIALLY_APPROVED;
                }
            }
        }
        return winner.getStatus();
    }
}
