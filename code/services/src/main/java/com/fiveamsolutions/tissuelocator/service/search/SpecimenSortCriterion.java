/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.search;

import com.fiveamsolutions.nci.commons.data.search.SortCriterion;
import com.fiveamsolutions.tissuelocator.data.Specimen;

/**
 * @author ddasgupta
 */
public enum SpecimenSortCriterion implements SortCriterion<Specimen> {

    /**
     * sort by external id.
     */
    ID("id", null),

    /**
     * sort by external id.
     */
    EXTERNAL_ID("externalId", null),

    /**
     * sort by specimen type.
     */
    SPECIMEN_TYPE("specimenType.name", null),

    /**
     * sort by pathological characteristic.
     */
    PATHOLOGICAL_CHARACTERISTIC("pathologicalCharacteristic.name", null),

    /**
     * sort by amount required (to be used in conjunction with AMOUNT_UNITS).
     */
    AVAILABLE_QUANTITY("availableQuantity", null),

    /**
     * sort by amount required units (to be used in conjunction with AMOUNT).
     */
    AVAILABLE_QUANTITY_UNITS("availableQuantityUnits", null),

    /**
     * sort by price negotiable (to be used in conjunction with MINIMUM_PRICE and MAXIMUM_PRICE).
     */
    PRICE_NEGOTIABLE("priceNegotiable", null),

    /**
     * sort by minimum price (to be used in conjunction with PRICE_NEGOTIABLE and MAXIMUM_PRICE).
     */
    MINIMUM_PRICE("minimumPrice", null),

    /**
     * sort by maximum price (to be used in conjunction with PRICE_NEGOTIABLE and MINIMUM_PRICE).
     */
    MAXIMUM_PRICE("maximumPrice", null),

    /**
     * sort by status.
     */
    STATUS("status", null),

    /**
     * sort by patient age at collection (to be used in conjunction with PATIENT_AGE_AT_COLLECTION_UNITS).
     */
    PATIENT_AGE_AT_COLLECTION("patientAgeAtCollection", null),

    /**
     * sort by patient age at collection (to be used in conjunction with PATIENT_AGE_AT_COLLECTION).
     */
    PATIENT_AGE_AT_COLLECTION_UNITS("patientAgeAtCollectionUnits", null),

    /**
     * sort by collection year.
     */
    COLLECTION_YEAR("collectionYear", null);


    private String orderField;
    private String leftJoinField;

    private SpecimenSortCriterion(String orderField, String leftJoinField) {
        this.orderField = orderField;
        this.leftJoinField = leftJoinField;
    }

    /**
     * {@inheritDoc}
     */
    public String getOrderField() {
        return this.orderField;
    }

    /**
     * {@inheritDoc}
     */
    public String getLeftJoinField() {
        return this.leftJoinField;
    }
}
