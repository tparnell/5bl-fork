/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.util.Date;
import java.util.List;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.Length;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.code.FundingSource;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableNotEmpty;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableNotNull;
import com.fiveamsolutions.tissuelocator.data.validation.FundingStatusCheck;
import com.fiveamsolutions.tissuelocator.data.validation.StudyIrbApprovalRequired;
import com.fiveamsolutions.tissuelocator.data.validation.study.RequiredProtocolDocument;
import com.fiveamsolutions.tissuelocator.data.validation.study.StudyFundingStatus;
import com.fiveamsolutions.tissuelocator.data.validation.study.StudyIrbApprovalStatus;
import com.fiveamsolutions.tissuelocator.hibernate.interceptor.DeleteOrphans;

/**
 * @author ddasgupta
 *
 */
@Entity(name = "study")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@StudyIrbApprovalStatus
@StudyFundingStatus
@DeleteOrphans(targetProperties = { "protocolDocument", "irbApprovalLetter", "irbExemptionLetter" },
        idProperties = { "lob.id", "lob.id", "lob.id" },
        orphanClasses = { LobHolder.class, LobHolder.class, LobHolder.class })
public class Study implements PersistentObject, Auditable {

    private static final long serialVersionUID = -2184628888767148734L;
    private static final int LENGTH = 254;
    private static final int ABSTRACT_LENGTH = 3999;

    private Long id;
    private String protocolTitle;
    private List<FundingSource> fundingSources;
    private FundingStatus fundingStatus;
    private Boolean preliminary;
    private TissueLocatorFile protocolDocument;
    private String irbName;
    private IrbApprovalStatus irbApprovalStatus;
    private String irbApprovalNumber;
    private String irbRegistrationNumber;
    private String studyAbstract;
    private TissueLocatorFile irbExemptionLetter;
    private Date irbApprovalExpirationDate;
    private TissueLocatorFile irbApprovalLetter;

    /**
     * {@inheritDoc}
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id db id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the protocolTitle
     */
    @DisableableNotEmpty
    @Length(max = LENGTH)
    @Column(name = "protocol_title")
    public String getProtocolTitle() {
        return protocolTitle;
    }

    /**
     * @param protocolTitle the protocolTitle to set
     */
    public void setProtocolTitle(String protocolTitle) {
        this.protocolTitle = protocolTitle;
    }

    /**
     * @return the fundingSources
     */
    @ManyToMany
    @JoinTable(name = "study_funding_source",
                joinColumns = @JoinColumn(name = "study_id"),
                inverseJoinColumns = @JoinColumn(name = "funding_source_id"))
    @ForeignKey(name = "STUDY_FUNDING_SOURCE_FK",
                inverseName = "FUNDING_SOURCE_STUDY_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @OrderBy(value = "id")
    public List<FundingSource> getFundingSources() {
        return fundingSources;
    }

    /**
     * @param fundingSources the fundingSources to set
     */
    public void setFundingSources(List<FundingSource> fundingSources) {
        this.fundingSources = fundingSources;
    }

    /**
     * @return the funding status
     */
    @DisableableNotNull
    @Column(name = "funding_status")
    @Enumerated(EnumType.STRING)
    @FundingStatusCheck
    public FundingStatus getFundingStatus() {
        return fundingStatus;
    }

    /**
     * @param fundingStatus the funding status value to set
     */
    public void setFundingStatus(FundingStatus fundingStatus) {
        this.fundingStatus = fundingStatus;
    }

    /**
     * @return the preliminary
     */
    @Column(name = "preliminary_study")
    public Boolean getPreliminary() {
        return preliminary;
    }

    /**
     * @param preliminary the preliminary to set
     */
    public void setPreliminary(Boolean preliminary) {
        this.preliminary = preliminary;
    }

    /**
     * @return the irbName
     */
    @Length(max = LENGTH)
    @Column(name = "irb_name")
    public String getIrbName() {
        return irbName;
    }

    /**
     * @param irbName the irbName to set
     */
    public void setIrbName(String irbName) {
        this.irbName = irbName;
    }

    /**
     * @return The IRB approval status of this study.
     */
    @DisableableNotNull
    @StudyIrbApprovalRequired
    @Column(name = "irb_approval_status")
    @Enumerated(EnumType.STRING)
    public IrbApprovalStatus getIrbApprovalStatus() {
        return irbApprovalStatus;
    }

    /**
     * @param irbApprovalStatus The IRB approval status of this study.
     */
    public void setIrbApprovalStatus(IrbApprovalStatus irbApprovalStatus) {
        this.irbApprovalStatus = irbApprovalStatus;
    }

    /**
     * @return the irbApprovalNumber
     */
    @Length(max = LENGTH)
    @Column(name = "irb_approval_number")
    public String getIrbApprovalNumber() {
        return irbApprovalNumber;
    }

    /**
     * @param irbApprovalNumber the irbApprovalNumber to set
     */
    public void setIrbApprovalNumber(String irbApprovalNumber) {
        this.irbApprovalNumber = irbApprovalNumber;
    }

    /**
     * @return the irbRegistrationNumber
     */
    @Length(max = LENGTH)
    @Column(name = "irb_registration_number")
    public String getIrbRegistrationNumber() {
        return irbRegistrationNumber;
    }

    /**
     * @param irbRegistrationNumber the irbRegistrationNumber to set
     */
    public void setIrbRegistrationNumber(String irbRegistrationNumber) {
        this.irbRegistrationNumber = irbRegistrationNumber;
    }

    /**
     * @return the studyAbstract
     */
    @DisableableNotEmpty(message = "{validator.study.studyAbstract.required}")
    @Length(max = ABSTRACT_LENGTH)
    @Column(name = "study_abstract")
    public String getStudyAbstract() {
        return studyAbstract;
    }

    /**
     * @param studyAbstract the studyAbstract to set
     */
    public void setStudyAbstract(String studyAbstract) {
        this.studyAbstract = studyAbstract;
    }

    /**
     * @return The expiration date of the irb approval
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "irb_approval_expiration_date")
    public Date getIrbApprovalExpirationDate() {
        return irbApprovalExpirationDate;
    }

    /**
     * @param irbApprovalExpirationDate The irb approval expiration date to set
     */
    public void setIrbApprovalExpirationDate(Date irbApprovalExpirationDate) {
        this.irbApprovalExpirationDate = irbApprovalExpirationDate;
    }

    /**
     * @return the protocolDocument
     */
    @Valid
    @RequiredProtocolDocument
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "contentType", column = @Column(name = "protocol_document_content_type")),
        @AttributeOverride(name = "name", column = @Column(name = "protocol_document_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = "lob", joinColumns = @JoinColumn(name = "protocol_document_lob_id"))
    })
    public TissueLocatorFile getProtocolDocument() {
        return protocolDocument;
    }

    /**
     * @param protocolDocument the protocolDocument to set
     */
    public void setProtocolDocument(TissueLocatorFile protocolDocument) {
        this.protocolDocument = protocolDocument;
    }

    /**
     * @return the irbExemptionLetter
     */
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "contentType", column = @Column(name = "irb_exemption_content_type")),
        @AttributeOverride(name = "name", column = @Column(name = "irb_exemption_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = "lob", joinColumns = @JoinColumn(name = "irb_exemption_lob_id"))
    })
    public TissueLocatorFile getIrbExemptionLetter() {
        return irbExemptionLetter;
    }

    /**
     * @param irbExemptionLetter the irbExemptionLetter to set
     */
    public void setIrbExemptionLetter(TissueLocatorFile irbExemptionLetter) {
        this.irbExemptionLetter = irbExemptionLetter;
    }

    /**
     * @return the irbApprovalLetter
     */
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "contentType", column = @Column(name = "irb_approval_content_type")),
        @AttributeOverride(name = "name", column = @Column(name = "irb_approval_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = "lob", joinColumns = @JoinColumn(name = "irb_approval_lob_id"))
    })
    public TissueLocatorFile getIrbApprovalLetter() {
        return irbApprovalLetter;
    }

    /**
     * @param irbApprovalLetter the irbApprovalLetter to set
     */
    public void setIrbApprovalLetter(TissueLocatorFile irbApprovalLetter) {
        this.irbApprovalLetter = irbApprovalLetter;
    }

}
