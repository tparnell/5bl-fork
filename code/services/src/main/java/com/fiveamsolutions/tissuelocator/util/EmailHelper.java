/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.util;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;

/**
 * @author smiller
 * 
 */
public class EmailHelper {

    /**
     * Send an email to a collection of recipients.
     * 
     * @param keyBase the base of the resource keys containing the text of the email to be sent.
     * @param emails the email addresses to send the email to
     * @param replacementValues the replacement values to substitute into the email text.
     */
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public void sendEmail(String keyBase, String[] emails, String[] replacementValues) {
        try {
            Email template = getEmail(keyBase, replacementValues);
            if (template != null) {
                // send the template only if the template is valid
                for (String emailAddress : emails) {
                    Email email = new Email();
                    email.setRecipient(emailAddress);
                    email.setSubject(template.getSubject());
                    email.setText(template.getText());
                    email.setHtml(template.getHtml());
                    email.send();
                }
            }
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get an email that hasn't been sent yet.
     * 
     * @param keyBase the base of the resource keys containing the text of the email to be sent.
     * @param replacementValues the replacement values to substitute into the email text.
     * @return an email to be sent or null if no subject and no text are found
     */
    public Email getEmail(String keyBase, String[] replacementValues) {
        ResourceBundle resources = getResourceBundle();
        String baseUrl = resources.getString("tissuelocator.baseUrl");

        String subject = resources.getString(keyBase + ".subject");
        String text = resources.getString(keyBase + ".text");
        String html = resources.getString(keyBase + ".html");

        if (StringUtils.isBlank(subject) && (StringUtils.isBlank(text) || StringUtils.isBlank(html))) {
            // if no template has been specified return a null template
            return null;
        }
        text = text.replace("{0}", baseUrl);
        html = html.replace("{0}", baseUrl);

        int i = 1;
        for (String replacementValue : replacementValues) {
            subject = subject.replace("{" + i + "}", replacementValue);
            text = text.replace("{" + i + "}", replacementValue);
            html = html.replace("{" + i + "}", replacementValue);
            i++;
        }
        subject = replaceResources(subject);
        text = replaceResources(text);
        html = replaceResources(html);

        Email email = new Email();
        email.setSubject(subject);
        email.setText(text);
        email.setHtml(html);
        return email;
    }

    private String replaceResources(String text) {
        // Searches for resource keys in the form ${key}
        // and replaces with with the referenced resource
        ResourceBundle bundle = getResourceBundle();
        String keyRegex = "\\$\\{([^}]*)\\}";
        Pattern p = Pattern.compile(keyRegex);
        Matcher m = p.matcher(text);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            String key = m.group(1);
            String replacement = bundle.getString(key);
            m.appendReplacement(sb, replacement);
        }
        m.appendTail(sb);
        return sb.toString();
    }

    /**
     * get the resource bundle.
     * 
     * @return the resource bundle
     */
    public ResourceBundle getResourceBundle() {
        return ResourceBundle.getBundle("ApplicationResources");
    }

    /**
     * get a date format using the default date format specified in the resource bundle.
     * 
     * @return the default date format
     */
    public SimpleDateFormat getDefaultDateFormat() {
        String format = getResourceBundle().getString("default.date.format");
        return new SimpleDateFormat(format, Locale.getDefault());
    }

}
