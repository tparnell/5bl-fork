/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.Digits;
import org.hibernate.validator.Length;
import org.hibernate.validator.Min;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.Searchable;
import com.fiveamsolutions.tissuelocator.data.annotation.OnSave;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.hibernate.interceptor.DeleteOrphans;

/**
 * @author ddasgupta
 */
@Entity(name = "shipment")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"request_id", "sending_institution_id" }))
@com.fiveamsolutions.tissuelocator.data.validation.UniqueConstraint(
        propertyNames = {"request", "sendingInstitution" }, message = "{validator.shipment.duplicate}")
@DeleteOrphans(targetProperties = { "mtaAmendment" }, orphanClasses = { LobHolder.class },
        idProperties = { "lob.id" })
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@SuppressWarnings({ "PMD.TooManyFields", "PMD.ExcessiveClassLength" })
public class Shipment implements PersistentObject, Auditable, AccessRestricted {

    private static final long serialVersionUID = -9187129120687328439L;
    private static final int LENGTH = 254;
    private static final int INT_DIGITS = 17;
    private static final int NOTES_LENGTH = 3999;

    private Long id;
    private ShipmentStatus status;
    private Date updatedDate;
    private Date shipmentDate;
    private Date nextReminderDate;
    private SpecimenRequest request;

    private Institution sendingInstitution;
    private TissueLocatorUser sender;
    private TissueLocatorUser processingUser;
    private Person recipient;
    private Person billingRecipient;
    private String shippingInstructions;
    private ShippingMethod shippingMethod;
    private String trackingNumber;
    private BigDecimal shippingPrice;
    private BigDecimal fees;
    private Set<SpecimenRequestLineItem> lineItems = new HashSet<SpecimenRequestLineItem>();
    private Set<AggregateSpecimenRequestLineItem> aggregateLineItems = new HashSet<AggregateSpecimenRequestLineItem>();
    private String notes;
    private TissueLocatorFile mtaAmendment;
    private SignedMaterialTransferAgreement signedSenderMta;
    private SignedMaterialTransferAgreement signedRecipientMta;

    private boolean readyForResearcherReview;
    private boolean previousReadyForResearcherReview;

    /**
     * {@inheritDoc}
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the status
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Searchable
    @Index(name = "shipment_status_idx")
    public ShipmentStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(ShipmentStatus status) {
        this.status = status;
    }

    /**
     * @return the updatedDate
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * @param updatedDate the updatedDate to set
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * @return the shipmentDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "shipment_date")
    public Date getShipmentDate() {
        return shipmentDate;
    }

    /**
     * @param shipmentDate the shipmentDate to set
     */
    public void setShipmentDate(Date shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    /**
     * @return the nextReminderDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "next_reminder_date")
    public Date getNextReminderDate() {
        return nextReminderDate;
    }

    /**
     * @param nextReminderDate the nextReminderDate to set
     */
    public void setNextReminderDate(Date nextReminderDate) {
        this.nextReminderDate = nextReminderDate;
    }

    /**
     * @return the request
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "request_id")
    @ForeignKey(name = "shipment_request_fk")
    @Index(name = "shipment_request_idx")
    public SpecimenRequest getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(SpecimenRequest request) {
        this.request = request;
    }

    /**
     * @return the sendingInstitution
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "sending_institution_id")
    @ForeignKey(name = "shipment_sending_institution_fk")
    @Index(name = "shipment_sending_institution_idx")
    @Searchable
    public Institution getSendingInstitution() {
        return sendingInstitution;
    }

    /**
     * @param sendingInstitution the sendingInstitution to set
     */
    public void setSendingInstitution(Institution sendingInstitution) {
        this.sendingInstitution = sendingInstitution;
    }

    /**
     * @return the sender
     */
    @ManyToOne
    @JoinColumn(name = "sender_id")
    @ForeignKey(name = "shipment_sender_fk")
    @Index(name = "shipment_sender_idx")
    public TissueLocatorUser getSender() {
        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(TissueLocatorUser sender) {
        this.sender = sender;
    }

    /**
     * @return the processingUser
     */
    @ManyToOne
    @JoinColumn(name = "processing_user_id")
    @ForeignKey(name = "shipment_processing_user_fk")
    @Index(name = "shipment_processing_user_idx")
    public TissueLocatorUser getProcessingUser() {
        return processingUser;
    }

    /**
     * @param processingUser the processingUser to set
     */
    public void setProcessingUser(TissueLocatorUser processingUser) {
        this.processingUser = processingUser;
    }

    /**
     * @return the recipient
     */
    @Valid
    @NotNull
    @ManyToOne
    @JoinColumn(name = "recipient_id")
    @ForeignKey(name = "shipment_recipient_fk")
    @Index(name = "shipment_recipient_idx")
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    public Person getRecipient() {
        return recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(Person recipient) {
        this.recipient = recipient;
    }
    

    /**
     * @return the billingRecipient
     */
    @Valid
    @ManyToOne
    @Cascade({ CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @JoinColumn(name = "billing_recipient_id")
    @ForeignKey(name = "shipment_billing_recipient_fk")
    public Person getBillingRecipient() {
        return billingRecipient;
    }

    /**
     * @param billingRecipient the billingRecipient to set
     */
    public void setBillingRecipient(Person billingRecipient) {
        this.billingRecipient = billingRecipient;
    }

    /**
     * @return the shippingMethod
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "shipping_method")
    public ShippingMethod getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod the shippingMethod to set
     */
    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return the trackingNumber
     */
    @Length(max = LENGTH)
    @Column(name = "tracking_number")
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the shippingPrice
     */
    @Min(value = 0)
    @Digits(integerDigits = INT_DIGITS, fractionalDigits = 2)
    @Column(name = "shipping_price")
    public BigDecimal getShippingPrice() {
        return shippingPrice;
    }

    /**
     * @param shippingPrice the shippingPrice to set
     */
    public void setShippingPrice(BigDecimal shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    /**
     * @return the fees
     */
    @Min(value = 0)
    @Digits(integerDigits = INT_DIGITS, fractionalDigits = 2)
    public BigDecimal getFees() {
        return fees;
    }

    /**
     * @param fees the fees to set
     */
    public void setFees(BigDecimal fees) {
        this.fees = fees;
    }

    /**
     * @return the lineItems
     */
    @ManyToMany
    @Valid
    @JoinTable(name = "shipment_line_items",
                joinColumns = @JoinColumn(name = "shipment_id"),
                inverseJoinColumns = @JoinColumn(name = "line_item_id"))
    @ForeignKey(name = "line_item_shipment_fk",
                inverseName = "shipment_line_item_fk")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = CascadeType.ALL)
    @OrderBy(value = "id")
    public Set<SpecimenRequestLineItem> getLineItems() {
        return lineItems;
    }

    /**
     * @param lineItems the lineItems to set
     */
    public void setLineItems(Set<SpecimenRequestLineItem> lineItems) {
        this.lineItems = lineItems;
    }

    /**
     * @return the aggregate lineItems
     */
    @ManyToMany
    @Valid
    @JoinTable(name = "shipment_aggregate_line_items",
                joinColumns = @JoinColumn(name = "shipment_id"),
                inverseJoinColumns = @JoinColumn(name = "line_item_id"))
    @ForeignKey(name = "AGGREGATE_LINE_ITEM_SHIPMENT_FK",
                inverseName = "SHIPMENT_AGGREGATE_LINE_ITEM_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = CascadeType.ALL)
    @OrderBy(value = "id")
    public Set<AggregateSpecimenRequestLineItem> getAggregateLineItems() {
        return aggregateLineItems;
    }

    /**
     * @param aggregateLineItems the lineItems to set
     */
    public void setAggregateLineItems(Set<AggregateSpecimenRequestLineItem> aggregateLineItems) {
        this.aggregateLineItems = aggregateLineItems;
    }

    /**
     * @return the shipment notes
     */
    @Length(max = NOTES_LENGTH)
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the shipment notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the shippingInstructions
     */
    @Length(max = NOTES_LENGTH)
    @Column(name = "shipping_instructions")
    public String getShippingInstructions() {
        return shippingInstructions;
    }

    /**
     * @param shippingInstructions the shippingInstructions to set
     */
    public void setShippingInstructions(String shippingInstructions) {
        this.shippingInstructions = shippingInstructions;
    }

    /**
     * calculate the total price.
     * @return the total
     */
    @Transient
    public BigDecimal getTotalPrice() {
        BigDecimal total = BigDecimal.ZERO;
        total = total.add(getLineItemTotalPrice());
        total = total.add(getAggregateLineItemTotalPrice());

        if (getFees() != null) {
            total = total.add(getFees());
        }
        if (getShippingPrice() != null) {
            total = total.add(getShippingPrice());
        }
        return total;
    }

    @Transient
    private BigDecimal getLineItemTotalPrice() {
        BigDecimal total = BigDecimal.ZERO;
        for (SpecimenRequestLineItem li : getLineItems()) {
            if (li.getFinalPrice() != null) {
                total = total.add(li.getFinalPrice());
            }
        }
        return total;
    }

    @Transient
    private BigDecimal getAggregateLineItemTotalPrice() {
        BigDecimal total = BigDecimal.ZERO;
        for (AggregateSpecimenRequestLineItem li : getAggregateLineItems()) {
            if (li.getFinalPrice() != null) {
                total = total.add(li.getFinalPrice());
            }
        }
        return total;
    }

    /**
     * @return the number of specimens in this shipment.
     */
    @Transient
    public int getSpecimenCount() {
        int count = getLineItems().size();
        for (AggregateSpecimenRequestLineItem li : getAggregateLineItems()) {
            count += li.getQuantity();
        }
        return count;
    }

    /**
     * @return the mtaAmendment
     */
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "contentType", column = @Column(name = "mta_amendment_content_type")),
        @AttributeOverride(name = "name", column = @Column(name = "mta_amendment_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = "lob", joinColumns = @JoinColumn(name = "mta_amendment_lob_id"))
    })
    public TissueLocatorFile getMtaAmendment() {
        return mtaAmendment;
    }

    /**
     * @param mtaAmendment the mtaAmendment to set
     */
    public void setMtaAmendment(TissueLocatorFile mtaAmendment) {
        this.mtaAmendment = mtaAmendment;
    }

    /**
     * @return the signedSenderMta
     */
    @ManyToOne
    @JoinColumn(name = "signed_sender_mta_id")
    @ForeignKey(name = "SENDER_MTA_FK")
    public SignedMaterialTransferAgreement getSignedSenderMta() {
        return signedSenderMta;
    }

    /**
     * @param signedSenderMta the signedSenderMta to set
     */
    public void setSignedSenderMta(SignedMaterialTransferAgreement signedSenderMta) {
        this.signedSenderMta = signedSenderMta;
    }

    /**
     * @return the signedRecipientMta
     */
    @ManyToOne
    @JoinColumn(name = "signed_recipient_mta_id")
    @ForeignKey(name = "RECIPIENT_MTA_FK")
    public SignedMaterialTransferAgreement getSignedRecipientMta() {
        return signedRecipientMta;
    }

    /**
     * @param signedRecipientMta the signedRecipientMta to set
     */
    public void setSignedRecipientMta(
            SignedMaterialTransferAgreement signedRecipientMta) {
        this.signedRecipientMta = signedRecipientMta;
    }

    /**
     * Determines whether this order needs a biospecimen quality update by the given user.
     * @param user the user potentially performing the biospecimen quality update
     * @return an indication of whether this order needs a biospecimen quality update from the user
     */
    @Transient
    public boolean needsQualityUpdate(TissueLocatorUser user) {
        if (!ShipmentStatus.SHIPPED.equals(getStatus()) && !ShipmentStatus.RECEIVED.equals(getStatus())) {
            return false;
        }

        boolean needsUpdate = lineItemsNeedQualityUpdate();

        return getRequest().getRequestor().equals(user) && needsUpdate;
    }

    @Transient
    private boolean lineItemsNeedQualityUpdate() {
        for (SpecimenRequestLineItem li : getLineItems()) {
            if (li.getReceiptQuality() == null) {
                return true;
            }
        }
        for (AggregateSpecimenRequestLineItem li : getAggregateLineItems()) {
            if (li.getReceiptQuality() == null) {
                return true;
            }
        }
        return false;
    }


    /**
     * @param user - the user who is trying to access the shipment.
     * @return - whether this user has access to this shipment.
     *
     *         A user can access a shipment if he is the original requestor, or has the
     *         SHIPMENT_ADMIN role, the LEAD_REVIEWER role, or the
     *         LINE_ITEM_REVIEW_VOTER role.
     *
     *         Requestor is self explanatory.
     *
     *         A SHIPMENT_ADMIN needs access to the shipment if he is in the institution of the shipment and might need
     *         to mark it as shipped.
     *
     *         LEAD_REVIEWER creates the shipment by doing the final vote.
     *
     *         LINE_ITEM_REVIEW_VOTER creates the shipment by approving the line items of
     *         a request pertaining to a particular institution.
     */
    public boolean isAccessible(TissueLocatorUser user) {
        if (user.getId().equals(getRequest().getRequestor().getId())) {
            return true;
        } else if ((user.inRole(Role.SHIPMENT_ADMIN) || user.inRole(Role.LINE_ITEM_REVIEW_VOTER))
                && getSendingInstitution().getId().equals(user.getInstitution().getId())
                || user.inRole(Role.LEAD_REVIEWER)) {
            return true;
        }
        return false;
    }

    /**
     * @return the readyForResearcherReview
     */
    @NotNull
    @Column(name = "ready_for_researcher_review")
    public boolean isReadyForResearcherReview() {
        return readyForResearcherReview;
    }

    /**
     * @param readyForResearcherReview the readyForResearcherReview to set
     */
    public void setReadyForResearcherReview(boolean readyForResearcherReview) {
        this.readyForResearcherReview = readyForResearcherReview;
}

    /**
     * @return the previousReadyForResearcherReview
     */
    @Column(name = "ready_for_researcher_review", updatable = false, insertable = false, nullable = false)
    public boolean isPreviousReadyForResearcherReview() {
        return previousReadyForResearcherReview;
    }

    /**
     * @param previousReadyForResearcherReview the previousReadyForResearcherReview to set
     */
    public void setPreviousReadyForResearcherReview(boolean previousReadyForResearcherReview) {
        this.previousReadyForResearcherReview = previousReadyForResearcherReview;
    }

    /**
     * Update the status transition history of this and associated objects.
     */
    @OnSave
    public void updateStatusTransitionHistory() {
        getRequest().updateStatusTransitionHistory();
        for (SpecimenRequestLineItem lineItem : getLineItems()) {
            lineItem.getSpecimen().updateStatusTransitionHistory();
        }
    }
}
