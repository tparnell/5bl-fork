/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import com.fiveamsolutions.tissuelocator.data.Specimen;

/**
 * @author bpickeral
 *
 */
@Remote
public interface SpecimenServiceRemote {

    /**
     * Remote API to get specimen for an institution by external identifier.
     * @param institutionId id of the Institution
     * @param extIds external identifiers of the specimens
     * @return a map of specimen external identifier to specimen
     */
    Map<String, Specimen> getSpecimens(Long institutionId, Collection<String> extIds);

    /**
     * Remote API for creating/saving a Specimen.
     * @param specimen Specimen object to create/save
     * @return Specimen object saved
     */
    Specimen importSpecimen(Specimen specimen);

    /**
     * Get all specimen that are active.
     * @param page the offset in to the result to retrun
     * @param pageSize the max number of records to return
     * @return the list of specimen
     */
    List<Specimen> getAvailableSpecimens(int page, int pageSize);
}
