/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.config.search;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;

/**
 * Search field configuration corresponding to a text field
 * qualified by a selectable value.
 * @author gvaughn
 *
 */
@Entity
@Table(name = "text_select_composite_config")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class TextSelectCompositeConfig extends AbstractSearchFieldConfig {

    private static final long serialVersionUID = -2021968108292297229L;

    private TextConfig textConfig;
    private SelectConfig selectConfig;

    /**
     * @return the textConfig
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "text_config_id")
    @ForeignKey(name = "text_select_fk")
    public TextConfig getTextConfig() {
        return textConfig;
    }

    /**
     * @param textConfig the textConfig to set
     */
    public void setTextConfig(TextConfig textConfig) {
        this.textConfig = textConfig;
    }

    /**
     * @return the selectConfig
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "select_config_id")
    @ForeignKey(name = "select_text_fk")
    public SelectConfig getSelectConfig() {
        return selectConfig;
    }

    /**
     * @param selectConfig the selectConfig to set
     */
    public void setSelectConfig(SelectConfig selectConfig) {
        this.selectConfig = selectConfig;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public String getSearchFieldDisplayName() {
        return getTextConfig().getSearchFieldDisplayName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public ApplicationRole getRequiredRole() {
        return getTextConfig().getRequiredRole();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public GenericFieldConfig getFieldConfig() {
        return getTextConfig().getFieldConfig();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCriteriaString(Object object, String mapName) {
        String textString = getTextConfig().getValueString(object, mapName);
        String selectString = getSelectConfig().getValueString(object, mapName);

        if (StringUtils.isBlank(textString) && (StringUtils.isBlank(selectString)
                || !getSelectConfig().isShowEmptyOption())) {
            return super.getCriteriaString(object, mapName);
        }

        StringBuffer criteria = new StringBuffer();
        criteria.append(getTextConfig().getSearchFieldDisplayName());
        criteria.append(CONNECTOR);
        criteria.append(StringUtils.trim(StringUtils.join(new String[] {textString, " ", selectString})));
        return criteria.toString();
    }

    /**
     * Returns the name of the property being searched.
     *
     * @return the property name
     */
    @Override
    @Transient
    public String getSearchFieldName() {
        return getTextConfig().getSearchFieldName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<AbstractSearchFieldConfig, Object> getValues(Object object, String mapName) {
        Map<AbstractSearchFieldConfig, Object> configMap = new HashMap<AbstractSearchFieldConfig, Object>();
        configMap.putAll(getTextConfig().getValues(object, mapName));
        configMap.putAll(getSelectConfig().getValues(object, mapName));
        return configMap;
    }


}
