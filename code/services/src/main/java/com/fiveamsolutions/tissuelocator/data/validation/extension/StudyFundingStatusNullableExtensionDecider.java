/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.validation.extension;

import org.apache.commons.lang.ObjectUtils;

import com.fiveamsolutions.dynamicextensions.ExtendableEntity;
import com.fiveamsolutions.tissuelocator.data.FundingStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.Study;

/**
 * This NullableExtensionDecider should be configured for use on extensions to SpecimenRequest, because
 * the implementation of isExtensionNullable assumes that the incoming ExtendableEntity is a
 * SpecimenRequest.
 *
 * This NullableExtensionDecider allows extensions to SpecimenRequest to be null when validation is
 * disabled, or when when the fundingStatus property of the SpecimenRequest's study object equals FUNDED
 *
 * @author ddasgupta
 *
 */
public class StudyFundingStatusNullableExtensionDecider extends DisableableNullableExtensionDecider {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isExtensionNullable(ExtendableEntity entity) {
        if (super.isExtensionNullable(entity)) {
            return true;
        }

        Study study = ((SpecimenRequest) entity).getStudy();
        return !ObjectUtils.equals(FundingStatus.FUNDED, study.getFundingStatus());
    }
}
