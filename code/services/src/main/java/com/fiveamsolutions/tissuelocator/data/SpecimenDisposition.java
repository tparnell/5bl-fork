/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

/**
 * @author ddasgupta
 */
public enum SpecimenDisposition {

    /**
     * In use.
     */
    IN_USE("specimenDisposition.inUse"),

    /**
     * Fully consumed.
     */
    FULLY_CONSUMED("specimenDisposition.fullyConsumed"),

    /**
     * Partly consumed or unused, with the residual being returned to the source institution.
     */
    PARTIALLY_CONSUMED_RETURNED("specimenDisposition.partiallyConsumedReturned"),
    
    /**
     * Specimen fully returned to the source institution.
     */
    FULLY_RETURNED("specimenDisposition.fullyReturned"),

    /**
     * Partly consumed or unused, with the residual being destroyed.
     */
    PARTIALLY_CONSUMED_DESTROYED("specimenDisposition.partiallyConsumedDestroyed"),

    /**
     * Damaged and unusable/destroyed.
     */
    DAMAGED("specimenDisposition.damaged");
    
    private static final Map<SpecimenDisposition, List<SpecimenDisposition>> TRANSITIONS;
    static {
        TRANSITIONS = new HashMap<SpecimenDisposition, List<SpecimenDisposition>>();
        List<SpecimenDisposition> t = new ArrayList<SpecimenDisposition>();
        CollectionUtils.addAll(t, values());
        TRANSITIONS.put(IN_USE, t);
        TRANSITIONS.put(FULLY_CONSUMED, t);
        TRANSITIONS.put(PARTIALLY_CONSUMED_RETURNED, t);
        TRANSITIONS.put(FULLY_RETURNED, t);
        
        t = new ArrayList<SpecimenDisposition>();
        t.add(PARTIALLY_CONSUMED_DESTROYED);
        t.add(DAMAGED);
        TRANSITIONS.put(PARTIALLY_CONSUMED_DESTROYED, t);
        TRANSITIONS.put(DAMAGED, t);
    }


    private String resourceKey;

    /**
     * Constructor.
     * @param resourceKey the key of the human readable text
     */
    private SpecimenDisposition(String resourceKey) {
        this.resourceKey = resourceKey;
    }

    /**
     * @return the resourceKey
     */
    public String getResourceKey() {
        return resourceKey;
    }
    
    /**
     * @return the allowedTransitions
     */
    public List<SpecimenDisposition> getAllowedTransitions() {
        return TRANSITIONS.get(this);
    }
}
