/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.locator;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.util.JndiUtils;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceLocal;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceRemote;
import com.fiveamsolutions.tissuelocator.service.FixedConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.FullConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.HelpConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceRemote;
import com.fiveamsolutions.tissuelocator.service.LineItemReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceRemote;
import com.fiveamsolutions.tissuelocator.service.ReportServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SignedMaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestProcessingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;

/**
 * JNDI backed implementation of the service locator.
 *
 * @author smiller
 */
@SuppressWarnings("PMD.CouplingBetweenObjects")
public class JndiServiceLocator implements ServiceLocator {

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public GenericServiceLocal<PersistentObject> getGenericService() {
        return (GenericServiceLocal) JndiUtils.lookup("tissuelocator/GenericServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InstitutionServiceLocal getInstitutionService() {
        return (InstitutionServiceLocal) JndiUtils.lookup("tissuelocator/InstitutionServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    public InstitutionServiceRemote getInstitutionServiceRemote() {
        return (InstitutionServiceRemote) JndiUtils.lookup("tissuelocator/InstitutionServiceRemoteBean/remote");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CodeServiceLocal getCodeService() {
        return (CodeServiceLocal) JndiUtils.lookup("tissuelocator/CodeServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    public SpecimenServiceRemote getSpecimenServiceRemote() {
        return (SpecimenServiceRemote) JndiUtils.lookup("tissuelocator/SpecimenServiceRemoteBean/remote");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenServiceLocal getSpecimenService() {
        return (SpecimenServiceLocal) JndiUtils.lookup("tissuelocator/SpecimenServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectionProtocolServiceLocal getCollectionProtocolService() {
        return (CollectionProtocolServiceLocal) JndiUtils.lookup("tissuelocator/CollectionProtocolServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    public CollectionProtocolServiceRemote getCollectionProtocolServiceRemote() {
        return (CollectionProtocolServiceRemote) JndiUtils
                .lookup("tissuelocator/CollectionProtocolServiceRemoteBean/remote");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ParticipantServiceLocal getParticipantService() {
        return (ParticipantServiceLocal) JndiUtils.lookup("tissuelocator/ParticipantServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    public ParticipantServiceRemote getParticipantServiceRemote() {
        return (ParticipantServiceRemote) JndiUtils.lookup("tissuelocator/ParticipantServiceBean/remote");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenRequestServiceLocal getSpecimenRequestService() {
        return (SpecimenRequestServiceLocal) JndiUtils.lookup("tissuelocator/SpecimenRequestServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShipmentServiceLocal getShipmentService() {
        return (ShipmentServiceLocal) JndiUtils.lookup("tissuelocator/ShipmentServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenRequestProcessingServiceLocal getSpecimenRequestProcessingService() {
        return (SpecimenRequestProcessingServiceLocal) JndiUtils
                .lookup("tissuelocator/SpecimenRequestProcessingServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FixedConsortiumReviewServiceLocal getFixedConsortiumReviewService() {
        return (FixedConsortiumReviewServiceLocal) JndiUtils
                .lookup("tissuelocator/FixedConsortiumReviewServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FullConsortiumReviewServiceLocal getFullConsortiumReviewService() {
        return (FullConsortiumReviewServiceLocal) JndiUtils
                .lookup("tissuelocator/FullConsortiumReviewServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LineItemReviewServiceLocal getLineItemReviewService() {
        return (LineItemReviewServiceLocal) JndiUtils.lookup("tissuelocator/LineItemReviewServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MaterialTransferAgreementServiceLocal getMaterialTransferAgreementService() {
        return (MaterialTransferAgreementServiceLocal) JndiUtils
                .lookup("tissuelocator/MaterialTransferAgreementServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SignedMaterialTransferAgreementServiceLocal getSignedMaterialTransferAgreementService() {
        return (SignedMaterialTransferAgreementServiceLocal) JndiUtils
                .lookup("tissuelocator/SignedMaterialTransferAgreementServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReportServiceLocal getReportService() {
        return (ReportServiceLocal) JndiUtils.lookup("tissuelocator/ReportServiceBean/local");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HelpConfigServiceLocal getHelpConfigService() {
        return (HelpConfigServiceLocal) JndiUtils.lookup("tissuelocator/HelpConfigServiceBean/local");
    }
}
