/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import java.io.IOException;
import java.util.List;

import javax.ejb.Local;

import com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement;

/**
 * @author ddasgupta
 *
 */
@Local
public interface MaterialTransferAgreementServiceLocal extends GenericServiceLocal<MaterialTransferAgreement> {

    /**
     * Sets up a new unsigned material transfer agreement.  This method does the following:
     * 1.  Save the new MTA to the database
     * 2.  Marks all signed MTAs for the previous MTA version as out of date
     * 3.  Notifies Institutional Administrators of consortium member institutions that the new MTA must be signed.
     * 4.  Notifies all MTA contacts of non-consortium member institutions that previously had a signed MTA that
     *     the new MTA must be signed
     * @param mta the mta to save
     * @return the id of the new MTA
     * @throws IOException on error
     */
    Long saveMaterialTransferAgreement(MaterialTransferAgreement mta) throws IOException;

    /**
     * Get all MTAs in the database, in order of decreasing upload time.
     * @return a list of MTAs order by decreasing upload time
     */
    List<MaterialTransferAgreement> getAll();

    /**
     * Get the current MTA, which is the most recently uploaded one.
     * @return the current MTA
     */
    MaterialTransferAgreement getCurrentMta();
}
