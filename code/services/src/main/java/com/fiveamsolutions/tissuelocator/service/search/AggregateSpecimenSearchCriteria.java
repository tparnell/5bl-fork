/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.search.GroupableSearchCriteria;
import com.fiveamsolutions.nci.commons.search.SearchableUtils;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author bhumphrey
 *
 */
public class AggregateSpecimenSearchCriteria extends SpecimenSearchCriteria
        implements GroupableSearchCriteria<Institution, Specimen> {

    private static final long serialVersionUID = 1L;
    private int countThreshold;
    private String countThresholdOperator;
    
    /**
     * Constructor with additional options for specimen search.
     *
     * @param example the example specimen
     * @param minimumAvailableQuantity the minimum available quantity
     * @param multiSelectCollectionParamMap the patient races
     * @param multiSelectParamValues Mapping of object property name to search param values.
     * @param searchConditions list of search conditions
     */
    @SuppressWarnings("PMD.ExcessiveParameterList")
    //CHECKSTYLE:OFF - Too many parameters
    public AggregateSpecimenSearchCriteria(Specimen example, BigDecimal minimumAvailableQuantity, Map<String, Collection<Object>> multiSelectCollectionParamMap,
            Map<String, Collection<Object>> multiSelectParamValues, Collection<SearchCondition> searchConditions) {
      //CHECKSTYLE:ON
        super(example, minimumAvailableQuantity, multiSelectCollectionParamMap, 
                multiSelectParamValues, searchConditions);
    }

    /**
     * {@inheritDoc}
     */
    public Query getQuery(String orderByProperty, String leftJoinClause, String groupByProperty, boolean isCountOnly) {
        return (countThresholdOperator == null 
                ? SearchableUtils.getQueryBySearchableFields(getCriteria(), isCountOnly, null, groupByProperty,
                        leftJoinClause, getSession(), getAfterIterationHelper())
                : SearchableUtils.getQueryBySearchableFields(getCriteria(), isCountOnly, null, groupByProperty,
                        leftJoinClause, getSession(), getAfterIterationHelper(), countThreshold, 
                        countThresholdOperator));
    }

    /**
     * {@inheritDoc}
     */
    public Institution build(Object[] results) {
        return TissueLocatorRegistry.getServiceLocator().getInstitutionService().getInstitution(
                (Long) results[0]);
    }
    
    /**
     * Set the minimum count for which results should be returned.
     * @param count the minimum count for which results should be returned.
     */
    public void setMinimumCountThreshold(int count) {
        this.countThreshold = count;
        countThresholdOperator = SearchableUtils.GTE;
    }
    
    /**
     * Set the maximum count for which results should be returned.
     * @param count the maximum count for which results should be returned.
     */
    public void setMaximumCountThreshold(int count) {
        this.countThreshold = count;
        countThresholdOperator = SearchableUtils.LTE;
    }

}
