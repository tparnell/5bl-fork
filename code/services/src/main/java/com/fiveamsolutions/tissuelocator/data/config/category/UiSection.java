/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config.category;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OrderBy;
import org.hibernate.validator.NotEmpty;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;

/**
 * User interface section.
 *
 * @author jstephens
 */
@Entity
@Table(name = "ui_section")
public class UiSection implements PersistentObject {

    private static final long serialVersionUID = 3707158595346559769L;
    private Long id;
    private String sectionName;
    private String entityClassName;
    private List<UiDynamicFieldCategory> uiDynamicFieldCategories;
    private List<UiSearchFieldCategory> uiSearchFieldCategories;

    /**
     * Maximum string field length.
     */
    protected static final int MAX_STRING_LENGTH = 255;

    /**
     * Default constructor.
     */
    public UiSection() {
        uiDynamicFieldCategories = new ArrayList<UiDynamicFieldCategory>();
        uiSearchFieldCategories = new ArrayList<UiSearchFieldCategory>();
    }

    /**
     * Gets the id of this section.
     *
     * @return the section id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * Sets the id of this section.
     *
     * @param id the section id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the name of this section.
     *
     * @return the section name
     */
    @NotEmpty
    @Column(name = "section_name", nullable = false, length = MAX_STRING_LENGTH)
    public String getSectionName() {
        return sectionName;
    }

    /**
     * Sets the name of this section.
     *
     * @param sectionName the section name
     */
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    /**
     * Gets the class name of the entity to which this section belongs.
     *
     * @return the entity class name to which this section belongs
     */
    @NotEmpty
    @Column(name = "entity_class_name", nullable = false, length = MAX_STRING_LENGTH)
    public String getEntityClassName() {
        return entityClassName;
    }

    /**
     * Sets the class name of the entity to which this section belongs.
     *
     * @param entityClassName the class name of the specified entity
     */
    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    /**
     * Gets the list of dynamic field categories that belong to this section.
     *
     * @return a list of dynamic field categories
     */
    @OneToMany
    @JoinTable(name = "ui_section_ui_dynamic_field_category",
        joinColumns = @JoinColumn(name = "ui_section_id"),
        inverseJoinColumns = @JoinColumn(name = "ui_dynamic_field_category_id"))
    @OrderBy(clause = "ui_section_id ASC")
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<UiDynamicFieldCategory> getUiDynamicFieldCategories() {
        return uiDynamicFieldCategories;
    }

    /**
     * Sets the list of dynamic field categories that belong to this section.
     *
     * @param uiDynamicFieldCategories a list of dynamic field categories
     */
    public void setUiDynamicFieldCategories(List<UiDynamicFieldCategory> uiDynamicFieldCategories) {
        this.uiDynamicFieldCategories = uiDynamicFieldCategories;
    }

    /**
     * Gets the list of search field categories that belong to this section.
     *
     * @return a list of search field categories
     */
    @OneToMany
    @JoinTable(name = "ui_section_ui_search_field_category",
        joinColumns = @JoinColumn(name = "ui_section_id"),
        inverseJoinColumns = @JoinColumn(name = "ui_search_field_category_id"))
    @OrderBy(clause = "ui_section_id ASC")
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<UiSearchFieldCategory> getUiSearchFieldCategories() {
        return uiSearchFieldCategories;
    }

    /**
     * Sets the list of search field categories that belong to this section.
     *
     * @param uiSearchFieldCategories a list of search field categories
     */
    public void setUiSearchFieldCategories(List<UiSearchFieldCategory> uiSearchFieldCategories) {
        this.uiSearchFieldCategories = uiSearchFieldCategories;
    }

}