/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data.validation;

import java.io.Serializable;
import java.util.EnumMap;

import org.hibernate.mapping.Property;
import org.hibernate.validator.PropertyConstraint;
import org.hibernate.validator.Validator;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.util.TimeConverter;

/**
 * Verify the patient age at collection does not exceed HIPPA limits.
 * @author akong
 */
public class SpecimenPatientAgeValidator
    implements Validator<SpecimenPatientAge>, PropertyConstraint, Serializable {

    private static final long serialVersionUID = -1084082261265174629L;
    
    private static final EnumMap<TimeUnits, Integer> MAX_AGE_MAP = new EnumMap<TimeUnits, Integer>(TimeUnits.class);
    private static final int MAX_AGE_IN_YEARS = 90;
    
    static {        
        MAX_AGE_MAP.put(TimeUnits.YEARS, MAX_AGE_IN_YEARS);
        MAX_AGE_MAP.put(TimeUnits.MONTHS, 
                (int) TimeConverter.convert(MAX_AGE_IN_YEARS, TimeUnits.YEARS, TimeUnits.MONTHS));
        MAX_AGE_MAP.put(TimeUnits.DAYS, 
                (int) TimeConverter.convert(MAX_AGE_IN_YEARS, TimeUnits.YEARS, TimeUnits.DAYS));
        MAX_AGE_MAP.put(TimeUnits.HOURS, 
                (int) TimeConverter.convert(MAX_AGE_IN_YEARS, TimeUnits.YEARS, TimeUnits.HOURS));
    }
    
    /**
     * Returns the maximum age allowed per HIPPA standards.
     * 
     * @param timeUnit The time unit of the max value to retrieve
     * @return the maximum allowed age per HIPPA standards.  Will return null
     *         if the passed in time unit is not valid.
     */
    public static final Integer getHippaMaxAge(TimeUnits timeUnit) {        
        return MAX_AGE_MAP.get(timeUnit);
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean isValid(Object value) {
        if (!(value instanceof Specimen)) {
            return false;
        }

        // Retrieve patient age values from the object
        Specimen specimen = (Specimen) value;
        int patientAge = specimen.getPatientAgeAtCollection();
        TimeUnits timeUnits = specimen.getPatientAgeAtCollectionUnits();
        
        // Check that the age does not exceed maximum  
        Integer maxAgeValue = getHippaMaxAge(timeUnits);                
        if ((maxAgeValue == null) || (patientAge > maxAgeValue)) {
            return false;
        }
        
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public void initialize(SpecimenPatientAge params) {
        // do nothing
    }

    /**
     * {@inheritDoc}
     */
    public void apply(Property property) {
        // do nothing
    }
}
