/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.Searchable;
import com.fiveamsolutions.tissuelocator.data.validation.UniqueConstraint;

/**
 * @author smiller
 *
 */
@Entity(name = "participant")
@UniqueConstraint(propertyNames = {"externalId", "externalIdAssigner" }, message = "{validator.participant.duplicate}")
//Note that the case insensitive unique constraint for this table in the database is specified in plain sql.
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@XmlRootElement(name = "participant")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Participant", propOrder = {
    "ethnicity",
    "races",
    "gender",
    "externalId"
})
public class Participant implements PersistentObject, InstitutionRestricted, Auditable {

    private static final long serialVersionUID = 1L;
    private static final int MAX_LENGTH = 254;

    @XmlTransient
    private Long id;
    private Ethnicity ethnicity;
    @XmlElement(name = "race")
    private List<Race> races;
    private Gender gender;
    private String externalId;
    @XmlTransient
    private Institution externalIdAssigner;
    @XmlTransient
    private Set<Specimen> specimens;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Note that the case insensitive index for this column in the database is specified in plain sql.
     * @return the externalId
     */
    @NotEmpty
    @Length(max = MAX_LENGTH)
    @Searchable(caseSensitive = false, matchMode = Searchable.MATCH_MODE_CONTAINS)
    public String getExternalId() {
        return externalId;
    }

    /**
     * @param externalId the externalId to set
     */
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    /**
     * @return the ethnicity
     */
    @Enumerated(EnumType.STRING)
    @Index(name = "participant_ethnicity_idx")
    public Ethnicity getEthnicity() {
        return ethnicity;
    }

    /**
     * @param ethnicity the ethnicity to set
     */
    public void setEthnicity(Ethnicity ethnicity) {
        this.ethnicity = ethnicity;
    }

    /**
     * @return the gender
     */
    @Enumerated(EnumType.STRING)
    @Index(name = "participant_gender_idx")
    public Gender getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    /**
     * Indexes for both columns of the join table for this property are created in plain sql.
     * @return the races
     */
    @CollectionOfElements(fetch = FetchType.LAZY)
    @JoinTable(name = "participant_races", joinColumns = @JoinColumn(name = "participant_id", nullable = false))
    @Column(name = "element", nullable = false)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    public List<Race> getRaces() {
        return races;
    }

    /**
     * @param races the races to set
     */
    public void setRaces(List<Race> races) {
        this.races = races;
    }

    /**
     * @return the externalIdAssigner
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "external_id_assigner_id")
    @ForeignKey(name = "participant_institution_fk")
    @Index(name = "participant_institution_idx")
    @Searchable(caseSensitive = false, matchMode = Searchable.MATCH_MODE_CONTAINS, fields = "name")
    public Institution getExternalIdAssigner() {
        return externalIdAssigner;
    }

    /**
     * @param externalIdAssigner the externalIdAssigner to set
     */
    public void setExternalIdAssigner(Institution externalIdAssigner) {
        this.externalIdAssigner = externalIdAssigner;
    }

    /**
     * @return the specimens
     */
    @OneToMany(mappedBy = "participant")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    public Set<Specimen> getSpecimens() {
        return specimens;
    }

    /**
     * @param specimens the specimens to set
     */
    public void setSpecimens(Set<Specimen> specimens) {
        this.specimens = specimens;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public Collection<Institution> getRelatedInstitutions() {
        return Collections.singleton(getExternalIdAssigner());
    }
}
