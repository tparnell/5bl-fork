/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.config.search;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;


/**
 * Search field configuration object corresponding to a
 * list of singly-selectable values.
 * @author gvaughn
 *
 */
@Entity
@Table(name = "select_config")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class SelectConfig extends SimpleSearchFieldConfig {

    private static final long serialVersionUID = 428947874154583613L;
    private static final Logger LOG = Logger.getLogger(SelectConfig.class);
    private static final int MIN_WIDTH_FIELD_LENGTH = 10;

    private String listClass;
    private DataType listType;
    private boolean multiSelect;
    private boolean multiSelectCollection;
    private MultiSelectValueType multiSelectValueType;
    private boolean showEmptyOption = true;
    private String minWidth;

    /**
     * @return Whether multiple values can be selected.
     */
    @NotNull
    @Column(name = "multi_select")
    public boolean isMultiSelect() {
        return multiSelect;
    }

    /**
     * @param multiSelect Whether multiple values can be selected.
     */
    public void setMultiSelect(boolean multiSelect) {
        this.multiSelect = multiSelect;
    }

    /**
     * @return True if this config is multi-select and corresponds to
     * a collection in teh searchable object, false otherwise.
     */
    @NotNull
    @Column(name = "multi_select_collection")
    public boolean isMultiSelectCollection() {
        return multiSelectCollection;
    }

    /**
     * Whether this config is multi-select and corresponds to
     * a collection in teh searchable object.
     * @param multiSelectCollection Whether this config is multi-select and corresponds to
     * a collection in teh searchable object.
     */
    public void setMultiSelectCollection(boolean multiSelectCollection) {
        this.multiSelectCollection = multiSelectCollection;
    }

    /**
     * Class name of the values corresponding to this configuration.
     * @return Class name of the values corresponding to this configuration.
     */
    @NotNull
    @Length(max = MAX_LENGTH)
    @Column(name = "list_class")
    public String getListClass() {
        return listClass;
    }

    /**
     * Class name of the values corresponding to this configuration.
     * @param listClass Class name of the values corresponding to this configuration.
     */
    public void setListClass(String listClass) {
        this.listClass = listClass;
    }
    
    /**
     * @return the minWidth
     */
    @NotNull
    @Length(max = MIN_WIDTH_FIELD_LENGTH)
    @Column(name = "min_width")
    public String getMinWidth() {
        return minWidth;
    }

    /**
     * @param minWidth the minWidth to set
     */
    public void setMinWidth(String minWidth) {
        this.minWidth = minWidth;
    }

    /**
     * Broad category of the values corresponding to this configuration.
     * @return Broad category of the values corresponding to this configuration.
     */
    @NotNull
    @Column(name = "list_type")
    @Enumerated(EnumType.STRING)
    public DataType getListType() {
        return listType;
    }

    /**
     * Broad category of the values corresponding to this configuration.
     * @param listType Broad category of the values corresponding to this configuration.
     */
    public void setListType(DataType listType) {
        this.listType = listType;
    }

    /**
     * @return the expected data type for values of the corresponding search field.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "multi_select_value_type")
    public MultiSelectValueType getMultiSelectValueType() {
        return multiSelectValueType;
    }

    /**
     * @param multiSelectValueType the expected data type for values of the corresponding search field.
     */
    public void setMultiSelectValueType(MultiSelectValueType multiSelectValueType) {
        this.multiSelectValueType = multiSelectValueType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getValueString(Object object, String mapName) {
        Object value = getValue(object, getSearchFieldName(), mapName);
        if (value != null) {
            if (isMultiSelect()) {
                return getMultiSelectValueString(value);
            }
            return getListType().getDataRetriever().getValueString(value);
        }
        return StringUtils.EMPTY;
    }

    @SuppressWarnings("unchecked")
    private String getMultiSelectValueString(Object value) {
        Collection<String> values = new ArrayList<String>();
        try {
            values.addAll(getListType().getDataRetriever()
                .getValueStrings((Collection<Object>) value, Class.forName(getListClass())));
        } catch (ClassNotFoundException e) {
            LOG.error(e);
        }
        return StringUtils.join(values, DELIMITER);
    }

    /**
     * @return Whether an empty option should be displayed with the values.
     */
    @NotNull
    @Column(name = "show_empty_option")
    public boolean isShowEmptyOption() {
        return showEmptyOption;
    }

    /**
     * @param showEmptyOption Whether an empty option should be displayed with the values.
     */
    public void setShowEmptyOption(boolean showEmptyOption) {
        this.showEmptyOption = showEmptyOption;
    }
}
