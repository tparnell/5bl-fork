/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
    *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.mta;

import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Index;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;

/**
 * @author ddasgupta
 *
 */
@Entity
@Table(name = "material_transfer_agreement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MaterialTransferAgreement implements PersistentObject {

    private static final long serialVersionUID = 9188685408998752161L;

    private Long id;
    private TissueLocatorFile document;
    private Date version;
    private Date uploadTime;

    /**
     * {@inheritDoc}
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the document
     */
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "name", column = @Column(name = "document_name", nullable = false)),
        @AttributeOverride(name = "contentType", column = @Column(name = "document_content_type", nullable = false))
    })
    @AssociationOverrides({
        @AssociationOverride(name = "lob", joinColumns = @JoinColumn(name = "document_lob_id", nullable = false))
    })
    public TissueLocatorFile getDocument() {
        return document;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(TissueLocatorFile document) {
        this.document = document;
    }

    /**
     * The version/activation date for this mta.
     * @return the version
     */
    @NotNull
    @Temporal(TemporalType.DATE)
    @Index(name = "mta_version_idx")
    public Date getVersion() {
        return version;
    }

    /**
     * @param version The version/activation date for this mta.
     */
    public void setVersion(Date version) {
        this.version = version;
    }

    /**
     * @return the uploadTime
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "upload_time")
    @Index(name = "mta_upload_time_idx")
    public Date getUploadTime() {
        return uploadTime;
    }

    /**
     * @param uploadTime the uploadTime to set
     */
    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }
}
