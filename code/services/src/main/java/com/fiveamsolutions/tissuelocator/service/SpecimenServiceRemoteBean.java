/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.service.search.SearchCondition;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSortCriterion;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateSessionInterceptor;

/**
 * @author bpickeral
 *
 */
@Stateless
@Interceptors({ TissueLocatorHibernateSessionInterceptor.class })
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SpecimenServiceRemoteBean implements SpecimenServiceRemote {
    @EJB
    private SpecimenServiceLocal specimenService;

    /**
     * @param specimenService the specimenService to set
     */
    public void setSpecimenService(SpecimenServiceLocal specimenService) {
        this.specimenService = specimenService;
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Map<String, Specimen> getSpecimens(Long institutionId, Collection<String> extIds) {
        return specimenService.getSpecimens(institutionId, extIds);
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Specimen> getAvailableSpecimens(int page, int pageSize) {
        SpecimenSearchCriteria criteria =
            new SpecimenSearchCriteria(new Specimen(), null, null, null, new ArrayList<SearchCondition>());
        criteria.getCriteria().setStatus(SpecimenStatus.AVAILABLE);
        PageSortParams<Specimen> params = new PageSortParams<Specimen>(pageSize, page, SpecimenSortCriterion.ID, false);
        return hydrateLazyCollections(specimenService.search(criteria, params));
    }

    private List<Specimen> hydrateLazyCollections(List<Specimen> search) {
        for (Specimen s : search) {
            List<Race> races = s.getParticipant().getRaces();
            for (Race r : races) {
                // do nothing
                r.name();
            }
        }
        return search;
    }

    /**
     * {@inheritDoc}
     */
    public Specimen importSpecimen(Specimen specimen) {
        return specimenService.importSpecimen(specimen);
    }
}
