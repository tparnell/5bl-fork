/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.AssertTrue;
import org.hibernate.validator.Digits;
import org.hibernate.validator.Length;
import org.hibernate.validator.Min;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.dynamicextensions.ExtendableEntity;
import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.Searchable;
import com.fiveamsolutions.tissuelocator.data.annotation.OnSave;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.data.validation.ConsistentInstitutions;
import com.fiveamsolutions.tissuelocator.data.validation.NotFuture;
import com.fiveamsolutions.tissuelocator.data.validation.PastYear;
import com.fiveamsolutions.tissuelocator.data.validation.RequiredSpecimenPrice;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenConsentWithdrawn;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenPatientAge;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenPatientAgeValidator;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenPriceRelationship;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenStateChange;
import com.fiveamsolutions.tissuelocator.data.validation.UniqueConstraint;
import com.fiveamsolutions.tissuelocator.data.validation.ValidExtensions;
import com.fiveamsolutions.tissuelocator.util.AdditionalPathologicFindingUseCountHandler;
import com.fiveamsolutions.tissuelocator.util.CustomPropertiesMapAdapter;
import com.fiveamsolutions.tissuelocator.util.SpecimenStatusTransitionHistoryHandler;


/**
 * @author ddasgupta
 *
 */
@Entity(name = "specimen")
@ConsistentInstitutions
@RequiredSpecimenPrice
@SpecimenPatientAge
@SpecimenPriceRelationship
@SpecimenStateChange
@SpecimenConsentWithdrawn
@UniqueConstraint(propertyNames = { "externalId", "externalIdAssigner" }, message = "{validator.specimen.duplicate}")
//Note that the case insensitive unique constraint for this table in the database is specified in plain sql.
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Specimen", propOrder = { "id", "externalId", "availableQuantity", "availableQuantityUnits",
        "patientAgeAtCollection", "patientAgeAtCollectionUnits", "collectionYear", "protocol", "participant",
        "pathologicalCharacteristic", "specimenType", "minimumPrice", "maximumPrice", "priceNegotiable",
        "createdDate", "status", "previousStatus", "consentWithdrawn", "consentWithdrawnDate",
        "customProperties" })
@XmlRootElement(name = "specimen")
@SuppressWarnings({ "PMD.TooManyFields", "PMD.ExcessiveClassLength", "PMD.TooManyMethods" })
@ValidExtensions
public class Specimen implements PersistentObject, InstitutionRestricted, Auditable, ExtendableEntity,
    Timestampable, StatusTransitionable<SpecimenStatusTransition, SpecimenStatus> {

    private static final long serialVersionUID = 5398469781277768601L;
    private static final int MAX_LENGTH = 254;
    private static final int INT_DIGITS = 17;
    private static final int MIN_COLLECTION_YEAR = 1900;

    private Long id;
    private String externalId;
    private BigDecimal availableQuantity;
    private QuantityUnits availableQuantityUnits;
    private int patientAgeAtCollection;
    private TimeUnits patientAgeAtCollectionUnits;
    private Integer collectionYear;

    @XmlTransient
    private Institution externalIdAssigner;
    private CollectionProtocol protocol;
    private Participant participant;

    private AdditionalPathologicFinding pathologicalCharacteristic;
    @XmlTransient
    private AdditionalPathologicFinding previousPathologicalCharacteristic;
    private SpecimenType specimenType;

    private BigDecimal minimumPrice;
    private BigDecimal maximumPrice;
    private boolean priceNegotiable;

    private Date createdDate = new Date();
    @XmlTransient
    private Date lastUpdatedDate = createdDate;

    private SpecimenStatus status;
    private SpecimenStatus previousStatus;
    @XmlTransient
    private List<SpecimenStatusTransition> statusTransitionHistory
        = new ArrayList<SpecimenStatusTransition>();
    @XmlTransient
    private Date statusTransitionDate;

    private boolean consentWithdrawn;
    private Date consentWithdrawnDate;

    @XmlTransient
    private List<SpecimenRequestLineItem> lineItems;

    @XmlJavaTypeAdapter(CustomPropertiesMapAdapter.class)
    private Map<String, Object> customProperties = new HashMap<String, Object>();

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Searchable
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Note that the case insensitive index for this column in the database is specified in plain sql.
     * @return the externalId
     */
    @NotEmpty
    @Length(max = MAX_LENGTH)
    @Column(name = "external_id")
    @Searchable(caseSensitive = false, matchMode = Searchable.MATCH_MODE_CONTAINS)
    public String getExternalId() {
        return externalId;
    }

    /**
     * @param externalId the externalId to set
     */
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    /**
     * @return the availableQuantity
     */
    @Min(value = 0)
    @Column(name = "available_quantity")
    @Index(name = "specimen_available_quantity_idx")
    public BigDecimal getAvailableQuantity() {
        return availableQuantity;
    }

    /**
     * @param availableQuantity the availableQuantity to set
     */
    public void setAvailableQuantity(BigDecimal availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    /**
     * @return the availableQuantityUnits
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "available_quantity_units")
    @Index(name = "specimen_available_quantity_units_idx")
    public QuantityUnits getAvailableQuantityUnits() {
        return availableQuantityUnits;
    }

    /**
     * @param availableQuantityUnits the availableQuantityUnits to set
     */
    public void setAvailableQuantityUnits(QuantityUnits availableQuantityUnits) {
        this.availableQuantityUnits = availableQuantityUnits;
    }

    /**
     * @return true or false depending on quantity validity.
     */
    @AssertTrue(message = "{validator.specimen.quantity.units.required}")
    @Transient
    public boolean isValidQuantity() {
        return getAvailableQuantity() == null == (getAvailableQuantityUnits() == null);
    }

    /**
     * @return the patientAgeAtCollection
     */
    @NotNull
    @Min(value = 0)
    @Column(name = "patient_age_at_collection")
    @Index(name = "specimen_patient_age_idx")
    public int getPatientAgeAtCollection() {
        return patientAgeAtCollection;
    }

    /**
     * @param patientAgeAtCollection the patientAgeAtCollection to set
     */
    public void setPatientAgeAtCollection(int patientAgeAtCollection) {
        this.patientAgeAtCollection = patientAgeAtCollection;
    }

    /**
     * @return the patientAgeAtCollectionUnits
     */
    @NotNull
    @Enumerated(value = EnumType.STRING)
    @Column(name = "patient_age_at_collection_units")
    @Index(name = "specimen_patient_age_units_idx")
    public TimeUnits getPatientAgeAtCollectionUnits() {
        return patientAgeAtCollectionUnits;
    }

    /**
     * @param patientAgeAtCollectionUnits the patientAgeAtCollectionUnits to set
     */
    public void setPatientAgeAtCollectionUnits(TimeUnits patientAgeAtCollectionUnits) {
        this.patientAgeAtCollectionUnits = patientAgeAtCollectionUnits;
    }

    /**
     *  Returns a boolean flag denoting whether this specimen's age is at or exceeds the maximum
     *  allowed value by Hippa standards.
     *
     * @return boolean true if this specimen's age is at or above the Hippa max allowed of 90 years
     */
    @Transient
    public boolean isPatientAgeAtCollectionAtHippaMax() {
        return patientAgeAtCollection >= SpecimenPatientAgeValidator.getHippaMaxAge(patientAgeAtCollectionUnits);
    }

    /**
     * @return the externalIdAssigner
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "external_id_assigner_id")
    @ForeignKey(name = "specimen_external_id_assigner_fk")
    @Index(name = "specimen_external_id_assigner_idx")
    @Searchable(caseSensitive = false, matchMode = Searchable.MATCH_MODE_EXACT, fields = "name")
    public Institution getExternalIdAssigner() {
        return externalIdAssigner;
    }

    /**
     * @param externalIdAssigner the externalIdAssigner to set
     */
    public void setExternalIdAssigner(Institution externalIdAssigner) {
        this.externalIdAssigner = externalIdAssigner;
    }

    /**
     * @param lineItems the lineItems to set
     */
    public void setLineItems(List<SpecimenRequestLineItem> lineItems) {
        this.lineItems = lineItems;
    }

    /**
     * @return the lineItems
     */
    @OneToMany(mappedBy = "specimen")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    public List<SpecimenRequestLineItem> getLineItems() {
        return lineItems;
    }

    /**
     * @return the protocol
     */
    @NotNull
    @Valid
    @ManyToOne
    @JoinColumn(name = "protocol_id")
    @ForeignKey(name = "specimen_protocol_fk")
    @Index(name = "specimen_protocol_idx")
    public CollectionProtocol getProtocol() {
        return protocol;
    }

    /**
     * @param protocol the protocol to set
     */
    public void setProtocol(CollectionProtocol protocol) {
        this.protocol = protocol;
    }

    /**
     * @return the participant
     */
    @NotNull
    @Valid
    @ManyToOne
    @JoinColumn(name = "participant_id")
    @ForeignKey(name = "specimen_participant_fk")
    @Index(name = "specimen_participant_idx")
    @Searchable(fields = { "gender", "ethnicity", "id" })
    public Participant getParticipant() {
        return participant;
    }

    /**
     * @param participant the participant to set
     */
    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    /**
     * @return the collectionYear
     */
    @Column(name = "collection_year")
    @Index(name = "specimen_collection_year")
    @Min(value = MIN_COLLECTION_YEAR)
    @PastYear
    public Integer getCollectionYear() {
        return collectionYear;
    }

    /**
     * @param collectionYear the collectionYear to set
     */
    public void setCollectionYear(Integer collectionYear) {
        this.collectionYear = collectionYear;
    }

    /**
     * @return the pathologicalCharacteristic
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "pathological_characteristic_id")
    @ForeignKey(name = "specimen_pathological_characteristic_fk")
    @Index(name = "specimen_pathological_characteristic_idx")
    @Searchable(caseSensitive = false, matchMode = Searchable.MATCH_MODE_CONTAINS, fields = "name")
    public AdditionalPathologicFinding getPathologicalCharacteristic() {
        return pathologicalCharacteristic;
    }

    /**
     * @param pathologicalCharacteristic the pathologicalCharacteristic to set
     */
    public void setPathologicalCharacteristic(AdditionalPathologicFinding pathologicalCharacteristic) {
        this.pathologicalCharacteristic = pathologicalCharacteristic;
    }
    
    /**
     * @return the previousPathologicalCharacteristic
     */
    @ManyToOne
    @JoinColumn(name = "pathological_characteristic_id", updatable = false, insertable = false, nullable = false)
    @ForeignKey(name = "specimen_pathological_characteristic_fk")
    public AdditionalPathologicFinding getPreviousPathologicalCharacteristic() {
        return previousPathologicalCharacteristic;
    }

    /**
     * @param previousPathologicalCharacteristic the previousPathologicalCharacteristic to set
     */
    public void setPreviousPathologicalCharacteristic(
            AdditionalPathologicFinding previousPathologicalCharacteristic) {
        this.previousPathologicalCharacteristic = previousPathologicalCharacteristic;
    }

    /**
     * @return the specimenType
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "specimen_type_id")
    @ForeignKey(name = "specimen_specimen_type_fk")
    @Index(name = "specimen_specimen_type_idx")
    @Searchable(caseSensitive = true, fields = { "name", "specimenClass" })
    public SpecimenType getSpecimenType() {
        return specimenType;
    }

    /**
     * @param specimenType the specimenType to set
     */
    public void setSpecimenType(SpecimenType specimenType) {
        this.specimenType = specimenType;
    }

    /**
     * @return the minimumPrice
     */
    @Min(value = 0)
    @Digits(integerDigits = INT_DIGITS, fractionalDigits = 2)
    @Column(name = "minimum_price")
    public BigDecimal getMinimumPrice() {
        return minimumPrice;
    }

    /**
     * @param minimumPrice the minimumPrice to set
     */
    public void setMinimumPrice(BigDecimal minimumPrice) {
        this.minimumPrice = minimumPrice;
    }

    /**
     * @return the maximumPrice
     */
    @Min(value = 0)
    @Digits(integerDigits = INT_DIGITS, fractionalDigits = 2)
    @Column(name = "maximum_price")
    public BigDecimal getMaximumPrice() {
        return maximumPrice;
    }

    /**
     * @param maximumPrice the maximumPrice to set
     */
    public void setMaximumPrice(BigDecimal maximumPrice) {
        this.maximumPrice = maximumPrice;
    }

    /**
     * @return the priceNegotiable
     */
    @NotNull
    @Column(name = "price_negotiable")
    public boolean isPriceNegotiable() {
        return priceNegotiable;
    }

    /**
     * @param priceNegotiable the priceNegotiable to set
     */
    public void setPriceNegotiable(boolean priceNegotiable) {
        this.priceNegotiable = priceNegotiable;
    }

    /**
     * @return the createdDate
     */
    @NotNull
    @NotFuture
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @NotFuture
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated_date")
    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return the specimen's status
     */
    @NotNull
    @Enumerated(value = EnumType.STRING)
    @Column(name = "status")
    @Index(name = "specimen_status_idx")
    @Searchable
    public SpecimenStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(SpecimenStatus status) {
        this.status = status;
    }

    /**
     * @return the specimen's status
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "status", updatable = false, insertable = false, nullable = false)
    public SpecimenStatus getPreviousStatus() {
        return previousStatus;
    }

    /**
     * @param previousStatus the status to set
     */
    public void setPreviousStatus(SpecimenStatus previousStatus) {
        this.previousStatus = previousStatus;
    }

    /**
     * @return the status transitions.
     */
    @ManyToMany
    @Valid
    @JoinTable(name = "specimen_status_transition_history",
                joinColumns = @JoinColumn(name = "specimen_id"),
                inverseJoinColumns = @JoinColumn(name = "status_transition_id"))
    @ForeignKey(name = "SPECIMEN_TRANSITION_FK",
                inverseName = "TRANSITION_SPECIMEN_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = CascadeType.ALL)
    @OrderBy(value = "systemTransitionDate")
    public List<SpecimenStatusTransition> getStatusTransitionHistory() {
        return statusTransitionHistory;
    }

    /**
     * @param statusTransitionHistory the status transitions.
     */
    public void setStatusTransitionHistory(
            List<SpecimenStatusTransition> statusTransitionHistory) {
        this.statusTransitionHistory = statusTransitionHistory;
    }


    /**
     * @return the date of the transition to the current status, if not the current date.
     */
    @Transient
    public Date getStatusTransitionDate() {
        return statusTransitionDate;
    }

    /**
     * @param statusTransitionDate the date of the transition to the current status, if not the current date.
     */
    public void setStatusTransitionDate(Date statusTransitionDate) {
        this.statusTransitionDate = statusTransitionDate;
    }

    /**
     * Update the status transition history based on the current and previous status.
     */
    @OnSave(index = 1)
    public void updateStatusTransitionHistory() {
        new SpecimenStatusTransitionHistoryHandler().updateStatusTransitionHistory(this);
    }

    /**
     * Update the inUse flag of pathological characteristics associated
     * with this specimen.
     */
    @OnSave(index = 0)
    public void updatePathologicalCharacteristicUseFlag() {
        new AdditionalPathologicFindingUseCountHandler().updateAdditionalPathologicFindings(this);
    }
    
    /**
     * @return the consentWithdrawn
     */
    @NotNull
    @Column(name = "consent_withdrawn")
    public boolean isConsentWithdrawn() {
        return consentWithdrawn;
    }

    /**
     * @param consentWithdrawn the consentWithdrawn to set
     */
    public void setConsentWithdrawn(boolean consentWithdrawn) {
        this.consentWithdrawn = consentWithdrawn;
    }

    /**
     * @return the consentWithdrawnDate
     */
    @NotFuture
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "consent_withdrawn_date")
    public Date getConsentWithdrawnDate() {
        return consentWithdrawnDate;
    }

    /**
     * @param consentWithdrawnDate the consentWithdrawnDate to set
     */
    public void setConsentWithdrawnDate(Date consentWithdrawnDate) {
        this.consentWithdrawnDate = consentWithdrawnDate;
    }

    /**
     * @return whether the consent withdrawn date is consistent.
     */
    @Transient
    @AssertTrue(message = "Consent Withdrawn and Consent Withdrawn Date must both be set.")
    public boolean isConsentWithdrawnDateValid() {
        return isConsentWithdrawn() == (getConsentWithdrawnDate() != null);
    }

    /**
     * @return whether consent can be withdrawn for this specimen.
     */
    @Transient
    public boolean isConsentWithdrawable() {
        return !consentWithdrawn && !SpecimenStatus.DESTROYED.equals(getStatus());
    }

    /**
     * @return whether this specimen can be returned to the source institution.
     */
    @Transient
    public boolean isReturnableToSourceInstitution() {
        return getStatus().getAllowedTransitions()
            .contains(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
    }

    /**
     * @return the customProperties
     */
    @Transient
    public Map<String, Object> getCustomProperties() {
        return customProperties;
    }

    /**
     * @param customProperties the customProperties to set
     */
    public void setCustomProperties(Map<String, Object> customProperties) {
        this.customProperties = customProperties;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getCustomProperty(String name) {
        return getCustomProperties().get(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCustomProperty(String name, Object value) {
        getCustomProperties().put(name, value);
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public Collection<Institution> getRelatedInstitutions() {
        return Collections.singleton(getExternalIdAssigner());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Specimen)) {
            return false;
        }

        Specimen rhs = (Specimen) obj;
        return new EqualsBuilder().append(getExternalId(), rhs.getExternalId())
                .append(getExternalIdAssigner(), rhs.getExternalIdAssigner()).isEquals();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getExternalId()).append(getExternalIdAssigner()).toHashCode();
    }
}
