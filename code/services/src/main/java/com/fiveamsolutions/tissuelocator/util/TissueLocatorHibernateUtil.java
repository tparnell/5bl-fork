/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.util;

import org.hibernate.Session;

import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsConfigurator;
import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsHibernateHelper;
import com.fiveamsolutions.tissuelocator.hibernate.interceptor.TissueLocatorDeleteOrphanInterceptor;

/**
 * @author smiller
 */
public class TissueLocatorHibernateUtil {
    private static final TissueLocatorDeleteOrphanInterceptor TISSUE_LOCATOR_INTERCEPTOR = 
        new TissueLocatorDeleteOrphanInterceptor();
    private static final DynamicExtensionsHibernateHelper HIBERNATE_HELPER =
        new DynamicExtensionsHibernateHelper(null, TISSUE_LOCATOR_INTERCEPTOR);
    static {
        HIBERNATE_HELPER.initialize();
        TISSUE_LOCATOR_INTERCEPTOR.setHibernateHelper(HIBERNATE_HELPER);
    }

    /**
     * Get the hibernate helper.
     * @return the helper.
     */
    public static DynamicExtensionsHibernateHelper getHibernateHelper() {
        return HIBERNATE_HELPER;
    }

    /**
     * Get the current session.
     * @return the session.
     */
    public static Session getCurrentSession() {
        return getHibernateHelper().getCurrentSession();
    }

    /**
     * Get the dynamic extensions configurator.
     * @return the dynamic extensions configurator.
     */
    public static DynamicExtensionsConfigurator getDynamicExtensionConfigurator() {
        return getHibernateHelper().getDynamicExtensionsConfigurator();
    }
}
