/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.config.category;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * Search field user interface category service implementation.
 *
 * @author jstephens
 */
@Stateless
public class UiSearchFieldCategoryServiceBean extends GenericServiceBean<UiSearchFieldCategory>
    implements UiSearchFieldCategoryServiceLocal {

    private static final Map<String, Map<SearchSetType, List<UiSearchFieldCategory>>>
        SEARCH_SET_TYPE_MAP_BY_ENTITY_CLASS = new HashMap<String, Map<SearchSetType, List<UiSearchFieldCategory>>>();

    /**
     * Returns the search field user interface categories associated with an action.
     *
     * @param entityClass the entity to which the search field categories belong
     * @param searchSetType the search set type
     * @return list of search field user interface categories
     */
    @Override
    public synchronized List<UiSearchFieldCategory> getUiSearchFieldCategories(
            Class<? extends PersistentObject> entityClass, SearchSetType searchSetType) {
        Map<SearchSetType, List<UiSearchFieldCategory>>
            uiSearchFieldCategoriesBySearchSetType = SEARCH_SET_TYPE_MAP_BY_ENTITY_CLASS.get(entityClass.getName());

        if (uiSearchFieldCategoriesBySearchSetType == null) {
            uiSearchFieldCategoriesBySearchSetType =
                createUiSearchFieldCategoriesBySearchSetType(entityClass, searchSetType);
            SEARCH_SET_TYPE_MAP_BY_ENTITY_CLASS.put(entityClass.getName(), uiSearchFieldCategoriesBySearchSetType);
        }

        List<UiSearchFieldCategory> uiSearchFieldCategories = uiSearchFieldCategoriesBySearchSetType.get(searchSetType);
        if (uiSearchFieldCategories == null) {
            uiSearchFieldCategories = retrieveUiSearchFieldCategories(entityClass, searchSetType);
            SEARCH_SET_TYPE_MAP_BY_ENTITY_CLASS.get(entityClass.getName()).put(searchSetType, uiSearchFieldCategories);
        }

        return uiSearchFieldCategories;
    }

    private Map<SearchSetType, List<UiSearchFieldCategory>> createUiSearchFieldCategoriesBySearchSetType(
            Class<? extends PersistentObject> entityClass, SearchSetType searchSetType) {
        final Map<SearchSetType, List<UiSearchFieldCategory>> uiSearchFieldCategoriesBySearchSetType =
                new EnumMap<SearchSetType, List<UiSearchFieldCategory>>(SearchSetType.class);

        final List<UiSearchFieldCategory> retrievedUiSearchFieldCategories =
            retrieveUiSearchFieldCategories(entityClass, searchSetType);

        uiSearchFieldCategoriesBySearchSetType.put(searchSetType, retrievedUiSearchFieldCategories);

        return uiSearchFieldCategoriesBySearchSetType;
    }

    @SuppressWarnings("unchecked")
    private List<UiSearchFieldCategory> retrieveUiSearchFieldCategories(
            Class<? extends PersistentObject> entityClass, SearchSetType searchSetType) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("FROM ").append(UiSearchFieldCategory.class.getName()).append(" ");
        queryString.append("WHERE entityClassName = :entityClassName ");
        queryString.append("AND searchSetType = :searchSetType ");
        queryString.append("ORDER BY id ASC");

        Query query = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString.toString());
        query.setParameter("entityClassName", entityClass.getName());
        query.setParameter("searchSetType", searchSetType.name());

        return query.list();
    }

}
