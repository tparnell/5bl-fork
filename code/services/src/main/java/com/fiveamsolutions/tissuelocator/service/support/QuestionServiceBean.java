/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.support;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Institution.InstitutionComparator;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.support.Question;
import com.fiveamsolutions.tissuelocator.data.support.QuestionResponse;
import com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;

/**
 * @author ddasgupta
 *
 */
@Stateless
public class QuestionServiceBean extends GenericServiceBean<Question> implements QuestionServiceLocal {

    private final EmailHelper emailHelper = new EmailHelper();

    /**
     * {@inheritDoc}
     */
    @Override
    public Long saveQuestion(Question question, List<Institution> responders) {
        Collections.sort(responders, new InstitutionComparator());
        for (Institution responder : responders) {
            QuestionResponse qr = new QuestionResponse();
            qr.setInstitution(responder);
            qr.setStatus(SupportRequestStatus.PENDING);
            qr.setQuestion(question);
            question.getResponses().add(qr);
        }
        Long id = savePersistentObject(question);
        TissueLocatorUser requestor = question.getRequestor();
        String supportEmail = emailHelper.getResourceBundle().getString("footer.support.email");
        emailHelper.sendEmail("question.submitted.requestor.email",
                new String[] {requestor.getEmail()},
                new String[] {id.toString(), requestor.getFirstName(), requestor.getLastName(), supportEmail});
        notifyResponders(question, responders);
        notifyAdmins(question);
        return id;
    }

    private void notifyResponders(Question question, List<Institution> responders) {
        Map<Long, List<String>> reviewers = getResponderMap(responders);
        for (QuestionResponse response : question.getResponses()) {
            List<String> emails = reviewers.get(response.getInstitution().getId());
            emailHelper.sendEmail("question.submitted.responder.email",
                    emails.toArray(new String[emails.size()]),
                    new String[] {question.getId().toString(), question.getRequestor().getFirstName(),
                            question.getRequestor().getLastName(), response.getId().toString()});
        }
    }

    private Map<Long, List<String>> getResponderMap(List<Institution> responders) {
        Map<Long, List<String>> responderMap = new HashMap<Long, List<String>>();
        Set<TissueLocatorUser> reviewers = getUserService().getByRoleAndInstitutions(Role.QUESTION_RESPONDER.getName(),
                new HashSet<Institution>(responders));
        for (TissueLocatorUser reviewer : reviewers) {
            Long instId = reviewer.getInstitution().getId();
            List<String> instResponders = responderMap.get(instId);
            if (instResponders == null) {
                instResponders = new ArrayList<String>();
                responderMap.put(instId, instResponders);
            }
            instResponders.add(reviewer.getEmail());
        }
        return responderMap;
    }

    private void notifyAdmins(Question question) {
        List<String> emails = new ArrayList<String>();
        Set<AbstractUser> reviewers = getUserService().getUsersInRole(Role.QUESTION_ADMINISTRATOR.getName());
        for (AbstractUser reviewer : reviewers) {
            emails.add(reviewer.getEmail());
        }
        emailHelper.sendEmail("question.submitted.administrator.email", emails.toArray(new String[emails.size()]),
                new String[] {question.getId().toString(), question.getRequestor().getFirstName(),
                        question.getRequestor().getLastName()});
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long addAdminResponse(Question question, QuestionResponse response) {
        response.setResponder(getUserService().getByUsername(UsernameHolder.getUser()));
        response.setInstitution(response.getResponder().getInstitution());
        response.setLastUpdatedDate(new Date());
        question.setLastUpdatedDate(response.getLastUpdatedDate());
        question.getResponses().add(response);
        response.setQuestion(question);
        Long id = savePersistentObject(question);

        String[] args = new String[] {id.toString(), StringUtils.defaultString(response.getResponse())};
        emailHelper.sendEmail("question.adminResponse.email", new String[] {question.getRequestor().getEmail()}, args);

        return id;
    }
}
