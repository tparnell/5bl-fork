/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.extension;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.struts2.converter.DynamicExtensionsTypeConversionException;
import com.fiveamsolutions.dynamicextensions.struts2.converter.DynamicExtensionsTypeConverter;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateSessionInterceptor;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * Implementation of the DynamicExtensionsServiceRemote interface.
 *
 * @author jstephens
 */
@Stateless
@Interceptors({ TissueLocatorHibernateSessionInterceptor.class })
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class DynamicExtensionsServiceRemoteBean implements DynamicExtensionsServiceRemote {

    private final DynamicExtensionsTypeConverter typeConverter;
    private final Map<String, Object> conversionErrors;

    /**
     * Constructs a new DynamicExtensionsServiceRemoteBean.
     */
    public DynamicExtensionsServiceRemoteBean() {
        conversionErrors = new HashMap<String, Object>();
        typeConverter = new DynamicExtensionsTypeConverter(TissueLocatorHibernateUtil.getHibernateHelper()
            .getDynamicExtensionsConfigurator(), "com.fiveamsolutions.tissuelocator.data", conversionErrors,
            TissueLocatorRegistry.getServiceLocator().getGenericService());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Object convertObject(Object object) throws DynamicExtensionsTypeConversionException {
        try {
            typeConverter.convertObject(object);
        } catch (Exception e) {
            throw new DynamicExtensionsTypeConversionException(e);
        }

        if (!conversionErrors.isEmpty()) {
            StringBuilder message = new StringBuilder();
            message.append("An error has occurred while converting the following properties: ");
            message.append(conversionErrors.toString());
            throw new DynamicExtensionsTypeConversionException(message.toString(), conversionErrors);
        }

        return object;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions(String clazz) {
        return TissueLocatorHibernateUtil.getDynamicExtensionConfigurator().getDynamicFieldDefinitionsForClass(clazz);
    }
}
