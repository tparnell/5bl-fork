/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;

/**
 * Bean for storing configuration data common to multiple field-level
 * configuration objects such as categorized edit and display fields,
 * search fields, and search result fields.
 * @author gvaughn
 *
 */
@Entity
@org.hibernate.annotations.Entity(mutable = false)
@Table(name = "generic_field_config")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class GenericFieldConfig implements PersistentObject {

    private static final long serialVersionUID = 8882759834945793088L;
    
    private static final int MAX_LENGTH = 255;
    
    private Long id;
    private ApplicationRole requiredRole;
    private String fieldName;
    private String searchFieldName;
    private String fieldDisplayName;
    private String searchFieldDisplayName;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the ApplicationRole required in order to view the field.
     * @return The ApplicationRole required in order to view the field.
     */
    @ManyToOne
    @JoinColumn(name = "required_role_id")
    @ForeignKey(name = "generic_field_config_role_fk")
    public ApplicationRole getRequiredRole() {
        return requiredRole;
    }

    /**
     * @param requiredRole the ApplicationRole required in order to view the field.
     */
    public void setRequiredRole(ApplicationRole requiredRole) {
        this.requiredRole = requiredRole;
    }

    /**
     * @return The name of the corresponding field relative to the parent object.
     */
    @NotEmpty
    @Column(name = "field_name")
    @Length(max = MAX_LENGTH)
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName The name of the corresponding field relative to the parent object.
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * Returns the field name used for searching on this field.
     * This field name does not necessarily match exactly a field
     * of the parent object (for example, it may be a min or max), but
     * does correspond directly to the field indicated by {@link #getFieldName()}.
     * @return The field name used for searching on this field.
     */
    @Column(name = "search_field_name")
    @Length(max = MAX_LENGTH)
    public String getSearchFieldName() {
        return searchFieldName;
    }

    /**
     * Set the field name used for searching on this field.
     * This field name does not necessarily match exactly a field
     * of the parent object (for example, it may be a min or max), but
     * does correspond directly to the field indicated by {@link #getFieldName()}.
     * @param searchFieldName The field name used for searching on this field.
     */
    public void setSearchFieldName(String searchFieldName) {
        this.searchFieldName = searchFieldName;
    }

    /**
     * Returns the display name for this field.
     * In general this is the display name that should be used
     * when referencing {@link #getFieldName()}.
     * @return The display name for this field.
     */
    @NotEmpty
    @Column(name = "field_display_name")
    @Length(max = MAX_LENGTH)
    public String getFieldDisplayName() {
        return fieldDisplayName;
    }

    /**
     * Set the display name for this field.
     * In general this is the display name that should be used
     * when referencing {@link #getFieldName()}.
     * @param fieldDisplayName The display name for this field.
     */
    public void setFieldDisplayName(String fieldDisplayName) {
        this.fieldDisplayName = fieldDisplayName;
    }

    /**
     * Returns the display name for the search field.
     * In general this is the display name that should be used
     * when referencing {@link #getSearchFieldName()}.
     * @return The display name for the search field.
     */
    @Column(name = "search_field_display_name")
    @Length(max = MAX_LENGTH)
    public String getSearchFieldDisplayName() {
        return searchFieldDisplayName;
    }

    /**
     * Set the display name for the search field.
     * In general this is the display name that should be used
     * when referencing {@link #getSearchFieldName()}.
     * @param searchFieldDisplayName The display name for the search field.
     */
    public void setSearchFieldDisplayName(String searchFieldDisplayName) {
        this.searchFieldDisplayName = searchFieldDisplayName;
    }
    
}
