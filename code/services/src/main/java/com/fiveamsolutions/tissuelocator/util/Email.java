/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.util;

import javax.mail.MessagingException;

import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;

/**
 * @author ddasgupta
 *
 */
public class Email {

    private String recipient;
    private String subject;
    private String html;
    private String text;

    private SpecimenRequest request;
    private String textReplacement;
    private String htmlReplacement;
    private String assignedUser;
    private String leadReviewer;

    /**
     * send this email.
     * @throws MessagingException on error
     */
    public void send() throws MessagingException {
        TissueLocatorUser u = new TissueLocatorUser();
        u.setEmail(getRecipient());
        MailUtils.sendEmail(u, getSubject(), getHtml(), getText());
    }

    /**
     * @return the recipient
     */
    public String getRecipient() {
        return this.recipient;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return this.subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the html
     */
    public String getHtml() {
        return this.html;
    }

    /**
     * @param html the html to set
     */
    public void setHtml(String html) {
        this.html = html;
    }

    /**
     * @return the text
     */
    public String getText() {
        return this.text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the request
     */
    public SpecimenRequest getRequest() {
        return this.request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(SpecimenRequest request) {
        this.request = request;
    }

    /**
     * @return the textReplacement
     */
    public String getTextReplacement() {
        return this.textReplacement;
    }

    /**
     * @param textReplacement the textReplacement to set
     */
    public void setTextReplacement(String textReplacement) {
        this.textReplacement = textReplacement;
    }

    /**
     * @return the htmlReplacement
     */
    public String getHtmlReplacement() {
        return this.htmlReplacement;
    }

    /**
     * @param htmlReplacement the htmlReplacement to set
     */
    public void setHtmlReplacement(String htmlReplacement) {
        this.htmlReplacement = htmlReplacement;
    }

    /**
     * @return the assignedUser
     */
    public String getAssignedUser() {
        return this.assignedUser;
    }

    /**
     * @param assignedUser the assignedUser to set
     */
    public void setAssignedUser(String assignedUser) {
        this.assignedUser = assignedUser;
    }

    /**
     * @return the leadReviewer
     */
    public String getLeadReviewer() {
        return this.leadReviewer;
    }

    /**
     * @param leadReviewer the leadReviewer to set
     */
    public void setLeadReviewer(String leadReviewer) {
        this.leadReviewer = leadReviewer;
    }
}
