/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.Length;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.tissuelocator.data.validation.DisableableNotEmpty;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableNotNull;

/**
 * @author smiller
 *
 */
@Embeddable
public class TissueLocatorFile {

    private static final int LENGTH = 254;
    private LobHolder lob;
    private String name;
    private String contentType;

    /**
     * Default constructor.
     */
    public TissueLocatorFile() {
        // do nothing
    }

    /**
     * Copy constructor.
     * @param file the file to copy.
     */
    public TissueLocatorFile(TissueLocatorFile file) {
        //copy everything but the actual lob data, which requires a transaction to load
        lob = new LobHolder();
        lob.setId(file.getLob().getId());
        name = file.getName();
        contentType = file.getContentType();
    }

    /**
     * Constructor.
     * @param data the data
     * @param fileName the file name
     * @param contentType the content type
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public TissueLocatorFile(byte[] data, String fileName, String contentType) {
        lob = new LobHolder(data);
        name = fileName;
        this.contentType = contentType;
    }

    /**
     * @return the lob
     */
    @Valid
    @ManyToOne(fetch = FetchType.LAZY)
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @DisableableNotNull
    public LobHolder getLob() {
        return lob;
    }

    /**
     * @param lob the lob to set
     */
    public void setLob(LobHolder lob) {
        this.lob = lob;
    }

    /**
     * @return the name
     */
    @DisableableNotEmpty
    @Length(max = LENGTH)
    @Column(name = "file_name")
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the contentType
     */
    @DisableableNotEmpty
    @Length(max = LENGTH)
    @Column(name = "content_type")
    public String getContentType() {
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}