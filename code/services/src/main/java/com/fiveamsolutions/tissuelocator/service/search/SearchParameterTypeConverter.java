/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.ExtendableEntity;
import com.opensymphony.xwork2.conversion.impl.XWorkBasicConverter;

/**
 * Handles the most common type conversions for search parameters. This includes support
 * for each of the following: strings, booleans, characters, dates (short and RFC 3339
 * formats), integers, longs, big decimals, floats and doubles. All objects that are to
 * be converted are assumed to be strings.
 *
 * @author jstephens
 */
public class SearchParameterTypeConverter {

    private final Class<?> targetClass;
    private final List<AbstractDynamicFieldDefinition> definitions;

    /**
     * Creates a new instance of the search parameter type converter.
     *
     * @param targetClass the target class
     */
    public SearchParameterTypeConverter(Class<?> targetClass) {
        this(targetClass, new ArrayList<AbstractDynamicFieldDefinition>());
    }

    /**
     * Creates a new instance of the search parameter type converter.
     *
     * @param targetClass the target class
     * @param definitions dynamic field definitions
     */
    public SearchParameterTypeConverter(Class<?> targetClass, List<AbstractDynamicFieldDefinition> definitions) {
        this.targetClass = targetClass;
        this.definitions = definitions;
    }

    /**
     * Converts the given value to the type of the specified parameter.
     *
     * @param parameterName name of the parameter
     * @param parameterValue value of the parameter
     * @return the converted value to the type given
     * @throws NoSuchFieldException if a field with the specified name is not found
     * @throws ClassNotFoundException if a class of the specified name cannot be located
     */
    public Object convertValue(String parameterName, Object parameterValue)
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = parameterValue;

        if (StringUtils.isNotBlank(parameterValue.toString())) {
            convertedValue = convertValue(targetClass, parameterName, parameterValue);
        }

        return convertedValue;
    }

    private Object convertValue(Class<?> target, String parameterName, Object parameterValue)
            throws NoSuchFieldException, ClassNotFoundException {
        Object convertedValue = null;
        Class<?> fieldType = null;

        if (parameterName.contains(".")) {
            String[] nestedProperty = parameterName.split("\\.");
            Field field = target.getDeclaredField(nestedProperty[0]);
            fieldType = field.getType();
            convertedValue = convertValue(fieldType, nestedProperty[1], parameterValue);
        } else {
            if (ExtendableEntity.class.isAssignableFrom(target)) {
                fieldType = getDynamicExtensionFieldType(parameterName);
            }

            if (fieldType == null) {
                Field field = target.getDeclaredField(parameterName);
                fieldType = field.getType();
            }

            XWorkBasicConverter converter = new XWorkBasicConverter();
            convertedValue = converter.convertValue(null, null, null, null, parameterValue, fieldType);
        }

        return convertedValue;
    }

    private Class<?> getDynamicExtensionFieldType(String parameterName) throws ClassNotFoundException {
        Class<?> fieldType = null;
        
        for (AbstractDynamicFieldDefinition definition : definitions) {
            if (StringUtils.equals(parameterName, definition.getFieldName())) {
                fieldType = Class.forName(definition.getTypeName());
            }
        }

        return fieldType;
    }

}