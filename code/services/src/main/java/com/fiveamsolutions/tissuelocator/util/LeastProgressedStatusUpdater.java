/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.ShipmentStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.Vote;

/**
 * Updates the status of a request based on the least-progressed
 * portion of the line item review process.
 * @author gvaughn
 *
 */
public class LeastProgressedStatusUpdater implements RequestStatusUpdater {

    private static final Map<RequestStatus, Map<Vote, RequestStatus>> PENDING_STATUS_TRANSITION_MAP =
        new HashMap<RequestStatus, Map<Vote, RequestStatus>>();

    static {
        Map<Vote, RequestStatus> pendingMap = new HashMap<Vote, RequestStatus>();
        pendingMap.put(Vote.DENY, RequestStatus.DENIED);
        pendingMap.put(Vote.APPROVE, RequestStatus.APPROVED);
        PENDING_STATUS_TRANSITION_MAP.put(RequestStatus.PENDING, pendingMap);
        Map<Vote, RequestStatus> approvedMap = new HashMap<Vote, RequestStatus>();
        approvedMap.put(Vote.DENY, RequestStatus.PARTIALLY_APPROVED);
        approvedMap.put(Vote.APPROVE, RequestStatus.APPROVED);
        PENDING_STATUS_TRANSITION_MAP.put(RequestStatus.APPROVED, approvedMap);
        Map<Vote, RequestStatus> deniedMap = new HashMap<Vote, RequestStatus>();
        deniedMap.put(Vote.DENY, RequestStatus.DENIED);
        deniedMap.put(Vote.APPROVE, RequestStatus.PARTIALLY_APPROVED);
        PENDING_STATUS_TRANSITION_MAP.put(RequestStatus.DENIED, deniedMap);
        Map<Vote, RequestStatus> paMap = new HashMap<Vote, RequestStatus>();
        paMap.put(Vote.DENY, RequestStatus.PARTIALLY_APPROVED);
        paMap.put(Vote.APPROVE, RequestStatus.PARTIALLY_APPROVED);
        PENDING_STATUS_TRANSITION_MAP.put(RequestStatus.PARTIALLY_APPROVED, paMap);
    }

    /**
     * {@inheritDoc}
     */
    public void updateStatus(SpecimenRequest request) {
        if (RequestStatus.PENDING.equals(request.getStatus())) {
            updatePendingStatus(request);
        }

        if (!RequestStatus.PENDING.equals(request.getStatus())
                && !RequestStatus.DENIED.equals(request.getStatus())) {
            updateShippedStatus(request);
        }
    }

    private void updatePendingStatus(SpecimenRequest request) {
        List<AggregateSpecimenRequestLineItem> allLineItems = new ArrayList<AggregateSpecimenRequestLineItem>();
        allLineItems.addAll(request.getOrderAggregateLineItems());
        allLineItems.addAll(request.getAggregateLineItems());
        for (AggregateSpecimenRequestLineItem lineItem : allLineItems) {
            SpecimenRequestReviewVote vote = lineItem.getVote();
            if (vote.getVote() == null) {
                request.setStatus(RequestStatus.PENDING);
                break;
            } else {
                request.setStatus(PENDING_STATUS_TRANSITION_MAP.get(request.getStatus()).get(vote.getVote()));
            }
        }
    }

    private int getNonPendingOrderCount(SpecimenRequest request) {
        int count = 0;
        for (Shipment shipment : request.getOrders()) {
            if (!ShipmentStatus.PENDING.equals(shipment.getStatus())) {
                count++;
            }
        }
        return count;
    }

    private void updateShippedStatus(SpecimenRequest request) {
        int shippedCount = getNonPendingOrderCount(request);
        if (shippedCount > 0 && shippedCount < request.getOrders().size()) {
            request.setStatus(RequestStatus.PARTIALLY_PROCESSED);
        } else if (shippedCount == request.getOrders().size()) {
            request.setStatus(RequestStatus.FULLY_PROCESSED);
        }
    }

}
