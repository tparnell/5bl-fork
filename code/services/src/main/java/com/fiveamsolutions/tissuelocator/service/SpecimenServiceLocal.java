/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.util.NameCountPair;
import com.fiveamsolutions.tissuelocator.util.ParticipantReturnRequestData;

/**
 * @author ddasgupta
 */
@Local
public interface SpecimenServiceLocal extends GenericServiceLocal<Specimen> {

    /**
     * Get a count of specimens in the database for each specimen class.
     * @return a map indicating how many specimens of each specimen class exist in the database.
     * The map keys are the SpecimenClass enum values, and the values are the corresponding counts.
     */
    Map<SpecimenClass, Long> getCountsByClass();

    /**
     * Get a count of specimens in the database for each pathological characteristic.
     * @return a map indicating how many specimens exist in the database for each pathological characteristic.
     * The map keys are the names of the AdditionalPathologicFinding objects, and the values are are a
     * Long array with code id [0] and count [1].
     */
    Map<String, Long[]> getCountsByPathologicalCharacteristic();
    
    /**
     * Get the name and id of each distinct pathological characteristic for which there are associated
     * specimens in the database.
     * @return A mapping of name to id for each distinct pathological characteristic for which there are 
     * associated specimens in the database.
     */
    Map<String, Long> getPathologicalCharacteristics();

    /**
     * Get the name and id of each institution for which there are associated specimens in the database.
     * @return A mapping of name to id for each institution for which there are associated specimens in the database.
     */
    Map<String, Long> getInstitutions();

    /**
     * Remote API to get specimen for an institution by external identifier.
     * @param institutionId id of the Institution
     * @param extIds external identifiers of the specimens
     * @return a map of specimen external identifier to specimen
     */
    Map<String, Specimen> getSpecimens(Long institutionId, Collection<String> extIds);

    /**
     * Withdraw consent for all applicable specimen associated with the participant.
     * @param participant Participant for which consent is being withdrawn.
     */
    void withdrawConsentForParticipant(Participant participant);

    /**
     * Withdraw consent form the given specimen, if applicable.
     * @param specimen Specimen for which consent is being withdrawn.
     */
    void withdrawConsent(Specimen specimen);

    /**
     * Mark a specimen as having been returned to the source institution.
     * @param specimen Specimen returned to the source institution.
     */
    void returnToSourceInstitution(Specimen specimen);

    /**
     * Remote API for creating/saving a Specimen without enforcing institution restrictions
     *  (used in flat-file import).
     * @param specimen Specimen object to create/save
     * @return Specimen object saved
     */
    Specimen importSpecimen(Specimen specimen);

    /**
     * Get counts of the most frequently requested specimen types within a date range.
     * @param startDate the start date of the date range
     * @param endDate the end date of the date range
     * @return a map identifying the most frequently requested specimen types within the date range.
     * The map keys are the names of the SpecimenType objects, and the values are the
     * corresponding counts.
     */
    List<NameCountPair> getTopRequestedSpecimenTypes(Date startDate, Date endDate);
    
    /**
     * Get data on specimen return requests within a date range, grouped by participant. 
     * @param startDate the start date of the date range
     * @param endDate the end date of the date range
     * @return A mapping of participant id to specimen return request data for that participant.
     */
    Map<Long, ParticipantReturnRequestData> getReturnRequestDataByParticipant(Date startDate, Date endDate);
}
