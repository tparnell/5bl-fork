/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.Length;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableNotEmpty;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableNotNull;

/**
 * @author ddasgupta
 */
@Entity
@Table(name = "address")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Address implements Auditable {

    private static final long serialVersionUID = 1L;

    private static final int LENGTH = 254;
    private static final int ZIP_LENGTH = 20;
    private static final int PHONE_LENGTH = 20;
    private static final int STATE_LENGTH = 30;
    private static final int COUNTRY_LENGTH = 50;

    private Long id;
    private String line1;
    private String line2;
    private String city;
    private State state;
    private String zip;
    private Country country;
    private String phone;
    private String phoneExtension;
    private String fax;

    /**
     * default no-arg constructor.
     */
    public Address() {
        //do nothing
    }

    /**
     * copy constructor.
     * @param address the address to copy
     */
    public Address(Address address) {
        line1 = address.getLine1();
        line2 = address.getLine2();
        city = address.getCity();
        state = address.getState();
        zip = address.getZip();
        country = address.getCountry();
        phone = address.getPhone();
        phoneExtension = address.getPhoneExtension();
        fax = address.getFax();
    }

    /**
     * {@inheritDoc}
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id db id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the line1
     */
    @DisableableNotEmpty
    @Length(max = LENGTH)
    @Column(length = LENGTH)
    public String getLine1() {
        return line1;
    }

    /**
     * @param line1 the line1 to set
     */
    public void setLine1(String line1) {
        this.line1 = line1;
    }

    /**
     * @return the line2
     */
    @Length(max = LENGTH)
    @Column(length = LENGTH)
    public String getLine2() {
        return line2;
    }

    /**
     * @param line2 the line2 to set
     */
    public void setLine2(String line2) {
        this.line2 = line2;
    }

    /**
     * @return the city
     */
    @DisableableNotEmpty
    @Length(max = LENGTH)
    @Column(length = LENGTH)
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    @Enumerated(EnumType.STRING)
    @DisableableNotNull
    @Column(length = STATE_LENGTH)
    public State getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * @return the zip
     */
    @DisableableNotEmpty
    @Length(max = ZIP_LENGTH)
    @Column(length = ZIP_LENGTH)
    public String getZip() {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return the country
     */
    @Enumerated(EnumType.STRING)
    @DisableableNotNull
    @Column(length = COUNTRY_LENGTH)
    public Country getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(Country country) {
        this.country = country;
    }

    /**
     * @return the phone
     */
    @DisableableNotEmpty
    @Length(max = PHONE_LENGTH)
    @Column(length = PHONE_LENGTH)
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the phone extension
     */
    @Length(max = PHONE_LENGTH)
    @Column(name = "phone_extension")
    public String getPhoneExtension() {
        return phoneExtension;
    }

    /**
     * @param phoneExtension the phone extension to set
     */
    public void setPhoneExtension(String phoneExtension) {
        this.phoneExtension = phoneExtension;
    }

    /**
     * @return the fax
     */
    @Length(max = PHONE_LENGTH)
    @Column(length = PHONE_LENGTH)
    public String getFax() {
        return fax;
    }

    /**
     * @param fax the fax to set
     */
    public void setFax(String fax) {
        this.fax = fax;
    }
}
