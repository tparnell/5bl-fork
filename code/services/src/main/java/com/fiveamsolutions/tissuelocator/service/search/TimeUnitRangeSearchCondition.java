

/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.search.SearchableUtils;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.util.TimeConverter;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;

/**
 * Time unit range search condition.
 * 
 * @author jstephens
 */
public class TimeUnitRangeSearchCondition extends RangeSearchCondition {

    private static final long serialVersionUID = 1L;

    /**
     * Appends this search condition to a query.
     *
     * @param object the example object
     * @param whereClause the query where clause
     * @param bindParameters the query bind parameters
     */
    @Override
    public void appendCondition(Object object,
            StringBuffer whereClause,
            Map<String, Object> bindParameters) {
        /*
         * Objects with time unit properties should be refactored to implement a time
         * unit interface so that append condition object parameter can be cast to an
         * abstract type.
         */
        Specimen specimen = (Specimen) object;
        TimeUnits timeUnits = specimen.getPatientAgeAtCollectionUnits();
        addStatement(whereClause, ">=", getMinPropertyValue(), timeUnits, "min", bindParameters);
        addStatement(whereClause, "<=", getMaxPropertyValue(), timeUnits, "max", bindParameters);
    }

    @SuppressWarnings("PMD.ExcessiveParameterList")
    //CHECKSTYLE:OFF - too many parameters
    private void addStatement(StringBuffer whereClause,
            String operator,
            Object timeParameterValue,
            TimeUnits timeUnits,
            String parameterSuffix,
            Map<String, Object> bindParameters) {
    //CHECKSTYLE:ON
        if ((timeParameterValue != null
                && StringUtils.isNotBlank(timeParameterValue.toString())) && (timeUnits != null)) {
            String timeUnitsPropertyName = getPropertyName() + "Units";
            List<String> conditions = new ArrayList<String>();
            int index = 0;
            
            Map<TimeUnits, Double> conversions =
                TimeConverter.getAllValues(Integer.parseInt(timeParameterValue.toString()), timeUnits);
            
            for (Map.Entry<TimeUnits, Double> entry : conversions.entrySet()) {
                String timeUnitsParameterName =
                    timeUnitsPropertyName + parameterSuffix + Integer.toString(index++);
                
                String timeCondition =
                    String.format("%s.%s %s %s AND %s.%s = :%s",
                        SearchableUtils.ROOT_OBJ_ALIAS,
                        getPropertyName(),
                        operator,
                        entry.getValue(),
                        SearchableUtils.ROOT_OBJ_ALIAS,
                        timeUnitsPropertyName,
                        timeUnitsParameterName);
                
                conditions.add(timeCondition);
                
                bindParameters.put(timeUnitsParameterName, entry.getKey());
            }

            whereClause.append(whereOrAnd(whereClause));
            whereClause.append('(');
            whereClause.append(StringUtils.join(conditions, SearchableUtils.OR));
            whereClause.append(')');
        }
    }

}