/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import static com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus.APPROVED;
import static com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus.OUT_OF_DATE;
import static com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus.REJECTED;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;

/**
 * @author ddasgupta
 *
 */
@Stateless
public class SignedMaterialTransferAgreementServiceBean extends GenericServiceBean<SignedMaterialTransferAgreement>
        implements SignedMaterialTransferAgreementServiceLocal {

    @EJB
    private InstitutionServiceLocal institutionService;
    
    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Long saveSignedMaterialTransferAgreement(SignedMaterialTransferAgreement mta) {
        Long id = savePersistentObject(mta);

        Set<AbstractUser> mtaAdmins = getUserService().getUsersInRole(Role.MTA_ADMINISTRATOR.getName());
        Set<String> mtaAdminEmails = new HashSet<String>();
        for (AbstractUser mtaAdmin : mtaAdmins) {
            mtaAdminEmails.add(mtaAdmin.getEmail());
        }
        EmailHelper helper = new EmailHelper();
        helper.sendEmail("signedMta.email", mtaAdminEmails.toArray(new String[mtaAdminEmails.size()]), new String[0]);

        return id;
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void reviewSignedMta(SignedMaterialTransferAgreement signedMta, Institution institution, boolean valid,
            boolean replaceExisting) {
        SignedMaterialTransferAgreement old = institution.getCurrentSignedRecipientMta();

        signedMta.setStatus(valid ? APPROVED : REJECTED);
        if (isOldCurrent(signedMta, old)) {
            if (replaceExisting) {
                old.setStatus(OUT_OF_DATE);
                savePersistentObject(old);
            } else {
                signedMta.setStatus(REJECTED);
            }
        }

        if (!institution.getId().equals(signedMta.getReceivingInstitution().getId())) {
            signedMta.setReceivingInstitution(institution);
        }

        savePersistentObject(signedMta);
        getInstitutionService().savePersistentObject(institution);
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Long updateSignedMta(SignedMaterialTransferAgreement newMta, 
            SignedMaterialTransferAgreement currentMta) {             
        if (currentMta != null && isOldCurrent(newMta, currentMta)) {
            currentMta.setStatus(OUT_OF_DATE);
            savePersistentObject(currentMta);
        }
        
        return savePersistentObject(newMta);
    }
    
    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deprecateSignedMta(SignedMaterialTransferAgreement signedMta) {             
        if (signedMta != null && signedMta.getStatus() != OUT_OF_DATE) {
            signedMta.setStatus(OUT_OF_DATE);
            savePersistentObject(signedMta);
        }
    }
        
    private boolean isOldCurrent(SignedMaterialTransferAgreement signedMta, SignedMaterialTransferAgreement old) {
        return old != null && !old.getId().equals(signedMta.getId()) && !OUT_OF_DATE.equals(old.getStatus());
    }

    /**
     * @return the institutionService
     */
    public InstitutionServiceLocal getInstitutionService() {
        return institutionService;
    }

    /**
     * @param institutionService the institutionService to set
     */
    public void setInstitutionService(InstitutionServiceLocal institutionService) {
        this.institutionService = institutionService;
    }
}
