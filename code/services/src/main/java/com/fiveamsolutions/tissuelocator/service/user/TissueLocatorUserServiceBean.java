/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.user;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.mail.MessagingException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.data.security.AccountStatus;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.nci.commons.data.security.PasswordReset;
import com.fiveamsolutions.nci.commons.data.security.UserGroup;
import com.fiveamsolutions.nci.commons.util.SecurityUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionRestricted;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.ForgotPasswordStatus;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.google.inject.Inject;

/**
 * @author ddasgupta
 *
 */
@Stateless
@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public class TissueLocatorUserServiceBean extends GenericServiceBean<TissueLocatorUser>
    implements TissueLocatorUserServiceLocal {

    private static final Logger LOG = Logger.getLogger(TissueLocatorUserServiceBean.class);
    private static final int MAX_OUTSTANDING_REQUESTS = 10;

    private static final long USER_GROUP_ID = 1L;
    private static final long TISSUE_TECH_GROUP_ID = 2L;
    private static final long SCIENTIFIC_REVIEW_GROUP_ID = 3L;
    private static final long INST_ADMIN_GROUP_ID = 4L;
    private static final long CONSORTIUM_ADMIN_GROUP_ID = 5L;
    private static final long SUPER_ADMIN_GROUP_ID = 6L;

    private final EmailHelper emailHelper = new EmailHelper();

    private ApplicationSettingServiceLocal applicationSettingService;

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public TissueLocatorUser getByUsername(String username) {
        String queryString = "from " + TissueLocatorUser.class.getName() + " where lower(username) = lower(:username)";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setString("username", username);
        return (TissueLocatorUser) q.uniqueResult();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Set<TissueLocatorUser> getByRoleAndInstitution(String roleName, Institution i) {
        Set<TissueLocatorUser> users = new HashSet<TissueLocatorUser>();
        String queryString = "from " + ApplicationRole.class.getName() + " where name = :name";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setString("name", roleName);
        ApplicationRole ar = (ApplicationRole) q.uniqueResult();

        queryString = "select distinct u from " + TissueLocatorUser.class.getName() + " as u "
            + "left join u.groups as g where u.status = :status and "
            + "(:role in elements (g.roles) or :role in elements (u.roles)) and "
            + "u.institution = :institution ";
        q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setEntity("role", ar);
        q.setParameter("status", AccountStatus.ACTIVE);
        q.setEntity("institution", i);
        users.addAll(q.list());
        return users;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Set<TissueLocatorUser> getByRoleAndInstitutions(String roleName,
            Set<Institution> institutions) {

        Set<TissueLocatorUser> users = new HashSet<TissueLocatorUser>();
        String queryString = "from " + ApplicationRole.class.getName() + " where name = :name";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setString("name", roleName);
        ApplicationRole ar = (ApplicationRole) q.uniqueResult();

        queryString = "select distinct u from " + TissueLocatorUser.class.getName() + " as u "
            + "left join u.groups as g where u.status = :status and "
            + "(:role in elements (g.roles) or :role in elements (u.roles)) and "
            + "u.institution in (:institutions) ";
        q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setEntity("role", ar);
        q.setParameter("status", AccountStatus.ACTIVE);
        q.setParameterList("institutions", institutions);
        users.addAll(q.list());
        return users;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean performAlternateChecks(InstitutionRestricted ir, TissueLocatorUser u) {
        TissueLocatorUser object = (TissueLocatorUser) ir;
        return object.getId() == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long savePersistentObject(TissueLocatorUser user) {
        // update the updated date
        user.setUpdatedDate(new Date());

        TissueLocatorUser loggedInUser = getByUsername(UsernameHolder.getUser());
        if (loggedInUser != null) {
            enforceLoggedInUserGroupAccessConstraints(user, loggedInUser);
        }

        return super.savePersistentObject(user);
    }

    private void enforceLoggedInUserGroupAccessConstraints(TissueLocatorUser user, TissueLocatorUser loggedInUser) {
     // verify that the currently logged in user is only adding groups they have access to.
        List<UserGroup> administratableGroups = getAdministratableGroups(loggedInUser);
        for (UserGroup g : user.getGroups()) {
            if (!administratableGroups.contains(g)) {
                throw new IllegalStateException("The logged in user can not add " + g.getName()
                        + " to a user's list of groups.");
            }
        }

        // now if the user previously existed make sure any group they were in that the current user
        // does not have access to is not removed
        if (user.getId() != null) {
            Set<UserGroup> previousGroups = getPreviousGroups(user);
            for (UserGroup g : previousGroups) {
                if (!administratableGroups.contains(g)) {
                    // because the items in the previous group set were loaded in a different session, we reload them
                    user.getGroups().add((UserGroup) TissueLocatorHibernateUtil.
                            getCurrentSession().load(UserGroup.class, g.getId()));
                }
            }
        }
    }

    private Set<UserGroup> getPreviousGroups(TissueLocatorUser user) {
        Session s = null;
        try {
            s = TissueLocatorHibernateUtil.getHibernateHelper().getSessionFactory().openSession();
            TissueLocatorUser oldUser = (TissueLocatorUser) s.load(TissueLocatorUser.class, user.getId());
            Set<UserGroup> oldgroups = new HashSet<UserGroup>();
            oldgroups.addAll(oldUser.getGroups());
            return oldgroups;
        } finally {
            @SuppressWarnings("deprecation")
            Connection c = s.connection();
            s.close();
            try {
                c.close();
            } catch (SQLException e) {
                Logger.getLogger(this.getClass()).error(e);
            }
        }
    }

    /**
     * Deny a user registration request. An email is sent to the user with a
     * reason for the denial.
     *
     * @param user The denied user registration.
     * @throws MessagingException if an error occurs while sending email.
     */
    public void denyUser(TissueLocatorUser user) throws MessagingException {
        String denialReason = user.getStatusTransitionComment();
        savePersistentObject(user);
        emailHelper.sendEmail("user.denied.email",
                new String[] {user.getEmail()}, new String[] {denialReason});
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deactivateUser(TissueLocatorUser user)
            throws MessagingException {
        String reason = user.getStatusTransitionComment();
        savePersistentObject(user);
        emailHelper.sendEmail("user.deactivated.email",
                new String[] {user.getEmail()}, new String[] {reason});
    }

    /**
     * {@inheritDoc}
     */
    public Long saveUser(TissueLocatorUser u, boolean isAdmin, String password) throws MessagingException {
        setUserStatusAndGroups(u, isAdmin);
        String[] approverEmails = getApproverEmails();
        String[] args = new String[0];
        boolean isNew = u.getId() == null;
        boolean isApproved = AccountStatus.PENDING.equals(u.getPreviousStatus())
            && AccountStatus.ACTIVE.equals(u.getStatus());
        Long id = savePersistentObject(u);

        if (isNew) {
            String keyBase = "user.register.email";
            if (isAdmin) {
                PasswordReset pr = PasswordReset.newInstance(u);
                TissueLocatorHibernateUtil.getCurrentSession().save(pr);
                keyBase = "user.create.email";
                args = new String[] {u.getUsername(), pr.getNonce()};
            } else if (getApplicationSettingService().isAccountApprovalActive()) {
                keyBase = "user.pendingApproval.email";
                emailHelper.sendEmail("user.review.email", approverEmails, new String[] {u.getId().toString()});
            }
            emailHelper.sendEmail(keyBase, new String[] {u.getEmail()}, args);
        } else if (getApplicationSettingService().isAccountApprovalActive() && isApproved) {
            sendApprovedEmail(u);
        }

        return id;
    }

    private void setUserStatusAndGroups(TissueLocatorUser u, boolean isAdmin) {
        if (!isAdmin) {
            u.setStatus(AccountStatus.ACTIVE);
            if (u.getId() == null) {
                u.getGroups().clear();
                u.getGroups().add(getPersistentObject(UserGroup.class, USER_GROUP_ID));
                if (getApplicationSettingService().isAccountApprovalActive()) {
                    u.setStatus(AccountStatus.PENDING);
                }
            }
        }
    }

    private String[] getApproverEmails() {
        List<String> emails = new ArrayList<String>();
        Set<AbstractUser> approvers = getUsersInRole(Role.USER_ACCOUNT_APPROVER.getName());
        for (AbstractUser approver : approvers) {
            emails.add(approver.getEmail());
        }
        return emails.toArray(new String[emails.size()]);
    }

    private void sendApprovedEmail(TissueLocatorUser user) {
        emailHelper.sendEmail("user.approved.email",
                new String[] {user.getEmail()}, new String[] {user.getUsername()});
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<UserGroup> getAdministratableGroups(TissueLocatorUser user) {
        List<UserGroup> userGroups = new ArrayList<UserGroup>();

        @SuppressWarnings("unchecked")
        Collection<Long> groupIds = CollectionUtils.collect(user.getGroups(), new Transformer() {
            /**
             * {@inheritDoc}
             */
            public Object transform(Object input) {
                UserGroup ug = (UserGroup) input;
                return ug.getId();
            }

        });

        boolean isSuperAdmin = groupIds.contains(SUPER_ADMIN_GROUP_ID);
        boolean isConsortiumAdmin = groupIds.contains(CONSORTIUM_ADMIN_GROUP_ID) || isSuperAdmin;
        boolean isUserAdmin = groupIds.contains(INST_ADMIN_GROUP_ID) || isConsortiumAdmin;

        if (isUserAdmin) {
            userGroups.add(getPersistentObject(UserGroup.class, USER_GROUP_ID));
            userGroups.add(getPersistentObject(UserGroup.class, TISSUE_TECH_GROUP_ID));
            userGroups.add(getPersistentObject(UserGroup.class, INST_ADMIN_GROUP_ID));
            userGroups.add(getPersistentObject(UserGroup.class, SCIENTIFIC_REVIEW_GROUP_ID));
        }

        if (isConsortiumAdmin) {
            userGroups.add(getPersistentObject(UserGroup.class, CONSORTIUM_ADMIN_GROUP_ID));
        }

        if (isSuperAdmin) {
            userGroups.add(getPersistentObject(UserGroup.class, SUPER_ADMIN_GROUP_ID));
        }
        return userGroups;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Set<AbstractUser> getUsersInRole(String roleName) {
        Set<AbstractUser> users = new HashSet<AbstractUser>();

        String queryString = "from " + ApplicationRole.class.getName() + " where name = :name";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setString("name", roleName);
        ApplicationRole ar = (ApplicationRole) q.uniqueResult();

        queryString = "select distinct u from " + AbstractUser.class.getName() + " as u "
            + "left join u.groups as g where u.status = :status and "
            + "(:role in elements (g.roles) or :role in elements (u.roles))";
        q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setEntity("role", ar);
        q.setParameter("status", AccountStatus.ACTIVE);
        users.addAll(q.list());
        return users;
    }

    /**
     * {@inheritDoc}
     */
    public ForgotPasswordStatus forgotPassword(String username) throws MessagingException {

        Calendar expiryDate = Calendar.getInstance();
        expiryDate.add(Calendar.HOUR, -AbstractUser.REQUEST_TIMEOUT_HOURS);

        TissueLocatorUser u = getByUsername(username);
        if (u == null) {
            LOG.debug("Unknown username for forgot password: " + username);
            return ForgotPasswordStatus.UNKNOWN_USERNAME;
        }

        if (!AccountStatus.ACTIVE.equals(u.getStatus())) {
            LOG.debug("user is not active, so password may not be reset");
            return ForgotPasswordStatus.INACTIVE_USER;
        }

        // figure out of there are too many outstanding requests, and clean up
        // old requests that for whatever reason have not been deleted yet.
        int outstandingRequests = 0;
        Iterator<PasswordReset> it = u.getPasswordResets().iterator();
        while (it.hasNext()) {
            PasswordReset pr = it.next();
            if (pr.getCreateDate().after(expiryDate.getTime())) {
                ++outstandingRequests;
            } else {
                // we can cleanup old requests
                it.remove();
                TissueLocatorHibernateUtil.getCurrentSession().delete(pr);
            }
        }
        if (outstandingRequests >= MAX_OUTSTANDING_REQUESTS) {
            LOG.debug("Too many password reset requests outstanding.  Disallowing.");
            return ForgotPasswordStatus.TOO_MANY_REQUESTS;
        }
        PasswordReset pr = PasswordReset.newInstance(u);
        TissueLocatorHibernateUtil.getCurrentSession().save(pr);

        String[] args = {pr.getNonce(), u.getFirstName(), u.getLastName()};
        emailHelper.sendEmail("user.forgotPassword.email", new String[] {u.getEmail()}, args);

        return ForgotPasswordStatus.SUCCESS;
    }

    /**
     * {@inheritDoc}
     */
    public TissueLocatorUser updatePassword(TissueLocatorUser user, String newPassword,
            String nonce) throws MessagingException {
        if (!SecurityUtils.isAcceptablePassword(newPassword)) {
            throw new IllegalArgumentException("Unacceptable password");
        }
        TissueLocatorUser u = getByUsername(user.getUsername()); // need an up to date copy
        if (nonce == null || !u.checkNonce(nonce)) {
            throw new IllegalArgumentException("Missing, invalid or unknown nonce.");
        }
        u.getPasswordResets().clear();
        u.setPassword(SecurityUtils.create(newPassword));
        TissueLocatorHibernateUtil.getCurrentSession().save(u);

        String[] args = {u.getFirstName(), u.getLastName()};
        emailHelper.sendEmail("user.changedPassword.email", new String[] {u.getEmail()}, args);

        return u;
    }

    /**
     * @return the applicationSettingService
     */
    public ApplicationSettingServiceLocal getApplicationSettingService() {
        return applicationSettingService;
    }

    /**
     * @param applicationSettingService the applicationSettingService to set
     */
    @Inject
    public void setApplicationSettingService(ApplicationSettingServiceLocal applicationSettingService) {
        this.applicationSettingService = applicationSettingService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNewUserCount(Date startDate, Date endDate) {
        String queryString = "select count(*) "
            + "from " + TissueLocatorUser.class.getName() + " user "
            + " where user.creationDate >= :start and user.creationDate < :end ";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        q.setDate("start", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        q.setDate("end", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        return ((Long) q.uniqueResult()).intValue();
    }
}
