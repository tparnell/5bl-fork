/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.support;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.AssertTrue;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.Searchable;
import com.fiveamsolutions.tissuelocator.data.AccessRestricted;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionRestricted;
import com.fiveamsolutions.tissuelocator.data.StatusTransitionable;
import com.fiveamsolutions.tissuelocator.data.Timestampable;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.annotation.OnSave;
import com.fiveamsolutions.tissuelocator.data.validation.ConsistentInstitutions;
import com.fiveamsolutions.tissuelocator.data.validation.NotFuture;
import com.fiveamsolutions.tissuelocator.util.QuestionResponseStatusTransitionHistoryHandler;

/**
 * @author ddasgupta
 *
 */
@Entity
@Table(name = "question_response",
        uniqueConstraints = @UniqueConstraint(columnNames = {"question_id", "institution_id" }))
@com.fiveamsolutions.tissuelocator.data.validation.UniqueConstraint(
        propertyNames = {"question", "institution" }, message = "{validator.questionResponse.unique}")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@ConsistentInstitutions
public class QuestionResponse implements PersistentObject, Auditable, AccessRestricted, InstitutionRestricted,
    StatusTransitionable<QuestionResponseStatusTransition, SupportRequestStatus>, Timestampable {

    private static final long serialVersionUID = -1821688486090158203L;
    private static final int RESPONSE_LENGTH = 3999;

    private Long id;
    private Institution institution;
    private TissueLocatorUser responder;
    private String response;
    private Question question;

    private SupportRequestStatus status;
    private SupportRequestStatus previousStatus;
    private Date createdDate = new Date();
    private Date lastUpdatedDate = new Date();
    private Date nextReminderDate;

    private List<QuestionResponseStatusTransition> statusTransitionHistory
        = new ArrayList<QuestionResponseStatusTransition>();
    private Date statusTransitionDate;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the institution
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "institution_id")
    @ForeignKey(name = "question_response_institution_fk")
    @Index(name = "question_response_institution_idx")
    @Searchable
    public Institution getInstitution() {
        return institution;
    }

    /**
     * @param institution the institution to set
     */
    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    /**
     * @return the responder
     */
    @ManyToOne
    @JoinColumn(name = "responder_id")
    @ForeignKey(name = "question_response_responder_fk")
    @Index(name = "question_response_responder_idx")
    public TissueLocatorUser getResponder() {
        return responder;
    }

    /**
     * @param responder the responder to set
     */
    public void setResponder(TissueLocatorUser responder) {
        this.responder = responder;
    }

    /**
     * @return the response
     */
    @Length(max = RESPONSE_LENGTH)
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return true or false depending on validity.
     */
    @AssertTrue(message = "{validator.questionResponse.required}")
    @Transient
    public boolean isResponseValid() {
        return !SupportRequestStatus.RESPONDED.equals(getStatus()) || StringUtils.isNotBlank(getResponse());
    }

    /**
     * @return the nextReminderDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "next_reminder_date")
    public Date getNextReminderDate() {
        return nextReminderDate;
    }

    /**
     * @param nextReminderDate the nextReminderDate to set
     */
    public void setNextReminderDate(Date nextReminderDate) {
        this.nextReminderDate = nextReminderDate;
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @NotFuture
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @NotFuture
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated_date")
    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return the status
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    @Searchable
    @Index(name = "question_response_status_idx")
    public SupportRequestStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(SupportRequestStatus status) {
        this.status = status;
    }

    /**
     * @return the specimen's status
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "status", updatable = false, insertable = false, nullable = false)
    public SupportRequestStatus getPreviousStatus() {
        return previousStatus;
    }

    /**
     * @param previousStatus the status to set
     */
    public void setPreviousStatus(SupportRequestStatus previousStatus) {
        this.previousStatus = previousStatus;
    }

    /**
     * @return the status transitions.
     */
    @Fetch(FetchMode.SELECT)
    @ManyToMany(fetch = FetchType.EAGER)
    @Valid
    @JoinTable(name = "question_response_status_transition_history",
                joinColumns = @JoinColumn(name = "response_id"),
                inverseJoinColumns = @JoinColumn(name = "status_transition_id"))
    @ForeignKey(name = "QUESTION_RESPONSE_TRANSITION_FK",
                inverseName = "TRANSITION_QUESTION_RESPONSE_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @OrderBy(value = "systemTransitionDate")
    public List<QuestionResponseStatusTransition> getStatusTransitionHistory() {
        return statusTransitionHistory;
    }

    /**
     * @param statusTransitionHistory the status transitions.
     */
    public void setStatusTransitionHistory(List<QuestionResponseStatusTransition> statusTransitionHistory) {
        this.statusTransitionHistory = statusTransitionHistory;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public Date getStatusTransitionDate() {
        return statusTransitionDate;
    }

    /**
     * {@inheritDoc}
     */
    public void setStatusTransitionDate(Date statusTransitionDate) {
        this.statusTransitionDate = statusTransitionDate;
    }

    /**
     * Update the status transition history based on the current and previous status.
     */
    @OnSave
    public void updateStatusTransitionHistory() {
        new QuestionResponseStatusTransitionHistoryHandler().updateStatusTransitionHistory(this);
    }

    /**
     * @return the question
     */
    @Valid
    @NotNull
    @ManyToOne
    @JoinColumn(name = "question_id")
    @ForeignKey(name = "question_response_question_fk")
    @Index(name = "question_response_question_idx")
    @Cascade(value = CascadeType.ALL)
    public Question getQuestion() {
        return question;
    }

    /**
     * @param question the question to set
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAccessible(TissueLocatorUser user) {
        return getQuestion().isAccessible(user);
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public Collection<Institution> getRelatedInstitutions() {
        return Collections.singleton(getInstitution());
    }
}
