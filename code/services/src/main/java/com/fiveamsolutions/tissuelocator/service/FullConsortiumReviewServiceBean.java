/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.mail.MessagingException;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;

/**
 * @author ddasgupta
 *
 */
@Stateless
public class FullConsortiumReviewServiceBean extends SpecimenRequestServiceBean implements
        FullConsortiumReviewServiceLocal {

    /**
     * {@inheritDoc}
     */
    public void assignRequest(SpecimenRequest request, TissueLocatorUser assignee, int votingPeriod,
            int reviewPeriod) throws MessagingException {
        savePersistentObject(request);
        EmailHelper emailHelper = new EmailHelper();
        String idString = String.valueOf(request.getId());
        SimpleDateFormat sdf = emailHelper.getDefaultDateFormat();
        String reviewDeadline = sdf.format(addDaysToUpdatedDate(request, reviewPeriod));
        String votingDeadline = sdf.format(addDaysToUpdatedDate(request, votingPeriod));
        String[] args = new String[] {idString, votingDeadline, reviewDeadline};

        if (RequestStatus.PENDING_FINAL_DECISION.equals(request.getStatus())) {
            emailHelper.sendEmail("specimenRequest.email.voteFinalized", new String[] {assignee.getEmail()}, args);
        } else if (request.getReviewers().contains(assignee.getInstitution())) {
            emailHelper.sendEmail("specimenRequest.email.review", new String[] {assignee.getEmail()}, args);
        } else {
            emailHelper.sendEmail("specimenRequest.email.vote", new String[] {assignee.getEmail()}, args);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void addComment(SpecimenRequest request, int votingPeriod) throws MessagingException {
        savePersistentObject(request);

        EmailHelper emailHelper = new EmailHelper();
        SimpleDateFormat sdf = emailHelper.getDefaultDateFormat();
        String votingDeadline = sdf.format(addDaysToUpdatedDate(request, votingPeriod));
        String idString = String.valueOf(request.getId());
        String[] args = new String[] {idString, votingDeadline};

        Set<AbstractUser> voters = new HashSet<AbstractUser>();
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null) {
                voters.add(vote.getUser());
            }
        }
        TissueLocatorUser loggedInUser = getUserService().getByUsername(UsernameHolder.getUser());
        if (voters.contains(loggedInUser) && request.getReviewers().contains(loggedInUser.getInstitution())) {
            voters.remove(loggedInUser);
            List<String> emails = new ArrayList<String>();
            for (AbstractUser voter : voters) {
                emails.add(voter.getEmail());
            }
            emailHelper.sendEmail("specimenRequest.email.reviewDone", emails.toArray(new String[emails.size()]), args);
        }
    }
}
