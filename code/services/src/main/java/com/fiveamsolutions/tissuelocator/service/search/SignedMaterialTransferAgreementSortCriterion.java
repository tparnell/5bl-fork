/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.search;

import com.fiveamsolutions.nci.commons.data.search.SortCriterion;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;

/**
 * @author ddasgupta
 */
public enum SignedMaterialTransferAgreementSortCriterion implements SortCriterion<SignedMaterialTransferAgreement> {

    /**
     * sort by id.
     */
    ID("id", null),

    /**
     * sort by sending institution name.
     */
    SENDING_INSTITUTION("name", "sendingInstitution"),

    /**
     * sort by receiving institution name.
     */
    RECEIVING_INSTITUTION("name", "receivingInstitution"),

    /**
     * sort by uploader first name (to be used in conjunction with UPLOADER_LAST_NAME).
     */
    UPLOADER_FIRST_NAME("uploader.firstName", null),

    /**
     * sort by uploader last name (to be used in conjunction with UPLOADER_FIRST_NAME).
     */
    UPLOADER_LAST_NAME("uploader.lastName", null),

    /**
     * sort by uploader email.
     */
    UPLOADER_EMAIL("uploader.email", null),

    /**
     * sort by uploader phone.
     */
    UPLOADER_PHONE("phone", "uploader.address"),

    /**
     * sort by upload time.
     */
    UPLOAD_TIME("uploadTime", null),

    /**
     * sort by status.
     */
    STATUS("status", null),

    /**
     * sort by original MTA version.
     */
    ORIGINAL_MTA_VERSION("originalMta.version", null);

    private String orderField;
    private String leftJoinField;

    private SignedMaterialTransferAgreementSortCriterion(String orderField, String leftJoinField) {
        this.orderField = orderField;
        this.leftJoinField = leftJoinField;
    }

    /**
     * {@inheritDoc}
     */
    public String getOrderField() {
        return this.orderField;
    }

    /**
     * {@inheritDoc}
     */
    public String getLeftJoinField() {
        return leftJoinField;
    }
}
