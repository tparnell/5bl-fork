/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.Deletable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.Searchable;
import com.fiveamsolutions.tissuelocator.data.validation.UniqueConstraint;
import com.fiveamsolutions.tissuelocator.hibernate.interceptor.DeleteOrphans;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchCriteria;

/**
 * This class represents a saved specimen search that can be loaded and re-run.
 *
 * @author smiller
 */
@Entity(name = "saved_specimen_search")
@UniqueConstraint(propertyNames = { "name", "owner" }, message = "{validator.savedSpecimenSearch.duplicate}")
// Note that the case insensitive unique constraint for this table in the database is specified in plain sql
@DeleteOrphans(targetProperties = { "criteriaData" }, idProperties = { "id" }, orphanClasses = { LobHolder.class })
public class SavedSpecimenSearch implements PersistentObject, Auditable, Deletable {

    private static final int MAX_LENGTH = 254;
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String description;
    private TissueLocatorUser owner;
    private LobHolder criteriaData;

    /**
     * Gets the saved specimen search id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * Sets the saved specimen search id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the saved specimen search name.
     *
     * @return the name
     */
    @NotEmpty
    @Length(max = MAX_LENGTH)
    public String getName() {
        return name;
    }

    /**
     * Sets the saved specimen search name.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the saved specimen search description.
     *
     * @return the description
     */
    @Length(max = MAX_LENGTH)
    public String getDescription() {
        return description;
    }

    /**
     * Sets the saved specimen search description.
     *
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the saved specimen search owner.
     *
     * @return the owner
     */
    @NotNull
    @Valid
    @ManyToOne
    @JoinColumn(name = "owner_id")
    @ForeignKey(name = "saved_specimen_search_tissue_locator_user_fk")
    @Searchable
    public TissueLocatorUser getOwner() {
        return owner;
    }

    /**
     * Sets the saved specimen search owner.
     *
     * @param owner the owner to set
     */
    public void setOwner(TissueLocatorUser owner) {
        this.owner = owner;
    }

    /**
     * Gets the saved specimen search criteria data.
     *
     * @return the criteriaData
     */
    @NotNull
    @Valid
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "criteria_data_id")
    @ForeignKey(name = "saved_specimen_search_lob_holder_fk")
    @Cascade(value = CascadeType.ALL)
    public LobHolder getCriteriaData() {
        return criteriaData;
    }

    /**
     * Sets the saved specimen search criteria data.
     *
     * @param criteriaData the criteria data to set
     */
    public void setCriteriaData(LobHolder criteriaData) {
        this.criteriaData = criteriaData;
    }

    /**
     * Gets the search criteria from the lob.
     *
     * @return the criteria or null
     * @throws IOException when serialization fails
     * @throws ClassNotFoundException when serialization fails
     */
    @Transient
    public SpecimenSearchCriteria getSearchCriteria() throws IOException, ClassNotFoundException {
        if (getCriteriaData() != null) {
            ByteArrayInputStream bis = new ByteArrayInputStream(getCriteriaData().getData());
            ObjectInputStream ois = new ObjectInputStream(bis);
            return (SpecimenSearchCriteria) ois.readObject();
        }

        return null;
    }

    /**
     * Sets the criteria in to the lob.
     *
     * @param criteria the criteria.
     * @throws IOException on failure of serialization.
     */
    public void setSearchCriteria(SpecimenSearchCriteria criteria) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(bos);
        os.writeObject(criteria);
        byte[] data = bos.toByteArray();
        if (getCriteriaData() == null) {
            setCriteriaData(new LobHolder());
        }
        getCriteriaData().setData(data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public boolean isDeletable() {
        return true;
    }

}
