/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.util;

import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.Vote;

/**
 * @author ddasgupta
 *
 */
public class RequestReviewReport {

    private Map<String, Map<Vote, Long>> scientificReviewMap;
    private Map<String, Map<Vote, Long>> institutionalReviewMap;
    private Map<String, Integer> lateScientificReviewMap;
    private Map<String, Integer> lateInstitutionalReviewMap;

    /**
     * @return the scientificReviewMap
     */
    public Map<String, Map<Vote, Long>> getScientificReviewMap() {
        return scientificReviewMap;
    }

    /**
     * @param scientificReviewMap the scientificReviewMap to set
     */
    public void setScientificReviewMap(Map<String, Map<Vote, Long>> scientificReviewMap) {
        this.scientificReviewMap = scientificReviewMap;
    }

    /**
     * @return the institutionalReviewMap
     */
    public Map<String, Map<Vote, Long>> getInstitutionalReviewMap() {
        return institutionalReviewMap;
    }

    /**
     * @param institutionalReviewMap the institutionalReviewMap to set
     */
    public void setInstitutionalReviewMap(Map<String, Map<Vote, Long>> institutionalReviewMap) {
        this.institutionalReviewMap = institutionalReviewMap;
    }

    /**
     * @return the lateScientificReviewMap
     */
    public Map<String, Integer> getLateScientificReviewMap() {
        return lateScientificReviewMap;
    }

    /**
     * @param lateScientificReviewMap the lateScientificReviewMap to set
     */
    public void setLateScientificReviewMap(Map<String, Integer> lateScientificReviewMap) {
        this.lateScientificReviewMap = lateScientificReviewMap;
    }

    /**
     * @return the lateInstitutionalReviewMap
     */
    public Map<String, Integer> getLateInstitutionalReviewMap() {
        return lateInstitutionalReviewMap;
    }

    /**
     * @param lateInstitutionalReviewMap the lateInstitutionalReviewMap to set
     */
    public void setLateInstitutionalReviewMap(Map<String, Integer> lateInstitutionalReviewMap) {
        this.lateInstitutionalReviewMap = lateInstitutionalReviewMap;
    }
}
