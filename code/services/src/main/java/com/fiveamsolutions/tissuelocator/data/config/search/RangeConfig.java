/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config.search;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;

import com.fiveamsolutions.tissuelocator.service.search.RangeSearchCondition;
import com.fiveamsolutions.tissuelocator.service.search.RangeSearchParameterAccessor;

/**
 * Configuration object for range search fields.
 *
 * @author gvaughn
 */
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Entity
@Table(name = "range_config")
public class RangeConfig extends SimpleSearchFieldConfig {

    private static final long serialVersionUID = 2361118636127572702L;
    private String searchConditionClass;

    /**
     * Gets the name of the property corresponding to the minimum value in the range.
     * 
     * @return the property name of the minimum value in the range.
     */
    @Transient
    public String getMinSearchFieldName() {
        return RangeSearchCondition.RANGE_MIN_PREFIX + getSearchFieldName();
    }

    /**
     * Gets the name of the property corresponding to the maximum value in the range.
     *
     * @return the property name of the maximum value in the range.
     */
    @Transient
    public String getMaxSearchFieldName() {
        return RangeSearchCondition.RANGE_MAX_PREFIX + getSearchFieldName();
    }

    /**
     * Gets the name of the optional search condition class used by the search criteria.
     *
     * @return the search condition class name
     */
    @Column(name = "search_condition_class", nullable = false)
    @Length(max = MAX_LENGTH)
    @NotEmpty
    @Override
    public String getSearchConditionClass() {
        return searchConditionClass;
    }

    /**
     * Sets the name of the optional search condition class used by the search criteria.
     *
     * @param searchConditionClass the name of the search condition class
     */
    public void setSearchConditionClass(String searchConditionClass) {
        this.searchConditionClass = searchConditionClass;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getValueString(Object object, String mapName) {
        Object minValue = getValue((RangeSearchParameterAccessor) object, getMinSearchFieldName());
        Object maxValue = getValue((RangeSearchParameterAccessor) object, getMaxSearchFieldName());
        if (StringUtils.isNotBlank(minValue.toString())) {
            if (StringUtils.isNotBlank(maxValue.toString())) {
                if (minValue.equals(maxValue)) {
                    return minValue.toString();
                }
                return minValue.toString() +  " - " + maxValue.toString();
            } else {
                return "greater than or equal to " + minValue.toString();
            }
        } else {
            if (StringUtils.isNotBlank(maxValue.toString())) {
                return "less than or equal to " + maxValue.toString();
            } else {
                return StringUtils.EMPTY;
            }
        }
    }

    private Object getValue(RangeSearchParameterAccessor accessor, String parameterName) {
        Object value = accessor.getRangeSearchParameters().get(parameterName);
        if (value == null) {
            value = StringUtils.EMPTY;
        }
        return value;
    }

}