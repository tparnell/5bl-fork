/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.search;

import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.search.SearchableUtils;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewComment;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.ShipmentStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;

/**
 * @author ddasgupta
 *
 */
public class SpecimenRequestSearchCriteria extends TissueLocatorAnnotatedBeanSearchCriteria<SpecimenRequest> {

    private static final long serialVersionUID = 1L;

    private final boolean includeReviewerAssignment;
    private final boolean includeLeadReviewer;
    private final boolean includeConsortiumReview;
    private final boolean includeInsitutionalReview;
    private final boolean includeLineItemReview;
    private final boolean includeFinalComment;
    private final boolean includeReviewerNotification;
    private final boolean includeNeedsResearcherAction;
    private final boolean includeInstitutionRestriction;
    private final TissueLocatorUser currentUser;
    private final ReviewProcess reviewProcess;

    private static final String OPEN_PAREN = " ( ";
    private static final String CLOSE_PAREN = " ) ";
    private static final String GREATER_THAN_ZERO = " > 0 ";
    private static final String CONSORTIUM_REVIEWS_CONDITION = " vote in elements("
            + SearchableUtils.ROOT_OBJ_ALIAS + ".consortiumReviews) ";
    private static final String VOTE_SELECT_CLAUSE = " select max(vote.id) from "
            + SpecimenRequestReviewVote.class.getName() + " vote ";
    private static final String LEAD_REVIEWER_CONDITION = OPEN_PAREN + VOTE_SELECT_CLAUSE
            + " where vote.user = :currentUser and vote.institution in elements ("
            + SearchableUtils.ROOT_OBJ_ALIAS + ".reviewers) and " + CONSORTIUM_REVIEWS_CONDITION
            + CLOSE_PAREN + " is not null ";
    private static final String STATUS_PROPERTY = SearchableUtils.ROOT_OBJ_ALIAS + ".status = :";
    private static final String AND = " and ";
    private static final String CURRENT_USER_INST = "currentUserInst";

    /**
     * Constructor with additional options for specimen request search.
     * @param example the example specimen request
     * @param includeReviewerAssignment whether to include the reviewer assignment conditions
     * @param includeLeadReviewer whether to include the lead reviewer conditions
     * @param includeConsortiumReview whether to include the consortium review conditions
     * @param includeInsitutionalReview whether to include the institutional review conditions
     * @param includeLineItemReview whether to include line item review conditions
     * @param includeFinalComment whether to include the final comment conditions
     * @param includeReviewerNotification whether to include the review notification conditions
     * @param includeNeedsResearcherAction whether to include needs researcher action conditions
     * @param includeInstitutionRestriction whether to include condition restricting requests to those for the
     * current user's institution
     * @param currentUser the current user
     * @param reviewProcess the reviewProcess for whom this search is being done
     */
    @SuppressWarnings("PMD.ExcessiveParameterList")
    //CHECKSTYLE:OFF too many parameters
    public SpecimenRequestSearchCriteria(SpecimenRequest example, boolean includeReviewerAssignment,
            boolean includeLeadReviewer, boolean includeConsortiumReview, boolean includeInsitutionalReview,
            boolean includeLineItemReview, boolean includeFinalComment, boolean includeReviewerNotification,
            boolean includeNeedsResearcherAction, boolean includeInstitutionRestriction,
            TissueLocatorUser currentUser, ReviewProcess reviewProcess) {
    //CHECKSTYLE:ON

        super(example);
        this.includeReviewerAssignment = includeReviewerAssignment;
        this.includeLeadReviewer = includeLeadReviewer;
        this.includeConsortiumReview = includeConsortiumReview;
        this.includeInsitutionalReview = includeInsitutionalReview;
        this.includeLineItemReview = includeLineItemReview;
        this.includeFinalComment = includeFinalComment;
        this.includeReviewerNotification = includeReviewerNotification;
        this.includeNeedsResearcherAction = includeNeedsResearcherAction;
        this.includeInstitutionRestriction = includeInstitutionRestriction;
        this.currentUser = currentUser;
        this.reviewProcess = reviewProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Query getQuery(String orderByProperty, boolean isCountOnly) {
        return getQuery(orderByProperty, null, isCountOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Query getQuery(String orderByProperty, String leftJoinClause, boolean isCountOnly) {
        return SearchableUtils.getQueryBySearchableFields(getCriteria(), isCountOnly, orderByProperty,
                leftJoinClause, getSession(), new DynamicExtensionHelper(new SpecimenRequestHelper()));
    }

    /**
     * Helper that adds checks for the additional specimen request search options.
     */
    private class SpecimenRequestHelper implements SearchableUtils.AfterIterationHelper {

        /**
         * {@inheritDoc}
         */
        @SuppressWarnings("PMD.ExcessiveMethodLength")
        public void afterIteration(Object obj, boolean isCountOnly,
                                   StringBuffer whereClause, Map<String, Object> params) {
            StringBuffer conditions = new StringBuffer();
            if (ReviewProcess.FULL_CONSORTIUM_REVIEW.equals(reviewProcess)) {
                String condition = OPEN_PAREN + VOTE_SELECT_CLAUSE
                        + " where vote.user is null and vote.institution = :currentUserInst and "
                        + CONSORTIUM_REVIEWS_CONDITION  + CLOSE_PAREN + " is not null "
                        + " and (" + STATUS_PROPERTY + RequestStatus.PENDING.name()
                        + " or (" + STATUS_PROPERTY + RequestStatus.PENDING_FINAL_DECISION.name()
                        + " and :currentUserInst in elements (" + SearchableUtils.ROOT_OBJ_ALIAS + ".reviewers))) ";
                addCondition(includeReviewerAssignment, condition, new String[]{CURRENT_USER_INST},
                        new Object[]{currentUser.getInstitution()}, conditions, params, RequestStatus.PENDING,
                        RequestStatus.PENDING_FINAL_DECISION);

                condition = LEAD_REVIEWER_CONDITION
                        + AND + SearchableUtils.ROOT_OBJ_ALIAS + ".externalComment is null ";
                addCondition(includeFinalComment, condition, new String[] {"currentUser"}, new Object[]{currentUser},
                        conditions, params, RequestStatus.PENDING_FINAL_DECISION);

                condition = LEAD_REVIEWER_CONDITION
                        + " and (select max(comment.id) from " + ReviewComment.class.getName()
                        + " comment where comment.user = :currentUser and comment in elements("
                        + SearchableUtils.ROOT_OBJ_ALIAS + ".comments) ) is null ";
                addCondition(includeLeadReviewer, condition, new String[]{"currentUser"}, new Object[]{currentUser},
                        conditions, params, RequestStatus.PENDING);

                addConsortiumReviewCondition(conditions, params);
                addInstitutionalReviewCondition(conditions, params);
                addFalseCondition(includeReviewerNotification, conditions, params);
                addFalseCondition(includeLineItemReview, conditions, params);
                addFalseCondition(includeInstitutionRestriction, conditions, params);
            } else if (ReviewProcess.FIXED_CONSORTIUM_REVIEW.equals(reviewProcess)) {
                String condition = OPEN_PAREN + " select count(vote) from "
                        + SpecimenRequestReviewVote.class.getName() + " vote where vote.user is null and "
                        + CONSORTIUM_REVIEWS_CONDITION  + CLOSE_PAREN + GREATER_THAN_ZERO;
                addCondition(includeReviewerAssignment, condition, null, null,
                        conditions, params, RequestStatus.PENDING);

                condition = SearchableUtils.ROOT_OBJ_ALIAS + ".externalComment is null ";
                addCondition(includeFinalComment, condition, null, null,
                        conditions, params, RequestStatus.PENDING_FINAL_DECISION);

                condition = SearchableUtils.ROOT_OBJ_ALIAS + ".finalVote is not null and "
                        + SearchableUtils.ROOT_OBJ_ALIAS + ".externalComment is not null ";
                addCondition(includeReviewerNotification, condition, null, null, conditions, params,
                        RequestStatus.PENDING_FINAL_DECISION);

                addConsortiumReviewCondition(conditions, params);
                addFalseCondition(includeLeadReviewer, conditions, params);
                addFalseCondition(includeInsitutionalReview, conditions, params);
                addFalseCondition(includeLineItemReview, conditions, params);
                addFalseCondition(includeInstitutionRestriction, conditions, params);
            } else if (ReviewProcess.LINE_ITEM_REVIEW.equals(reviewProcess)) {
                addLineItemReviewCondition(conditions, params);
                addInstitutionRestrictionCondition(conditions, params);
                addFalseCondition(includeReviewerAssignment, conditions, params);
                addFalseCondition(includeFinalComment, conditions, params);
                addFalseCondition(includeLeadReviewer, conditions, params);
                addFalseCondition(includeReviewerNotification, conditions, params);
                addFalseCondition(includeConsortiumReview, conditions, params);
                addFalseCondition(includeInsitutionalReview, conditions, params);
            }

            String condition = OPEN_PAREN
                    + OPEN_PAREN
                    + "select count(shipment) from " + Shipment.class.getName() + " shipment where "
                    + "(shipment.status = :shipped or shipment.status = :received) "
                    + "and shipment in elements(" + SearchableUtils.ROOT_OBJ_ALIAS + ".orders) "
                    + "and" + OPEN_PAREN
                    + OPEN_PAREN + " select count(li) from " + SpecimenRequestLineItem.class.getName()
                    + " li where li.receiptQuality is null and li in "
                    + "elements(shipment.lineItems) " + CLOSE_PAREN + GREATER_THAN_ZERO
                    + " or "
                    + OPEN_PAREN + " select count(ali) from " + AggregateSpecimenRequestLineItem.class.getName()
                    + " ali where ali.receiptQuality is null and ali in "
                    + "elements(shipment.aggregateLineItems) " + CLOSE_PAREN + GREATER_THAN_ZERO
                    + CLOSE_PAREN + CLOSE_PAREN + "  > 0 "
                    + "and " + SearchableUtils.ROOT_OBJ_ALIAS + ".requestor = :requestor " + CLOSE_PAREN
                    + " or (" + STATUS_PROPERTY + RequestStatus.PENDING_REVISION.name() + AND
                    + SearchableUtils.ROOT_OBJ_ALIAS + ".requestor = :requestor )"
                    + " or (" + STATUS_PROPERTY + RequestStatus.DRAFT.name() + AND
                    + SearchableUtils.ROOT_OBJ_ALIAS + ".requestor = :requestor )";
            addCondition(includeNeedsResearcherAction, condition, new String[]{"shipped", "received", "requestor"},
                    new Object[]{ShipmentStatus.SHIPPED, ShipmentStatus.RECEIVED, currentUser}, conditions, params,
                    RequestStatus.PENDING_REVISION, RequestStatus.DRAFT);

            if (getCriteria().getRequestor() == null) {
                whereClause.append(whereOrAnd(whereClause));
                whereClause.append(OPEN_PAREN);
                whereClause.append(SearchableUtils.ROOT_OBJ_ALIAS);
                whereClause.append(".requestor <> :requestor ");
                whereClause.append(CLOSE_PAREN);
                params.put("requestor", currentUser);

                whereClause.append(whereOrAnd(whereClause));
                whereClause.append(OPEN_PAREN);
                whereClause.append(SearchableUtils.ROOT_OBJ_ALIAS);
                whereClause.append(".status <> :");
                whereClause.append(RequestStatus.DRAFT.name());
                whereClause.append(CLOSE_PAREN);
                params.put(RequestStatus.DRAFT.name(), RequestStatus.DRAFT);
            }

            if (conditions.length() > 0) {
                whereClause.append(whereOrAnd(whereClause));
                whereClause.append(OPEN_PAREN);
                whereClause.append(conditions.toString());
                whereClause.append(CLOSE_PAREN);
            }
        }

        @SuppressWarnings("PMD.ExcessiveParameterList")
        private void addCondition(boolean includeCondition, String condition, String[] paramNames, Object[] paramValues,
                StringBuffer conditions, Map<String, Object> params, RequestStatus... statuses) {
            if (includeCondition) {
                conditions.append(getConjunction(conditions));
                conditions.append(OPEN_PAREN);
                conditions.append(condition);
                if (statuses.length == 1) {
                    conditions.append(AND + STATUS_PROPERTY + statuses[0].name());
                }
                conditions.append(CLOSE_PAREN);
                if (!ArrayUtils.isEmpty(paramNames)) {
                    int i = 0;
                    for (String paramName : paramNames) {
                        params.put(paramName, paramValues[i]);
                        i++;
                    }
                }
                for (RequestStatus status : statuses) {
                    params.put(status.name(), status);
                }
            }
        }

        private String getConjunction(StringBuffer conditions) {
            if (conditions.length() > 0) {
                return SearchableUtils.OR;
            }
            return StringUtils.EMPTY;
        }

        private void addConsortiumReviewCondition(StringBuffer conditions, Map<String, Object> params) {
            String condition = OPEN_PAREN + VOTE_SELECT_CLAUSE
                    + " where vote.user = :currentUser and vote.vote is null and "
                    + CONSORTIUM_REVIEWS_CONDITION + CLOSE_PAREN + " is not null";
            addCondition(includeConsortiumReview, condition, new String[]{"currentUser"}, new Object[]{currentUser},
                    conditions, params, RequestStatus.PENDING);
        }

        private void addInstitutionalReviewCondition(StringBuffer conditions, Map<String, Object> params) {
            String condition = OPEN_PAREN  + VOTE_SELECT_CLAUSE
                    + " where vote.vote is null and vote.institution = :currentUserInst and vote in elements("
                    + SearchableUtils.ROOT_OBJ_ALIAS + ".institutionalReviews)" + CLOSE_PAREN + " is not null";
            addCondition(includeInsitutionalReview, condition, new String[]{CURRENT_USER_INST},
                    new Object[]{currentUser.getInstitution()}, conditions, params, RequestStatus.PENDING);
        }

        private void addLineItemReviewCondition(StringBuffer conditions, Map<String, Object> params) {
            String condition = OPEN_PAREN  + VOTE_SELECT_CLAUSE
                    + " where vote.vote is null and vote.institution = :currentUserInst "
                    + " and vote in (select ali.vote from " + AggregateSpecimenRequestLineItem.class.getName()
                    + " ali where ali in elements(" + SearchableUtils.ROOT_OBJ_ALIAS + ".aggregateLineItems))"
                    + CLOSE_PAREN + " is not null";
            addCondition(getCriteria().getRequestor() == null && includeLineItemReview, condition,
                    new String[] {CURRENT_USER_INST}, new Object[] {currentUser.getInstitution()}, conditions,
                    params, RequestStatus.PENDING);
        }

        private void addInstitutionRestrictionCondition(StringBuffer conditions, Map<String, Object> params) {
            String condition = OPEN_PAREN  + " select count(ali) from "
                    + AggregateSpecimenRequestLineItem.class.getName() + " ali "
                    + " where ali.institution = :currentUserInst "
                    + " and ali in elements(" + SearchableUtils.ROOT_OBJ_ALIAS + ".aggregateLineItems) "
                    + CLOSE_PAREN + GREATER_THAN_ZERO
                    + " or " + OPEN_PAREN + " select count(shipment) from "
                    + Shipment.class.getName() + " shipment "
                    + " where shipment.sendingInstitution = :currentUserInst "
                    + " and shipment in elements(" + SearchableUtils.ROOT_OBJ_ALIAS + ".orders) "
                    + CLOSE_PAREN + GREATER_THAN_ZERO;
            addCondition(getCriteria().getRequestor() == null && includeInstitutionRestriction, condition,
                    new String[] {CURRENT_USER_INST}, new Object[] {currentUser.getInstitution()}, conditions, params);
        }

        private void addFalseCondition(boolean includeCondition, StringBuffer conditions, Map<String, Object> params) {
            String condition = OPEN_PAREN  + "1 <> 1" + CLOSE_PAREN;
            addCondition(includeCondition, condition, new String[0], new Object[0], conditions, params);
        }
    }
}
