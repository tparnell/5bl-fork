/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.util;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;

/**
 * @author ddasgupta
 *
 */
public class FixedConsortiumReviewUserActionAnalyzer extends AbstractUserActionAnalyzer {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean needsFinalComment(TissueLocatorUser user, SpecimenRequest request) {
        if (!RequestStatus.PENDING_FINAL_DECISION.equals(request.getStatus())) {
            return false;
        }
        return StringUtils.isBlank(request.getExternalComment());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean needsInstitutionalReview(TissueLocatorUser user, SpecimenRequest request) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean needsResearcherNotification(TissueLocatorUser user, SpecimenRequest request) {
        if (!RequestStatus.PENDING_FINAL_DECISION.equals(request.getStatus())) {
            return false;
        }

        return request.getFinalVote() != null && StringUtils.isNotBlank(request.getExternalComment());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean needsReviewerAssignment(TissueLocatorUser user, SpecimenRequest request) {
        if (!RequestStatus.PENDING.equals(request.getStatus())) {
            return false;
        }

        boolean unassignedReviewers = false;
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            unassignedReviewers |= vote.getUser() == null;
        }
        return unassignedReviewers;
    }
}
