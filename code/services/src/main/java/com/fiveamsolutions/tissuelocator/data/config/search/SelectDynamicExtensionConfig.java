/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.config.search;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.dynamicextensions.ControlledVocabularyDynamicFieldDefinition;

/**
 * @author ddasgupta
 *
 */
@Entity
@Table(name = "select_dynamic_extension_config")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class SelectDynamicExtensionConfig extends SimpleSearchFieldConfig {

    private static final int MIN_WIDTH_FIELD_LENGTH = 10;
    private static final long serialVersionUID = 1408559398594001445L;

    private ControlledVocabularyDynamicFieldDefinition controlledVocabConfig;
    private boolean multiSelect;
    private MultiSelectValueType multiSelectValueType;
    private boolean showEmptyOption = true;
    private String minWidth;

    /**
     * @return the controlledVocabConfig
     */
    @ManyToOne
    @JoinColumn(name = "controlled_vocab_config_id")
    @ForeignKey(name = "controlled_vocab_config_fk")
    public ControlledVocabularyDynamicFieldDefinition getControlledVocabConfig() {
        return this.controlledVocabConfig;
    }

    /**
     * @param controlledVocabConfig the controlledVocabConfig to set
     */
    public void setControlledVocabConfig(ControlledVocabularyDynamicFieldDefinition controlledVocabConfig) {
        this.controlledVocabConfig = controlledVocabConfig;
    }

    /**
     * @return Whether multiple values can be selected.
     */
    @NotNull
    @Column(name = "multi_select")
    public boolean isMultiSelect() {
        return multiSelect;
    }

    /**
     * @param multiSelect Whether multiple values can be selected.
     */
    public void setMultiSelect(boolean multiSelect) {
        this.multiSelect = multiSelect;
    }


    /**
     * @return the expected data type for values of the corresponding search field.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "multi_select_value_type")
    public MultiSelectValueType getMultiSelectValueType() {
        return multiSelectValueType;
    }

    /**
     * @param multiSelectValueType the expected data type for values of the corresponding search field.
     */
    public void setMultiSelectValueType(MultiSelectValueType multiSelectValueType) {
        this.multiSelectValueType = multiSelectValueType;
    }
    
    /**
     * @return Whether an empty option should be displayed with the values.
     */
    @NotNull
    @Column(name = "show_empty_option")
    public boolean isShowEmptyOption() {
        return showEmptyOption;
    }

    /**
     * @param showEmptyOption Whether an empty option should be displayed with the values.
     */
    public void setShowEmptyOption(boolean showEmptyOption) {
        this.showEmptyOption = showEmptyOption;
    }
    
    /**
     * @return the minWidth
     */
    @NotNull
    @Length(max = MIN_WIDTH_FIELD_LENGTH)
    @Column(name = "min_width")
    public String getMinWidth() {
        return minWidth;
    }

    /**
     * @param minWidth the minWidth to set
     */
    public void setMinWidth(String minWidth) {
        this.minWidth = minWidth;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    protected String getValueString(Object object, String mapName) {
        Object value = getValue(object, getSearchFieldName(), mapName);
        if (value != null) {
            if (isMultiSelect()) {
                return StringUtils.join((Collection<String>) value, DELIMITER);
            }
            return (String) value;
        }
        return StringUtils.EMPTY;
    }
}
