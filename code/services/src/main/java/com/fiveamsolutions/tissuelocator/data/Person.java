/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.Email;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableNotEmpty;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableNotNull;

/**
 * @author ddasgupta
 *
 */
@Entity(name = "person")
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Person implements PersistentObject, Auditable {

    private static final long serialVersionUID = 5749987314035735861L;
    private static final int LENGTH = 254;

    private Long id;
    private String firstName;
    private String lastName;
    private Institution organization;
    private Address address;
    private String email;

    /**
     * default no-arg constructor.
     */
    public Person() {
        address = new Address();
    }

    /**
     * copy constructor.
     * @param user the user to copy from
     */
    public Person(TissueLocatorUser user) {
        firstName = user.getFirstName();
        lastName = user.getLastName();
        organization = user.getInstitution();
        address = new Address(user.getAddress());
        email = user.getEmail();
    }

    /**
     * {@inheritDoc}
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id db id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the firstName
     */
    @DisableableNotEmpty
    @Length(max = LENGTH)
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    @DisableableNotEmpty
    @Length(max = LENGTH)
    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the organization
     */
    @DisableableNotNull
    @ManyToOne
    @JoinColumn(name = "organization_id")
    @ForeignKey(name = "person_organization_fk")
    @Index(name = "person_organization_idx")
    public Institution getOrganization() {
        return organization;
    }

    /**
     * @param organization the organization to set
     */
    public void setOrganization(Institution organization) {
        this.organization = organization;
    }

    /**
     * @return the address
     */
    @Valid
    @NotNull
    @ManyToOne
    @Cascade(CascadeType.ALL)
    @JoinColumn(name = "address_id")
    @ForeignKey(name = "person_address_fk")
    @Index(name = "person_address_idx")
    public Address getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * @return the email
     */
    @DisableableNotEmpty
    @Email
    @Length(max = LENGTH)
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param person the person object to copy the values from
     */
    public void copyValues(Person person) {
        setAddress(person.getAddress());
        setEmail(person.getEmail());
        setFirstName(person.getFirstName());
        setLastName(person.getLastName());
        setOrganization(person.getOrganization());
    }
}
