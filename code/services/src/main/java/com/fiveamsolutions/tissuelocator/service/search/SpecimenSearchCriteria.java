/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.search;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.search.SearchableUtils;
import com.fiveamsolutions.nci.commons.search.SearchableUtils.AfterIterationHelper;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.util.QuantityConverter;

/**
 * @author ddasgupta
 *
 */
@SuppressWarnings({ "PMD.CyclomaticComplexity" })
public class SpecimenSearchCriteria extends TissueLocatorAnnotatedBeanSearchCriteria<Specimen> {
    private static final long serialVersionUID = 1L;

    private final BigDecimal minimumAvailableQuantity;
    private final Map<String, Collection<Object>> multiSelectCollectionParamValues;
    private final Map<String, Collection<Object>> multiSelectParamValues;
    private final Collection<SearchCondition> searchConditions;

    /**
     * Constructor with additional options for specimen search.
     * @param example the example specimen
     * @param minimumAvailableQuantity the minimum available quantity
     * @param multiSelectCollectionParamMap Mapping of object property name to search 
     * param values for searchable collections.
     * @param multiSelectParamValues Mapping of object property name to search param values.
     * @param searchConditions collection of search conditions
     */
    public SpecimenSearchCriteria(Specimen example,
            BigDecimal minimumAvailableQuantity,
            Map<String, Collection<Object>> multiSelectCollectionParamMap,
            Map<String, Collection<Object>> multiSelectParamValues,
            Collection<SearchCondition> searchConditions) {
        super(example);
        this.minimumAvailableQuantity = minimumAvailableQuantity;
        this.multiSelectCollectionParamValues = multiSelectCollectionParamMap;
        this.multiSelectParamValues = multiSelectParamValues;
        this.searchConditions = searchConditions;
    }
    
    /**
     * @return the minimumAvailableQuantity
     */
    public BigDecimal getMinimumAvailableQuantity() {
        return minimumAvailableQuantity;
    }

    /**
     * @return the multiSelectCollectionParamValues
     */
    public Map<String, Collection<Object>> getMultiSelectCollectionParamValues() {
        return multiSelectCollectionParamValues;
    }

    /**
     * @return the multiSelectParamValues
     */
    public Map<String, Collection<Object>> getMultiSelectParamValues() {
        return multiSelectParamValues;
    }

    /**
     * @return the searchConditions
     */
    public Collection<SearchCondition> getSearchConditions() {
        return searchConditions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Query getQuery(String orderByProperty, boolean isCountOnly) {
        return getQuery(orderByProperty, null, isCountOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Query getQuery(String orderByProperty, String leftJoinClause, boolean isCountOnly) {
        return SearchableUtils.getQueryBySearchableFields(getCriteria(), isCountOnly, orderByProperty, leftJoinClause,
                getSession(), getAfterIterationHelper());
    }

    /**
     * exposes the private inner class to subclasses.
     * @return the inner class for the iteration helper.
     */
    public AfterIterationHelper getAfterIterationHelper() {
        return new DynamicExtensionHelper(new SpecimenHelper());
    }

    /**
     * Helper that adds checks for the additional specimen search options.
     */
    private class SpecimenHelper implements AfterIterationHelper {

        /**
         * {@inheritDoc}
         */
        public void afterIteration(Object obj, boolean isCountOnly,
                                   StringBuffer whereClause, Map<String, Object> params) {
            Specimen specimen = (Specimen) obj;

            appendAvailableQuantity(whereClause, params, specimen);
            if (multiSelectCollectionParamValues != null) {
                appendMultiSelectCollectionCondition(whereClause, params);
            }           

            if (multiSelectParamValues != null) {
                int index = 0;
                for (Map.Entry<String, Collection<Object>> paramEntry : multiSelectParamValues.entrySet()) {
                    if (!paramEntry.getValue().isEmpty()) {
                        String paramName = "multiSelect" + Integer.toString(index++);
                        String condition = String.format("%s %s." + paramEntry.getKey() + " in (:%s)",
                                whereOrAnd(whereClause), SearchableUtils.ROOT_OBJ_ALIAS, paramName);
                        whereClause.append(condition);
                        params.put(paramName, paramEntry.getValue());
                    }
                }
            }
            if (searchConditions != null) {
                for (SearchCondition searchCondition : searchConditions) {
                    searchCondition.appendCondition(obj, whereClause, params);
                }
            }
        }

        private void appendAvailableQuantity(StringBuffer whereClause, Map<String, Object> params, Specimen specimen) {
            if (minimumAvailableQuantity != null && specimen.getAvailableQuantityUnits() != null) {
                appendQuantityCondition(whereClause, params, "availableQuantity", "availableQuantityUnits",
                        ">=", minimumAvailableQuantity, specimen.getAvailableQuantityUnits(), "aq");
            }
        }

        @SuppressWarnings("PMD.ExcessiveParameterList")
        //CHECKSTYLE:OFF - too many parameters
        private void appendQuantityCondition(StringBuffer whereClause, Map<String, Object> params, String quantityField,
                String unitsField, String operator, BigDecimal quantity, QuantityUnits units, String paramSuffix) {
        //CHECKSTYLE:ON
            int index = 0;
            String cond = "(%s." + quantityField + " " + operator + " :%s " + SearchableUtils.AND
                + "%s." + unitsField + " = :%s)";
            List<String> conditions = new ArrayList<String>();
            Map<QuantityUnits, BigDecimal> conversions =
                QuantityConverter.getAllValues(quantity, units);
            for (Map.Entry<QuantityUnits, BigDecimal> entry : conversions.entrySet()) {
                String paramName = quantityField + paramSuffix + Integer.toString(index);
                String unitParamName = unitsField + paramSuffix + Integer.toString(index++);
                String unitCondition = String.format(cond, SearchableUtils.ROOT_OBJ_ALIAS,
                        paramName, SearchableUtils.ROOT_OBJ_ALIAS, unitParamName);
                conditions.add(unitCondition);
                params.put(paramName, entry.getValue());
                params.put(unitParamName, entry.getKey());
            }
            whereClause.append(whereOrAnd(whereClause));
            whereClause.append('(');
            whereClause.append(StringUtils.join(conditions, SearchableUtils.OR));
            whereClause.append(')');
        }
    }

    @SuppressWarnings("rawtypes")
    private void appendMultiSelectCollectionCondition(StringBuffer whereClause,
            Map<String, Object> params) {
        List<String> collectionConditions = new ArrayList<String>();
        int index = 0;
        for (Map.Entry<String, Collection<Object>> paramEntry : multiSelectCollectionParamValues
                .entrySet()) {
            if (!paramEntry.getValue().isEmpty()) {
                for (Object param : paramEntry.getValue()) {
                    String paramName = "multiSelectCollection"
                            + Integer.toString(index++);
                    String condition = String.format(":%s in elements(%s."
                            + paramEntry.getKey() + ")", paramName,
                            SearchableUtils.ROOT_OBJ_ALIAS);
                    collectionConditions.add(condition);
                    Object value = param.getClass().isEnum() ? ((Enum) param).ordinal() : param;
                    params.put(paramName, value);
                }
            }
        }

        if (!collectionConditions.isEmpty()) {
            whereClause.append(whereOrAnd(whereClause));
            whereClause.append('(');
            whereClause.append(StringUtils.join(collectionConditions,
                    SearchableUtils.OR));
            whereClause.append(')');
        }
    }
}