/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.config.search;


/**
 * Possible search field configuration types along with
 * metadata about the type.
 * @author gvaughn
 *
 */
@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public enum SearchFieldType {

    /**
     * Text search field.
     */
    @SuppressWarnings("unchecked")
    TEXT(TextConfig.class, (Class<AbstractSearchFieldConfig>[])
            new Class[]{TextSelectCompositeConfig.class}),

    /**
     * Select search field.
     */
    @SuppressWarnings("unchecked")
    SELECT(SelectConfig.class, (Class<AbstractSearchFieldConfig>[])
            new Class[]{TextSelectCompositeConfig.class, RangeSelectCompositeConfig.class}),

    /**
     * Range search field.
     */
    @SuppressWarnings("unchecked")
    RANGE(RangeConfig.class, (Class<AbstractSearchFieldConfig>[])
            new Class[]{RangeSelectCompositeConfig.class}),

    /**
     * Text - select composite fields.
     */
    TEXT_SELECT_COMPOSITE(TextSelectCompositeConfig.class, null),

    /**
     * Range - select composite fields.
     */
    RANGE_SELECT_COMPOSITE(RangeSelectCompositeConfig.class, null),

    /**
     * Select dynamic extension field.
     */
    SELECT_DYNAMIC_EXTENSION(SelectDynamicExtensionConfig.class, null),

    /**
     * Autocomplete fields.
     */
    @SuppressWarnings("unchecked")
    AUTOCOMPLETE(AutocompleteConfig.class, (Class<AbstractSearchFieldConfig>[])
            new Class[]{CheckboxAutocompleteCompositeConfig.class}),

    /**
     * Checkbox fields.
     */
    @SuppressWarnings("unchecked")
    CHECKBOX(CheckboxConfig.class, (Class<AbstractSearchFieldConfig>[])
            new Class[]{CheckboxAutocompleteCompositeConfig.class}),

    /**
     * Checkbox - autocomplete composites.
     */
    CHECKBOX_AUTOCOMPLETE_COMPOSITE(CheckboxAutocompleteCompositeConfig.class, null);

    private Class<? extends AbstractSearchFieldConfig> configClass;
    private Class<AbstractSearchFieldConfig>[] compositeClasses;

    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    private SearchFieldType(Class<? extends AbstractSearchFieldConfig> configClass,
            Class<AbstractSearchFieldConfig>[] selectCompositeClasses) {
        this.configClass = configClass;
        this.compositeClasses = selectCompositeClasses;
    }

    /**
     * Class type of the configuration object corresponding to this type.
     * @return Class type of the configuration object corresponding to this type.
     */
    public Class<? extends AbstractSearchFieldConfig> getConfigClass() {
        return configClass;
    }

    /**
     * Returns the composite configuration types that
     * include this type as a component.
     * @return the composite configuration types that include this type as a component.
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public Class<AbstractSearchFieldConfig>[] getCompositeClasses() {
        return compositeClasses;
    }

}
