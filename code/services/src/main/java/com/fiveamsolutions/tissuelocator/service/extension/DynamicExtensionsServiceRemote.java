/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.extension;

import java.util.Collection;

import javax.ejb.Remote;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.struts2.converter.DynamicExtensionsTypeConversionException;

/**
 * A remote service for performing dynamic extensions related actions, like type conversion of
 * extensions to an extendable object and retrieving dynamic field definitions.
 *
 * @author jstephens
 */
@Remote
public interface DynamicExtensionsServiceRemote {

    /**
     * Recursively converts all custom property values of the specified object
     * to their field defined types.
     *
     * @param  object the object to be converted.
     * @return the converted object.
     * @throws DynamicExtensionsTypeConversionException when an error occurs
     * during the conversion.
     */
    Object convertObject(Object object) throws DynamicExtensionsTypeConversionException;

    /**
     * Get the dynamic field definitions for a class.
     *
     * @param  clazz the full name of the class whose dynamic field definitions are to be retrieved
     * @return a collection of dynamic field definitions for the given class.
     */
    Collection<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions(String clazz);
}
