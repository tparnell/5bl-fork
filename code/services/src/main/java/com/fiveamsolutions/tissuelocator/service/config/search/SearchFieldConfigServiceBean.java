/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service.config.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.hibernate.Criteria;
import org.hibernate.Query;

import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchFieldType;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author gvaughn
 *
 */
@Stateless
@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public class SearchFieldConfigServiceBean extends
        GenericServiceBean<AbstractSearchFieldConfig> implements
        SearchFieldConfigServiceLocal {

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public Collection<AbstractSearchFieldConfig> getSearchFieldConfigs(SearchSetType searchSetType) {
        List<AbstractSearchFieldConfig> configs = new ArrayList<AbstractSearchFieldConfig>();
        for (SearchFieldType type : SearchFieldType.values()) {
            if (type.getCompositeClasses() != null) {
                configs.addAll(getSimpleConfigs(type, searchSetType));
            } else {
                StringBuffer hql = new StringBuffer("from ");
                hql.append(type.getConfigClass().getName());
                if (!searchSetType.equals(SearchSetType.ADVANCED)) {
                    hql.append(" where searchSetType = :set");
                }
                Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(hql.toString());
                if (!searchSetType.equals(SearchSetType.ADVANCED)) {
                    q.setParameter("set", searchSetType);                    
                }
                configs.addAll(q.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list());
            }
        }

        @SuppressWarnings("rawtypes")
        Comparator c = new Comparator<AbstractSearchFieldConfig>() {
            /** {@inheritDoc} */
            public int compare(AbstractSearchFieldConfig o1, AbstractSearchFieldConfig o2) {
                return new CompareToBuilder().append(o1.getId(), o2.getId()).toComparison();
            }
        };
        Collections.sort(configs, c);

        return configs;
    }

    @SuppressWarnings("unchecked")
    private Collection<AbstractSearchFieldConfig> getSimpleConfigs(SearchFieldType type, SearchSetType searchSetType) {
        StringBuffer query = new StringBuffer("select distinct config from ");
        query.append(type.getConfigClass().getName());
        query.append(" config where ");
        query.append(getCompositeQuery(type));
        if (!searchSetType.equals(SearchSetType.ADVANCED)) {
            query.append(" and config.searchSetType = :set");            
        }
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(query.toString());
        if (!searchSetType.equals(SearchSetType.ADVANCED)) {
            q.setParameter("set", searchSetType); 
        }
        return (Collection<AbstractSearchFieldConfig>) q.list();
    }

    @SuppressWarnings({ "rawtypes" })
    private String getCompositeQuery(final SearchFieldType type) {
        List<String> compositeIdClauses = new ArrayList<String>();
        for (Class compositeClass : type.getCompositeClasses()) {
            String q = "config.id not in ( select "
                + StringUtils.uncapitalize(type.getConfigClass().getSimpleName()) + ".id"
                + " from " + compositeClass.getSimpleName() + ")";
            compositeIdClauses.add(q);
        }
        return StringUtils.join(compositeIdClauses, " and ");
    }

}
