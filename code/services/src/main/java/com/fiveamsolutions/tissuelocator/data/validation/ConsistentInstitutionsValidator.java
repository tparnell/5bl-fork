/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data.validation;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.hibernate.mapping.Property;
import org.hibernate.validator.PropertyConstraint;
import org.hibernate.validator.Validator;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.support.QuestionResponse;


/**
 * @author ddasgupta
 */
@SuppressWarnings("PMD.CyclomaticComplexity")
public class ConsistentInstitutionsValidator
    implements Validator<ConsistentInstitutions>, PropertyConstraint, Serializable {

    private static final long serialVersionUID = -1084082261265174629L;

    /**
     * {@inheritDoc}
     */
    public boolean isValid(Object value) {
        if (value instanceof Specimen) {
            return areSpecimenInstitutionsValid((Specimen) value);
        }

        if (value instanceof SpecimenRequestReviewVote) {
            return areInstitutionReviewInstitutionsValid((SpecimenRequestReviewVote) value);
        }

        if (value instanceof QuestionResponse) {
            return areQuestionResponseInstitutionsValid((QuestionResponse) value);
        }

        return false;
    }

    private boolean areInstitutionReviewInstitutionsValid(SpecimenRequestReviewVote review) {
        if (review.getInstitution() == null || review.getUser() == null
                || review.getInstitution().getId().equals(review.getUser().getInstitution().getId())) {
            return true;
        }
        return false;
    }

    private boolean areSpecimenInstitutionsValid(Specimen specimen) {
        Long specId = null;
        if (specimen.getExternalIdAssigner() != null) {
            specId = specimen.getExternalIdAssigner().getId();
        }
        Long protId = null;
        if (specimen.getProtocol() != null && specimen.getProtocol().getInstitution() != null) {
            protId = specimen.getProtocol().getInstitution().getId();
        }
        Long partId = null;
        if (specimen.getParticipant() != null && specimen.getParticipant().getExternalIdAssigner() != null) {
            partId = specimen.getParticipant().getExternalIdAssigner().getId();
        }
        return new EqualsBuilder().append(specId, protId).append(specId, partId).isEquals();
    }

    private boolean areQuestionResponseInstitutionsValid(QuestionResponse response) {
        if (response.getInstitution() == null || response.getResponder() == null
                || response.getInstitution().getId().equals(response.getResponder().getInstitution().getId())) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public void initialize(ConsistentInstitutions params) {
        // do nothing
    }

    /**
     * {@inheritDoc}
     */
    public void apply(Property property) {
        // do nothing
    }
}
