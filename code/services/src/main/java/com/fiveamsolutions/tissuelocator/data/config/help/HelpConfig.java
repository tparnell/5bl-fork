/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config.help;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * Encapsulates the help configuration for an entity class.
 * @author gvaughn
 *
 */
public class HelpConfig {

    private static final Map<String, Map<String, FieldHelpConfig>> FIELD_CONFIG_MAP 
        = Collections.synchronizedMap(new HashMap<String, Map<String, FieldHelpConfig>>());
    
    private final Map<String, FieldHelpConfig> configMap 
        = new HashMap<String, FieldHelpConfig>();
    
    /**
     * Initializes a help configuration.
     * @param entityClassName Entity class for this help configuration.
     */
    public HelpConfig(String entityClassName) {
        configMap.putAll(getFieldConfigs(entityClassName));
    }
    
    /**
     * Returns the help configuration for the given field, or null if none exists.
     * @param fieldName Field name.
     * @return The corresponding help configuration.
     */
    public FieldHelpConfig getFieldHelpConfig(String fieldName) {
        return configMap.get(fieldName);
    }
    
    private static synchronized Map<String, FieldHelpConfig> getFieldConfigs(String entityClassName) {
        if (!FIELD_CONFIG_MAP.containsKey(entityClassName)) {
            Map<String, FieldHelpConfig> fieldConfigMap = new HashMap<String, FieldHelpConfig>();
            List<FieldHelpConfig> configList = TissueLocatorRegistry.getServiceLocator()
                .getHelpConfigService().getFieldHelpConfigs(entityClassName);
            for (FieldHelpConfig config : configList) {
                fieldConfigMap.put(config.getFieldName(), config);
            }
            FIELD_CONFIG_MAP.put(entityClassName, fieldConfigMap);
        }
        return FIELD_CONFIG_MAP.get(entityClassName);
    }
}
