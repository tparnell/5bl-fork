/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.user;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ejb.Local;
import javax.mail.MessagingException;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.data.security.UserGroup;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.ForgotPasswordStatus;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;

/**
 * @author ddasgupta
 *
 */
@Local
public interface TissueLocatorUserServiceLocal extends GenericServiceLocal<TissueLocatorUser> {

    /**
     * Get the <code>TissueLocatorUser</code> object for the given username.
     * @param username the username of the <code>TissueLocatorUser</code> object to be retrieved
     * @return an initialized <code>TissueLocatorUser</code> object with the given username
     */
    TissueLocatorUser getByUsername(String username);

    /**
     * Deny a user registration request. An email is sent to the user with a
     * reason for the denial.
     *
     * @param user The denied user registration.
     * @throws MessagingException if an error occurs while sending email.
     */
    void denyUser(TissueLocatorUser user) throws MessagingException;
    
    /**
     * Deactivate an active user. An email is sent to the user with a
     * reason for the deactivation.
     *
     * @param user The deactivated user.
     * @throws MessagingException if an error occurs while sending email.
     */
    void deactivateUser(TissueLocatorUser user) throws MessagingException;

    /**
     * Save or update a user and send an email to that user describing the change.
     * @param u the user to save
     * @param isAdmin whether the current user is a user administrator
     * @param password the user's password
     * @return the id of the user
     * @throws MessagingException on error sending email
     */
    Long saveUser(TissueLocatorUser u, boolean isAdmin, String password) throws MessagingException;

    /**
     * Gets the groups that this user has all of the roles in.
     * @param user the user
     * @return the list of groups
     */
    List<UserGroup> getAdministratableGroups(TissueLocatorUser user);

    /**
     * Get the users in a given role.
     * @param roleName the name of the role.
     * @return a list of users in a given role
     */
    Set<AbstractUser> getUsersInRole(String roleName);

    /**
     * Sets up the system with a password reset request.  Emails the provided user with a password change link.
     *
     * @param username the username that claims to have forgotten their password
     * @return An indication of the result of this action.
     * @throws MessagingException on email exception
     */
    ForgotPasswordStatus forgotPassword(String username) throws MessagingException;

    /**
     * Updates the user password to the new provided value.  A valid PasswordReset for the given username
     * must exist, based on a provided nonce. If a valid PasswordReset does not exist, this method throws
     * an unchecked exception.  Otherwise, the password is updated and all PasswordRests for the given user
     * are deleted.
     *
     * @See PasswordReset
     * @param user the user to change the password for.  must not be null.  may be from prior session.
     * @param newPassword requested new password.  may be null (but must pass validation)
     * @param nonce a valid nonce from a current <code>PasswordReset</code> for the provided username.
     * @return the user, with updated password
     * @throws MessagingException if emailing the user about the changed password failed
     */
    TissueLocatorUser updatePassword(TissueLocatorUser user, String newPassword,
            String nonce) throws MessagingException;

    /**
     * Finds a user with this role and belonging to the institution.
     * @param roleName the name of the role that we are looking for
     * @param i the institution that the user belongs to
     * @return the user, with this role and belonging to this institution
     */
    Set<TissueLocatorUser> getByRoleAndInstitution(String roleName, Institution i);

    /**
     * Returns users with the given role belonging to one of the
     * given institutions.
     * @param roleName Role of the users to be returned.
     * @param institutions Institutions to which the users may belong.
     * @return  users with the given role belonging to one of the given institutions.
     */
    Set<TissueLocatorUser> getByRoleAndInstitutions(String roleName, Set<Institution> institutions);

    /**
     * Get the number of users created within a given time period.
     * @param startDate the start date of the date range
     * @param endDate the end date of the date range
     * @return  a count of the number of users created between the start date and the end date
     */
    int getNewUserCount(Date startDate, Date endDate);
}
