/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.config.search;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.tissuelocator.data.code.Code;

/**
 * Search field configuration object corresponding to an autocomplete
 * box with a corresponding checkbox.
 * @author gvaughn
 *
 */
@Entity
@Table(name = "autocomplete_config")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class AutocompleteConfig extends SimpleSearchFieldConfig {

    private static final long serialVersionUID = -8639924326415167522L;

    private String valueProperty;
    private String listUrl;

    /**
     * @return The name of the property containing the selection value.
     */
    @NotNull
    @Length(max = MAX_LENGTH)
    @Column(name = "value_property")
    public String getValueProperty() {
        return valueProperty;
    }

    /**
     * @param valueProperty The name of the property containing the selection value.
     */
    public void setValueProperty(String valueProperty) {
        this.valueProperty = valueProperty;
    }

    /**
     * @return The url for obtaining the value list.
     */
    @NotNull
    @Length(max = MAX_LENGTH)
    @Column(name = "list_url")
    public String getListUrl() {
        return listUrl;
    }

    /**
     * @param listUrl The url for obtaining the value list.
     */
    public void setListUrl(String listUrl) {
        this.listUrl = listUrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getValueString(Object object, String mapName) {
        Object value = getValue(object, getSearchFieldName(), mapName);
        if (value != null) {
            if (Code.class.isAssignableFrom(value.getClass())) {
                return ((Code) value).getName();
            }
            return value.toString();
        }
        return StringUtils.EMPTY;
    }
}
