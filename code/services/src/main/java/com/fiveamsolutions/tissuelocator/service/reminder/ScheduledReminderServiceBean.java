/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.reminder;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.support.Question;
import com.fiveamsolutions.tissuelocator.data.support.QuestionResponse;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.QuestionResponseServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.SupportLetterRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.google.inject.Inject;

/**
 * @author smiller
 */
@Stateless
public class ScheduledReminderServiceBean implements ScheduledReminderServiceLocal {

    private final EmailHelper emailHelper = new EmailHelper();

    @EJB
    private ShipmentServiceLocal shipmentServiceLocal;

    @EJB
    private SpecimenRequestServiceLocal requestService;

    @EJB
    private TissueLocatorUserServiceLocal userService;

    private SupportLetterRequestServiceLocal supportLetterRequestService;
    private QuestionResponseServiceLocal questionResponseService;

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void sendReminderEmails(int shipmentReceiptGracePeriod, int lineItemReviewGracePeriod,
            int supportLetterRequestReviewGracePeriod, int questionResponseGracePeriod) {

        try {
            // Manually managing session because this method is called from a background thread,
            // so the OSIV filter is not in effect
            TissueLocatorHibernateUtil.getHibernateHelper().openAndBindSession();
            if (shipmentReceiptGracePeriod > 0) {
                sendOrderRecieptReminderEmails(shipmentReceiptGracePeriod);
            }
            if (lineItemReviewGracePeriod > 0) {
                sendLineItemReviewReminderEmails(lineItemReviewGracePeriod);
            }
            if (supportLetterRequestReviewGracePeriod > 0) {
                sendSupportLetterRequestReviewReminderEmails(supportLetterRequestReviewGracePeriod);
            }
            if (questionResponseGracePeriod > 0) {
                sendQuestionResponseReminderEmails(questionResponseGracePeriod);
            }

        } finally {
            TissueLocatorHibernateUtil.getHibernateHelper().unbindAndCleanupSession();
        }
    }

    private void sendOrderRecieptReminderEmails(int shipmentReceiptGracePeriod) {
        // Calculate the next reminder date to be just before this thread runs again after
        // a new round of the grace period passes.
        Date nextReminder = new Date();
        nextReminder = DateUtils.addHours(nextReminder, -1);
        nextReminder = DateUtils.addDays(nextReminder, shipmentReceiptGracePeriod);

        // Get the list of orders that have not been marked as received since the grace period has expired.
        List<Shipment> shipments = getShipmentService().getUnreceivedShippedOrders(shipmentReceiptGracePeriod);

        for (Shipment shipment : shipments) {
            // for each of these orders, find the tissue tech and the researcher to email
            TissueLocatorUser tissueTech = shipment.getProcessingUser();
            Person recepient = shipment.getRecipient();

            // generate tissue techs reminder email
            String shipmentDate = emailHelper.getDefaultDateFormat().format(shipment.getShipmentDate());
            emailHelper.sendEmail("shipment.email.tissueTech.notReceived",
                    new String[] {tissueTech.getEmail() },
                    new String[] {shipment.getId().toString(),
                        shipmentDate,
                        Integer.toString(shipmentReceiptGracePeriod)});

            // generate researcher's reminder email
            emailHelper.sendEmail("shipment.email.researcher.notReceived",
                    new String[] {recepient.getEmail() },
                    new String[] {shipment.getId().toString(),
                        shipmentDate, shipment.getSendingInstitution().getName()});

            shipment.setNextReminderDate(nextReminder);
            shipmentServiceLocal.savePersistentObject(shipment);
        }
    }

    @SuppressWarnings("unchecked")
    private void sendLineItemReviewReminderEmails(int lineItemReviewGracePeriod) {
        // Calculate the next reminder date to be just before this thread runs again after
        // a new round of the grace period passes.
        Date nextReminder = new Date();
        nextReminder = DateUtils.addHours(nextReminder, -1);
        nextReminder = DateUtils.addDays(nextReminder, lineItemReviewGracePeriod);

        List<SpecimenRequest> requests = getRequestService().getUnreviewedRequests(lineItemReviewGracePeriod);

        for (SpecimenRequest request : requests) {
            Set<String> reviewerEmails = new HashSet<String>();
            for (AggregateSpecimenRequestLineItem lineItem : request.getAggregateLineItems()) {
                if (lineItem.getVote().getVote() == null) {
                    Set<TissueLocatorUser> reviewers = getUserService().getByRoleAndInstitution(
                            Role.LINE_ITEM_REVIEW_VOTER.getName(), lineItem.getInstitution());
                    reviewerEmails.addAll(CollectionUtils.collect(reviewers, new Transformer() {

                        public Object transform(Object input) {
                            TissueLocatorUser user = (TissueLocatorUser) input;
                            return user.getEmail();
                        }
                    }));
                }
            }

            String requestDate = emailHelper.getDefaultDateFormat().format(request.getRequestedDate());
            emailHelper.sendEmail("specimenRequest.email.lineItemReviewer.notReviewed",
                    reviewerEmails.toArray(new String[reviewerEmails.size()]),
                    new String[] {request.getId().toString(),
                        requestDate,
                        Integer.toString(lineItemReviewGracePeriod)});

            request.setNextReminderDate(nextReminder);
            getRequestService().savePersistentObject(request);
        }
    }

    private void sendSupportLetterRequestReviewReminderEmails(int supportLetterRequestReviewGracePeriod) {
        // Calculate the next reminder date to be just before this thread runs again after
        // a new round of the grace period passes.
        Date nextReminder = new Date();
        nextReminder = DateUtils.addHours(nextReminder, -1);
        nextReminder = DateUtils.addDays(nextReminder, supportLetterRequestReviewGracePeriod);
        String[] reviewers = getReviewerEmails();

        // Get the list of orders that have not been marked as received since the grace period has expired.
        List<SupportLetterRequest> requests =
            getSupportLetterRequestService().getUnreviewedRequests(supportLetterRequestReviewGracePeriod);

        for (SupportLetterRequest request : requests) {
            // for each of these requests, email all of the request reviewers
            String createdDate = emailHelper.getDefaultDateFormat().format(request.getCreatedDate());
            emailHelper.sendEmail("supportLetterRequest.reminder.email", reviewers,
                    new String[] {request.getId().toString(), request.getRequestor().getFirstName(),
                        request.getRequestor().getLastName(), createdDate,
                        Integer.toString(supportLetterRequestReviewGracePeriod)});

            request.setNextReminderDate(nextReminder);
            getSupportLetterRequestService().updateSupportLetterRequest(request);
        }

    }

    private String[] getReviewerEmails() {
        List<String> emails = new ArrayList<String>();
        Set<AbstractUser> reviewers = getUserService().getUsersInRole(Role.SUPPORT_LETTER_REQUEST_REVIEWER.getName());
        for (AbstractUser reviewer : reviewers) {
            emails.add(reviewer.getEmail());
        }
        return emails.toArray(new String[emails.size()]);
    }

    private void sendQuestionResponseReminderEmails(int questionResponseGracePeriod) {
        Date nextReminder = new Date();
        nextReminder = DateUtils.addHours(nextReminder, -1);
        nextReminder = DateUtils.addDays(nextReminder, questionResponseGracePeriod);
        Map<Long, List<String>> responders = getResponderMap();

        // Get the list of questions that have not been marked as responded since the grace period has expired.
        List<QuestionResponse> responses =
            getQuestionResponseService().getUnrespondedQuestions(questionResponseGracePeriod);
        Set<Question> questions = new TreeSet<Question>(new Comparator<Question>() {
            @Override
            public int compare(Question q1, Question q2) {
                return q1.getId().compareTo(q2.getId());
            }
        });

        for (QuestionResponse response : responses) {
            response.setNextReminderDate(nextReminder);
            getQuestionResponseService().updateQuestionResponse(response);
            questions.add(response.getQuestion());

            // for each of these questions, email all of the question responders
            List<String> institutionResponders = responders.get(response.getInstitution().getId());
            String createdDate = emailHelper.getDefaultDateFormat().format(response.getCreatedDate());
            emailHelper.sendEmail("questionResponse.reminder.responder.email",
                    institutionResponders.toArray(new String[institutionResponders.size()]),
                    new String[] {response.getQuestion().getId().toString(),
                        response.getQuestion().getRequestor().getFirstName(),
                        response.getQuestion().getRequestor().getLastName(),
                        createdDate, response.getId().toString(),
                        Integer.toString(questionResponseGracePeriod)});
        }
        sendQuestionReminderEmails(questions, questionResponseGracePeriod);

    }

    private Map<Long, List<String>> getResponderMap() {
        Map<Long, List<String>> responderMap = new HashMap<Long, List<String>>();
        Set<AbstractUser> reviewers = getUserService().getUsersInRole(Role.QUESTION_RESPONDER.getName());
        for (AbstractUser reviewer : reviewers) {
            Long instId = ((TissueLocatorUser) reviewer).getInstitution().getId();
            List<String> responders = responderMap.get(instId);
            if (responders == null) {
                responders = new ArrayList<String>();
                responderMap.put(instId, responders);
            }
            responders.add(reviewer.getEmail());
        }
        return responderMap;
    }

    private void sendQuestionReminderEmails(Set<Question> questions, int questionResponseGracePeriod) {
        Set<AbstractUser> admins = getUserService().getUsersInRole(Role.QUESTION_ADMINISTRATOR.getName());
        List<String> adminEmails = new ArrayList<String>();
        for (AbstractUser admin : admins) {
            adminEmails.add(admin.getEmail());
        }

        for (Question question : questions) {
            List<String> outstandingInstitutionNames = new ArrayList<String>();
            for (QuestionResponse response : question.getResponses()) {
                if (SupportRequestStatus.PENDING.equals(response.getStatus())) {
                    outstandingInstitutionNames.add(response.getInstitution().getName());
                }
            }
            String createdDate = emailHelper.getDefaultDateFormat().format(question.getCreatedDate());
            emailHelper.sendEmail("questionResponse.reminder.administrator.email",
                    adminEmails.toArray(new String[adminEmails.size()]),
                    new String[] {question.getId().toString(), question.getRequestor().getFirstName(),
                        question.getRequestor().getLastName(), createdDate,
                        StringUtils.join(outstandingInstitutionNames, ", "),
                        Integer.toString(questionResponseGracePeriod)});
        }
    }

    /**
     * @return the specimenRequestService
     */
    public ShipmentServiceLocal getShipmentService() {
        return shipmentServiceLocal;
    }

    /**
     * @param shipmentService the shipmentService to set
     */
    public void setShipmentService(ShipmentServiceLocal shipmentService) {
        shipmentServiceLocal = shipmentService;
    }

    /**
     * @return the requestServiceLocal
     */
    public SpecimenRequestServiceLocal getRequestService() {
        return requestService;
    }

    /**
     * @param requestServiceLocal the requestServiceLocal to set
     */
    public void setRequestService(
            SpecimenRequestServiceLocal requestServiceLocal) {
        requestService = requestServiceLocal;
    }

    /**
     * @return the userService
     */
    public TissueLocatorUserServiceLocal getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    @Inject
    public void setUserService(
            TissueLocatorUserServiceLocal userService) {
        this.userService = userService;
    }

    /**
     * @return the supportLetterRequestService
     */
    public SupportLetterRequestServiceLocal getSupportLetterRequestService() {
        return supportLetterRequestService;
    }

    /**
     * @param supportLetterRequestService the supportLetterRequestService to set
     */
    @Inject
    public void setSupportLetterRequestService(SupportLetterRequestServiceLocal supportLetterRequestService) {
        this.supportLetterRequestService = supportLetterRequestService;
    }

    /**
     * @return the questionResponseService
     */
    public QuestionResponseServiceLocal getQuestionResponseService() {
        return questionResponseService;
    }

    /**
     * @param questionResponseService the questionResponseService to set
     */
    @Inject
    public void setQuestionResponseService(QuestionResponseServiceLocal questionResponseService) {
        this.questionResponseService = questionResponseService;
    }
}
