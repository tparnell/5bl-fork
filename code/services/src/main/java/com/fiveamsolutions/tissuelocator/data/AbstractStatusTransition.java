/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.validation.NotFuture;

/**
 * @author gvaughn
 * @param <E> The type of status.
 */
@MappedSuperclass
public abstract class AbstractStatusTransition<E extends Enum<E>> implements PersistentObject {

    private static final long serialVersionUID = -7793825211496331361L;

    private Long id;
    private Date transitionDate = new Date();
    private Date systemTransitionDate = new Date();
    private E previousStatus;
    private E newStatus;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @Enumerated(value = EnumType.STRING)
    @Column(name = "previous_status")
    public E getPreviousStatus() {
        return previousStatus;
    }

    /**
     * {@inheritDoc}
     */
    public void setPreviousStatus(E previousStatus) {
        this.previousStatus = previousStatus;
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @Enumerated(value = EnumType.STRING)
    @Column(name = "new_status")
    public E getNewStatus() {
        return newStatus;
    }

    /**
     * {@inheritDoc}
     */
    public void setNewStatus(E newStatus) {
        this.newStatus = newStatus;
    }
    /**
     * @return the actual date the transition occurred.
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "transition_date")
    public Date getTransitionDate() {
        return transitionDate;
    }

    /**
     * @param transitionDate the actual date the transition occurred.
     */
    public void setTransitionDate(Date transitionDate) {
        this.transitionDate = transitionDate;
    }

    /**
     * @return the date the transition occured in the system.
     */
    @NotNull
    @NotFuture
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "system_transition_date")
    public Date getSystemTransitionDate() {
        return systemTransitionDate;
    }

    /**
     * @param systemTransitionDate the date the transition occured in the system.
     */
    public void setSystemTransitionDate(Date systemTransitionDate) {
        this.systemTransitionDate = systemTransitionDate;
    }
}
