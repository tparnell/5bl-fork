/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * This adapter transforms a map of custom properties into a form that can
 * be marshaled and unmarshaled by a Java XML binder. The adapter converts
 * custom property dynamic extension object values into strings that can
 * be persisted to XML.
 * 
 * @author jstephens
 */
public class CustomPropertiesMapAdapter
    extends XmlAdapter<CustomPropertiesMapType, Map<String, Object>> {

    /**
     * Converts a bound custom properties map to a value custom properties map.
     * 
     * @param boundMap the map bound to the application.
     * @return the converted value type map.
     */
    @Override
    public CustomPropertiesMapType marshal(Map<String, Object> boundMap) {
        CustomPropertiesMapType valueMap = new CustomPropertiesMapType();
        for (Entry<String, Object> entry : boundMap.entrySet()) {
            valueMap.put(entry.getKey(), entry.getValue().toString());
        }
        return valueMap;
    }
    
    /**
     * Converts a value custom properties map to a bound custom properties map.
     * 
     * @param valueMap the map associated with the XML representation.
     * @return the converted bounded type map.
     */
    @Override
    public Map<String, Object> unmarshal(CustomPropertiesMapType valueMap) {
        Map<String, Object> boundMap = new HashMap<String, Object>();
        for (CustomPropertiesMapType.MapTypeEntry entry
                : valueMap.getEntryList()) {
            boundMap.put(entry.getKey(), entry.getValue());
        }
        return boundMap;
    }
    
}
