/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.util;

import com.fiveamsolutions.tissuelocator.data.AbstractStatusTransition;
import com.fiveamsolutions.tissuelocator.data.StatusTransitionable;

/**
 * Updates an object's status transition history if 1) the status has changed since
 * the last update, and 2) the object passes validation. Any previous unsaved transitions in the
 * history will be removed, regardless of whether the history is updated.
 * 
 * @author gvaughn
 * @param <E> The status type.
 * @param <T> Status transition type.
 */
public abstract class AbstractStatusTransitionHistoryHandler<T extends AbstractStatusTransition<E>, 
    E extends Enum<E>> {

    private static final long serialVersionUID = 1918899065603447142L;

    /**
     * Updates the status transition history of the given object based
     * on the current status and previous status. The history will not be
     * updated if the object fails validation. In all cases, any previous unsaved
     * transitions will be removed.
     * @param transitionable Object to be updated.
     */
    public void updateStatusTransitionHistory(StatusTransitionable<T, E> transitionable) {
        if (isStatusUpdated(transitionable) && isValid(transitionable)) {
            T transition = getStatusTransition();
            transition.setNewStatus(transitionable.getStatus());
            transition.setPreviousStatus(transitionable.getPreviousStatus());
            if (transitionable.getStatusTransitionDate() != null) {
                transition.setTransitionDate(transitionable
                        .getStatusTransitionDate());
                transitionable.setStatusTransitionDate(null);
            }
            performAdditionalProcessing(transitionable, transition);
            transitionable.getStatusTransitionHistory().add(transition);
            transitionable.setPreviousStatus(transitionable.getStatus());
        }
    }
    
    private boolean isStatusUpdated(StatusTransitionable<T, E> transitionable) {
        return transitionable.getPreviousStatus() != null 
        && !transitionable.getPreviousStatus().equals(
                transitionable.getStatus());
    }
    
    /**
     * Subclasses can override this method to perform any type-specific processing.
     * No-op by default.
     * @param transitionable Object to be updated.
     * @param transition Status transition for the current update.
     */
    @SuppressWarnings("PMD.EmptyMethodInAbstractClassShouldBeAbstract")
    protected void performAdditionalProcessing(StatusTransitionable<T, E> transitionable, T transition) {
        // no-op
    }
    
    /**
     * @return An appropriate status transition.
     */
    protected abstract T getStatusTransition();
    
    /**
     * Whether the object to be updated is otherwise valid.
     * @param transitionable The object to be updated.
     * @return Whether the object to be updated is valid.
     */
    protected abstract boolean isValid(StatusTransitionable<T, E> transitionable);
}
