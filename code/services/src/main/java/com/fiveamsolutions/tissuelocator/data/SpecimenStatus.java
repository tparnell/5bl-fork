/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author aevansel, smiller
 */
public enum SpecimenStatus {
    
    /**
     * Available.
     */
    AVAILABLE("specimenStatus.available"),
    
    /**
     * Under review.
     */
    UNDER_REVIEW("specimenStatus.underReview"),
    
    /**
     * Under review.
     */
    PENDING_SHIPMENT("specimenStatus.pendingShipment"),
    
    /**
     * Unavailable.
     */
    UNAVAILABLE("specimenStatus.unavailable"),
    
    /**
     * Shipped.
     */
    SHIPPED("specimenStatus.shipped"),
    
    /**
     * Returned to source institution.
     */
    RETURNED_TO_SOURCE_INSTITUTION("specimenStatus.returnedToSourceInstitution"),
    
    /**
     * Returned to participant.
     */
    RETURNED_TO_PARTICIPANT("specimenStatus.returnedToParticipant"),
    
    /**
     * Destroyed.
     */
    DESTROYED("specimenStatus.destroyed");
    
    private static final Map<SpecimenStatus, List<SpecimenStatus>> TRANSITIONS;
    static {
        TRANSITIONS = new HashMap<SpecimenStatus, List<SpecimenStatus>>();
        List<SpecimenStatus> t = new ArrayList<SpecimenStatus>();
        t.add(AVAILABLE);
        t.add(UNAVAILABLE);
        t.add(UNDER_REVIEW);
        t.add(PENDING_SHIPMENT);
        t.add(RETURNED_TO_PARTICIPANT);
        t.add(DESTROYED);
        TRANSITIONS.put(AVAILABLE, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(UNDER_REVIEW);
        t.add(UNAVAILABLE);
        t.add(AVAILABLE);
        t.add(PENDING_SHIPMENT);
        t.add(RETURNED_TO_PARTICIPANT);
        t.add(DESTROYED);
        TRANSITIONS.put(UNDER_REVIEW, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(PENDING_SHIPMENT);
        t.add(UNAVAILABLE);
        t.add(AVAILABLE);
        t.add(SHIPPED);
        t.add(RETURNED_TO_PARTICIPANT);
        t.add(DESTROYED);
        TRANSITIONS.put(PENDING_SHIPMENT, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(UNAVAILABLE);
        t.add(AVAILABLE);
        t.add(RETURNED_TO_PARTICIPANT);
        t.add(DESTROYED);
        TRANSITIONS.put(UNAVAILABLE, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(SHIPPED);
        t.add(RETURNED_TO_SOURCE_INSTITUTION);
        t.add(DESTROYED);
        TRANSITIONS.put(SHIPPED, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(RETURNED_TO_PARTICIPANT);
        TRANSITIONS.put(RETURNED_TO_PARTICIPANT, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(RETURNED_TO_SOURCE_INSTITUTION);
        t.add(RETURNED_TO_PARTICIPANT);
        t.add(DESTROYED);
        TRANSITIONS.put(RETURNED_TO_SOURCE_INSTITUTION, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(DESTROYED);
        TRANSITIONS.put(DESTROYED, t);
    }
    
    private static final Map<SpecimenStatus, List<SpecimenStatus>> USER_ASSIGNABLE_TRANSITIONS;
    static {
        USER_ASSIGNABLE_TRANSITIONS = new HashMap<SpecimenStatus, List<SpecimenStatus>>();
        List<SpecimenStatus> t = new ArrayList<SpecimenStatus>();
        t.add(AVAILABLE);
        t.add(UNAVAILABLE);
        t.add(RETURNED_TO_PARTICIPANT);
        t.add(DESTROYED);
        USER_ASSIGNABLE_TRANSITIONS.put(AVAILABLE, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(UNDER_REVIEW);
        t.add(UNAVAILABLE);
        t.add(RETURNED_TO_PARTICIPANT);
        t.add(DESTROYED);
        USER_ASSIGNABLE_TRANSITIONS.put(UNDER_REVIEW, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(PENDING_SHIPMENT);
        t.add(UNAVAILABLE);
        t.add(RETURNED_TO_PARTICIPANT);
        t.add(DESTROYED);
        USER_ASSIGNABLE_TRANSITIONS.put(PENDING_SHIPMENT, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(UNAVAILABLE);
        t.add(AVAILABLE);
        t.add(RETURNED_TO_PARTICIPANT);
        t.add(DESTROYED);
        USER_ASSIGNABLE_TRANSITIONS.put(UNAVAILABLE, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(SHIPPED);
        t.add(RETURNED_TO_SOURCE_INSTITUTION);
        t.add(DESTROYED);
        USER_ASSIGNABLE_TRANSITIONS.put(SHIPPED, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(RETURNED_TO_SOURCE_INSTITUTION);
        t.add(RETURNED_TO_PARTICIPANT);
        t.add(DESTROYED);
        USER_ASSIGNABLE_TRANSITIONS.put(RETURNED_TO_SOURCE_INSTITUTION, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(RETURNED_TO_PARTICIPANT);
        USER_ASSIGNABLE_TRANSITIONS.put(RETURNED_TO_PARTICIPANT, t);
        
        t = new ArrayList<SpecimenStatus>();
        t.add(DESTROYED);
        USER_ASSIGNABLE_TRANSITIONS.put(DESTROYED, t);
    }
    
    private static final List<SpecimenStatus> CONSENT_WITHDRAWN_CONSISTENT_STATUSES
        = Arrays.asList(new SpecimenStatus[]{UNAVAILABLE, RETURNED_TO_PARTICIPANT, RETURNED_TO_SOURCE_INSTITUTION, 
                SHIPPED, DESTROYED});
    
    private String resourceKey;
    
    
    /**
     * Constructor.
     * @param resourceKey the key of the human readable text
     */
    private SpecimenStatus(String resourceKey) {
        this.resourceKey = resourceKey;
    }
    
    /**
     * @return the resourceKey
     */
    public String getResourceKey() {
        return resourceKey;
    }

    /**
     * @return the allowedTransitions
     */
    public List<SpecimenStatus> getAllowedTransitions() {
        return TRANSITIONS.get(this);
    }
    
    /**
     * @return the allowedTransitions
     */
    public List<SpecimenStatus> geUserAssignableTransitions() {
        return USER_ASSIGNABLE_TRANSITIONS.get(this);
    }
    
    /**
     * @return whether this status is consistent with withdrawn consent.
     */
    public boolean isConsentWithdrawnConsistent() {
        return CONSENT_WITHDRAWN_CONSISTENT_STATUSES.contains(this);
    }
}