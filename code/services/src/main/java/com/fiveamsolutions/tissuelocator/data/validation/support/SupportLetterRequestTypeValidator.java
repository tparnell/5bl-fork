/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.validation.support;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.validator.AbstractMultipleCriteriaValidator;
import com.fiveamsolutions.nci.commons.validator.ValidationError;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterType;

/**
 * Validates fields related to a support letter request's type.
 * @author ddasgupta
 *
 */
public class SupportLetterRequestTypeValidator
    extends AbstractMultipleCriteriaValidator<SupportLetterRequestStatus> {

    private static final long serialVersionUID = 5173417618334099643L;
    private static final String GRANT_TYPE_FIELD_NAME = "grantType";
    private static final String GRANT_TYPE_MESSAGE_KEY = "{validator.supportLetterRequest.grantType.required}";
    private static final String FUNDING_SOURCES_FIELD_NAME = "fundingSources";
    private static final String FUNDING_SOURCES_MESSAGE_KEY =
        "{validator.supportLetterRequest.fundingSources.required}";
    private static final String OTHER_TYPE_FIELD_NAME = "otherType";
    private static final String OTHER_TYPE_MESSAGE_KEY = "{validator.supportLetterRequest.otherType.required}";

    /**
     * {@inheritDoc}
     */
    @Override
    protected Collection<ValidationError> validateObject(Object value) {
        Collection<ValidationError> errors = new ArrayList<ValidationError>();
        if (value instanceof SupportLetterRequest) {
            SupportLetterRequest request = (SupportLetterRequest) value;
            validateGrantType(request, errors);
            validateOtherType(request, errors);
        }

        return errors;
    }

    private void validateGrantType(SupportLetterRequest request, Collection<ValidationError> errors) {
        if (SupportLetterType.GRANT.equals(request.getType())) {
            if (StringUtils.isBlank(request.getGrantType())) {
                ValidationError ve = new ValidationError(GRANT_TYPE_FIELD_NAME, GRANT_TYPE_MESSAGE_KEY);
                errors.add(ve);
            }
            if (request.getFundingSources() == null || request.getFundingSources().isEmpty()) {
                ValidationError ve = new ValidationError(FUNDING_SOURCES_FIELD_NAME, FUNDING_SOURCES_MESSAGE_KEY);
                errors.add(ve);
            }
        }
    }

    private void validateOtherType(SupportLetterRequest request, Collection<ValidationError> errors) {
        if (SupportLetterType.OTHER.equals(request.getType())
                && StringUtils.isBlank(request.getOtherType())) {
            ValidationError ve = new ValidationError(OTHER_TYPE_FIELD_NAME, OTHER_TYPE_MESSAGE_KEY);
            errors.add(ve);
        }
    }
}
