/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.setting;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.fiveamsolutions.tissuelocator.data.ApplicationSetting;
import com.fiveamsolutions.tissuelocator.data.FundingStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author ddasgupta
 */
@Stateless
@SuppressWarnings("PMD.TooManyMethods")
public class ApplicationSettingServiceBean extends GenericServiceBean<ApplicationSetting>
    implements ApplicationSettingServiceLocal {

    private static final Logger LOG = Logger.getLogger(ApplicationSettingServiceBean.class);

    private static final int START_TIME_PROP_LENGTH = 4;
    private static final String NUMBER_REVIEWERS_KEY = "number_reviewers";
    private static final String GRACE_PERIOD_KEY = "shipment_receipt_grace_period";
    private static final String LINE_ITEM_REVIEW_GRACE_PERIOD_KEY = "institutional_review_grace_period";
    private static final String LETTER_REQUEST_REVIEW_GRACE_PERIOD_KEY = "support_letter_request_review_grace_period";
    private static final String QUESTION_RESPONSE_GRACE_PERIOD_KEY = "question_response_grace_period";
    private static final String SCHEDULED_REMINDER_THREAD_START_TIME = "scheduled_reminder_thread_start_time";
    private static final String DISPLAY_PI_LEGAL_FIELDS = "display_pi_legal_fields";
    private static final String DISPLAY_PROSPECTIVE_COLLECTION = "display_prospective_collection";
    private static final String DISPLAY_CONSORTIUM_REGISTRATION = "display_consortium_registration";
    private static final String SIMPLE_SEARCH_ENABLED = "simple_search_enabled";
    private static final String CENTER_BROWSE_WIDGET = "center_browse_widget";
    private static final String LEFT_BROWSE_WIDGET = "left_browse_widget";
    private static final String RIGHT_BROWSE_WIDGET = "right_browse_widget";
    private static final String CENTER_HOME_WIDGET = "center_home_widget";
    private static final String LEFT_HOME_WIDGET = "left_home_widget";
    private static final String RIGHT_HOME_WIDGET = "right_home_widget";
    private static final String VOTING_TASK_ENABLED = "voting_task_enabled";
    private static final String SPECIMEN_GROUPING = "specimen_grouping";
    private static final String AGGREGATE_CART_CRITERIA = "general_population_sort_regex";
    private static final String DISPLAY_REQUEST_REVIEW_VOTES = "display_request_review_votes";
    private static final String REVIEW_PROCESS_KEY = "review_process";
    private static final String ACCOUNT_APPROVAL_ACTIVE_KEY = "account_approval_active";
    private static final String MTA_CERTIFICATION_REQUIRED_KEY = "mta_certification_required";
    private static final String NEW_REQUEST_COUNT_CONDITION_KEY = "new_request_count_condition";
    private static final String MINIMUM_REQUIRED_FUNDING_STATUS_KEY = "minimum_required_funding_status";
    private static final String IRB_APPROVAL_REQUIRED_KEY = "irb_approval_required";
    private static final String MINIMUM_AGGREGATE_RESULTS_DISPLAYED = "minimum_aggregate_results_displayed";
    private static final String TABBED_SEARCH_AND_SECTIONED_CATEGORIES_VIEWS_ENABLED =
        "tabbed_search_and_sectioned_categories_views_enabled";
    private static final String DISPLAY_COLLECTION_PROTOCOL = "display_collection_protocol";
    private static final String SPECIMEN_DETAILS_PANEL_COLUMN1_LABEL = "specimen_details_panel_column1_label";
    private static final String SPECIMEN_DETAILS_PANEL_COLUMN1_VALUE = "specimen_details_panel_column1_value";
    private static final String SPECIMEN_DETAILS_PANEL_COLUMN2_LABEL = "specimen_details_panel_column2_label";
    private static final String SPECIMEN_DETAILS_PANEL_COLUMN2_VALUE = "specimen_details_panel_column2_value";
    private static final String SPECIMEN_DETAILS_PANEL_COLUMN3_LABEL = "specimen_details_panel_column3_label";
    private static final String SPECIMEN_DETAILS_PANEL_COLUMN3_VALUE = "specimen_details_panel_column3_value";
    private static final String SPECIMEN_DETAILS_PANEL_COLUMN4_LABEL = "specimen_details_panel_column4_label";
    private static final String SPECIMEN_DETAILS_PANEL_COLUMN4_VALUE = "specimen_details_panel_column4_value";
    private static final String DISPLAY_USAGE_RESTRICTIONS = "display_usage_restrictions";
    private static final String PROTOCOL_DOCUMENT_REQUIRED = "protocol_document_required";
    private static final String HOME_PAGE_DATA_CACHE_REFRESH_INTERVAL = "home_page_data_cache_refresh_interval";
    private static final String DISPLAY_REQUEST_PROCESS_STEPS = "display_request_process_steps";
    private static final String DISPLAY_FUNDING_STATUS_PENDING = "display_funding_status_pending";
    private static final String DISPLAY_SHIPMENT_BILLING_ADDRESS = "display_shipment_billing_recipient";
    private static final String USER_RESUME_REQUIRED = "user_resume_required";

    private ApplicationSetting getByName(String name) {
        String queryString = "from " + ApplicationSetting.class.getName() + " where name = :name";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setString("name", name);
        return (ApplicationSetting) q.uniqueResult();
    }

    @SuppressWarnings("deprecation")
    private ApplicationSetting getInNewSession(String name) {
        // This method should be used when a setting must be retrieved
        // without flushing other objects in an existing session.
        Session s = null;
        try {
            s = TissueLocatorHibernateUtil.getHibernateHelper()
                    .getSessionFactory().openSession();

            String queryString = "from " + ApplicationSetting.class.getName() + " where name = :name";
            Query q = s.createQuery(queryString);
            q.setString("name", name);
            return (ApplicationSetting) q.uniqueResult();
        } finally {
            Connection c = s.connection();
            s.close();
            try {
                c.close();
            } catch (SQLException e) {
                LOG.error("error closing connection", e);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNumberOfReviewers() {
        return Integer.parseInt(getByName(NUMBER_REVIEWERS_KEY).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date getScheduledReminderThreadStartTime() {
        String startTime = getByName(SCHEDULED_REMINDER_THREAD_START_TIME).getValue();
        if (startTime.length() != START_TIME_PROP_LENGTH) {
            return null;
        }

        Calendar startDate = Calendar.getInstance();
        startDate.set(Calendar.HOUR_OF_DAY, Integer.parseInt(startTime.substring(0, 2)));
        startDate.set(Calendar.MINUTE, Integer.parseInt(startTime.substring(2)));
        startDate.set(Calendar.SECOND, 0);
        if (startDate.before(Calendar.getInstance())) {
            startDate.add(Calendar.DAY_OF_YEAR, 1);
        }
        return startDate.getTime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getShipmentReceiptGracePeriod() {
        return Integer.parseInt(getByName(GRACE_PERIOD_KEY).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getLineItemReviewGracePeriod() {
        return Integer.parseInt(getByName(LINE_ITEM_REVIEW_GRACE_PERIOD_KEY).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSupportLetterRequestReviewGracePeriod() {
        return Integer.parseInt(getByName(LETTER_REQUEST_REVIEW_GRACE_PERIOD_KEY).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getQuestionResponseGracePeriod() {
        return Integer.parseInt(getByName(QUESTION_RESPONSE_GRACE_PERIOD_KEY).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayPiLegalFields() {
        return Boolean.valueOf(getByName(DISPLAY_PI_LEGAL_FIELDS).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayProspectiveCollection() {
        return Boolean.valueOf(getByName(DISPLAY_PROSPECTIVE_COLLECTION).getValue());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayConsortiumRegistration() {
        return Boolean.valueOf(getByName(DISPLAY_CONSORTIUM_REGISTRATION).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSimpleSearchEnabled() {
        return Boolean.valueOf(getByName(SIMPLE_SEARCH_ENABLED).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTabbedSearchAndSectionedCategoriesViewsEnabled() {
        return Boolean.valueOf(getByName(TABBED_SEARCH_AND_SECTIONED_CATEGORIES_VIEWS_ENABLED).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCenterBrowseWidget() {
        return getByName(CENTER_BROWSE_WIDGET).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLeftBrowseWidget() {
        return  getByName(LEFT_BROWSE_WIDGET).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRightBrowseWidget() {
        return getByName(RIGHT_BROWSE_WIDGET).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCenterHomeWidget() {
        return getByName(CENTER_HOME_WIDGET).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLeftHomeWidget() {
        return  getByName(LEFT_HOME_WIDGET).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRightHomeWidget() {
        return getByName(RIGHT_HOME_WIDGET).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVotingTaskEnabled() {
        return Boolean.valueOf(getByName(VOTING_TASK_ENABLED).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenGrouping() {
        return getByName(SPECIMEN_GROUPING).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getGeneralPopulationSortRegex() {
        return getByName(AGGREGATE_CART_CRITERIA).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayRequestReviewVotes() {
        return Boolean.valueOf(getByName(DISPLAY_REQUEST_REVIEW_VOTES).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReviewProcess getReviewProcess() {
        return ReviewProcess.valueOf(getByName(REVIEW_PROCESS_KEY).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAccountApprovalActive() {
        return Boolean.valueOf(getByName(ACCOUNT_APPROVAL_ACTIVE_KEY).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMtaCertificationRequired() {
        return Boolean.valueOf(getInNewSession(MTA_CERTIFICATION_REQUIRED_KEY).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getNewRequestCountCondition() {
        return getByName(NEW_REQUEST_COUNT_CONDITION_KEY).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FundingStatus getMinimunRequiredFundingStatus() {
        return FundingStatus.valueOf(getInNewSession(MINIMUM_REQUIRED_FUNDING_STATUS_KEY).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isIrbApprovalRequired() {
        return Boolean.valueOf(getInNewSession(IRB_APPROVAL_REQUIRED_KEY).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getMinimumAggregateResultsDisplayed() {
        return Integer.parseInt(getByName(MINIMUM_AGGREGATE_RESULTS_DISPLAYED).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn1Label() {
        return getByName(SPECIMEN_DETAILS_PANEL_COLUMN1_LABEL).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn1Value() {
        return getByName(SPECIMEN_DETAILS_PANEL_COLUMN1_VALUE).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn2Label() {
        return getByName(SPECIMEN_DETAILS_PANEL_COLUMN2_LABEL).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn2Value() {
        return getByName(SPECIMEN_DETAILS_PANEL_COLUMN2_VALUE).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn3Label() {
        return getByName(SPECIMEN_DETAILS_PANEL_COLUMN3_LABEL).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn3Value() {
        return getByName(SPECIMEN_DETAILS_PANEL_COLUMN3_VALUE).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn4Label() {
        return getByName(SPECIMEN_DETAILS_PANEL_COLUMN4_LABEL).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn4Value() {
        return getByName(SPECIMEN_DETAILS_PANEL_COLUMN4_VALUE).getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayCollectionProtocol() {
        return Boolean.valueOf(getByName(DISPLAY_COLLECTION_PROTOCOL).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayUsageRestrictions() {
        return Boolean.valueOf(getByName(DISPLAY_USAGE_RESTRICTIONS).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isProtocolDocumentRequired() {
        return Boolean.valueOf(getInNewSession(PROTOCOL_DOCUMENT_REQUIRED).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getHomePageDataCacheRefreshInterval() {
        return Integer.parseInt(getByName(HOME_PAGE_DATA_CACHE_REFRESH_INTERVAL).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayRequestProcessSteps() {
        return Boolean.valueOf(getByName(DISPLAY_REQUEST_PROCESS_STEPS).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayFundingStatusPending() {
        return Boolean.valueOf(getByName(DISPLAY_FUNDING_STATUS_PENDING).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayShipmentBillingRecipient() {
        return Boolean.valueOf(getByName(DISPLAY_SHIPMENT_BILLING_ADDRESS).getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUserResumeRequired() {
        return Boolean.valueOf(getInNewSession(USER_RESUME_REQUIRED).getValue());
    }
}
