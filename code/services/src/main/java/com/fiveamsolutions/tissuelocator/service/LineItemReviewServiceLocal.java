/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import javax.ejb.Local;
import javax.mail.MessagingException;

import com.fiveamsolutions.tissuelocator.data.ReviewComment;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;

/**
 * @author ddasgupta
 */
@Local
public interface LineItemReviewServiceLocal extends SpecimenRequestServiceLocal {

    /**
     * Save a request with new line item reviews, create a shipment, and send
     * emails as appropriate. This method functions under the assumption that the
     * review process consists only of line item reviews and not institutional or
     * scientific reviews.
     * @param request The request to be saved.
     * @throws MessagingException on error sending emails.
     */
    void addLineItemReviews(SpecimenRequest request) throws MessagingException;

    /**
     * Add an internal comment and send emails to all interested users.
     * It is this method's responsibility to add the comment to the
     * list of associated comments.
     * @param request The request to which the comment is related.
     * @param comment The comment to be added.
     * @throws MessagingException on error sending emails.
     */
    void addReviewComment(SpecimenRequest request, ReviewComment comment) throws MessagingException;

}
