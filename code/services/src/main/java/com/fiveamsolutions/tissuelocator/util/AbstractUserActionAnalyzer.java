/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.util;

import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;

/**
 * @author ddasgupta
 *
 */
@SuppressWarnings("PMD.EmptyMethodInAbstractClassShouldBeAbstract")
public abstract class AbstractUserActionAnalyzer implements UserActionAnalyzer {

    /**
     * {@inheritDoc}
     */
    public boolean needsConsortiumReview(TissueLocatorUser user, SpecimenRequest request) {
        if (!RequestStatus.PENDING.equals(request.getStatus())) {
            return false;
        }
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null && vote.getUser().equals(user)) {
                return vote.getVote() == null;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public boolean needsFinalComment(TissueLocatorUser user, SpecimenRequest request) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public boolean needsInstitutionalReview(TissueLocatorUser user, SpecimenRequest request) {
        if (!RequestStatus.PENDING.equals(request.getStatus())) {
            return false;
        }
        for (SpecimenRequestReviewVote vote : request.getInstitutionalReviews()) {
            if (vote.getInstitution().equals(user.getInstitution())) {
                return vote.getVote() == null;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public boolean needsLineItemReview(TissueLocatorUser user, SpecimenRequest request) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public boolean needsLeadReview(TissueLocatorUser user, SpecimenRequest request) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public boolean needsResearcherNotification(TissueLocatorUser user, SpecimenRequest request) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public boolean needsReviewerAssignment(TissueLocatorUser user, SpecimenRequest request) {
        return false;
    }
}
