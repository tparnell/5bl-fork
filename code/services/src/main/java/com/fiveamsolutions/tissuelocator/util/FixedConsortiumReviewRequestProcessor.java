/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import org.apache.log4j.Logger;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.google.inject.Inject;

/**
 * Implementation of the request processor interface to support the fixed consortium review process.
 * @author ddasgupta
 */
public class FixedConsortiumReviewRequestProcessor implements RequestProcessor {

    private static final Logger LOG = Logger.getLogger(FixedConsortiumReviewRequestProcessor.class);
    private static final double PERCENT_MULTIPLIER = 100.0;

    private TissueLocatorUserServiceLocal userService;
    private ApplicationSettingServiceLocal applicationSettingService;

    /**
     * The following is the purpose of each email provided as an argument to this method:
     * emailBase: email to coordinator informing them of a new or revised request
     * leadReviewContent: email to chair instructing them to assign reviewers to a new request, or just notifying
     * them of a revised request
     * institutionalApprovalContent: nothing
     * revisionEmail: email to assigned reviewer informing them of a revised request
     * reviewerNotAssignedBase email to chair informing them to assign reviewers to a revised request.
     * {@inheritDoc}
     */
    @SuppressWarnings("PMD.ExcessiveParameterList")
    //CHECKSTYLE:OFF too many parameters
    public Collection<Email> initializeRequest(SpecimenRequest request, Email emailBase, Email leadReviewContent,
            Email institutionalApprovalContent, Email revisionEmail, Email reviewerNotAssignedBase,
            int numReviewers, boolean isNew) {
    //CHECKSTYLE:ON

        Collection<Email> emails = new ArrayList<Email>();
        if (isNew) {
            // on first save make sure all review objects are created with a null vote
            Institution inst = getUserService().getByUsername(UsernameHolder.getUser()).getInstitution();
            Set<AbstractUser> coordinators = getUserService().getUsersInRole(Role.REVIEW_DECISION_NOTIFIER.getName());
            if (!coordinators.isEmpty()) {
                inst = ((TissueLocatorUser) coordinators.iterator().next()).getInstitution();
            }

            for (int i = 0; i < getApplicationSettingService().getNumberOfReviewers(); i++) {
                SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
                vote.setInstitution(inst);
                request.getConsortiumReviews().add(vote);
            }

            emails.addAll(getRequestEmails(request, emailBase, leadReviewContent, null));
        } else {
            emails.addAll(getRevisionEmails(request, revisionEmail));
            emails.addAll(getRequestEmails(request, emailBase, reviewerNotAssignedBase, leadReviewContent));
        }
        return emails;
    }

    private Collection<Email> getRequestEmails(SpecimenRequest request, Email coordinatorEmail,
            Email chairAssignEmail, Email chairNotifyEmail) {
        Collection<Email> emails = new ArrayList<Email>();
        Set<AbstractUser> coordinators = getUserService().getUsersInRole(Role.REVIEW_DECISION_NOTIFIER.getName());
        for (AbstractUser coordinator : coordinators) {
            emails.add(getEmail(coordinatorEmail, coordinator.getEmail(), request));
        }

        boolean allAssigned = true;
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            allAssigned &= vote.getUser() != null;
        }
        Email chairEmailBase = chairAssignEmail;
        if (allAssigned) {
            chairEmailBase = chairNotifyEmail;
        }

        Set<AbstractUser> assigners = getUserService().getUsersInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName());
        for (AbstractUser assigner : assigners) {
            emails.add(getEmail(chairEmailBase, assigner.getEmail(), request));
        }

        return emails;
    }

    private Collection<Email> getRevisionEmails(SpecimenRequest request, Email revisionEmail) {
        Collection<Email> emails = new ArrayList<Email>();
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null) {
                emails.add(getEmail(revisionEmail, vote.getUser().getEmail(), request));
            }
        }
        return emails;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<Email> processPendingRequest(SpecimenRequest request, RequestProcessingConfiguration config) {
        Collection<Email> emails = new ArrayList<Email>();

        LOG.info("processing request " + request.getId());
        if (isVotingPeriodDone(request, config)) {
            if (hasEnoughVotes(request, config)) {
                emails.addAll(finishVote(request, config));
            } else {
                emails.addAll(emailMissingVotes(request, config));
            }
        }

        if (RequestStatus.PENDING.equals(request.getStatus())) {
            emails.addAll(analyzeRequestAssignmentStatus(request, config));
        }
        return emails;
    }

    private boolean isVotingPeriodDone(SpecimenRequest request, RequestProcessingConfiguration config) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        LOG.trace("update date: " + cal.getTime().toString());
        cal.add(Calendar.DATE, config.getVotingPeriod());
        Date votingPeriodClose = cal.getTime();
        LOG.trace("voting period close: " + votingPeriodClose.toString());
        return votingPeriodClose.before(new Date());
    }

    private boolean hasEnoughVotes(SpecimenRequest request, RequestProcessingConfiguration config) {
        int voteCount = 0;
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getVote() != null) {
                voteCount++;
            }
        }
        LOG.trace("number of submitted votes: " + voteCount);
        LOG.trace("total number of votes: " + request.getConsortiumReviews().size());
        LOG.trace("required percentage available votes: " + config.getMinPercentAvailableVotes());

        double percentAvailableVotes = PERCENT_MULTIPLIER
            * voteCount / request.getConsortiumReviews().size();
        return Boolean.valueOf(percentAvailableVotes >= config.getMinPercentAvailableVotes());
    }

    private Collection<Email> finishVote(SpecimenRequest request, RequestProcessingConfiguration config) {
        LOG.info("finishing vote for request " + request.getId());
        request.setStatus(RequestStatus.PENDING_FINAL_DECISION);
        request.setUpdatedDate(new Date());
        request.setExternalComment(null);
        return processPendingDecisionRequest(request, config);
    }

    private Collection<Email> emailMissingVotes(SpecimenRequest request, RequestProcessingConfiguration config) {
        Collection<Email> emails = new ArrayList<Email>();
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null && vote.getVote() == null) {
                emails.add(getEmail(config.getVoteLateEmail(), vote.getUser().getEmail(), request));
            }
        }
        return emails;
    }

    private Collection<Email> analyzeRequestAssignmentStatus(SpecimenRequest request,
            RequestProcessingConfiguration config) {
        Collection<Email> emails = new ArrayList<Email>();
        LOG.info("analyzing reviewer assignment status for request: " + request.getId());
        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, config.getReviewerAssignmentPeriod());
        if (cal.getTime().after(new Date())) {
            LOG.info("reviewer assignment period not complete for request " + request.getId());
            return emails;
        }

        boolean allAssigned = true;
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            allAssigned &= vote.getUser() != null && allAssigned;
        }
        LOG.info("all votes assigned? : " + allAssigned);
        if (!allAssigned) {
            Set<AbstractUser> reviewerAssigners =
                getUserService().getUsersInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName());
            for (AbstractUser reviewerAssigner : reviewerAssigners) {
                emails.add(getEmail(config.getReviewerAssignmentLateEmail(), reviewerAssigner.getEmail(), request));

            }
        }

        return emails;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<Email> processPendingDecisionRequest(SpecimenRequest request,
            RequestProcessingConfiguration config) {
        UserActionAnalyzer fixedConsortiumAnalyzer = ReviewProcess.FIXED_CONSORTIUM_REVIEW.getUserActionAnalyzer();
        Collection<Email> emails = new ArrayList<Email>();
        Set<AbstractUser> leadReviewers = getUserService().getUsersInRole(Role.LEAD_REVIEWER.getName());
        Set<AbstractUser> reviewDecisionNotifiers =
            getUserService().getUsersInRole(Role.REVIEW_DECISION_NOTIFIER.getName());
        for (AbstractUser leadReviewer : leadReviewers) {
            if (!fixedConsortiumAnalyzer.needsResearcherNotification(null, request)) {
                emails.add(getEmail(config.getVoteFinalizedEmail(), leadReviewer.getEmail(), request));
            }
        }

        for (AbstractUser notifier : reviewDecisionNotifiers) {
            if (fixedConsortiumAnalyzer.needsResearcherNotification(null, request)) {
                emails.add(getEmail(config.getRequestPendingReleaseEmail(), notifier.getEmail(), request));
            }
        }
        return emails;
    }

    /**
     * {@inheritDoc}
     */
    public void setInstitutionService(InstitutionServiceLocal institutionService) {
        // do nothing
    }

    /**
     * @return the userService
     */
    public TissueLocatorUserServiceLocal getUserService() {
        return userService;
    }

    /**
     * {@inheritDoc}
     */
    @Inject
    public void setUserService(TissueLocatorUserServiceLocal userService) {
        this.userService = userService;
    }

    /**
     * @return the applicationSettingService
     */
    public ApplicationSettingServiceLocal getApplicationSettingService() {
        return applicationSettingService;
    }

    /**
     * {@inheritDoc}
     */
    @Inject
    public void setApplicationSettingService(ApplicationSettingServiceLocal applicationSettingService) {
        this.applicationSettingService = applicationSettingService;
    }

    @SuppressWarnings({"PMD.ExcessiveParameterList" })
    private Email getEmail(Email template, String recipient, SpecimenRequest request) {
        Email email = new Email();
        email.setRecipient(recipient);
        email.setSubject(template.getSubject());
        email.setHtml(template.getHtml());
        email.setText(template.getText());
        email.setRequest(request);
        return email;
    }
}
