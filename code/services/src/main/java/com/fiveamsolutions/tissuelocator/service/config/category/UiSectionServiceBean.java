/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.config.category;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSection;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * User interface section service implementation.
 *
 * @author jstephens
 */
@Stateless
public class UiSectionServiceBean extends GenericServiceBean<UiSection> implements UiSectionServiceLocal {

    private static final ConcurrentHashMap<String, List<UiSection>>
        UI_SECTIONS_BY_ENTITY_CLASS_NAME = new ConcurrentHashMap<String, List<UiSection>>();

    /**
     * Gets the user interface sections associated with an entity.
     *
     * @param entityClass entity to which the sections belong
     * @return user interface sections associated with an entity
     */
    public List<UiSection> getUiSections(Class<? extends PersistentObject> entityClass) {
        List<UiSection> uiSections = UI_SECTIONS_BY_ENTITY_CLASS_NAME.get(entityClass.getName());
        if (uiSections == null) {
            uiSections = retrieveUiSectionsFromDatabase(entityClass);
            UI_SECTIONS_BY_ENTITY_CLASS_NAME.putIfAbsent(entityClass.getName(), uiSections);
        }

        return uiSections;
    }

    @SuppressWarnings("unchecked")
    private List<UiSection> retrieveUiSectionsFromDatabase(Class<? extends PersistentObject> entityClass) {
        Criteria criteria = TissueLocatorHibernateUtil.getCurrentSession().createCriteria(UiSection.class);
        criteria.add(Restrictions.ilike("entityClassName", entityClass.getName()));
        criteria.addOrder(Order.asc("id"));
        return criteria.list();
    }

}
