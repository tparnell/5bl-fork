INSERT INTO specimen_type (id,active, name, specimen_class) VALUES (nextval('hibernate_sequence'), true, 'testType1', 'CELLS');
insert into participant (id, ethnicity, externalid, gender, external_id_assigner_id) select nextval('hibernate_sequence'), 'HISPANIC_OR_LATINO', 'test male participant from ' || name, 'MALE', id from institution;
insert into participant (id, ethnicity, externalid, gender, external_id_assigner_id) select nextval('hibernate_sequence'), 'NON_HISPANIC_OR_LATINO', 'test female participant from ' || name, 'FEMALE', id from institution;
insert into participant (id, ethnicity, externalid, gender, external_id_assigner_id) select nextval('hibernate_sequence'), 'NOT_REPORTED', 'test unspecified participant from ' || name, 'UNSPECIFIED', id from institution;
insert into participant_races (participant_id, element) select id, 1 from participant where lower(externalid) like '%test%';
insert into participant_races (participant_id, element) select id, 2 from participant where lower(externalid) like '%test%';
insert into participant_races (participant_id, element) select id, 3 from participant where lower(externalid) like '%test%';
insert into collection_protocol (id, name, institution_id) select nextval('hibernate_sequence'), 'test cp from ' || name, id from institution i where i.id = (select min(id) from institution where consortiumMember = true) or i.id not in (select institution_id from collection_protocol);
insert into collection_protocol (id, name, institution_id) select nextval('hibernate_sequence'), 'test cp2 from ' || name, id from institution i where i.id = (select min(id) from institution where consortiumMember = true);
