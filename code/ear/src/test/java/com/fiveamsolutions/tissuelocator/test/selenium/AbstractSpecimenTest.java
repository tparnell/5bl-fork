/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;

/**
 * @author ddasgupta
 *
 */
public abstract class AbstractSpecimenTest extends AbstractTissueLocatorSeleniumTest {

    /**
     * Delay.
     */
    protected static final int DELAY = 500;

    /**
     * Title.
     */
    protected static final String TITLE = ClientProperties.getBiospecimenTerm() + " Administration";

    /**
     * Xpath to specific elements on the view page.
     */
    protected static final String VIEW_PAGE_XPATH = "xpath=" + ClientProperties.getViewPageXPath();

    /**
     * Xpath to status history values.
     */
    protected static final String STATUS_HISTORY_XPATH = "xpath=" + ClientProperties.getStatusHistoryXPath();

    /**
     * Price error message.
     */
    protected static final String PRICE_ERROR_MESSAGE = "If the specimen price is not negotiable, "
        + "minimum price and maximum price are required.";
    private static final String CUSTOM_PROPERTY_DEFAULT_UNAVAILABLE_TEXT = "Unknown";

    private String expectedDetailsPanelColumn1Label;
    private String expectedDetailsPanelColumn1Value;
    private String expectedDetailsPanelColumn2Label;
    private String expectedDetailsPanelColumn2Value;
    private String expectedDetailsPanelColumn3Label;
    private String expectedDetailsPanelColumn3Value;
    private String expectedDetailsPanelColumn4Label;
    private String expectedDetailsPanelColumn4Value;
    private int specimenIdIndex = 0;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "SpecimenTest.sql";
    }

    /**
     * Tests specimen management.
     */
    @Test
    public void testSpecimenManagement() {
        goToSpecimenPage();
        requiredValidation();
        formatValidation();
        minMaxValidation();
        priceValidation();
        create(true, ClientProperties.getDefaultConsortiumMemberName());
        duplicateValidation();
        viewPage();
        edit();
        emptyExtensions();
        verifyStatusHistory();
        verifySinglePrice();
        clickAndWait("link=Sign Out");
        defaultProtocol();
        exerciseNormalSampleCheckbox();
    }

    /**
     * Go to Specimen page.
     */
    protected void goToSpecimenPage() {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");
        clickAndWait("link=" + ClientProperties.getBiospecimenAdministrationLink());
        assertTrue(selenium.isTextPresent(TITLE));
        clickAndWait("link=Add New " + ClientProperties.getBiospecimenTerm());
        assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " Details"));
    }

    /**
     * Tests required validation.
     */
    protected void requiredValidation() {
        selenium.select("institution", "label=" + ClientProperties.getDefaultConsortiumMemberName());
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("External ID (Record ID) must be set"));
        assertTrue(selenium.isTextPresent("You must select a valid protocol."));
        assertTrue(selenium.isTextPresent("You must select a valid " 
                + ClientProperties.getParticipantTerm().toLowerCase() + "."));
        assertTrue(selenium.isTextPresent("Specimen, participant, and protocol institutions are not consistent."));
        assertTrue(selenium.isTextPresent("You must select either a valid "
                + ClientProperties.getDiseaseTerm().toLowerCase() + " or " + ClientProperties.getNormalTerm() + "."));
        requiredExtensionValidation();

        selenium.type("availableQuantity", "1.5");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        String bothMessage = "Both the " + ClientProperties.getQuantityTerm().toLowerCase()
            + " and the units must be set.";
        assertTrue(selenium.isTextPresent(bothMessage));

        selenium.type("availableQuantity", "");
        selenium.select("availableQuantityUnits", "label=milligrams");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(bothMessage));
        assertFalse(selenium.isTextPresent("Consent Withdrawn"));
        assertFalse(selenium.isTextPresent("Consent Withdrawn Date"));
    }

    /**
     * Tests required validation of extensions. Subclasses should override to verify client-specific dynamic extensions.
     */
    protected void requiredExtensionValidation() {
    }

    /**
     * Tests format validaiton.
     */
    protected void formatValidation() {
        String invalidMessage = ClientProperties.getQuantityTerm() + " must be a number greater than 0.";
        selenium.type("age", "invalid");
        selenium.type("availableQuantity", "invalid");
        if (ClientProperties.isDisplayPrices()) {
            selenium.type("minimumPrice", "invalid");
            selenium.type("maximumPrice", "invalid");
        }
        selenium.typeKeys("dom=document.forms[0].elements[3]", "invalid ");
        extensionFormatValidationEntry();
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Patients Age at Collection must be a whole number."));
        assertTrue(selenium.isTextPresent(invalidMessage));
        if (ClientProperties.isDisplayPrices()) {
            assertTrue(selenium.isTextPresent("Minimum Price must be a number greater than 0."));
            assertTrue(selenium.isTextPresent("Maximum Price must be a number greater than 0."));
        }
        assertTrue(selenium.isTextPresent("You must select either a valid "
                + ClientProperties.getDiseaseTerm().toLowerCase() + " or " + ClientProperties.getNormalTerm() + "."));
        assertEquals("invalid", selenium.getValue("dom=document.forms[0].elements[3]"));

        if (ClientProperties.isDisplayPrices()) {
            selenium.type("minimumPrice", "100.0987");
            selenium.type("maximumPrice", "100.0987");
            clickAndWait("btn_save");
            assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
            assertTrue(selenium.isTextPresent("Minimum Price should contain at most 2 decimal digits"));
            assertTrue(selenium.isTextPresent("Maximum Price should contain at most 2 decimal digits"));
        }

        selenium.type("availableQuantity", "54321");
        assertEquals("54321", selenium.getValue("availableQuantity"));
        clickAndWait("btn_save");
        assertFalse(selenium.isTextPresent(invalidMessage));
        selenium.type("availableQuantity", "54,321");
        clickAndWait("btn_save");
        assertEquals("54321", selenium.getValue("availableQuantity"));
        assertFalse(selenium.isTextPresent(invalidMessage));
        selenium.type("availableQuantity", "123.45");
        clickAndWait("btn_save");
        assertEquals("123.45", selenium.getValue("availableQuantity"));
        assertFalse(selenium.isTextPresent(invalidMessage));
        selenium.type("availableQuantity", "1,234.56");
        clickAndWait("btn_save");
        assertEquals("1234.56", selenium.getValue("availableQuantity"));
        assertFalse(selenium.isTextPresent(invalidMessage));
        extensionFormatValidationMessage();
    }

    /**
     * Tests format validation of extensions. Subclasses should override to verify client-specific dynamic extensions.
     */
    protected void extensionFormatValidationEntry() {

    }

    /**
     * Tests format validation of extensions. Subclasses should override to verify client-specific dynamic extensions.
     */
    protected void extensionFormatValidationMessage() {

    }

    /**
     * Tests min/max validation.
     */
    protected void minMaxValidation() {
        selenium.type("age", "-20");
        selenium.type("availableQuantity", "-20.0");
        if (ClientProperties.isDisplayPrices()) {
            selenium.type("minimumPrice", "-20.0");
            selenium.type("maximumPrice", "-20.0");
        }
        extensionMinMaxValidationEntry();
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Patients Age at Collection must be greater than or equal to 0"));
        assertTrue(selenium.isTextPresent(ClientProperties.getQuantityTerm() + " must be greater than or equal to 0"));
        if (ClientProperties.isDisplayPrices()) {
            assertTrue(selenium.isTextPresent("Maximum Price must be greater than or equal to 0"));
            assertTrue(selenium.isTextPresent("Minimum Price must be greater than or equal to 0"));
        }
        extensionMinMaxValidationMessage();
    }

    /**
     * Tests min/max validation of extensions. Subclasses should override to verify client-specific dynamic extensions.
     */
    protected void extensionMinMaxValidationEntry() {

    }

    /**
     * Tests min/max validation of extensions. Subclasses should override to verify client-specific dynamic extensions.
     */
    protected void extensionMinMaxValidationMessage() {

    }

    /**
     * Tests price validation.
     */
    protected void priceValidation() {
        if (ClientProperties.isDisplayPrices()) {
            selenium.type("minimumPrice", "20.0");
            selenium.type("maximumPrice", "10.0");
            clickAndWait("btn_save");
            assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
            assertTrue(selenium.isTextPresent("Maximum Price is not greater than or equal to Minimum Price."));
            selenium.type("minimumPrice", "");
            selenium.type("maximumPrice", "10.0");
            clickAndWait("btn_save");
            assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
            assertTrue(selenium.isTextPresent("Maximum Price is not greater than or equal to Minimum Price."));
            selenium.type("minimumPrice", "10.0");
            selenium.type("maximumPrice", "20.0");
            selenium.check("priceNegotiable");
            selenium.fireEvent("priceNegotiable", "click");
            pause(DELAY);
            assertFalse(selenium.isEditable("minimumPrice"));
            assertTrue(StringUtils.isBlank(selenium.getValue("minimumPrice")));
            assertFalse(selenium.isEditable("maximumPrice"));
            assertTrue(StringUtils.isBlank(selenium.getValue("maximumPrice")));
            selenium.uncheck("priceNegotiable");
            selenium.fireEvent("priceNegotiable", "click");
            pause(DELAY);
            assertTrue(selenium.isEditable("minimumPrice"));
            assertTrue(StringUtils.isBlank(selenium.getValue("minimumPrice")));
            assertTrue(selenium.isEditable("maximumPrice"));
            assertTrue(StringUtils.isBlank(selenium.getValue("maximumPrice")));
            clickAndWait("btn_save");
            assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
            assertTrue(selenium.isTextPresent(PRICE_ERROR_MESSAGE));
            selenium.type("minimumPrice", "10.0");
            clickAndWait("btn_save");
            assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
            assertTrue(selenium.isTextPresent(PRICE_ERROR_MESSAGE));
            selenium.type("minimumPrice", "");
            selenium.type("maximumPrice", "20.0");
            clickAndWait("btn_save");
            assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
            assertTrue(selenium.isTextPresent(PRICE_ERROR_MESSAGE));
        }
    }

    /**
     * Creates a specimen.
     * @param enterProtocol whether protocol must be entered manually
     * @param institutionName institution name
     */
    protected void create(boolean enterProtocol, String institutionName) {
        enterData(getNewSpecimenId(), enterProtocol, institutionName);
        assertTrue(selenium.isTextPresent("Specimen successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(TITLE));
        if (ClientProperties.getFluidSpecimenCount() > 0) {
            showOnlyTestResults();
        }
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent(getCurrentSpecimenId()));
        assertFalse(selenium.isTextPresent("Call"));
    }

    /**
     * Enter specimen data.
     * @param specimenId specimen external id.
     * @param enterProtocol whether protocol must be entered manually
     * @param institutionName institution name
     */
    protected void enterData(String specimenId, boolean enterProtocol, String institutionName) {
        enterDataNoExtensions(specimenId, enterProtocol, institutionName, false);
        enterExtensionData();
        setUpExpectedDetailsPanelColumns();
        clickAndWait("btn_save");
    }

    private void enterDataNoExtensions(String specimenId, boolean enterProtocol, String institutionName,
            boolean useNormalDisease) {
        if (enterProtocol) {
            selenium.select("institution", "label=" + institutionName);
            if (!isCollectionProtocolDisplayed()) {
                pause(DELAY);
            }
            selenium.fireEvent("institution", "change");
            if (!isCollectionProtocolDisplayed()) {
                waitForText("Collection Protocol");
            }
        }
        verifyTimeUnitOptions("ageUnits");
        selenium.type("age", "50");
        selenium.select("ageUnits", "label=months");
        selenium.type("externalId", specimenId);
        selenium.select("specimenType", "label=testType1");
        selenium.type("collectionYear", "2009");
        if (useNormalDisease) {
            selectFromAutocomplete("pathologicalCharacteristic", ClientProperties.getNormalTerm());
        } else {
        selectFromAutocomplete("pathologicalCharacteristic", ClientProperties.getAutocompleteDiseaseNameStart(),
                ClientProperties.getAutocompleteDiseaseFullName());
        }
        selenium.type("availableQuantity", "");
        selenium.select("availableQuantityUnits", "- Select -");
        if (ClientProperties.isDisplayPrices()) {
            selenium.type("minimumPrice", "50.0");
            selenium.type("maximumPrice", "100.0");
        }
        String fullName = institutionName;
        String instName = StringUtils.split(institutionName)[0].toLowerCase();
        if (enterProtocol) {
            selectFromAutocomplete("protocol", "test cp2 from " + instName, "test cp2 from " + fullName);
        }
        selectFromAutocomplete("participant", "test male participant from " + instName,
                "test male participant from " + fullName);
    }

    /**
     * @return a new specimen external id.
     */
    protected String getNewSpecimenId() {
        return "test specimen" + ++specimenIdIndex;
    }

    private String getCurrentSpecimenId() {
        return "test specimen" + specimenIdIndex;
    }

    /**
     * Enter data for extensions. Subclasses should override to verify client-specific dynamic extensions.
     */
    protected void enterExtensionData() {

    }

    /**
     * Tests duplicate validation.
     */
    protected void duplicateValidation() {
        clickAndWait("link=Add New " + ClientProperties.getBiospecimenTerm());
        enterData(getCurrentSpecimenId(), true, ClientProperties.getDefaultConsortiumMemberName());
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("A specimen" + " exists with this external ID for this institution."));
    }

    /**
     * Views a specimen.
     */
    protected void viewPage() {
        clickAndWait("link=Back To List");
        assertTrue(selenium.isTextPresent(TITLE));
        String expectedHeader = selenium.getTable("specimen.1.2").replaceAll("\\s+", "");
        clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/a");
        String actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
        verifyDetailsPanel();
        assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " Characteristics"));
        assertTrue(selenium.isTextPresent("50 months"));
        assertTrue(selenium.isTextPresent(getCurrentSpecimenId()));
        assertTrue(selenium.isTextPresent("testType1"));
        assertTrue(selenium.isTextPresent("2009"));
        assertTrue(selenium.isTextPresent(ClientProperties.getAutocompleteDiseaseFullName()));
        assertEquals(ClientProperties.isDisplayPrices() && !ClientProperties.isAggregateSearchResultsActive(),
                selenium.isTextPresent("$50.00"));
        assertEquals(ClientProperties.isDisplayPrices() && !ClientProperties.isAggregateSearchResultsActive(),
                selenium.isTextPresent("$100.00"));
        assertNotSame(ClientProperties.isAggregateSearchResultsActive(), selenium.isElementPresent("add_to_cart"));
        if (isCollectionProtocolDisplayed()) {
            assertTrue(selenium.isTextPresent("test cp2 from " + ClientProperties.getDefaultConsortiumMemberName()));
        } else {
            assertFalse(selenium.isTextPresent("Collection Protocol"));
        }

        assertTrue(selenium.isTextPresent("Patient Demographics"));
        assertTrue(selenium.isTextPresent("Male"));
        assertTrue(selenium.isTextPresent("Asian"));
        assertTrue(selenium.isTextPresent("Black or African American"));
        assertTrue(selenium.isTextPresent("Native Hawaiian or other Pacific Islander"));
        assertTrue(selenium.isTextPresent("Usage Restrictions"));
        if (ClientProperties.isDisplayInstitutionSpecimenAdmin()) {
            assertTrue(selenium.isTextPresent(ClientProperties.getInstitutionTerm()));
        }
        viewExtensionData();
    }

    /**
     * Verifies the contents of the specimen details panel. Subclasses can override this method
     * to provide client specific validation of the details panel.
     */
    protected void verifyDetailsPanel() {
        verifyDetailsPanelColumnLabels();
        verifyDetailsPanelColumnValues();
    }

    /**
     * Set up the expected details panel columns. Subclasses can override this method to set up
     * client specific expected details panel columns.
     */
    protected void setUpExpectedDetailsPanelColumns() {
        setExpectedDetailsPanelColumn2Label("Type");
        setExpectedDetailsPanelColumn2Value("testType1");
        setExpectedDetailsPanelColumn4Label(ClientProperties.getQuantityTerm());
        setExpectedDetailsPanelColumn4Value("Unknown");
    }

    /**
     * Verifies the details panel column labels. Subclasses can use the setter methods to set
     * the expected column labels before verification.
     */
    protected void verifyDetailsPanelColumnLabels() {
        String actualColumn1Label = selenium.getText("id=details_panel_column1_label");
        String actualColumn2Label = selenium.getText("id=details_panel_column2_label");
        String actualColumn3Label = selenium.getText("id=details_panel_column3_label");
        String actualColumn4Label = selenium.getText("id=details_panel_column4_label");
        assertEquals(getExpectedDetailsPanelColumn1Label(), actualColumn1Label);
        assertEquals(getExpectedDetailsPanelColumn2Label(), actualColumn2Label);
        assertEquals(getExpectedDetailsPanelColumn3Label(), actualColumn3Label);
        assertEquals(getExpectedDetailsPanelColumn4Label(), actualColumn4Label);
    }

    /**
     * Verifies the details panel column values. Subclasses can use the setter methods to set
     * the expected column values before verification.
     */
    protected void verifyDetailsPanelColumnValues() {
        assertEquals(getExpectedDetailsPanelColumn1Value(), selenium.getText("id=details_panel_column1_value"));
        assertEquals(getExpectedDetailsPanelColumn2Value(), selenium.getText("id=details_panel_column2_value"));
        assertEquals(getExpectedDetailsPanelColumn3Value(), selenium.getText("id=details_panel_column3_value"));
        assertEquals(getExpectedDetailsPanelColumn4Value(), selenium.getText("id=details_panel_column4_value"));
    }

    /**
     * Enter data for extensions. Subclasses should override to verify client-specific dynamic extensions.
     */
    protected void viewExtensionData() {

    }

    /**
     * Edits a specimen.
     */
    protected void edit() {
        clickAndWait("link=Cancel");
        assertTrue(selenium.isTextPresent(TITLE));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        String expectedHeader = selenium.getTable("specimen.1.2").replaceAll("\\s+", "");
        clickAndWait("link=Edit");
        String actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);

        clickAndWait("link=Cancel");
        assertTrue(selenium.isTextPresent(TITLE));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        expectedHeader = selenium.getTable("specimen.1.2").replaceAll("\\s+", "");
        clickAndWait("link=Edit");
        actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);

        clickAndWait("link=Back To List");
        assertTrue(selenium.isTextPresent(TITLE));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        expectedHeader = selenium.getTable("specimen.1.2").replaceAll("\\s+", "");
        clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/a");
        actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);

        clickAndWait("link=Edit");
        actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
        assertFalse(selenium.isChecked("normalSample"));
        selenium.type("externalId", "test specimen updated");
        selenium.type("availableQuantity", "100");
        selenium.select("availableQuantityUnits", "cells");
        assertEquals(ClientProperties.getAutocompleteDiseaseFullName(),
                selenium.getValue("pathologicalCharacteristic"));
        if (ClientProperties.isDisplayPrices()) {
            selenium.check("priceNegotiable");
            selenium.fireEvent("priceNegotiable", "click");
            pause(DELAY);
        }
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Specimen successfully saved."));
        assertTrue(selenium.isTextPresent(TITLE));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("100 cells"));
        clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/a");
        assertFalse(selenium.isTextPresent("$50.00"));
        assertFalse(selenium.isTextPresent("$100.00"));
        assertEquals(ClientProperties.isDisplayPrices() && !ClientProperties.isAggregateSearchResultsActive(),
                selenium.isTextPresent("To be negotiated"));
        assertTrue(selenium.isTextPresent("100 cells"));
        clickAndWait("link=Edit");
        pause(DELAY);
        if (ClientProperties.isDisplayPrices()) {
            assertFalse(selenium.isEditable("minimumPrice"));
            assertTrue(StringUtils.isBlank(selenium.getValue("minimumPrice")));
            assertFalse(selenium.isEditable("maximumPrice"));
            assertTrue(StringUtils.isBlank(selenium.getValue("maximumPrice")));
        }
        selenium.click("consentWithdrawn_YES");
        selenium.type("consentWithdrawnDate", "");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Consent Withdrawn and Consent Withdrawn Date must both be set."));
        assertTrue(selenium.isTextPresent("Consent cannot be withdrawn for a specimen with the given status."));
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        selenium.click("consentWithdrawn_YES");
        selenium.type("consentWithdrawnDate", "01/01/2010");
        selenium.select("specimenStatus", "label=Returned to " + ClientProperties.getParticipantTerm());
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Specimen successfully saved."));

        expectedHeader = selenium.getTable("specimen.1.2").replaceAll("\\s+", "");
        clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/a");
        actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
        clickAndWait("link=Edit");
        assertEquals(expectedHeader, actualHeader);
        selenium.click("consentWithdrawn_NO");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Specimen successfully saved."));
        clickAndWait("link=Sign Out");
    }

    /**
     * Test the display of unused dynamic extensions.
     * Subclasses can override if not applicable.
     */
    protected void emptyExtensions() {
        goToSpecimenPage();
        enterDataNoExtensions(getNewSpecimenId(), true, ClientProperties.getDefaultConsortiumMemberName(), false);
        clickAndWait("btn_save");
        selenium.type("externalId", getCurrentSpecimenId());
        clickAndWait("btn_search");
        clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/a");
        viewEmptyExtensions();
    }

    /**
     * Test display of specific empty dynamic extensions.
     */
    protected void viewEmptyExtensions() {
        for (int index : getExtensionViewIndexes()) {
            assertEquals(getExtensionUnavailableText(), selenium.getText(String.format(VIEW_PAGE_XPATH, index)));
        }
    }

    /**
     * Verify status history on the details page.
     */
    protected void verifyStatusHistory() {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");
        clickAndWait("link=" + ClientProperties.getBiospecimenAdministrationLink());
        selenium.type("externalId", getCurrentSpecimenId());
        clickAndWait("btn_search");
        clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/a");
        String history = selenium.getText(STATUS_HISTORY_XPATH);
        String dateString = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
        assertTrue(history.startsWith("Available"));
        assertTrue(history.endsWith(dateString));
        clickAndWait("link=Back");
        clickAndWait("link=Edit");
        assertFalse(selenium.isVisible("statusTransitionDateContainer"));
        selenium.select("specimenStatus", "Unavailable");
        pause(SHORT_DELAY);
        assertTrue(selenium.isVisible("statusTransitionDateContainer"));
        assertTrue(selenium.isTextPresent("Unavailable Date"));
        selenium.type("statusTransitionDate", "01/01/2010");
        clickAndWait("btn_save");
        clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/a");
        history = selenium.getText(STATUS_HISTORY_XPATH);
        String[] histories = StringUtils.stripAll(StringUtils.split(history, '\n'));
        assertTrue(histories[0].startsWith("Available"));
        assertTrue(histories[0].endsWith(dateString));
        assertTrue(histories[1].startsWith("Unavailable"));
        assertTrue(histories[1].endsWith("01/01/2010"));
        clickAndWait("link=Back");
        clickAndWait("link=Edit");
        selenium.select("specimenStatus", "Unavailable");
        assertFalse(selenium.isVisible("statusTransitionDateContainer"));
        clickAndWait("btn_save");
        clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/a");
        String unchangedHistory = selenium.getText(STATUS_HISTORY_XPATH);
        assertEquals(history, unchangedHistory);
        clickAndWait("link=Back");
        clickAndWait("link=Edit");
        selenium.select("specimenStatus", "Destroyed");
        pause(SHORT_DELAY);
        assertTrue(selenium.isVisible("statusTransitionDateContainer"));
        assertTrue(selenium.isTextPresent("Destroyed Date"));
        clickAndWait("btn_save");
        clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/a");
        history = selenium.getText(STATUS_HISTORY_XPATH);
        histories = StringUtils.stripAll(StringUtils.split(history, '\n'));
        assertTrue(histories[0].startsWith("Available"));
        assertTrue(histories[0].endsWith(dateString));
        assertTrue(histories[1].startsWith("Unavailable"));
        assertTrue(histories[1].endsWith("01/01/2010"));
        assertTrue(histories[2].startsWith("Destroyed"));
        assertTrue(histories[2].endsWith(dateString));
    }

    /**
     * Verifies display of single specimen price (when minimum and maximum price are equal).
     */
    protected void verifySinglePrice() {
        String priceRange = "$20.00 - $20.00";
        String listPrice  = "$20.00";

        if (ClientProperties.isDisplayPrices()) {
            loginAsAdmin();
            goToSpecimenPage();
            enterData(getNewSpecimenId(), true, ClientProperties.getDefaultConsortiumMemberName());
            mouseOverAndPause("link=Administration");
            clickAndWait("link=" + ClientProperties.getBiospecimenAdministrationLink());

            String externalId = getCurrentSpecimenId();
            selenium.type("externalId", externalId);
            clickAndWait("btn_search");
            clickAndWait("//table[@id='specimen']/tbody/tr[1]/td[10]/a");

            if (selenium.isChecked("id=priceNegotiable")) {
                selenium.click("//input[@id='priceNegotiable']");
            }

            selenium.type("minimumPrice", "20.00");
            selenium.type("maximumPrice", "20.00");
            clickAndWait("btn_save");

            clickAndWait("link=" + selenium.getTable("specimen.1.0"));
            assertFalse(selenium.isTextPresent(priceRange));
            assertNotSame(ClientProperties.isAggregateSearchResultsActive(), selenium.isTextPresent(listPrice));

            selectSearchMenu();
            clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
            if (selenium.isElementPresent("id=externalId")) {
                selenium.type("externalId", externalId);
                clickAndWait("btn_search");
                String fee = selenium.getTable("specimen.1.7");
                assertFalse(fee.equals(priceRange));
                assertEquals(listPrice, fee);
            }
        }
    }

    private void defaultProtocol() {
       loginAsAdmin();
       goToSpecimenPage();
       // confirm field appears with no selection if more than 1 protocol
       selenium.select("institution", "label=" + ClientProperties.getDefaultConsortiumMemberName());
       if (!isCollectionProtocolDisplayed()) {
           pause(DELAY);
       }
       selenium.fireEvent("institution", "change");
       if (!isCollectionProtocolDisplayed()) {
           waitForText("Collection Protocol");
       }
       assertTrue(StringUtils.isEmpty(selenium.getValue("protocol")));
       // confirm protocol is selected (and possibly hidden) if only 1 exists
       selenium.select("institution", "label=" + ClientProperties.getSecondaryConsortiumMemberName());
       pause(DELAY);
       if (!isCollectionProtocolDisplayed()) {
           waitForText("Collection Protocol", false);
       }
       assertFalse(StringUtils.isEmpty(selenium.getValue("protocol")));
       // confirm selection works
       enterData(getNewSpecimenId(), false, ClientProperties.getSecondaryConsortiumMemberName());
       assertTrue(selenium.isTextPresent("Specimen successfully saved."));
       assertFalse(selenium.isTextPresent(ERROR_MESSAGE));

       // confirm edit field does not appear if default exists for inst admin
       clickAndWait("link=Sign Out");
       login(ClientProperties.getAlternateInstitutionalAdminEmail(), PASSWORD);
       goToSpecimenPage();
       if (!isCollectionProtocolDisplayed()) {
           assertFalse(selenium.isTextPresent("Collection Protocol"));
       }
       assertFalse(StringUtils.isEmpty(selenium.getValue("protocol")));
       enterData(getNewSpecimenId(), false, ClientProperties.getSecondaryConsortiumMemberName());
       assertTrue(selenium.isTextPresent("Specimen successfully saved."));
       assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
       clickAndWait("link=Sign Out");
    }

    /**
     * @return whether protocol is displayed.
     */
    protected boolean isCollectionProtocolDisplayed() {
        return true;
    }

    /**
     * Return the xpath indexes for individual dynamic
     * extensions for the specimen view page, to be implemented by subclasses.
     * @return the xpath indexes for individual dynamic extensions.
     */
    protected int[] getExtensionViewIndexes() {
        return new int[0];
    }

    /**
     * @return the display text for empty extensions. Subclasses should override if necessary.
     */
    protected String getExtensionUnavailableText() {
        return CUSTOM_PROPERTY_DEFAULT_UNAVAILABLE_TEXT;
    }

    /**
     * Gets the expected label for column 1 of the details panel.
     *
     * @return expected label for column 1
     */
    public String getExpectedDetailsPanelColumn1Label() {
        return expectedDetailsPanelColumn1Label;
    }

    /**
     * Sets the expected label for column 1 of the details panel.
     *
     * @param label expected label for column 1
     */
    protected void setExpectedDetailsPanelColumn1Label(String label) {
        expectedDetailsPanelColumn1Label = label;
    }

    /**
     * Gets the expected label for column 2 of the details panel.
     *
     * @return expected label for column 2
     */
    public String getExpectedDetailsPanelColumn2Label() {
        return expectedDetailsPanelColumn2Label;
    }

    /**
     * Sets the expected label for column 2 of the details panel.
     *
     * @param label expected label for column 2
     */
    protected void setExpectedDetailsPanelColumn2Label(String label) {
        expectedDetailsPanelColumn2Label = label;
    }

    /**
     * Gets the expected label for column 3 of the details panel.
     *
     * @return expected label for column 3
     */
    public String getExpectedDetailsPanelColumn3Label() {
        return expectedDetailsPanelColumn3Label;
    }

    /**
     * Sets the expected label for column 3 of the details panel.
     *
     * @param label expected label for column 3
     */
    protected void setExpectedDetailsPanelColumn3Label(String label) {
        expectedDetailsPanelColumn3Label = label;
    }

    /**
     * Gets the expected label for column 4 of the details panel.
     *
     * @return expected label for column 4
     */
    public String getExpectedDetailsPanelColumn4Label() {
        return expectedDetailsPanelColumn4Label;
    }

    /**
     * Sets the expected label for column 4 of the details panel.
     *
     * @param label expected label for column 4
     */
    protected void setExpectedDetailsPanelColumn4Label(String label) {
        expectedDetailsPanelColumn4Label = label;
    }

    /**
     * Gets the expected value for column 1 of the details panel.
     *
     * @return expected value for column 1
     */
    public String getExpectedDetailsPanelColumn1Value() {
        return expectedDetailsPanelColumn1Value;
    }

    /**
     * Sets the expected value for column 1 of the details panel.
     *
     * @param value expected value for column 1
     */
    protected void setExpectedDetailsPanelColumn1Value(String value) {
        expectedDetailsPanelColumn1Value = value;
    }

    /**
     * Gets the expected value for column 2 of the details panel.
     *
     * @return expected value for column 2
     */
    public String getExpectedDetailsPanelColumn2Value() {
        return expectedDetailsPanelColumn2Value;
    }

    /**
     * Sets the expected value for column 2 of the details panel.
     *
     * @param value expected value for column 2
     */
    protected void setExpectedDetailsPanelColumn2Value(String value) {
        expectedDetailsPanelColumn2Value = value;
    }

    /**
     * Gets the expected value for column 3 of the details panel.
     *
     * @return expected value for column 3
     */
    public String getExpectedDetailsPanelColumn3Value() {
        return expectedDetailsPanelColumn3Value;
    }

    /**
     * Sets the expected value for column 3 of the details panel.
     *
     * @param value expected value for column 3
     */
    protected void setExpectedDetailsPanelColumn3Value(String value) {
        expectedDetailsPanelColumn3Value = value;
    }

    /**
     * Gets the expected value for column 4 of the details panel.
     *
     * @return expected value for column 4
     */
    public String getExpectedDetailsPanelColumn4Value() {
        return expectedDetailsPanelColumn4Value;
    }

    /**
     * Sets the expected value for column 4 of the details panel.
     *
     * @param value expected value for column 4
     */
    protected void setExpectedDetailsPanelColumn4Value(String value) {
        expectedDetailsPanelColumn4Value = value;
    }

    /**
     * verify the functionality of the normal sample checkbox and the associated autocompleter.
     */
    protected void exerciseNormalSampleCheckbox() {
        goToSpecimenPage();

        selenium.click("normalSample");
        pause(SHORT_DELAY);
        assertTrue(selenium.isChecked("normalSample"));
        assertFalse(selenium.isEditable("pathologicalCharacteristic"));
        assertEquals(ClientProperties.getNormalTerm(), selenium.getValue("pathologicalCharacteristic"));
        String normalId = selenium.getValue("pathologicalCharacteristicHidden");
        assertTrue(StringUtils.isNotBlank(normalId));
        assertTrue(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));
        assertEquals(ClientProperties.getNormalTerm(), selenium.getValue("pathologicalCharacteristicAutoHidden"));

        clickAndWait("btn_save");
        assertTrue(selenium.isChecked("normalSample"));
        assertFalse(selenium.isEditable("pathologicalCharacteristic"));
        assertEquals(ClientProperties.getNormalTerm(), selenium.getValue("pathologicalCharacteristic"));
        assertEquals(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertTrue(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));
        assertEquals(ClientProperties.getNormalTerm(), selenium.getValue("pathologicalCharacteristicAutoHidden"));

        selenium.click("normalSample");
        pause(SHORT_DELAY);
        assertFalse(selenium.isChecked("normalSample"));
        assertTrue(selenium.isEditable("pathologicalCharacteristic"));
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristicHidden")));
        assertNotSame(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertFalse(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));

        clickAndWait("btn_save");
        assertFalse(selenium.isChecked("normalSample"));
        assertTrue(selenium.isEditable("pathologicalCharacteristic"));
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristicHidden")));
        assertNotSame(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertFalse(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));

        selectFromAutocomplete("pathologicalCharacteristic", ClientProperties.getAutocompleteDiseaseNameStart(),
                ClientProperties.getAutocompleteDiseaseFullName());
        assertFalse(selenium.isChecked("normalSample"));
        assertTrue(selenium.isEditable("pathologicalCharacteristic"));
        assertFalse(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));
        assertEquals(ClientProperties.getAutocompleteDiseaseFullName(),
                selenium.getValue("pathologicalCharacteristic"));
        assertFalse(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristicHidden")));
        assertNotSame(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertFalse(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));

        clickAndWait("btn_save");
        assertFalse(selenium.isChecked("normalSample"));
        assertTrue(selenium.isEditable("pathologicalCharacteristic"));
        assertFalse(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));
        assertEquals(ClientProperties.getAutocompleteDiseaseFullName(),
                selenium.getValue("pathologicalCharacteristic"));
        assertFalse(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristicHidden")));
        assertNotSame(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertFalse(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));

        String externalId = getNewSpecimenId();
        clickAndWait("link=Back To List");
        clickAndWait("link=Add New " + ClientProperties.getBiospecimenTerm());
        enterDataNoExtensions(externalId, true,  ClientProperties.getDefaultConsortiumMemberName(), true);
        enterExtensionData();
        setUpExpectedDetailsPanelColumns();
        clickAndWait("btn_save");
        selenium.type("externalId", externalId);
        clickAndWait("btn_search");
        clickAndWait("link=Edit");
        assertTrue(selenium.isChecked("normalSample"));
        assertFalse(selenium.isEditable("pathologicalCharacteristic"));
        assertEquals(ClientProperties.getNormalTerm(), selenium.getValue("pathologicalCharacteristic"));
        assertEquals(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertTrue(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));
        assertEquals(ClientProperties.getNormalTerm(), selenium.getValue("pathologicalCharacteristicAutoHidden"));
}
}
