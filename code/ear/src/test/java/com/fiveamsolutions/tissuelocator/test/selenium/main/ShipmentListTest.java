/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author ddasgupta
 *
 */
public class ShipmentListTest extends AbstractListTest {

    /**
     * Column count.
     */
    private static final int COL_COUNT = 6;

    /**
     * Results count.
     */
    private static final int RESULT_COUNT = 72;

    /**
     * Beginning index for disease after specimen cell of the request table has been split into an array.
     */
    private static final int DISEASE_BEGIN_INDEX = 5;

    private static final String ALL = "All";
    private static final String PENDING = "Pending Shipment";
    private static final String SHIPPED = "Shipped";
    private static final String RECEIVED = "Received";
    private static final String CANCELED = "Canceled";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "ShipmentListTest.sql";
    }

    /**
     * test the shipment list page.
     * @throws Exception on error
     */
    @Test
    public void testList() throws Exception {
        if (ClientProperties.isOrderAdministrationActive()) {
            goToListPage();
            statusFilter();
            sorting(COL_COUNT, "shipment", true);
            paging("shipment");
            verifyExportPresent();
            pageSize(RESULT_COUNT);
            goToListPage();
            editPage();
            markAsShipped();
            viewPage();
            updatedStatusFilter();
            trackingUrl();
            homePageMessage();
        }
    }

    /**
     * Status filter.
     */
    private void statusFilter() {
        verifyStatus(PENDING, "1-18 of 18 Results", false, true);
        verifyStatus(RECEIVED, "1-18 of 18 Results", true, true);
        verifyStatus(SHIPPED, "1-18 of 18 Results", true, true);
        verifyStatus(CANCELED, "1-18 of 18 Results", true, true);
        verifyStatus(PENDING, "1-18 of 18 Results", true, true);
        verifyStatus(ALL, "1-20 of 72 Results", true, false);
    }

    /**
     * Updated status filter.
     */
    private void updatedStatusFilter() {
        verifyStatus(PENDING, "1-17 of 17 Results", false, true);
        verifyStatus(RECEIVED, "1-18 of 18 Results", true, true);
        verifyStatus(SHIPPED, "1-19 of 19 Results", true, true);
        verifyStatus(CANCELED, "1-18 of 18 Results", true, true);
        verifyStatus(PENDING, "1-17 of 17 Results", true, true);
        verifyStatus(ALL, "1-20 of 72 Results", true, false);
    }

    private void verifyStatus(String statusLabel, String resultsString, boolean selectStatus, boolean verifyStatus) {
        if (selectStatus) {
            selenium.select("status", "label=" + statusLabel);
            waitForPageToLoad();
        }
        assertEquals(statusLabel, selenium.getSelectedLabel("status"));
        if (verifyStatus) {
            assertEquals(statusLabel, selenium.getTable("shipment.1.4"));
        }
        assertTrue(selenium.isTextPresent(resultsString));
    }

    /**
     * Go to list page.
     */
    private void goToListPage() {
        login(ClientProperties.getTissueTechEmail(), "tissueLocator1");
        goToOrderList();
    }

    /**
     * Edit page.
     */
    private void editPage() {
        selenium.select("status", "label=All");
        waitForPageToLoad();
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Order Fulfillment"));
        verifyRequest(false);
        if (ClientProperties.isAggregateSearchResultsActive()) {
            verifyAggregateRequests();
        } else {
            verifySpecimens();
        }
        verifyRecipientEdit();
        verifyTrackingEdit();

        if (ClientProperties.isDisplayPrices()) {
            if (!ClientProperties.isAggregateSearchResultsActive()) {
                verifySpecimenPrices();
            }
            assertTrue(selenium.isTextPresent("Shipping Price"));
            assertEquals("12.34", selenium.getValue("price"));
            assertTrue(selenium.isTextPresent("Fees"));
            assertEquals("56.78", selenium.getValue("fees"));
        }

        clickAndWait("link=Back To List");
        selenium.select("status", "label=Pending Shipment");
        waitForPageToLoad();
    }

    /**
     * View page.
     */
    private void viewPage() {
        selenium.select("status", "label=Shipped");
        pause(LONG_DELAY);
        waitForText("DHL");
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
        assertTrue("'Order * Shipped' not found, body text: " + selenium.getBodyText(),
                selenium.isTextPresent("Order * (Shipped)"));
        verifyRequest(true);
        if (ClientProperties.isAggregateSearchResultsActive()) {
            verifyAggregateRequests();
        } else {
            verifySpecimens();
        }
        verifyRecipientView();
        verifyTrackingView();
        verifyMta();

        if (ClientProperties.isDisplayPrices()) {
            assertTrue(selenium.isTextPresent("Shipping Price"));
            assertTrue(selenium.isTextPresent("$12.34"));
            assertTrue(selenium.isTextPresent("Fees"));
            assertTrue(selenium.isTextPresent("$56.78"));
            assertTrue(selenium.isTextPresent("Order Total"));
            assertTrue(selenium.isTextPresent("$69.12"));
        }

        goToListPage();
        selenium.select("status", "label=Pending Shipment");
        waitForPageToLoad();
    }

    /**
     * Verify the request associated with this order.
     * @param isView whether a read-only view is being verified
     */
    private void verifyRequest(boolean isView) {
        collapseSection("Shipping Information", "shipping_details");
        if (isView) {
            collapseSection("Study, Protocol, and P.I.", "study_details");
        } else {
            collapseSection("Ship To", "shipping_address");
        }
        clickAndWait("link=View Request Details");
        assertTrue(selenium.isTextPresent("Research Proposal"));
        assertFalse(selenium.isElementPresent("cart"));
        testFileDownload("resume.txt");
        boolean showPILegalFields = ClientProperties.getDisplayPILegalFields();
        assertEquals(showPILegalFields, selenium.isTextPresent("I certify that the Principal investigator"));
        assertEquals(showPILegalFields, selenium.isElementPresent("investigatorCertifiedLabel"));
        assertEquals(showPILegalFields, selenium.isTextPresent("Has the Principal Investigator been subject"));
        assertEquals(showPILegalFields, selenium.isElementPresent("investigatorInvestigatedLabel"));
        assertFalse(selenium.isTextPresent("Explanation"));
        clickAndWait("link=Go Back To Order Administration");
    }

    /**
     * Verify the specimens associated with this order.
     */
    private void verifySpecimens() {
        String[] diseases = ClientProperties.getDiseases();
        assertTrue(selenium.isTextPresent("test 10"));
        assertTrue(selenium.isTextPresent(diseases[0]));
        assertTrue(selenium.isTextPresent("100.00 milligram"));
        assertTrue(selenium.isTextPresent("test 15"));
        assertTrue(selenium.isTextPresent(diseases[1]));
        assertTrue(selenium.isTextPresent("200.00 cells"));
        assertTrue(selenium.isTextPresent("test 20"));
        assertTrue(selenium.isTextPresent(diseases[2]));
        assertTrue(selenium.isTextPresent("300.00 count"));
        String[] specimenCellContent = selenium.getTable("request.1.0").split("\\s+");
        String expectedHeader = StringUtils.join(specimenCellContent, "", DISEASE_BEGIN_INDEX,
                specimenCellContent.length);
        clickAndWait("xpath=//table[@id='request']/tbody/tr[1]/td[1]/a");
        String actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
        assertTrue(selenium.isTextPresent("test 10"));
        selenium.goBack();
        waitForPageToLoad();
    }

    private void verifyAggregateRequests() {
        assertTrue(selenium.isTextPresent(ClientProperties.getDefaultConsortiumMemberName()));
        assertTrue(selenium.isTextPresent("No Criteria Specified"));
        assertTrue(selenium.isTextPresent("Birth Year: 2005"));
        assertTrue(selenium.isTextPresent("Birth Year: 2008"));
        assertTrue(selenium.isTextPresent("600"));
        assertTrue(selenium.isTextPresent("700"));
        assertTrue(selenium.isTextPresent("800"));
    }

    private void verifySpecimenPrices() {
        assertTrue(selenium.isTextPresent("To be negotiated"));
        assertTrue(selenium.isTextPresent("$50.00"));
        assertTrue(selenium.isTextPresent("$100.00"));
        assertTrue(selenium.isTextPresent("$25.00"));
        assertTrue(selenium.isTextPresent("$75.00"));
    }

    /**
     * Verify the editable view of the recipient of this order.
     */
    private void verifyRecipientEdit() {
        assertTrue(selenium.isTextPresent("First Name"));
        assertEquals("test first", selenium.getValue("recipientFirstName"));
        assertTrue(selenium.isTextPresent("Last Name"));
        assertEquals("test last", selenium.getValue("recipientLastName"));
        assertTrue(selenium.isTextPresent("Address"));
        assertEquals("123 test street", selenium.getValue("recipientLine1"));
        assertEquals("", selenium.getValue("recipientLine2"));
        assertTrue(selenium.isTextPresent("City"));
        assertEquals("test city", selenium.getValue("recipientCity"));
        assertTrue(selenium.isTextPresent("State"));
        assertEquals("MARYLAND", selenium.getValue("recipientState"));
        assertTrue(selenium.isTextPresent("Zip/Postal Code"));
        assertEquals("12345", selenium.getValue("recipientZip"));
        assertTrue(selenium.isTextPresent("Country"));
        assertEquals("UNITED_STATES", selenium.getValue("recipientCountry"));
        assertTrue(selenium.isTextPresent("Email"));
        assertEquals("test@example.com", selenium.getValue("recipientEmail"));
        assertTrue(selenium.isTextPresent("Phone"));
        assertEquals("1234567890", selenium.getValue("recipientPhone"));
        assertTrue(selenium.isTextPresent("ext."));
        assertEquals("1234", selenium.getValue("recipientPhoneExtension"));
        assertFalse(selenium.isElementPresent("recipientFax"));
    }

    /**
     * Verify the read only view of the recipient of this order.
     */
    private void verifyRecipientView() {
        assertTrue(selenium.isTextPresent("test first"));
        assertTrue(selenium.isTextPresent("test last"));
        assertTrue(selenium.isTextPresent("123 test street"));
        assertTrue(selenium.isTextPresent("test city"));
        assertTrue(selenium.isTextPresent("Maryland"));
        assertTrue(selenium.isTextPresent("12345"));
        assertTrue(selenium.isTextPresent("United States"));
        assertTrue(selenium.isTextPresent("test@example.com"));
        assertTrue(selenium.isTextPresent("1234567890"));
    }

    /**
     * Verify the editable view of the shipping and tracking information for this order.
     */
    private void verifyTrackingEdit() {
        assertTrue(selenium.isTextPresent("Shipping Vendor"));
        assertEquals("DHL", selenium.getValue("sv"));
        assertTrue(selenium.isTextPresent("Tracking Number"));
        assertEquals("DHL", selenium.getValue("tn"));
        assertTrue(selenium.isTextPresent("Shipping Instructions"));
        assertEquals("test instructions", selenium.getValue("si"));
    }

    /**
     * Verify the read only view of the shipping and tracking information for this order.
     */
    private void verifyTrackingView() {
        assertTrue(selenium.isTextPresent("Shipping Vendor"));
        assertTrue("DHL not found on page, body text: " + selenium.getBodyText(),
                selenium.isTextPresent("DHL"));
        assertTrue(selenium.isTextPresent("Tracking Link"));
        assertTrue(selenium.isElementPresent("link=DHL"));
        assertTrue(selenium.isTextPresent("Shipping Instructions"));
        assertTrue(selenium.isTextPresent("test instructions"));
    }

    private void verifyMta() {
        boolean isMtaActive = ClientProperties.isMtaActive();
        String requestorName = ClientProperties.getResearcherInstitution();
        String senderName = ClientProperties.getDefaultConsortiumMemberName();
        assertEquals(isMtaActive, selenium.isElementPresent("link=Requestor: " + requestorName + " signed ABC MTA"));
        assertEquals(isMtaActive, selenium.isElementPresent("link=Sender: " + senderName + " signed ABC MTA"));
        if (isMtaActive) {
            String fileTarget = selenium.getAttribute("link=Requestor: " + requestorName + " signed ABC MTA@href");
            assertTrue(Pattern.matches(".*/protected/downloadFile[^/]*.*text[^/]*plain[^/]*.*", fileTarget));
            fileTarget = selenium.getAttribute("link=Sender: " + senderName + " signed ABC MTA@href");
            assertTrue(Pattern.matches(".*/protected/downloadFile[^/]*.*text[^/]*plain[^/]*.*", fileTarget));
        }
    }

    /**
     * tests file download.
     * @param fileName the file to download
     */
    private void testFileDownload(String fileName) {
        assertTrue(selenium.isElementPresent("link=" + fileName));
        String fileTarget = selenium.getAttribute("link=" + fileName + "@href");
        String docUrlBase = "/protected/downloadFile";
        assertTrue(fileTarget.contains(docUrlBase));
        assertFalse(fileTarget.substring(fileTarget.lastIndexOf(docUrlBase) + docUrlBase.length()).contains("/"));
        assertTrue(fileTarget.contains(fileName));
    }

    /**
     * Tracking url.
     */
    private void trackingUrl() {
        selenium.select("pageSize", "label=100");
        waitForPageToLoad();
        String[] shippers = new String[] {"DHL", "FedEx", "UPS", "USPS"};
        for (String shipper : shippers) {
            assertTrue(selenium.isElementPresent("link=" + shipper));
            selenium.openWindow("", "externalTracking");
            selenium.click("link=" + shipper);
            selenium.selectWindow("externalTracking");
            selenium.windowFocus();
            pause(LONG_DELAY);
            assertTrue("Shipper info not found on external page for " + shipper + ", body text: "
                    + selenium.getBodyText(), selenium.isTextPresent(shipper));
            selenium.close();
            selenium.selectWindow(null);
        }
        assertTrue(selenium.isTextPresent("OTHER"));
        assertFalse(selenium.isElementPresent("link=OTHER"));
    }

    /**
     * Home page messages.
     * @throws Exception on error
     */
    private void homePageMessage() throws Exception {
        clickAndWait("link=Home");
        assertTrue(selenium.isTextPresent("Welcome back"));
        assertTrue(selenium.isTextPresent("17 Shipment Requests"));
        assertTrue(selenium.isElementPresent("link=17 Shipment Requests"));
        clickAndWait("link=17 Shipment Requests");
        assertTrue(selenium.isTextPresent("Order Administration"));
        verifyStatus(PENDING, "1-17 of 17 Results", false, true);

        runSqlScript("ShipmentListTest-update.sql");
        clickAndWait("link=Home");
        assertTrue(selenium.isTextPresent("Welcome back"));
        assertTrue(selenium.isTextPresent("1 Shipment Request"));
        assertTrue(selenium.isElementPresent("link=1 Shipment Request"));
        clickAndWait("link=1 Shipment Request");
        assertTrue(selenium.isTextPresent("Order Administration"));
        verifyStatus(PENDING, "1-1 of 1 Result", false, true);
    }

    /**
     * Mark as shipped.
     */
    private void markAsShipped() {
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
        clickAndWait("link=Mark As Shipped");
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent("Set Shipped Date"));
        clickAndWait("btn_setShipmentDate");
        pause(LONG_DELAY);
        selenium.selectWindow(null);
        goToOrderList();
    }

}
