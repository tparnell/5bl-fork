/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium;

import org.junit.Test;
import com.fiveamsolutions.tissuelocator.test.ClientProperties;

/**
 * User account approval tests.
 * 
 * @author jstephens
 */
public abstract class AbstractUserAccountApprovalTest extends AbstractTissueLocatorSeleniumTest {

    private int userCount = ClientProperties.getDefaultUserCount();
    
    /**
     * Get the name of the test data script.
     * @return script name.
     */
    @Override
    protected String getScriptName() {
        return "UserAccountApprovalTest.sql";
    }
    
    /**
     *  Approve user account.
     */
    @Test
    public void testApproveUserAccount() {
        if (ClientProperties.isAccountApprovalActive()) {
            loginAsAdmin();
            clickAndWait("link=1 Registration Review");
            assertEquals("Pending", selenium.getTable("user.1.3"));
            clickAndWait("link=Edit");
            selenium.select("id=status", "label=Active");
            clickAndWait("xpath=//input[@type='submit' and @value='Save']");        
            selenium.select("id=status", "label=Active");
            waitForPageToLoad();                    
            assertTrue(selenium.isTextPresent("of " + (userCount + 1) + " Results"));
            clickAndWait("link=Sign Out");
        }
    }
    
    /**
     *  Deny user account.
     */
    @Test
    public void testDenyUserAccount() {
        if (ClientProperties.isAccountApprovalActive()) {
            loginAsAdmin();
            clickAndWait("link=1 Registration Review");
            assertEquals("Pending", selenium.getTable("user.1.3"));
            clickAndWait("link=Edit");
            selenium.select("id=status", "label=Inactive");
            waitForElementById("statusTransitionComment");
            selenium.type("id=statusTransitionComment", "Access denied.");
            clickAndWait("xpath=//input[@type='submit' and @value='Save']");
            selenium.select("id=status", "label=All Statuses");
            waitForPageToLoad();
            assertTrue(selenium.isTextPresent("of " + (userCount + 1) + " Results"));
            selenium.select("id=status", "label=Inactive");
            waitForPageToLoad();
            assertEquals("Inactive", selenium.getTable("user.1.3"));
            // When a read-only view is implemented, the view should be 
            // checked for the status comments TODO
            clickAndWait("link=Sign Out");
        }
    }
    
    /**
     * Deny user account without reason.
     */
    @Test
    public void testDenyUserAccountWithoutReason() {
        if (ClientProperties.isAccountApprovalActive()) {
            loginAsAdmin();
            clickAndWait("link=1 Registration Review");
            assertEquals("Pending", selenium.getTable("user.1.3"));
            clickAndWait("link=Edit");
            selenium.select("id=status", "label=Inactive");
            waitForElementById("statusTransitionComment");
            clickAndWait("xpath=//input[@type='submit' and @value='Save']");
            assertTrue(selenium.isTextPresent("A comment is required for denying a pending user."));
            assertTrue(selenium.isElementPresent("id=statusTransitionComment"));
            clickAndWait("link=Sign Out");
        }
    }
    
}
