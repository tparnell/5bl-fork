/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * @author ddasgupta
 */

public class ClientProperties {

    private static final Logger LOG = Logger.getLogger(ClientProperties.class);

    /**
     * the client properties file name.
     */
    private static final String CLIENT_PROPERTIES_FILE_NAME = "client.properties";

    /**
     * the property key for the researcher institution.
     */
    private static final String RESEARCHER_INSTITUTION_KEY = "researcher.institution";

    /**
     * the property key for the tissue tech email address.
     */
    private static final String TISSUE_TECH_EMAIL_KEY = "tissue.tech.email";

    /**
     * the property key for the institutional admin email address.
     */
    private static final String INSTITUTIONAL_ADMIN_EMAIL_KEY = "institutional.admin.email";

    /**
     * the property key for the institutional admin name.
     */
    private static final String INSTITUTIONAL_ADMIN_NAME_KEY = "institutional.admin.name";

    /**
     * the property key for the alternate institutional admin name.
     */
    private static final String ALTERNATE_INSTITUTIONAL_ADMIN_EMAIL_KEY = "alternate.institutional.admin.email";

    /**
     * the property key for the scientific reviewer email address.
     */
    private static final String SCIENTIFIC_REVIEWER_EMAIL_KEY = "scientific.reviewer.email";

    /**
     * the property key for the scientific reviewer name.
     */
    private static final String SCIENTIFIC_REVIEWER_NAME_KEY = "scientific.reviewer.name";

    /**
     * the property key for the alternate scientific reviewer email address.
     */
    private static final String ALTERNATE_SCIENTIFIC_REVIEWER_EMAIL_KEY = "alternate.scientific.reviewer.email";

    /**
     * the property key for the alternate scientific reviewer name.
     */
    private static final String ALTERNATE_SCIENTIFIC_REVIEWER_NAME_KEY = "alternate.scientific.reviewer.name";

    private static final String RESEARCHER_EMAIL_KEY = "researcher.email";
    private static final String RESEARCHER_NAME_KEY = "researcher.name";
    private static final String CONSORTIUM_ADMIN_EMAIL_KEY = "consortium.admin.email";
    private static final String SUPERADMIN_NAME = "superadmin.name";

    /**
     * the property key for the diseases.
     */
    private static final String DISEASES_KEY = "diseases";
    private static final String DISEASE_COUNTS_KEY = "diseaseCounts";
    private static final String SPECIMEN_SEARCH_DISEASES_KEY = "specimenSearch.diseases";
    private static final String SPECIMEN_SEARCH_FULL_LIST = "specimenSearch.diseases.isFullList";

    private static final String INSTITUTIONS_KEY = "institutions";
    private static final String INSTITUTIONS_COUNTS_KEY = "institutionCounts";

    /**
     * the property key for the anatomic source.
     */
    private static final String ANATOMIC_SOURCE_KEY = "anatomicSource";

    /**
     * the property key for the number of anatomic source results a test will return.
     */
    private static final String ANATOMIC_SOURCE_RESULTS_KEY = "anatomicSource.results";

    /**
     * the property key for the number of anatomic source pages a test will return.
     */
    private static final String ANATOMIC_SOURCE_PAGES_KEY = "anatomicSource.pages";

    /**
     * the property key for the number of default institutions.
     */
    private static final String DEFAULT_INSTITUTION_COUNT_KEY = "default.institution.count";

    /**
     * the property key for the number of default users.
     */
    private static final String DEFAULT_USER_COUNT_KEY = "default.user.count";

    /**
     * the property key for the default consortium member name.
     */
    private static final String DEFAULT_CONSORTIUM_MEMBER_NAME_KEY = "default.consortium.member.name";

    /**
     * the property key for the secondary consortium member name.
     */
    private static final String SECONDARY_CONSORTIUM_MEMBER_NAME_KEY = "secondary.consortium.member.name";

    /**
     * the property key for the beginning of the name of the disease used for the autocompleter.
     */
    private static final String AUTOCOMPLETE_DISEASE_NAME_START_KEY = "autocomplete.disease.name.start";

    /**
     * the property key for the full name of the disease used for the autocompleter.
     */
    private static final String AUTOCOMPLETE_DISEASE_FULL_NAME_KEY = "autocomplete.disease.full.name";

    /**
     * the property key for the text identifying the biospecimen search menu.
     */
    private static final String BIOSPECIMEN_SEARCH_MENU_KEY = "biospecimen.search.menu";

    /**
     * the property key for the text of the biospecimen search link in the menu.
     */
    private static final String BIOSPECIMEN_SEARCH_LINK_KEY = "biospecimen.search.link";

    /**
     * the property key for the text of the cart link in the menu.
     */
    private static final String CART_LINK_KEY = "cart.link";

    /**
     * the property key for the text of the prospective collection link in the menu.
     */
    private static final String PROSPECTIVE_COLLECTION_LINK_KEY = "prospectiveCollection.link";

    /**
     * the property key for the text of the prospective collection notes input.
     */
    private static final String PROSPECTIVE_COLLECTION_NOTES_KEY = "prospectiveCollection.notes";

    /**
     * the property key for the text of the prospective collection required error message.
     */
    private static final String PROSPECTIVE_COLLECTION_REQUIRED_KEY = "prospectiveCollection.required";

    /**
     * the property key for the term used for prospective collection.
     */
    private static final String PROSPECTIVE_COLLECTION_TERM_KEY = "prospectiveCollection.term";

    /**
     * the property key for the general term used for biospecimens.
     */
    private static final String BIOSPECIMEN_TERM_KEY = "biospecimen.term";

    /**
     * the property key for the general term used for biospecimens, in lowercase.
     */
    private static final String BIOSPECIMEN_TERM_LOWERCASE_KEY = "biospecimen.term.lowercase";

    /**
     * the property key for the general term used for disease.
     */
    private static final String DISEASE_TERM_KEY = "disease.term";

    /**
     * the property key for the general term used for specimens.
     */
    private static final String SPECIMEN_TERM_KEY = "specimen.term";

    /**
     * the property key for the general term used for normal samples.
     */
    private static final String NORMAL_TERM_KEY = "normal.term";

    /**
     * the property key for the general term used for specimens, in lowercase.
     */
    private static final String SPECIMEN_TERM_LOWERCASE_KEY = "specimen.term.lowercase";

    /**
     * the property key for the general term used for institutes.
     */
    private static final String INSTITUTION_TERM_KEY = "institution.term";

    /**
     * the property key for the general term used for 'institutional'.
     */
    private static final String INSTITUTIONAL_TERM_KEY = "institutional.term";
    /**
     * the property key for the general term used for a P.I. resume.
     */
    private static final String PI_RESUME_TERM_KEY = "investigator.resume.term";
    /**
     * the property key for the group to which the lead reviewer role is assigned.
     */
    private static final String LEAD_REVIEWER_GROUP_KEY = "leadReviewer.group";
    private static final String SCIENTIFIC_REVIEWER_GROUP_NAME = "scientificReviewer";
    private static final String INSTITUTIONAL_ADMIN_GROUP_NAME = "instAdmin";
    private static final String CONSORTIUM_REVIEW_ACTIVE_KEY = "consortiumReview.active";
    private static final String INSTITUTIONAL_REVIEW_ACTIVE_KEY = "institutionalReview.active";

    private static final String FINAL_VOTE_LABEL_KEY = "finalVoteLabel";
    private static final String FINAL_COMMENT_LABEL_KEY = "finalCommentLabel";
    private static final String RECEIPT_QUALITY_COLUMN_INDEX = "receipt.quality.column.index";
    private static final String DISPLAY_PI_LEGAL_FIELDS_KEY = "display.pi.legal.fields";
    private static final String DISPLAY_PROSPECTIVE_COLLECTION_KEY = "display.prospective.collection";
    private static final String DISPLAY_PROSPECTIVE_COLLECTION_TABLE_KEY = "display.prospective.collection.table";
    private static final String SIMPLE_SEARCH_ENABLED_KEY = "simple.search.enabled";
    private static final String SIMPLE_SEARCH_TITLE_KEY = "simple.search.title";
    private static final String ORDER_ADMINISTRATION_ACTIVE_KEY = "order.admin.active";
    private static final String PROTOCOL_ADMINISTRATION_ACTIVE_KEY = "protocol.admin.active";
    private static final String DISPLAY_PRICES_KEY = "display.prices";
    private static final String SPECIMEN_CELLS_COUNT_KEY = "specimen.cells.count";
    private static final String SPECIMEN_FLUID_COUNT_KEY = "specimen.fluid.count";
    private static final String SPECIMEN_MOLECULAR_COUNT_KEY = "specimen.molecular.count";
    private static final String SPECIMEN_TISSUE_COUNT_KEY = "specimen.tissue.count";
    private static final String PARTICIPANT_COUNT_KEY = "participant.count";
    private static final String COLLECTION_PROTOCOL_COUNT_KEY = "collection.protocol.count";
    private static final String CONSORTIUM_INFORMATION_LINK_KEY = "consortium.information.link";
    private static final String ORDER_ADMINISTRATION_LINK_KEY = "order.administration.link";
    private static final String BIOSPECIMEN_ADMINISTRATION_LINK_KEY = "biospecimen.administration.link";
    private static final String AGGREGATE_SEARCH_RESULTS_ACTIVE_KEY = "aggregate.search.results.active";
    private static final String COLLECTION_YEAR_KEY = "collectionYearFieldName";

    private static final String DISPLAY_INSTITUTION_SPECIMEN_ADMIN = "admin.specimen.institution.display";
    private static final String CONSORTIUM_MEMBER_CONTACT_INFO_REQUIRED_KEY =
        "validator.consortiumMembers.contactInformation.required";
    private static final String DISPLAY_REVIEW_VOTES_KEY = "display.review.votes";
    private static final String ACCOUNT_APPROVAL_ACTIVE_KEY = "account.approval.active";
    private static final String MTA_ACTIVE_KEY = "mta.active";
    private static final String IRB_APPROVAL_REQUIRED_KEY = "irbApproval.required";
    private static final String MINIMUM_FUNDING_STATUS_REQUIRED_KEY = "fundingStatus.minimum.required";
    private static final String SHIPMENT_RECIPIENT_INSTITUION_KEY = "shipment.recipient.institution";
    private static final String QUANITITY_TERM_KEY = "quantity.term";
    private static final String PROTOCOL_DOCUMENT_REQUIRED_KEY = "protocol.document.required";
    private static final String INSTITUTION_PROFILE_URL_ACTIVE_KEY = "institutionProfileUrl.active";
    private static final String CART_SEARCH_MORE_KEY = "cart.searchMore";
    private static final String REQUEST_LIST_INSTITUTION_RESTRICTED = "request.list.institution.restricted";
    private static final String RESEARCHER_SUPPORT_ACTIVE = "researcher.support.active";
    private static final String STATUS_HISTORY_XPATH = "status.history.xpath";
    private static final String VIEW_PAGE_XPATH = "view.page.xpath";
    private static final String DISPLAY_REQUEST_PROCESS_STEPS_KEY = "display.request.process.steps";
    private static final String DISPLAY_FUNDING_STATUS_PENDING = "display.fundingStatus.pending";
    private static final String DISPLAY_SHIPMENT_BILLING_RECIPIENT = "display.shipment.billingRecipient";
    private static final String PARTICIPANT_TERM = "participant.term";
    private static final String USER_RESUME_REQUIRED_KEY = "user.resume.required";
    private static final String RECEIPT_QUALITY_OPTION_KEY = "receipt.quality.option";
    private static final String ALTERNATE_RECEIPT_QUALITY_OPTION_KEY = "alternate.receipt.quality.option";

    private static Properties properties = new Properties();
    static {
        try {
            final InputStream stream =
                ClassLoader.getSystemClassLoader().getResourceAsStream(CLIENT_PROPERTIES_FILE_NAME);
            properties.load(stream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return the researcher institution
     */
    public static String getResearcherInstitution() {
        return properties.getProperty(RESEARCHER_INSTITUTION_KEY);
    }

    /**
     * @return the tissue tech email
     */
    public static String getTissueTechEmail() {
        return properties.getProperty(TISSUE_TECH_EMAIL_KEY);
    }

    /**
     * @return the institutional admin email
     */
    public static String getInstitutionalAdminEmail() {
        return properties.getProperty(INSTITUTIONAL_ADMIN_EMAIL_KEY);
    }

    /**
     * @return the institutional admin name
     */
    public static String getInstitutionalAdminName() {
        return properties.getProperty(INSTITUTIONAL_ADMIN_NAME_KEY);
    }

    /**
     * @return the alternate institutional admin email
     */
    public static String getAlternateInstitutionalAdminEmail() {
        return properties.getProperty(ALTERNATE_INSTITUTIONAL_ADMIN_EMAIL_KEY);
    }

    /**
     * @return the scientific reviewer email
     */
    public static String getScientificReviewerEmail() {
        return properties.getProperty(SCIENTIFIC_REVIEWER_EMAIL_KEY);
    }

    /**
     * @return the scientific reviewer name
     */
    public static String getScientificReviewerName() {
        return properties.getProperty(SCIENTIFIC_REVIEWER_NAME_KEY);
    }

    /**
     * @return the alternate scientific reviewer email
     */
    public static String getAlternateScientificReviewerEmail() {
        return properties.getProperty(ALTERNATE_SCIENTIFIC_REVIEWER_EMAIL_KEY);
    }

    /**
     * @return the alternate scientific reviewer name
     */
    public static String getAlternateScientificReviewerName() {
        return properties.getProperty(ALTERNATE_SCIENTIFIC_REVIEWER_NAME_KEY);
    }

    /**
     * @return the researcher email
     */
    public static String getResearcherEmail() {
        return properties.getProperty(RESEARCHER_EMAIL_KEY);
    }

    /**
     * @return the researcher name
     */
    public static String getResearcherName() {
        return properties.getProperty(RESEARCHER_NAME_KEY);
    }

    /**
     * @return the consortium admin email
     */
    public static String getConsortiumAdminEmail() {
        return properties.getProperty(CONSORTIUM_ADMIN_EMAIL_KEY);
    }

    /**
     * @return the super admin name
     */
    public static String getSuperadminName() {
        return properties.getProperty(SUPERADMIN_NAME);
    }

    /**
     * @return the diseases
     */
    public static String[] getDiseases() {
        return StringUtils.split(properties.getProperty(DISEASES_KEY), ",");
    }

    /**
     * @return the diseases available in a specimens search
     */
    public static String[] getSpecimenSearchDiseases() {
        return StringUtils.split(properties.getProperty(SPECIMEN_SEARCH_DISEASES_KEY), ",");
    }

    /**
     * @return the diseases available in a specimens search
     */
    public static Boolean isSpecimenSearchDiseasesFullList() {
        return Boolean.valueOf(properties.getProperty(SPECIMEN_SEARCH_FULL_LIST));
    }

    /**
     * @return the diseases
     */
    public static int[] getDiseaseCounts() {
        String[] countStrings = StringUtils.split(properties.getProperty(DISEASE_COUNTS_KEY), ",");
        int[] counts = new int[countStrings.length];
        for (int i = 0; i < countStrings.length; i++) {
            try {
                counts[i] = Integer.parseInt(countStrings[i]);
            } catch (NumberFormatException e) {
                LOG.error("error getting disease counts", e);
            }
        }
        return counts;
    }

    /**
     * @return the institutions.
     */
    public static String[] getInstitutions() {
        return StringUtils.split(properties.getProperty(INSTITUTIONS_KEY), ",");
    }

    /**
     * @return the specimen counts for each institution.
     */
    public static int[] getInstitutionCounts() {
        String[] countStrings = StringUtils.split(properties.getProperty(INSTITUTIONS_COUNTS_KEY), ",");
        int[] counts = new int[countStrings.length];
        for (int i = 0; i < countStrings.length; i++) {
            try {
                counts[i] = Integer.parseInt(countStrings[i]);
            } catch (NumberFormatException e) {
                LOG.error("error getting institution counts", e);
            }
        }
        return counts;
    }

    /**
     * @return the anatomic Source
     */
    public static String getAnatomicSource() {
        return properties.getProperty(ANATOMIC_SOURCE_KEY);
    }

    /**
     * @return the expected number of anatomic Source results in testing.
     */
    public static String getAnatomicSourceResults() {
        return properties.getProperty(ANATOMIC_SOURCE_RESULTS_KEY);
    }

    /**
     * @return the expected number of pages with anatomicSources for testing.
     */
    public static String getAnatomicSourcePages() {
        return properties.getProperty(ANATOMIC_SOURCE_PAGES_KEY);
    }

    /**
     * @return the number of default institutions
     */
    public static int getDefaultInstitutionCount() {
        return Integer.valueOf(properties.getProperty(DEFAULT_INSTITUTION_COUNT_KEY));
    }

    /**
     * @return the name of a default institution that is a consortium member
     */
    public static String getDefaultConsortiumMemberName() {
        return properties.getProperty(DEFAULT_CONSORTIUM_MEMBER_NAME_KEY);
    }

    /**
     * @return the name of a default institution that is a consortium member
     */
    public static String getSecondaryConsortiumMemberName() {
        return properties.getProperty(SECONDARY_CONSORTIUM_MEMBER_NAME_KEY);
    }

    /**
     * @return the number of default users
     */
    public static int getDefaultUserCount() {
        return Integer.valueOf(properties.getProperty(DEFAULT_USER_COUNT_KEY));
    }

    /**
     * @return the beginning of the name of the disease used for the autocompleter
     */
    public static String getAutocompleteDiseaseNameStart() {
        return properties.getProperty(AUTOCOMPLETE_DISEASE_NAME_START_KEY);
    }

    /**
     * @return the full name of the disease used for the autocompleter
     */
    public static String getAutocompleteDiseaseFullName() {
        return properties.getProperty(AUTOCOMPLETE_DISEASE_FULL_NAME_KEY);
    }

    /**
     * @return the text identifying the biospecimen search menu.
     */
    public static String getBiospecimenSearchMenu() {
        return properties.getProperty(BIOSPECIMEN_SEARCH_MENU_KEY);
    }

    /**
     * @return the text of the biospecimen search link in the menu.
     */
    public static String getBiospecimenSearchLink() {
        return properties.getProperty(BIOSPECIMEN_SEARCH_LINK_KEY);
    }

    /**
     * @return the text of the cart link in the menu.
     */
    public static String getCartLink() {
        return properties.getProperty(CART_LINK_KEY);
    }

    /**
     * @return the text of the prospective collection link in the menu.
     */
    public static String getProspectiveCollectionLink() {
        return properties.getProperty(PROSPECTIVE_COLLECTION_LINK_KEY);
    }

    /**
     * @return the text of the prospective collection notes input.
     */
    public static String getProspectiveCollectionNotes() {
        return properties.getProperty(PROSPECTIVE_COLLECTION_NOTES_KEY);
    }

    /**
     * @return the text of the prospective collection required error message.
     */
    public static String getProspectiveCollectionRequired() {
        return properties.getProperty(PROSPECTIVE_COLLECTION_REQUIRED_KEY);
    }

    /**
     * @return the term used for prospective collection.
     */
    public static String getProspectiveCollectionTerm() {
        return properties.getProperty(PROSPECTIVE_COLLECTION_TERM_KEY);
    }

    /**
     * @return whether a scientific reviewer is assigned the lead reviewer role
     */
    public static boolean isScientificReviewerLeadReviewer() {
        return SCIENTIFIC_REVIEWER_GROUP_NAME.equals(properties.getProperty(LEAD_REVIEWER_GROUP_KEY));
    }

    /**
     * @return whether consortium/scientific review is active.
     */
    public static boolean isConsortiumReviewActive() {
        return Boolean.valueOf(properties.getProperty(CONSORTIUM_REVIEW_ACTIVE_KEY));
    }

    /**
     * @return whether institutional review is active.
     */
    public static boolean isInstitutionalReviewActive() {
        return Boolean.valueOf(properties.getProperty(INSTITUTIONAL_REVIEW_ACTIVE_KEY));
    }

    /**
     * @return whether an instiutional admin is assigned the lead reviewer role
     */
    public static boolean isInstitutionalAdminLeadReviewer() {
        return INSTITUTIONAL_ADMIN_GROUP_NAME.equals(properties.getProperty(LEAD_REVIEWER_GROUP_KEY));
    }

    /**
     * @return The label for the final vote.
     */
    public static String getFinalVoteLabel() {
        return properties.getProperty(FINAL_VOTE_LABEL_KEY);
    }

    /**
     * @return the label for the final comment.
     */
    public static String getFinalCommentLabel() {
        return properties.getProperty(FINAL_COMMENT_LABEL_KEY);
    }

    /**
     * @return the index of the receipt quality column in the cart table
     */
    public static int getReceiptQualityColumnIndex() {
        return Integer.valueOf(properties.getProperty(RECEIPT_QUALITY_COLUMN_INDEX));
    }

    /**
     * @return the properties object
     */
    public static Properties getProperties() {
        return properties;
    }

    /**
     * @return whether the pi legal fields should be displayed
     */
    public static boolean getDisplayPILegalFields() {
        return Boolean.valueOf(properties.getProperty(DISPLAY_PI_LEGAL_FIELDS_KEY));
    }

    /**
     * @return whether the pi legal fields should be displayed
     */
    public static boolean getDisplayProspectiveCollection() {
        return Boolean.valueOf(properties.getProperty(DISPLAY_PROSPECTIVE_COLLECTION_KEY));
    }

    /**
     * @return whether prospective collection data is displayed in a table in the cart.
     */
    public static boolean getDisplayProspectiveCollectionTable() {
        return Boolean.valueOf(properties.getProperty(DISPLAY_PROSPECTIVE_COLLECTION_TABLE_KEY));
    }

    /**
     * @return whether prospective collection search is enabled.
     */
    public static boolean isSimpleSearchEnabled() {
        return Boolean.valueOf(properties.getProperty(SIMPLE_SEARCH_ENABLED_KEY));
    }

    /**
     * @return Title of the prospective collection search page.
     */
    public static String getSimpleSearchTitle() {
        return properties.getProperty(SIMPLE_SEARCH_TITLE_KEY);
    }

    /**
     * @return whether order administration is active in the application
     */
    public static boolean isOrderAdministrationActive() {
        return Boolean.valueOf(properties.getProperty(ORDER_ADMINISTRATION_ACTIVE_KEY));
    }

    /**
     * @return whether protocol administration is active in the application
     */
    public static boolean isProtocolAdministrationActive() {
        return Boolean.valueOf(properties.getProperty(PROTOCOL_ADMINISTRATION_ACTIVE_KEY));
    }

    /**
     * @return whether prices are displayed throughout the app
     */
    public static boolean isDisplayPrices() {
        return Boolean.valueOf(properties.getProperty(DISPLAY_PRICES_KEY));
    }

    /**
     * @return the number of cell specimens in the database
     */
    public static int getCellsSpecimenCount() {
        return Integer.valueOf(properties.getProperty(SPECIMEN_CELLS_COUNT_KEY));
    }

    /**
     * @return the number of fluid specimens in the database
     */
    public static int getFluidSpecimenCount() {
        return Integer.valueOf(properties.getProperty(SPECIMEN_FLUID_COUNT_KEY));
    }

    /**
     * @return the number of molecular specimens in the database
     */
    public static int getMolecularSpecimenCount() {
        return Integer.valueOf(properties.getProperty(SPECIMEN_MOLECULAR_COUNT_KEY));
    }

    /**
     * @return the number of fluid specimens in the database
     */
    public static int getTissueSpecimenCount() {
        return Integer.valueOf(properties.getProperty(SPECIMEN_TISSUE_COUNT_KEY));
    }

    /**
     * @return the number of participants in the database
     */
    public static int getParticipantCount() {
        return Integer.valueOf(properties.getProperty(PARTICIPANT_COUNT_KEY));
    }

    /**
     * @return the number of collection protocols in the database
     */
    public static int getCollectionProtocolCount() {
        return Integer.valueOf(properties.getProperty(COLLECTION_PROTOCOL_COUNT_KEY));
    }

    /**
     * @return the text for the link to consortium information in the menu.
     */
    public static String getConsortiumInformationLink() {
        return properties.getProperty(CONSORTIUM_INFORMATION_LINK_KEY);
    }

    /**
     * @return the text for the link to order fulfillment in the menu.
     */
    public static String getOrderAdministrationLink() {
        return properties.getProperty(ORDER_ADMINISTRATION_LINK_KEY);
    }

    /**
     * @return the text for the link to biospecimen fulfillment in the menu.
     */
    public static String getBiospecimenAdministrationLink() {
        return properties.getProperty(BIOSPECIMEN_ADMINISTRATION_LINK_KEY);
    }

    /**
     * @return the general term used for specimens.
     */
    public static String getBiospecimenTerm() {
        return properties.getProperty(BIOSPECIMEN_TERM_KEY);
    }

    /**
     * @return the general term used for biospecimens, in lowercase.
     */
    public static String getBiospecimenTermLowerCase() {
        return properties.getProperty(BIOSPECIMEN_TERM_LOWERCASE_KEY);
    }

    /**
     * @return the general term used for biospecimens.
     */
    public static String getDiseaseTerm() {
        return properties.getProperty(DISEASE_TERM_KEY);
    }

    /**
     * @return the general term used for specimens.
     */
    public static String getSpecimenTerm() {
        return properties.getProperty(SPECIMEN_TERM_KEY);
    }

    /**
     * @return the general term used for specimens, in lowercase.
     */
    public static String getSpecimenTermLowerCase() {
        return properties.getProperty(SPECIMEN_TERM_LOWERCASE_KEY);
    }

    /**
     * @return the general term used for institutes.
     */
    public static String getInstitutionTerm() {
        return properties.getProperty(INSTITUTION_TERM_KEY);
    }

    /**
     * @return the general term used for 'institutional'.
     */
    public static String getInstitutionalTerm() {
        return properties.getProperty(INSTITUTIONAL_TERM_KEY);
    }

    /**
     * @return whether specimen administrators can see the specimen institution
     */
    public static boolean isDisplayInstitutionSpecimenAdmin() {
        return Boolean.valueOf(properties.getProperty(DISPLAY_INSTITUTION_SPECIMEN_ADMIN));
    }

    /**
     * @return whether search results are aggregated
     */
    public static boolean isAggregateSearchResultsActive() {
        return Boolean.valueOf(properties.getProperty(AGGREGATE_SEARCH_RESULTS_ACTIVE_KEY));
    }

    /**
     * @return The term for normal condition.
     */
    public static String getNormalTerm() {
        return properties.getProperty(NORMAL_TERM_KEY);
    }

    /**
     * @return the collection year term.
     */
    public static String getCollectionYearTerm() {
        return properties.getProperty(COLLECTION_YEAR_KEY);
    }

    /**
     * @return The PI resume term.
     */
    public static String getInvestigatorResumeTerm() {
        return properties.getProperty(PI_RESUME_TERM_KEY);
    }

    /**
     * @return the required message for consortium member contact information.
     */
    public static String getConsortiumMemberContactInfoRequiredMessage() {
        return properties.getProperty(CONSORTIUM_MEMBER_CONTACT_INFO_REQUIRED_KEY);
    }

    /**
     * @return whether request review votes are displayed on the read only request view
     */
    public static boolean isDisplayRequestReviewVotes() {
        return Boolean.valueOf(properties.getProperty(DISPLAY_REVIEW_VOTES_KEY));
    }

    /**
     * @return whether account approval is active
     */
    public static boolean isAccountApprovalActive() {
        return Boolean.valueOf(properties.getProperty(ACCOUNT_APPROVAL_ACTIVE_KEY));
    }

    /**
     * @return whether MTAs are present.
     */
    public static boolean isMtaActive() {
        return Boolean.valueOf(properties.getProperty(MTA_ACTIVE_KEY));
    }

    /**
     * @return whether IRB approval is required.
     */
    public static boolean isIrbApprovalRequired() {
        return Boolean.valueOf(properties.getProperty(IRB_APPROVAL_REQUIRED_KEY));
    }

    /**
     * @return the minimum funding status required.
     */
    public static String getMinimumRequiredFundingStatus() {
        return properties.getProperty(MINIMUM_FUNDING_STATUS_REQUIRED_KEY);
    }

    /**
     * @return the shipment recipient institution
     */
    public static String getShipmentRecipientInstitution() {
        return properties.getProperty(SHIPMENT_RECIPIENT_INSTITUION_KEY);
    }

    /**
     * @return the general term used for available quantity.
     */
    public static String getQuantityTerm() {
        return properties.getProperty(QUANITITY_TERM_KEY);
    }

    /**
     * @return whether IRB approval is required.
     */
    public static boolean isProtocolDocumentRequired() {
        return Boolean.valueOf(properties.getProperty(PROTOCOL_DOCUMENT_REQUIRED_KEY));
    }

    /**
     * @return whether the institution profile url is active.
     */
    public static boolean isInstitutionProfileUrlActive() {
        return Boolean.valueOf(properties.getProperty(INSTITUTION_PROFILE_URL_ACTIVE_KEY));
    }

    /**
     * @return the text of the button for adding additional requests to your cart
     */
    public static String getCartSearchMore() {
        return properties.getProperty(CART_SEARCH_MORE_KEY);
    }

    /**
     * @return whether the request list is restricted by institution
     */
    public static Boolean isRequestListInstitutionRestricted() {
        return Boolean.valueOf(properties.getProperty(REQUEST_LIST_INSTITUTION_RESTRICTED));
    }

    /**
     * @return whether researcher support is active
     */
    public static boolean isResearcherSupportActive() {
        return Boolean.valueOf(properties.getProperty(RESEARCHER_SUPPORT_ACTIVE));
    }

    /**
     * @return the status history xpath
     */
    public static String getStatusHistoryXPath() {
        return properties.getProperty(STATUS_HISTORY_XPATH);
    }

    /**
     * @return the view page xpath
     */
    public static String getViewPageXPath() {
        return properties.getProperty(VIEW_PAGE_XPATH);
    }

    /**
     * @return whether the request process steps progress bar is displayed
     */
    public static boolean isDisplayRequestProcessSteps() {
        return Boolean.valueOf(properties.getProperty(DISPLAY_REQUEST_PROCESS_STEPS_KEY));
    }

    /**
     * @return whether the pending funding status is displayed.
     */
    public static boolean isDisplayFundingStatusPending() {
        return Boolean.valueOf(properties.getProperty(DISPLAY_FUNDING_STATUS_PENDING));
    }

    /**
     * @return whether the shipment billing recipient is displayed.
     */
    public static boolean isDisplayShipmentBillingRecipient() {
        return Boolean.valueOf(properties.getProperty(DISPLAY_SHIPMENT_BILLING_RECIPIENT));
    }

    /**
     * @return the term used for participant.
     */
    public static String getParticipantTerm() {
        return properties.getProperty(PARTICIPANT_TERM);
    }

    /**
     * @return whether the shipment billing recipient is displayed.
     */
    public static boolean isUserResumeRequired() {
        return Boolean.valueOf(properties.getProperty(USER_RESUME_REQUIRED_KEY));
    }

    /**
     * @return the receipt quality option to use.
     */
    public static String getReceiptQualityOption() {
        return properties.getProperty(RECEIPT_QUALITY_OPTION_KEY);
    }

    /**
     * @return the alternate receipt quality option to use.
     */
    public static String getAlternateReceiptQualityOption() {
        return properties.getProperty(ALTERNATE_RECEIPT_QUALITY_OPTION_KEY);
    }
}
