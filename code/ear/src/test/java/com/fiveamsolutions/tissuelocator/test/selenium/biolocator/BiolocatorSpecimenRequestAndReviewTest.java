/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.biolocator;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.abrc.AbrcSpecimenRequestAndReviewTest;

/**
 * @author ddasgupta
 *
 */
public class BiolocatorSpecimenRequestAndReviewTest extends AbrcSpecimenRequestAndReviewTest {

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp() throws Exception {
        super.setUp();
        runSqlScript("BiolocatorCartTest.sql");
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void addSearchCriteria() {
        selenium.type("object.customProperties['storageTemperature']", "-400");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyNotFundedMessage() {
        assertFalse(selenium.isElementPresent("xpath=//div[@id='fundingStatusLink_NOT_FUNDED']/p"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyFundedFieldVisibility(boolean visible) {
        assertEquals(visible,
                selenium.isVisible("//div[@id='wwgrp_fundingSource']/button"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateFundedFieldsFormat() {
        // do nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void enterFundedFields() {
        selenium.select("object.study.fundingSources", "label=Foundation (not peer reviewed)");
        selenium.addSelection("object.study.fundingSources", "label=Other");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyFundedFieldsCleared() {
        assertTrue(StringUtils.isBlank(selenium.getValue("object.study.fundingSources")));
        assertEquals("- Select -", selenium.getText("//div[@id='wwgrp_fundingSource']/button"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyFundedFieldValues() {
        List<String> selections = Arrays.asList(selenium.getSelectedLabels("object.study.fundingSources"));
        assertTrue(selections.contains("Foundation (not peer reviewed)"));
        assertTrue(selections.contains("Other"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void confirmFundedFieldText() {
        assertTrue(selenium.isTextPresent("Currently Funded"));
        assertTrue(selenium.isTextPresent("Yes"));
        assertTrue(selenium.isTextPresent("Foundation (not peer reviewed), Other"));
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected String getNoIrbApprovalNote() {
        return "If the research involves human subjects, it may require IRB review before the "
                + ClientProperties.getBiospecimenTermLowerCase() + "s may be provided.";
    }
}