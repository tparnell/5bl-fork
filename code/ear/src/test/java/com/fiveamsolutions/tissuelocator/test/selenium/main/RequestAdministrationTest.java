/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;


/**
 * @author ddasgupta
 *
 */
public class RequestAdministrationTest extends AbstractTissueLocatorSeleniumTest {

    private static final String ORDER_LIST_LINK = "xpath=//a[@onclick=\"toggleVisibility('ordercart');\"]";
    private static final String PROFILE_LINK_SUFFIX = "/td[1]/a[@target='_blank']";

    private static final int FULLY_PROCESSED_INDEX = 10;
    private static final int PENDING_FINAL_INDEX = 5;
    private static final int DENIED_INDEX = 3;
    private static final int APPROVED_INDEX = 4;
    private static final int PARTIALLY_APPROVED_INDEX = 6;
    private static final int PARTIALLY_PROCESSED_INDEX = 8;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "MyRequestsTest.sql";
    }

    /**
     * test the request view page under request administration.
     * @throws Exception on error
     */
    public void testRequestAdministration() throws Exception {
        loadRequestsWithOrders();
        goToRequestAdministrationPage();
        draft();
        pending();
        pendingFinalDecision();
        pendingRevision();
        denied();
        approved();
        partiallyApproved();
        partiallyProcessed();
        fullyProcessed();
    }

    private void loadRequestsWithOrders() throws Exception {
        if (ClientProperties.isOrderAdministrationActive()) {
            runSqlScript("MyRequestsTest-orders.sql");
        }
    }

    /**
     * Go to my request page.
     */
    protected void goToRequestAdministrationPage() {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Request Administration");
        assertTrue(selenium.isTextPresent("Requests"));
        assertEquals("All", selenium.getSelectedLabel("status"));
        if (ClientProperties.isOrderAdministrationActive()) {
            assertTrue(selenium.isTextPresent("1-8 of 8 Results"));
        } else {
            assertTrue(selenium.isTextPresent("1-5 of 5 Results"));
        }
    }

    /**
     * Tests draft requests.
     */
    private void draft() {
        assertFalse(Arrays.asList(selenium.getSelectOptions("status")).contains("Draft"));
    }

    /**
     * Tests pending requests.
     */
    protected void pending() {
        goToDetailsPage("Pending");
        assertTrue(selenium.isElementPresent(getId(1)));
        if (!ClientProperties.isAggregateSearchResultsActive()) {
            assertTrue(selenium.isElementPresent(getSpecimenListLink()));
            String linkText = selenium.getText(getSpecimenListLink());
            collapseSection(linkText, getCartPrefix() + "cart");
        }
        assertEquals(!ClientProperties.isAggregateSearchResultsActive(), selenium.isTextPresent("test 1"));
        assertTrue(selenium.isElementPresent(getDataRowXPath(1, 1, "unreviewed")));
        assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                selenium.isElementPresent(getDataRowXPath(1, 1, "unreviewed") + PROFILE_LINK_SUFFIX));
        assertFalse(selenium.isElementPresent(getDataRowXPath(1, 2, "unreviewed")));
        assertFalse(selenium.isElementPresent(getOrderId(1)));
        assertFalse(selenium.isElementPresent(ORDER_LIST_LINK));
        verifyReviewPage(null, null);
    }

    /**
     * Tests pending final decision requests.
     */
    protected void pendingFinalDecision() {
        goToDetailsPage("Pending Final Decision");
        assertTrue(selenium.isElementPresent(getId(PENDING_FINAL_INDEX)));
        assertNotEquals(ClientProperties.isAggregateSearchResultsActive(),
                selenium.isElementPresent(getSpecimenListLink()));
        assertEquals(!ClientProperties.isAggregateSearchResultsActive(), selenium.isTextPresent("test 7"));
        assertTrue(selenium.isElementPresent(getDataRowXPath(PENDING_FINAL_INDEX, 1, "reviewed")));
        assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                selenium.isElementPresent(getDataRowXPath(PENDING_FINAL_INDEX, 1, "reviewed") + PROFILE_LINK_SUFFIX));
        assertFalse(selenium.isElementPresent(getDataRowXPath(PENDING_FINAL_INDEX, 2, "reviewed")));
        assertFalse(selenium.isElementPresent(getOrderId(PENDING_FINAL_INDEX)));
        assertFalse(selenium.isElementPresent(ORDER_LIST_LINK));
        verifyReviewPage(null, null);
    }

    /**
     * Tests pending revisions requests.
     */
    protected void pendingRevision() {
        goToDetailsPage("Revision Requested");
        assertTrue(selenium.isElementPresent(getId(2)));
        assertNotEquals(ClientProperties.isAggregateSearchResultsActive(),
                selenium.isElementPresent(getSpecimenListLink()));
        assertEquals(!ClientProperties.isAggregateSearchResultsActive(), selenium.isTextPresent("test 2"));
        assertTrue(selenium.isElementPresent(getDataRowXPath(2, 1, "reviewed")));
        assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                selenium.isElementPresent(getDataRowXPath(2, 1, "reviewed") + PROFILE_LINK_SUFFIX));
        assertFalse(selenium.isElementPresent(getDataRowXPath(2, 2, "reviewed")));
        assertFalse(selenium.isElementPresent(getOrderId(2)));
        assertFalse(selenium.isElementPresent(ORDER_LIST_LINK));
        verifyReviewPage("Revision Requested", "please revise");
    }

    /**
     * Tests denied requests.
     */
    protected void denied() {
        goToDetailsPage("Denied");
        assertTrue(selenium.isElementPresent(getId(DENIED_INDEX)));
        assertNotEquals(ClientProperties.isAggregateSearchResultsActive(),
                selenium.isElementPresent(getSpecimenListLink()));
        assertEquals(!ClientProperties.isAggregateSearchResultsActive(), selenium.isTextPresent("test 3"));
        assertTrue(selenium.isElementPresent(getDataRowXPath(DENIED_INDEX, 1, "reviewed")));
        assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                selenium.isElementPresent(getDataRowXPath(DENIED_INDEX, 1, "reviewed") + PROFILE_LINK_SUFFIX));
        assertFalse(selenium.isElementPresent(getDataRowXPath(DENIED_INDEX, 2, "reviewed")));
        assertFalse(selenium.isElementPresent(getOrderId(DENIED_INDEX)));
        assertFalse(selenium.isElementPresent(ORDER_LIST_LINK));
        verifyReviewPage("Denied", "denied");
    }

    /**
     * Tests approved requests.
     */
    protected void approved() {
        goToDetailsPage("Approved");
        boolean orderAdminActive = ClientProperties.isOrderAdministrationActive();
        assertEquals(!orderAdminActive, selenium.isElementPresent(getId(APPROVED_INDEX)));
        assertEquals(!orderAdminActive && !ClientProperties.isAggregateSearchResultsActive(),
                selenium.isElementPresent(getSpecimenListLink()));
        if (!orderAdminActive) {
            assertEquals(!ClientProperties.isAggregateSearchResultsActive(), selenium.isTextPresent("test 6"));
            assertTrue(selenium.isElementPresent(getDataRowXPath(APPROVED_INDEX, 1, "unreviewed")));
            assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                    selenium.isElementPresent(getDataRowXPath(APPROVED_INDEX, 1, "unreviewed") + PROFILE_LINK_SUFFIX));
            assertFalse(selenium.isElementPresent(getDataRowXPath(APPROVED_INDEX, 2, "unreviewed")));
        }

        assertEquals(orderAdminActive, selenium.isElementPresent(getOrderId(APPROVED_INDEX)));
        assertEquals(orderAdminActive && !ClientProperties.isAggregateSearchResultsActive(),
                selenium.isElementPresent(ORDER_LIST_LINK));
        if (orderAdminActive) {
            if (ClientProperties.isAggregateSearchResultsActive()) {
                assertTrue(selenium.isTextPresent("note 4"));
                assertTrue(selenium.isElementPresent(getOrderRowXPath(APPROVED_INDEX, 1)));
            } else {
                assertTrue(selenium.isTextPresent("test 4"));
                assertTrue(selenium.isElementPresent(getOrderRowXPath(0, 1)));
                assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                        selenium.isElementPresent(getOrderRowXPath(0, 1) + PROFILE_LINK_SUFFIX));
                assertFalse(selenium.isElementPresent(getOrderRowXPath(0, 2)));
            }
        }
        verifyReviewPage("Approved", "approved");
    }

    /**
     * Tests partially approved requests.
     */
    protected void partiallyApproved() {
        boolean orderAdminActive = ClientProperties.isOrderAdministrationActive();
        goToDetailsPage("Partially Approved", orderAdminActive);
        if (orderAdminActive) {
            assertTrue(selenium.isElementPresent(getId(PARTIALLY_APPROVED_INDEX)));
            assertNotEquals(ClientProperties.isAggregateSearchResultsActive(),
                    selenium.isElementPresent(getSpecimenListLink()));
            assertEquals(!ClientProperties.isAggregateSearchResultsActive(), selenium.isTextPresent("test 6"));
            assertTrue(selenium.isElementPresent(getDataRowXPath(PARTIALLY_APPROVED_INDEX, 1, "reviewed")));
            String profileLink = getDataRowXPath(PARTIALLY_APPROVED_INDEX, 1, "reviewed") + PROFILE_LINK_SUFFIX;
            assertEquals(ClientProperties.isInstitutionProfileUrlActive(), selenium.isElementPresent(profileLink));
            assertFalse(selenium.isElementPresent(getDataRowXPath(PARTIALLY_APPROVED_INDEX, 2, "reviewed")));

            assertTrue(selenium.isElementPresent(getOrderId(PARTIALLY_APPROVED_INDEX)));
            if (ClientProperties.isAggregateSearchResultsActive()) {
                assertTrue(selenium.isTextPresent("note 5"));
                assertTrue(selenium.isElementPresent(getOrderRowXPath(PARTIALLY_APPROVED_INDEX, 1)));
            } else {
                assertTrue(selenium.isElementPresent(ORDER_LIST_LINK));
                assertTrue(selenium.isTextPresent("test 5"));
                assertTrue(selenium.isElementPresent(getOrderRowXPath(0, 1)));
                assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                        selenium.isElementPresent(getOrderRowXPath(0, 1) + PROFILE_LINK_SUFFIX));
                assertFalse(selenium.isElementPresent(getOrderRowXPath(0, 2)));
            }
            verifyReviewPage("Partially Approved", "partially approved");
        }
    }

    /**
     * Tests partially processed requests.
     */
    protected void partiallyProcessed() {
        boolean orderAdminActive = ClientProperties.isOrderAdministrationActive();
        goToDetailsPage("Partially Processed", orderAdminActive);
        if (orderAdminActive) {
            assertFalse(selenium.isElementPresent(getId(PARTIALLY_PROCESSED_INDEX)));
            assertFalse(selenium.isElementPresent(getSpecimenListLink()));
            assertTrue(selenium.isElementPresent(getOrderId(PARTIALLY_PROCESSED_INDEX)));
            if (ClientProperties.isAggregateSearchResultsActive()) {
                assertTrue(selenium.isTextPresent("note 8"));
                assertTrue(selenium.isElementPresent(getOrderRowXPath(PARTIALLY_PROCESSED_INDEX, 1)));
            } else {
                assertTrue(selenium.isElementPresent(ORDER_LIST_LINK));
                assertTrue(selenium.isTextPresent("test 8"));
                assertTrue(selenium.isElementPresent(getOrderRowXPath(0, 1)));
                assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                        selenium.isElementPresent(getOrderRowXPath(0, 1) + PROFILE_LINK_SUFFIX));
                assertFalse(selenium.isElementPresent(getOrderRowXPath(0, 2)));
            }
            verifyReviewPage("Partially Approved", "partially approved");
        }
    }

    /**
     * Tests fully processed requests.
     */
    protected void fullyProcessed() {
        boolean orderAdminActive = ClientProperties.isOrderAdministrationActive();
        goToDetailsPage("Fully Processed", orderAdminActive);
        if (orderAdminActive) {
            assertFalse(selenium.isElementPresent(getId(FULLY_PROCESSED_INDEX)));
            assertFalse(selenium.isElementPresent(getSpecimenListLink()));
            assertTrue(selenium.isElementPresent(getOrderId(FULLY_PROCESSED_INDEX)));
            if (ClientProperties.isAggregateSearchResultsActive()) {
                assertTrue(selenium.isTextPresent("note 10"));
                assertTrue(selenium.isElementPresent(getOrderRowXPath(FULLY_PROCESSED_INDEX, 1)));
            } else {
                assertTrue(selenium.isElementPresent(ORDER_LIST_LINK));
                assertTrue(selenium.isTextPresent("test 10"));
                assertTrue(selenium.isElementPresent(getOrderRowXPath(0, 1)));
                assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                        selenium.isElementPresent(getOrderRowXPath(0, 1) + PROFILE_LINK_SUFFIX));
                assertFalse(selenium.isElementPresent(getOrderRowXPath(0, 2)));
            }
            verifyReviewPage("Partially Approved", "partially approved");
        }
    }

    private void goToDetailsPage(String status) {
        goToDetailsPage(status, true);
    }

    private void goToDetailsPage(String status, boolean resultsExpected) {
        selenium.select("status", "label=" + status);
        waitForPageToLoad();
        if (resultsExpected) {
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertTrue(selenium.isElementPresent("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a"));
            clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a");
            assertTrue(selenium.isTextPresent("Request Review"));
        } else {
            assertTrue(selenium.isTextPresent("0 Results found."));
        }
    }

    private String getId(int specimenIndex) {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            if (specimenIndex == 1) {
                return "unreviewedspecific_title";
            } else if (specimenIndex == 2) {
                return "reviewedgeneral_title";
            } else if (specimenIndex == DENIED_INDEX) {
                return "reviewedspecific_title";
            } else if (specimenIndex == APPROVED_INDEX) {
                return "unreviewedgeneral_title";
            } else if (specimenIndex == PENDING_FINAL_INDEX) {
                return "reviewedspecific_title";
            } else if (specimenIndex == PARTIALLY_APPROVED_INDEX) {
                return "reviewedgeneral_title";
            } else if (specimenIndex == PARTIALLY_PROCESSED_INDEX) {
                return "unreviewedgeneral_title";
            } else if (specimenIndex == FULLY_PROCESSED_INDEX) {
                return "unreviewedgeneral_title";
            }
        }
        return "request";
    }

    private String getDataRowXPath(int specimenIndex, int rowIndex, String prefix) {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            if (specimenIndex % 2 == 0) {
                return "xpath=//tr[@id='" + prefix + "general_data" + (rowIndex - 1) + "']";
            }
            return "xpath=//tr[@id='" + prefix + "specific_data" + (rowIndex - 1) + "']";
        }
        return "xpath=//table[@id='request']/tbody/tr[" + rowIndex + "]";
    }

    private String getOrderId(int specimenIndex) {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            if (specimenIndex % 2 == 0) {
                return "ordergeneral_title";
            }
            return "orderspecific_title";
        }
        return "orderrequest";
    }

    private String getOrderRowXPath(int specimenIndex, int rowIndex) {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            if (specimenIndex % 2 == 0) {
                return "xpath=//tr[@id='ordergeneral_data" + (rowIndex - 1) + "']";
            }
            return "xpath=//tr[@id='orderspecific_data" + (rowIndex - 1) + "']";
        }
        return "xpath=//table[@id='orderrequest']/tbody/tr[" + rowIndex + "]";
    }

    private String getCartPrefix() {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            return "aggregate";
        }
        return "";
    }

    private String getSpecimenListLink() {
        return "xpath=//a[@onclick=\"toggleVisibility('" + getCartPrefix() + "cart');\"]";

    }

    private void verifyReviewPage(String finalVote, String finalComment) {
        clickAndWait("link=Go Back To Request Review");
        assertTrue(selenium.isTextPresent("Request Review"));
        if (ClientProperties.isDisplayRequestReviewVotes()) {
            assertFalse(selenium.isElementPresent("xpath=//h3[@class='approved']"));
            assertFalse(selenium.isElementPresent("xpath=//h3[@class='denied']"));
            assertFalse(selenium.isElementPresent("xpath=//h3[@class='']"));
            assertFalse(selenium.isElementPresent("xpath=//p[@class='comment']"));
        } else if (StringUtils.isNotBlank(finalVote) && StringUtils.isNotBlank(finalComment)) {
            assertTrue(selenium.isTextPresent("Consortium's Review Decision"));
            assertTrue(selenium.isTextPresent(ClientProperties.getFinalVoteLabel()));
            assertTrue(selenium.isTextPresent(finalVote));
            assertTrue(selenium.isTextPresent(ClientProperties.getFinalCommentLabel()));
            assertTrue(selenium.isTextPresent(finalComment));
        } else {
            assertFalse(selenium.isTextPresent("Consortium's Review Decision"));
            assertFalse(selenium.isTextPresent(ClientProperties.getFinalVoteLabel()));
            assertFalse(selenium.isTextPresent(ClientProperties.getFinalCommentLabel()));
        }
        clickAndWait("link=Back To List");
    }
}
