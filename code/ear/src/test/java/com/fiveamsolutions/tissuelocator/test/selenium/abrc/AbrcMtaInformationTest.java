/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import java.io.File;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author ddasgupta
 *
 */
public class AbrcMtaInformationTest extends AbstractListTest {

    private static final int RESULT_COUNT = 56;
    private static final int INSITITUION_SET_SIZE = 8;
    private static final String IMAGE_XPATH = "//table[@id='institution']/tbody/tr[%d]/td[3]/img";
    private static final String CELL_XPATH = "//table[@id='institution']/tbody/tr[%d]/td[%d]/@class";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "MtaInformationTest.sql";
    }

    /**
     * test the mta information page.
     * @throws Exception on error
     */
    @Test
    public void testMtaInformation() throws Exception {
        selenium.open(CONTEXT_ROOT);
        clickAndWait("link=MTA");
        verifyData(false);
        sorting(1, "institution", false);
        paging("institution", 1);
        pageSize(RESULT_COUNT);

        loginAsUser1();
        clickAndWait("link=MTA Submission");
        verifyData(true);
        sorting(1, "institution", false);
        paging("institution", 1);
        pageSize(RESULT_COUNT);

        loginAsUser1();
        clickAndWait("link=MTA Submission");
        validation();
        create();
    }

    /**
     * Test filtering mta by institution.
     */
    @Test
    public void testFilterMtaByInstitution() {
        selenium.open(CONTEXT_ROOT);
        clickAndWait("link=MTA");
        assertTrue("Institution table not found, body text: " + selenium.getBodyText(), 
                selenium.isElementPresent("institution"));
        String name = selenium.getTable("institution.1.0");
        selenium.type("name", StringUtils.substring(name, 0, name.length()));
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        selenium.type("name", StringUtils.substring(name, 2, name.length()));
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        selenium.type("name", "");
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-20 of 56 Results"));
        assertTrue(selenium.isTextPresent("1 of 3 Pages"));
    }

    private void verifyData(boolean includeUpload) {
        assertTrue(selenium.isTextPresent("Material Transfer Agreement (MTA) Information"));
        assertTrue(selenium.isTextPresent("Current version of the MTA: 01/01/2011"));
        String fileTarget = selenium.getAttribute("link=exact:Download latest ABC MTA*@href");
        assertTrue(Pattern.matches(".*/mtaDownload.action", fileTarget));
        assertTrue(selenium.isTextPresent("1-20 of 56 Results"));
        assertTrue(selenium.isTextPresent("1 of 3 Pages"));
        assertEquals(includeUpload, selenium.isElementPresent("mtaForm"));
        assertEquals(includeUpload, selenium.isElementPresent("document"));
        assertEquals(includeUpload, selenium.isElementPresent("mtaForm_btn_save"));
        for (int i = 1; i <= 2; i++) {
            int rowIndex = INSITITUION_SET_SIZE * (i - 1) + 1;
            assertEquals("test" + i + "a", selenium.getTable("institution." + rowIndex + ".0"));
            assertEquals("01/01/2011", selenium.getTable("institution." + rowIndex + ".1"));
            assertEquals("Current", selenium.getTable("institution." + rowIndex + ".2"));
            assertTrue(selenium.isElementPresent(String.format(IMAGE_XPATH, rowIndex)));
            assertEquals("inst_col", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 1)));
            assertEquals("vers_col", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2)));
            assertEquals("stat_col", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2 + 1)));
            rowIndex++;
            assertEquals("test" + i + "b", selenium.getTable("institution." + rowIndex + ".0"));
            assertEquals("01/01/2010", selenium.getTable("institution." + rowIndex + ".1"));
            assertEquals("Out-of-Date", selenium.getTable("institution." + rowIndex + ".2"));
            assertFalse(selenium.isElementPresent(String.format(IMAGE_XPATH, rowIndex)));
            assertEquals("inst_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 1)));
            assertEquals("vers_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2)));
            assertEquals("stat_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2 + 1)));
            rowIndex++;
            assertEquals("test" + i + "c", selenium.getTable("institution." + rowIndex + ".0"));
            assertEquals("01/01/2011", selenium.getTable("institution." + rowIndex + ".1"));
            assertEquals("Pending Review", selenium.getTable("institution." + rowIndex + ".2"));
            assertFalse(selenium.isElementPresent(String.format(IMAGE_XPATH, rowIndex)));
            assertEquals("inst_col pending", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 1)));
            assertEquals("vers_col pending", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2)));
            assertEquals("stat_col pending", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2 + 1)));
            rowIndex++;
            assertEquals("test" + i + "d", selenium.getTable("institution." + rowIndex + ".0"));
            assertEquals("01/01/2011", selenium.getTable("institution." + rowIndex + ".1"));
            assertEquals("Current", selenium.getTable("institution." + rowIndex + ".2"));
            assertTrue(selenium.isElementPresent(String.format(IMAGE_XPATH, rowIndex)));
            assertEquals("inst_col", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 1)));
            assertEquals("vers_col", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2)));
            assertEquals("stat_col", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2 + 1)));
            rowIndex++;
            assertEquals("test" + i + "f", selenium.getTable("institution." + rowIndex + ".0"));
            assertEquals("01/01/2011", selenium.getTable("institution." + rowIndex + ".1"));
            assertEquals("Pending Review", selenium.getTable("institution." + rowIndex + ".2"));
            assertFalse(selenium.isElementPresent(String.format(IMAGE_XPATH, rowIndex)));
            assertEquals("inst_col pending", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 1)));
            assertEquals("vers_col pending", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2)));
            assertEquals("stat_col pending", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2 + 1)));
            rowIndex++;
            assertEquals("test" + i + "g", selenium.getTable("institution." + rowIndex + ".0"));
            assertEquals("01/01/2010", selenium.getTable("institution." + rowIndex + ".1"));
            assertEquals("Out-of-Date", selenium.getTable("institution." + rowIndex + ".2"));
            assertFalse(selenium.isElementPresent(String.format(IMAGE_XPATH, rowIndex)));
            assertEquals("inst_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 1)));
            assertEquals("vers_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2)));
            assertEquals("stat_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2 + 1)));
            rowIndex++;
            assertEquals("test" + i + "h", selenium.getTable("institution." + rowIndex + ".0"));
            assertEquals("01/01/2010", selenium.getTable("institution." + rowIndex + ".1"));
            assertEquals("Out-of-Date", selenium.getTable("institution." + rowIndex + ".2"));
            assertFalse(selenium.isElementPresent(String.format(IMAGE_XPATH, rowIndex)));
            assertEquals("inst_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 1)));
            assertEquals("vers_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2)));
            assertEquals("stat_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2 + 1)));
            rowIndex++;
            assertEquals("test" + i + "i", selenium.getTable("institution." + rowIndex + ".0"));
            assertEquals("01/01/2009", selenium.getTable("institution." + rowIndex + ".1"));
            assertEquals("Out-of-Date", selenium.getTable("institution." + rowIndex + ".2"));
            assertFalse(selenium.isElementPresent(String.format(IMAGE_XPATH, rowIndex)));
            assertEquals("inst_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 1)));
            assertEquals("vers_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2)));
            assertEquals("stat_col outdated", selenium.getAttribute(String.format(CELL_XPATH, rowIndex, 2 + 1)));
        }
    }

    private void validation() {
        clickAndWait("mtaForm_btn_save");
        assertTrue(selenium.isTextPresent("Material Transfer Agreement (MTA) Information"));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("MTA File must be set"));
        assertTrue(selenium.isTextPresent("1-20 of 56 Results"));
        assertTrue(selenium.isTextPresent("1 of 3 Pages"));

        selenium.type("document", "invalid");
        clickAndWait("mtaForm_btn_save");
        assertTrue(selenium.isTextPresent("Material Transfer Agreement (MTA) Information"));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("MTA File must be set"));
        assertTrue(selenium.isTextPresent("1-20 of 56 Results"));
        assertTrue(selenium.isTextPresent("1 of 3 Pages"));
    }

    private void create() throws Exception {
        selenium.type("document",
                new File(ClassLoader.getSystemResource("MtaInformationTest.sql").toURI()).toString());
        clickAndWait("mtaForm_btn_save");
        assertTrue(selenium.isTextPresent("Material Transfer Agreement (MTA) Information"));
        assertTrue(selenium.isTextPresent("Material Transfer Agreement successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("1-20 of 57 Results"));
        assertTrue(selenium.isTextPresent("1 of 3 Pages"));

        assertEquals("Arizona Biomedical Research Commission", selenium.getTable("institution.1.0"));
        assertEquals("01/01/2011", selenium.getTable("institution.1.1"));
        assertEquals("Pending Review", selenium.getTable("institution.1.2"));
        assertFalse(selenium.isElementPresent(String.format(IMAGE_XPATH, 1)));
        assertEquals("inst_col pending", selenium.getAttribute(String.format(CELL_XPATH, 1, 1)));
        assertEquals("vers_col pending", selenium.getAttribute(String.format(CELL_XPATH, 1, 2)));
        assertEquals("stat_col pending", selenium.getAttribute(String.format(CELL_XPATH, 1, 2 + 1)));

        assertTrue(StringUtils.isBlank(selenium.getValue("document")));
    }

}
