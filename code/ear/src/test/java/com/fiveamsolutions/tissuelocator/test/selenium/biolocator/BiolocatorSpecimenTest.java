/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.biolocator;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractSpecimenTest;

/**
 * @author ddasgupta
 *
 */
public class BiolocatorSpecimenTest extends AbstractSpecimenTest {

    private static final String CUSTOM_PROPERTIES_SPECIMEN_DENSITY = "object.customProperties['specimenDensity']";
    private static final String CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING =
        "object.customProperties['timeLapseToProcessing']";
    private static final String CUSTOM_PROPERTIES_PRESERVATION_TYPE =
        "object.customProperties['preservationType']";
    private static final String CUSTOM_PROPERTIES_ANATOMIC_SOURCE =
        "object.customProperties['anatomicSource']";
    private static final String CUSTOM_PROPERTIES_COLLECTION_TYPE =
        "object.customProperties['collectionType']";
    private static final String CUSTOM_PROPERTIES_STORAGE_TEMPERATURE =
        "object.customProperties['storageTemperature']";

    private static final int[] CUSTOM_PROPERTY_VIEW_INDEXES = new int[] {4, 7, 8, 11, 12, 13, 15};

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionFormatValidationEntry() {
        setDynamicTextSearchFields("invalid");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionFormatValidationMessage() {
        assertTrue(selenium.isTextPresent("Time Lapse To Processing (minutes) is invalid."));
        assertTrue(selenium.isTextPresent("Specimen Density is invalid."));
        assertTrue(selenium.isTextPresent("Storage Temperature is invalid."));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionMinMaxValidationEntry() {
        setDynamicTextSearchFields("-20");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionMinMaxValidationMessage() {
        assertTrue(selenium.isTextPresent("Time Lapse To Processing (minutes) must be greater than or equal to 0"));
        assertTrue(selenium.isTextPresent("Specimen Density must be greater than or equal to 0."));
        selenium.type(CUSTOM_PROPERTIES_SPECIMEN_DENSITY, "120");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Specimen Density must be less than or equal to 100"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void enterExtensionData() {
        selenium.select(CUSTOM_PROPERTIES_COLLECTION_TYPE, "label=Autopsy");
        selenium.select(CUSTOM_PROPERTIES_ANATOMIC_SOURCE, "label="
                + ClientProperties.getAnatomicSource());
        selenium.select(CUSTOM_PROPERTIES_PRESERVATION_TYPE, "label=Fixed Formalin");
        selenium.type(CUSTOM_PROPERTIES_STORAGE_TEMPERATURE, "32");
        selenium.type(CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING, "25");
        selenium.type(CUSTOM_PROPERTIES_SPECIMEN_DENSITY, "50");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void viewExtensionData() {
        assertTrue(selenium.isTextPresent("Fixed Formalin"));
        assertTrue(selenium.isTextPresent(ClientProperties.getAnatomicSource()));
        assertTrue(selenium.isTextPresent("32"));
        assertTrue(selenium.isTextPresent("25"));
        assertTrue(selenium.isTextPresent("50"));
    }

    private void setDynamicTextSearchFields(String value) {
        selenium.type(CUSTOM_PROPERTIES_STORAGE_TEMPERATURE, value);
        selenium.type(CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING, value);
        selenium.type(CUSTOM_PROPERTIES_SPECIMEN_DENSITY, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int[] getExtensionViewIndexes() {
        return CUSTOM_PROPERTY_VIEW_INDEXES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setUpExpectedDetailsPanelColumns() {
        super.setUpExpectedDetailsPanelColumns();
        setExpectedDetailsPanelColumn1Label("Anatomic Source");
        setExpectedDetailsPanelColumn1Value(ClientProperties.getAnatomicSource());
        setExpectedDetailsPanelColumn3Label("Preservation Type");
        setExpectedDetailsPanelColumn3Value("Fixed Formalin");
    }

}
