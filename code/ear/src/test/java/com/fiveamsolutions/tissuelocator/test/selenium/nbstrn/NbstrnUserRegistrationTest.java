/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;

import java.io.File;

import com.fiveamsolutions.tissuelocator.test.selenium.AbstractUserRegistrationTest;

/**
 * @author ddasgupta
 *
 */
public class NbstrnUserRegistrationTest extends AbstractUserRegistrationTest {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void requiredResumeValidation() {
        assertTrue(selenium.isTextPresent("Resume/C.V./Biosketch must be set"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void requiredExtensionValidation() {
        assertTrue(selenium.isTextPresent("Institutional Signing Official must be set"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void enterResume() throws Exception {
        String biosketchHelp = "The biographical sketch, or biosketch, should follow the formatting requirements"
            + " set forth by the U.S. Department of Health and Human Services";
        verifyHelpText("aid.resume", biosketchHelp, new String[]{"Grant Application page"},
                new String[]{"http://grants.nih.gov/grants/funding/phs398/phs398.html"});
        String resumePath = new File(ClassLoader.getSystemResource("test.properties").toURI()).toString();
        assertTrue(selenium.isTextPresent("Resume/C.V./Biosketch"));
        assertTrue(selenium.isElementPresent("resume"));
        selenium.type("resume", resumePath);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void enterExtensionData() {
        assertTrue(selenium.isTextPresent("Additional Information"));
        String helpText = "The Institutional Signing Official is the official responsible for sponsored programs "
            + "at the institution where the research will be performed (generally someone from the "
            + "institution's grants and contracts office, office of research, or sponsored programs office). "
            + "The Institution may be academic, private, for-profit, non-profit, government, etc.";
        verifyHelpText("aid.institutionalOfficial", helpText, null, null);
        selenium.type("object.customProperties['institutionalOfficial']", "institutional official");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void viewResume() {
        assertTrue(selenium.isTextPresent("Resume/C.V./Biosketch"));
        assertTrue(selenium.isElementPresent("resume"));
        assertTrue(selenium.isElementPresent("link=test.properties"));
        String fileTarget = selenium.getAttribute("link=test.properties@href");
        String docUrlBase = "/protected/downloadFile";
        assertTrue(fileTarget.contains(docUrlBase));
        assertFalse(fileTarget.substring(fileTarget.lastIndexOf(docUrlBase) + docUrlBase.length()).contains("/"));
        assertTrue(fileTarget.contains("test.properties"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void viewExtensions() {
        assertTrue(selenium.isTextPresent("Institutional Signing Official"));
        assertEquals("institutional official", selenium.getValue("object.customProperties['institutionalOfficial']"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void clickRegister(boolean isRegister) {
        assertNotSame(isRegister, selenium.isEditable("btn_register"));
        if (isRegister) {
            selenium.selectFrame("termsIframe");
            selenium.click("agree");
            selenium.selectWindow(null);
        }
        assertTrue(selenium.isEditable("btn_register"));
        clickAndWait("btn_register");
    }


}
