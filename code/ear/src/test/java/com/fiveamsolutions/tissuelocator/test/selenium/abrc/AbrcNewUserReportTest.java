/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author ddasgupta
 *
 */
public class AbrcNewUserReportTest extends AbstractTissueLocatorSeleniumTest {

    private static final String USER_DATA_XPATH =
        "xpath=//div[@id='requestform']/div[@id='reportcontent']/table/tbody/tr[%d]/td[%d]/span";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "UsageReportTest.sql";
    }

    /**
     * test the usage report page.
     */
    public void testUsageReport() {
        goToMainReportPage();
        changeDates("2010");
        goToNewUserReportPage();
        verifyData();
        changeDates("2009");
        verifyNoData();
        paging();
    }

    private void goToMainReportPage() {
        loginAsAdmin();
        mouseOverAndPause("link=Reports");
        clickAndWait("link=Usage Report");
        assertTrue(selenium.isTextPresent("Reports"));
    }

    private void changeDates(String year) {
        selenium.type("startDate", "01/01/" + year);
        selenium.type("endDate", "02/01/" + year);
        clickAndWait("reportDateRangeForm_btn_save");
        assertEquals("01/01/" + year, selenium.getValue("startDate"));
        assertEquals("02/01/" + year, selenium.getValue("endDate"));
        assertTrue(selenium.isTextPresent("Report for 01/01/" + year + " - 02/01/" + year));
    }

    private void goToNewUserReportPage() {
        assertTrue(selenium.isElementPresent("link=2"));
        clickAndWait("link=2");

        assertTrue(selenium.isTextPresent("New User Report"));
        assertEquals("01/01/2010", selenium.getValue("startDate"));
        assertEquals("02/01/2010", selenium.getValue("endDate"));
        assertTrue(selenium.isTextPresent("Report for 01/01/2010 - 02/01/2010"));
    }

    private void verifyData() {
        //CHECKSTYLE:OFF - magic numbers
        String today = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
        assertEquals("Researcher Arizona", selenium.getText(String.format(USER_DATA_XPATH, 5, 2)));
        assertEquals("Institution: " + ClientProperties.getDefaultConsortiumMemberName(),
                selenium.getText(String.format(USER_DATA_XPATH, 6, 2)));
        assertEquals("Email: test-arizona-",
                selenium.getText(String.format(USER_DATA_XPATH, 7, 2)));
        assertEquals("Status: ACTIVE", selenium.getText(String.format(USER_DATA_XPATH, 7, 3)));
        assertEquals("Created On: 01/05/2010", selenium.getText(String.format(USER_DATA_XPATH, 7, 4)));
        assertEquals("Last Updated: " + today, selenium.getText(String.format(USER_DATA_XPATH, 7, 5)));
        assertEquals("Title: Unknown", selenium.getText(String.format(USER_DATA_XPATH, 8, 2)));
        assertEquals("Degree: Unknown", selenium.getText(String.format(USER_DATA_XPATH, 8, 3)));
        assertEquals("Department: Unknown", selenium.getText(String.format(USER_DATA_XPATH, 8, 4)));
        assertEquals("Research Area: cancer", selenium.getText(String.format(USER_DATA_XPATH, 8, 5)));

        assertEquals("Researcher Maryland", selenium.getText(String.format(USER_DATA_XPATH, 11, 2)));
        assertEquals("Institution: " + ClientProperties.getDefaultConsortiumMemberName(),
                selenium.getText(String.format(USER_DATA_XPATH, 12, 2)));
        assertEquals("Email: test-maryland-",
                selenium.getText(String.format(USER_DATA_XPATH, 13, 2)));
        assertEquals("Status: ACTIVE", selenium.getText(String.format(USER_DATA_XPATH, 13, 3)));
        assertEquals("Created On: 01/05/2010", selenium.getText(String.format(USER_DATA_XPATH, 13, 4)));
        assertEquals("Last Updated: " + today, selenium.getText(String.format(USER_DATA_XPATH, 13, 5)));
        assertEquals("Title: Unknown", selenium.getText(String.format(USER_DATA_XPATH, 14, 2)));
        assertEquals("Degree: Unknown", selenium.getText(String.format(USER_DATA_XPATH, 14, 3)));
        assertEquals("Department: Unknown", selenium.getText(String.format(USER_DATA_XPATH, 14, 4)));
        assertEquals("Research Area: cancer", selenium.getText(String.format(USER_DATA_XPATH, 14, 5)));
        //CHECKSTYLE:ON
    }

    private void verifyNoData() {
        assertFalse(selenium.isElementPresent("xpath=//div[@id='requestform']/div[@id='reportcontent']/table"));
    }

    private void paging() {
        int endYear = Calendar.getInstance().get(Calendar.YEAR) + 1;
        selenium.type("startDate", "01/01/2010");
        selenium.type("endDate", "01/01/" + endYear);
        clickAndWait("reportDateRangeForm_btn_save");
        assertEquals("01/01/2010", selenium.getValue("startDate"));
        assertEquals("01/01/" + endYear, selenium.getValue("endDate"));
        assertTrue(selenium.isTextPresent("Report for 01/01/2010 - 01/01/" + endYear));
        String page1Name = "Maricopa Admin";
        String page2Name = "PCH Reviewer";
        String page3Name = "Superadmin Test";

        assertTrue(selenium.isTextPresent("1 of 3 Pages"));
        assertTrue(selenium.isTextPresent(page1Name));
        assertFalse(selenium.isTextPresent(page2Name));
        assertFalse(selenium.isTextPresent(page3Name));

        clickAndWait("link=Next >>");
        assertTrue(selenium.isTextPresent("2 of 3 Pages"));
        assertFalse(selenium.isTextPresent(page1Name));
        assertTrue(selenium.isTextPresent(page2Name));
        assertFalse(selenium.isTextPresent(page3Name));

        clickAndWait("link=Next >>");
        assertTrue(selenium.isTextPresent("3 of 3 Pages"));
        assertFalse(selenium.isTextPresent(page1Name));
        assertFalse(selenium.isTextPresent(page2Name));
        assertTrue(selenium.isTextPresent(page3Name));

        clickAndWait("link=<< Prev");
        assertTrue(selenium.isTextPresent("2 of 3 Pages"));
        assertFalse(selenium.isTextPresent(page1Name));
        assertTrue(selenium.isTextPresent(page2Name));
        assertFalse(selenium.isTextPresent(page3Name));

        clickAndWait("link=<< Prev");
        assertTrue(selenium.isTextPresent("1 of 3 Pages"));
        assertTrue(selenium.isTextPresent(page1Name));
        assertFalse(selenium.isTextPresent(page2Name));
        assertFalse(selenium.isTextPresent(page3Name));

        clickAndWait("link=Last >|");
        assertTrue(selenium.isTextPresent("3 of 3 Pages"));
        assertFalse(selenium.isTextPresent(page1Name));
        assertFalse(selenium.isTextPresent(page2Name));
        assertTrue(selenium.isTextPresent(page3Name));

        clickAndWait("link=|< First");
        assertTrue(selenium.isTextPresent("1 of 3 Pages"));
        assertTrue(selenium.isTextPresent(page1Name));
        assertFalse(selenium.isTextPresent(page2Name));
        assertFalse(selenium.isTextPresent(page3Name));

        clickAndWait("link=2");
        assertTrue(selenium.isTextPresent("2 of 3 Pages"));
        assertFalse(selenium.isTextPresent(page1Name));
        assertTrue(selenium.isTextPresent(page2Name));
        assertFalse(selenium.isTextPresent(page3Name));

        clickAndWait("link=3");
        assertTrue(selenium.isTextPresent("3 of 3 Pages"));
        assertFalse(selenium.isTextPresent(page1Name));
        assertFalse(selenium.isTextPresent(page2Name));
        assertTrue(selenium.isTextPresent(page3Name));

        clickAndWait("link=1");
        assertTrue(selenium.isTextPresent("1 of 3 Pages"));
        assertTrue(selenium.isTextPresent(page1Name));
        assertFalse(selenium.isTextPresent(page2Name));
        assertFalse(selenium.isTextPresent(page3Name));
    }
}
