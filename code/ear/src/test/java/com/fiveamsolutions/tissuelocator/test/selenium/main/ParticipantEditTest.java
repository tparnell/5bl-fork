/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.text.ParseException;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author smiller
 *
 */
public class ParticipantEditTest  extends AbstractListTest {
    private static final int SPECIMEN_COL_COUNT = 2;
    private static final int RESULT_COUNT = 6;
    private static final int UNDER_REVIEW_COUNT = 2;
    private static final int SHIPPED_COUNT = 2;
    private static final int AVAILABLE_COUNT = 2;
    private static final int PAGE_SIZE = 20;
    private static final String DATE_REGEX = "[\\d]{1,2}/[\\d]{1,2}/[\\d]{4}";
    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "ParticipantEditTest.sql";
    }

    /**
     * Tests participant management.
     * @throws ParseException on error
     */
    @Test
    public void testParticipantCreateAndEdit() throws ParseException {
        int expectedResults = ClientProperties.getParticipantCount() + 2;
        verifyParticipantBiospecimens();
        verifyStatusFilters();
        verifyWithdrawConsent();
        goToParticipantPage();
        requiredValidation();
        create(expectedResults);
        duplicateValidation();
        edit(expectedResults);
        createAsTech(expectedResults + 1);
        exerciseAutocomplete();
    }

    /**
     * Navigate to participants page.
     */
    private void goToParticipantPage() {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");

        // go to list page
        clickAndWait("link=" + ClientProperties.getParticipantTerm() + " Administration");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        assertTrue(selenium.isTextPresent("Search By External"));
        String originalResultsCount = selenium.getText("xpath=//div[@class='resultscount']");

        // go to edit page
        clickAndWait("link=Add New " + ClientProperties.getParticipantTerm());
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        assertFalse(selenium.isTextPresent("Search By External"));

        // use cancel link
        clickAndWait("link=Cancel");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        assertTrue(selenium.isTextPresent("Search By External"));

        // go to edit page
        clickAndWait("link=Add New " + ClientProperties.getParticipantTerm());
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        assertFalse(selenium.isTextPresent("Search By External"));

        // use back to list link
        clickAndWait("link=Back To List");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        assertTrue(selenium.isTextPresent("Search By External"));
        assertEquals(originalResultsCount, selenium.getText("xpath=//div[@class='resultscount']"));

        // go to edit page
        clickAndWait("link=Add New " + ClientProperties.getParticipantTerm());
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        assertFalse(selenium.isTextPresent("Search By External"));
    }

    /**
     * Tests required validation.
     */
    private void requiredValidation() {
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("External ID must be set"));
        assertTrue(selenium.isTextPresent("You must select a valid "
                + ClientProperties.getInstitutionTerm().toLowerCase() + "."));
    }

    /**
     * Creates a participant.
     * @param resultCount expected results count
     */
    private void create(int resultCount) {
        enterData();
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        assertTrue(selenium.isTextPresent("Search By External"));
        resultsCount(resultCount, PAGE_SIZE);
        showOnlyTestResults();
        assertTrue(selenium.isTextPresent("test-123456789"));
        showAllResults();
    }

    private void enterData() {
        selenium.type("externalId", "test-123456789");
        String instName = StringUtils.split(ClientProperties.getDefaultConsortiumMemberName())[0].toLowerCase();
        selectFromAutocomplete("institution", instName, ClientProperties.getDefaultConsortiumMemberName());
        selenium.select("gender", "Male");
        selenium.select("ethnicity", "Not Hispanic or Latino");
        selenium.check("object.races-3");
        selenium.check("object.races-1");
        clickAndWait("btn_save");
    }

    /**
     * Tests duplicate validation.
     */
    private void duplicateValidation() {
        clickAndWait("link=Add New " + ClientProperties.getParticipantTerm());
        enterData();
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("A participant exists with this external id for this institution."));
    }


    /**
     * Edits a participant.
     * @param resultCount expected number of results
     */
    private void edit(int resultCount) {
        clickAndWait("link=Cancel");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        assertTrue(selenium.isTextPresent("Search By External"));
        resultsCount(resultCount, PAGE_SIZE);
        showOnlyTestResults();
        clickAndWait("link=test-123456789");
        assertEquals("test-123456789", selenium.getValue("externalId"));
        selenium.type("externalId", "test-123456789-updated");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " successfully saved."));
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        assertTrue(selenium.isTextPresent("Search By External"));
        showAllResults();
        resultsCount(resultCount, PAGE_SIZE);
        showOnlyTestResults();
        assertTrue(selenium.isTextPresent("test-123456789-updated"));
        clickAndWait("link=Sign Out");
    }

    private void verifyWithdrawConsent() {
        login(ClientProperties.getTissueTechEmail(), "tissueLocator1");
        mouseOverAndPause("link=Administration");
        clickAndWait("link=" + ClientProperties.getParticipantTerm() + " Administration");
        showOnlyTestResults();
        assertTrue(selenium.isElementPresent("xpath=//table[@id='participant']/tbody/tr[1]/td[1]/a"));
        clickAndWait("xpath=//table[@id='participant']/tbody/tr[1]/td[1]/a");
        for (int i = 1; i <= RESULT_COUNT; i++) {
            assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[" + i + "]/td[6]/a[1]"));
        }
        clickAndWait("xpath=//table[@id='specimens']/tbody/tr[1]/td[6]/a");
        assertTrue(selenium.isTextPresent("Consent was successfully withdrawn for the specimen"));
        assertTrue(selenium.getText("xpath=//table[@id='specimens']/tbody/tr[1]/td[4]")
                .matches(DATE_REGEX));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[1]/td[6]/a[2]"));
        String confirm = "Are you sure you want to withdraw consent for all specimens? "
            + "Click OK to withdraw consent and Cancel to remain on the current page.";
        selenium.chooseCancelOnNextConfirmation();
        selenium.click("link=Withdraw Consent");
        assertEquals(confirm, selenium.getConfirmation());
        assertFalse(selenium.isTextPresent("Consent was successfully withdrawn for all applicable specimens"));
        selenium.chooseOkOnNextConfirmation();
        selenium.click("link=Withdraw Consent");
        assertEquals(confirm, selenium.getConfirmation());
        waitForPageToLoad();
        waitForText("Consent was successfully withdrawn for all applicable specimens");
        for (int i = 1; i <= RESULT_COUNT; i++) {
            assertFalse(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[" + i + "]/td[6]/a[2]"));
            assertTrue(selenium.getText("xpath=//table[@id='specimens']/tbody/tr[" + i + "]/td[4]")
                    .matches(DATE_REGEX));
        }
        clickAndWait("xpath=//table[@id='specimens']/tbody/tr[1]/td[6]/a");
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent("Set Returned to Source Institution Date"));
        selenium.type("statusTransitionDate", "01/01/2020");
        clickAndWait("btn_returnToSource");
        pause(LONG_DELAY);
        assertTrue(selenium.isTextPresent("The specimen was successfully marked as returned"
                + " to the source institution"));
        assertEquals("01/01/2020", selenium.getText("xpath=//table[@id='specimens']/tbody/tr[1]/td[5]"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[1]/td[6]/a[1]"));
        clickAndWait("link=Sign Out");
    }

    /**
     * Create a participant as a tissue tech.
     * @param resultCount expected number of results
     */
    private void createAsTech(int resultCount) {
        login(ClientProperties.getTissueTechEmail(), "tissueLocator1");
        mouseOverAndPause("link=Administration");
        clickAndWait("link=" + ClientProperties.getParticipantTerm() + " Administration");
        clickAndWait("link=Add New " + ClientProperties.getParticipantTerm());
        selenium.type("externalId", "test-tech");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        resultsCount(resultCount, PAGE_SIZE);
        showOnlyTestResults();
        assertTrue(selenium.isTextPresent("test-tech"));
        clickAndWait("link=Sign Out");
    }

    /**
     * Verify participant biospecimens.
     * @throws ParseException on error
     */
    private void verifyParticipantBiospecimens() throws ParseException {
        login(ClientProperties.getTissueTechEmail(), "tissueLocator1");
        mouseOverAndPause("link=Administration");
        clickAndWait("link=" + ClientProperties.getParticipantTerm() + " Administration");
        showOnlyTestResults();

        assertTrue(selenium.isElementPresent("xpath=//table[@id='participant']/tbody/tr[1]/td[1]/a"));
        clickAndWait("xpath=//table[@id='participant']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " " 
                + ClientProperties.getBiospecimenTerm() + "s"));
        collapseSection(ClientProperties.getParticipantTerm() + " " 
                + ClientProperties.getBiospecimenTerm() + "s", "participant_specimens");
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[1]/td[1]/a"));
        assertEquals(ClientProperties.isOrderAdministrationActive(),
                selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[1]/td[3]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[2]/td[1]/a"));
        assertEquals(ClientProperties.isOrderAdministrationActive(),
                selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[2]/td[3]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[3]/td[1]/a"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[3]/td[3]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[4]/td[1]/a"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[4]/td[3]/a"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[7]/td[1]/a"));

        String[] specimenCellContents = selenium.getTable("specimens.1.0").split("\\s+");
        String disease = specimenCellContents[specimenCellContents.length - 1];
        clickAndWait("xpath=//table[@id='specimens']/tbody/tr[1]/td[1]/a");
        String header = selenium.getText("//div[@id='content']/h1");
        assertTrue(header.contains(disease));
        clickAndWait("link=Back");

        if (ClientProperties.isOrderAdministrationActive()) {
            clickAndWait("xpath=//table[@id='specimens']/tbody/tr[1]/td[3]/a");
            assertTrue(selenium.isTextPresent("Shipping Information"));
            clickAndWait("link=Back");

            clickAndWait("xpath=//table[@id='specimens']/tbody/tr[2]/td[3]/a");
            assertTrue(selenium.isTextPresent("Shipping Information"));
            clickAndWait("link=Back");
        }
        clickAndWait("link=Sign Out");

        login(ClientProperties.getInstitutionalAdminEmail(), "tissueLocator1");
        mouseOverAndPause("link=Administration");
        clickAndWait("link=" + ClientProperties.getParticipantTerm() + " Administration");
        showOnlyTestResults();

        assertTrue(selenium.isElementPresent("xpath=//table[@id='participant']/tbody/tr[1]/td[1]/a"));
        clickAndWait("xpath=//table[@id='participant']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " " 
                + ClientProperties.getBiospecimenTerm() + "s"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[3]/td[1]/a"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[3]/td[3]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[4]/td[1]/a"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[4]/td[3]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[5]/td[1]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[5]/td[3]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[6]/td[1]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[6]/td[3]/a"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimens']/tbody/tr[7]/td[1]/a"));

        specimenCellContents = selenium.getTable("specimens.5.0").split("\\s+");
        disease = specimenCellContents[specimenCellContents.length - 1];
        clickAndWait("xpath=//table[@id='specimens']/tbody/tr[5]/td[1]/a");
        assertTrue(selenium.isTextPresent(disease));
        clickAndWait("link=Back");

        clickAndWait("xpath=//table[@id='specimens']/tbody/tr[5]/td[3]/a");
        assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + "s in Request"));
        clickAndWait("link=Back");

        clickAndWait("xpath=//table[@id='specimens']/tbody/tr[6]/td[3]/a");
        assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + "s in Request"));
        clickAndWait("link=Back");

        sorting(SPECIMEN_COL_COUNT, "specimens", false);
        clickAndWait("link=Back To List");
    }

    /**
     * Tests status filters.
     */
    private void verifyStatusFilters() {
        assertTrue(selenium.isElementPresent("xpath=//table[@id='participant']/tbody/tr[1]/td[1]/a"));
        clickAndWait("xpath=//table[@id='participant']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " " 
                + ClientProperties.getBiospecimenTerm() + "s"));
        resultsCount(RESULT_COUNT, PAGE_SIZE);
        assertEquals("All", selenium.getSelectedLabel("status"));
        verifyStatus("Unavailable", 0);
        verifyStatus("Under Review", UNDER_REVIEW_COUNT);
        verifyStatus("Shipped", SHIPPED_COUNT);
        verifyStatus("Pending Shipment", 0);
        verifyStatus("Returned to Source Institution", 0);
        verifyStatus("Destroyed", 0);
        verifyStatus("Available", AVAILABLE_COUNT);
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        resultsCount(RESULT_COUNT, PAGE_SIZE);
        clickAndWait("link=Sign Out");
    }

    private void verifyStatus(String status, int resultCount) {
        selenium.select("status", "label=" + status);
        waitForPageToLoad();
        if (resultCount > 0) {
            assertEquals(status, selenium.getSelectedLabel("status"));
            assertEquals(status, selenium.getTable("specimens.1.1"));
            resultsCount(resultCount, PAGE_SIZE);
        } else {
            assertTrue(selenium.isTextPresent("Nothing found to display."));
            assertTrue(selenium.isTextPresent("0 Results found."));
        }
    }

    private void exerciseAutocomplete() {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");
        clickAndWait("link=" + ClientProperties.getParticipantTerm() + " Administration");
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));
        clickAndWait("link=Add New " + ClientProperties.getParticipantTerm());
        assertTrue(selenium.isTextPresent(ClientProperties.getParticipantTerm() + " Administration"));

        String optionListXPath = "xpath=//ul[@role]";
        String option1XPath = optionListXPath + "/li[1]";
        String option2XPath = optionListXPath + "/li[2]";
        //verify clicking arrow shows list
        selenium.click("institutionArrow");
        pause(LONG_DELAY);
        assertTrue(selenium.isElementPresent(optionListXPath));
        assertTrue(selenium.isVisible(optionListXPath));
        assertTrue(selenium.isElementPresent(option1XPath));
        assertTrue(selenium.isElementPresent(option2XPath));

        //verify clicking arrow hides list
        selenium.click("institutionArrow");
        pause(DELAY);
        assertTrue(selenium.isElementPresent(optionListXPath));
        assertFalse(selenium.isVisible(optionListXPath));

        //verify clicking arrow after typing in field shows limited options
        String instName = ClientProperties.getShipmentRecipientInstitution().toLowerCase();
        selenium.type("institution", instName);
        pause(LONG_DELAY);
        assertTrue(selenium.isElementPresent(optionListXPath));
        if (selenium.isVisible(optionListXPath)) {
            selenium.click("institutionArrow");
            pause(DELAY);
        }
        selenium.click("institutionArrow");
        pause(LONG_DELAY);
        assertTrue(selenium.isElementPresent(optionListXPath));
        assertTrue(selenium.isVisible(optionListXPath));
        assertTrue(selenium.isElementPresent(option1XPath));
        assertFalse(selenium.isElementPresent(option2XPath));
        selenium.click("institutionArrow");

        //verify pressing down arrow shows list
        selenium.type("institution", "");
        pause(DELAY);
        selenium.keyDown("institution", "\\40");
        selenium.keyUp("institution", "\\40");
        pause(LONG_DELAY);
        assertTrue(selenium.isElementPresent(optionListXPath));
        assertTrue(selenium.isVisible(optionListXPath));
        assertTrue(selenium.isElementPresent(option1XPath));
        assertTrue(selenium.isElementPresent(option2XPath));
        selenium.click("institutionArrow");

        //type autocomplete tab to select (instead of click)
        String message = "You must select a valid " + ClientProperties.getInstitutionTerm().toLowerCase() + ".";
        selenium.type("institution", "");
        pause(DELAY);
        selenium.typeKeys("institution", instName);
        pause(LONG_DELAY);
        selenium.keyDown("institution", "\\9");
        selenium.keyUp("institution", "\\9");
        pause(SHORT_DELAY);
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertFalse(selenium.isTextPresent(message));

        //delete to clear selection
        selenium.type("institution", "");
        pause(DELAY);
        selenium.type("institution", instName);
        selenium.keyDown("institution", "\\127");
        selenium.keyUp("institution", "\\127");
        pause(DELAY);
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(message));

        //type autocomplete click away to select only option
        selenium.type("institution", ClientProperties.getShipmentRecipientInstitution());
        pause(DELAY);
        selenium.keyDown("institution", "\\40");
        selenium.keyUp("institution", "\\40");
        pause(LONG_DELAY);
        assertTrue(selenium.isElementPresent(optionListXPath));
        assertTrue(selenium.isVisible(optionListXPath));
        assertTrue(selenium.isElementPresent(option1XPath));
        assertFalse(selenium.isElementPresent(option2XPath));
        selenium.select("gender", "Male");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertFalse(selenium.isTextPresent(message));

        //backspace to clear selection
        selenium.type("institution", "");
        pause(DELAY);
        selenium.type("institution", instName);
        selenium.keyDown("institution", "\\8");
        selenium.keyUp("institution", "\\8");
        pause(DELAY);
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(message));
        clickAndWait("link=Sign Out");
    }
}
