/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;

import java.text.ParseException;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author ddasgupta
 *
 */
public class NbstrnQuestionListTest extends AbstractListTest {

    private static final int COL_COUNT = 7;
    private static final int[] DATE_COLS = {5, 6};
    private static final int[] SKIP_COLS = {4};
    private static final int RESULT_COUNT = 60;
    private static final int PENDING_COUNT = 20;
    private static final int PARTIALLY_RESPONDED_COUNT = 20;
    private static final int FULL_RESPONDED_COUNT = 20;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "QuestionListTest.sql";
    }

    /**
     * test the administrator's support letter request list page.
     * @throws ParseException on error
     */
    public void testAdminList() throws ParseException {
        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        goToAdminListPage();
        adminStatusFilter();
        adminFilterSaved();
        adminTabs();
        sorting(COL_COUNT, "question", false, SKIP_COLS, DATE_COLS);
        paging("question");
        pageSize(RESULT_COUNT);
    }

    private void goToAdminListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Request Administration");
        waitForPageToLoad();
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("DBS Specimens"));
        clickAndWait("link=Questions to States");
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isElementPresent("status"));
        assertTrue(selenium.isTextPresent("Action"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='question']/thead/tr/th[8]"));
    }

    private void adminStatusFilter() {
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
        verifyAdminStatus("Pending", PENDING_COUNT);
        verifyAdminStatus("Partially Responded", PARTIALLY_RESPONDED_COUNT);
        verifyAdminStatus("Fully Responded", FULL_RESPONDED_COUNT);
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
    }

    private void verifyAdminStatus(String status, int resultCount) {
        selenium.select("status", "label=" + status);
        waitForPageToLoad();
        assertEquals(status, selenium.getSelectedLabel("status"));
        assertEquals(status, selenium.getTable("question.1.6"));
        assertTrue(selenium.isTextPresent("1-" + resultCount + " of " + resultCount + " Results"));

        assertTrue(selenium.isElementPresent("link=Review"));
        String buttonXPath = "xpath=//table[@id='question']/tbody/tr[%s]/td[8]/a[%s]";
        for (int i = 1; i <= resultCount; i++) {
            assertTrue(selenium.isElementPresent(String.format(buttonXPath, i, 1)));
        }
    }

    private void adminFilterSaved() {
        selenium.select("status", "label=Pending");
        waitForPageToLoad();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        clickAndWait("link=Home");
        goToAdminListPage();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
    }

    private void adminTabs() {
        assertTrue(selenium.getTitle().contains("Requests"));
        assertFalse(selenium.getTitle().contains("My Requests"));
        assertFalse(selenium.isElementPresent("supportLetterRequest"));
        assertTrue(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=Letters of Support");
        assertTrue(selenium.getTitle().contains("Requests"));
        assertFalse(selenium.getTitle().contains("My Requests"));
        assertTrue(selenium.isElementPresent("supportLetterRequest"));
        assertFalse(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=Questions to States");
        clickAndWait("link=DBS Specimens");
        assertTrue(selenium.getTitle().contains("Requests"));
        assertFalse(selenium.getTitle().contains("My Requests"));
        assertFalse(selenium.isElementPresent("supportLetterRequest"));
        assertFalse(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertTrue(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=Questions to States");
        assertTrue(selenium.getTitle().contains("Requests"));
        assertFalse(selenium.getTitle().contains("My Requests"));
        assertFalse(selenium.isElementPresent("supportLetterRequest"));
        assertTrue(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
    }

    /**
     * test the investigator's support letter request list page.
     */
    public void testUserList() {
        loginAsUser1();
        goToUserListPage();
        userStatusFilter();
        userFilterSaved();
        userTabs();
    }

    private void goToUserListPage() {
        clickAndWait("link=My Requests");
        assertTrue(selenium.isTextPresent("My Requests"));
        assertTrue(selenium.isTextPresent("DBS Specimens"));
        clickAndWait("link=Questions to States");
        assertTrue(selenium.isTextPresent("Questions to States"));
        assertTrue(selenium.isElementPresent("status"));
        assertFalse(selenium.isTextPresent("Action"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='question']/thead/tr/th[8]"));
    }

    private void userStatusFilter() {
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-6 of 6 Results"));
        verifyUserStatus("Pending", 2);
        verifyUserStatus("Partially Received", 2);
        verifyUserStatus("Fully Received", 2);
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-6 of 6 Results"));
    }

    private void verifyUserStatus(String status, int resultCount) {
        selenium.select("status", "label=" + status);
        waitForPageToLoad();
        assertEquals(status, selenium.getSelectedLabel("status"));
        assertEquals(status, selenium.getTable("question.1.6"));
        assertTrue(selenium.isTextPresent("1-" + resultCount + " of " + resultCount + " Results"));
        assertFalse(selenium.isElementPresent("link=Review"));
    }

    private void userFilterSaved() {
        selenium.select("status", "label=Pending");
        waitForPageToLoad();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        clickAndWait("link=Home");
        goToUserListPage();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
    }

    private void userTabs() {
        assertTrue(selenium.getTitle().contains("My Requests"));
        assertFalse(selenium.isElementPresent("supportLetterRequest"));
        assertTrue(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=Letters of Support");
        assertTrue(selenium.getTitle().contains("My Requests"));
        assertTrue(selenium.isElementPresent("supportLetterRequest"));
        assertFalse(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=Questions to States");
        clickAndWait("link=DBS Specimens");
        assertTrue(selenium.getTitle().contains("My Requests"));
        assertFalse(selenium.isElementPresent("supportLetterRequest"));
        assertFalse(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertTrue(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=Questions to States");
        assertTrue(selenium.getTitle().contains("My Requests"));
        assertFalse(selenium.isElementPresent("supportLetterRequest"));
        assertTrue(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-6 of 6 Results"));
    }
}
