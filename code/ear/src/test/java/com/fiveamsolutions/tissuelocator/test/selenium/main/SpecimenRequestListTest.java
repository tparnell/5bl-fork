/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.text.ParseException;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author ddasgupta
 *
 */
public class SpecimenRequestListTest extends AbstractListTest {

    private static final int COL_COUNT = 6;
    private static final int[] DATE_COLS = {4, 5};
    private static final int RESULT_COUNT = 69;
    private static final int PENDING_COUNT = 12;
    private static final int REVISION_COUNT = 11;
    private static final int APPROVED_COUNT = 10;
    private static final int DENIED_COUNT = 9;
    private static final int PARTIALLY_APPROVED_COUNT = 8;
    private static final int PENDING_FINAL_DECISION_COUNT = 7;
    private static final int PARTIALLY_PROCESSED_COUNT = 6;
    private static final int FULLY_PROCESSED_COUNT = 6;
    private static final int USER_RESULT_COUNT = 9;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "SpecimenRequestListTest.sql";
    }

    /**
     * test the specimen request administration list page.
     * @throws ParseException on error
     */
    public void testAdminList() throws ParseException {
        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        goToAdminListPage();
        adminStatusFilter();
        consortiumAdminTabs();
        verifyExportPresent();
        scientificReviewerActionRequired();
        instAdminActionRequired();
        filterSaved();
        instAdminTabs();
        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        goToAdminListPage();
        backToList("1-20 of " + RESULT_COUNT + " Results");
        sorting(COL_COUNT, "specimenRequest", true, DATE_COLS);
        paging("specimenRequest");
        pageSize(RESULT_COUNT);
    }

    private void adminStatusFilter() {
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
        verifyStatus("Revision Requested", REVISION_COUNT, true);
        verifyStatus("Approved", APPROVED_COUNT, false);
        verifyStatus("Denied", DENIED_COUNT, false);
        verifyStatus("Pending", PENDING_COUNT, false);
        verifyStatus("Partially Approved", PARTIALLY_APPROVED_COUNT, false);
        verifyStatus("Pending Final Decision", PENDING_FINAL_DECISION_COUNT, false);
        verifyStatus("Partially Processed", PARTIALLY_PROCESSED_COUNT, false);
        verifyStatus("Fully Processed", FULLY_PROCESSED_COUNT, false);
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
    }

    private void verifyStatus(String status, int resultCount, boolean showEditButton) {
        selenium.select("status", "label=" + status);
        waitForPageToLoad();
        assertEquals(status, selenium.getSelectedLabel("status"));
        assertEquals(status, selenium.getTable("specimenRequest.1.5"));
        assertTrue(selenium.isTextPresent("1-" + resultCount + " of " + resultCount + " Results"));
        assertTrue(selenium.isElementPresent("link=View"));
        String name = ClientProperties.getScientificReviewerName();
        assertEquals(selenium.isTextPresent(name) && showEditButton, selenium.isElementPresent("link=Edit"));
        String buttonXPath = "xpath=//table[@id='specimenRequest']/tbody/tr[%s]/td[8]/a[%s]";
        for (int i = 1; i <= resultCount; i++) {
            boolean sameRequestor = selenium.getTable("specimenRequest." + i + ".1").contains(name);
            boolean hasButton = selenium.isElementPresent(String.format(buttonXPath, i, 1));
            if (showEditButton) {
                assertTrue(hasButton);
            } else {
                assertEquals(!sameRequestor, hasButton);
            }

        }
    }

    private void scientificReviewerActionRequired() {
        boolean isLeadReviewer = ClientProperties.isScientificReviewerLeadReviewer();
        assertFalse(selenium.isChecked("actionNeededOnly"));
        selenium.click("actionNeededOnly");
        waitForPageToLoad();
        assertTrue(selenium.isChecked("actionNeededOnly"));
        if (ClientProperties.isConsortiumReviewActive()) {
            assertEquals(isLeadReviewer, selenium.isTextPresent("Lead Review"));
            assertTrue(selenium.isTextPresent("Consortium Review"));
            assertEquals(isLeadReviewer, selenium.isTextPresent("Final Comment"));
            if (isLeadReviewer) {
                assertTrue(selenium.isTextPresent("1-3 of 3 Results"));
                assertEquals("Consortium Review", selenium.getTable("specimenRequest.1.6"));
                assertEquals("Lead Review", selenium.getTable("specimenRequest.2.6"));
                assertEquals("Final Comment", selenium.getTable("specimenRequest.3.6"));
            } else {
                assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
                assertEquals("Consortium Review", selenium.getTable("specimenRequest.1.6"));
            }
        } else {
            assertTrue(selenium.isTextPresent("0 Results found."));
            assertTrue(selenium.isTextPresent("Nothing found to display."));
            assertFalse(selenium.isTextPresent("Lead Review"));
            assertFalse(selenium.isTextPresent("Consortium Review"));
            assertFalse(selenium.isTextPresent("Final Comment"));
        }
        assertFalse(selenium.isTextPresent("Assign Reviewer"));
        assertFalse(selenium.isTextPresent(ClientProperties.getInstitutionalTerm() + " Review"));

        selenium.select("status", "label=Pending");
        waitForPageToLoad();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        if (ClientProperties.isConsortiumReviewActive()) {
            if (isLeadReviewer) {
                assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
                assertEquals("Consortium Review", selenium.getTable("specimenRequest.1.6"));
                assertEquals("Lead Review", selenium.getTable("specimenRequest.2.6"));
            } else {
                assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
                assertEquals("Consortium Review", selenium.getTable("specimenRequest.1.6"));
            }
        } else {
            assertTrue(selenium.isTextPresent("0 Results found."));
            assertTrue(selenium.isTextPresent("Nothing found to display."));
            assertFalse(selenium.isTextPresent("Lead Review"));
            assertFalse(selenium.isTextPresent("Consortium Review"));
        }

        selenium.select("status", "label=Pending Final Decision");
        waitForPageToLoad();
        assertEquals("Pending Final Decision", selenium.getSelectedLabel("status"));
        if (isLeadReviewer && ClientProperties.isConsortiumReviewActive()) {
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertEquals("Final Comment", selenium.getTable("specimenRequest.1.6"));
        } else {
            assertTrue(selenium.isTextPresent("0 Results found."));
            assertTrue(selenium.isTextPresent("Nothing found to display."));
            assertFalse(selenium.isTextPresent("Final Comment"));
        }

        selenium.select("status", "label=Approved");
        waitForPageToLoad();
        assertEquals("Approved", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("0 Results found."));
        assertTrue(selenium.isTextPresent("Nothing found to display."));

        clickAndWait("link=Home");
        assertEquals(ClientProperties.isConsortiumReviewActive(), selenium.isTextPresent("1 Consortium Review"));
        if (isLeadReviewer) {
            assertEquals(ClientProperties.isConsortiumReviewActive(), selenium.isTextPresent("1 Lead Review"));
            assertEquals(ClientProperties.isConsortiumReviewActive(), selenium.isTextPresent("1 Final Comment"));
        }
        clickAndWait("link=Sign Out");
    }

    private void instAdminActionRequired() {
        boolean isLeadReviewer = ClientProperties.isInstitutionalAdminLeadReviewer();
        login(ClientProperties.getInstitutionalAdminEmail(), "tissueLocator1");
        assertEquals(ClientProperties.isInstitutionalReviewActive(),
                selenium.isTextPresent("1 " + ClientProperties.getInstitutionalTerm() + " Review"));
        if (ClientProperties.isConsortiumReviewActive()) {
            assertTrue(selenium.isTextPresent("1 Reviewer Assignment"));
            if (isLeadReviewer) {
                assertTrue(selenium.isTextPresent("8 Final Comments"));
            }
        }
        goToAdminListPage();
        assertFalse(selenium.isChecked("actionNeededOnly"));
        selenium.click("actionNeededOnly");
        waitForPageToLoad();
        assertTrue(selenium.isChecked("actionNeededOnly"));
        assertFalse(selenium.isTextPresent("Lead Review"));
        assertFalse(selenium.isTextPresent("Consortium Review"));
        assertEquals(ClientProperties.isInstitutionalReviewActive(),
                selenium.isTextPresent(ClientProperties.getInstitutionalTerm() + " Review"));
        if (ClientProperties.isInstitutionalReviewActive()) {
            if (ClientProperties.isConsortiumReviewActive()) {
                assertTrue(selenium.isTextPresent("Assign Reviewer"));
                assertEquals(isLeadReviewer, selenium.isTextPresent("Final Comment"));
                assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
                assertEquals("Assign Reviewer", selenium.getTable("specimenRequest.1.6"));
                assertEquals(ClientProperties.getInstitutionalTerm() + " Review",
                        selenium.getTable("specimenRequest.2.6"));
            } else {
                assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
                assertEquals(ClientProperties.getInstitutionalTerm() + " Review",
                        selenium.getTable("specimenRequest.1.6"));
            }
        } else {
            if (ClientProperties.isConsortiumReviewActive()) {
                assertTrue(selenium.isTextPresent("Assign Reviewer"));
                assertEquals(isLeadReviewer, selenium.isTextPresent("Final Comment"));
                if (isLeadReviewer) {
                    assertTrue(selenium.isTextPresent("1-9 of 9 Results"));
                    for (int i = 1; i < PENDING_FINAL_DECISION_COUNT; i++) {
                        assertEquals("Final Comment", selenium.getTable("specimenRequest." + i + ".6"));
                    }
                    assertEquals("Assign Reviewer", selenium.getTable("specimenRequest.7.6"));
                    assertEquals("Final Comment", selenium.getTable("specimenRequest.8.6"));
                    assertEquals("Final Comment", selenium.getTable("specimenRequest.9.6"));
                } else {
                    assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
                    assertEquals("Assign Reviewer", selenium.getTable("specimenRequest.1.6"));
                }
            } else {
                assertTrue(selenium.isTextPresent("0 Results found."));
                assertTrue(selenium.isTextPresent("Nothing found to display."));
                assertFalse(selenium.isTextPresent("Final Comment"));
                assertFalse(selenium.isTextPresent("Assign Reviewer"));
            }
        }

        selenium.select("status", "label=Pending");
        waitForPageToLoad();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        if (ClientProperties.isInstitutionalReviewActive()) {
            if (ClientProperties.isConsortiumReviewActive()) {
                assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
                assertEquals("Assign Reviewer", selenium.getTable("specimenRequest.1.6"));
                assertEquals(ClientProperties.getInstitutionalTerm() + " Review",
                        selenium.getTable("specimenRequest.2.6"));
            } else {
                assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
                assertEquals(ClientProperties.getInstitutionalTerm() + " Review",
                        selenium.getTable("specimenRequest.1.6"));
            }
        } else {
            if (ClientProperties.isConsortiumReviewActive()) {
                assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
                assertEquals("Assign Reviewer", selenium.getTable("specimenRequest.1.6"));
            } else {
                assertTrue(selenium.isTextPresent("0 Results found."));
                assertTrue(selenium.isTextPresent("Nothing found to display."));
            }
        }

        selenium.select("status", "label=Pending Final Decision");
        waitForPageToLoad();
        assertEquals("Pending Final Decision", selenium.getSelectedLabel("status"));
        if (isLeadReviewer && ClientProperties.isConsortiumReviewActive()) {
            assertTrue(selenium.isTextPresent("1-8 of 8 Results"));
            for (int i = 1; i < PENDING_FINAL_DECISION_COUNT + 2; i++) {
                assertEquals("Final Comment", selenium.getTable("specimenRequest." + i + ".6"));
            }
        } else {
            assertTrue(selenium.isTextPresent("0 Results found."));
            assertTrue(selenium.isTextPresent("Nothing found to display."));
            assertFalse(selenium.isTextPresent("Final Comment"));
        }

        selenium.select("status", "label=Approved");
        waitForPageToLoad();
        assertEquals("Approved", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("0 Results found."));
        assertTrue(selenium.isTextPresent("Nothing found to display."));

        selenium.click("actionNeededOnly");
        waitForPageToLoad();
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertFalse(selenium.isChecked("actionNeededOnly"));
        if (ClientProperties.isRequestListInstitutionRestricted()) {
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        } else {
            assertTrue(selenium.isTextPresent("1 of 4 Pages"));
        }
    }

    private void filterSaved() {
        assertFalse(selenium.isChecked("actionNeededOnly"));
        selenium.click("actionNeededOnly");
        waitForPageToLoad();
        assertTrue(selenium.isChecked("actionNeededOnly"));
        selenium.select("status", "label=Pending");
        waitForPageToLoad();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        clickAndWait("link=Home");
        goToAdminListPage();
        assertTrue(selenium.isChecked("actionNeededOnly"));
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        selenium.click("actionNeededOnly");
        waitForPageToLoad();
        assertFalse(selenium.isChecked("actionNeededOnly"));
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertFalse(selenium.isChecked("actionNeededOnly"));
        assertEquals("All", selenium.getSelectedLabel("status"));
    }

    private void consortiumAdminTabs() {
        if (ClientProperties.isResearcherSupportActive()) {
            assertTrue(selenium.getTitle().contains("Requests"));
            assertFalse(selenium.getTitle().contains("My Requests"));
            assertFalse(selenium.isElementPresent("supportLetterRequest"));
            assertFalse(selenium.isElementPresent("question"));
            assertFalse(selenium.isElementPresent("questionResponse"));
            assertTrue(selenium.isElementPresent("specimenRequest"));

            clickAndWait("link=Letters of Support");
            assertTrue(selenium.getTitle().contains("Requests"));
            assertFalse(selenium.getTitle().contains("My Requests"));
            assertTrue(selenium.isElementPresent("supportLetterRequest"));
            assertFalse(selenium.isElementPresent("question"));
            assertFalse(selenium.isElementPresent("questionResponse"));
            assertFalse(selenium.isElementPresent("specimenRequest"));

            clickAndWait("link=* Specimens");
            clickAndWait("link=Questions to *");
            assertTrue(selenium.getTitle().contains("Requests"));
            assertFalse(selenium.getTitle().contains("My Requests"));
            assertFalse(selenium.isElementPresent("supportLetterRequest"));
            assertTrue(selenium.isElementPresent("question"));
            assertFalse(selenium.isElementPresent("questionResponse"));
            assertFalse(selenium.isElementPresent("specimenRequest"));

            clickAndWait("link=* Specimens");
            assertFalse(selenium.isChecked("actionNeededOnly"));
            assertEquals("All", selenium.getSelectedLabel("status"));
            assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
        } else {
            assertFalse(selenium.isElementPresent("xpath=//div[@class='tabbox']"));
            assertFalse(selenium.isElementPresent("xpath=//h2[@class='formtop tabbedtop']"));
        }
    }

    private void instAdminTabs() {
        if (ClientProperties.isResearcherSupportActive()) {
            assertTrue(selenium.getTitle().contains("Requests"));
            assertFalse(selenium.getTitle().contains("My Requests"));
            assertFalse(selenium.isElementPresent("supportLetterRequest"));
            assertFalse(selenium.isElementPresent("question"));
            assertFalse(selenium.isElementPresent("questionResponse"));
            assertTrue(selenium.isElementPresent("specimenRequest"));

            clickAndWait("link=Questions to *");
            assertTrue(selenium.getTitle().contains("Requests"));
            assertFalse(selenium.getTitle().contains("My Requests"));
            assertFalse(selenium.isElementPresent("supportLetterRequest"));
            assertFalse(selenium.isElementPresent("question"));
            assertTrue(selenium.isElementPresent("questionResponse"));
            assertFalse(selenium.isElementPresent("specimenRequest"));

            clickAndWait("link=* Specimens");
            assertFalse(selenium.isChecked("actionNeededOnly"));
            assertEquals("All", selenium.getSelectedLabel("status"));
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        } else {
            assertFalse(selenium.isElementPresent("xpath=//div[@class='tabbox']"));
            assertFalse(selenium.isElementPresent("xpath=//h2[@class='formtop tabbedtop']"));
        }
        clickAndWait("link=Sign Out");
    }

    private void backToList(String resultLabel) {
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Research Proposal"));
        clickAndWait("link=Back");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Filter By Status"));
        assertTrue(selenium.isTextPresent(resultLabel));
        assertEquals("All", selenium.getSelectedLabel("status"));
    }

    private void goToAdminListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Request Administration");
        waitForPageToLoad();
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isElementPresent("actionNeededOnly"));
        assertTrue(selenium.isTextPresent("Action Required"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimenRequest']/thead/tr/th[8]"));
    }

    /**
     * test the my requests page.
     * @throws ParseException on error
     */
    public void testMyRequests() throws ParseException {
        loginAsUser1();
        clickAndWait("link=My Requests");
        assertTrue(selenium.isElementPresent("actionNeededOnly"));
        assertTrue(selenium.isTextPresent("Action Required"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[8]"));
        userStatusFilter();
        backToList("1-" + USER_RESULT_COUNT + " of " + USER_RESULT_COUNT + " Results");
        userTabs();
        sorting(COL_COUNT, "specimenRequest", true, DATE_COLS);

        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Request Details"));

        String urlHackLocation = selenium.getLocation();

        clickAndWait("link=Sign Out");
        login("testunauthorized-user@example.com", "tissueLocator1", true);

        selenium.open(urlHackLocation);
        assertTrue(selenium.isTextPresent("java.lang.IllegalArgumentException"));
        assertTrue(selenium.isTextPresent("User does not have access to this object."));
    }

    private void userStatusFilter() {
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-" + USER_RESULT_COUNT + " of " + USER_RESULT_COUNT + " Results"));
        String[] statuses = new String[] {"Revision Requested", "Approved", "Denied", "Partially Approved",
                "Pending Final Decision", "Partially Processed", "Fully Processed"};
        for (String status : statuses) {
            selenium.select("status", "label=" + status);
            waitForPageToLoad();
            assertEquals(status, selenium.getSelectedLabel("status"));
            assertEquals(status, selenium.getTable("specimenRequest.1.5"));
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertEquals("Revision Requested".equals(status),
                    selenium.isElementPresent("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[8]/a"));
            assertEquals("Revision Requested".equals(status), selenium.isElementPresent("link=Edit"));
            assertFalse(selenium.isElementPresent("link=Review"));
            assertFalse(selenium.isElementPresent("link=View"));
        }
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-" + USER_RESULT_COUNT + " of " + USER_RESULT_COUNT + " Results"));
    }

    private void userTabs() {
        if (ClientProperties.isResearcherSupportActive()) {
            assertTrue(selenium.getTitle().contains("My Requests"));
            assertFalse(selenium.isElementPresent("supportLetterRequest"));
            assertFalse(selenium.isElementPresent("question"));
            assertFalse(selenium.isElementPresent("questionResponse"));
            assertTrue(selenium.isElementPresent("specimenRequest"));

            clickAndWait("link=Letters of Support");
            assertTrue(selenium.getTitle().contains("My Requests"));
            assertTrue(selenium.isElementPresent("supportLetterRequest"));
            assertFalse(selenium.isElementPresent("question"));
            assertFalse(selenium.isElementPresent("questionResponse"));
            assertFalse(selenium.isElementPresent("specimenRequest"));

            clickAndWait("link=* Specimens");
            clickAndWait("link=Questions to *");
            assertTrue(selenium.getTitle().contains("My Requests"));
            assertFalse(selenium.isElementPresent("supportLetterRequest"));
            assertTrue(selenium.isElementPresent("question"));
            assertFalse(selenium.isElementPresent("questionResponse"));
            assertFalse(selenium.isElementPresent("specimenRequest"));

            clickAndWait("link=* Specimens");
            assertFalse(selenium.isChecked("actionNeededOnly"));
            assertEquals("All", selenium.getSelectedLabel("status"));
            assertTrue(selenium.isTextPresent("1-" + USER_RESULT_COUNT + " of " + USER_RESULT_COUNT + " Results"));
        } else {
            assertFalse(selenium.isElementPresent("xpath=//div[@class='tabbox']"));
            assertFalse(selenium.isElementPresent("xpath=//h2[@class='formtop tabbedtop']"));
        }
    }
}
