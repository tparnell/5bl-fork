/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractSpecimenTest;

/**
 * @author ddasgupta
 *
 */
public class NbstrnSpecimenTest extends AbstractSpecimenTest {

    private static final String[] STORAGE_CONDITION_OPTIONS = {"-70 - - 80 (deep freeze)", "-20 (Freezer)",
            "Refrigerated", "Room Temperature", "Unknown"};
    private static final String[] NUTRITIONAL_STATUS_OPTIONS = {"Breast", "Formula", "Both", "TPN", "Unknown",
            "Not Reported"};

    /**
     * {@inheritDoc}
     */
    @Override
    protected void requiredExtensionValidation() {
        assertTrue(selenium.isTextPresent("Birth Weight (g) must be set"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionFormatValidationEntry() {
        selenium.type("object.customProperties['birthWeight']", "invalid");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionFormatValidationMessage() {
        assertTrue(selenium.isTextPresent("Birth Weight (g) is invalid."));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionMinMaxValidationEntry() {
        selenium.type("object.customProperties['birthWeight']", "-10");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void extensionMinMaxValidationMessage() {
        assertTrue(selenium.isTextPresent("Birth Weight (g) must be greater than or equal to 0.00"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void enterExtensionData() {
        selenium.type("object.customProperties['birthWeight']", "12345");

        String[] storageConditionOptions = selenium.getSelectOptions("object.customProperties['storageCondition']");
        assertEquals(STORAGE_CONDITION_OPTIONS.length, storageConditionOptions.length);
        for (int i = 0; i < STORAGE_CONDITION_OPTIONS.length; i++) {
            assertEquals(STORAGE_CONDITION_OPTIONS[i], storageConditionOptions[i]);
        }
        selenium.select("object.customProperties['storageCondition']", "label=Refrigerated");

        String[] nutritionalStatusOptions = selenium.getSelectOptions("object.customProperties['nutritionalStatus']");
        assertEquals(NUTRITIONAL_STATUS_OPTIONS.length, nutritionalStatusOptions.length);
        for (int i = 0; i < NUTRITIONAL_STATUS_OPTIONS.length; i++) {
            assertEquals(NUTRITIONAL_STATUS_OPTIONS[i], nutritionalStatusOptions[i]);
        }
        selenium.select("object.customProperties['nutritionalStatus']", "label=TPN");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void viewExtensionData() {
        assertTrue(selenium.isTextPresent("Birth Weight (g)"));
        assertTrue(selenium.isTextPresent("12,345"));
        assertTrue(selenium.isTextPresent("DBS Specimen Storage Condition"));
        assertTrue(selenium.isTextPresent("Refrigerated"));
        assertTrue(selenium.isTextPresent("Nutritional Status"));
        assertTrue(selenium.isTextPresent("TPN"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void emptyExtensions() {
       // all extensions are required
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    protected void verifyStatusHistory() {
        loginAsAdmin();
        goToSpecimenPage();
        enterData(getNewSpecimenId(), true, ClientProperties.getDefaultConsortiumMemberName());
        super.verifyStatusHistory();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setUpExpectedDetailsPanelColumns() {
        setExpectedDetailsPanelColumn1Label("State");
        setExpectedDetailsPanelColumn1Value("California");
        setExpectedDetailsPanelColumn2Label("Birth Year");
        setExpectedDetailsPanelColumn2Value("2009");
        setExpectedDetailsPanelColumn3Label("Patient's Age at Collection");
        setExpectedDetailsPanelColumn3Value("50 months");
        setExpectedDetailsPanelColumn4Label("DBS Specimen Storage Condition");
        setExpectedDetailsPanelColumn4Value("Refrigerated");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isCollectionProtocolDisplayed() {
        return false;
    }
}
