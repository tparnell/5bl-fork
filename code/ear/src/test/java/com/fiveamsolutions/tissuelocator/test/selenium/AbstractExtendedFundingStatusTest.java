/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium;

import org.apache.commons.lang.StringUtils;

/**
 * @author gvaughn
 *
 */
public abstract class AbstractExtendedFundingStatusTest extends
        AbstractSpecimenRequestAndReviewTest {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyFundedFieldVisibility(boolean visible) {
        super.verifyFundedFieldVisibility(visible);
        assertEquals(visible, selenium.isVisible("object.customProperties['grantNumber']"));
        assertEquals(visible, selenium.isVisible("object.customProperties['totalFunding']"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateFundedFieldsFormat() {
        super.validateFundedFieldsFormat();
        String invalidMessage = "Total Funding is invalid.";
        String digitsMessage = "Total Funding should contain at most 2 decimal digits";
        String requiredMessage = "Total Funding must be set";
        selenium.type("object.customProperties['totalFunding']", "54321");
        assertEquals("54321", selenium.getValue("object.customProperties['totalFunding']"));
        clickAndWait("btn_save");
        assertFalse(selenium.isTextPresent(invalidMessage));
        assertFalse(selenium.isTextPresent(digitsMessage));
        assertFalse(selenium.isTextPresent(requiredMessage));
    
        selenium.type("object.customProperties['totalFunding']", "54,321");
        clickAndWait("btn_save");
        assertEquals("54321", selenium.getValue("object.customProperties['totalFunding']"));
        assertFalse(selenium.isTextPresent(invalidMessage));
        assertFalse(selenium.isTextPresent(digitsMessage));
        assertFalse(selenium.isTextPresent(requiredMessage));
    
        selenium.type("object.customProperties['totalFunding']", "123.45");
        clickAndWait("btn_save");
        assertEquals("123.45", selenium.getValue("object.customProperties['totalFunding']"));
        assertFalse(selenium.isTextPresent(invalidMessage));
        assertFalse(selenium.isTextPresent(digitsMessage));
        assertFalse(selenium.isTextPresent(requiredMessage));
    
        selenium.type("object.customProperties['totalFunding']", "1,234.56");
        clickAndWait("btn_save");
        assertEquals("1234.56", selenium.getValue("object.customProperties['totalFunding']"));
        assertFalse(selenium.isTextPresent(invalidMessage));
        assertFalse(selenium.isTextPresent(digitsMessage));
        assertFalse(selenium.isTextPresent(requiredMessage));
    
        selenium.type("object.customProperties['totalFunding']", "1,234.5678");
        clickAndWait("btn_save");
        assertEquals("1234.5678", selenium.getValue("object.customProperties['totalFunding']"));
        assertFalse(selenium.isTextPresent(invalidMessage));
        assertTrue(selenium.isTextPresent(digitsMessage));
        assertFalse(selenium.isTextPresent(requiredMessage));
    
        selenium.type("object.customProperties['totalFunding']", "invalid");
        clickAndWait("btn_save");
        assertEquals("invalid", selenium.getValue("object.customProperties['totalFunding']"));
        assertTrue(selenium.isTextPresent(invalidMessage));
        assertFalse(selenium.isTextPresent(digitsMessage));
        assertFalse(selenium.isTextPresent(requiredMessage));
        selenium.type("object.customProperties['totalFunding']", "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void enterFundedFields() {
        super.enterFundedFields();
        selenium.type("object.customProperties['grantNumber']", "12345");
        selenium.type("object.customProperties['totalFunding']", "54321");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyFundedFieldsCleared() {
        super.verifyFundedFieldsCleared();
        assertTrue(StringUtils.isBlank(selenium.getValue("object.customProperties['grantNumber']")));
        assertTrue(StringUtils.isBlank(selenium.getValue("object.customProperties['totalFunding']")));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyFundedFieldValues() {
        super.verifyFundedFieldValues();
        assertEquals("12345", selenium.getValue("object.customProperties['grantNumber']"));
        assertEquals("54321", selenium.getValue("object.customProperties['totalFunding']"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void confirmFundedFieldText() {
        super.confirmFundedFieldText();
        assertTrue(selenium.isTextPresent("12345"));
        assertTrue(selenium.isTextPresent("$ 54,321"));
    }

}
