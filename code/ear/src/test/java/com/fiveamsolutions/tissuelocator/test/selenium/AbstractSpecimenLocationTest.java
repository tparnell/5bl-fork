package com.fiveamsolutions.tissuelocator.test.selenium;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;

/**
 *
 * @author aswift
 *
 */
public abstract class AbstractSpecimenLocationTest extends AbstractTissueLocatorSeleniumTest {

    /**
     * three.
     */
    protected static final int THREE = 3;

    /**
     * four.
     */
    protected static final int FOUR = 4;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "SpecimenLocationTest.sql";
    }

    /**
     * Go to list page.
     */
    protected void goToListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=" + ClientProperties.getBiospecimenAdministrationLink());
        assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " Administration"));
    }

    /**
     * Go to my request page.
     */
    protected void goToMyRequestsPage() {
        clickAndWait("link=My Requests");
    }

    /**
     * validate the biospecimen admin row.
     * @param rowNum the row number
     * @param display whether the column should be displayed.
     * @param displayRow whether the row should be displayed.
     */
    protected void validateBioSpecimenAdminRow(final int rowNum, final boolean display, final boolean displayRow) {
        goToListPage();

        if (displayRow) {
            if (display) {
                assertTrue(StringUtils.isNotEmpty(selenium.getTable("specimen." + rowNum + ".8")));
            } else {
                assertTrue(StringUtils.isEmpty(selenium.getTable("specimen." + rowNum + ".8")));
            }
        } else {
            assertNotEquals(ClientProperties.getInstitutionTerm(), selenium.getTable("specimen.0.8"));
        }
        clickTableCellLink("specimen", rowNum, 1);

        if (display) {
            checkDiv(display);
        }
    }

    /**
     * check the div.
     * @param toDisplay whether the div should be displayed.
     */
    protected void checkDiv(final boolean toDisplay) {
        if (toDisplay) {
            assertTrue("Was Expecting a Div called " + ClientProperties.getInstitutionTerm() + " but was not found",
                    selenium.isTextPresent(ClientProperties.getInstitutionTerm()));
        } else {
            assertFalse("a Div called " + ClientProperties.getInstitutionTerm() + " should not have been present",
                    selenium.isTextPresent(ClientProperties.getInstitutionTerm()));
        }
    }
}
