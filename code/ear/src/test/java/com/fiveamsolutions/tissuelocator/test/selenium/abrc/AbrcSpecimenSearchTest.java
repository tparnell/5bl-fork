/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractSpecimenSearchTest;

/**
 * @author smiller
 *
 */
public class AbrcSpecimenSearchTest extends AbstractSpecimenSearchTest {

    private static final String LABEL_SELECT = "label=- All -";
    private static final String CUSTOM_PROPERTIES_PRESERVATION_TYPE = "object.customProperties['preservationType']";
    private static final String CUSTOM_PROPERTIES_ANATOMIC_SOURCE = "object.customProperties['anatomicSource']";
    private static final String CUSTOM_PROPERTIES_SPECIMEN_DENSITY = "customProperties['specimenDensity']";
    private static final String CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING =
        "customProperties['timeLapseToProcessing']";
    private static final String CUSTOM_PROPERTIES_STORAGE_TEMPERATURE = "customProperties['storageTemperature']";
    private static final int SHORT_SEARCH_DELAY = 500;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validation() {
        setDynamicTextSearchFields("invalid");

        super.validation();

        assertTrue(selenium.isTextPresent("Storage Temperature is invalid."));
        assertTrue(selenium.isTextPresent("Time Lapse To Processing (minutes) is invalid."));
        assertTrue(selenium.isTextPresent("Specimen Density is invalid."));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void filters() {
        setDynamicTextSearchFields("");

        super.filters();

        selenium.select(CUSTOM_PROPERTIES_ANATOMIC_SOURCE, "label=" + ClientProperties.getAnatomicSource());
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Anatomic Source: " + ClientProperties.getAnatomicSource()));

        selenium.select(CUSTOM_PROPERTIES_ANATOMIC_SOURCE, LABEL_SELECT);
        selenium.select(CUSTOM_PROPERTIES_PRESERVATION_TYPE, "label=Fixed Formalin");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Preservation Type: Fixed Formalin"));

        selenium.select(CUSTOM_PROPERTIES_PRESERVATION_TYPE, LABEL_SELECT);
        selenium.type(CUSTOM_PROPERTIES_STORAGE_TEMPERATURE, "-90");
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Maximum Storage Temperature: -90"));

        selenium.type(CUSTOM_PROPERTIES_STORAGE_TEMPERATURE, "");
        selenium.type(CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING, "35");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Maximum Time to Processing: 35"));

        selenium.type(CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING, "");
        selenium.type(CUSTOM_PROPERTIES_SPECIMEN_DENSITY, "65");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Minimum Specimen Density: 65"));

        selenium.type(CUSTOM_PROPERTIES_SPECIMEN_DENSITY, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void clearAllFilters() {
        super.clearAllFilters();

        setDynamicTextSearchFields("");

        selenium.select(CUSTOM_PROPERTIES_ANATOMIC_SOURCE, LABEL_SELECT);
        selenium.select(CUSTOM_PROPERTIES_PRESERVATION_TYPE, LABEL_SELECT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void fillInAllFilters() {
        super.fillInAllFilters();
        selenium.select(CUSTOM_PROPERTIES_ANATOMIC_SOURCE, "label=" + ClientProperties.getAnatomicSource());
        selenium.select(CUSTOM_PROPERTIES_PRESERVATION_TYPE, "label=Fixed Formalin");

        selenium.type(CUSTOM_PROPERTIES_STORAGE_TEMPERATURE, "-80");
        selenium.type(CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING, "30");
        selenium.type(CUSTOM_PROPERTIES_SPECIMEN_DENSITY, "75");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void sessionStorage() {
        super.sessionStorage();

        assertEquals(ClientProperties.getAnatomicSource(),
                selenium.getSelectedLabel(CUSTOM_PROPERTIES_ANATOMIC_SOURCE));
        assertEquals("Fixed Formalin", selenium.getSelectedLabel(CUSTOM_PROPERTIES_PRESERVATION_TYPE));
        assertEquals("-80", selenium.getValue(CUSTOM_PROPERTIES_STORAGE_TEMPERATURE));
        assertEquals("30", selenium.getValue(CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING));
        assertEquals("75", selenium.getValue(CUSTOM_PROPERTIES_SPECIMEN_DENSITY));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifySearchDisplayOptions() {
        goToSearchPage(false);
        assertEquals("Configure Displayed Columns", selenium.getText("link=Configure Displayed Columns"));
        clickAndWait("link=Configure Displayed Columns");
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent("Specimen Results Display Options"));
        // select biospecimen characteristics section
        // click first on participant then on biospecimen to make sure I am on biospecimen
        selenium.click("link=Patient Demographics");
        pause(SHORT_SEARCH_DELAY);
        selenium.click("link=Biospecimen Characteristics");
        pause(SHORT_SEARCH_DELAY);
        assertEquals("Check all", selenium.getText("link=Check all"));
        assertEquals("Uncheck all", selenium.getText("link=Uncheck all"));
        assertTrue(selenium.isElementPresent("Biospecimen_Characteristics_externalId"));
        assertEquals("on", selenium.getValue("Biospecimen_Characteristics_externalId"));
        assertTrue(selenium.isElementPresent("Biospecimen_Characteristics_preservationType"));
        assertEquals("on", selenium.getValue("Biospecimen_Characteristics_preservationType"));
        // check un-select all functionality
        selenium.click("link=Uncheck all");
        assertFalse(selenium.isChecked("css=input[id^='Biospecimen_Characteristics_']"));
        selenium.click("link=Check all");
        assertTrue(selenium.isChecked("css=input[id^='Biospecimen_Characteristics_']"));
        selenium.click("Biospecimen_Characteristics_externalId");
        assertEquals("off", selenium.getValue("Biospecimen_Characteristics_externalId"));
        selenium.click("Biospecimen_Characteristics_preservationType");
        assertEquals("off", selenium.getValue("Biospecimen_Characteristics_preservationType"));
        // cancel selection
        selenium.click("link=Cancel");
        selenium.selectFrame("relative=up");
        // go back and check the unselected values haven't actually changed value because of cancel
        clickAndWait("link=Configure Displayed Columns");
        selenium.selectFrame("popupFrame");
        assertEquals("on", selenium.getValue("Biospecimen_Characteristics_externalId"));
        assertEquals("on", selenium.getValue("Biospecimen_Characteristics_preservationType"));
        selenium.click("Biospecimen_Characteristics_externalId");
        assertEquals("off", selenium.getValue("Biospecimen_Characteristics_externalId"));
        selenium.click("Biospecimen_Characteristics_preservationType");
        assertEquals("off", selenium.getValue("Biospecimen_Characteristics_preservationType"));
        selenium.click("makeSettingsPersistendId"); // make changes persistent
        // save the selection
        selenium.click("btn_savedisplaypreferences");
        waitForPageToLoad();
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertFalse(selenium.isTextPresent("External ID"));
        assertFalse(selenium.isTextPresent("Preservation Type"));
        // leave the page
        clickAndWait("link=Home");
        // go to specimen administration and check that nothing has changed there
        clickAndWait("link=Biospecimen Administration");
        assertTrue(selenium.isTextPresent("External ID"));
        // go back to search
        goToSearchPage(false);
        assertFalse(selenium.isTextPresent("External ID"));
        assertFalse(selenium.isTextPresent("Preservation Type"));
        // logout
        clickAndWait("link=Sign Out");
        // login as user1
        loginAsUser1();
        // go to the search page
        goToSearchPage(false);
        // and click on search because this is the first time on page after a login
        clickAndWait("btn_search");
        clickAndWait("link=Configure Displayed Columns");
        selenium.selectFrame("popupFrame");
        verifyFieldsPresence("Biospecimen Characteristics", false, new String[] {"externalId" });
        // cancel selection
        selenium.click("link=Cancel");
        selenium.selectFrame("relative=up");
        // logout
        clickAndWait("link=Sign Out");
        // log back in as admin
        loginAsAdmin();
        // go to the search page
        goToSearchPage(false);
        // and click on search because this is the first time on page after a login
        clickAndWait("btn_search");
        assertFalse(selenium.isTextPresent("External ID"));
        assertFalse(selenium.isTextPresent("Preservation Type"));
        clickAndWait("link=Configure Displayed Columns");
        selenium.selectFrame("popupFrame");
        selenium.click("Biospecimen_Characteristics_externalId");
        assertEquals("on", selenium.getValue("Biospecimen_Characteristics_externalId"));
        selenium.click("Biospecimen_Characteristics_preservationType");
        assertEquals("on", selenium.getValue("Biospecimen_Characteristics_preservationType"));
        // make changes persistent so that the user settings would replicate the defaults
        selenium.click("makeSettingsPersistendId");
        // save the selection
        selenium.click("btn_savedisplaypreferences");
        waitForPageToLoad();
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        // check that the ID and the type show again
        assertTrue(selenium.isTextPresent("External ID"));
        assertTrue(selenium.isTextPresent("Preservation Type"));
    }

    private void setDynamicTextSearchFields(String value) {
        selenium.type(CUSTOM_PROPERTIES_STORAGE_TEMPERATURE, value);
        selenium.type(CUSTOM_PROPERTIES_TIME_LAPSE_TO_PROCESSING, value);
        selenium.type(CUSTOM_PROPERTIES_SPECIMEN_DENSITY, value);
    }
}
