/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import java.text.ParseException;

import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author ddasgupta
 *
 */
public class AbrcMtaReviewListTest extends AbstractListTest {
    private static final int COL_COUNT = 5;
    private static final int RESULT_COUNT = 54;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "MtaReviewTest.sql";
    }

    /**
     * test the user list page.
     * @throws ParseException on error
     */
    public void testList() throws ParseException {
        navigation();
        sorting(COL_COUNT, "signedMta", false, 1);
        paging("signedMta");
        pageSize(RESULT_COUNT);
    }

    private void navigation() {
        loginAsAdmin();
        assertTrue(selenium.isTextPresent(RESULT_COUNT + " MTA Reviews"));
        assertTrue(selenium.isElementPresent("link=" + RESULT_COUNT + " MTA Reviews"));
        clickAndWait("link=" + RESULT_COUNT + " MTA Reviews");
        assertTrue(selenium.isTextPresent("Material Transfer Agreement (MTA) Administration"));
        assertTrue(selenium.isTextPresent("Review Submitted MTAs"));
        assertTrue(selenium.isTextPresent("Submission Date"));
        clickAndWait("link=Review Submitted MTAs");
        assertTrue(selenium.isTextPresent("Material Transfer Agreement (MTA) Administration"));
        assertTrue(selenium.isTextPresent("Review Submitted MTAs"));
        assertTrue(selenium.isTextPresent("Submission Date"));
        clickAndWait("link=Add New Consortium MTA");
        assertTrue(selenium.isTextPresent("Material Transfer Agreement (MTA) Administration"));
        assertTrue(selenium.isTextPresent("Add New Consortium MTA"));
        assertTrue(selenium.isTextPresent("Upload Consortium MTA"));
        assertTrue(selenium.isTextPresent("Download Consortium MTA"));

        clickAndWait("link=Add New Consortium MTA");
        assertTrue(selenium.isTextPresent("Material Transfer Agreement (MTA) Administration"));
        assertTrue(selenium.isTextPresent("Add New Consortium MTA"));
        assertTrue(selenium.isTextPresent("Upload Consortium MTA"));
        assertTrue(selenium.isTextPresent("Download Consortium MTA"));
        clickAndWait("link=Review Submitted MTAs");
        assertTrue(selenium.isTextPresent("Material Transfer Agreement (MTA) Administration"));
        assertTrue(selenium.isTextPresent("Review Submitted MTAs"));
        assertTrue(selenium.isTextPresent("Submission Date"));
    }
}
