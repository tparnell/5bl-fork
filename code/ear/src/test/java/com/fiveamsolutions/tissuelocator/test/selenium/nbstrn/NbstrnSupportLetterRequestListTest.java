/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;

import java.text.ParseException;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author ddasgupta
 *
 */
public class NbstrnSupportLetterRequestListTest extends AbstractListTest {

    private static final int COL_COUNT = 6;
    private static final int[] DATE_COLS = {4, 5};
    private static final int RESULT_COUNT = 60;
    private static final int PENDING_COUNT = 20;
    private static final int RESPONDED_COUNT = 20;
    private static final int RESPONDED_OFFLINE_COUNT = 20;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "SupportLetterRequestListTest.sql";
    }

    /**
     * test the administrator's support letter request list page.
     * @throws ParseException on error
     */
    public void testAdminList() throws ParseException {
        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        goToAdminListPage();
        adminStatusFilter();
        adminFilterSaved();
        adminTabs();
        sorting(COL_COUNT, "supportLetterRequest", false, DATE_COLS);
        paging("supportLetterRequest");
        pageSize(RESULT_COUNT);
    }

    private void goToAdminListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Request Administration");
        waitForPageToLoad();
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("DBS Specimens"));
        clickAndWait("link=Letters of Support");
        assertTrue(selenium.isTextPresent("Letters of Support"));
        assertTrue(selenium.isElementPresent("status"));
        assertTrue(selenium.isTextPresent("Action Required"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='supportLetterRequest']/thead/tr/th[7]"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='supportLetterRequest']/thead/tr/th[8]"));
    }

    private void adminStatusFilter() {
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
        verifyAdminStatus("Pending", PENDING_COUNT, true);
        verifyAdminStatus("Responded", RESPONDED_COUNT, false);
        verifyAdminStatus("Responded Off-line", RESPONDED_OFFLINE_COUNT, false);
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
    }

    private void verifyAdminStatus(String status, int resultCount, boolean showReviewButton) {
        selenium.select("status", "label=" + status);
        waitForPageToLoad();
        assertEquals(status, selenium.getSelectedLabel("status"));
        assertEquals(status, selenium.getTable("supportLetterRequest.1.5"));
        assertTrue(selenium.isTextPresent("1-" + resultCount + " of " + resultCount + " Results"));

        assertEquals(showReviewButton, selenium.isElementPresent("link=Review"));
        String buttonXPath = "xpath=//table[@id='supportLetterRequest']/tbody/tr[%s]/td[8]/a[%s]";
        for (int i = 1; i <= resultCount; i++) {
            String actionRequiredText = selenium.getTable("supportLetterRequest." + i + ".6");
            boolean hasButton = selenium.isElementPresent(String.format(buttonXPath, i, 1));
            assertEquals(showReviewButton, hasButton);
            if (showReviewButton) {
                assertTrue(hasButton);
                assertEquals("NBSTRN Response", actionRequiredText);
            } else {
                assertFalse(hasButton);
                assertTrue(StringUtils.isBlank(actionRequiredText));
            }
        }
    }

    private void adminFilterSaved() {
        selenium.select("status", "label=Pending");
        waitForPageToLoad();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        clickAndWait("link=Home");
        goToAdminListPage();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
    }

    private void adminTabs() {
        assertTrue(selenium.getTitle().contains("Requests"));
        assertFalse(selenium.getTitle().contains("My Requests"));
        assertTrue(selenium.isElementPresent("supportLetterRequest"));
        assertFalse(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=DBS Specimens");
        assertTrue(selenium.getTitle().contains("Requests"));
        assertFalse(selenium.getTitle().contains("My Requests"));
        assertFalse(selenium.isElementPresent("supportLetterRequest"));
        assertFalse(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertTrue(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=Letters of Support");
        clickAndWait("link=Questions to States");
        assertTrue(selenium.getTitle().contains("Requests"));
        assertFalse(selenium.getTitle().contains("My Requests"));
        assertFalse(selenium.isElementPresent("supportLetterRequest"));
        assertTrue(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=Letters of Support");
        assertTrue(selenium.getTitle().contains("Requests"));
        assertFalse(selenium.getTitle().contains("My Requests"));
        assertTrue(selenium.isElementPresent("supportLetterRequest"));
        assertFalse(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-20 of " + RESULT_COUNT + " Results"));
    }

    /**
     * test the investigator's support letter request list page.
     */
    public void testUserList() {
        loginAsUser1();
        goToUserListPage();
        userStatusFilter();
        userFilterSaved();
        userTabs();
    }

    private void goToUserListPage() {
        clickAndWait("link=My Requests");
        assertTrue(selenium.isTextPresent("My Requests"));
        assertTrue(selenium.isTextPresent("DBS Specimens"));
        clickAndWait("link=Letters of Support");
        assertTrue(selenium.isTextPresent("Letters of Support"));
        assertTrue(selenium.isElementPresent("status"));
        assertFalse(selenium.isTextPresent("Action Required"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='supportLetterRequest']/thead/tr/th[7]"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='supportLetterRequest']/thead/tr/th[8]"));
    }

    private void userStatusFilter() {
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-6 of 6 Results"));
        verifyUserStatus("Pending", 2);
        verifyUserStatus("Received", 2);
        verifyUserStatus("Received Off-line", 2);
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-6 of 6 Results"));
    }

    private void verifyUserStatus(String status, int resultCount) {
        selenium.select("status", "label=" + status);
        waitForPageToLoad();
        assertEquals(status, selenium.getSelectedLabel("status"));
        assertEquals(status, selenium.getTable("supportLetterRequest.1.5"));
        assertTrue(selenium.isTextPresent("1-" + resultCount + " of " + resultCount + " Results"));
    }

    private void userFilterSaved() {
        selenium.select("status", "label=Pending");
        waitForPageToLoad();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        clickAndWait("link=Home");
        goToUserListPage();
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
    }

    private void userTabs() {
        assertTrue(selenium.getTitle().contains("My Requests"));
        assertTrue(selenium.isElementPresent("supportLetterRequest"));
        assertFalse(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=DBS Specimens");
        assertTrue(selenium.getTitle().contains("My Requests"));
        assertFalse(selenium.isElementPresent("supportLetterRequest"));
        assertFalse(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertTrue(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=Letters of Support");
        clickAndWait("link=Questions to States");
        assertTrue(selenium.getTitle().contains("My Requests"));
        assertFalse(selenium.isElementPresent("supportLetterRequest"));
        assertTrue(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));

        clickAndWait("link=Letters of Support");
        assertTrue(selenium.getTitle().contains("My Requests"));
        assertTrue(selenium.isElementPresent("supportLetterRequest"));
        assertFalse(selenium.isElementPresent("question"));
        assertFalse(selenium.isElementPresent("questionResponse"));
        assertFalse(selenium.isElementPresent("specimenRequest"));
        assertEquals("All", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-6 of 6 Results"));
    }
}
