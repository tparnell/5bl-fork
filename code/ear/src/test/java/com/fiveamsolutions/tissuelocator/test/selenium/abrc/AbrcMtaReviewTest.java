/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
    *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.xwork.StringUtils;

import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author ddasgupta
 *
 */
public class AbrcMtaReviewTest extends AbstractListTest {

    private static final int RESULT_COUNT = 21;
    private static final String BUTTON_XPATH = "//table[@id='signedMta']/tbody/tr[%d]/td[6]/a";
    private int remainingMtas = RESULT_COUNT;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "MtaInformationTest.sql";
    }

    /**
     * test the signed mta review page.
     * @throws ParseException on error
     */
    public void testSignedMtaReview() throws ParseException {
        showHideSections();
        validation();
        validationWithExisting();
        goToListPage();
        review(true, false, false, "test7c");
        review(true, true, true, "test7f");
        review(false, false, false, "test6c");
        review(true, true, false, "test6f");
        verifyContactPopulated();
        varyInstitution();
        goToListPage();
        review(true, false, false, "test6f");
        review(false, false, false, "test5c");
        review(true, true, false, "test5f");
        review(false, false, false, "test5f");
    }

    private void goToListPage() {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");
        clickAndWait("link=MTA Administration");
        clickAndWait("link=Review Submitted MTAs");
    }

    private void showHideSections() {
        goToListPage();
        assertEquals("test7f", selenium.getTable("signedMta.2.1"));
        clickAndWait(String.format(BUTTON_XPATH, 2));
        verifyDisplay(false, false, false, false, false);
        assertFalse(selenium.isChecked("correctInstitution_yes"));
        assertFalse(selenium.isChecked("correctInstitution_no"));
        assertFalse(selenium.isChecked("replaceExisting_yes"));
        assertFalse(selenium.isChecked("replaceExisting_no"));
        assertFalse(selenium.isChecked("valid_yes"));
        assertFalse(selenium.isChecked("valid_no"));

        selenium.click("correctInstitution_yes");
        verifyDisplay(false, true, false, false, false);
        selenium.click("replaceExisting_yes");
        verifyDisplay(false, true, true, false, false);
        selenium.click("valid_yes");
        verifyDisplay(false, true, true, true, true);
        selenium.type("mtaContactFirstName", "test mta contact first");
        selenium.type("mtaContactLastName", "test mta contact last");
        selenium.type("mtaContactLine1", "line 1");
        selenium.type("mtaContactLine2", "line 2");
        selenium.type("mtaContactCity", "city");
        selenium.select("mtaContactState", "label=Maryland");
        selenium.type("mtaContactZip", "zip");
        assertEquals("United States", selenium.getSelectedLabel("mtaContactCountry"));
        selenium.type("mtaContactEmail", "mta_contact@example.com");
        selenium.type("mtaContactPhone", "1234567890");
        selenium.type("mtaContactPhoneExtension", "1234");
        selenium.type("mtaContactFax", "0987654321");
        selenium.click("valid_no");
        verifyDisplay(false, true, true, false, true);
        selenium.click("valid_yes");
        verifyDisplay(false, true, true, true, true);
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactFirstName")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactLastName")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactLine1")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactLine2")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactCity")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactState")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactZip")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactEmail")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactPhone")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactPhoneExtension")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactFax")));

        selenium.click("replaceExisting_no");
        verifyDisplay(false, true, false, true, true);
        assertFalse(selenium.isChecked("valid_yes"));
        assertFalse(selenium.isChecked("valid_no"));
        selenium.click("correctInstitution_no");
        verifyDisplay(true, false, false, false, false);
        assertFalse(selenium.isChecked("replaceExisting_yes"));
        assertFalse(selenium.isChecked("replaceExisting_no"));
        assertFalse(selenium.isChecked("valid_yes"));
        assertFalse(selenium.isChecked("valid_no"));
    }

    private void validation() {
        goToListPage();
        assertEquals("test7c", selenium.getTable("signedMta.1.1"));
        clickAndWait(String.format(BUTTON_XPATH, 1));
        verifyDisplay(false, false, false, false, false);
        assertFalse(selenium.isChecked("correctInstitution_yes"));
        assertFalse(selenium.isChecked("correctInstitution_no"));
        selenium.click("correctInstitution_yes");
        verifyDisplay(false, false, true, false, false);
        selenium.click("valid_yes");
        verifyDisplay(false, false, true, true, true);
        clickAndWait("reviewSignedMtaForm_btn_save");
        verifyDisplay(false, false, true, true, true);
        assertTrue(selenium.isChecked("correctInstitution_yes"));
        assertFalse(selenium.isChecked("correctInstitution_no"));
        assertTrue(selenium.isChecked("valid_yes"));
        assertFalse(selenium.isChecked("valid_no"));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("First Name must be set"));
        assertTrue(selenium.isTextPresent("Last Name must be set"));
        assertTrue(selenium.isTextPresent("Address must be set"));
        assertTrue(selenium.isTextPresent("City must be set"));
        assertTrue(selenium.isTextPresent("State must be set"));
        assertTrue(selenium.isTextPresent("Zip/Postal Code must be set"));
        assertTrue(selenium.isTextPresent("Email must be set"));
        assertTrue(selenium.isTextPresent("Phone must be set"));

        selenium.type("mtaContactEmail", "invalid");
        clickAndWait("reviewSignedMtaForm_btn_save");
        verifyDisplay(false, false, true, true, true);
        assertTrue(selenium.isChecked("correctInstitution_yes"));
        assertFalse(selenium.isChecked("correctInstitution_no"));
        assertTrue(selenium.isChecked("valid_yes"));
        assertFalse(selenium.isChecked("valid_no"));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        selenium.isTextPresent("Email is not a well-formed email address");
        clickAndWait("link=Home");
    }

    private void validationWithExisting() {
        goToListPage();
        assertEquals("test7f", selenium.getTable("signedMta.2.1"));
        clickAndWait(String.format(BUTTON_XPATH, 2));
        verifyDisplay(false, false, false, false, false);
        assertFalse(selenium.isChecked("correctInstitution_yes"));
        assertFalse(selenium.isChecked("correctInstitution_no"));
        selenium.click("correctInstitution_yes");
        verifyDisplay(false, true, false, false, false);
        selenium.click("replaceExisting_no");
        verifyDisplay(false, true, false, true, true);
        clickAndWait("reviewSignedMtaForm_btn_save");
        verifyDisplay(false, true, false, true, true);
        assertTrue(selenium.isChecked("correctInstitution_yes"));
        assertFalse(selenium.isChecked("correctInstitution_no"));
        assertTrue(selenium.isChecked("replaceExisting_no"));
        assertFalse(selenium.isChecked("replaceExisting_yes"));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("First Name must be set"));
        assertTrue(selenium.isTextPresent("Last Name must be set"));
        assertTrue(selenium.isTextPresent("Address must be set"));
        assertTrue(selenium.isTextPresent("City must be set"));
        assertTrue(selenium.isTextPresent("State must be set"));
        assertTrue(selenium.isTextPresent("Zip/Postal Code must be set"));
        assertTrue(selenium.isTextPresent("Email must be set"));
        assertTrue(selenium.isTextPresent("Phone must be set"));

        selenium.click("replaceExisting_yes");
        verifyDisplay(false, true, true, false, false);
        selenium.click("valid_yes");
        verifyDisplay(false, true, true, true, true);
        selenium.type("mtaContactEmail", "invalid");
        clickAndWait("reviewSignedMtaForm_btn_save");
        verifyDisplay(false, true, true, true, true);
        assertTrue(selenium.isChecked("correctInstitution_yes"));
        assertFalse(selenium.isChecked("correctInstitution_no"));
        assertTrue(selenium.isChecked("replaceExisting_yes"));
        assertFalse(selenium.isChecked("replaceExisting_no"));
        assertTrue(selenium.isChecked("valid_yes"));
        assertFalse(selenium.isChecked("valid_no"));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        selenium.isTextPresent("Email is not a well-formed email address");
        clickAndWait("link=Home");
    }

    private void review(boolean approve, boolean hasExisting, boolean replaceExisting, String instName) {
        assertEquals(instName, selenium.getTable("signedMta.1.1"));
        clickAndWait(String.format(BUTTON_XPATH, 1));
        verifyDisplay(false, false, false, false, false);
        selenium.click("correctInstitution_yes");
        verifyDisplay(false, hasExisting, !hasExisting, false, false);

        assertEquals(hasExisting, selenium.isElementPresent("replace_existing"));
        if (hasExisting) {
            if (replaceExisting) {
                selenium.click("replaceExisting_yes");
                verifyDisplay(false, true, true, false, false);
            } else {
                selenium.click("replaceExisting_no");
                verifyDisplay(false, true, false, true, true);
            }
        }

        boolean showCertification = !hasExisting || replaceExisting;
        verifyDisplay(false, hasExisting, showCertification, !showCertification, !showCertification);
        if (showCertification) {
            if (approve) {
                selenium.click("valid_yes");
                verifyDisplay(false, hasExisting, showCertification, true, true);
            } else {
                selenium.click("valid_no");
                verifyDisplay(false, hasExisting, showCertification, false, true);
            }
        }
        boolean showContact = !showCertification || approve;
        verifyDisplay(false, hasExisting, showCertification, showContact, true);

        if (showContact) {
            assertFalse(selenium.isElementPresent("mtaContactOrganization"));
            selenium.type("mtaContactFirstName", "test mta contact first");
            selenium.type("mtaContactLastName", "test mta contact last");
            selenium.type("mtaContactLine1", "line 1");
            selenium.type("mtaContactLine2", "line 2");
            selenium.type("mtaContactCity", "city");
            selenium.select("mtaContactState", "label=Maryland");
            selenium.type("mtaContactZip", "zip");
            selenium.select("mtaContactCountry", "label=United States");
            selenium.type("mtaContactEmail", "mta_contact@example.com");
            selenium.type("mtaContactPhone", "1234567890");
            selenium.type("mtaContactPhoneExtension", "1234");
            selenium.type("mtaContactFax", "0987654321");
        }

        remainingMtas--;
        if (replaceExisting) {
            remainingMtas--;
        }
        clickAndWait("reviewSignedMtaForm_btn_save");
        assertTrue(selenium.isTextPresent("Material Transfer Agreement (MTA) Administration"));
        assertTrue(selenium.isTextPresent("Review Submitted MTAs"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Signed MTA successfully reviewed."));
        assertTrue(selenium.isTextPresent("1-" + remainingMtas + " of " + remainingMtas + " Results"));
    }

    private void verifyContactPopulated() {
        assertEquals("test6f", selenium.getTable("signedMta.1.1"));
        clickAndWait(String.format(BUTTON_XPATH, 1));
        String expectedDate = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
        assertTrue("Invalid date, expected date: " + expectedDate + ", body text: " + selenium.getBodyText(), 
                selenium.isTextPresent(expectedDate));
        assertTrue(selenium.isTextPresent("test6f"));
        assertTrue(selenium.isTextPresent("Reviewer, Maricopa"));
        assertTrue(selenium.isTextPresent("maricopa-reviewer@example.com"));
        assertTrue(selenium.isTextPresent("6023445011"));
        selenium.click("correctInstitution_yes");
        selenium.click("valid_yes");
        assertEquals("test mta contact first", selenium.getValue("mtaContactFirstName"));
        assertEquals("test mta contact last", selenium.getValue("mtaContactLastName"));
        assertEquals("line 1", selenium.getValue("mtaContactLine1"));
        assertEquals("line 2", selenium.getValue("mtaContactLine2"));
        assertEquals("city", selenium.getValue("mtaContactCity"));
        assertEquals("Maryland", selenium.getSelectedLabel("mtaContactState"));
        assertEquals("zip", selenium.getValue("mtaContactZip"));
        assertEquals("United States", selenium.getSelectedLabel("mtaContactCountry"));
        assertEquals("mta_contact@example.com", selenium.getValue("mtaContactEmail"));
        assertEquals("1234567890", selenium.getValue("mtaContactPhone"));
        assertEquals("1234", selenium.getValue("mtaContactPhoneExtension"));
        assertEquals("0987654321", selenium.getValue("mtaContactFax"));
    }

    private void varyInstitution() {
        assertTrue(selenium.isElementPresent("link=(Reference List)"));
        assertFalse(selenium.isVisible("institutions_list"));
        selenium.click("link=(Reference List)");
        pause(DELAY * 2);
        assertTrue(selenium.isVisible("institutions_list"));
        assertTrue(selenium.getSelectOptions("institutions_existing").length > 0);
        selenium.click("link=(Reference List)");
        pause(DELAY * 2);
        assertFalse(selenium.isVisible("institutions_list"));

        selenium.click("correctInstitution_no");
        verifyDisplay(true, false, false, false, false);
        assertEquals("-", selenium.getTable("institution.1.1"));
        assertEquals("-", selenium.getTable("institution.1.2"));
        assertTrue(selenium.isTextPresent("1-20 of 74 Results"));
        assertTrue(selenium.isTextPresent("1 of 4 Pages"));
        clickAndWait("xpath=//table[@id='institution']/tbody/tr[5]/td[1]/input");
        verifyDisplay(true, true, false, false, false);
        assertFalse(selenium.isChecked("correctInstitution_yes"));
        assertTrue(selenium.isChecked("correctInstitution_no"));
        assertTrue(selenium.isChecked("xpath=//table[@id='institution']/tbody/tr[5]/td[1]/input"));

        selenium.click("replaceExisting_yes");
        verifyDisplay(true, true, true, false, false);
        selenium.click("valid_yes");
        verifyDisplay(true, true, true, true, true);
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactFirstName")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactLastName")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactLine1")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactLine2")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactCity")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactState")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactZip")));
        assertEquals("United States", selenium.getSelectedLabel("mtaContactCountry"));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactEmail")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactPhone")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactPhoneExtension")));
        assertTrue(StringUtils.isBlank(selenium.getValue("mtaContactFax")));

        selenium.click("correctInstitution_yes");
        waitForPageToLoad();
        verifyDisplay(false, false, true, false, false);
        assertTrue(selenium.isChecked("correctInstitution_yes"));
        assertFalse(selenium.isChecked("correctInstitution_no"));
        for (int i = 1; i <= DEFAULT_PAGE_SIZE; i++) {
            assertFalse(selenium.isChecked(String
                    .format("xpath=//table[@id='institution']/tbody/tr[%d]/td[1]/input", i)));
        }

        selenium.click("valid_yes");
        verifyDisplay(false, false, true, true, true);
        assertEquals("test mta contact first", selenium.getValue("mtaContactFirstName"));
        assertEquals("test mta contact last", selenium.getValue("mtaContactLastName"));
        assertEquals("line 1", selenium.getValue("mtaContactLine1"));
        assertEquals("line 2", selenium.getValue("mtaContactLine2"));
        assertEquals("city", selenium.getValue("mtaContactCity"));
        assertEquals("Maryland", selenium.getSelectedLabel("mtaContactState"));
        assertEquals("zip", selenium.getValue("mtaContactZip"));
        assertEquals("United States", selenium.getSelectedLabel("mtaContactCountry"));
        assertEquals("mta_contact@example.com", selenium.getValue("mtaContactEmail"));
        assertEquals("1234567890", selenium.getValue("mtaContactPhone"));
        assertEquals("1234", selenium.getValue("mtaContactPhoneExtension"));
        assertEquals("0987654321", selenium.getValue("mtaContactFax"));
    }

    private void verifyDisplay(boolean institutionList, boolean replaceExisting, boolean certification,
            boolean contactInfo, boolean submitButton) {
        assertEquals(institutionList, selenium.isVisible("institution_list"));
        if (selenium.isElementPresent("replace_existing")) {
            assertEquals(replaceExisting, selenium.isVisible("replace_existing"));
            if (!replaceExisting) {
                assertFalse(selenium.isChecked("replaceExisting_yes"));
                assertFalse(selenium.isChecked("replaceExisting_no"));
            }
        }
        assertEquals(certification, selenium.isVisible("mta_certification"));
        if (!certification) {
            assertFalse(selenium.isChecked("valid_yes"));
            assertFalse(selenium.isChecked("valid_no"));
        }
        assertEquals(contactInfo, selenium.isVisible("mta_contact"));
        assertEquals(submitButton, selenium.isVisible("mta_submit_button"));
    }
}
