/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractExtendedFundingStatusTest;

/**
 * @author ddasgupta
 *
 */
public class NbstrnSpecimenRequestAndReviewTest extends AbstractExtendedFundingStatusTest {

    private static final String CART_ROW_XPATH = "xpath=//tr[@id='%sgeneral_data%d']";
    private static final int INST1_REVIEW_ROW = 0;
    private static final int INST2_REVIEW_ROW = 1;
    private static final String TECH_1_EMAIL = ClientProperties.getTissueTechEmail();
    private static final String TECH_2_EMAIL = "newyork-tech@example.com";
    private static final int NUM_SEARCH_RESULTS = 3;

    /**
     * test the request submission and review workflow.
     * @throws Exception on error
     */
    @Test
    public void testSpecimenRequestAndReview() throws Exception {
        String orderCount = getOrderCount();

        // login and make first request
        addToCart();
        studyDetails();
        confirm(new String[]{"Draft"});
        save();

        // test validation and make second request
        validation();
        copyFromUser("investigator");
        copyFromUser("recipient");
        navigation();

        loginAsUser1();
        clickAndWait("link=My Requests");
        selenium.select("status", "Revision Requested");
        waitForPageToLoad();
        viewRequest(false, false, false, new String[]{"Draft", "Pending"});
        clickAndWait("link=Sign Out");

        login("test-user-inst@example.com", "tissueLocator1");
        performLineItemReview(INST1_REVIEW_ROW, INST2_REVIEW_ROW, 1);
        viewRequestVotes("Pending", INST1_REVIEW_ROW, new String[]{"Pending"});
        clickAndWait("link=Sign Out");
        // Mark an order as shipped, to ensure remaining
        // institutions can still vote
        goToOrderEditPage(TECH_1_EMAIL);
        markAsShipped();
        viewRequestVotes("Pending", INST1_REVIEW_ROW, new String[]{"Pending"});
        clickAndWait("link=Sign Out");
        login("test-user-inst2@example.com", "tissueLocator1");
        performLineItemReview(INST2_REVIEW_ROW, INST1_REVIEW_ROW, 2);
        viewRequestVotes("Partially Processed", INST2_REVIEW_ROW, new String[]{"Pending", "Partially Processed"});
        clickAndWait("link=Sign Out");
        verifyOrderCount(orderCount);
        goToOrderEditPage(TECH_2_EMAIL);
        markAsShipped();
        viewRequestVotes("Fully Processed", INST2_REVIEW_ROW,
                new String[]{"Pending", "Partially Processed", "Fully Processed"});

        verifyDraftValidation();
        verifyDeleteDraft();
        verifyEditErrorRequest();
        verifyDraftFlow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addToCart() {
        loginAsUser1();
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        verifyRequestProcessStepDisplay("search");
        clickAndWait("btn_search");
        verifyRequestProcessStepDisplay("select");
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        String textFieldXPath = "xpath=//table[@id='specimen']/tbody/tr[%d]/td[3]/input[3]";
        selenium.type(String.format(textFieldXPath, 1), Integer.toString(NUM_SEARCH_RESULTS));
        selenium.keyUp(String.format(textFieldXPath, 1), Integer.toString(NUM_SEARCH_RESULTS));
        selenium.type(String.format(textFieldXPath, 2), Integer.toString(NUM_SEARCH_RESULTS));
        selenium.keyUp(String.format(textFieldXPath, 2), Integer.toString(NUM_SEARCH_RESULTS));
        clickAndWait("btn_addtocart");
        assertTrue(selenium.isTextPresent(ClientProperties.getCartLink()));
        assertTrue(selenium.isTextPresent(ClientProperties.getDefaultConsortiumMemberName()));
        assertTrue(selenium.isElementPresent(String.format(CART_ROW_XPATH, "", 0)));
        assertTrue(selenium.isElementPresent(String.format(CART_ROW_XPATH, "", 0) + "/td[1]/a"));
        assertTrue(selenium.isElementPresent(String.format(CART_ROW_XPATH, "", 1)));
        assertTrue(selenium.isElementPresent(String.format(CART_ROW_XPATH, "", 1) + "/td[1]/a"));
        verifyExternalCommentDisplay(false);
        verifyRequestProcessStepDisplay("request");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyCart(boolean isResearcher) {
        assertTrue(selenium.isTextPresent(ClientProperties.getInstitutionTerm()));
        assertTrue(selenium.isTextPresent(ClientProperties.getDefaultConsortiumMemberName()));
        assertTrue(selenium.isElementPresent(String.format(CART_ROW_XPATH, "aggregate", 0)));
        assertTrue(selenium.isElementPresent(String.format(CART_ROW_XPATH, "aggregate", 0) + "/td[1]/a"));
        assertTrue(selenium.isElementPresent(String.format(CART_ROW_XPATH, "aggregate", 1)));
        assertTrue(selenium.isElementPresent(String.format(CART_ROW_XPATH, "aggregate", 1) + "/td[1]/a"));
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyStudyHelpText() {
        String biosketchHelp = "The biographical sketch, or biosketch, should follow the formatting requirements"
            + " set forth by the U.S. Department of Health and Human Services";
        verifyHelpText("aid.principalInvestigator.resume", biosketchHelp, new String[]{"Grant Application page"},
                new String[]{"http://grants.nih.gov/grants/funding/phs398/phs398.html"});
        String totalFundingHelp = "We are asking for this information because states need to know if there "
            + "is enough funding to recover the costs for locating, packaging DBS specimens and carrying out "
            + "proposed research.";
        verifyHelpText("aid.totalFunding", totalFundingHelp, new String[0], new String[0]);
    }

    private void performLineItemReview(int editableRow, int uneditableRow, int numReviews) {
        String confirm = "Once you submit your decision, you will not be able to change it. Do you want to continue?";
        clickAndWait("Link=2 State Reviews");
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        String buttonXPath = "xpath=//table[@id='specimenRequest']/tbody/tr[2]/td[8]/a";
        assertTrue(selenium.isElementPresent(buttonXPath));
        assertEquals("Review", selenium.getText(buttonXPath));
        clickAndWait(buttonXPath);
        assertTrue(selenium.isTextPresent("You must take the following action(s) on this request:"));
        assertTrue(selenium.isTextPresent("Submit your State Approval"));

        assertTrue(selenium.isTextPresent("Request Summary"));
        assertTrue(selenium.isTextPresent("test-first-name-investigator test-last-name-investigator"));
        assertTrue(selenium.isTextPresent("California"));
        assertTrue(selenium.isTextPresent("test-email-investigator@email.com"));
        assertTrue(selenium.isTextPresent("investi"));
        assertTrue(selenium.isTextPresent("ext."));
        assertTrue(selenium.isTextPresent("exinves"));
        assertTrue(selenium.isTextPresent("test-line1-investigator"));
        assertTrue(selenium.isTextPresent("test-line2-investigator"));
        assertTrue(selenium.isTextPresent("test-city-investigator"));
        assertTrue(selenium.isTextPresent("Maryland,"));
        assertTrue(selenium.isTextPresent("inves"));
        assertTrue(selenium.isTextPresent("Canada"));
        assertTrue(selenium.isElementPresent("link=test.properties"));
        assertTrue(selenium.isElementPresent("link=testui.properties.example"));
        assertTrue(selenium.isElementPresent("link=View Request Details"));

        assertTrue(selenium.isTextPresent("Internal Comments"));
        assertTrue(selenium.isElementPresent("btn_addComment"));
        assertTrue(selenium.isElementPresent("xpath=//tr[@id='combinedGeneral_data"
                + editableRow + "']/td[7]/select"));
        assertTrue(selenium.isElementPresent("xpath=//tr[@id='combinedGeneral_data"
                + editableRow + "']/td[8]/textarea"));
        assertFalse(selenium.isElementPresent("xpath=//tr[@id='combinedGeneral_data"
                + uneditableRow + "']/td[7]/select"));
        assertFalse(selenium.isElementPresent("xpath=//tr[@id='combinedGeneral_data"
                + uneditableRow + "']/td[8]/textarea"));
        selenium.chooseCancelOnNextConfirmation();
        selenium.click("lineItemReviewForm_btn_save");
        assertEquals(confirm, selenium.getConfirmation());
        selenium.chooseOkOnNextConfirmation();
        clickAndWait("lineItemReviewForm_btn_save");
        assertEquals(confirm, selenium.getConfirmation());
        waitForText("Vote is required");
        selenium.select("combinedGeneralLineItem" + editableRow + "Vote", "label=Deny");
        selenium.chooseOkOnNextConfirmation();
        selenium.click("lineItemReviewForm_btn_save");
        assertEquals(confirm, selenium.getConfirmation());
        waitForPageToLoad();
        assertTrue(selenium.isTextPresent("If you do not approve the request a comment is required."));
        selenium.select("combinedGeneralLineItem" + editableRow + "Vote", "label=Approve");
        selenium.type("combinedGeneralLineItem" + editableRow + "Comment", "this is my approval explanation");
        selenium.chooseOkOnNextConfirmation();
        selenium.click("lineItemReviewForm_btn_save");
        assertEquals(confirm, selenium.getConfirmation());
        waitForPageToLoad();
        assertTrue(selenium.isTextPresent("Your vote has been saved."));
        assertFalse(selenium.isElementPresent("xpath=//tr[@id='combinedGeneral_data"
                + editableRow + "']/td[7]/select"));
        assertFalse(selenium.isElementPresent("xpath=//tr[@id='combinedGeneral_data"
                + editableRow + "']/td[8]/textarea"));

        clickAndWait("link=Back To List");
        selenium.click("actionNeededOnly");
        waitForPageToLoad();
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertTrue(selenium.isElementPresent(buttonXPath));
        assertEquals("View", selenium.getText(buttonXPath));

        clickAndWait("link=Home");
        assertTrue(selenium.isTextPresent("1 State Review"));
        clickAndWait("link=Sign Out");
    }

    private void viewRequestVotes(String status, int instIndex, String[] statusHistory) {
        loginAsUser1();
        clickAndWait("link=My Requests");
        assertTrue(selenium.isTextPresent("My Requests"));
        String xpath = "xpath=//table[@id='specimenRequest']/tbody/tr[2]/td[1]/a";
        if (!selenium.isElementPresent(xpath)) {
            if (selenium.isChecked("actionNeededOnly")) {
                selenium.click("actionNeededOnly");
                waitForPageToLoad();
            }
            selenium.select("status", "label=" + status);
            waitForPageToLoad();
        }
        clickAndWait(xpath);
        assertTrue(selenium.isTextPresent("(" + status + ")"));
        String xPath = "xpath=//td[@id='ordergeneral_votingResults" + instIndex + "']/";
        assertEquals("Request Approved", selenium.getText(xPath));
        if (statusHistory != null) {
            String history = selenium.getText("status_details");
            String[] histories = StringUtils.stripAll(StringUtils.split(history, '\n'));
            assertEquals(statusHistory.length, histories.length);
            String dateString = new SimpleDateFormat("(MM/dd/yyyy)").format(new Date());
            for (int i = 0; i < statusHistory.length; i++) {
                assertTrue(histories[i].startsWith(statusHistory[i]));
                assertTrue("Invalid status history date, expected date: " + dateString
                        + ", actual date: " + histories[i], histories[i].endsWith(dateString));
            }
        } else {
            assertFalse(selenium.isElementPresent("status_details"));
        }
    }

    /**
     * Go to edit page.
     */
    private void goToOrderEditPage(String username) {
        login(username, "tissueLocator1");
        goToOrderList();
        clickAndWait("xpath=//table[@id='shipment']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Order Fulfillment"));
    }

    /**
     * Mark as shipped.
     */
    private void markAsShipped() {
        assertTrue(selenium.isElementPresent("Link=Mark As Shipped"));
        clickAndWait("Link=Mark As Shipped");
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent("Set Shipped Date"));
        clickAndWait("btn_setShipmentDate");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        waitForText("Shipment successfully marked as shipped.");
        clickAndWait("link=Sign Out");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void simpleAddToCart(String cartLabel, boolean setQuantity) {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        verifyRequestProcessStepDisplay("search");
        clickAndWait("btn_search");
        verifyRequestProcessStepDisplay("select");
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        String textFieldXPath = "xpath=//table[@id='specimen']/tbody/tr[%d]/td[3]/input[3]";
        selenium.type(String.format(textFieldXPath, 1), Integer.toString(NUM_SEARCH_RESULTS));
        selenium.keyUp(String.format(textFieldXPath, 1), Integer.toString(NUM_SEARCH_RESULTS));
        selenium.type(String.format(textFieldXPath, 2), Integer.toString(NUM_SEARCH_RESULTS));
        selenium.keyUp(String.format(textFieldXPath, 2), Integer.toString(NUM_SEARCH_RESULTS));
        clickAndWait("btn_addtocart");
        verifyRequestProcessStepDisplay("request");
        assertTrue(selenium.isTextPresent(ClientProperties.getCartLink()));
        selenium.type("generalLineItemsArray[0].note", "default institution note");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void collapseMainSections() {
        collapseSection("Research Proposal", "requestform");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getNoIrbApprovalNote() {
        return "If your research is not approved or is pending IRB approval, you cannot "
                + "complete the request process. You can save a draft of your request until "
                + "you have IRB approval or an exemption letter. Save a copy of the draft for later use.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateFundedFields(boolean required) {
        super.validateFundedFields(required);
        assertEquals(required, selenium.isTextPresent("Institutional Grants & Contract Number must be set"));
        assertEquals(required, selenium.isTextPresent("Total Funding must be set"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyNotFundedMessage() {
        String message = "Only funded Investigators may complete the request process. "
            + "Once funding has been allocated, a request can be sent to participating states. "
            + "This preserves limited public health resources. "
            + "You can save a draft until funding has been allocated; however the DBS may no "
            + "longer be available. Save a copy of the "
            + "draft for later use.";
        assertTrue(selenium.isTextPresent(message));
    }

}
