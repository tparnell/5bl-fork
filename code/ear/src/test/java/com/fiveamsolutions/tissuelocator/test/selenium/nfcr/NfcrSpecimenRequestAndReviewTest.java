/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.nfcr;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractSpecimenRequestAndReviewTest;

/**
 * @author ddasgupta
 *
 */
public class NfcrSpecimenRequestAndReviewTest extends AbstractSpecimenRequestAndReviewTest {
    private static final int LINK_COL = 5;
    private static final int REVIEWER_COUNT = 2;

    private String orderCount;
    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp() throws Exception {
        super.setUp();
        runSqlScript("BiolocatorCartTest.sql");
    }


    /**
     * test the request submission and review workflow.
     * @throws Exception on error
     */
    @Test
    public void testSpecimenRequestAndReview() throws Exception {
        orderCount = getOrderCount();
        // login and make first request
        addToCart();
        cartValidation();
        studyDetails();
        confirm(new String[]{"Draft"});
        save();

        // test validation and make second request
        validation();
        copyFromUser("investigator");
        copyFromUser("recipient");
        navigation();

        verifySpecimenUnavailable();
        pendingRevisionMessage();
        clickAndWait("link=Sign Out");

        loginAsUser1();
        clickAndWait("link=My Requests");
        selenium.select("status", "Revision Requested");
        waitForPageToLoad();
        viewRequest(false, true, false, new String[]{"Draft", "Pending"});
        editRequest(LINK_COL);
        viewRequest(true, false, false, new String[]{"Draft", "Pending", "Pending"});
        verifyHomeToDoList(0, 0, 0);
        clickAndWait("link=Sign Out");

        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        addComment();
        verifyReviewExternalCommentDisplay();
        verifyHomeToDoList(0, 0, 0);
        clickAndWait("link=Sign Out");

        login(ClientProperties.getInstitutionalAdminEmail(), "tissueLocator1");
        verifyHomeToDoList(2, 0, 0);
        assignReviewer(1);
        verifyHomeToDoList(2, 0, 0);
        assignReviewer(2);
        verifyHomeToDoList(1, 0, 0);
        assignReviewer(0);
        clickAndWait("link=Sign Out");

        login("superadmin@example.com", "tissueLocator1");
        verifyHomeToDoList(0, 0, 0);
        verifyReadOnlyReview();
        clickAndWait("link=Sign Out");

        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        verifyHomeToDoList(0, 1, 0);
        performReview();
        verifyHomeToDoList(0, 0, 0);
        clickAndWait("link=Sign Out");

        login(ClientProperties.getAlternateScientificReviewerEmail(), "tissueLocator1");
        verifyHomeToDoList(0, 1, 0);
        declineReview();
        verifyHomeToDoList(0, 0, 0);
        clickAndWait("link=Sign Out");

        login(ClientProperties.getInstitutionalAdminEmail(), "tissueLocator1");
        verifyHomeToDoList(2, 0, 0);
        assignReviewer(2);
        verifyHomeToDoList(1, 0, 0);
        assignReviewer(0);
        finalComment();
        clickAndWait("link=Sign Out");

        loginAsAdmin();
        verifyHomeToDoList(0, 0, 1);
        notifyResearcherOfDecision();
        verifyHomeToDoList(0, 0, 0);
        verifyRequiredSpecimenValidation();
        verifyOrderCount(orderCount);

        verifyDraftValidation();
        verifyDeleteDraft();
        verifyEditErrorRequest();
        verifyDraftFlow();
    }


    /**
     * verify the view request page that you get from the review page.
     */
    protected void verifyViewRequestPageFromReview() {
        clickAndWait("link=View Request Details");
        collapseMainSections();
        assertTrue(selenium.isTextPresent("Biospecimens in Request"));
        assertTrue(selenium.isTextPresent(ID_TWO));
        assertTrue(selenium.isTextPresent(ID_THREE));
        confirmText(true, false, false, "/protected/downloadFile", new String[]{"Draft", "Pending",
                "Pending"});
        clickAndWait("link=Go Back To Request Review");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addSearchCriteria() {
        selenium.type("object.customProperties['storageTemperature']", "-400");
    }

   /**
    * {@inheritDoc}
    */
   @Override
   protected void verifyCart(boolean isResearcher) {
        assertTrue(selenium.isTextPresent("Biospecimens in Request"));
        if (isResearcher) {
            assertFalse(selenium.isTextPresent(ID_ONE));
            assertFalse(selenium.isTextPresent(ID_TWO));
            assertFalse(selenium.isTextPresent(ID_THREE));
            assertFalse(StringUtils.isBlank(selenium.getTable("request.1.1")));
            assertFalse(StringUtils.isBlank(selenium.getTable("request.2.1")));
            assertTrue(StringUtils.isBlank(selenium.getTable("request.3.1")));
        } else {
            assertTrue(selenium.isTextPresent(ID_ONE));
            assertTrue(selenium.isTextPresent(ID_TWO));
            assertTrue(selenium.isTextPresent(ID_THREE));
            assertFalse(StringUtils.isBlank(selenium.getTable("request.1.2")));
            assertFalse(StringUtils.isBlank(selenium.getTable("request.2.2")));
            assertTrue(StringUtils.isBlank(selenium.getTable("request.3.2")));
        }
        assertTrue(StringUtils.isEmpty(selenium.getTable("request.4.0")));
        assertTrue(StringUtils.isEmpty(selenium.getTable("request.4.1")));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifySpecimenUnavailable() {
        loginAsUser1();
        clickAndWait("link=My Requests");
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a");
        String[] specimenCellContent = selenium.getTable("request.1.0").split("\\s+");
        String expectedHeader = StringUtils.join(specimenCellContent, "", 1, specimenCellContent.length);
        clickAndWait("xpath=//table[@id='request']/tbody/tr[1]/td[1]/a");
        String actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
        assertFalse(selenium.isElementPresent("add_to_cart"));
        assertTrue(selenium.isElementPresent("add_to_cart_disabled"));
        clickAndWait("link=Sign Out");
    }

    private void goToRequestAdminPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Request Administration");
    }

    private void addComment() {
        goToRequestAdminPage();
        assertTrue(StringUtils.isBlank(selenium.getTable("specimenRequest.1.6")));
        assertTrue(StringUtils.isBlank(selenium.getTable("specimenRequest.2.6")));
        assertTrue(StringUtils.isBlank(selenium.getTable("specimenRequest.3.6")));
        clickAndWait("link=View");

        verifyReviewToDoList(false, false, false, false);
        clickAndWait("btn_addComment");
        selenium.selectFrame("popupFrame");
        selenium.type("comment", "This is an internal comment.");
        clickAndWait("btn_saveComment");
        waitForElementById("btn_addComment");
        assertTrue(selenium.isTextPresent("Internal Comments"));
        assertTrue(selenium.isTextPresent("This is an internal comment."));
        assertTrue(selenium.isTextPresent("Internal comment successfully saved."));
    }

    private void verifyHomeToDoList(int requestCount, int reviewCount, int decisionCount) {
        clickAndWait("link=Home");
        if (requestCount == 0) {
            assertFalse(selenium.isTextPresent("Reviewer Assignment"));
        } else if (requestCount == 1) {
            assertTrue(selenium.isTextPresent("1 Reviewer Assignment"));
            clickAndWait("link=1 Reviewer Assignment");
            selenium.select("status", "label=Pending");
            waitForPageToLoad();
            assertEquals("Pending", selenium.getSelectedLabel("status"));
            assertTrue(selenium.isTextPresent("Requests"));
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertTrue(selenium.isChecked("actionNeededOnly"));
        } else if (requestCount > 1) {
            assertTrue(selenium.isTextPresent(requestCount + " Reviewer Assignments"));
            clickAndWait("link=" + requestCount + " Reviewer Assignments");
            selenium.select("status", "label=Pending");
            waitForPageToLoad();
            assertEquals("Pending", selenium.getSelectedLabel("status"));
            assertTrue(selenium.isTextPresent("Requests"));
            assertTrue(selenium.isTextPresent("1-" + requestCount + " of " + requestCount + " Results"));
            assertTrue(selenium.isChecked("actionNeededOnly"));
        }

        if (reviewCount == 0) {
            assertFalse(selenium.isTextPresent("Consortium Review"));
        } else if (reviewCount == 1) {
            assertTrue(selenium.isTextPresent("1 Consortium Review"));
            clickAndWait("link=1 Consortium Review");
            assertTrue(selenium.isTextPresent("Requests"));
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertTrue(selenium.isChecked("actionNeededOnly"));
        } else if (reviewCount > 1) {
            assertTrue(selenium.isTextPresent(reviewCount + " Consortium Reviews"));
            clickAndWait("link=" + reviewCount + " Consortium Reviews");
            assertTrue(selenium.isTextPresent("Requests"));
            assertTrue(selenium.isTextPresent("1-" + reviewCount + " of " + reviewCount + " Results"));
            assertTrue(selenium.isChecked("actionNeededOnly"));
        }

        if (decisionCount == 0) {
            assertFalse(selenium.isTextPresent("Review Decision Notification"));
        } else if (decisionCount == 1) {
            assertTrue(selenium.isTextPresent("1 Review Decision Notification"));
            clickAndWait("link=1 Review Decision Notification");
            assertTrue(selenium.isTextPresent("Requests"));
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertTrue(selenium.isChecked("actionNeededOnly"));
        } else if (decisionCount > 1) {
            assertTrue(selenium.isTextPresent(reviewCount + " Review Decision Notifications"));
            clickAndWait("link=" + reviewCount + " Review Decision Notifications");
            assertTrue(selenium.isTextPresent("Requests"));
            assertTrue(selenium.isTextPresent("1-" + reviewCount + " of " + reviewCount + " Results"));
            assertTrue(selenium.isChecked("actionNeededOnly"));
        }

    }

    private void verifyReviewToDoList(boolean assignReviewer, boolean performReview, boolean finalComment,
            boolean notifyResearcher) {
        assertEquals(assignReviewer || performReview || finalComment || notifyResearcher,
                selenium.isTextPresent("You must take the following action(s) on this request:"));
        assertEquals(assignReviewer, selenium.isTextPresent("Assign a Scientific Reviewer"));
        assertEquals(performReview, selenium.isTextPresent("Submit your Scientific Review Vote"));
        assertEquals(finalComment, selenium.isTextPresent("Submit your Final Vote and Comment"));
        assertEquals(notifyResearcher, selenium.isTextPresent("Notify Researcher of Decision"));

        for (int i = 1; i <= REVIEWER_COUNT + 1; i++) {
            String xpath = "xpath=//table[@id='consortiumReviewTable']/tbody/tr[" + i + "]";
            assertTrue(selenium.isElementPresent(xpath));
        }
        String xpath = "xpath=//table[@id='consortiumReviewTable']/tbody/tr[" + (REVIEWER_COUNT + 2) + "]";
        assertFalse(selenium.isElementPresent(xpath));
    }

    private void verifyReadOnlyReview() {
        goToRequestAdminPage();
        assertTrue(StringUtils.isBlank(selenium.getTable("specimenRequest.1.6")));
        assertTrue(StringUtils.isBlank(selenium.getTable("specimenRequest.2.6")));
        assertTrue(StringUtils.isBlank(selenium.getTable("specimenRequest.3.6")));
        clickAndWait("link=View");
        verifyReviewToDoList(false, false, false, false);
        String expectedName1 = ClientProperties.getScientificReviewerName();
        String expectedName2 = ClientProperties.getAlternateScientificReviewerName();
        String actualName1 = selenium.getTable("consortiumReviewTable.1.0");
        String actualName2 = selenium.getTable("consortiumReviewTable.2.0");
        boolean isOrderOne = actualName1.equals(expectedName1) && actualName2.equals(expectedName2);
        boolean isOrderTwo = actualName1.equals(expectedName2) && actualName2.equals(expectedName1);
        assertTrue(isOrderOne || isOrderTwo);
    }

    private void assignReviewer(int reviewerIndex) {
        goToRequestAdminPage();
        String linkText;
        if (reviewerIndex == 0) {
            selenium.click("actionNeededOnly");
            waitForPageToLoad();
            assertTrue(StringUtils.isBlank(selenium.getTable("specimenRequest.1.6")));
            linkText = "View";
        } else {
            assertEquals("Assign Reviewer", selenium.getTable("specimenRequest.1.6"));
            linkText = "Review";
        }
        assertEquals("Assign Reviewer", selenium.getTable("specimenRequest.2.6"));
        clickAndWait("link=" + linkText);

        verifyReviewToDoList(reviewerIndex != 0, false, false, false);
        if (reviewerIndex == 1) {
            selenium.select("consortiumReviewUser" + reviewerIndex,
                    "label=" + ClientProperties.getScientificReviewerName());
            clickAndWait("btn_save");
            assertTrue(selenium.isTextPresent("Scientific Review Voting"));
            assertTrue(selenium.isTextPresent("Scientific reviewer successfully assigned."));
        } else if (reviewerIndex == 2) {
            String reviewerLabel = "label=" + ClientProperties.getAlternateScientificReviewerName();
            if (selenium.getSelectedLabel("consortiumReviewUser1").contains("Select")) {
                selenium.select("consortiumReviewUser1", reviewerLabel);
            } else {
                selenium.select("consortiumReviewUser2", reviewerLabel);
            }
            clickAndWait("btn_save");
            assertTrue(selenium.isTextPresent("Scientific Review Voting"));
            assertTrue(selenium.isTextPresent("Scientific reviewer successfully assigned."));
        }
    }

    private void performReview() {
        assertEquals("Consortium Review", selenium.getTable("specimenRequest.1.6"));
        clickAndWait("link=Review");

        verifyReviewToDoList(false, true, false, false);
        verifyViewRequestPageFromReview();

        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Vote is required"));

        selenium.select("consortiumReviewVote", "label=Deny");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("If you do not approve the request a comment is required."));

        selenium.select("consortiumReviewVote", "label=Approve");
        selenium.type("consortiumReviewComment", "this is my approval explanation");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Scientific Review Voting"));
        assertTrue(selenium.isTextPresent("Scientific review successfully saved."));
        verifyReviewToDoList(false, false, false, false);
        goToRequestAdminPage();
        assertFalse(selenium.isTextPresent("Consortium Review"));
    }

    private void declineReview() {
        assertEquals("Consortium Review", selenium.getTable("specimenRequest.1.6"));
        clickAndWait("link=Review");
        verifyReviewToDoList(false, true, false, false);
        clickAndWait("link=Decline");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Scientific review successfully declined."));
        assertFalse(selenium.isTextPresent("Consortium Review"));
    }

    private void finalComment() throws Exception {
        clickAndWait("link=Home");
        assertTrue(selenium.isElementPresent("link=1 Final Comment"));
        clickAndWait("link=1 Final Comment");
        assertEquals("Pending Final Decision", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isChecked("actionNeededOnly"));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("Final Comment"));
        clickAndWait("link=Review");
        verifyReviewToDoList(false, false, true, false);
        assertFalse(selenium.isElementPresent("consortiumReviewUser"));
        assertFalse(selenium.isElementPresent("btn_addComment"));
        assertTrue(selenium.isTextPresent("Consortium's Review Decision"));
        assertTrue(selenium.isTextPresent(ClientProperties.getFinalVoteLabel()));
        assertTrue(selenium.isElementPresent("finalVote"));
        selenium.select("finalVote", "label=- Select -");
        assertTrue(selenium.isTextPresent(ClientProperties.getFinalCommentLabel()));
        assertTrue(selenium.isElementPresent("externalComment"));
        assertTrue(selenium.isElementPresent("btn_save"));
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Consortium's Review Decision"));
        verifyReviewToDoList(false, false, true, false);
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isElementPresent("finalVote"));
        assertTrue(selenium.isTextPresent("A Final Vote must be set."));
        assertTrue(selenium.isElementPresent("externalComment"));
        assertTrue(selenium.isTextPresent("A Final Vote Comment must be set."));
        assertTrue(selenium.isElementPresent("btn_save"));
        selenium.select("finalVote", "label=Approve");
        selenium.type("externalComment", "final comment");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Consortium's Review Decision"));
        verifyReviewToDoList(false, false, false, false);
        assertTrue(selenium.isTextPresent("Final Vote and Comment successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isElementPresent("finalVote"));
        assertEquals("Approve", selenium.getSelectedLabel("finalVote"));
        assertTrue(selenium.isElementPresent("externalComment"));
        assertEquals("final comment", selenium.getValue("externalComment"));
        assertTrue(selenium.isElementPresent("btn_save"));
        clickAndWait("link=Back To List");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("0 Results found."));
        assertTrue(selenium.isTextPresent("Nothing found to display."));
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        selenium.click("actionNeededOnly");
        waitForPageToLoad();
        assertFalse(selenium.isChecked("actionNeededOnly"));
        assertFalse(selenium.isTextPresent("Final Comment"));
    }

    private void notifyResearcherOfDecision() {
        assertEquals("Notify Researcher", selenium.getTable("specimenRequest.1.6"));
        clickAndWait("link=Review");
        verifyReviewToDoList(false, false, false, true);
        assertTrue(selenium.isTextPresent("Final Vote: Approve"));
        assertTrue(selenium.isTextPresent("Final Comment"));
        assertTrue(selenium.isTextPresent("final comment"));
        assertTrue(selenium.isElementPresent("link=Notify Researcher of Decision"));
        clickAndWait("link=Notify Researcher of Decision");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("0 Results found."));
        assertTrue(selenium.isTextPresent("Nothing found to display."));
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertEquals("All", selenium.getSelectedLabel("status"));
        selenium.click("actionNeededOnly");
        waitForPageToLoad();
        assertFalse(selenium.isChecked("actionNeededOnly"));
        assertFalse(selenium.isTextPresent("Notify Researcher"));
    }
}
