/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.text.ParseException;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author ddasgupta
 *
 */
public class UserListTest extends AbstractListTest {

    private static final int COL_COUNT = 5;
    private static final int DATE_COL = 5;
    private static final int RESULT_COUNT = 72;
    private static final int BUTTON_COL = 7;

    private static final String ALL = "All Statuses";
    private static final String ACTIVE = "Active";
    private static final String INACTIVE = "Inactive";
    private static final String PENDING = "Pending";
    private static final int STATUS_COUNT = 3;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "UserListTest.sql";
    }

    /**
     * test the user list page.
     * @throws ParseException on error
     */
    public void testList() throws ParseException {
        loginAsAdmin();
        goToListPage();
        statusFilter();
        verifyExportPresent();
        sorting(COL_COUNT, "user", true, DATE_COL);
        paging("user");
        pageSize(RESULT_COUNT + ClientProperties.getDefaultUserCount());
        institutionRestriction("user", "User", "institution", 2, BUTTON_COL, true, false);
    }

    private void goToListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=User Administration");
        waitForPageToLoad();
    }

    private void statusFilter() {
        filterByStatus(ACTIVE, RESULT_COUNT / STATUS_COUNT + ClientProperties.getDefaultUserCount());
        filterByStatus(INACTIVE, RESULT_COUNT / STATUS_COUNT);
        filterByStatus(PENDING, RESULT_COUNT / STATUS_COUNT);
        filterByStatus(ALL, RESULT_COUNT + ClientProperties.getDefaultUserCount());
    }

    private void filterByStatus(String userStatus, int userCount) {
        selenium.select("status", "label=" + userStatus);
        waitForPageToLoad();
        assertEquals(userStatus, selenium.getSelectedLabel("status"));
        if (!userStatus.equals(ALL)) {
            assertEquals(userStatus, selenium.getTable("user.1.3"));
        }
        assertTrue(selenium.isTextPresent("1-20 of " + userCount + " Results"));
    }

}
