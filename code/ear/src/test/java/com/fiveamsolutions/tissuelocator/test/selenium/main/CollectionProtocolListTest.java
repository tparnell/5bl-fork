/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.text.ParseException;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractListTest;

/**
 * @author smiller
 */
public class CollectionProtocolListTest extends AbstractListTest {

    /**
     * Column count.
     */
    private static final int COL_COUNT = 8;

    /**
     * Start date column.
     */
    private static final int START_DATE_COL = 3;

    /**
     * End date column.
     */
    private static final int END_DATE_COL = 4;

    /**
     * Button column.
     */
    private static final int BUTTON_COL = 9;

    /**
     * Number of results in SQL script.
     */
    private static final int RESULT_COUNT = 54;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "CollectionProtocolListTest.sql";
    }

    /**
     * test the user list page.
     * @throws ParseException on error
     */
    public void testList() throws ParseException {
        if (ClientProperties.isProtocolAdministrationActive()) {
            loginAsAdmin();
            goToListPage();
            extIdFilter();
            verifyExportPresent();
            sorting(COL_COUNT, "protocol", false, START_DATE_COL, END_DATE_COL);
            paging("protocol");
            pageSize(RESULT_COUNT + ClientProperties.getCollectionProtocolCount());
            institutionRestriction("protocol", "Protocol", "institution", 1, BUTTON_COL, true, false);
        }        
    }

    /**
     * Go to colection protocol list page.
     */
    private void goToListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Protocol Administration");
        assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
    }

    /**
     * Filter collection protocols.
     */
    private void extIdFilter() {
        String name  = selenium.getTable("protocol.1.0");
        selenium.type("name", StringUtils.substring(name, 0, name.length() - 1));
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        selenium.type("name", StringUtils.substring(name, 2, name.length()));
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        selenium.type("name", "");
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1 of 3 Pages"));
    }
}
