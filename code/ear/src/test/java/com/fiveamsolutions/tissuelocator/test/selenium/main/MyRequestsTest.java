/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author ddasgupta
 *
 */
public class MyRequestsTest extends AbstractTissueLocatorSeleniumTest {

    private static final int[] SHIPPED_SPECIMENS = {4, 5, 8, 10};
    private static final int FULLY_PROCESSED_INDEX = 3;
    private static final int PENDING_FINAL_INDEX = 5;
    private static final int APPROVED_INDEX = 4;
    private static final int PARTIALLY_APPROVED_INDEX = 6;
    private static final int PARTIALLY_PROCESSED_INDEX = 8;
    private static final int SPECIMEN_COUNT = 10;
    private static final int PAGE_SIZE = 20;
    private static final String PROFILE_LINK_SUFFIX = "/td[1]/a[@target='_blank']";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "MyRequestsTest.sql";
    }

    /**
     * test the my requests page.
     * @throws Exception on error
     */
    public void testMyRequests() throws Exception {
        loadRequestsWithOrders();
        goToMyRequestsPage();
        draft();
        pending();
        pendingFinalDecision();
        pendingRevision();
        denied();
        approved();
        partiallyApproved();
        partiallyProcessed();
        fullyProcessed();
        verifyCartRetained();
    }

    private void loadRequestsWithOrders() throws Exception {
        if (ClientProperties.isOrderAdministrationActive()) {
            runSqlScript("MyRequestsTest-orders.sql");
        }
    }

    /**
     * Go to my request page.
     */
    private void goToMyRequestsPage() {
        loginAsUser1();
        clickAndWait("link=My Requests");
        assertEquals("All", selenium.getSelectedLabel("status"));
        if (ClientProperties.isOrderAdministrationActive()) {
            assertTrue(selenium.isTextPresent("1-9 of 9 Results"));
        } else {
            assertTrue(selenium.isTextPresent("1-6 of 6 Results"));
        }
    }

    /**
     * Tests draft requests.
     */
    private void draft() {
        selenium.select("status", "label=Draft");
        waitForPageToLoad();
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(StringUtils.isBlank(selenium.getTable("specimenRequest.1.6")));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[7]/a"));
    }

    /**
     * Tests pending requests.
     */
    private void pending() {
        goToDetailsPage("Pending", true);
        assertTrue(selenium.isElementPresent(getId(1)));
        if (ClientProperties.isAggregateSearchResultsActive()) {
            assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + "s in Request"));
            assertFalse(selenium.isElementPresent("Unavailable " + ClientProperties.getBiospecimenTerm() + "s"));
        } else {
            assertTrue(selenium.isElementPresent("link=" + ClientProperties.getBiospecimenTerm()
                    + "s in Request"));
            assertFalse(selenium.isElementPresent("link=Unavailable "
                    + ClientProperties.getBiospecimenTerm() + "s"));
            collapseSection(ClientProperties.getBiospecimenTerm() + "s in Request", getCartPrefix() + "cart");
        }
        assertFalse(selenium.isTextPresent("test 1"));
        assertTrue(selenium.isElementPresent(getDataRowXPath(1, 1, "unreviewed")));
        assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                selenium.isElementPresent(getDataRowXPath(1, 1, "unreviewed") + PROFILE_LINK_SUFFIX));
        assertFalse(selenium.isElementPresent(getDataRowXPath(1, 2, "unreviewed")));
        assertFalse(selenium.isElementPresent("shipment"));
        assertFalse(selenium.isElementPresent("link=Orders"));
        verifyNoVotingResultsDisplayed();
        clickAndWait("link=Back");
    }

    /**
     * Tests pending final decision requests.
     */
    private void pendingFinalDecision() {
        goToDetailsPage("Pending Final Decision", true);
        if (!ClientProperties.isAggregateSearchResultsActive()) {
            assertTrue(selenium.isElementPresent(getId(PENDING_FINAL_INDEX)));
            assertTrue(selenium.isElementPresent("link=" + ClientProperties.getBiospecimenTerm() + "s in Request"));
            assertTrue(selenium.isElementPresent(getDataRowXPath(PENDING_FINAL_INDEX, 1, "reviewed")));
            String profilePath = getDataRowXPath(PENDING_FINAL_INDEX, 1, "reviewed") + PROFILE_LINK_SUFFIX;
            assertEquals(ClientProperties.isInstitutionProfileUrlActive(), selenium.isElementPresent(profilePath));
        }
        assertFalse(selenium.isElementPresent("link=Unavailable " + ClientProperties.getBiospecimenTerm() + "s"));
        assertFalse(selenium.isTextPresent("test 7"));
        assertFalse(selenium.isElementPresent(getDataRowXPath(PENDING_FINAL_INDEX, 2, "reviewed")));
        assertFalse(selenium.isElementPresent("shipment"));
        assertFalse(selenium.isElementPresent("link=Orders"));
        verifyNoVotingResultsDisplayed();
        clickAndWait("link=Back");
    }

    /**
     * Tests pending revisions requests.
     */
    private void pendingRevision() {
        goToDetailsPage("Revision Requested", true);
        assertTrue(selenium.isElementPresent(getId(2)));
        if (ClientProperties.isAggregateSearchResultsActive()) {
            assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + "s in Request"));
            assertFalse(selenium.isElementPresent("Unavailable " + ClientProperties.getBiospecimenTerm() + "s"));
        } else {
            assertTrue(selenium.isElementPresent("link=" + ClientProperties.getBiospecimenTerm()
                    + "s in Request"));
            assertFalse(selenium.isElementPresent("link=Unavailable "
                    + ClientProperties.getBiospecimenTerm() + "s"));
        }
        assertFalse(selenium.isTextPresent("test 2"));
        assertTrue(selenium.isElementPresent(getDataRowXPath(2, 1, "reviewed")));
        assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                selenium.isElementPresent(getDataRowXPath(2, 1, "reviewed") + PROFILE_LINK_SUFFIX));
        assertFalse(selenium.isElementPresent(getDataRowXPath(2, 2, "reviewed")));
        assertFalse(selenium.isElementPresent("shipment"));
        assertFalse(selenium.isElementPresent("link=Orders"));

        if (ClientProperties.isDisplayRequestReviewVotes()) {
            assertTrue(selenium.isElementPresent("xpath=//td[@class='']"));
            assertTrue(selenium.isTextPresent("Request Revision Requested"));
            assertTrue(selenium.isTextPresent("revision comment"));
        } else {
            verifyFinalCommentDisplay("Revision Requested", "please revise");
        }

        clickAndWait("link=Back");
    }

    /**
     * Tests denied requests.
     */
    private void denied() {
        goToDetailsPage("Denied", true);
        assertTrue(selenium.isElementPresent(getId(FULLY_PROCESSED_INDEX)));
        boolean orderAdministrationActive = ClientProperties.isOrderAdministrationActive();
        if (!ClientProperties.isAggregateSearchResultsActive()) {
            assertEquals(!orderAdministrationActive, selenium.isElementPresent("link="
                    + ClientProperties.getBiospecimenTerm() + "s in Request"));
            assertEquals(orderAdministrationActive, selenium.isElementPresent("link=Unavailable "
                    + ClientProperties.getBiospecimenTerm() + "s"));
            if (orderAdministrationActive) {
                collapseSection("Unavailable " + ClientProperties.getBiospecimenTerm() + "s",
                        getCartPrefix() + "cart");
            }
        }
        assertFalse(selenium.isTextPresent("test 3"));
        assertTrue(selenium.isElementPresent(getDataRowXPath(FULLY_PROCESSED_INDEX, 1, "reviewed")));
        assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                selenium.isElementPresent(getDataRowXPath(FULLY_PROCESSED_INDEX, 1, "reviewed") + PROFILE_LINK_SUFFIX));
        assertFalse(selenium.isElementPresent(getDataRowXPath(FULLY_PROCESSED_INDEX, 2, "reviewed")));
        assertFalse(selenium.isElementPresent("shipment"));
        assertFalse(selenium.isElementPresent("link=Orders"));

        if (ClientProperties.isDisplayRequestReviewVotes()) {
            assertTrue(selenium.isElementPresent("xpath=//td[@class='denied']"));
            assertTrue(selenium.isTextPresent("Request Denied"));
            assertTrue(selenium.isTextPresent("denial comment"));
        } else {
            verifyFinalCommentDisplay("Denied", "denied");
        }

        clickAndWait("link=Back");
    }

    /**
     * Tests approved requests.
     */
    private void approved() {
        goToDetailsPage("Approved", true);
        boolean orderAdministrationActive = ClientProperties.isOrderAdministrationActive();
        boolean displayVotes = ClientProperties.isDisplayRequestReviewVotes();
        assertEquals(!orderAdministrationActive, selenium.isElementPresent(getId(APPROVED_INDEX)));
        assertEquals(displayVotes, selenium.isElementPresent("ordergeneral_title"));
        if (!ClientProperties.isAggregateSearchResultsActive()) {
            assertEquals(!orderAdministrationActive || displayVotes, selenium.isElementPresent("link="
                    + ClientProperties.getBiospecimenTerm() + "s in Request"));
            if (displayVotes) {
                collapseSection("" + ClientProperties.getBiospecimenTerm() + "s in Request", "ordercart");
            } else if (!orderAdministrationActive) {
                collapseSection("" + ClientProperties.getBiospecimenTerm() + "s in Request", getCartPrefix()
                        + "cart");
            }
        }
        assertFalse(selenium.isElementPresent("link=Unavailable " + ClientProperties.getBiospecimenTerm() + "s"));
        assertFalse(selenium.isTextPresent("test 4"));

        assertEquals(orderAdministrationActive, selenium.isElementPresent("shipment"));
        assertEquals(orderAdministrationActive, selenium.isElementPresent("link=Orders"));
        if (orderAdministrationActive) {
            collapseSection("Orders", "orders");
            assertTrue(selenium.isTextPresent("Shipment #"));
            assertTrue(selenium.isTextPresent("Sending " + ClientProperties.getInstitutionTerm()));
            assertTrue(selenium.isTextPresent("Status"));
            assertTrue(selenium.isTextPresent("Tracking Number"));
            assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " #"));
            assertTrue(selenium.isTextPresent("Pending Shipment"));
            assertTrue(selenium.isElementPresent("link=FedEx"));
            assertTrue(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]"));
            assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                    selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]/td[2]/a"));
            assertTrue(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]/td[5]/a"));
            assertFalse(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[2]"));
            verifyShipment(SHIPPED_SPECIMENS[0], "FedEx");
        }

        verifyApprovedVotingResultsDisplayed("Approved");
        clickAndWait("link=Back");
    }

    /**
     * Tests partially approved requests.
     */
    private void partiallyApproved() {
        goToDetailsPage("Partially Approved", ClientProperties.isOrderAdministrationActive());
        if (ClientProperties.isOrderAdministrationActive()) {
            assertTrue(selenium.isElementPresent(getId(PARTIALLY_APPROVED_INDEX)));
            if (!ClientProperties.isAggregateSearchResultsActive()) {
                assertEquals(ClientProperties.isDisplayRequestReviewVotes(),
                        selenium.isElementPresent("link=" + ClientProperties.getBiospecimenTerm() + "s in Request"));
                assertTrue(selenium.isElementPresent("link=Unavailable " + ClientProperties.getBiospecimenTerm()
                        + "s"));
            }
            assertFalse(selenium.isTextPresent("test 6"));
            assertFalse(selenium.isTextPresent("test 5"));
            assertTrue(selenium.isElementPresent(getDataRowXPath(PARTIALLY_APPROVED_INDEX, 1, "reviewed")));
            String profilePath = getDataRowXPath(PARTIALLY_APPROVED_INDEX, 1, "reviewed") + PROFILE_LINK_SUFFIX;
            assertEquals(ClientProperties.isInstitutionProfileUrlActive(), selenium.isElementPresent(profilePath));
            assertFalse(selenium.isElementPresent(getDataRowXPath(PARTIALLY_APPROVED_INDEX, 2, "reviewed")));

            assertTrue(selenium.isElementPresent("shipment"));
            assertTrue(selenium.isElementPresent("link=Orders"));
            assertTrue(selenium.isTextPresent("Shipment #"));
            assertTrue(selenium.isTextPresent("Sending " + ClientProperties.getInstitutionTerm()));
            assertTrue(selenium.isTextPresent("Status"));
            assertTrue(selenium.isTextPresent("Tracking Number"));
            assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " #"));
            assertTrue(selenium.isTextPresent("Pending Shipment"));
            assertTrue(selenium.isTextPresent("Other"));
            assertTrue(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]"));
            assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                    selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]/td[2]/a"));
            assertTrue(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]/td[5]/a"));
            assertFalse(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[2]"));

            verifyApprovedVotingResultsDisplayed("Partially Approved");
            verifyShipment(SHIPPED_SPECIMENS[1], "Other");
            clickAndWait("link=Back");
        }
    }


    /**
     * Tests partially processed requests.
     */
    private void partiallyProcessed() {
        goToDetailsPage("Partially Processed", ClientProperties.isOrderAdministrationActive());
        if (ClientProperties.isOrderAdministrationActive()) {
            assertFalse(selenium.isElementPresent(getId(PARTIALLY_PROCESSED_INDEX)));
            if (!ClientProperties.isAggregateSearchResultsActive()) {
                assertEquals(ClientProperties.isDisplayRequestReviewVotes(),
                        selenium.isElementPresent("link=" + ClientProperties.getBiospecimenTerm() + "s in Request"));
                assertFalse(selenium.isElementPresent("link=Unavailable " + ClientProperties.getBiospecimenTerm()
                        + "s"));
            }
            assertFalse(selenium.isTextPresent("test 8"));
            assertFalse(selenium.isTextPresent("test 9"));

            assertTrue(selenium.isElementPresent("shipment"));
            assertTrue(selenium.isElementPresent("link=Orders"));
            collapseSection("Orders", "orders");
            assertTrue(selenium.isTextPresent("Shipment #"));
            assertTrue(selenium.isTextPresent("Sending " + ClientProperties.getInstitutionTerm()));
            assertTrue(selenium.isTextPresent("Status"));
            assertTrue(selenium.isTextPresent("Tracking Number"));
            assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " #"));
            assertTrue(selenium.isTextPresent("Processed"));
            assertTrue(selenium.isElementPresent("link=FedEx"));
            assertTrue(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]"));
            assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                    selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]/td[2]/a"));
            assertTrue(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]/td[6]/a"));
            assertFalse(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[2]"));

            verifyApprovedVotingResultsDisplayed("Partially Approved");
            verifyShipment(SHIPPED_SPECIMENS[2], "FedEx");
            clickAndWait("link=Back");
        }
    }

    /**
     * Tests fully processed requests.
     */
    private void fullyProcessed() {
        goToDetailsPage("Fully Processed", ClientProperties.isOrderAdministrationActive());
        if (ClientProperties.isOrderAdministrationActive()) {
            assertFalse(selenium.isElementPresent(getId(FULLY_PROCESSED_INDEX)));
            if (!ClientProperties.isAggregateSearchResultsActive()) {
                assertEquals(ClientProperties.isDisplayRequestReviewVotes(),
                        selenium.isElementPresent("link=" + ClientProperties.getBiospecimenTerm() + "s in Request"));
                assertFalse(selenium.isElementPresent("link=Unavailable " + ClientProperties.getBiospecimenTerm()
                        + "s"));
            }
            assertFalse(selenium.isTextPresent("test 10"));

            assertTrue(selenium.isElementPresent("shipment"));
            assertTrue(selenium.isElementPresent("link=Orders"));
            collapseSection("Orders", "orders");
            assertTrue(selenium.isTextPresent("Shipment #"));
            assertTrue(selenium.isTextPresent("Sending " + ClientProperties.getInstitutionTerm()));
            assertTrue(selenium.isTextPresent("Status"));
            assertTrue(selenium.isTextPresent("Shipped (??/??/????)"));
            assertTrue(selenium.isTextPresent("Tracking Number"));
            assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " #"));
            assertTrue(selenium.isTextPresent("Processed"));
            assertTrue(selenium.isElementPresent("link=FedEx"));
            assertTrue(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]"));
            assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                    selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]/td[2]/a"));
            assertTrue(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[1]/td[6]/a"));
            assertFalse(selenium.isElementPresent("xpath=//table[@id='shipment']/tbody/tr[2]"));

            verifyApprovedVotingResultsDisplayed("Partially Approved");
            verifyShipment(SHIPPED_SPECIMENS[FULLY_PROCESSED_INDEX], "FedEx");
            clickAndWait("link=Back");
        }
    }

    private void goToDetailsPage(String status, boolean resultsExpected) {
        selenium.select("status", "label=" + status);
        waitForPageToLoad();
        if (!resultsExpected) {
            assertTrue(selenium.isTextPresent("0 Results found."));
            return;
        }

        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        if (StringUtils.equals(status, "Revision Requested")) {
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertEquals("Revision Required", selenium.getTable("specimenRequest.1.6"));
        } else if (StringUtils.equals(status, "Patially Processed") || StringUtils.equals(status, "Fully Processed")) {
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertEquals("Record Receipt Quality", selenium.getTable("specimenRequest.1.6"));
        }

        assertTrue(selenium.isElementPresent("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a"));
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a");
        assertTrue(selenium.isTextPresent("Request Details"));
        boolean showPILegalFields = ClientProperties.getDisplayPILegalFields();
        assertEquals(showPILegalFields, selenium.isTextPresent("I certify that the Principal investigator"));
        assertEquals(showPILegalFields, selenium.isElementPresent("investigatorCertifiedLabel"));
        assertEquals(showPILegalFields, selenium.isTextPresent("Has the Principal Investigator been subject"));
        assertEquals(showPILegalFields, selenium.isElementPresent("investigatorInvestigatedLabel"));
        assertFalse(selenium.isTextPresent("Explanation"));
    }

    /**
     * Verifies shipment.
     * @param specimenIndex the specimen index
     * @param shippingMethod the shipping method
     */
    private void verifyShipment(int specimenIndex, String shippingMethod) {
        String shipmentId = selenium.getTable("shipment.1.0");
        assertTrue(selenium.isElementPresent("link=" + shipmentId));
        clickAndWait("link=" + shipmentId);
        assertTrue(selenium.isTextPresent("Order " + shipmentId));
        collapseSection("Shipping Information", "shipping_details");
        collapseSection("Study, Protocol, and P.I.", "study_details");
        assertFalse(selenium.isTextPresent("test " + specimenIndex));
        if (ClientProperties.isAggregateSearchResultsActive()) {
            assertTrue(selenium.isTextPresent("note " + specimenIndex));
        } else {
            assertTrue(selenium.isTextPresent(ClientProperties.getDiseases()[0]));
            assertTrue(selenium.isTextPresent(specimenIndex + "00.00 milligram"));
        }
        assertTrue(selenium.isTextPresent(shippingMethod));
        if (!"Other".equals(shippingMethod)) {
            assertTrue(selenium.isTextPresent("Tracking Link"));
            assertTrue(selenium.isElementPresent("link=" + shippingMethod));
        } else {
            assertTrue(selenium.isTextPresent("Tracking Number"));
        }

        assertTrue(selenium.isTextPresent("test instructions"));
        assertEquals(ClientProperties.isDisplayPrices() && !ClientProperties.isAggregateSearchResultsActive(),
                selenium.isTextPresent("To be negotiated"));
        assertEquals(ClientProperties.isDisplayPrices(), selenium.isTextPresent("$12.34"));
        assertEquals(ClientProperties.isDisplayPrices(), selenium.isTextPresent("$56.78"));
        assertTrue(selenium.isTextPresent("test user first test user last"));
        assertTrue(selenium.isTextPresent(ClientProperties.getDefaultConsortiumMemberName()));
        assertTrue(selenium.isTextPresent("123 test street"));
        assertTrue(selenium.isTextPresent("test city Maryland, 12345"));
        assertTrue(selenium.isTextPresent("United States"));
        assertTrue(selenium.isTextPresent("test@example.com"));
        assertTrue(selenium.isTextPresent("1234567890"));
        assertTrue(selenium.isTextPresent("test notes"));
        clickAndWait("link=Back");

    }

    /**
     * Verifies that the cart is retained when you view another one of your requests.
     */
    private void verifyCartRetained() {
        if (!ClientProperties.isAggregateSearchResultsActive()) {
            String[] cartLabelParts = StringUtils.split(ClientProperties.getCartLink());
            String cartLabel = cartLabelParts[cartLabelParts.length - 1].toLowerCase();
            int specimenCount = SPECIMEN_COUNT;
            int totalSpecimens = ClientProperties.getFluidSpecimenCount() + ClientProperties.getTissueSpecimenCount()
                + ClientProperties.getCellsSpecimenCount() + ClientProperties.getMolecularSpecimenCount();
            if (totalSpecimens > PAGE_SIZE) {
                specimenCount = PAGE_SIZE;
            }

            selectSearchMenu();
            clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
            clickAndWait("btn_search");
            selenium.click("selectall");
            clickAndWait("btn_addtocart");
            assertTrue(selenium.isTextPresent("Added " + specimenCount + " "
                    + ClientProperties.getSpecimenTermLowerCase() + "(s) to your " + cartLabel));
            assertTrue(selenium.isElementPresent(String.format("xpath=//table[@id='lineItem']/tbody/tr[%d]",
                    specimenCount)));
            clickAndWait("link=My Requests");
            goToDetailsPage("Denied", true);
            clickAndWait("link=" + ClientProperties.getCartLink());
            assertTrue(selenium.isElementPresent(String.format("xpath=//table[@id='lineItem']/tbody/tr[%d]",
                    specimenCount)));
        }
    }

    private String getId(int specimenIndex) {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            if (specimenIndex == 1) {
                return "unreviewedspecific_title";
            } else if (specimenIndex == 2) {
                return "reviewedgeneral_title";
            } else if (specimenIndex == APPROVED_INDEX) {
                return "unreviewedgeneral_title";
            } else if (specimenIndex == PENDING_FINAL_INDEX) {
                return "reviewedspecific_title";
            } else if (specimenIndex == PARTIALLY_APPROVED_INDEX) {
                return "reviewedgeneral_title";
            } else if (specimenIndex == PARTIALLY_PROCESSED_INDEX) {
                return "unreviewedgeneral_title";
            } else if (specimenIndex == FULLY_PROCESSED_INDEX) {
                return "reviewedspecific_title";
            }
        }
        return "request";
    }

    private String getDataRowXPath(int specimenIndex, int rowIndex, String prefix) {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            if (specimenIndex % 2 == 0) {
                return "xpath=//tr[@id='" + prefix + "general_data" + (rowIndex - 1) + "']";
            }
            return "xpath=//tr[@id='" + prefix + "specific_data" + (rowIndex - 1) + "']";
        }
        return "xpath=//table[@id='request']/tbody/tr[" + rowIndex + "]";
    }

    private String getCartPrefix() {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            return "aggregate";
        }
        return "";
    }

    private void verifyNoVotingResultsDisplayed() {
        if (ClientProperties.isDisplayRequestReviewVotes()) {
            assertFalse(selenium.isElementPresent("xpath=//td[@class='approved']"));
            assertFalse(selenium.isElementPresent("xpath=//td[@class='denied']"));
            assertFalse(selenium.isElementPresent("xpath=//td[@class='']"));
        } else {
            assertFalse(selenium.isTextPresent("Consortium's Review Decision"));
            assertFalse(selenium.isTextPresent(ClientProperties.getFinalVoteLabel()));
            assertFalse(selenium.isTextPresent(ClientProperties.getFinalCommentLabel()));
        }
    }

    private void verifyApprovedVotingResultsDisplayed(String finalVote) {
        if (ClientProperties.isDisplayRequestReviewVotes()) {
            assertTrue(selenium.isElementPresent("xpath=//td[@class='approved']"));
            assertTrue(selenium.isTextPresent("Request Approved"));
        } else {
            verifyFinalCommentDisplay(finalVote, finalVote.toLowerCase());
        }
    }

    private void verifyFinalCommentDisplay(String finalVote, String finalComment) {
        assertTrue(selenium.isTextPresent("Consortium's Review Decision"));
        assertTrue(selenium.isTextPresent(ClientProperties.getFinalVoteLabel()));
        assertTrue(selenium.isTextPresent(finalVote));
        assertTrue(selenium.isTextPresent(ClientProperties.getFinalCommentLabel()));
        assertTrue(selenium.isTextPresent(finalComment));
    }
}
