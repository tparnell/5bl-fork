/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.lang.xwork.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author ddasgupta
 *
 */
public class AbrcMtaAdministrationTest  extends AbstractTissueLocatorSeleniumTest {

    private static final String VERSION = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
    private static final String FILE_NAME = "cleanTables.sql";
    private static final String SUCCESS_MESSAGE = "Material Transfer Agreement successfully saved.";
    private static final String TITLE = "Material Transfer Agreement (MTA) Administration";
    private static final String CONFIRM_MESSAGE = "Warning: Uploading a new version of the MTA will make "
        + "every institution with a previous MTA in place become out-of-date. This action cannot be undone. "
        + "Are you sure you want to continue?";

    /**
     * test the mta administration page.
     * @throws Exception on error
     */
    @Test
    public void testMtaAdministration() throws Exception {
        loginAsAdmin();
        goToListPage();
        requiredValidation();
        create("first", 1);
        create("second", 2);
    }

    private void goToListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=MTA Administration");
        assertTrue(selenium.isTextPresent("Add New Consortium MTA"));
        assertTrue(selenium.isTextPresent("Upload Consortium MTA"));
        assertTrue(selenium.isTextPresent("Download Consortium MTA"));
        assertTrue(selenium.isTextPresent(TITLE));
        assertTrue(selenium.isTextPresent("Nothing found to display."));
    }

    private void requiredValidation() {
        selenium.chooseOkOnNextConfirmation();
        clickAndWait("btn_save");
        assertEquals(CONFIRM_MESSAGE, selenium.getConfirmation());
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Version # must be set"));
        assertTrue(selenium.isTextPresent("MTA File must be set"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='mta']/tbody/tr[1]"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='mta']/tbody/tr[2]"));
    }

    private void create(String prefix, int resultCount) throws Exception {
        enterData();
        assertTrue(selenium.isTextPresent(SUCCESS_MESSAGE));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(TITLE));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='mta']/tbody/tr[" + resultCount + "]"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='mta']/tbody/tr[" + (resultCount + 1) + "]"));
        assertTrue(selenium.isTextPresent(VERSION));
        assertTrue(selenium.isElementPresent("link=Download"));
        assertEquals(VERSION, selenium.getTable("mta.1.0"));
        String fileTarget = selenium.getAttribute("xpath=//table[@id='mta']/tbody/tr[1]/td[3]/a@href");
        assertTrue(Pattern.matches(".*/protected/downloadFile[^/]*.*" + FILE_NAME + "[^/]*.*", fileTarget));
        assertTrue(StringUtils.isBlank(selenium.getValue("version")));
        assertTrue(StringUtils.isBlank(selenium.getValue("document")));
    }

    private void enterData() throws Exception {
        selenium.type("version", VERSION);
        selenium.type("document", new File(ClassLoader.getSystemResource(FILE_NAME).toURI()).toString());
        selenium.chooseCancelOnNextConfirmation();
        selenium.click("btn_save");
        assertEquals(CONFIRM_MESSAGE, selenium.getConfirmation());
        selenium.chooseOkOnNextConfirmation();        
        clickAndWait("btn_save");
        assertEquals(CONFIRM_MESSAGE, selenium.getConfirmation());  
    }
}
