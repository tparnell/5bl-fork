/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractHomePageTest;

/**
 * @author ddasgupta
 *
 */
public class NbstrnHomePageTest extends AbstractHomePageTest {

    /**
     * test the user home page.
     */
    @Test
    public void testHomePage() {
        goToHomePage();
        browseByDisease();
        browseByInstitution();
        simpleSearch();
        staticFaq();
    }

    /**
     * browse by institution.
     */
    protected void browseByInstitution() {
        String[] institutions = ClientProperties.getInstitutions();
        int[] counts = ClientProperties.getInstitutionCounts();
        for (int i  = 0; i < institutions.length; i++) {
            clickAndWait("link=Home");
            String option = institutions[i];
            selenium.select("institution", option);
            waitForPageToLoad();
            assertTrue(selenium.isTextPresent(SEARCH_TITLE));
            assertEquals("California", selenium.getSelectedLabels("multiSelectParamMap['externalIdAssigner.id']")[0]);
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertEquals("" + counts[i], selenium.getTable("specimen.1.1"));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getDiseaseLink(String disease, int count) {
        return "link=" + disease;
    }

    /**
     * Link to search pages.
     */
    protected void simpleSearch() {
        clickAndWait("link=Home");
        clickAndWait("btn_home_search_collection");
        assertTrue(selenium.isTextPresent(ClientProperties.getSimpleSearchTitle()));
        assertTrue(selenium.isChecked("simpleSearch"));
        clickAndWait("link=Home");
        clickAndWait("btn_home_search_advanced");
        assertTrue(selenium.isTextPresent(SEARCH_TITLE));
        assertFalse(selenium.isTextPresent(ClientProperties.getSimpleSearchTitle()));
        assertFalse(selenium.isChecked("simpleSearch"));
    }

    /**
     * Tests the FAQ page to ensure that div's are hiding and showing as expected.
     */
    private void staticFaq()  {
        String divLink = "Why is a state not listed as a participant in this project?";
        String divId = "faq_states";
        String altDivLink = "How long will it take to get my request approved?";
        String altDivID = "faq_time_to_approval";
        mouseOverAndPause("link=About the Virtual Repository");
        clickAndWait("link=FAQ");

        assertFalse(selenium.isVisible(divId));
        assertFalse(selenium.isVisible(altDivID));

        selenium.click("link=" + divLink);
        pause(DELAY);
        assertTrue(selenium.isVisible(divId));

        selenium.click("link=" + altDivLink);
        assertTrue(selenium.isVisible(altDivID));
        
        selenium.click("link=" + divLink);
        pause(DELAY);
        assertFalse(selenium.isVisible(divId));
        assertTrue(selenium.isVisible(altDivID));
        
        selenium.click("link=" + altDivLink);
        pause(DELAY);
        assertFalse(selenium.isVisible(divId));
        assertFalse(selenium.isVisible(altDivID));

    }
}
