/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium;

import java.text.NumberFormat;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;

/**
 * @author smiller
 */
public abstract class AbstractTissueLocatorSeleniumTest extends AbstractSeleniumTest {

    /**
     * password.
     */
    protected static final String PASSWORD = "tissueLocator1";

    /**
     * basic error message.
     */
    protected static final String ERROR_MESSAGE = "Sorry, there is a problem with one or more fields in the form.";

    /**
     * Default delay.
     */
    protected static final int DELAY = 1500;

    /**
     * Short delay.
     */
    protected static final int SHORT_DELAY = 100;

    /**
     * Long delay.
     */
    protected static final int LONG_DELAY = 6000;

    /**
     * Context root.
     */
    protected static final String CONTEXT_ROOT = "/tissuelocator-web";

    /**
     * Default page size.
     */
    protected static final int PAGE_SIZE = 20;

    private static final int FILTER_TIMEOUT = LONG_DELAY;
    private static final int AUTOCOMPLETE_TIMEOUT = 60000;
    private static final int AUTOCOMPLETE_SLEEP_TIME = 100;
    private static final int AUTOCOMPLETE_DELAY = 500;
    private static final int PAGE_TIMEOUT = 180000;
    private static final int TIME_UNIT_COUNT = 4;

    /**
     * Zip code length.
     */
    protected static final int ZIP_LENGTH = 5;
    /**
     * Phone field length.
     */
    protected static final int PHONE_LENGTH = 7;
    /**
     * Phone extension length.
     */
    protected static final int EXTENSION_LENGTH = 7;

    /**
     * login to the app.
     *
     * @param username the username.
     * @param password the password
     * @param shouldWork expected pass or fail?
     */
    protected void login(String username, String password, boolean shouldWork) {
        selenium.open(CONTEXT_ROOT);
        waitForPageToLoad();

        // verify that the svn info was properly substituted
        assertTrue(selenium.isTextPresent("Version"));
        assertFalse(selenium.isTextPresent("${svn.version}"));
        assertFalse(selenium.isTextPresent("${svn.revision}"));

        if (selenium.isElementPresent("link=Sign Out")) {
            testLoggedIn(true);
        } else {
            testLoggedIn(false);
            assertTrue(selenium.isTextPresent("Sign In"));
            assertTrue(selenium.isTextPresent("Email Address"));
            assertTrue(selenium.isTextPresent("Password"));
            assertTrue(selenium.isElementPresent("j_username"));
            assertTrue(selenium.isElementPresent("j_password"));
            assertTrue(selenium.isElementPresent("submitLogin"));

            selenium.type("j_username", username);
            selenium.type("j_password", password);
            clickAndWait("submitLogin");
        }

        if (shouldWork) {
            testLoggedIn(true);
            assertTrue(selenium.isTextPresent("Home"));
        } else {
            testLoggedIn(false);
            assertTrue(selenium.isTextPresent("Invalid username and/or password, please try again."));
        }
    }

    private void testLoggedIn(boolean loggedIn) {
        assertEquals(loggedIn, selenium.isTextPresent("Welcome,"));
        assertTrue(selenium.isElementPresent("link=Home"));
        assertEquals(loggedIn, selenium.isElementPresent("link=Sign Out"));
        assertEquals(!loggedIn, selenium.isElementPresent("link=Sign In"));
        assertEquals(!loggedIn, selenium.isElementPresent("link=Forgot password?"));
    }

    /**
     * Go to the biospecimen search menu.
     */
    protected void selectSearchMenu() {
        String searchMenu = ClientProperties.getBiospecimenSearchMenu();
        pause(SHORT_DELAY);
        if (StringUtils.isNotEmpty(searchMenu)) {
            assertTrue(selenium.isTextPresent(searchMenu));
            mouseOverAndPause("link=" + searchMenu);
        }
    }

    /**
     * login expecting success.
     *
     * @param username the username.
     * @param password the password.
     */
    protected void login(String username, String password) {
        login(username, password, true);
    }

    /**
     * login as user1 expecting success.
     */
    protected void loginAsUser1() {
        login(ClientProperties.getResearcherEmail(), PASSWORD, true);
    }

    /**
     * login as admin, expecting success.
     */
    protected void loginAsAdmin() {
        login(ClientProperties.getConsortiumAdminEmail(), PASSWORD, true);
    }

    /**
     * test collapsing and expanding of a div.
     *
     * @param linkText the link to trigger the expand and collapse
     * @param divId the id of the div to be shown or hidden
     */
    protected void collapseSection(String linkText, String divId) {
        assertTrue(selenium.isVisible(divId));
        assertTrue(selenium.isElementPresent("link=" + linkText));
        selenium.click("link=" + linkText);
        pause(DELAY);
        assertFalse(selenium.isVisible(divId));
        selenium.click("link=" + linkText);
        pause(DELAY);
        assertTrue(selenium.isVisible(divId));
    }

    /**
     * Tests the results count.
     *
     * @param resultCount the number of results
     * @param pageSize the page size
     */
    protected void resultsCount(int resultCount, int pageSize) {
        String formatedResults = NumberFormat.getInstance().format(resultCount);
        if (resultCount == 0) {
            assertTrue(selenium.isTextPresent("0 Results found."));
        } else if (resultCount == 1) {
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        } else if (pageSize < resultCount) {
            assertTrue(selenium.isTextPresent("1-" + pageSize + " of " + formatedResults + " Results"));
        } else {
            assertTrue(selenium.isTextPresent("1-" + resultCount + " of " + formatedResults + " Results"));
        }
    }

    /**
     * Shows only test results.
     */
    protected void showOnlyTestResults() {
        selenium.type("externalId", "test");
        clickAndWait("btn_search");
    }

    /**
     * Shows all results.
     */
    protected void showAllResults() {
        selenium.type("externalId", "");
        clickAndWait("btn_search");
    }

    /**
     * Go to the order administration page, depending on whether or order administration is active.
     */
    protected void goToOrderList() {
        mouseOverAndPause("link=Administration");
        assertTrue(selenium.isElementPresent("link=" + ClientProperties.getOrderAdministrationLink()));
        clickAndWait("link=" + ClientProperties.getOrderAdministrationLink());
    }

    /**
     * select an option from the jQuery autocomplete widget.
     *
     * @param autoCompleteId the id of the autocomplete
     * @param selectString the string to type in and select
     */
    protected void selectFromAutocomplete(String autoCompleteId, String selectString) {
        selectFromAutocomplete(autoCompleteId, selectString, selectString);
    }

    /**
     * select an option from the jQuery autocomplete widget.
     *
     * @param autoCompleteId the id of the autocomplete
     * @param searchString the string to type in
     * @param elementSelect the string to select
     */
    protected void selectFromAutocomplete(String autoCompleteId, String searchString, String elementSelect) {
        selectFromAutocomplete(autoCompleteId, searchString, elementSelect, null);
    }

    /**
     * Select an option from the jQuery autocomplete widget. Allows
     * the specification of location in a page of the select list,
     * for cases when a list is repeated in a single page.
     * @param autoCompleteId the id of the autocomplete
     * @param searchString the string to type in
     * @param elementSelect the string to select
     * @param pageIndex The index in the page of the select list to choose from
     * (starting at 1).
     */
    protected void selectFromAutocomplete(String autoCompleteId, String searchString,
            String elementSelect, Integer pageIndex) {
        waitForElementById(autoCompleteId);
        pause(AUTOCOMPLETE_DELAY);
        selenium.typeKeys(autoCompleteId, searchString);
        for (int timeSpent = 0;; timeSpent += AUTOCOMPLETE_SLEEP_TIME) {
            if (timeSpent >= AUTOCOMPLETE_TIMEOUT) {
                fail("timeout");
            }
            try {
                if (selenium.isTextPresent(elementSelect)) {
                    break;
                }
                pause(AUTOCOMPLETE_SLEEP_TIME);
            } catch (Exception e) {
            }
        }
        String optionXPath = "//ul[@role]";
        if (pageIndex != null) {
            optionXPath += "[" + pageIndex + "]";
        }
        optionXPath += "/li/a[. = \"" + elementSelect + "\"]";
        waitForElementByXPath(optionXPath, AUTOCOMPLETE_TIMEOUT);
        selenium.mouseOver(optionXPath);
        selenium.click(optionXPath);
        pause(AUTOCOMPLETE_DELAY);
    }

    /**
     * select an option from the jQuery autocomplete widget by typing chars one by one.
     *
     * @param autoCompleteId the id of the autocomplete
     * @param searchString the string to type in
     * @param elementSelect the string to select
     * @return true if the entry was found in the selection list
     */
    protected boolean progressiveSelectFromAutocomplete(String autoCompleteId, String searchString,
            String elementSelect) {
        waitForElementById(autoCompleteId);
        pause(AUTOCOMPLETE_DELAY);
        StringBuffer typedKeysBuffer = new StringBuffer();
        String fullOptionXPath = "//ul[@role]/li/a[. = \"" + elementSelect + "\"]";
        String optionXPath = null;
        String textToSelect = null;
        boolean entryFound = false;
        for (char c : searchString.toCharArray()) {
            typedKeysBuffer.append(c);
            selenium.focus(autoCompleteId);
            selenium.typeKeys(autoCompleteId, String.valueOf(c));
            pause(AUTOCOMPLETE_DELAY);
            if (!selenium.getValue(autoCompleteId).toLowerCase().contains(typedKeysBuffer.toString().toLowerCase())) {
                selenium.type(autoCompleteId, typedKeysBuffer.toString());
            }
            String typedOptionXPath = "//ul[@role]/li/a[. = \"" + typedKeysBuffer.toString() + "\"]";
            if (isElementPresentByJSXPath(fullOptionXPath)) {
                optionXPath = fullOptionXPath;
                textToSelect = elementSelect;
                break;
            } else if (isElementPresentByJSXPath(typedOptionXPath)) {
                optionXPath = typedOptionXPath;
                textToSelect = typedKeysBuffer.toString();
                break;
            }
        }
        if (optionXPath != null) {
            int timeSpent = 0;
            pause(AUTOCOMPLETE_DELAY);
            while (!selenium.isVisible("css=.ui-menu-item") && !selenium.isTextPresent(textToSelect)) {
                pause(AUTOCOMPLETE_DELAY);
                timeSpent += AUTOCOMPLETE_SLEEP_TIME;
                if (timeSpent > AUTOCOMPLETE_TIMEOUT) {
                    fail("timeout");
                }
            }
            waitForElementByXPath(optionXPath, AUTOCOMPLETE_TIMEOUT);
            entryFound = true;
            selenium.mouseOver(optionXPath);
            selenium.click("link=" + textToSelect);
        }
        return entryFound;
    }

    /**
     * Mouses over the locator and pasues for a delay.
     *
     * @param locator the location to mouseover.
     */
    protected void mouseOverAndPause(String locator) {
        selenium.mouseOver(locator);
        pause(SHORT_DELAY);
    }

    /**
     * click show and hide filters.
     *
     * @param filtersVisible if filters should be visible.
     */
    protected void clickShowHideFilters(boolean filtersVisible) {
        selenium.click("link=Show/Hide Filters...");
        if (filtersVisible) {
            assertElementVisible("filters", FILTER_TIMEOUT);
        } else {
            pause(SHORT_DELAY);
        }
    }

    /**
     * Checks for an elements visiblity within a certain amount of time.
     *
     * @param timeout maximum number of milieconds to wait
     * @param element the element to test
     */
    protected void assertElementVisible(String element, int timeout) {
        long start = System.currentTimeMillis();
        long elapsed = 0;
        while (elapsed < timeout && !selenium.isVisible(element)) {
            pause(SHORT_DELAY / 2);
            elapsed = System.currentTimeMillis() - start;
        }
        assertTrue("Timeout while waiting for element " + element + ", page contents:\n" + selenium.getBodyText(),
                selenium.isVisible(element));
    }

    /**
     * Checks for an elements visiblity within a certain amount of time.
     *
     * @param timeout maximum number of milieconds to wait
     * @param element the element to test
     */
    protected void assertElementNotVisible(String element, int timeout) {
        long start = System.currentTimeMillis();
        long elapsed = 0;
        while (elapsed < timeout && selenium.isVisible(element)) {
            pause(SHORT_DELAY / 2);
            elapsed = System.currentTimeMillis() - start;
        }
        assertTrue(
                "Timeout while waiting for element " + element + " to disappear, page contents:\n"
                        + selenium.getBodyText(), !selenium.isVisible(element));
    }

    /**
     * Checks for the presence of a popup help text page.
     *
     * @param helpLink Selenium locator for the link to the help page.
     * @param expectedText Expected text on the help page.
     * @param embeddedLinks Text of any links expected on the help page.
     * @param embeddedHrefs Expected hrefs of embedded links.
     */
    protected void verifyHelpText(String helpLink, String expectedText, String[] embeddedLinks,
            String[] embeddedHrefs) {
        assertTrue(selenium.isElementPresent(helpLink));
        clickAndWait(helpLink);
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent(expectedText));
        if (embeddedLinks != null) {
            for (int i = 0; i < embeddedLinks.length; i++) {
                assertTrue(selenium.isElementPresent("link=" + embeddedLinks[i]));
                String target = selenium.getAttribute("link=exact:" + embeddedLinks[i] + "@href");
                assertEquals(embeddedHrefs[i], target);
            }
        }
        clickAndWait("link=Close");
        selenium.selectWindow(null);
    }

    /**
     * Wait for the given text to appear or a timeout occurs.
     * @param text The text to wait for.
     */
    protected void waitForText(String text) {
        waitForText(text, true);
    }

    /**
     * Wait for text to appear or disappear or for a timeout to occur.
     * @param text The text to wait for.
     * @param isPresent Whether the text should be present.
     */
    protected void waitForText(String text, boolean isPresent) {
        int elapsedTime = 0;
        while (selenium.isTextPresent(text) != isPresent && elapsedTime < PAGE_TIMEOUT) {
            pause(SHORT_DELAY);
            elapsedTime += SHORT_DELAY;
        }
        assertTrue("Timeout waiting for text, expected text: " + text + ", body text: "
                + selenium.getBodyText(), selenium.isTextPresent(text) == isPresent);

    }

    /**
     * verify the options in a select list of time units.
     * @param fieldId the id of the select list
     */
    protected void verifyTimeUnitOptions(String fieldId) {
        String[] options = selenium.getSelectOptions(fieldId);
        assertEquals(TIME_UNIT_COUNT, options.length);
        int index = 0;
        assertEquals("hours", options[index++]);
        assertEquals("days", options[index++]);
        assertEquals("months", options[index++]);
        assertEquals("years", options[index++]);
    }

    /**
     * Enter person data.
     * @param idPrefix Field id prefix.
     * @param state State.
     * @param country Country.
     * @param institutionName Institution name.
     * @param autoselectIndex Page index of the org. autoselect field.
     */
    protected void enterPersonData(String idPrefix, String state, String country,
            String institutionName, int autoselectIndex) {
        selenium.type(idPrefix + "FirstName", "test-first-name-" + idPrefix);
        selenium.type(idPrefix + "LastName", "test-last-name-" + idPrefix);
        selenium.type(idPrefix + "Line1", "test-line1-" + idPrefix);
        selenium.type(idPrefix + "Line2", "test-line2-" + idPrefix);
        selenium.type(idPrefix + "City", "test-city-" + idPrefix);
        selenium.select(idPrefix + "State", "label=" + state);
        selenium.type(idPrefix + "Zip", StringUtils.left(idPrefix, ZIP_LENGTH));
        selenium.select(idPrefix + "Country", "label=" + country);
        selenium.type(idPrefix + "Email", "test-email-" + idPrefix + "@email.com");
        selenium.type(idPrefix + "Phone", StringUtils.left(idPrefix, PHONE_LENGTH));
        selenium.type(idPrefix + "PhoneExtension", StringUtils.left("ex" + idPrefix, EXTENSION_LENGTH));
        selectFromAutocomplete(idPrefix + "Organization",
                StringUtils.substringBefore(institutionName, "'").toLowerCase(),
                institutionName, autoselectIndex);
    }

    /**
     * Confirm person details.
     * @param idPrefix Field id prefix.
     * @param state State.
     * @param country Country.
     * @param institutionName Institution name.
     */
    protected void confirmPerson(String idPrefix, String state, String country,
            String institutionName) {
                assertTrue(selenium.isTextPresent("test-first-name-" + idPrefix));
                assertTrue(selenium.isTextPresent("test-last-name-" + idPrefix));
                assertTrue(selenium.isTextPresent("test-line1-" + idPrefix));
                assertTrue(selenium.isTextPresent("test-line2-" + idPrefix));
                assertTrue(selenium.isTextPresent("test-city-" + idPrefix));
                assertTrue(selenium.isTextPresent(state));
                assertTrue(selenium.isTextPresent(StringUtils.left(idPrefix, ZIP_LENGTH)));
                assertTrue(selenium.isTextPresent(country));
                assertTrue(selenium.isTextPresent("test-email-" + idPrefix + "@email.com"));
                assertTrue(selenium.isTextPresent(StringUtils.left(idPrefix, PHONE_LENGTH)));
                assertTrue(selenium.isTextPresent(StringUtils.left("ex" + idPrefix, EXTENSION_LENGTH)));
                assertTrue(selenium.isTextPresent(institutionName));
            }
}
