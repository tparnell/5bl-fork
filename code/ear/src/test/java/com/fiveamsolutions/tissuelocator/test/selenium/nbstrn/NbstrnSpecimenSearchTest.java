/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;

import org.apache.commons.lang.xwork.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractSpecimenSearchTest;

/**
 * @author ddasgupta
 *
 */
public class NbstrnSpecimenSearchTest extends AbstractSpecimenSearchTest {

    private static final String MIN_RESULT_STATE_1 = "Michigan";
    private static final String MIN_RESULT_STATE_2 = "New York";
    private static final String DEFAULT_HELP_ANCHOR_ID_PREFIX = "aid.";

    /**
     * test the specimen search page.
     */
    @Test
    @Override
    public void testList() {
        loginAsAdmin();
        goToSearchPage();
        verifyHelpPopups();
        simpleSearchTransition();
        validation();
        simpleSearch();
        filters();
        verifyResultsResize();
        showHideFilters();
        verifySaveSearchCriteria();
        verifyCartDisabled();
        verifyCriteriaFiltering();
        exerciseNormalSampleCheckbox();
    }

    /**
     * Tests validation.
     */
    @Override
    protected void validation() {
        selenium.type("min_birthWeight", "invalid");
        selenium.type("max_birthWeight", "invalid");
        selenium.type("min_collectionYear", "invalid");
        selenium.type("max_collectionYear", "invalid");
        selenium.type("min_patientAgeAtCollection", "invalid");
        selenium.type("max_patientAgeAtCollection", "invalid");
        clickAndWait("btn_search");
        assertTrue(selenium.isVisible("filters"));
        assertFalse(selenium.isVisible("criteria"));
        assertFalse(selenium.isElementPresent("specimen"));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Minimum Birth Weight must be a whole number."));
        assertTrue(selenium.isTextPresent("Maximum Birth Weight must be a whole number."));
        assertTrue(selenium.isTextPresent("Minimum Birth Year (yyyy) must be a whole number."));
        assertTrue(selenium.isTextPresent("Maximum Birth Year (yyyy) must be a whole number."));
        assertTrue(selenium.isTextPresent("Minimum Age at Collection must be a whole number."));
        assertTrue(selenium.isTextPresent("Maximum Age at Collection must be a whole number."));
    }

    // CHECKSTYLE:OFF method length
    /**
     * Tests filtering.
     */
    @Override
    protected void filters() {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        searchAllAndVerifyResult();

        selenium.select("multiSelectParamMap['externalIdAssigner.id']", "label=California");
        searchAndVerifyResult(1, null);
        assertTrue(selenium.isTextPresent("State of Origin: California"));

        selenium.removeAllSelections("multiSelectParamMap['externalIdAssigner.id']");
        selenium.type("min_collectionYear", "2009");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Birth Year (yyyy): greater than or equal to 2009"));

        selenium.type("min_collectionYear", "");
        selenium.type("max_collectionYear", "2008");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_2});
        assertTrue(selenium.isTextPresent("Birth Year (yyyy): less than or equal to 2008"));

        selenium.type("max_collectionYear", "");
        clickShowHideFilters(true);
        String normalTerm = ClientProperties.getNormalTerm();
        selectFromAutocomplete("pathologicalCharacteristic", normalTerm.substring(0, normalTerm.length() - 3),
                normalTerm);
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Condition: " + ClientProperties.getNormalTerm()));

        clickShowHideFilters(true);
        selenium.click("normalSample");
        selenium.type("pathologicalCharacteristic", "");
        selenium.click("normalSample");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_2});
        assertTrue(selenium.isTextPresent("Condition: " + ClientProperties.getNormalTerm()));

        clickShowHideFilters(true);
        selenium.click("normalSample");
        selenium.select("multiSelectParamMap['participant.gender']", "label=Male");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Sex: Male"));

        selenium.addSelection("multiSelectParamMap['participant.gender']", "label=Female");
        searchAndVerifyResult(2, new String[] {MIN_RESULT_STATE_1, MIN_RESULT_STATE_2});
        String pattern = "regexp:Sex: (Female, Male)|(Male, Female)";
        assertTrue(selenium.isTextPresent(pattern));

        selenium.removeSelection("multiSelectParamMap['participant.gender']", "label=Male");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Sex: Female"));

        selenium.removeAllSelections("multiSelectParamMap['participant.gender']");
        selenium.select("multiSelectCollectionParamMap['participant.races']", "label=Asian");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Race: Asian"));

        selenium.addSelection("multiSelectCollectionParamMap['participant.races']", "label=Black or African American");
        searchAndVerifyResult(2, new String[] {MIN_RESULT_STATE_1, MIN_RESULT_STATE_2});
        pattern = "regexp:Race: (Asian, Black or African American)|(Black or African American, Asian)";
        assertTrue(selenium.isTextPresent(pattern));

        selenium.removeSelection("multiSelectCollectionParamMap['participant.races']", "label=Asian");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Race: Black or African American"));

        selenium.removeAllSelections("multiSelectCollectionParamMap['participant.races']");
        selenium.select("multiSelectParamMap['participant.ethnicity']", "label=Hispanic or Latino");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Ethnicity: Hispanic or Latino"));

        selenium.addSelection("multiSelectParamMap['participant.ethnicity']", "label=Unknown");
        searchAndVerifyResult(2, new String[] {MIN_RESULT_STATE_1, MIN_RESULT_STATE_2});
        pattern = "regexp:Ethnicity: (Unknown, Hispanic or Latino)|(Hispanic or Latino, Unknown)";
        assertTrue(selenium.isTextPresent(pattern));

        selenium.removeSelection("multiSelectParamMap['participant.ethnicity']", "label=Hispanic or Latino");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Ethnicity: Unknown"));

        selenium.removeAllSelections("multiSelectParamMap['participant.ethnicity']");
        selenium.type("min_birthWeight", "2750");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_2});
        assertTrue(selenium.isTextPresent("Birth Weight: greater than or equal to 2750"));

        selenium.type("min_birthWeight", "");
        selenium.type("max_birthWeight", "2750");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Birth Weight: less than or equal to 2750"));

        selenium.type("min_birthWeight", "2000");
        selenium.type("max_birthWeight", "3500");
        searchAndVerifyResult(2, new String[] {MIN_RESULT_STATE_1, MIN_RESULT_STATE_2});
        assertTrue(selenium.isTextPresent("Birth Weight: 2000 - 3500"));

        selenium.type("min_birthWeight", "");
        selenium.type("max_birthWeight", "");
        selenium.select("object.customProperties['storageCondition']", "label=Room Temperature");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("DBS Specimen Storage Condition: Room Temperature"));

        selenium.select("object.customProperties['storageCondition']", "label=Refrigerated");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("DBS Specimen Storage Condition: Refrigerated"));

        selenium.select("object.customProperties['storageCondition']", "label=- All -");
        selenium.select("object.customProperties['nutritionalStatus']", "label=Breast");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Nutritional Status: Breast"));

        selenium.select("object.customProperties['nutritionalStatus']", "label=Formula");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Nutritional Status: Formula"));

        selenium.select("object.customProperties['nutritionalStatus']", "label=- All -");
        selenium.type("min_patientAgeAtCollection", "25");
        verifyTimeUnitOptions("object.patientAgeAtCollectionUnits");
        selenium.select("object.patientAgeAtCollectionUnits", "label=hours");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Age at Collection: greater than or equal to 25 hours"));

        selenium.type("min_patientAgeAtCollection", "10");
        selenium.select("object.patientAgeAtCollectionUnits", "label=days");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Age at Collection: greater than or equal to 10 days"));

        selenium.type("min_patientAgeAtCollection", "");
        selenium.type("max_patientAgeAtCollection", "25");
        selenium.select("object.patientAgeAtCollectionUnits", "label=hours");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Age at Collection: less than or equal to 25 hours"));

        selenium.type("max_patientAgeAtCollection", "10");
        selenium.select("object.patientAgeAtCollectionUnits", "label=days");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Age at Collection: less than or equal to 10 days"));

        selenium.type("max_patientAgeAtCollection", "");
        selenium.select("object.patientAgeAtCollectionUnits", "label=hours");
        clickAndWait("btn_search");
        assertFalse(selenium.isTextPresent("Age at Collection: hours"));

        selenium.type("min_patientAgeAtCollection", "15");
        selenium.type("max_patientAgeAtCollection", "24");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Age at Collection: 15 - 24 hours"));

        selenium.type("min_patientAgeAtCollection", "2");
        selenium.type("max_patientAgeAtCollection", "12");
        selenium.select("object.patientAgeAtCollectionUnits", "label=days");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Age at Collection: 2 - 12 days"));

        selenium.type("min_patientAgeAtCollection", "");
        selenium.type("max_patientAgeAtCollection", "");
    }
    // CHECKSTYLE:ON

    /**
     * {@inheritDoc}
     */
    @Override
    protected void simpleSearchFilters() {
        assertFalse(selenium.isElementPresent("normalSample"));
        assertFalse(selenium.isElementPresent("multiSelectParamMap['participant.gender']"));
        assertFalse(selenium.isElementPresent("multiSelectCollectionParamMap['participant.races']"));
        assertFalse(selenium.isElementPresent("multiSelectParamMap['participant.ethnicity']"));
        assertFalse(selenium.isElementPresent("min_birthWeight"));
        assertFalse(selenium.isElementPresent("max_birthWeight"));
        assertFalse(selenium.isElementPresent("object.customProperties['storageCondition']"));
        assertFalse(selenium.isElementPresent("object.customProperties['nutritionalStatus']"));
        assertFalse(selenium.isElementPresent("min_patientAgeAtCollection"));
        assertFalse(selenium.isElementPresent("max_patientAgeAtCollection"));
        assertFalse(selenium.isElementPresent("object.patientAgeAtCollectionUnits"));
        assertTrue(selenium.isElementPresent("multiSelectParamMap['externalIdAssigner.id']"));
        assertTrue(selenium.isElementPresent("min_collectionYear"));
        assertTrue(selenium.isElementPresent("max_collectionYear"));

        selenium.select("multiSelectParamMap['externalIdAssigner.id']", "label=California");
        searchAndVerifyResult(1, null);
        assertTrue(selenium.isTextPresent("State of Origin: California"));

        selenium.removeAllSelections("multiSelectParamMap['externalIdAssigner.id']");
        selenium.type("min_collectionYear", "2009");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        assertTrue(selenium.isTextPresent("Birth Year (yyyy): greater than or equal to 2009"));

        selenium.type("min_collectionYear", "");
        selenium.type("max_collectionYear", "2008");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_2});
        assertTrue(selenium.isTextPresent("Birth Year (yyyy): less than or equal to 2008"));

        selenium.type("max_collectionYear", "");
        clickAndWait("simpleSearch");
        assertTrue(selenium.isElementPresent("normalSample"));
        assertTrue(selenium.isElementPresent("multiSelectParamMap['participant.gender']"));
        assertTrue(selenium.isElementPresent("multiSelectCollectionParamMap['participant.races']"));
        assertTrue(selenium.isElementPresent("multiSelectParamMap['participant.ethnicity']"));
        assertTrue(selenium.isElementPresent("min_birthWeight"));
        assertTrue(selenium.isElementPresent("max_birthWeight"));
        assertTrue(selenium.isElementPresent("object.customProperties['storageCondition']"));
        assertTrue(selenium.isElementPresent("object.customProperties['nutritionalStatus']"));
        assertTrue(selenium.isElementPresent("min_patientAgeAtCollection"));
        assertTrue(selenium.isElementPresent("max_patientAgeAtCollection"));
        assertTrue(selenium.isElementPresent("object.patientAgeAtCollectionUnits"));
        assertTrue(selenium.isElementPresent("min_collectionYear"));
        assertTrue(selenium.isElementPresent("max_collectionYear"));
    }

    private void simpleSearchTransition() {
        selenium.select("multiSelectParamMap['participant.gender']", "label=Male");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});
        clickAndWait("simpleSearch");
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertFalse(selenium.isElementPresent("Link=Show/Hide Filters"));
        assertTrue(selenium.isVisible("filters"));
        assertFalse(selenium.isTextPresent("1-1 of 1 Result"));
        assertFalse(selenium.isElementPresent("//table[id='specimen']"));
        selenium.type("max_collectionYear", "2008");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_2});
        clickAndWait("simpleSearch");
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertFalse(selenium.isElementPresent("Link=Show/Hide Filters"));
        assertTrue(selenium.isVisible("filters"));
        assertFalse(selenium.isTextPresent("1-1 of 1 Result"));
        assertFalse(selenium.isElementPresent("//table[id='specimen']"));
    }

    /**
     * Searches and verifies results.
     */
    private void searchAllAndVerifyResult() {
        String profileLinkPath = "xpath=//table[@id='specimen']/tbody/tr[%d]/td[1]/a";
        String usageLinkPath = "xpath=//table[@id='specimen']/tbody/tr[%d]/td[4]/a";
        clickAndWait("btn_search");
        assertFalse(selenium.isVisible("filters"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        assertEquals("California", selenium.getTable("specimen.1.0"));
        assertTrue(selenium.isElementPresent(String.format(usageLinkPath, 1)));
        assertTrue(selenium.isElementPresent(String.format(profileLinkPath, 1)));
        assertEquals("6", selenium.getTable("specimen.1.1"));
        assertEquals("Iowa", selenium.getTable("specimen.2.0"));
        assertTrue(selenium.isElementPresent(String.format(usageLinkPath, 2)));
        assertTrue(selenium.isElementPresent(String.format(profileLinkPath, 2)));
        assertEquals("6", selenium.getTable("specimen.2.1"));
        selenium.isElementPresent("link=" + MIN_RESULT_STATE_1);
        selenium.isElementPresent("link=" + MIN_RESULT_STATE_2);
        assertTrue(selenium.isTextPresent("These numbers reflect only those cases in the virtual "
                + "repository, not the incidence of the condition in the population."));
    }

    /**
     * Searches and verifies results.
     */
    private void searchAndVerifyResult(int count, String[] minResultStates) {
        clickAndWait("btn_search");
        assertFalse(selenium.isVisible("filters"));
        assertTrue(selenium.isVisible("criteria"));
        assertTrue(selenium.isElementPresent("specimen"));
        assertTrue(selenium.isVisible("specimen"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        if (count == 1) {
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        } else {
            assertTrue(selenium.isTextPresent("1-" + count + " of " + count + " Results"));
        }
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        for (int i = 1; i <= count; i++) {
            assertEquals("6", selenium.getTable("specimen." + i + ".1"));
        }
        if (minResultStates != null) {
            String minResultsMessage = "The federal regulations governing research require that the specimens"
                + " not be \"reasonably identifiable\" to the investigator. In accordance with these regulations,"
                + " and to ensure anonymity of the specimens, your search yields a result of too few DBS specimens "
                + "for the following states:";
            assertTrue(selenium.isTextPresent(minResultsMessage));
            for (String state : minResultStates) {
                selenium.isElementPresent("link=" + state);
            }
        }

        String usageLinkPath = "xpath=//table[@id='specimen']/tbody/tr[%d]/td[4]/a";
        for (int i = 1; i <= count; i++) {
            verifyHelpText(String.format(usageLinkPath, i), "All leftover DBS specimens must be "
                    + "returned to the State after use.", null, null);
        }

        String profileLinkPath = "xpath=//table[@id='specimen']/tbody/tr[%d]/td[1]/a";
        for (int i = 1; i <= count; i++) {
            assertTrue(selenium.isElementPresent(String.format(profileLinkPath, i)));
        }
    }

    /**
     * Clears all search filters.
     */
    @Override
    protected void clearAllFilters() {
        String[] textFilters = new String[] {"min_birthWeight", "max_birthWeight", "min_collectionYear",
                "max_collectionYear", "min_patientAgeAtCollection", "max_patientAgeAtCollection"};
        for (String filter : textFilters) {
            selenium.type(filter, "");
        }

        String selectLabel = "- All -";
        String[] selectFilters = new String[] {"object.customProperties['storageCondition']",
                "object.customProperties['nutritionalStatus']"};
        for (String filter : selectFilters) {
            selenium.select(filter, "label=" + selectLabel);
        }

        clearMultiSelects();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void clearMultiSelects() {
        super.clearMultiSelects();
        selenium.removeAllSelections("multiSelectParamMap['externalIdAssigner.id']");
    }

    /**
     * Fills in all specimen filters.
     */
    @Override
    protected void fillInAllFilters() {

        selenium.type("min_collectionYear", "2010");
        selenium.type("max_collectionYear", "2012");
        if (!selenium.isVisible("filters")) {
            clickShowHideFilters(true);
        }
        selenium.select("multiSelectParamMap['participant.gender']", "label=Unspecified");
        selenium.select("multiSelectCollectionParamMap['participant.races']", "label=White");
        selenium.select("multiSelectParamMap['participant.ethnicity']", "label=Unknown");
        selenium.type("min_birthWeight", "2750");
        selenium.type("max_birthWeight", "3000");
        selenium.select("object.customProperties['storageCondition']", "label=Unknown");
        selenium.select("object.customProperties['nutritionalStatus']", "label=TPN");
        selenium.type("min_patientAgeAtCollection", "40");
        selenium.type("max_patientAgeAtCollection", "50");
        selenium.select("object.patientAgeAtCollectionUnits", "label=days");

        selenium.removeAllSelections("multiSelectParamMap['externalIdAssigner.id']");
    }

    /**
     * Verifies the 'Add to Cart' button is disabled if no specimens are selected, and enabled if one or more are
     * selected.
     */
    @Override
    protected void verifyCartDisabled() {
        goToSearchPage();
        clearAllFilters();
        clickAndWait("btn_search");
        assertFalse(selenium.isEditable("btn_addtocart"));
        String textFieldXPath = "xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]";
        selenium.type(textFieldXPath, "2");
        selenium.keyUp(textFieldXPath, "2");
        assertTrue(selenium.isEditable("btn_addtocart"));
        selenium.type(textFieldXPath, "0");
        selenium.keyUp(textFieldXPath, "0");
        assertFalse(selenium.isEditable("btn_addtocart"));
        selenium.type("xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]", "1");
        selenium.keyUp("xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]", "1");
        assertTrue(selenium.isEditable("btn_addtocart"));
        selenium.type("xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]", "");
        selenium.keyUp("xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]", " ");
        assertFalse(selenium.isEditable("btn_addtocart"));
        clickShowHideFilters(false);
        fillInAllFilters();
        clickAndWait("btn_search");
        assertFalse(selenium.isEditable("btn_addtocart"));
        assertFalse(selenium.isVisible("filters"));
        assertTrue(selenium.isVisible("criteria"));
        assertTrue(selenium.isElementPresent("specimen"));
        assertTrue(selenium.isVisible("specimen"));
    }

    private String getHelpLabelSelector(String fieldName) {
        return DEFAULT_HELP_ANCHOR_ID_PREFIX + fieldName;
    }

    /**
     * verify help pop-ups from the search page.
     */
    private void verifyHelpPopups() {
        assertTrue(selenium.isTextPresent("These conditions reflect only those cases in the virtual "
            + "repository, not the incidence of the condition in the population."));
        verifyHelpText(getHelpLabelSelector("object.pathologicalCharacteristic"),
                "Diagnosed Condition indicates a positive screen", null, null);
        verifyHelpText(getHelpLabelSelector("externalIdAssigner.id"),
                "States participating in the Virtual repository project", null, null);
        verifyHelpText(getHelpLabelSelector("participant.gender"),
                "Gender of the newborn recorded on the newborn screening record.", null, null);
        verifyHelpText(getHelpLabelSelector("birthWeight"),
                "Although the standard indicator of prematurity is gestational age, "
                        + "birth weight can be an additional indicator.", null, null);
        verifyHelpText(getHelpLabelSelector("object.customProperties['storageCondition']"),
                "Each state stores residual DBS in different manners", null, null);
        verifyHelpText(getHelpLabelSelector("collectionYear"),
                "Year of birth recorded on the newborn screening record. "
                        + "This year may be different than the date of screening. If searching for a specific value, enter that "
                        + "number in both of the range boxes. E.g., \"2010-2010\" yields results for only the year 2010.", null, null);
        verifyHelpText(getHelpLabelSelector("participant.races"),
                "Race in the VRDBS is coded according to NIH standards (following OMB regulations ): "
                        + "American Indian or Alaska Native, Asian, Black or African American, Native Hawaiian "
                        + "or Other Pacific Islander, and White. If a state collects data on race using different "
                        + "standards, we have mapped those categories to the NIH categories whenever possible. "
                        + "\"Not Reported\" means that the state does not collect this information and \"unknown\" "
                        + "means that information was unanswered on the screening card.",
                        new String[]{"OMB regulations"},
                        new String[] {"http://www.census.gov/population/www/socdemo/race/Ombdir15.html"});
        verifyHelpText(getHelpLabelSelector("participant.ethnicity"),
                "Ethnicity in the VRDBS is coded according to NIH standards", null, null);
        verifyHelpText(getHelpLabelSelector("object.customProperties['nutritionalStatus']"),
                "At the time of collection of the specimen, "
                        + "some states collect information on the newborn's intake of food.", null, null);
        verifyHelpText(getHelpLabelSelector("patientAgeAtCollection"),
                "Age at collection is calculated by subtracting the date and time of collection "
                        + "from the date and time of birth. "
                        + "In the VRDBS searches should be constrained to hours. "
                        + "If searching for specific values, enter that number in both of the range "
                        + "boxes. E.g., \"24-24\" yields results of only those DBS that were "
                        + "collected at the 24 hour mark after birth.", null, null);
    }

    /**
     * Verify that the state of origin clause does not appear in the criteria display on the cart page
     * after searching.
     */
    private void verifyCriteriaFiltering() {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        selenium.select("multiSelectParamMap['externalIdAssigner.id']", "label=California");
        searchAndVerifyResult(1, null);
        assertTrue(selenium.isTextPresent("State of Origin: California"));
        String textFieldXPath = "xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]";
        selenium.type(textFieldXPath, "2");
        selenium.keyUp(textFieldXPath, "2");
        assertTrue(selenium.isEditable("btn_addtocart"));
        clickAndWait("btn_addtocart");
        assertTrue(selenium.isTextPresent("My Selections"));
        assertFalse(selenium.isTextPresent("State of Origin: California"));
        assertTrue(selenium.isTextPresent("No Criteria Specified"));
        clickAndWait("link=" + ClientProperties.getCartSearchMore());
        selenium.select("multiSelectParamMap['externalIdAssigner.id']", "label=California");
        selenium.select("multiSelectParamMap['participant.gender']", "label=Male");
        searchAndVerifyResult(1, null);
        assertTrue(selenium.isTextPresent("State of Origin: California; Sex: Male"));
        selenium.type(textFieldXPath, "2");
        selenium.keyUp(textFieldXPath, "2");
        assertTrue(selenium.isEditable("btn_addtocart"));
        clickAndWait("btn_addtocart");
        assertTrue(selenium.isTextPresent("My Selections"));
        assertFalse(selenium.isTextPresent("State of Origin: California"));
        assertTrue(selenium.isTextPresent("Sex: Male"));
    }
    /**
     * Verify save search criteria.
     */
    @Override
    protected void verifySaveSearchCriteria() {
        String savedSearchName = "testSavedSearch";
        goToSearchPage(false);
        clickAndWait("btn_search");
        if (!selenium.isVisible("filters")) {
            clickShowHideFilters(true);
        }

        selectFromAutocomplete("pathologicalCharacteristic",
                ClientProperties.getAutocompleteDiseaseNameStart(), ClientProperties.getAutocompleteDiseaseFullName());
        clickAndWait("btn_search");

        // go to save search but cancel the save
        clickAndWait("link=Save Search As");
        selenium.selectFrame("popupFrame");
        clickAndWait("link=Cancel");
        selenium.selectFrame("relative=up");
        assertTrue(selenium.isVisible("filters"));
        assertFalse(selenium.isVisible("criteria"));
        assertFalse(selenium.isElementPresent("specimen"));
        assertFalse(selenium.isTextPresent("Your search has been saved successfully."));
        // go again to save search but this time save it
        clickAndWait("link=Save Search As");
        selenium.selectFrame("popupFrame");
        // test the validation
        clickAndWait("saveSearch_submitButton");
        assertTrue(selenium.isTextPresent("Name must be set"));
        selenium.type("name", savedSearchName);
        String description = "This is a search for " + ClientProperties.getAutocompleteDiseaseFullName()
            + " specimens.";
        selenium.type("description", description);
        clickAndWait("saveSearch_submitButton");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertEquals("Your search has been saved successfully.", selenium.getText("//div[@class='topconfirm']/p"));
        assertTrue(selenium.isTextPresent("Update " + savedSearchName));

        // update the saved specimen search
        clickAndWait("link=Update " + savedSearchName);
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent(savedSearchName));
        assertEquals(description, selenium.getValue("description"));
        selenium.type("description", "This is an updated search for "
                + ClientProperties.getAutocompleteDiseaseFullName() + " specimens.");
        clickAndWait("saveSearch_submitButton");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertEquals("Your search has been saved successfully.", selenium.getText("//div[@class='topconfirm']/p"));

        // attempt to save a specimen search of the same name
        clickAndWait("link=Save Search As");
        selenium.selectFrame("popupFrame");
        clickAndWait("saveSearch_submitButton");
        assertTrue(selenium.isTextPresent("A saved specimen search with this name already exists."));
        clickAndWait("link=Cancel");
        selenium.selectWindow(null);
        pause(LONG_DELAY);

        // leave search page, go back
        clickAndWait("link=Home");
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        selenium.select("multiSelectParamMap['participant.gender']", "label=Male");
        searchAndVerifyResult(1, new String[] {MIN_RESULT_STATE_1});

        // go to manage my searches
        clickAndWait("link=Manage My Saved Searches");
        selenium.selectFrame("popupFrame");
        assertEquals(savedSearchName, selenium.getText("link=" + savedSearchName));
        // click on the saved search
        clickAndWait("link=" + savedSearchName);
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertEquals("Your search has been loaded successfully.", selenium.getText("//div[@class='topconfirm']/p"));
        assertEquals(selenium.getValue("pathologicalCharacteristic"),
                ClientProperties.getAutocompleteDiseaseFullName());
        assertTrue(StringUtils.isBlank(selenium.getValue("multiSelectParamMap['participant.gender']")));
        assertFalse(selenium.isChecked("normalSample"));

        // delete the saved specimen search
        clickAndWait("link=Manage My Saved Searches");
        selenium.selectFrame("popupFrame");
        assertEquals(savedSearchName, selenium.getText("link=" + savedSearchName));
        clickAndWait("link=Delete");
        assertTrue(selenium.isTextPresent("Search deleted successfully."));
        assertFalse(selenium.isTextPresent(savedSearchName));
        assertTrue(selenium.isTextPresent("Nothing found to display"));
        clickAndWait("link=Close");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertFalse(selenium.isTextPresent("Update " + savedSearchName));

        // re-create the saved specimen search
        selenium.click("normalSample");
        pause(SHORT_DELAY);
        clickAndWait("link=Save Search As");
        selenium.selectFrame("popupFrame");
        selenium.type("name", savedSearchName);
        description = "This is a search for " + ClientProperties.getAutocompleteDiseaseFullName() + " specimens.";
        selenium.type("description", description);
        clickAndWait("saveSearch_submitButton");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertEquals("Your search has been saved successfully.", selenium.getText("//div[@class='topconfirm']/p"));
        assertTrue(selenium.isTextPresent("Update " + savedSearchName));
        assertTrue(selenium.isChecked("normalSample"));
        clickAndWait("link=Manage My Saved Searches");
        selenium.selectFrame("popupFrame");
        assertEquals(savedSearchName, selenium.getText("link=" + savedSearchName));
        clickAndWait("link=" + savedSearchName);
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertEquals("Your search has been loaded successfully.", selenium.getText("//div[@class='topconfirm']/p"));
        assertTrue(selenium.isChecked("normalSample"));
    }

    /**
     * Test search field visibility based on user role.
     */
    @Override
    public void testSearchFieldVisibility() {
        loginAsAdmin();
        goToSearchPage();
        assertFalse(selenium.isTextPresent("External ID (Record ID)"));
        clickAndWait("Link=Sign Out");
        loginAsUser1();
        goToSearchPage();
        assertFalse(selenium.isTextPresent("External ID (Record ID)"));
        clickAndWait("Link=Sign Out");
}

}
