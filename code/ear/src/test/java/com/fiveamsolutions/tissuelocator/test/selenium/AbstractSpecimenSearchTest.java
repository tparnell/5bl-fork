/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium;

import org.apache.commons.lang.xwork.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;


/**
 * @author ddasgupta
 *
 */
public abstract class AbstractSpecimenSearchTest extends AbstractTissueLocatorSeleniumTest {

    private static final String SAVED_SPECIMEN_SEARCH_NAME_ROOT = "testSavedSearch";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "SpecimenSearchTest.sql";
    }

    /**
     * @return the name of the specimen type field.
     */
    protected String getSpecimenTypeFieldName() {
        return "object.specimenType.name";
    }

    /**
     * @return the name of the specimen class field.
     */
    protected String getSpecimenClassFieldName() {
        return "object.specimenType.specimenClass";
    }

    /**
     * test the specimen search page.
     */
    public void testList() {
        loginAsAdmin();
        goToSearchPage();
        validation();
        filters();
        verifyExportPresent();
        backToList();
        verifyResultsResize();
        selectAll();
        showHideFilters();
        verifyCategoryTabs();
        verifySearchDisplayOptions();
        pause(LONG_DELAY);
        verifySaveSearchCriteria();
        sessionStorage();
        verifyCartDisabled();
        exerciseNormalSampleCheckbox();
        verifyFilteredDiseaseSelections();
    }

    /**
     * Test search field visibility based on user role.
     */
    public void testSearchFieldVisibility() {
        loginAsAdmin();
        goToSearchPage();
        assertTrue(selenium.isTextPresent("External ID (Record ID)"));
        clickAndWait("Link=Sign Out");
        loginAsUser1();
        goToSearchPage();
        assertFalse(selenium.isTextPresent("External ID (Record ID)"));
        clickAndWait("Link=Sign Out");
    }

    /**
     * Goes to search page.
     */
    protected void goToSearchPage() {
        goToSearchPage(true);
    }

    /**
     * Goes to search page.
     * @param filtersMustBeVisible whether the filters must be visible on the search page
     */
    protected void goToSearchPage(boolean filtersMustBeVisible) {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        assertTrue(selenium.isTextPresent("Search for " + ClientProperties.getBiospecimenTerm() + "s"));
        if (filtersMustBeVisible) {
            assertTrue(selenium.isVisible("filters"));
            assertFalse(selenium.isVisible("criteria"));
            assertFalse(selenium.isElementPresent("specimen"));
        }
    }

    /**
     * Tests validation.
     */
    protected void validation() {
        selenium.type("id", "invalid");
        selenium.type("min_collectionYear", "invalid");
        selenium.type("max_collectionYear", "invalid");
        selenium.type("minimumAvailableQuantity", "invalid");
        selenium.type("min_patientAgeAtCollection", "invalid");
        selenium.type("max_patientAgeAtCollection", "invalid");
        // set the selections to valid values to check that the
        // values are not lost if the validation fails
        selenium.select(getSpecimenTypeFieldName(), "label=Lavage");
        selenium.select(getSpecimenClassFieldName(), "label=Molecular");
        selenium.select("multiSelectParamMap['participant.gender']", "label=Unspecified");
        selenium.select("multiSelectCollectionParamMap['participant.races']", "label=White");
        selenium.select("multiSelectParamMap['participant.ethnicity']", "label=Unknown");
        clickAndWait("btn_search");
        assertTrue(selenium.isVisible("filters"));
        assertFalse(selenium.isVisible("criteria"));
        assertFalse(selenium.isElementPresent("specimen"));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " ID must be a whole number"));
        assertTrue(selenium.isTextPresent("Minimum Available Quantity must be a number greater than 0."));
        assertTrue(selenium.isTextPresent("Minimum Patient's Age at Collection must be a whole number."));
        assertTrue(selenium.isTextPresent("Maximum Patient's Age at Collection must be a whole number."));
        assertTrue(selenium.isTextPresent("Minimum Collection Year (yyyy) must be a whole number."));
        assertTrue(selenium.isTextPresent("Maximum Collection Year (yyyy) must be a whole number."));
        // verify that the selections maintain the values
        assertEquals("Lavage", selenium.getSelectedLabel(getSpecimenTypeFieldName()));
        assertEquals("Molecular", selenium.getSelectedLabel(getSpecimenClassFieldName()));
        assertEquals("UNSPECIFIED", selenium.getSelectedValues("multiSelectParamMap['participant.gender']")[0]);
        assertEquals("WHITE", selenium.getSelectedValues("multiSelectCollectionParamMap['participant.races']")[0]);
        assertEquals("UNKNOWN", selenium.getSelectedValues("multiSelectParamMap['participant.ethnicity']")[0]);
        clearSelectFilters();
        clearMultiSelects();
    }

    /**
     * Tests filtering.
     */
    protected void filters() {
        if (ClientProperties.getFluidSpecimenCount() > 0) {
            showOnlyTestResults();
        }

        selenium.type("id", "");
        selenium.type("min_collectionYear", "");
        selenium.type("max_collectionYear", "");
        selenium.type("minimumAvailableQuantity", "");
        selenium.type("min_patientAgeAtCollection", "");
        selenium.type("max_patientAgeAtCollection", "");

        selenium.type("id", "1");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Biospecimen ID: 1"));

        selenium.type("id", "");
        String extId = selenium.getValue("externalId");
        selenium.type("externalId", "test 1");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("External ID (Record ID): test 1"));

        selenium.type("externalId", extId);
        selenium.select(getSpecimenTypeFieldName(), "label=Bile");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Type: Bile"));
        removeSelection(getSpecimenTypeFieldName());

        selenium.select(getSpecimenClassFieldName(), "label=Fluid");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Class: Fluid"));
        removeSelection(getSpecimenClassFieldName());

        selenium.type("min_collectionYear", "2009");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Collection Year (yyyy): greater than or equal to 2009"));

        selenium.type("min_collectionYear", "");
        selenium.type("max_collectionYear", "2008");
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Collection Year (yyyy): less than or equal to 2008"));

        selenium.type("max_collectionYear", "");
        clickShowHideFilters(true);
        selectFromAutocomplete("pathologicalCharacteristic", ClientProperties.getNormalTerm());
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Disease: " + ClientProperties.getNormalTerm()));

        clickShowHideFilters(true);
        selenium.click("normalSample");
        selenium.type("pathologicalCharacteristic", "");
        selenium.click("normalSample");
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Disease: " + ClientProperties.getNormalTerm()));
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Disease: " + ClientProperties.getNormalTerm()));

        clickShowHideFilters(true);
        selenium.click("normalSample");
        selenium.type("minimumAvailableQuantity", "150");
        selenium.select("object.availableQuantityUnits", "label=milligrams");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Minimum Available Quantity: 150 milligrams"));

        selenium.type("minimumAvailableQuantity", "150000");
        selenium.select("object.availableQuantityUnits", "label=micrograms");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Minimum Available Quantity: 150000 micrograms"));

        selenium.type("minimumAvailableQuantity", "");
        selenium.select("object.availableQuantityUnits", "label=milligrams");
        clickAndWait("btn_search");
        assertFalse(selenium.isTextPresent("Minimum Available Quantity: milligrams"));

        selenium.select("multiSelectParamMap['participant.gender']", "label=Male");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Sex: Male"));

        selenium.addSelection("multiSelectParamMap['participant.gender']", "label=Female");
        searchAndVerifyResult(1, 2);
        String pattern = "regexp:Sex: (Female, Male)|(Male, Female)";
        assertTrue(selenium.isTextPresent(pattern));

        selenium.removeSelection("multiSelectParamMap['participant.gender']", "label=Male");
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Sex: Female"));

        selenium.removeAllSelections("multiSelectParamMap['participant.gender']");
        selenium.select("multiSelectCollectionParamMap['participant.races']", "label=Asian");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Race: Asian"));

        selenium.addSelection("multiSelectCollectionParamMap['participant.races']", "label=Black or African American");
        searchAndVerifyResult(1, 2);
        pattern = "regexp:Race: (Asian, Black or African American)|(Black or African American, Asian)";
        assertTrue(selenium.isTextPresent(pattern));

        selenium.removeSelection("multiSelectCollectionParamMap['participant.races']", "label=Asian");
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Race: Black or African American"));

        selenium.removeAllSelections("multiSelectCollectionParamMap['participant.races']");
        selenium.select("multiSelectParamMap['participant.ethnicity']", "label=Hispanic or Latino");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Ethnicity: Hispanic or Latino"));

        selenium.addSelection("multiSelectParamMap['participant.ethnicity']", "label=Unknown");
        searchAndVerifyResult(1, 2);
        pattern = "regexp:Ethnicity: (Unknown, Hispanic or Latino)|(Hispanic or Latino, Unknown)";
        assertTrue(selenium.isTextPresent(pattern));

        selenium.removeSelection("multiSelectParamMap['participant.ethnicity']", "label=Hispanic or Latino");
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Ethnicity: Unknown"));

        selenium.removeAllSelections("multiSelectParamMap['participant.ethnicity']");
        verifyTimeUnitOptions("object.patientAgeAtCollectionUnits");
        selenium.select("object.patientAgeAtCollectionUnits", "label=years");
        clickAndWait("btn_search");
        assertFalse(selenium.isTextPresent("Patient's Age at Collection: years"));

        selenium.type("max_patientAgeAtCollection", "15");
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Patient's Age at Collection: less than or equal to 15 years"));

        selenium.type("max_patientAgeAtCollection", "180");
        selenium.select("object.patientAgeAtCollectionUnits", "label=months");
        searchAndVerifyResult(2);
        assertTrue(selenium.isTextPresent("Patient's Age at Collection: less than or equal to 180 months"));

        selenium.type("max_patientAgeAtCollection", "");
        selenium.type("min_patientAgeAtCollection", "15");
        selenium.select("object.patientAgeAtCollectionUnits", "label=years");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Patient's Age at Collection: greater than or equal to 15 years"));

        selenium.type("min_patientAgeAtCollection", "180");
        selenium.select("object.patientAgeAtCollectionUnits", "label=months");
        searchAndVerifyResult(1);
        assertTrue(selenium.isTextPresent("Patient's Age at Collection: greater than or equal to 180 months"));

        selenium.type("min_patientAgeAtCollection", "");
    }

    /**
     * Remove selection.
     *
     * @param selector the select list
     */
    protected void removeSelection(String selector) {
        selenium.select(selector, "label=- All -");
    }

    /**
     * Test simple search.
     */
    protected void simpleSearch() {
        if (ClientProperties.isSimpleSearchEnabled()) {
            clickAndWait("Link=Sign Out");
            loginAsAdmin();
            goToSearchPage();
            assertFalse(selenium.isChecked("simpleSearch"));
            assertFalse(selenium.isTextPresent(ClientProperties.getSimpleSearchTitle()));
            clickAndWait("simpleSearch");
            assertTrue(selenium.isChecked("simpleSearch"));
            assertTrue(selenium.isTextPresent(ClientProperties.getSimpleSearchTitle()));
            assertFalse(selenium.isTextPresent("Search for " + ClientProperties.getBiospecimenTerm() + "s"));
            assertTrue(selenium.isVisible("filters"));
            simpleSearchFilters();
        }
    }

    /**
     * Subclasses should implement client-specific filter tests
     * for prospective collection searches.
     */
    protected void simpleSearchFilters() {
        // no-op - to be implemented by subclasses
    }

    /**
     * Subclasses should implement client-specific filter tests
     * for prospective collection searches.
     */
    protected void verifyCategoryTabs() {
        // no-op - to be implemented by subclasses
    }

    /**
     * Verify save search criteria.
     */
    protected void verifySaveSearchCriteria() {
        goToSearchPage(false);
        clickAndWait("btn_search");
        String disease = selenium.getTable("specimen.1.2");
        if (!selenium.isVisible("filters")) {
            clickShowHideFilters(true);
        }
        selenium.type("externalId", "test");
        String diseaseToSearch = disease;
        progressiveSelectFromAutocomplete("pathologicalCharacteristic", diseaseToSearch, diseaseToSearch);
        clickAndWait("btn_search");
        searchAndVerifyResult(1);
        // go to save search but cancel the save
        clickAndWait("link=Save Search As");
        selenium.selectFrame("popupFrame");
        clickAndWait("link=Cancel");
        selenium.selectFrame("relative=up");
        assertTrue(selenium.isVisible("filters"));
        assertFalse(selenium.isVisible("criteria"));
        assertFalse(selenium.isElementPresent("specimen"));
        assertFalse(selenium.isTextPresent("Your search has been saved successfully."));

        // go again to save search but this time save it
        clickAndWait("link=Save Search As");
        selenium.selectFrame("popupFrame");
        // test the validation
        clickAndWait("saveSearch_submitButton");
        assertTrue(selenium.isTextPresent("Name must be set"));
        selenium.type("name", SAVED_SPECIMEN_SEARCH_NAME_ROOT);
        String description = "This is a search for " + disease + " samples.";
        selenium.type("description", description);
        clickAndWait("saveSearch_submitButton");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertEquals("Your search has been saved successfully.",
                selenium.getText("//div[@id='filters']/div[1]/div/p"));
        assertTrue(selenium.isTextPresent("Update " + SAVED_SPECIMEN_SEARCH_NAME_ROOT));

        // update the saved specimen search
        clickAndWait("link=Update " + SAVED_SPECIMEN_SEARCH_NAME_ROOT);
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent(SAVED_SPECIMEN_SEARCH_NAME_ROOT));
        assertEquals(description, selenium.getValue("description"));
        selenium.type("description", "This is an updated search for " + disease + " samples.");
        clickAndWait("saveSearch_submitButton");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertEquals("Your search has been saved successfully.",
                selenium.getText("//div[@id='filters']/div[1]/div/p"));

        // attempt to save a specimen search of the same name
        clickAndWait("link=Save Search As");
        selenium.selectFrame("popupFrame");
        clickAndWait("saveSearch_submitButton");
        assertTrue(selenium.isTextPresent("A saved specimen search with this name already exists."));
        clickAndWait("link=Cancel");
        selenium.selectWindow(null);
        pause(LONG_DELAY);

        // leave search page, go back
        clickAndWait("link=Home");
        clickAndWait("link=Advanced Search >>");
        selenium.type("externalId", "test");
        clickAndWait("btn_search");
        searchAndVerifyResult(1, 2);

        // go to manage my searches
        clickAndWait("link=Manage My Saved Searches");
        selenium.selectFrame("popupFrame");
        assertEquals(SAVED_SPECIMEN_SEARCH_NAME_ROOT, selenium.getText("link=" + SAVED_SPECIMEN_SEARCH_NAME_ROOT));
        // click on the saved search
        clickAndWait("link=" + SAVED_SPECIMEN_SEARCH_NAME_ROOT);
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertEquals("Your search has been loaded successfully.",
                selenium.getText("//div[@id='filters']/div[1]/div/p"));
        searchAndVerifyResult(1);

        // delete the saved specimen search
        clickAndWait("link=Manage My Saved Searches");
        selenium.selectFrame("popupFrame");
        assertEquals(SAVED_SPECIMEN_SEARCH_NAME_ROOT, selenium.getText("link=" + SAVED_SPECIMEN_SEARCH_NAME_ROOT));
        clickAndWait("link=Delete");
        assertTrue(selenium.isTextPresent("Search deleted successfully."));
        assertFalse(selenium.isTextPresent(SAVED_SPECIMEN_SEARCH_NAME_ROOT));
        assertTrue(selenium.isTextPresent("Nothing found to display"));
        clickAndWait("link=Close");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertFalse(selenium.isTextPresent("Update " + SAVED_SPECIMEN_SEARCH_NAME_ROOT));
        assertFalse(selenium.isVisible("filters"));

        // re-create the saved specimen search
        selenium.click("normalSample");
        pause(SHORT_DELAY);
        clickAndWait("link=Save Search As");
        selenium.selectFrame("popupFrame");
        selenium.type("name", SAVED_SPECIMEN_SEARCH_NAME_ROOT);
        description = "This is a search for " + disease + " samples.";
        selenium.type("description", description);
        clickAndWait("saveSearch_submitButton");
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertEquals("Your search has been saved successfully.",
                selenium.getText("//div[@id='filters']/div[1]/div/p"));
        assertTrue(selenium.isTextPresent("Update " + SAVED_SPECIMEN_SEARCH_NAME_ROOT));
        assertTrue(selenium.isChecked("normalSample"));
        clickAndWait("link=Manage My Saved Searches");
        selenium.selectFrame("popupFrame");
        assertEquals(SAVED_SPECIMEN_SEARCH_NAME_ROOT, selenium.getText("link=" + SAVED_SPECIMEN_SEARCH_NAME_ROOT));
        // click on the saved search
        clickAndWait("link=" + SAVED_SPECIMEN_SEARCH_NAME_ROOT);
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        assertEquals("Your search has been loaded successfully.",
                selenium.getText("//div[@id='filters']/div[1]/div/p"));
        assertTrue(selenium.isChecked("normalSample"));
    }

    /**
     * Subclasses should implement client-specific filter tests
     * for prospective collection searches.
     */
    protected void verifySearchDisplayOptions() {
        // no-op - to be implemented by subclasses
    }

    /**
     * Clears all text search filters.
     */
    private void clearTextFilters() {
        String[] textFilters = new String[]{"id", "min_collectionYear", "max_collectionYear",
                "minimumAvailableQuantity", "min_patientAgeAtCollection", "max_patientAgeAtCollection",
                "externalId"};
        for (String filter : textFilters) {
            selenium.type(filter, "");
        }

        clickShowHideFilters(true);
        selenium.type("pathologicalCharacteristic", "");
        clickShowHideFilters(false);
    }

    /**
     * Clears all select search filters.
     */
    private void clearSelectFilters() {
        String[] selectFilters = new String[] {getSpecimenTypeFieldName(), getSpecimenClassFieldName()};
        for (String filter : selectFilters) {
            removeSelection(filter);
        }
    }

    /**
     * Clears all search filters.
     */
    protected void clearAllFilters() {
        clearTextFilters();
        clearSelectFilters();
        clearMultiSelects();
    }

    /**
     * clear all multi select filters.
     */
    protected void clearMultiSelects() {
        selenium.removeAllSelections("multiSelectParamMap['participant.gender']");
        selenium.removeAllSelections("multiSelectParamMap['participant.ethnicity']");
        selenium.removeAllSelections("multiSelectCollectionParamMap['participant.races']");
    }

    /**
     * Fills in all specimen filters.
     */
    protected void fillInAllFilters() {
        selenium.type("id", "123");
        selenium.type("externalId", "abc");
        if (!selenium.isVisible("filters")) {
            clickShowHideFilters(true);
        }
        selenium.select(getSpecimenTypeFieldName(), "label=Lavage");
        selenium.select(getSpecimenClassFieldName(), "label=Molecular");
        selenium.type("min_collectionYear", "2010");
        selenium.type("max_collectionYear", "2012");
        selenium.type("minimumAvailableQuantity", "10.0");
        selenium.select("object.availableQuantityUnits", "label=count");
        selenium.select("multiSelectParamMap['participant.gender']", "label=Unspecified");
        selenium.select("multiSelectCollectionParamMap['participant.races']", "label=White");
        selenium.select("multiSelectParamMap['participant.ethnicity']", "label=Unknown");
        selenium.type("min_patientAgeAtCollection", "100");
        selenium.type("max_patientAgeAtCollection", "200");
        selenium.select("object.patientAgeAtCollectionUnits", "label=months");
    }

    /**
     * Searches and verifies results.
     * @param indexes the indexes
     */
    protected void searchAndVerifyResult(int... indexes) {
        clickAndWait("btn_search");
        assertFalse(selenium.isVisible("filters"));
        assertTrue(selenium.isVisible("criteria"));
        assertTrue(selenium.isElementPresent("specimen"));
        assertTrue(selenium.isVisible("specimen"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        if (indexes.length == 1) {
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        } else {
            assertTrue(selenium.isTextPresent("1-" + indexes.length + " of " + indexes.length + " Results"));
        }
        assertTrue(selenium.isTextPresent("1 of 1 Page"));
        for (int index : indexes) {
            assertTrue(selenium.isTextPresent("test " + index));
        }
    }

    /**
     * Goes back to list.
     */
    private void backToList() {
        String expectedHeader = selenium.getTable("specimen.1.2").replaceAll("\\s+", "");
        clickAndWait("link=1");
        String actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
        assertTrue(selenium.isTextPresent("test 1"));
        assertTrue(selenium.isElementPresent("link=Edit"));
        clickAndWait("link=Back");
        assertFalse(selenium.isVisible("filters"));
        assertTrue(selenium.isTextPresent("Search for " + ClientProperties.getBiospecimenTerm() + "s"));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        clearAllFilters();
        clickAndWait("btn_search");
    }

    /**
     * Test select all.
     */
    protected void selectAll() {
        clickAndWait("btn_search");
        verifySelectAll(true);
        verifySelectAll(false);
    }

    private void verifySelectAll(boolean checked) {
        selenium.click("selectall");
        assertEquals(checked, selenium.isChecked("selectall"));
        String checkboxPattern = "xpath=//table[@id='row']/tbody/tr[%d]/input[@type='checkbox']";

        int index = 1;
        while (true) {
            if (!selenium.isElementPresent("xpath=//table[@id='row']/tbody/tr[" + index + "]")) {
                break;
            }
            assertEquals(checked, selenium.isChecked(String.format(checkboxPattern, index)));
            index++;
        }
    }

    /**
     * Tests showing/hiding of filters.
     */
    protected void showHideFilters() {
        assertFalse(selenium.isVisible("filters"));
        assertTrue(selenium.isVisible("criteria"));
        clickShowHideFilters(true);
        assertElementNotVisible("criteria", DELAY);
        clickShowHideFilters(false);
        assertElementVisible("criteria", DELAY);
        assertElementNotVisible("filters", DELAY);
    }

    /**
     * Tests session storage.
     */
    protected void sessionStorage() {
        fillInAllFilters();

        clickAndWait("btn_search");
        assertFalse(selenium.isVisible("filters"));
        assertTrue(selenium.isVisible("criteria"));
        assertTrue(selenium.isElementPresent("specimen"));
        assertTrue(selenium.isVisible("specimen"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("0 Results found."));

        //leave search page, go back, and verify all filters are populated
        clickAndWait("link=Home");
        goToSearchPage(false);
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("0 Results found."));
        assertEquals("123", selenium.getValue("id"));
        assertEquals("abc", selenium.getValue("externalId"));
        assertEquals("Lavage", selenium.getSelectedLabel(getSpecimenTypeFieldName()));
        assertEquals("Molecular", selenium.getSelectedLabel(getSpecimenClassFieldName()));
        assertEquals("2010", selenium.getValue("min_collectionYear"));
        assertEquals("2012", selenium.getValue("max_collectionYear"));
        assertEquals("10", selenium.getValue("minimumAvailableQuantity"));
        assertEquals("count", selenium.getSelectedLabel("object.availableQuantityUnits"));
        assertEquals("UNSPECIFIED", selenium.getSelectedValues("multiSelectParamMap['participant.gender']")[0]);
        assertEquals("WHITE", selenium.getSelectedValues("multiSelectCollectionParamMap['participant.races']")[0]);
        assertEquals("UNKNOWN", selenium.getSelectedValues("multiSelectParamMap['participant.ethnicity']")[0]);
        assertEquals("100", selenium.getValue("min_patientAgeAtCollection"));
        assertEquals("200", selenium.getValue("max_patientAgeAtCollection"));
        assertEquals("months", selenium.getSelectedLabel("object.patientAgeAtCollectionUnits"));
    }

    /**
     * Verifies export all is present.
     */
    protected void verifyExportPresent() {
        assertTrue(selenium.isElementPresent("link=Export All"));
    }

    /**
     * Verifies the 'Add to Cart' button is disabled if no specimens
     * are selected, and enabled if one or more are selected.
     */
    protected void verifyCartDisabled() {
        clickAndWait("Link=Sign Out");
        loginAsAdmin();
        goToSearchPage();
        clickAndWait("btn_search");
        assertFalse(selenium.isEditable("btn_addtocart"));
        selenium.click("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/input[1]");
        assertTrue(selenium.isEditable("btn_addtocart"));
        selenium.click("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/input[1]");
        assertFalse(selenium.isEditable("btn_addtocart"));
        selenium.click("selectall");
        assertTrue(selenium.isEditable("btn_addtocart"));
        selenium.click("selectall");
        assertFalse(selenium.isEditable("btn_addtocart"));
        clickShowHideFilters(false);
        fillInAllFilters();
        clickAndWait("btn_search");
        selenium.click("selectall");
        assertFalse(selenium.isEditable("btn_addtocart"));
        assertFalse(selenium.isVisible("filters"));
        assertTrue(selenium.isVisible("criteria"));
        assertTrue(selenium.isElementPresent("specimen"));
        assertTrue(selenium.isVisible("specimen"));
    }

    /**
     * Verifies that when the number of shown results changes that the page is maintained and no other action is taken.
     */
    protected void verifyResultsResize() {
        selenium.select("pageSize", "label=100");
        waitForPageToLoad();
        assertTrue(selenium.isTextPresent("Search Results"));
    }

    /**
     * verify the functionality of the normal sample checkbox and the associated autocompleter.
     */
    protected void exerciseNormalSampleCheckbox() {
        clickAndWait("Link=Sign Out");
        loginAsUser1();
        goToSearchPage();
        selenium.click("normalSample");
        pause(SHORT_DELAY);
        assertTrue(selenium.isChecked("normalSample"));
        assertFalse(selenium.isEditable("pathologicalCharacteristic"));
        assertEquals(ClientProperties.getNormalTerm(), selenium.getValue("pathologicalCharacteristic"));
        String normalId = selenium.getValue("pathologicalCharacteristicHidden");
        assertTrue(StringUtils.isNotBlank(normalId));
        assertTrue(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));
        assertEquals(ClientProperties.getNormalTerm(), selenium.getValue("pathologicalCharacteristicAutoHidden"));

        clickAndWait("btn_search");
        clickShowHideFilters(true);
        assertTrue(selenium.isChecked("normalSample"));
        assertFalse(selenium.isEditable("pathologicalCharacteristic"));
        assertEquals(ClientProperties.getNormalTerm(), selenium.getValue("pathologicalCharacteristic"));
        assertEquals(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertTrue(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));
        assertEquals(ClientProperties.getNormalTerm(), selenium.getValue("pathologicalCharacteristicAutoHidden"));

        selenium.click("normalSample");
        pause(SHORT_DELAY);
        assertFalse(selenium.isChecked("normalSample"));
        assertTrue(selenium.isEditable("pathologicalCharacteristic"));
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristicHidden")));
        assertNotSame(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertFalse(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));

        clickAndWait("btn_search");
        clickShowHideFilters(true);
        assertFalse(selenium.isChecked("normalSample"));
        assertTrue(selenium.isEditable("pathologicalCharacteristic"));
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristicHidden")));
        assertNotSame(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertFalse(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));

        selectFromAutocomplete("pathologicalCharacteristic", ClientProperties.getAutocompleteDiseaseNameStart(),
                ClientProperties.getAutocompleteDiseaseFullName());
        assertFalse(selenium.isChecked("normalSample"));
        assertTrue(selenium.isEditable("pathologicalCharacteristic"));
        assertFalse(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));
        assertEquals(ClientProperties.getAutocompleteDiseaseFullName(),
                selenium.getValue("pathologicalCharacteristic"));
        assertFalse(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristicHidden")));
        assertNotSame(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertFalse(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));

        clickAndWait("btn_search");
        assertFalse(selenium.isChecked("normalSample"));
        assertTrue(selenium.isEditable("pathologicalCharacteristic"));
        assertFalse(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));
        assertEquals(ClientProperties.getAutocompleteDiseaseFullName(),
                selenium.getValue("pathologicalCharacteristic"));
        assertFalse(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristicHidden")));
        assertNotSame(normalId, selenium.getValue("pathologicalCharacteristicHidden"));
        assertFalse(selenium.isElementPresent("pathologicalCharacteristicAutoHidden"));
    }

    private void verifyFilteredDiseaseSelections() {
        clickAndWait("Link=Sign Out");
        loginAsUser1();
        goToSearchPage();
        String optionListXPath = "xpath=//ul[@role]";
        String[] diseases = ClientProperties.getSpecimenSearchDiseases();
        String optionXPath = optionListXPath + "/li[%d]/a";
        selenium.click("pathologicalCharacteristicArrow");
        pause(DELAY);
        assertTrue(selenium.isElementPresent(optionListXPath));
        assertTrue(selenium.isVisible(optionListXPath));
        for (int i = 1; i <= diseases.length; i++) {
            assertTrue(selenium.isElementPresent(String.format(optionXPath, i)));
            assertEquals(diseases[i - 1], selenium.getText(String.format(optionXPath, i)));
        }
        if (ClientProperties.isSpecimenSearchDiseasesFullList()) {
            assertFalse(selenium.isElementPresent(String.format(optionXPath, diseases.length + 1)));
        }
    }

    /**
     * Verifies that field's presence matches the presentFlag.
     *
     * @param category category to be selected
     * @param presentFlag true if fields must be present, false if they should be absent
     * @param categoryFieldIds fields to check
     */
    protected void verifyFieldsPresence(String category, boolean presentFlag, String... categoryFieldIds) {
        selenium.click("link=" + category);
        pause(SHORT_DELAY);
        if (!selenium.isTextPresent("Check all") && !selenium.isTextPresent("Uncheck all")) {
            selenium.click("link=" + category);
            pause(SHORT_DELAY);
        }
        for (String categoryFieldId : categoryFieldIds) {
            String fullFieldId = category.replace(' ', '_') + "_" + categoryFieldId;
            assertEquals(presentFlag, selenium.isElementPresent(fullFieldId));
        }
    }
}
