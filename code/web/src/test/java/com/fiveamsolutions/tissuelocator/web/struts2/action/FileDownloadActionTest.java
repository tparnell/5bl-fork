/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;
import junit.framework.Assert;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.LobHolder;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.opensymphony.xwork2.Action;

/**
 * @author smiller
 *
 */
public class FileDownloadActionTest {

    /**
     * tests the action.
     */
    @Test
    public void testPrepareAndDownload() {
        LobHolderServiceLocal mockService = mock(LobHolderServiceLocal.class);
        LobHolder dummyHolder = new LobHolder(new byte[1]);
        dummyHolder.setId(1L);
        stub(mockService.getLob(anyLong()))
            .toReturn(dummyHolder);
        FileDownloadAction action = new FileDownloadAction(mockService);
        action.getFile().setId(null);
        action.prepare();
        Assert.assertEquals(Action.SUCCESS, action.downloadFile());
        
        action.getFile().setId(1L);
        action.prepare();
        Assert.assertEquals(1L, action.getFile().getId().longValue());
        Assert.assertEquals(Action.SUCCESS, action.downloadFile());
        action.getFile().setData(new byte[] {1});
        Assert.assertNotNull(action.getFileStream());
    }
}
