/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.StringDynamicFieldDefinition;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.category.DisplayOption;
import com.fiveamsolutions.tissuelocator.data.config.category.SearchResultFieldDisplaySetting;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.SearchResultDisplaySettingsServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.SpecimenSearchServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author cgoina
 *
 */
public class SpecimenSearchResultConfigActionTest extends AbstractTissueLocatorWebTest {

    private static final String DYNAMIC_EXTENSION_FIELD = "test";
    private static final String DYNAMIC_EXTENSION_FIELD_DISPLAY = "TEST LABEL";

    private SpecimenSearchResultConfigAction actionToTest;
    private Map<String, Object> actionSession;

    /**
     * Test setup.
     */
    @Before
    public void initialize() {
        actionToTest = getSpecimenSearchResultConfigAction();
        actionSession = new HashMap<String, Object>();
        actionToTest.setSession(actionSession);
        SearchResultDisplaySettingsServiceStub displaySettingsService =
            getTestSearchResultDisplaySettingService();
        displaySettingsService.resetData();
    }

    /**
     * Test save method.
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testSaveSpecimenResultsDisplayOptions() {
        SearchResultDisplaySettingsServiceStub displaySettingsService =
            getTestSearchResultDisplaySettingService();
        Map<String, SearchResultFieldDisplaySetting> savedDisplayConfig =
            (Map<String, SearchResultFieldDisplaySetting>) actionSession
                .get(SearchResultDisplayConfigHelper.SPECIMEN_DISPLAY_RESULT_OPTIONS);
        assertNull(savedDisplayConfig);
        actionToTest.prepare();
        savedDisplayConfig = (Map<String, SearchResultFieldDisplaySetting>) actionSession
                .get(SearchResultDisplayConfigHelper.SPECIMEN_DISPLAY_RESULT_OPTIONS);
        assertNotNull(savedDisplayConfig);

        assertFalse(actionToTest.getCategoryNames().isEmpty());
        assertEquals(Action.INPUT, actionToTest.configureSpecimenResultsDisplayOptions());

        // disable all fields
        for (String category : actionToTest.getCategoryNames()) {
            List<SearchResultFieldDisplaySetting> categoryFieldDisplaySettings = actionToTest
                    .getSpecimenResultFieldsByCategory(category);
            for (SearchResultFieldDisplaySetting fieldDisplaySetting : categoryFieldDisplaySettings) {
                assertTrue(Arrays.asList(DisplayOption.values()).contains(fieldDisplaySetting.getDisplayFlag()));
                fieldDisplaySetting.setDisplayFlag(DisplayOption.DISABLED);
            }
        }
        // save the configuration
        assertEquals(Action.SUCCESS, actionToTest.saveSpecimenResultsDisplayOptions());
        // check that the field display options changed
        for (SearchResultFieldDisplaySetting fieldDisplaySetting : savedDisplayConfig.values()) {
            assertEquals(DisplayOption.DISABLED, fieldDisplaySetting.getDisplayFlag());
        }
        // now reinitialize
        displaySettingsService.resetData();
        actionToTest.prepare();
        // check that the field display options didn't change
        for (String category : actionToTest.getCategoryNames()) {
            List<SearchResultFieldDisplaySetting> categoryFieldDisplaySettings = actionToTest
                    .getSpecimenResultFieldsByCategory(category);
            for (SearchResultFieldDisplaySetting fieldDisplaySetting : categoryFieldDisplaySettings) {
                assertEquals(DisplayOption.DISABLED, fieldDisplaySetting.getDisplayFlag());
            }
        }
        // test what happens if the display options get cleaned up
        savedDisplayConfig = (Map<String, SearchResultFieldDisplaySetting>) actionSession
                .get(SearchResultDisplayConfigHelper.SPECIMEN_DISPLAY_RESULT_OPTIONS);
        savedDisplayConfig.clear();
        displaySettingsService.resetData();
        actionToTest.prepare();
        boolean atLeastOneFieldIsEnabled = false;
        for (String category : actionToTest.getCategoryNames()) {
            List<SearchResultFieldDisplaySetting> categoryFieldDisplaySettings = actionToTest
                    .getSpecimenResultFieldsByCategory(category);
            for (SearchResultFieldDisplaySetting fieldDisplaySetting : categoryFieldDisplaySettings) {
                if (fieldDisplaySetting.getDisplayFlag().equals(DisplayOption.ENABLED)) {
                    atLeastOneFieldIsEnabled = true;
                }
            }
        }
        assertTrue(atLeastOneFieldIsEnabled);
        actionToTest.setMakeCurrentSettingsPersistentFlag(true);
        // disable all fields
        for (String category : actionToTest.getCategoryNames()) {
            List<SearchResultFieldDisplaySetting> categoryFieldDisplaySettings = actionToTest
                    .getSpecimenResultFieldsByCategory(category);
            for (SearchResultFieldDisplaySetting fieldDisplaySetting : categoryFieldDisplaySettings) {
                assertTrue(Arrays.asList(DisplayOption.values()).contains(fieldDisplaySetting.getDisplayFlag()));
                fieldDisplaySetting.setDisplayFlag(DisplayOption.DISABLED);
            }
        }
        // save the configuration
        assertEquals(Action.SUCCESS, actionToTest.saveSpecimenResultsDisplayOptions());
        actionSession.clear();
        actionToTest.prepare();
        assertFalse(actionToTest.getCategoryNames().isEmpty());
        for (String category : actionToTest.getCategoryNames()) {
            List<SearchResultFieldDisplaySetting> categoryFieldDisplaySettings = actionToTest
                    .getSpecimenResultFieldsByCategory(category);
            for (SearchResultFieldDisplaySetting fieldDisplaySetting : categoryFieldDisplaySettings) {
                assertEquals(DisplayOption.DISABLED, fieldDisplaySetting.getDisplayFlag());
            }
        }
        
        // test saving empty settings
        actionToTest.prepare();
        actionToTest.setCategoryDisplayConfig(new HashMap<String, List<SearchResultFieldDisplaySetting>>());
        assertEquals(Action.SUCCESS, actionToTest.saveSpecimenResultsDisplayOptions());
        assertTrue(actionToTest.getCategoryDisplayConfig().isEmpty());
    }

    /**
     * Test that a field is added to the list only if the current role matches field's required role.
     */
    @Test
    public void testAddDisplaySettingWithRoleCheck() {
        SearchResultDisplaySettingsServiceStub displaySettingsService =
            getTestSearchResultDisplaySettingService();
        // no role
        SearchResultFieldDisplaySetting testFieldDisplaySetting = new SearchResultFieldDisplaySetting();
        testFieldDisplaySetting.setDisplayFlagAsBoolean(true);
        testFieldDisplaySetting.setFieldConfig(new GenericFieldConfig());
        testFieldDisplaySetting.getFieldConfig().setSearchFieldName("fieldName");
        testFieldDisplaySetting.setFieldCategory(new UiSearchFieldCategory());
        testFieldDisplaySetting.getFieldCategory().setCategoryName("specimen.search.biospecimen.title");
        List<SearchResultFieldDisplaySetting> defaultSettings = new ArrayList<SearchResultFieldDisplaySetting>();
        defaultSettings.add(testFieldDisplaySetting);
        displaySettingsService.setDefaultSearchResultDisplaySettings(defaultSettings);
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        Map<String, List<SearchResultFieldDisplaySetting>> settings = actionToTest.getCategoryDisplayConfig();
        assertEquals(1, settings.get("specimen.search.biospecimen.title").size());
        
        // allowed role
        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        request.addUserRole(Role.ADMINISTRATOR.getName());
        ApplicationRole testRole = new ApplicationRole();
        testRole.setName(Role.ADMINISTRATOR.getName());
        testFieldDisplaySetting.getFieldConfig().setRequiredRole(testRole);
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        settings = actionToTest.getCategoryDisplayConfig();
        assertEquals(1, settings.get("specimen.search.biospecimen.title").size());
        
        // unallowed role
        testRole.setName(Role.SUPER_ADMIN.getName());
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        settings = actionToTest.getCategoryDisplayConfig();
        assertNull(settings.get("specimen.search.biospecimen.title"));
    }
    
    /**
     * Test disabled display settings are filtered from returned settings.
     */
    @Test
    public void testFilterDisabledDisplaySettings() {
        SearchResultDisplaySettingsServiceStub displaySettingsService =
            getTestSearchResultDisplaySettingService();
        
        // valid display options
        SearchResultFieldDisplaySetting testFieldDisplaySetting = new SearchResultFieldDisplaySetting();
        testFieldDisplaySetting.setDisplayFlagAsBoolean(true);
        testFieldDisplaySetting.setFieldConfig(new GenericFieldConfig());
        testFieldDisplaySetting.getFieldConfig().setSearchFieldName("fieldName");
        testFieldDisplaySetting.setFieldCategory(new UiSearchFieldCategory());
        testFieldDisplaySetting.getFieldCategory().setCategoryName("specimen.search.biospecimen.title");
        List<SearchResultFieldDisplaySetting> defaultSettings = new ArrayList<SearchResultFieldDisplaySetting>();
        defaultSettings.add(testFieldDisplaySetting);
        displaySettingsService.setDefaultSearchResultDisplaySettings(defaultSettings);
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        Map<String, List<SearchResultFieldDisplaySetting>> settings = actionToTest.getCategoryDisplayConfig();
        assertEquals(1, settings.get("specimen.search.biospecimen.title").size());
        testFieldDisplaySetting.setDisplayFlagAsBoolean(false);
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        settings = actionToTest.getCategoryDisplayConfig();
        assertEquals(1, settings.get("specimen.search.biospecimen.title").size());
        
        // disabled display option
        testFieldDisplaySetting.setDisplayFlag(DisplayOption.NOT_APPLICABLE);
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        settings = actionToTest.getCategoryDisplayConfig();
        assertNull(settings.get("specimen.search.biospecimen.title"));
        
        // null display option
        testFieldDisplaySetting.setDisplayFlag(null);
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        settings = actionToTest.getCategoryDisplayConfig();
        assertNull(settings.get("specimen.search.biospecimen.title"));
    }
    
    /**
     * Test the getCategoryNames method.
     */
    @Test
    public void testGetCategoryNames() {
        SearchResultDisplaySettingsServiceStub displaySettingsService =
            getTestSearchResultDisplaySettingService();
        // single entry per category
        SearchResultFieldDisplaySetting testFieldDisplaySetting = new SearchResultFieldDisplaySetting();
        testFieldDisplaySetting.setDisplayFlagAsBoolean(true);
        testFieldDisplaySetting.setFieldConfig(new GenericFieldConfig());
        testFieldDisplaySetting.getFieldConfig().setSearchFieldName("fieldName");
        testFieldDisplaySetting.setFieldCategory(new UiSearchFieldCategory());
        testFieldDisplaySetting.getFieldCategory().setCategoryName("specimen.search.biospecimen.title");
        List<SearchResultFieldDisplaySetting> defaultSettings = new ArrayList<SearchResultFieldDisplaySetting>();
        defaultSettings.add(testFieldDisplaySetting);
        displaySettingsService.setDefaultSearchResultDisplaySettings(defaultSettings);
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        assertEquals(1, actionToTest.getCategoryNames().size());
        assertEquals("specimen.search.biospecimen.title", actionToTest.getCategoryNames().iterator().next());
        
        // multiple entry per category
        testFieldDisplaySetting = new SearchResultFieldDisplaySetting();
        testFieldDisplaySetting.setDisplayFlagAsBoolean(true);
        testFieldDisplaySetting.setFieldConfig(new GenericFieldConfig());
        testFieldDisplaySetting.getFieldConfig().setSearchFieldName("fieldName2");
        testFieldDisplaySetting.setFieldCategory(new UiSearchFieldCategory());
        testFieldDisplaySetting.getFieldCategory().setCategoryName("specimen.search.biospecimen.title");
        defaultSettings.add(testFieldDisplaySetting);
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        assertEquals(1, actionToTest.getCategoryNames().size());
        assertEquals("specimen.search.biospecimen.title", actionToTest.getCategoryNames().iterator().next());
        
        // multiple categories
        testFieldDisplaySetting = new SearchResultFieldDisplaySetting();
        testFieldDisplaySetting.setDisplayFlagAsBoolean(true);
        testFieldDisplaySetting.setFieldConfig(new GenericFieldConfig());
        testFieldDisplaySetting.getFieldConfig().setSearchFieldName("fieldName2");
        testFieldDisplaySetting.setFieldCategory(new UiSearchFieldCategory());
        testFieldDisplaySetting.getFieldCategory().setCategoryName("Miscellaneous Category");
        defaultSettings.add(testFieldDisplaySetting);
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        assertEquals(2, actionToTest.getCategoryNames().size());
        assertTrue(actionToTest.getCategoryNames().contains("specimen.search.biospecimen.title"));
        assertTrue(actionToTest.getCategoryNames().contains("Miscellaneous Category"));
        
        // no categories
        displaySettingsService.setDefaultSearchResultDisplaySettings(new ArrayList<SearchResultFieldDisplaySetting>());
        actionToTest.setSession(new HashMap<String, Object>());
        actionToTest.prepare();
        assertTrue(actionToTest.getCategoryNames().isEmpty());
    }
    
    private SpecimenSearchResultConfigAction getSpecimenSearchResultConfigAction() {
        return getSpecimenSearchResultConfigAction(new SpecimenSearchServiceStub());
    }

    private SpecimenSearchResultConfigAction getSpecimenSearchResultConfigAction(SpecimenSearchServiceStub stub) {
        return new SpecimenSearchResultConfigAction(stub, getTestUserService(), getTestAppSettingService(), 
                getTestUiSearchFieldCategoryService(), getTestSearchResultDisplaySettingService(), 
                getTestUiDynamicFieldCategoryService(), getTestUiSectionService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                List<AbstractDynamicFieldDefinition> deFieldDefinitions =
                    new ArrayList<AbstractDynamicFieldDefinition>();
                AbstractDynamicFieldDefinition deFieldDefinition = new StringDynamicFieldDefinition();
                deFieldDefinition.setEntityClassName(Specimen.class.getName());
                deFieldDefinition.setFieldName(DYNAMIC_EXTENSION_FIELD);
                deFieldDefinition.setFieldDisplayName(DYNAMIC_EXTENSION_FIELD_DISPLAY);
                deFieldDefinitions.add(deFieldDefinition);
                return deFieldDefinitions;
            }
        };
    }

}
