/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.support;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.LobHolder;
import com.fiveamsolutions.tissuelocator.data.support.Question;
import com.fiveamsolutions.tissuelocator.data.support.QuestionResponse;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.QuestionServiceLocal;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.opensymphony.xwork2.Action;


/**
 * @author ddasgupta
 *
 */
public class QuestionAdministrationActionTest extends AbstractTissueLocatorWebTest {

    /**
     * Tests the list method.
     */
    @Test
    public void testList() {
        QuestionAdministrationAction action = getAction();
        assertNull(action.getObjects().getList());
        String result = action.list();
        assertEquals(Action.SUCCESS, result);
        assertNotNull(action.getObjects().getList());
        assertEquals(1, action.getObjects().getList().size());
    }

    @SuppressWarnings("unchecked")
    private QuestionAdministrationAction getAction() {
        QuestionServiceLocal mock = mock(QuestionServiceLocal.class);
        stub(mock.count(any(SearchCriteria.class))).toReturn(1);
        List<Question> results = Collections.singletonList(new Question());
        stub(mock.search(any(SearchCriteria.class))).toReturn(results);
        stub(mock.search(any(SearchCriteria.class), any(PageSortParams.class))).toReturn(results);

        LobHolderServiceLocal lobServiceMock = mock(LobHolderServiceLocal.class);
        LobHolder lob = new LobHolder(new byte[] {1});
        lob.setId(1L);
        stub(lobServiceMock.getLob(anyLong())).toReturn(lob);

        return new QuestionAdministrationAction(mock, lobServiceMock);
    }

    /**
     * Test the review method.
     */
    @Test
    public void testReview() {
        QuestionAdministrationAction action = getAction();
        action.setAdminResponse(new QuestionResponse());
        assertEquals(Action.SUCCESS, action.review());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * Test the unsupported no-arg constructor.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedConstructor() {
        new QuestionAdministrationAction();
        fail();
    }
}
