/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * grep a file for contents.
 * @author vsemenov
 *
 */
public class FileGrep {


    private String[] excludes = new String[] {};

    /**
     * set the excludes.
     * @param excludes the excludes to set
     */
    public void setExcludes(String...excludes) {
        this.excludes = excludes;

    }

    /**
     * determine if a key is not to be excluded.
     * @param key the key to check
     * @return whether the given key is not to be excluded.
     */
    public boolean isNotExcluded(String key) {
        return !isExcluded(key);
    }

    /**
     * determine if a key is to be excluded.
     * @param key the key to check
     * @return whether the given key is to be excluded.
     */
    public boolean isExcluded(String key) {
        List<String> excludeKeys = Arrays.asList(excludes);
        for (String excluded : excludeKeys) {
            if (key.startsWith(excluded)) {
                return true;
            }
        }
        return false;
    }

    /**
     * escape a string.
     * @param value the value to escape
     * @return the escaped string.
     */
    public String escape(String value) {
        return value.replaceAll("\\.", "\\\\.");
    }

    /**
     * determine whether a file includes a pattern.
     * @param file the file to check
     * @param pattern the pattern to check
     * @return whether the file contains the pattern
     * @throws IOException on error
     */
    public boolean contains(File file, String pattern) throws IOException {
        Pattern p = Pattern.compile(pattern);
        // Get a Channel for the source file
        FileInputStream fis = null;
        FileChannel fc = null;
        try {
            fis = new FileInputStream(file);
            fc = fis.getChannel();

            // Get a CharBuffer from the source file
            ByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, (int) fc.size());
            Charset cs = Charset.forName("8859_1");
            CharsetDecoder cd = cs.newDecoder();
            CharBuffer cb = cd.decode(bb);

            // Run some matches
            Matcher m = p.matcher(cb);
            return m.find();
        } finally {
            if (fc != null) {
                fc.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
    }
}
