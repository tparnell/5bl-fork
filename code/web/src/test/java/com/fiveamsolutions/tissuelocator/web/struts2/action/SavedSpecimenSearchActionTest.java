/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsConfigurator;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.SavedSpecimenSearch;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.search.RangeSearchCondition;
import com.fiveamsolutions.tissuelocator.service.search.SavedSpecimenSearchServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SearchCondition;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchCriteria;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @author smiller
 *
 */
public class SavedSpecimenSearchActionTest extends AbstractTissueLocatorWebTest {

    private static final String SPECIMEN_SEARCH_PAGE = "specimenSearchPage";

    /**
     * Test prepare with null.
     *
     * @throws IOException on error
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testPrepare() throws IOException {
        SavedSpecimenSearchServiceLocal mockService = mock(SavedSpecimenSearchServiceLocal.class);
        DynamicExtensionsConfigurator mockConfigurator = mock(DynamicExtensionsConfigurator.class);
        SavedSpecimenSearchAction action = new SavedSpecimenSearchAction(mockConfigurator, mockService, 
                getTestSearchFieldConfigService());
        action.prepare();
        verify(mockService, times(0)).getPersistentObject(any(Class.class), any(Long.class));
        action.getObject().setId(1L);
        action.prepare();
        verify(mockService, times(1)).getPersistentObject(any(Class.class), any(Long.class));
    }

    /**
     * Test persisting the saved specimen search.
     *
     *  @throws IOException on error
     */
    @Test
    public void testPersistSavedSpecimenSearch() throws IOException {
        SavedSpecimenSearchServiceLocal mockService = mock(SavedSpecimenSearchServiceLocal.class);
        DynamicExtensionsConfigurator mockConfigurator = mock(DynamicExtensionsConfigurator.class);
        SavedSpecimenSearchAction action = new SavedSpecimenSearchAction(mockConfigurator, mockService, 
                getTestSearchFieldConfigService());
        SavedSpecimenSearch savedSpecimenSearch = new SavedSpecimenSearch();
        savedSpecimenSearch.setName("s1");
        action.setObject(savedSpecimenSearch);
        action.setSavedSpecimenSearches(null);
        assertEquals(0, action.getActionMessages().size());
        action.prepare();
        assertEquals(SPECIMEN_SEARCH_PAGE, action.persistSavedSpecimenSearch());
        verify(mockService).persistSavedSpecimenSearch(
                any(SavedSpecimenSearch.class), any(SpecimenSearchCriteria.class));
        assertTrue(action.isSaveCompleted());
        assertEquals(SPECIMEN_SEARCH_PAGE, action.savedSuccessfully());
        assertEquals(1, action.getActionMessages().size());
        savedSpecimenSearch.setId(1L);
        assertEquals(SPECIMEN_SEARCH_PAGE, action.persistSavedSpecimenSearch());
        assertTrue(action.isSaveCompleted());
        assertEquals(SPECIMEN_SEARCH_PAGE, action.persistSavedSpecimenSearch());
        assertTrue(action.isSaveCompleted());
        assertEquals(SPECIMEN_SEARCH_PAGE, action.persistSavedSpecimenSearch());
        assertTrue(action.isSaveCompleted());
    }

    /**
     * Test loading the saved specimen search.
     *
     * @throws IOException on error
     * @throws ClassNotFoundException on error
     */
    @Test
    public void testLoadSavedSpecimenSearch() throws IOException, ClassNotFoundException {
        SpecimenSearchCriteria criteria = getTestCriteria(true);
        SavedSpecimenSearch mockSavedSpecimenSearch = mock(SavedSpecimenSearch.class);
        when(mockSavedSpecimenSearch.getSearchCriteria()).thenReturn(criteria);

        SavedSpecimenSearchServiceLocal mockService = mock(SavedSpecimenSearchServiceLocal.class);
        when(mockService.getSavedSpecimenSearchAndData(any(Long.class))).thenReturn(mockSavedSpecimenSearch);

        DynamicExtensionsConfigurator mockConfigurator = mock(DynamicExtensionsConfigurator.class);
        SavedSpecimenSearchAction action = new SavedSpecimenSearchAction(mockConfigurator, mockService, 
                getTestSearchFieldConfigService());
        action.setObject(new SavedSpecimenSearch());
        assertEquals(0, action.getActionMessages().size());
        assertEquals(SPECIMEN_SEARCH_PAGE, action.loadSavedSpecimenSearch());
        assertEquals(1, action.getActionMessages().size());

        SpecimenSearchActionHelper helper =
            new SpecimenSearchActionHelper(null, SpecimenSearchAction.class.getSimpleName());
        helper.loadSearchParamsFromSession();

        assertEquals(criteria.getMinimumAvailableQuantity(), helper.getMinimumAvailableQuantity());
        assertEquals(criteria.getMultiSelectParamValues(), helper.getMultiSelectParamMap(criteria.getCriteria()));
        assertEquals(criteria.getMultiSelectCollectionParamValues(), 
                helper.getMultiSelectCollectionParamMap(criteria.getCriteria()));
        assertEquals(criteria.getSearchConditions().iterator().next().getConditionParameters(),
                helper.getRangeSearchParameters());
    }

    /**
     * Test loading the saved specimen search with no conditions.
     *
     * @throws IOException on error
     * @throws ClassNotFoundException on error
     */
    @Test
    public void testLoadSavedSpecimenSearchNoConditions() throws IOException, ClassNotFoundException {
        SpecimenSearchCriteria criteria = getTestCriteria(false);

        SavedSpecimenSearch mockSavedSpecimenSearch = mock(SavedSpecimenSearch.class);
        when(mockSavedSpecimenSearch.getSearchCriteria()).thenReturn(criteria);

        SavedSpecimenSearchServiceLocal mockService = mock(SavedSpecimenSearchServiceLocal.class);
        when(mockService.getSavedSpecimenSearchAndData(any(Long.class))).thenReturn(mockSavedSpecimenSearch);

        DynamicExtensionsConfigurator mockConfigurator = mock(DynamicExtensionsConfigurator.class);
        SavedSpecimenSearchAction action = new SavedSpecimenSearchAction(mockConfigurator, mockService, 
                getTestSearchFieldConfigService());
        action.setObject(new SavedSpecimenSearch());
        assertEquals(0, action.getActionMessages().size());
        assertEquals(SPECIMEN_SEARCH_PAGE, action.loadSavedSpecimenSearch());
        assertEquals(1, action.getActionMessages().size());

        SpecimenSearchActionHelper helper =
            new SpecimenSearchActionHelper(null, SpecimenSearchAction.class.getSimpleName());
        helper.loadSearchParamsFromSession();

        assertEquals(criteria.getMinimumAvailableQuantity(), helper.getMinimumAvailableQuantity());
        assertEquals(criteria.getMultiSelectParamValues(), helper.getMultiSelectParamMap(criteria.getCriteria()));
        assertEquals(criteria.getMultiSelectCollectionParamValues(), 
                helper.getMultiSelectCollectionParamMap(criteria.getCriteria()));
        assertTrue(criteria.getSearchConditions().isEmpty());
    }

    /**
     * Test list saved specimen search page.
     */
    @Test
    public void testListSavedSpecimenSearch() {
        ArrayList<SavedSpecimenSearch> savedSearches = new ArrayList<SavedSpecimenSearch>();
        savedSearches.add(new SavedSpecimenSearch());
        SavedSpecimenSearchServiceLocal mockService = mock(SavedSpecimenSearchServiceLocal.class);
        when(mockService.getSavedSpecimenSearchesByOwner(any(TissueLocatorUser.class))).thenReturn(savedSearches);
        DynamicExtensionsConfigurator mockConfigurator = mock(DynamicExtensionsConfigurator.class);
        SavedSpecimenSearchAction action = new SavedSpecimenSearchAction(mockConfigurator, mockService, 
                getTestSearchFieldConfigService());
        assertEquals(ActionSupport.SUCCESS, action.listSavedSpecimenSearches());
        assertEquals(savedSearches.size(), action.getSavedSpecimenSearches().size());
    }

    /**
     * Test create saved specimen search.
     *
     * @throws ClassNotFoundException on error
     * @throws IOException on error
     */
    @Test
    public void testCreateSavedSpecimenSearch() throws IOException, ClassNotFoundException {
        SavedSpecimenSearchServiceLocal mockService = mock(SavedSpecimenSearchServiceLocal.class);
        DynamicExtensionsConfigurator mockConfigurator = mock(DynamicExtensionsConfigurator.class);
        SavedSpecimenSearchAction action = new SavedSpecimenSearchAction(mockConfigurator, mockService, 
                getTestSearchFieldConfigService());
        SavedSpecimenSearch savedSpecimenSearch = new SavedSpecimenSearch();
        savedSpecimenSearch.setId(Long.valueOf(1L));
        action.setObject(savedSpecimenSearch);
        assertEquals(ActionSupport.SUCCESS, action.createSavedSpecimenSearch());
        assertNull(savedSpecimenSearch.getId());
    }

    /**
     * Test update saved specimen search.
     *
     * @throws ClassNotFoundException on error
     * @throws IOException on error
     */
    @Test
    public void testUpdateSavedSpecimenSearch() throws IOException, ClassNotFoundException {
        SavedSpecimenSearchServiceLocal mockService = mock(SavedSpecimenSearchServiceLocal.class);
        DynamicExtensionsConfigurator mockConfigurator = mock(DynamicExtensionsConfigurator.class);
        SavedSpecimenSearchAction action = new SavedSpecimenSearchAction(mockConfigurator, mockService, 
                getTestSearchFieldConfigService());
        SavedSpecimenSearch savedSpecimenSearch = new SavedSpecimenSearch();
        savedSpecimenSearch.setId(Long.valueOf(1L));
        savedSpecimenSearch.setName("Name 1");
        action.setObject(savedSpecimenSearch);
        assertEquals(ActionSupport.SUCCESS, action.createSavedSpecimenSearch());
        action.getObject().setName("Name 2");
        assertEquals(ActionSupport.SUCCESS, action.updateSavedSpecimenSearch());
        assertEquals("Name 2", action.getObject().getName());
    }

    /**
     * Test delete saved specimen search.
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testDeleteSavedSpecimenSearch() {
        SavedSpecimenSearch savedSpecimenSearch = new SavedSpecimenSearch();
        SavedSpecimenSearchServiceLocal mockService = mock(SavedSpecimenSearchServiceLocal.class);
        when(mockService.getPersistentObject(any(Class.class), any(Long.class))).thenReturn(savedSpecimenSearch);
        DynamicExtensionsConfigurator mockConfigurator = mock(DynamicExtensionsConfigurator.class);
        SavedSpecimenSearchAction action = new SavedSpecimenSearchAction(mockConfigurator, mockService, 
                getTestSearchFieldConfigService());
        action.setObject(savedSpecimenSearch);
        assertEquals(ActionSupport.SUCCESS, action.deleteSavedSpecimenSearch());
        assertFalse(action.getActionMessages().isEmpty());
    }

    private SpecimenSearchCriteria getTestCriteria(boolean includeConditions) {
        Specimen example = new Specimen();
        example.setCollectionYear(1);
        Set<Race> races = new HashSet<Race>();
        races.add(Race.ASIAN);
        races.add(Race.WHITE);
        Map<String, Collection<Object>> multiSelectCollectionParamValues = new HashMap<String, Collection<Object>>();
        multiSelectCollectionParamValues.put("testCollectionVal", new ArrayList<Object>(races));
        Map<String, Collection<Object>> multiSelectParamValues = new HashMap<String, Collection<Object>>();
        List<Object> vals = new ArrayList<Object>();
        vals.add("testVal1");
        vals.add("testVal2");
        multiSelectParamValues.put("testField1", vals);
        List<SearchCondition> searchConditions = new ArrayList<SearchCondition>();
        if (includeConditions) {
            RangeSearchCondition condition = new RangeSearchCondition();
            condition.setPropertyName("testField2");
            condition.getConditionParameters().put("param1", "paramValue1");
            searchConditions.add(new RangeSearchCondition());
        }
        return new SpecimenSearchCriteria(example, new BigDecimal(1), multiSelectCollectionParamValues,
                multiSelectParamValues, searchConditions);
    }

}
