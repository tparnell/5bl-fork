/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.test;

import java.util.HashSet;
import java.util.Set;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;

/**
 * @author gvaughn
 *
 */
public abstract class AbstractSpecimenRequestReviewActionTest extends
        AbstractTissueLocatorWebTest {

    private Institution userInstitution;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createTestServices() {
        setTestUserService(new TissueLocatorUserServiceStub() {

            /**
             * {@inheritDoc}
             */
            @Override
            public TissueLocatorUser getByUsername(String username) {
                TissueLocatorUser user = super.getByUsername(username);
                user.setInstitution(userInstitution);
                return user;
            }

            /**
             * {@inheritDoc}
             */
            @Override
            public Set<AbstractUser> getUsersInRole(String roleName) {
                Set<AbstractUser> users = new HashSet<AbstractUser>();
                TissueLocatorUser user = new TissueLocatorUser();
                user.setInstitution(userInstitution);
                users.add(user);
                Institution i = new Institution();
                i.setName(userInstitution.getName() + "2");
                user = new TissueLocatorUser();
                user.setInstitution(i);
                users.add(user);
                return users;
            }
        });
        setTestAppSettingService(new ApplicationSettingServiceStub());
        setTestSearchFieldConfigService(new SearchFieldConfigServiceStub());
        setTestUiDynamicFieldCategoryService(new UiDynamicFieldCategoryServiceStub());
        setTestUiSearchFieldCategoryService(new UiSearchFieldCategoryServiceStub());
        setTestUiSectionService(new UiSectionServiceStub());
        setTestSearchResultDisplaySettingService(new SearchResultDisplaySettingsServiceStub());
    }

    /**
     * Create a specimen request.
     * @return a specimen request.
     */
    protected SpecimenRequest getTestRequest() {
        Institution institution1 = new Institution();
        institution1.setName("institution1");
        Institution institution2 = new Institution();
        institution2.setName("institution2");
        Institution institution3 = new Institution();
        institution3.setName("institution3");

        SpecimenRequest request = new SpecimenRequest();
        SpecimenRequestReviewVote review = new SpecimenRequestReviewVote();
        review.setInstitution(institution1);
        request.getConsortiumReviews().add(review);

        review = new SpecimenRequestReviewVote();
        review.setInstitution(institution2);
        request.getConsortiumReviews().add(review);

        review = new SpecimenRequestReviewVote();
        review.setInstitution(institution3);
        request.getConsortiumReviews().add(review);

        review = new SpecimenRequestReviewVote();
        review.setInstitution(institution1);
        request.getInstitutionalReviews().add(review);

        userInstitution = institution1;

        Specimen s1 = new Specimen();
        s1.setExternalIdAssigner(institution1);
        Specimen s2 = new Specimen();
        s2.setExternalIdAssigner(institution2);
        Specimen s3 = new Specimen();
        s3.setExternalIdAssigner(institution3);

        SpecimenRequestLineItem li1 = new SpecimenRequestLineItem();
        li1.setSpecimen(s1);
        SpecimenRequestLineItem li2 = new SpecimenRequestLineItem();
        li2.setSpecimen(s2);
        SpecimenRequestLineItem li3 = new SpecimenRequestLineItem();
        li3.setSpecimen(s3);

        request.getLineItems().add(li1);
        request.getLineItems().add(li2);
        request.getLineItems().add(li3);

        SpecimenRequestLineItem sli1 = new SpecimenRequestLineItem();
        sli1.setSpecimen(s1);
        SpecimenRequestLineItem sli2 = new SpecimenRequestLineItem();
        sli2.setSpecimen(s2);
        SpecimenRequestLineItem sli3 = new SpecimenRequestLineItem();
        sli3.setSpecimen(s3);

        Shipment shipment = new Shipment();
        shipment.getLineItems().add(sli1);
        shipment.getLineItems().add(sli2);
        shipment.getLineItems().add(sli3);
        request.getOrders().add(shipment);
        return request;
    }

    /**
     * @return the userInstitution
     */
    protected Institution getUserInstitution() {
        return userInstitution;
    }

    /**
     * @param userInstitution the userInstitution to set
     */
    protected void setUserInstitution(Institution userInstitution) {
        this.userInstitution = userInstitution;
    }

}
