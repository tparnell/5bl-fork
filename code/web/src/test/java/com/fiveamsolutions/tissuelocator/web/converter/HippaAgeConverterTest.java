/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.converter;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author smiller
 *
 */
public class HippaAgeConverterTest extends AbstractTissueLocatorWebTest {

    private static final int MAX_AGE_IN_YEARS = 90;
    private static final int MAX_AGE_IN_MONTHS = 1080;
    private static final int MAX_AGE_IN_DAYS = 32850;
    private static final int MAX_AGE_IN_HOURS = 788400;

    /**
     * tests convert to string.
     */
    @Test
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void testConvertToString() {

        HippaAgeConverter converter = new HippaAgeConverter();

        // Test the conversion with null or bad parameters in the action context
        assertEquals("N/A", converter.convertToString(null, "N/A"));

        ActionContext.getContext().setParameters(null);
        assertEquals("89", converter.convertToString(null, MAX_AGE_IN_YEARS - 1));

        HashMap<String, String[]> hMap = new HashMap<String, String[]>();
        hMap.put("blah", new String[]{TimeUnits.YEARS.toString()});
        ActionContext.getContext().setParameters((Map) hMap);

        // Test the conversion with expected inputs
        hMap.put("object.patientAgeAtCollectionUnits", new String[]{TimeUnits.YEARS.toString()});
        assertEquals(null, converter.convertToString(null, null));
        assertEquals("89", converter.convertToString(null, MAX_AGE_IN_YEARS - 1));
        assertEquals("90+", converter.convertToString(null, MAX_AGE_IN_YEARS));
        assertEquals("90+", converter.convertToString(null, MAX_AGE_IN_YEARS + 1));

        hMap.put("object.patientAgeAtCollectionUnits", new String[]{TimeUnits.MONTHS.toString()});
        assertEquals("1079", converter.convertToString(null, MAX_AGE_IN_MONTHS - 1));
        assertEquals("1080+", converter.convertToString(null, MAX_AGE_IN_MONTHS));
        assertEquals("1080+", converter.convertToString(null, MAX_AGE_IN_MONTHS + 1));

        hMap.put("object.patientAgeAtCollectionUnits", new String[]{TimeUnits.DAYS.toString()});
        assertEquals("32849", converter.convertToString(null, MAX_AGE_IN_DAYS - 1));
        assertEquals("32850+", converter.convertToString(null, MAX_AGE_IN_DAYS));
        assertEquals("32850+", converter.convertToString(null, MAX_AGE_IN_DAYS + 1));

        hMap.put("object.patientAgeAtCollectionUnits", new String[]{TimeUnits.HOURS.toString()});
        assertEquals("788399", converter.convertToString(null, MAX_AGE_IN_HOURS - 1));
        assertEquals("788400+", converter.convertToString(null, MAX_AGE_IN_HOURS));
        assertEquals("788400+", converter.convertToString(null, MAX_AGE_IN_HOURS + 1));
    }

    /**
     * tests convert from string.
     */
    @Test
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void testConvertFromString() {

        HippaAgeConverter converter = new HippaAgeConverter();

        // Test the conversion with null or bad parameters in the action context
        ActionContext.getContext().setParameters(null);
        assertEquals(MAX_AGE_IN_MONTHS - 1, converter.convertFromString(null, new String[] {"1079"}, Integer.class));

        HashMap<String, String[]> hMap = new HashMap<String, String[]>();
        hMap.put("blah", new String[]{TimeUnits.MONTHS.toString()});
        ActionContext.getContext().setParameters((Map) hMap);
        assertEquals(MAX_AGE_IN_MONTHS - 1, converter.convertFromString(null, new String[] {"1079"}, Integer.class));

        // Test the conversion with expected inputs
        hMap.put("object.patientAgeAtCollectionUnits", new String[]{TimeUnits.YEARS.toString()});
        assertEquals(MAX_AGE_IN_YEARS - 1, converter.convertFromString(null, new String[] {"89"}, Integer.class));
        assertEquals(MAX_AGE_IN_YEARS, converter.convertFromString(null, new String[] {"90"}, Integer.class));
        assertEquals(MAX_AGE_IN_YEARS, converter.convertFromString(null, new String[] {"90+"}, Integer.class));
        assertEquals(MAX_AGE_IN_YEARS, converter.convertFromString(null, new String[] {"91"}, Integer.class));

        hMap.put("object.patientAgeAtCollectionUnits", new String[]{TimeUnits.MONTHS.toString()});
        assertEquals(MAX_AGE_IN_MONTHS - 1, converter.convertFromString(null, new String[] {"1079"}, Integer.class));
        assertEquals(MAX_AGE_IN_MONTHS, converter.convertFromString(null, new String[] {"1080"}, Integer.class));
        assertEquals(MAX_AGE_IN_MONTHS, converter.convertFromString(null, new String[] {"1080+"}, Integer.class));
        assertEquals(MAX_AGE_IN_MONTHS, converter.convertFromString(null, new String[] {"1081"}, Integer.class));

        hMap.put("object.patientAgeAtCollectionUnits", new String[]{TimeUnits.DAYS.toString()});
        assertEquals(MAX_AGE_IN_DAYS - 1, converter.convertFromString(null, new String[] {"32849"}, Integer.class));
        assertEquals(MAX_AGE_IN_DAYS, converter.convertFromString(null, new String[] {"32850"}, Integer.class));
        assertEquals(MAX_AGE_IN_DAYS, converter.convertFromString(null, new String[] {"32850+"}, Integer.class));
        assertEquals(MAX_AGE_IN_DAYS, converter.convertFromString(null, new String[] {"32851"}, Integer.class));

        hMap.put("object.patientAgeAtCollectionUnits", new String[]{TimeUnits.HOURS.toString()});
        assertEquals(MAX_AGE_IN_HOURS - 1, converter.convertFromString(null, new String[] {"788399"}, Integer.class));
        assertEquals(MAX_AGE_IN_HOURS, converter.convertFromString(null, new String[] {"788400"}, Integer.class));
        assertEquals(MAX_AGE_IN_HOURS, converter.convertFromString(null, new String[] {"788400+"}, Integer.class));
        assertEquals(MAX_AGE_IN_HOURS, converter.convertFromString(null, new String[] {"788401"}, Integer.class));
    }



}
