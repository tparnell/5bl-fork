/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;

import com.fiveamsolutions.nci.commons.data.search.GroupByCriteria;
import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceBean;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.util.NameCountPair;
import com.fiveamsolutions.tissuelocator.util.ParticipantReturnRequestData;

/**
 * @author ddasgupta
 */
public class SpecimenServiceStub extends GenericServiceStub<Specimen>
    implements SpecimenServiceLocal {

    /**
     * ID of specimen that should throw IllegalStateException because of consent not withdrawable.
     */
    public static final Long CONSENT_NOT_WITHDRAWABLE_ID = 12345L;

    /**
     * ID of specimen that should throw IllegalStateException because it is not returnable.
     */
    public static final Long SPECIMEN_NOT_RETURNABLE_ID = 56789L;

    /**
     * {@inheritDoc}
     */
    public Map<SpecimenClass, Long> getCountsByClass() {
        return new HashMap<SpecimenClass, Long>();
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, Long[]> getCountsByPathologicalCharacteristic() {
        return new HashMap<String, Long[]>();
    }
    
    /**
     * {@inheritDoc}
     */
    public Map<String, Long> getPathologicalCharacteristics() {
        return new HashMap<String, Long>();
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, Long> getInstitutions() {
        return new HashMap<String, Long>();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public List<Specimen> search(SearchCriteria<Specimen> criteria, PageSortParams<Specimen> params) {
        TissueLocatorAnnotatedBeanSearchCriteria<Specimen> crit =
            (TissueLocatorAnnotatedBeanSearchCriteria<Specimen>) criteria;
        List<Specimen> results = new ArrayList<Specimen>();
        crit.getCriteria().setId(1L);
        results.add(crit.getCriteria());
        return results;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Specimen> search(SearchCriteria<Specimen> criteria, PageSortParams<Specimen> pageSortParams,
            List<? extends GroupByCriteria<Specimen>> groupByCriterias) {
        TissueLocatorAnnotatedBeanSearchCriteria<Specimen> crit =
            (TissueLocatorAnnotatedBeanSearchCriteria<Specimen>) criteria;
        List<Specimen> results = new ArrayList<Specimen>();
        crit.getCriteria().setId(1L);
        results.add(crit.getCriteria());
        return results;

    }

    /**
     * {@inheritDoc}
     */
    public Map<String, Specimen> getSpecimens(Long institutionId, Collection<String> extIds) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public Specimen importSpecimen(Specimen specimen) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void withdrawConsentForParticipant(Participant participant) {
        // do nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void withdrawConsent(Specimen specimen) {
        if (specimen.getId().equals(CONSENT_NOT_WITHDRAWABLE_ID)) {
            throw new EJBException(new IllegalStateException(SpecimenServiceBean.CONSENT_NOT_WITHDRAWABLE));
        }
        specimen.setConsentWithdrawn(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void returnToSourceInstitution(Specimen specimen) {
        if (specimen.getId().equals(SPECIMEN_NOT_RETURNABLE_ID)) {
            throw new EJBException(new IllegalStateException(SpecimenServiceBean.SPECIMEN_NOT_RETURNABLE));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<NameCountPair> getTopRequestedSpecimenTypes(Date startDate, Date endDate) {
        return new ArrayList<NameCountPair>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Long, ParticipantReturnRequestData> getReturnRequestDataByParticipant(
            Date startDate, Date endDate) {
        return new HashMap<Long, ParticipantReturnRequestData>();
    }
    
}
