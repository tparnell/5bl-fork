/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.nci.commons.util.SecurityUtils;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 *
 */
public class ForgotPasswordActionTest extends AbstractTissueLocatorWebTest {

    private static final String USERNAME = "username";
    private static final String NONCE = "nonce";
    private static final String NEW_PW = "aoeuHDIUi23456@#$%";

    /**
     * tests the forgot password action.
     * @throws Exception on error
     */
    @Test
    public void testForgotPassword() throws Exception {
        ForgotPasswordAction fpa = new ForgotPasswordAction(getTestUserService());
        assertEquals(Action.INPUT, fpa.execute());
        assertTrue(StringUtils.isBlank(getTestUserService().getUsername()));
        assertFalse(fpa.getActionErrors().isEmpty());
        assertEquals(1, fpa.getActionErrors().size());
        fpa.clearErrorsAndMessages();
        fpa.setUsername(USERNAME);
        assertEquals(Action.SUCCESS, fpa.execute());
        assertFalse(StringUtils.isBlank(getTestUserService().getUsername()));
        assertEquals(USERNAME, getTestUserService().getUsername());
        assertTrue(fpa.getActionErrors().isEmpty());
    }

    /**
     * tests the change password action.
     * @throws Exception on error
     */
    @Test
    public void testChangePassword() throws Exception {
        ChangePasswordAction cpa = new ChangePasswordAction(getTestUserService());
        cpa.setUsername(USERNAME);
        cpa.setNonce(NONCE);
        cpa.setRequested(NEW_PW);
        cpa.setRetyped(NEW_PW);
        assertEquals(Action.SUCCESS, cpa.execute());

        assertFalse(StringUtils.isBlank(getTestUserService().getUsername()));
        assertEquals(USERNAME, getTestUserService().getUsername());
        assertFalse(StringUtils.isBlank(getTestUserService().getNonce()));
        assertEquals(NONCE, getTestUserService().getNonce());
        assertFalse(StringUtils.isBlank(getTestUserService().getPassword()));
        assertEquals(NEW_PW, getTestUserService().getPassword());
        assertNotNull(getTestUserService().getObject());
        assertTrue(SecurityUtils.matches(getTestUserService().getObject().getPassword(), NEW_PW));

        assertFalse(cpa.getActionMessages().isEmpty());
        assertEquals(1, cpa.getActionMessages().size());
        cpa.clearErrorsAndMessages();
    }

    /**
     * tests the validate method.
     * @throws Exception on error
     */
    @Test
    public void testValidate() throws Exception {
        testValidateHelper("", USERNAME, NEW_PW, NEW_PW);
        testValidateHelper(NONCE, "", NEW_PW, NEW_PW);
        testValidateHelper(NONCE, USERNAME, NEW_PW + "requested", NEW_PW);
        testValidateHelper(NONCE, USERNAME, NEW_PW, NEW_PW + "retyped");
        testValidateHelper(NONCE, USERNAME, "invalid", "invalid");
    }

    private void testValidateHelper(String nonce, String username, String requested, String retyped) throws Exception {
        ChangePasswordAction cpa = new ChangePasswordAction(getTestUserService());
        cpa.setNonce(nonce);
        cpa.setRequested(requested);
        cpa.setRetyped(retyped);
        cpa.setUsername(username);
        cpa.validate();
        assertFalse(cpa.getActionErrors().isEmpty());
        cpa.clearErrorsAndMessages();
    }
}
