/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.converter;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.opensymphony.xwork2.conversion.TypeConversionException;

/**
 * @author ddasgupta
 *
 */
public class BigDecimalConverterTest extends AbstractTissueLocatorWebTest {

    /**
     * tests convert to string.
     */
    @Test
    public void testConvertToString() {
        BigDecimalConverter bdc = new BigDecimalConverter();
        assertEquals("", bdc.convertToString(null, null));
        assertEquals("1.0", bdc.convertToString(null, new BigDecimal("1.0")));
    }

    /**
     * tests convert from string.
     */
    @Test(expected = TypeConversionException.class)
    public void testConvertFromString() {
        BigDecimalConverter bdc = new BigDecimalConverter();
        String[] values = new String[1];
        values[0] = null;
        assertEquals(null, bdc.convertFromString(null, values, BigDecimal.class));
        values[0] = "";
        assertEquals(null, bdc.convertFromString(null, values, BigDecimal.class));
        values[0] = "1.0";
        assertEquals(new BigDecimal("1"), bdc.convertFromString(null, values, BigDecimal.class));
        values[0] = "1234";
        assertEquals(new BigDecimal("1234"), bdc.convertFromString(null, values, BigDecimal.class));
        values[0] = "1.23";
        assertEquals(new BigDecimal("1.23"), bdc.convertFromString(null, values, BigDecimal.class));
        values[0] = "1,234";
        assertEquals(new BigDecimal("1234"), bdc.convertFromString(null, values, BigDecimal.class));
        values[0] = "1,234.56";
        assertEquals(new BigDecimal("1234.56"), bdc.convertFromString(null, values, BigDecimal.class));
        values[0] = "invalid";
        bdc.convertFromString(null, values, BigDecimal.class);
    }
}
