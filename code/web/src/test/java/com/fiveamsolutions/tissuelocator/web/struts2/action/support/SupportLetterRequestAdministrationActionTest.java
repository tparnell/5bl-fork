/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.support;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

import java.io.File;
import java.net.URISyntaxException;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.SupportLetterRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 */
public class SupportLetterRequestAdministrationActionTest extends AbstractTissueLocatorWebTest {

    /**
     * Test the prepare method.
     * @throws URISyntaxException on error
     */
    @Test
    public void testPrepare() throws URISyntaxException {
        SupportLetterRequestAdministrationAction action = getAction();
        SupportLetterRequest oldRequest = action.getObject();
        action.prepare();
        assertNull(action.getObject().getLetter());

        action.setLetter(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setLetterFileName("log4j.properties");
        action.setLetterContentType("text/plain");
        action.prepare();
        assertEquals(oldRequest, action.getObject());
        assertNotNull(action.getObject().getLetter());
        assertTrue(action.getObject().getLetter().getLob().getData().length > 0);
        assertNotNull(action.getObject().getLetter().getName());
        assertNotNull(action.getObject().getLetter().getContentType());
    }

    /**
     * Test the review method.
     */
    @Test
    public void testReview() {
        SupportLetterRequestAdministrationAction action = getAction();
        assertEquals(Action.SUCCESS, action.review());
        assertEquals(1, action.getActionMessages().size());
    }

    private SupportLetterRequestAdministrationAction getAction() {
        SupportLetterRequestServiceLocal mock = mock(SupportLetterRequestServiceLocal.class);
        stub(mock.reviewSupportLetterRequest(any(SupportLetterRequest.class))).toReturn(1L);
        LobHolderServiceLocal lobServiceMock = mock(LobHolderServiceLocal.class);
        return new SupportLetterRequestAdministrationAction(mock, lobServiceMock);
    }
}
