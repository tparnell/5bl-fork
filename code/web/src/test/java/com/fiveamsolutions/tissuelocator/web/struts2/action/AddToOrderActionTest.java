/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashSet;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 *
 */
public class AddToOrderActionTest extends AbstractTissueLocatorWebTest {

    /**
     * tests the add to cart method.
     */
    @Test
    public void testAddToCart() {
        AddToOrderAction action = new AddToOrderAction(getTestUserService(), getTestAppSettingService());
        action.setOrder(new Shipment());
        action.setSelection(new HashSet<Long>());
        action.getSelection().add(1L);
        String result = action.addToOrder();
        assertEquals(Action.SUCCESS, result);
        assertEquals(1, action.getOrder().getLineItems().size());
    }

    /**
     * Tests the addition of aggregate line items.
     */
    @Test
    public void testAddAggregateToCart() {
        AddToOrderAction action = new AddToOrderAction(getTestUserService(), getTestAppSettingService());
        Institution inst = new Institution();
        action.setOrder(new Shipment());
        AggregateSpecimenRequestLineItem li = new AggregateSpecimenRequestLineItem();
        li.setInstitution(inst);
        li.setCriteria("criteria");
        li.setQuantity(2);
        action.getOrder().getAggregateLineItems().add(li);
        action.setAggregateSelections(new AggregateSpecimenRequestLineItem[]{
                new AggregateSpecimenRequestLineItem(),
                new AggregateSpecimenRequestLineItem()
        });
        action.getAggregateSelections()[0].setInstitution(inst);
        action.getAggregateSelections()[0].setCriteria("criteria");
        action.getAggregateSelections()[0].setQuantity(1);
        action.getAggregateSelections()[1].setInstitution(inst);
        action.getAggregateSelections()[1].setCriteria("criteria2");
        action.getAggregateSelections()[1].setQuantity(1);

        String result = action.addAggregatesToOrder();
        assertEquals(Action.SUCCESS, result);
        assertEquals(2, action.getOrder().getAggregateLineItems().size());
        for (AggregateSpecimenRequestLineItem item : action.getOrder().getAggregateLineItems()) {
            assertNull(item.getVote());
            assertEquals(1, item.getQuantity());
        }

        getTestAppSettingService().setReviewProcess(ReviewProcess.LINE_ITEM_REVIEW);
        action.getOrder().getAggregateLineItems().clear();
        result = action.addAggregatesToOrder();
        assertEquals(Action.SUCCESS, result);
        assertEquals(2, action.getOrder().getAggregateLineItems().size());
        for (AggregateSpecimenRequestLineItem item : action.getOrder().getAggregateLineItems()) {
            assertNotNull(item.getVote());
            assertEquals(Vote.APPROVE, item.getVote().getVote());
            assertNotNull(item.getVote().getDate());
            assertNotNull(item.getVote().getUser());
            assertNotNull(item.getVote().getComment());
            assertNotNull(item.getVote().getInstitution());
        }
        getTestAppSettingService().setReviewProcess(ReviewProcess.FULL_CONSORTIUM_REVIEW);
    }

    /**
     * Test the prepare method.
     */
    @Test
    public void testPrepare() {
        getTestAppSettingService().setSpecimenGrouping("EXTERNAL_ID_ASSIGNER");
        getRequest().addParameter("otherParam", "1");
        getRequest().addParameter("aggregateSelections[1].institution", "1");
        getRequest().addParameter("aggregateSelections[3].institution", "1");
        getRequest().addParameter("aggregateSelections[0].institution", "1");
        getRequest().addParameter("aggregateSelections[2].institution", "1");
        AddToOrderAction action = new AddToOrderAction(getTestUserService(), getTestAppSettingService());
        action.setAggregateSelections(null);
        action.prepare();
        assertNotNull(action.getAggregateSelections());
        assertEquals(2 + 2, action.getAggregateSelections().length);
        for (int i = 0; i < action.getAggregateSelections().length; i++) {
            assertNotNull(action.getAggregateSelections()[i]);
        }
        getTestAppSettingService().setSpecimenGrouping(null);
    }
}
