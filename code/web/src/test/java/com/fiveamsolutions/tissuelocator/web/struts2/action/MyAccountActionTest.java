/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.junit.Test;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.opensymphony.xwork2.Action;

/**
 * @author smiller
 *
 */
public class MyAccountActionTest extends AbstractTissueLocatorWebTest {

    /**
     * test the prepare method.
     */
    @Test
    public void testPrepare() {
        MyAccountAction action = getMyAccountAction();
        action.prepare();
        assertEquals(UsernameHolder.getUser(), action.getObject().getUsername());
    }

    /**
     * test the save method.
     * @throws Exception on error
     */
    @Test
    public void testSave() throws Exception {
        MyAccountAction action = getMyAccountAction();
        action.prepare();
        assertEquals(Action.INPUT, action.save());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());

        HttpSession session = ServletActionContext.getRequest().getSession();
        TissueLocatorUser sessionUser = TissueLocatorSessionHelper.getLoggedInUser(session);
        sessionUser.setId(1L);
        action.getObject().setId(1L);
        action.getObject().setUsername("new username");
        assertEquals("logout", action.save());
    }

    private MyAccountAction getMyAccountAction() {
        return new MyAccountAction(getTestUserService(), getTestAppSettingService(), 
                getTestUiDynamicFieldCategoryService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
    }
}
