/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.struts2.ServletActionContext;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockServletContext;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.ReviewComment;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.test.AbstractSpecimenRequestReviewActionTest;
import com.fiveamsolutions.tissuelocator.web.test.FullConsortiumReviewServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author smiller
 *
 */
public class FullConsortiumReviewActionTest extends AbstractSpecimenRequestReviewActionTest {

    private static final String ATTRIBUTE_NAME = "_params";
    private static final int VOTING_PERIOD = 15;
    private static final int REVIEW_PERIOD = 10;

    /**
     * Tests the list method.
     */
    @Test
    public void testList() {
        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        request.addUserRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName());
        request.addUserRole(Role.INSTITUTIONAL_REVIEW_VOTER.getName());

        AbstractSpecimenRequestReviewAction action = getFullConsortiumReviewAction();
        action.setActionNeededOnly(true);
        assertEquals(Action.SUCCESS, action.list());
        assertEquals(0, action.getObjects().getList().size());
        assertTrue(action.isActionNeededOnly());
        Boolean actionNeededOnly = (Boolean) getSession().getAttribute(ATTRIBUTE_NAME);
        assertNotNull(actionNeededOnly);
        assertTrue(actionNeededOnly.booleanValue());

        action.setActionNeededOnly(false);
        assertEquals(Action.SUCCESS, action.list());
        assertEquals(0, action.getObjects().getList().size());
        assertTrue(action.isActionNeededOnly());
        actionNeededOnly = (Boolean) getSession().getAttribute(ATTRIBUTE_NAME);
        assertNotNull(actionNeededOnly);
        assertTrue(actionNeededOnly.booleanValue());

        action.setActionNeededOnly(false);
        assertEquals(Action.SUCCESS, action.filter());
        assertEquals(0, action.getObjects().getList().size());
        assertFalse(action.isActionNeededOnly());
        actionNeededOnly = (Boolean) getSession().getAttribute(ATTRIBUTE_NAME);
        assertNotNull(actionNeededOnly);
        assertFalse(actionNeededOnly.booleanValue());
    }

    /**
     * tests the prepare method.
     */
    @Test
    public void testPrepare() {
        FullConsortiumReviewAction action = getFullConsortiumReviewAction();
        action.setObject(getTestRequest());
        assertNull(action.getConsortiumReview());
        assertNull(action.getInstitutionalReview());
        assertNull(action.getComment().getUser());

        action.prepare();
        assertEquals(action.getComment().getUser().getUsername(), UsernameHolder.getUser());
        assertNull(action.getComment().getComment());
        assertEquals(getUserInstitution(), action.getConsortiumReview().getInstitution());
        assertEquals(getUserInstitution(), action.getInstitutionalReview().getInstitution());
        assertNotNull(action.getReviewers());
        assertFalse(action.getReviewers().isEmpty());
        assertEquals(1, action.getReviewers().size());
        assertTrue(action.getActionMessages().isEmpty());
        assertTrue(action.isDisplayPILegalField());

        action = getFullConsortiumReviewAction();
        action.setObject(getTestRequest());
        assertNull(action.getConsortiumReview());
        assertNull(action.getInstitutionalReview());
        assertNull(action.getComment().getUser());

        Institution userInstitution = new Institution();
        userInstitution.setName("not a match");
        setUserInstitution(userInstitution);
        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        request.addParameter("commentSaved", "true");

        action.prepare();
        assertEquals(action.getComment().getUser().getUsername(), UsernameHolder.getUser());
        assertNull(action.getComment().getComment());
        assertNull(action.getConsortiumReview());
        assertNull(action.getInstitutionalReview());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        assertTrue(action.isDisplayPILegalField());
        assertNull(action.getObjectId());
        assertNull(action.getObjectStatus());
    }

    /**
     * tests the voting process.
     */
    @Test
    public void testConsortiumReview() {
        AbstractSpecimenRequestReviewAction action = getFullConsortiumReviewAction();
        action.setConsortiumReview(new SpecimenRequestReviewVote());

        assertNull(action.getConsortiumReview().getUser());
        assertEquals(Action.INPUT, action.saveConsortiumReview());
        assertNotNull(action.getConsortiumReview().getUser());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * tests the voting process.
     */
    @Test
    public void testInstitutionalReview() {
        FullConsortiumReviewAction action = getFullConsortiumReviewAction();
        assertEquals(2, action.getInstiutionalReviewAllowableVotes().size());
        assertTrue(action.getInstiutionalReviewAllowableVotes().contains(Vote.APPROVE));
        assertTrue(action.getInstiutionalReviewAllowableVotes().contains(Vote.DENY));
        action.setInstitutionalReview(new SpecimenRequestReviewVote());

        assertNull(action.getInstitutionalReview().getUser());
        assertEquals(Action.INPUT, action.saveInstitutionalReview());
        assertNotNull(action.getInstitutionalReview().getUser());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * tests the commenting process.
     * @throws MessagingException on error.
     */
    @Test
    public void testComment() throws MessagingException {
        MockServletContext context = (MockServletContext) ServletActionContext.getServletContext();
        context.addInitParameter("votingPeriod", String.valueOf(VOTING_PERIOD));
        FullConsortiumReviewAction action = getFullConsortiumReviewAction();
        action.setComment(new ReviewComment());
        assertEquals(Action.INPUT, action.loadCommentForm());
        action.setObject(getTestRequest());
        action.prepare();
        assertEquals(action.getComment().getUser().getUsername(), UsernameHolder.getUser());
        assertNull(action.getComment().getComment());
        assertEquals(0, action.getObject().getComments().size());
        assertNotNull(action.getVotingEndDate());

        action.getComment().setComment("test");

        assertEquals(Action.SUCCESS, action.addComment());
        assertNotNull(action.getComment().getComment());
        assertEquals(1, action.getObject().getComments().size());
        FullConsortiumReviewServiceStub stub = (FullConsortiumReviewServiceStub)
            TissueLocatorRegistry.getServiceLocator().getFullConsortiumReviewService();
        assertEquals(stub.getObject(), action.getObject());
        assertEquals(VOTING_PERIOD, stub.getVotingPeriod());
    }

    /**
     * test the summary action.
     */
    @Test
    public void testSummary() {
        AbstractSpecimenRequestReviewAction action = getFullConsortiumReviewAction();
        assertEquals("summary", action.viewSummary());
    }

    /**
     * test the assign reviewer action.
     * @throws MessagingException on error
     */
    @Test
    public void testAssignReviewer() throws MessagingException {
        TissueLocatorUser currentUser = TissueLocatorSessionHelper.getLoggedInUser(getSession());
        MockServletContext context = (MockServletContext) ServletActionContext.getServletContext();
        context.addInitParameter("reviewPeriod", String.valueOf(REVIEW_PERIOD));
        context.addInitParameter("votingPeriod", String.valueOf(VOTING_PERIOD));
        FullConsortiumReviewAction action = getFullConsortiumReviewAction();
        action.setConsortiumReview(new SpecimenRequestReviewVote());
        action.getConsortiumReview().setUser(currentUser);
        assertEquals(Action.INPUT, action.assignScientificReviewer());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        action.clearErrorsAndMessages();
        FullConsortiumReviewServiceStub stub = (FullConsortiumReviewServiceStub)
            TissueLocatorRegistry.getServiceLocator().getFullConsortiumReviewService();
        assertEquals(stub.getObject(), action.getObject());
        assertNotNull(stub.getAssignee());
        assertEquals(currentUser, stub.getAssignee());
        assertEquals(stub.getAssignee(), action.getConsortiumReview().getUser());
        assertEquals(VOTING_PERIOD, stub.getVotingPeriod());
        assertEquals(REVIEW_PERIOD, stub.getReviewPeriod());
    }

    /**
     * tests the finalizeVote method.
     * @throws MessagingException on error.
     */
    @Test
    public void testFinalizeVote() throws MessagingException {
        FullConsortiumReviewAction action = getFullConsortiumReviewAction();
        action.setComment(new ReviewComment());
        action.setObject(getTestRequest());
        action.prepare();
        action.getObject().setRequestor(action.getComment().getUser());
        assertEquals(Action.SUCCESS, action.finalizeVote());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        FullConsortiumReviewServiceStub stub = (FullConsortiumReviewServiceStub)
            TissueLocatorRegistry.getServiceLocator().getFullConsortiumReviewService();
        assertEquals(stub.getObject(), action.getObject());
        assertTrue(stub.isCreateShipments());
    }

    /**
     * test the delegate method.
     */
    @Test
    public void testDelegate() {
        FullConsortiumReviewAction action = getFullConsortiumReviewAction();
        action.setReviewProcess(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        String result = action.delegate();
        assertNotNull(result);
        assertEquals(ReviewProcess.FULL_CONSORTIUM_REVIEW.getNamespace(), result);
    }

    private FullConsortiumReviewAction getFullConsortiumReviewAction() {
        return new FullConsortiumReviewAction(getTestUserService(), getTestAppSettingService(), 
                getTestUiDynamicFieldCategoryService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
    }
}
