/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceLocal;

/**
 * @author ddasgupta
 *
 */
public class MaterialTransferAgreementServiceStub extends GenericServiceStub<MaterialTransferAgreement> implements
        MaterialTransferAgreementServiceLocal {

    /**
     * MTA version.
     */
    public static final Date MTA_VERSION = new Date();
    
    /**
     * {@inheritDoc}
     */
    public Long saveMaterialTransferAgreement(MaterialTransferAgreement mta) {
        return super.savePersistentObject(mta);
    }

    /**
     * {@inheritDoc}
     */
    public List<MaterialTransferAgreement> getAll() {
        List<MaterialTransferAgreement> mtas = new ArrayList<MaterialTransferAgreement>();
        mtas.add(getCurrentMta());
        return mtas;
    }

    /**
     * {@inheritDoc}
     */
    public MaterialTransferAgreement getCurrentMta() {
        MaterialTransferAgreement mta = new MaterialTransferAgreement();
        mta.setId(1L);
        mta.setVersion(MTA_VERSION);
        mta.setUploadTime(new Date());
        mta.setDocument(createDocument());
        return mta;
    }

    private TissueLocatorFile createDocument() {
        return new TissueLocatorFile(new byte[] {1}, "mta.pdf", "application/pdf");
    }
}
