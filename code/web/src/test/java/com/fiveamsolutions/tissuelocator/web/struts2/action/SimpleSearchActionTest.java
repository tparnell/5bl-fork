/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.struts2.ServletActionContext;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;

/**
 * @author zmelnick
 *
 */
public class SimpleSearchActionTest extends AbstractTissueLocatorWebTest {

    /**
     * Tests the prepare method.
     */
    @Test
    public void testPrepare() {
        SimpleSearchAction ssa = new SimpleSearchAction(getTestSearchFieldConfigService());
        SearchFieldConfigServiceLocal svc = getTestSearchFieldConfigService();
        assertTrue(ssa.getSimpleSearchConfigs().size() < svc.getSearchFieldConfigs(SearchSetType.WIDGET).size());

        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();

        ssa = new SimpleSearchAction(getTestSearchFieldConfigService());
        ssa.prepare();
        assertTrue(ssa.getSimpleSearchConfigs().size() < svc.getSearchFieldConfigs(SearchSetType.WIDGET).size());

        for (AbstractSearchFieldConfig config : svc.getSearchFieldConfigs(SearchSetType.WIDGET)) {
            if (config.getRequiredRole() != null) {
                request.addUserRole(config.getRequiredRole().getName());
            }
        }
        ssa = new SimpleSearchAction(getTestSearchFieldConfigService());
        ssa.prepare();
        assertEquals(ssa.getSimpleSearchConfigs().size(), svc.getSearchFieldConfigs(SearchSetType.WIDGET).size());
    }
    
    /**
     * Test unsupported instantiation.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testConstructorException() {
        new SimpleSearchAction();
    }
}
