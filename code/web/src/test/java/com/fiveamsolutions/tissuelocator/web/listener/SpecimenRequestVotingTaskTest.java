/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.listener;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import javax.servlet.ServletContextEvent;

import org.junit.Test;
import org.springframework.mock.web.MockServletContext;

import com.fiveamsolutions.tissuelocator.service.reminder.ScheduledReminderServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.SpecimenRequestProcessingServiceStub;
import com.google.inject.Provider;

/**
 * @author ddasgupta
 *
 */
public class SpecimenRequestVotingTaskTest extends AbstractTissueLocatorWebTest {

    private static final int REVIEW_PERIOD = 10;
    private static final int VOTING_PERIOD = 15;
    private static final int MIN_PERCENT_AVAILABLE_VOTES = 50;
    private static final int MIN_PERCENT_WINNING_VOTES = 40;
    private static final int REVIEWER_ASSIGNMENT_PERIOD = 1;
    private static final int SHIPMENT_RECEIPT_GRACE_PERIOD = 1;
    private static final int LINE_ITEM_REVIEW_GRACE_PERIOD = 2;
    private static final int LETTER_REQUEST_REVIEW_GRACE_PERIOD = 3;
    private static final int QUESTION_RESPONSE_GRACE_PERIOD = 4;

    /**
     * test the context listener.
     */
    @Test
    public void testListener() {
        // there's not much to be tested here because the context listener does not provide any
        // hooks into any objects that it modifies. This test just verifies that when the context
        // listener is given valid init params, it runs without error.
        TissueLocatorContextListener listener = new TissueLocatorContextListener() {

            /**
             * {@inheritDoc}
             */
            @Override
            protected void closeHibernateSession() {
                // do nothing
            }

            /**
             * {@inheritDoc}
             */
            @Override
            protected void initializeHibernateSession() {
                // do nothing
            }

        };

        listener.setAppSettingServiceProvider(new Provider<ApplicationSettingServiceLocal>() {
            @Override
            public ApplicationSettingServiceLocal get() {
                return getTestAppSettingService();
            }
        });

        listener.contextInitialized(getEvent("0001"));
        listener.contextInitialized(getEvent("2359"));
        getTestAppSettingService().setVotingTaskEnabled(false);
        listener.contextInitialized(getEvent("0000"));
        listener.contextDestroyed(getEvent("1200"));
    }

    private ServletContextEvent getEvent(String startTime) {
        MockServletContext context = new MockServletContext();
        context.addInitParameter("votingThreadStartTime", startTime);
        context.addInitParameter("votingPeriod", "15");
        context.addInitParameter("reviewPeriod", "10");
        context.addInitParameter("minPercentAvailableVotes", "50");
        context.addInitParameter("minPercentWinningVotes", "40");
        context.addInitParameter("reviewerAssignmentPeriod", "1");
        return new ServletContextEvent(context);
    }

    /**
     * test the voting task.
     */
    @Test
    public void testVotingTask() {
        SpecimenRequestVotingTask task = new SpecimenRequestVotingTask(REVIEW_PERIOD, VOTING_PERIOD,
                MIN_PERCENT_AVAILABLE_VOTES, MIN_PERCENT_WINNING_VOTES, REVIEWER_ASSIGNMENT_PERIOD);
        task.run();
        SpecimenRequestProcessingServiceStub stub = (SpecimenRequestProcessingServiceStub) TissueLocatorRegistry
                .getServiceLocator().getSpecimenRequestProcessingService();
        assertEquals(REVIEW_PERIOD, stub.getConfig().getReviewPeriod());
        assertEquals(VOTING_PERIOD, stub.getConfig().getVotingPeriod());
        assertEquals(MIN_PERCENT_AVAILABLE_VOTES, stub.getConfig().getMinPercentAvailableVotes());
        assertEquals(MIN_PERCENT_WINNING_VOTES, stub.getConfig().getMinPercentWinningVotes());
        assertEquals(REVIEWER_ASSIGNMENT_PERIOD, stub.getConfig().getReviewerAssignmentPeriod());
    }

    /**
     * Test schedule reminder task.
     */
    @Test
    public void testReminderTask() {
        ScheduledRemindersTask task = new ScheduledRemindersTask(SHIPMENT_RECEIPT_GRACE_PERIOD,
                LINE_ITEM_REVIEW_GRACE_PERIOD, LETTER_REQUEST_REVIEW_GRACE_PERIOD, QUESTION_RESPONSE_GRACE_PERIOD);
        task.setScheduledReminderServiceProvider(new Provider<ScheduledReminderServiceLocal>() {
            @Override
            public ScheduledReminderServiceLocal get() {
                return mock(ScheduledReminderServiceLocal.class);
            }
        });
        task.run();
    }
}
