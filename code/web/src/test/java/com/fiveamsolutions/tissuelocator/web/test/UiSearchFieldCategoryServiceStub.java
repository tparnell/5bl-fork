/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.List;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.service.config.category.UiSearchFieldCategoryServiceLocal;

/**
 * Search field user interface category service stub.
 *
 * @author jstephens
 */
public class UiSearchFieldCategoryServiceStub extends GenericServiceStub<UiSearchFieldCategory>
    implements UiSearchFieldCategoryServiceLocal {

    // CHECKSTYLE:OFF

    /**
     * Returns the search field user interface categories associated with an action.
     *
     * @param entityClass the entity to which the search field categories belong
     * @param searchSetType the search set type
     * @return list of search field user interface categories
     */
    @Override
    public List<UiSearchFieldCategory> getUiSearchFieldCategories(
            Class<? extends PersistentObject> entityClass, SearchSetType searchSetType) {
        final List<UiSearchFieldCategory> uiSearchFieldCategories = new ArrayList<UiSearchFieldCategory>();
        for (final UiSearchFieldCategory uiCategory : createUiSearchFieldCategories()) {
            if (searchSetType.equals(SearchSetType.valueOf(uiCategory.getSearchSetType()))) {
                uiSearchFieldCategories.add(uiCategory);
            }
        }

        return uiSearchFieldCategories;
    }

    private List<UiSearchFieldCategory> createUiSearchFieldCategories() {
        final List<UiSearchFieldCategory> categories = new ArrayList<UiSearchFieldCategory>();

        final UiSearchFieldCategory advancedCategory = new UiSearchFieldCategory();
        advancedCategory.setCategoryName("specimen.search.biospecimen.title");
        advancedCategory.setViewable(Boolean.TRUE);
        advancedCategory.setEntityClassName(Specimen.class.getName());
        advancedCategory.setSearchSetType(SearchSetType.ADVANCED.name());
        advancedCategory.setUiSearchCategoryFields(createUiCategoryFields(4));
        advancedCategory.getUiSearchCategoryFields().get(0).getFieldConfig().setSearchFieldName("object.specimenType");
        advancedCategory.getUiSearchCategoryFields().get(1).getFieldConfig().setSearchFieldName("collectionYear");
        advancedCategory.getUiSearchCategoryFields().get(2).getFieldConfig().setSearchFieldName("priceNegotiable");
        advancedCategory.getUiSearchCategoryFields().get(3).getFieldConfig().setSearchFieldName("customProperties['test']");
        categories.add(advancedCategory);

        final UiSearchFieldCategory miscellaneousCategory = new UiSearchFieldCategory();
        miscellaneousCategory.setCategoryName("Miscellaneous Category");
        miscellaneousCategory.setViewable(Boolean.TRUE);
        miscellaneousCategory.setEntityClassName(Specimen.class.getName());
        miscellaneousCategory.setSearchSetType(SearchSetType.ADVANCED.name());
        miscellaneousCategory.setUiSearchCategoryFields(createUiCategoryFields(1));
        miscellaneousCategory.getUiSearchCategoryFields().get(0).getFieldConfig().setSearchFieldName("miscellaneousCategory");
        categories.add(miscellaneousCategory);

        final UiSearchFieldCategory simpleCategory = new UiSearchFieldCategory();
        simpleCategory.setCategoryName("specimen.search.simple.criteria.title");
        simpleCategory.setViewable(Boolean.TRUE);
        simpleCategory.setEntityClassName(Specimen.class.getName());
        simpleCategory.setSearchSetType(SearchSetType.SIMPLE.name());
        simpleCategory.setUiSearchCategoryFields(createUiCategoryFields(1));
        simpleCategory.getUiSearchCategoryFields().get(0).getFieldConfig().setSearchFieldName("object.externalId");
        categories.add(simpleCategory);

        final UiSearchFieldCategory dummyCategory = new UiSearchFieldCategory();
        dummyCategory.setCategoryName("Dummy Category");
        dummyCategory.setViewable(Boolean.TRUE);
        dummyCategory.setEntityClassName(Specimen.class.getName());
        dummyCategory.setSearchSetType(SearchSetType.ADVANCED.name());
        dummyCategory.setUiSearchCategoryFields(createUiCategoryFields(1));
        dummyCategory.getUiSearchCategoryFields().get(0).getFieldConfig().setSearchFieldName("dummyCategory");
        categories.add(dummyCategory);

        return categories;
    }

    private List<UiSearchCategoryField> createUiCategoryFields(final int numberOfFields) {
        final List<UiSearchCategoryField> categoryFields = new ArrayList<UiSearchCategoryField>();

        for (int i = 1; i <= numberOfFields; i++) {
            final UiSearchCategoryField cf = new UiSearchCategoryField();
            cf.setFieldConfig(new GenericFieldConfig());
            cf.getFieldConfig().setId(new Long(i));
            cf.getFieldConfig().setSearchFieldName("fieldName" + i);
            categoryFields.add(cf);
        }

        return categoryFields;
    }

    // CHECKSTYLE:ON
}
