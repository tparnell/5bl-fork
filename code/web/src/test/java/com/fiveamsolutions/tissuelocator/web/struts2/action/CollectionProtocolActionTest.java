/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.CollectionProtocolServiceStub;
import com.fiveamsolutions.tissuelocator.web.util.SelectOption;
import com.opensymphony.xwork2.Action;

/**
 * @author smiller
 *
 */
public class CollectionProtocolActionTest extends AbstractTissueLocatorWebTest {

    /**
     * Tests the save method.
     */
    @Test
    public void testSave() {
        CollectionProtocolAction action = new CollectionProtocolAction();
        CollectionProtocol cp = new CollectionProtocol();
        cp.setName("test");
        action.setObject(cp);
        action.save();
        CollectionProtocolServiceStub stub = (CollectionProtocolServiceStub)
            TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();
        assertEquals(action.getObject(), stub.getObject());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * Tests the list method.
     */
    @Test
    public void testlist() {
        CollectionProtocolAction action = new CollectionProtocolAction();
        action.setInstitutionSelection(null);
        assertNull(action.getInstitutionSelection());
        assertEquals(Action.SUCCESS, action.list());
        assertEquals(0, action.getObjects().getList().size());
    }

    /**
     * Tests the input method.
     */
    @Test
    public void testInput() {
        CollectionProtocolAction action = new CollectionProtocolAction();
        action.setObject(null);
        action.input();
        assertEquals(null, action.getInstitutionSelection());

        action.setObject(new CollectionProtocol());
        action.input();
        assertEquals(null, action.getInstitutionSelection());

        action.getObject().setInstitution(new Institution());
        action.input();
        assertEquals(null, action.getInstitutionSelection());

        action.getObject().getInstitution().setName("test");
        action.input();
        assertEquals("test", action.getInstitutionSelection());
    }

    /**
     * Tests the list method.
     */
    @Test
    public void testInstitutions() {
        CollectionProtocolAction action = new CollectionProtocolAction();
        assertEquals(Action.SUCCESS, action.autocompleteInstitution());
        action.setTerm(null);
        assertEquals(0, action.getInstitutionsMap().length);
        action.setTerm("test");
        assertEquals(1, action.getInstitutionsMap().length);
        SelectOption result = action.getInstitutionsMap()[0];
        assertNotNull(result.getId());
        assertNotNull(result.getLabel());
        assertNotNull(result.getValue());
    }
}
