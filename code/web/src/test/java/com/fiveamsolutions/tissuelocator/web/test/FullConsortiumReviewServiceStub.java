/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.test;

import javax.mail.MessagingException;

import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.FullConsortiumReviewServiceLocal;

/**
 * @author ddasgupta
 *
 */
public class FullConsortiumReviewServiceStub extends SpecimenRequestServiceStub implements
        FullConsortiumReviewServiceLocal {

    private TissueLocatorUser assignee;

    /**
     * {@inheritDoc}
     */
    public void assignRequest(SpecimenRequest request, TissueLocatorUser assigneeArg, int votingPeriod,
            int reviewPeriod) throws MessagingException {
        this.assignee = assigneeArg;
        setVotingPeriod(votingPeriod);
        setReviewPeriod(reviewPeriod);
        savePersistentObject(request);
    }

    /**
     * {@inheritDoc}
     */
    public void addComment(SpecimenRequest request, int votingPeriod) throws MessagingException {
        setVotingPeriod(votingPeriod);
        savePersistentObject(request);
    }

    /**
     * @return the assignee
     */
    public TissueLocatorUser getAssignee() {
        return this.assignee;
    }
}
