/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action.mta;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Date;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.MaterialTransferAgreementServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 *
 */
public class MaterialTransferAgreementActionTest extends AbstractTissueLocatorWebTest {

    /**
     * tests the prepare method.
     * @throws Exception on error
     */
    @Test
    public void testPrepare() throws Exception {
        MaterialTransferAgreementAction action = new MaterialTransferAgreementAction();
        action.prepare();
        assertNotNull(action.getMta());
        assertNull(action.getMta().getDocument());
        assertNotNull(action.getMtas());
        assertEquals(1, action.getMtas().size());

        action.setDocument(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setDocumentFileName("log4j.properties");
        action.setDocumentContentType("text/plain");
        action.prepare();
        assertNotNull(action.getMta().getDocument());
        assertTrue(action.getMta().getDocument().getLob().getData().length > 0);
        assertNotNull(action.getMta().getDocument().getName());
        assertNotNull(action.getMta().getDocument().getContentType());
        assertNotNull(action.getMtas());
        assertEquals(1, action.getMtas().size());
    }

    /**
     * tests the save method.
     * @throws Exception on error
     */
    @Test
    public void testSave() throws Exception {
        MaterialTransferAgreementAction action = new MaterialTransferAgreementAction();
        assertNotNull(action.getMta().getUploadTime());
        MaterialTransferAgreement mta = new MaterialTransferAgreement();
        mta.setVersion(new Date());
        action.setMta(mta);
        String result = action.save();
        assertEquals(Action.SUCCESS, result);
        MaterialTransferAgreementServiceStub stub = (MaterialTransferAgreementServiceStub)
            TissueLocatorRegistry.getServiceLocator().getMaterialTransferAgreementService();
        assertEquals(mta, stub.getObject());
        assertNotSame(action.getMta(), stub.getObject());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        assertNull(action.getMta().getVersion());
        assertNotNull(action.getMtas());
        assertEquals(1, action.getMtas().size());
    }
}
