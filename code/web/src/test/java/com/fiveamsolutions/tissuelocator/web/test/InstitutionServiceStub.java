/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;

/**
 * @author smiller
 *
 */
public class InstitutionServiceStub extends GenericServiceStub<Institution>
    implements InstitutionServiceLocal {

    /**
     * Invalid institution name for getInstitutionByName.
     */
    public static final String INVALID_NAME = "invalid";
    /**
     * Type id for getInstitutionByName.
     */
    public static final Long TYPE_ID = 1234L;
    private int searchResultsSize = 1;
    private List<Institution> consortiumMembers = new ArrayList<Institution>();

    /**
     * {@inheritDoc}
     */
    public List<Institution> getReviewers(int numberReviewers) {
        return new ArrayList<Institution>();
    }

    /**
     * {@inheritDoc}
     */
    public List<Institution> getConsortiumMembers() {
        return consortiumMembers;
    }

    /**
     * Set the consortium members to be returned.
     * @param consortiumMembers the consortium members to be returned.
     */
    public void setConsortiumMembers(List<Institution> consortiumMembers) {
        this.consortiumMembers = consortiumMembers;
    }

    /**
     * {@inheritDoc}
     */
    public Institution getInstitution(Long institutionId) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Institution getInstitutionByName(String name) {
        Institution inst = null;
        if (!INVALID_NAME.equals(name)) {
            inst = new Institution();
            inst.setName(name);
            inst.setType(new InstitutionType());
            inst.getType().setId(TYPE_ID);
        }
        return inst;
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, Map<Vote, Long>> getScientificReviewVoteCounts(Date startDate, Date endDate) {
        return new HashMap<String, Map<Vote, Long>>();
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, Map<Vote, Long>> getInstitutionalReviewVoteCounts(Date startDate, Date endDate) {
        return new HashMap<String, Map<Vote, Long>>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Institution> search(SearchCriteria<Institution> criteria, PageSortParams<Institution> params) {
        TissueLocatorAnnotatedBeanSearchCriteria<Institution> crit =
            (TissueLocatorAnnotatedBeanSearchCriteria<Institution>) criteria;
        List<Institution> results = new ArrayList<Institution>();
        if (StringUtils.isNotBlank(crit.getCriteria().getName())) {
            for (int i = 0; i < searchResultsSize; i++) {
                crit.getCriteria().setId(1L);
                results.add(crit.getCriteria());
            }
        }
        return results;
    }

    /**
     * @param searchResultsSize the searchResultsSize to set
     */
    public void setSearchResultsSize(int searchResultsSize) {
        this.searchResultsSize = searchResultsSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long saveNewInstitution(Institution institution) {
        return savePersistentObject(institution);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Long, List<String>> getScientificReviewRejectionComments(
            Date startDate, Date endDate, String institutionName) {
        return new HashMap<Long, List<String>>();
    }

}
