/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.support;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

import java.io.File;
import java.net.URISyntaxException;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.LobHolder;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.SupportLetterRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 *
 */
public class SupportLetterRequestActionTest extends AbstractTissueLocatorWebTest {

    /**
     * Tests preparing the file properties.
     * @throws URISyntaxException on error
     */
    @Test
    public void testPrepareFiles() throws URISyntaxException {
        SupportLetterRequestAction action = getAction();
        SupportLetterRequest oldRequest = action.getObject();
        action.prepare();
        assertEquals(oldRequest, action.getObject());
        assertNull(action.getObject().getResume());
        assertNull(action.getObject().getDraftProtocol());
        assertNull(action.getObject().getDraftLetter());

        action.setResume(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setResumeFileName("log4j.properties");
        action.setResumeContentType("text/plain");
        action.setDraftProtocol(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setDraftProtocolFileName("log4j.properties");
        action.setDraftProtocolContentType("text/plain");
        action.setDraftLetter(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setDraftLetterFileName("log4j.properties");
        action.setDraftLetterContentType("text/plain");
        action.prepare();
        assertEquals(oldRequest, action.getObject());
        assertNotNull(action.getObject().getResume());
        assertTrue(action.getObject().getResume().getLob().getData().length > 0);
        assertNotNull(action.getObject().getResume().getName());
        assertNotNull(action.getObject().getResume().getContentType());
        assertNotNull(action.getObject().getDraftProtocol());
        assertTrue(action.getObject().getDraftProtocol().getLob().getData().length > 0);
        assertNotNull(action.getObject().getDraftProtocol().getName());
        assertNotNull(action.getObject().getDraftProtocol().getContentType());
        assertNotNull(action.getObject().getDraftLetter());
        assertTrue(action.getObject().getDraftLetter().getLob().getData().length > 0);
        assertNotNull(action.getObject().getDraftLetter().getName());
        assertNotNull(action.getObject().getDraftLetter().getContentType());
    }

    /**
     * Tests preparing the resume properties.
     * @throws URISyntaxException on error
     */
    @Test
    public void testPrepareResume() throws URISyntaxException {
        TissueLocatorUser u = TissueLocatorSessionHelper.getLoggedInUser(getSession());
        u.setResume(new TissueLocatorFile(new byte[] {1}, "resume.txt", "text/plain"));
        SupportLetterRequestAction action = getAction();
        assertNotNull(action.getObject().getResume());
        assertNotNull(action.getObject().getResume().getLob());
        assertNull(action.getObject().getResume().getLob().getData());
        TissueLocatorFile oldFile = action.getObject().getResume();
        action.prepare();
        assertEquals(oldFile, action.getObject().getResume());
        assertTrue(action.isCopyinvestigator());

        action.getObject().getResume().setLob(new LobHolderProxy());
        oldFile = action.getObject().getResume();
        action.setCopyinvestigator(false);
        action.prepare();
        assertEquals(oldFile, action.getObject().getResume());
        assertFalse(action.isCopyinvestigator());

        action.getObject().getResume().setLob(new LobHolder());
        action.getObject().getResume().getLob().setId(1L);
        oldFile = action.getObject().getResume();
        action.prepare();
        assertNotSame(oldFile, action.getObject().getResume());
        assertNotNull(action.getObject().getResume().getLob().getData());
    }

    /**
     * Hibernate LobHolderProxy.
     * @author ddasgupta
     */
    public static class LobHolderProxy extends LobHolder {
        private static final long serialVersionUID = 1L;

        @Override
        public Long getId() {
            return 1L;
        }
    }

    /**
     * tests preparing the funding source properties.
     */
    @Test
    public void testPrepareFundingSource() {
        SupportLetterRequestAction action = getAction();
        SupportLetterRequest oldRequest = action.getObject();
        action.prepare();
        assertEquals(oldRequest, action.getObject());
        assertNotNull(action.getFundingSources());
        assertEquals(1, action.getFundingSources().size());
        assertNotNull(action.getFundingSources().get(0));
        assertEquals(1, action.getFundingSources().get(0).getId().intValue());
        action.getObject().setFundingSources(action.getFundingSources());
        assertNotNull(action.getFundingSourceIds());
        assertEquals(1, action.getFundingSourceIds().size());
        assertNotNull(action.getFundingSourceIds().get(0));
        assertEquals(1, action.getFundingSourceIds().get(0).intValue());
    }

    /**
     * tests preparing the investigator organization properties.
     */
    @Test
    public void testPrepareOrganization() {
        SupportLetterRequestAction action = getAction();
        SupportLetterRequest oldRequest = action.getObject();
        oldRequest.getInvestigator().setOrganization(null);
        action.prepare();
        assertEquals(oldRequest, action.getObject());
        assertNull(action.getInvestigatorOrganizationName());

        oldRequest = action.getObject();
        Institution i = new Institution();
        i.setName("test");
        oldRequest.getInvestigator().setOrganization(i);
        action.prepare();
        assertEquals(oldRequest, action.getObject());
        assertEquals("test", action.getInvestigatorOrganizationName());
    }

    /**
     * Test the review method.
     */
    @Test
    public void testSave() {
        SupportLetterRequestAction action = getAction();
        assertEquals(Action.SUCCESS, action.save());
        assertEquals(1, action.getActionMessages().size());
        assertEquals("view", action.view());
        assertEquals("help", action.help());
    }

    private SupportLetterRequestAction getAction() {
        SupportLetterRequestServiceLocal mock = mock(SupportLetterRequestServiceLocal.class);
        stub(mock.savePersistentObject(any(SupportLetterRequest.class))).toReturn(1L);
        LobHolderServiceLocal lobServiceMock = mock(LobHolderServiceLocal.class);
        LobHolder lob = new LobHolder(new byte[] {1});
        lob.setId(1L);
        stub(lobServiceMock.getLob(anyLong())).toReturn(lob);
        return new SupportLetterRequestAction(mock, lobServiceMock);
    }
}
