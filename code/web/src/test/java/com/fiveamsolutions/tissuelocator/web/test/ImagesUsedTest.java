/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * test whether images are used.
 * @author vsemenov
 */
public class ImagesUsedTest {
    private static final Logger LOG = Logger.getLogger(ImagesUsedTest.class);
    private static FileGrep fgrep = new FileGrep();

    /**
     * since an image might be found in the clients css/web files or the central css/web files we keep track of both the
     * folder with the images and the folders where those images might be found.
     */
    private static Map<String, List<String>> imageFolderToWebFolderMap;
    private static final List<String> IGNORE_PATTERNS = new ArrayList<String>();
    static {
        IGNORE_PATTERNS.add(".*process_steps_images.*");
    }

    private static String getSourceDir() throws IOException {
        Properties properties = new Properties();
        properties.load(ImagesUsedTest.class.getResourceAsStream("/unusedimagetest.properties"));

        String sourceDir = properties.getProperty("project.basedir");
        if (sourceDir == null) {
            sourceDir = "./web";
        }
        return sourceDir;
    }

    /**
     * set up directories.
     * @throws IOException on error
     */
    @BeforeClass
    public static void setUpDirs() throws IOException {
        String sourceDir = getSourceDir();
        imageFolderToWebFolderMap = new HashMap<String, List<String>>();

        String mainBase = (sourceDir + "/src/main/webapp/").replaceAll("/", File.separator);

        String clientBase = (sourceDir + "/target/client-jar/webapp/").replaceAll("/", File.separator);
        String clientImagesDir = clientBase + "images" + File.separator;
        imageFolderToWebFolderMap.put(clientImagesDir, new ArrayList<String>());
        imageFolderToWebFolderMap.get(clientImagesDir).add(mainBase);
        imageFolderToWebFolderMap.get(clientImagesDir).add(clientBase);
    }

    /**
     * test for unused images.
     * @throws Exception on error
     */
    @Test
    public void testUnusedImages() throws Exception {
        List<File> unusedImages = new ArrayList<File>();
        for (String imageDir : imageFolderToWebFolderMap.keySet()) {
            unusedImages.addAll(imagesAreUsed(imageDir, imageFolderToWebFolderMap.get(imageDir)));
        }

        for (File unusedImage : unusedImages) {
            LOG.error(unusedImage.getAbsolutePath() + " is not used");
        }
        Assert.assertEquals(0, unusedImages.size());
    }

    @SuppressWarnings("unchecked")
    private Collection<File> imagesAreUsed(String imagesPath, List<String> webFilePaths) throws Exception {
        List<File> allWebFiles = new ArrayList<File>();
        for (String webFileDir : webFilePaths) {
            allWebFiles.addAll(FileUtils.listFiles(new File(webFileDir), new String[] {"jsp", "tag", "jspx", "js",
                    "css", "ftl" }, true));
        }

        File baseImgDir = new File(imagesPath);
        Collection<File> imgFiles = FileUtils.listFiles(baseImgDir, new String[] {"gif", "jpg", "ico", "png" }, true);
        List<File> tmpImgFiles = new ArrayList<File>(imgFiles);
        for (File imgFile : tmpImgFiles) {
            for (String ignorePattern : IGNORE_PATTERNS) {
                if (imgFile.getAbsolutePath().matches(ignorePattern)) {
                    imgFiles.remove(imgFile);
                    break;
                }
            }
        }
        for (File file : allWebFiles) {
            List<File> tmpImgFiles2 = new ArrayList<File>(imgFiles);
            for (File imgFile : tmpImgFiles2) {
                if (fgrep.contains(file, fgrep.escape(imgFile.getName()))) {
                    imgFiles.remove(imgFile);
                }
            }
        }
        return imgFiles;
    }

    /**
     * test that images in the main code are referenced from jsp/css files in the main code or in every client.
     * @throws Exception on error
     */
    @Test
    public void testMainImages() throws Exception {
        testMainImagesForClient("/target/client-jar/webapp/");
    }

    private void testMainImagesForClient(String clientDir) throws Exception {
        String sourceDir = getSourceDir();

        String mainBase = (sourceDir + "/src/main/webapp/").replaceAll("/", File.separator);
        String mainImagesDir = mainBase + "images" + File.separator;

        String clientBase = (sourceDir + clientDir).replaceAll("/", File.separator);
        List<String> folders = new ArrayList<String>();
        folders.add(mainBase);
        folders.add(clientBase);

        Collection<File> unusedImages = imagesAreUsed(mainImagesDir, folders);
        for (File unusedImage : unusedImages) {
            LOG.error(unusedImage.getAbsolutePath() + " is not used");
        }
        Assert.assertEquals(0, unusedImages.size());
    }
}
