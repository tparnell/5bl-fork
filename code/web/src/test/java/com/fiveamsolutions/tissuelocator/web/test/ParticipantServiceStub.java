/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;

/**
 * @author smiller
 *
 */
public class ParticipantServiceStub extends GenericServiceStub<Participant> implements
    ParticipantServiceLocal {

    private int searchResultsSize = 1;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<Participant> search(SearchCriteria<Participant> criteria, PageSortParams<Participant> params) {
        TissueLocatorAnnotatedBeanSearchCriteria<Participant> crit =
            (TissueLocatorAnnotatedBeanSearchCriteria<Participant>) criteria;
        List<Participant> results = new ArrayList<Participant>();
        if (StringUtils.isNotBlank(crit.getCriteria().getExternalId())) {
            for (int i = 0; i < searchResultsSize; i++) {
                crit.getCriteria().setId(1L);
                results.add(crit.getCriteria());
            }           
        }
        return results;
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, Participant> getParticipants(Long institutionId, Collection<String> extIds) {
        return null;
    }

    /**
     * @param searchResultsSize the searchResultsSize to set
     */
    public void setSearchResultsSize(int searchResultsSize) {
        this.searchResultsSize = searchResultsSize;
    }
    
}