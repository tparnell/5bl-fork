/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fiveamsolutions.dynamicextensions.ControlledVocabularyDynamicFieldDefinition;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.AutocompleteConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.CheckboxAutocompleteCompositeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.CheckboxConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.DataType;
import com.fiveamsolutions.tissuelocator.data.config.search.MultiSelectValueType;
import com.fiveamsolutions.tissuelocator.data.config.search.RangeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.RangeSelectCompositeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectDynamicExtensionConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SimpleSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.TextConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.TextSelectCompositeConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;

/**
 * @author gvaughn
 *
 */
public class SearchFieldConfigServiceStub extends GenericServiceStub<AbstractSearchFieldConfig> implements
        SearchFieldConfigServiceLocal {

    /**
     * The number of composite configurations returned for advanced searches.
     */
    public static final int COMPOSITE_CONFIG_COUNT = 3;
    /**
     * {@inheritDoc}
     */
    public Collection<AbstractSearchFieldConfig> getSearchFieldConfigs(SearchSetType searchSetType) {
        List<AbstractSearchFieldConfig> configs = createConfigs();
        if (searchSetType.equals(SearchSetType.ADVANCED)) {
            return configs;
        }
        List<AbstractSearchFieldConfig> filteredConfigs = new ArrayList<AbstractSearchFieldConfig>();
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            if (searchFieldConfig.getSearchSetType().equals(searchSetType)) {
                filteredConfigs.add(searchFieldConfig);
            }
        }
        return filteredConfigs;
    }
    
    private List<AbstractSearchFieldConfig> createConfigs() {
        List<AbstractSearchFieldConfig> configList = new ArrayList<AbstractSearchFieldConfig>();
        Long index = 0L;
        TextConfig text = new TextConfig();
        populateSimpleConfig(text, index++, "object.externalId", "External ID", true);
        ApplicationRole role = new ApplicationRole();
        role.setName(Role.EXTERNAL_ID_REVIEWER.name());
        text.getFieldConfig().setRequiredRole(role);
        text.setSearchSetType(SearchSetType.SIMPLE);
        configList.add(text);

        SelectConfig select = new SelectConfig();
        populateSimpleConfig(select, index++, "externalIdAssigner.id", "Institution", false);
        select.setListType(DataType.INSTITUTION);
        select.setListClass(Institution.class.getName());
        select.setMultiSelect(true);
        select.setMultiSelectValueType(MultiSelectValueType.LONG);
        select.setObjectProperty(true);
        configList.add(select);
        
        select = new SelectConfig();
        populateSimpleConfig(select, index++, "object.specimenType", "Specimen Type", false);
        select.setListType(DataType.CODE);
        select.setListClass(SpecimenType.class.getName());
        configList.add(select);
        
        select = new SelectConfig();
        populateSimpleConfig(select, index++, "participant.gender", "Gender", false);
        select.setListType(DataType.ENUM);
        select.setListClass(Gender.class.getName());
        select.setMultiSelect(true);
        select.setMultiSelectValueType(MultiSelectValueType.GENDER);
        configList.add(select);
        
        select = new SelectConfig();
        populateSimpleConfig(select, index++, "participant.races", "Race", false);
        select.setListType(DataType.ENUM);
        select.setListClass(Race.class.getName());
        select.setMultiSelect(true);
        select.setMultiSelectValueType(MultiSelectValueType.RACE);
        select.setMultiSelectCollection(true);
        configList.add(select);

        RangeConfig range = new RangeConfig();
        range.setSearchConditionClass("com.fiveamsolutions.tissuelocator.service.search.RangeSearchCondition");
        populateSimpleConfig(range, index++, "collectionYear", "Collection Year", true);
        configList.add(range);

        TextConfig textComp = new TextConfig();
        populateSimpleConfig(textComp, index++, "minimumAvailableQuantity", "Minimum Available Quantity", false);
        SelectConfig textSelect = new SelectConfig();
        populateSimpleConfig(textSelect, index++, "object.availableQuantityUnits", "", false);
        textSelect.setListType(DataType.ENUM);
        textSelect.setListClass(QuantityUnits.class.getName());
        TextSelectCompositeConfig tsc = new TextSelectCompositeConfig();
        tsc.setTextConfig(textComp);
        tsc.setSelectConfig(textSelect);
        tsc.setId(index++);
        tsc.setSearchSetType(SearchSetType.ADVANCED);
        configList.add(tsc);

        RangeConfig rangeComp = new RangeConfig();
        String className = "com.fiveamsolutions.tissuelocator.service.search.TimeUnitRangeSearchCondition";
        rangeComp.setSearchConditionClass(className);
        populateSimpleConfig(rangeComp, index++, "patientAge", "Patient Age At Collection", true);
        SelectConfig rangeSelect = new SelectConfig();
        populateSimpleConfig(rangeSelect, index++, "object.patientAgeAtCollectionUnits", "", true);
        rangeSelect.setListType(DataType.ENUM);
        rangeSelect.setListClass(TimeUnits.class.getName());
        RangeSelectCompositeConfig rsc = new RangeSelectCompositeConfig();
        rsc.setRangeConfig(rangeComp);
        rsc.setSelectConfig(rangeSelect);
        rsc.setId(index++);
        rsc.setSearchSetType(SearchSetType.ADVANCED);
        configList.add(rsc);

        CheckboxConfig check = new CheckboxConfig();
        populateSimpleConfig(check, index++, "priceNegotiable", "Price Negotiable", false);
        configList.add(check);

        AutocompleteConfig auto = new AutocompleteConfig();
        populateSimpleConfig(auto, index++, "object.pathologicalCharacteristic", "Disease", true);
        auto.setListUrl("listUrl");
        auto.setValueProperty("valueProp");
        configList.add(auto);

        CheckboxConfig cComp = new CheckboxConfig();
        populateSimpleConfig(cComp, index++, "normalSample", "Normal Sample", false);
        AutocompleteConfig aComp = new AutocompleteConfig();
        populateSimpleConfig(aComp, index++, "object.pathologicalCharacteristic", "Disease", false);
        auto.setListUrl("listUrl");
        auto.setValueProperty("valueProp");
        CheckboxAutocompleteCompositeConfig cac = new CheckboxAutocompleteCompositeConfig();
        cac.setId(index++);
        cac.setAutocompleteConfig(aComp);
        cac.setCheckboxConfig(cComp);
        cac.setCheckboxTextValue("textVal");
        cac.setSearchSetType(SearchSetType.ADVANCED);
        configList.add(cac);

        SelectDynamicExtensionConfig sdeConfig = new SelectDynamicExtensionConfig();
        populateSimpleConfig(sdeConfig, index++, "object.customProperties['storageCondition']",
                "Storage Condition", true);
        ControlledVocabularyDynamicFieldDefinition fd = new ControlledVocabularyDynamicFieldDefinition();
        fd.setEntityClassName(SpecimenRequest.class.getName());
        fd.setFieldDisplayName("Storage Condition");
        fd.setFieldName("storageCondition");
        fd.setNullable(true);
        fd.setOptions(new ArrayList<String>());
        fd.setSearchable(false);
        sdeConfig.setControlledVocabConfig(fd);
        configList.add(sdeConfig);

        SelectDynamicExtensionConfig sdeConfig2 = new SelectDynamicExtensionConfig();
        populateSimpleConfig(sdeConfig2, index++, "object.customProperties['anatomicSource']",
                "Anatomic Source", false);
        ControlledVocabularyDynamicFieldDefinition fd2 = new ControlledVocabularyDynamicFieldDefinition();
        fd2.setEntityClassName(SpecimenRequest.class.getName());
        fd2.setFieldDisplayName("Storage Condition");
        fd2.setFieldName("storageCondition");
        fd2.setNullable(true);
        fd2.setOptions(new ArrayList<String>());
        fd2.setSearchable(false);
        sdeConfig2.setControlledVocabConfig(fd2);
        configList.add(sdeConfig2);
        sdeConfig2.setSearchSetType(SearchSetType.WIDGET);

        SelectDynamicExtensionConfig sdeConfig3 = new SelectDynamicExtensionConfig();
        populateSimpleConfig(sdeConfig3, index++, "object.customProperties['anatomicSource']",
                "Anatomic Source 2", true);
        ControlledVocabularyDynamicFieldDefinition fd3 = new ControlledVocabularyDynamicFieldDefinition();
        fd3.setEntityClassName(SpecimenRequest.class.getName());
        fd3.setFieldDisplayName("Storage Condition");
        fd3.setFieldName("storageCondition");
        fd3.setNullable(true);
        fd3.setOptions(new ArrayList<String>());
        fd3.setSearchable(false);
        sdeConfig3.setControlledVocabConfig(fd3);
        sdeConfig3.setSearchSetType(SearchSetType.WIDGET);
        sdeConfig3.getFieldConfig().setRequiredRole(role);
        configList.add(sdeConfig3);

        return configList;
    }

    private void populateSimpleConfig(SimpleSearchFieldConfig config, Long id, String propertyName,
            String displayName, boolean filterCriteriaString) {
        config.setFieldConfig(new GenericFieldConfig());
        config.getFieldConfig().setSearchFieldName(propertyName);
        config.getFieldConfig().setSearchFieldDisplayName(displayName);
        config.getFieldConfig().setId(id);
        config.setNote("note");
        config.setObjectProperty(false);
        config.setErrorProperty("error property");
        config.setId(id);
        config.setSearchSetType(SearchSetType.ADVANCED);
        config.setFilterCriteriaString(filterCriteriaString);
    }
}
