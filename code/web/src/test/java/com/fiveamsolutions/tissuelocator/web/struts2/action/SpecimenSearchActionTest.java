/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.util.SubsetIteratorFilter.Decider;
import org.displaytag.tags.TableTagParameters;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.SavedSpecimenSearch;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.category.DisplayOption;
import com.fiveamsolutions.tissuelocator.data.config.category.SearchResultFieldDisplaySetting;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSection;
import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.TextConfig;
import com.fiveamsolutions.tissuelocator.service.config.category.UiSearchFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SavedSpecimenSearchServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorRequestHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractPersistentObjectAction.PaginatedSearchHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.SpecimenSearchAction.SpecimenGroupedByInstitution;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.SearchFieldConfigServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.SearchResultDisplaySettingsServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.SpecimenSearchServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 *
 */
public class SpecimenSearchActionTest extends AbstractTissueLocatorWebTest {

    private static final int MIN_QUANTITY_INDEX = 0;
    private static final int MULTI_SELECT_COLLECTION_INDEX = 1;
    private static final int MULTI_SELECT_INDEX = 2;
    private static final int RANGE_PARAMS_INDEX = 3;
    private static final int SELECTED_TAB_INDEX = 4;
    private static final int PARAMS_SIZE = 5;

    private static final String ATTRIBUTE_NAME = SpecimenSearchAction.class.getSimpleName() + "_params";
    private static final String SELECTED_TAB = "#tab_2_content";

    /**
     * Tests the list method.
     */
    @Test
    @SuppressWarnings("rawtypes")
    public void testList() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        String result = ssa.list();
        assertEquals(Action.SUCCESS, result);
        Object[] params = (Object[]) getSession().getAttribute(ATTRIBUTE_NAME);
        assertNull(params);
        testFilter();
        ssa = getSpecimenSearchAction();
        result = ssa.list();
        assertEquals(Action.SUCCESS, result);
        params = (Object[]) getSession().getAttribute(ATTRIBUTE_NAME);
        assertNotNull(params);
        assertNotNull(ssa.getMinimumAvailableQuantity());
        assertEquals(params[MIN_QUANTITY_INDEX], ssa.getMinimumAvailableQuantity());
        assertNotNull(ssa.getMultiSelectCollectionParamMap());
        assertEquals(params[MULTI_SELECT_COLLECTION_INDEX], ssa.getMultiSelectCollectionParamMap());
        assertNotNull(ssa.getMultiSelectParamMap());
        assertEquals(params[MULTI_SELECT_INDEX], ssa.getMultiSelectParamMap());
        assertNotNull(ssa.getRangeSearchParameters());
        assertEquals(params[RANGE_PARAMS_INDEX], ssa.getRangeSearchParameters());
        assertNotNull(ssa.getSelectedTab());
        assertEquals(params[SELECTED_TAB_INDEX], ssa.getSelectedTab());

        Specimen sp = new Specimen();
        AdditionalPathologicFinding example = new AdditionalPathologicFinding();
        example.setName("fake");
        example.setActive(true);
        example.setId(1L);
        sp.setPathologicalCharacteristic(example);
        result = ssa.list();
        assertEquals(Action.SUCCESS, result);

        ssa.setObject(sp);
        ssa.setNormalSample(true);
        result = ssa.list();
        assertEquals(Action.SUCCESS, result);
        assertFalse(ssa.isNormalSample());

        getRequest().addParameter(TableTagParameters.PARAMETER_EXPORTING, "true");
        result = ssa.list();
        assertEquals(Action.SUCCESS, result);
        PaginatedSearchHelper helper = TissueLocatorRequestHelper.getPaginatedSearchHelper(getRequest());
        assertNotNull(helper);
    }

    /**
     * Test the refreshList method.
     */
    @Test
    public void testRefreshList() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        Object[] params = new Object[1];
        params[0] = "test";
        getSession().setAttribute(ATTRIBUTE_NAME, params);
        assertEquals(Action.SUCCESS, ssa.refreshList());
        assertNull(getSession().getAttribute(ATTRIBUTE_NAME));
    }

    /**
     * Tests the filter method.
     */
    @Test
    public void testFilter() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.prepare();
        ssa.setMinimumAvailableQuantity(new BigDecimal("1.0"));
        ssa.getMultiSelectCollectionParamMap().put("participant.races",
                new ArrayList<Object>(Collections.singleton(Race.ASIAN)));
        ssa.setNormalSample(true);
        ssa.setSelectedTab(SELECTED_TAB);
        Collection<Object> instList = new ArrayList<Object>();
        Institution inst = new Institution();
        inst.setName("test institution");
        inst.setId(1L);
        instList.add(inst.getId());
        Institution inst2 = new Institution();
        inst2.setName("test institution 2");
        inst2.setId(2L);
        TissueLocatorRegistry.getServiceLocator().getInstitutionService().savePersistentObject(inst2);
        ssa.getObject().setExternalIdAssigner(inst2);
        ssa.getMultiSelectParamMap().put("externalIdAssigner.id", instList);
        ssa.getRangeSearchParameters().put("min_patientAgeAtCollection", 1);
        ssa.getRangeSearchParameters().put("max_patientAgeAtCollection", 2);
        ssa.getRangeSearchParameters().put("min_collectionYear", 1);
        ssa.getRangeSearchParameters().put("max_collectionYear", 2);
        ssa.getObject().setPathologicalCharacteristic(new AdditionalPathologicFinding());
        ssa.getObject().getPathologicalCharacteristic().setName("disorder");
        ssa.getObject().setParticipant(new Participant());
        ssa.getObject().getParticipant().setGender(Gender.MALE);
        String result = ssa.filter();
        assertEquals(Action.SUCCESS, result);
        Object[] params = (Object[]) getSession().getAttribute(ATTRIBUTE_NAME);
        assertNotNull(params);
        assertEquals(PARAMS_SIZE, params.length);
        assertEquals(ssa.getMinimumAvailableQuantity(), params[MIN_QUANTITY_INDEX]);
        assertEquals(ssa.getMultiSelectCollectionParamMap(), params[MULTI_SELECT_COLLECTION_INDEX]);
        assertFalse(ssa.getMultiSelectCollectionParamMap().isEmpty());
        assertTrue(ssa.getMultiSelectCollectionParamMap().get("participant.races").contains(Race.ASIAN));
        assertEquals(ssa.getMultiSelectParamMap(), params[MULTI_SELECT_INDEX]);
        assertEquals(ssa.getRangeSearchParameters(), params[RANGE_PARAMS_INDEX]);
        assertEquals(ssa.getSelectedTab(), params[SELECTED_TAB_INDEX]);
        assertTrue(ssa.isNormalSample());
        assertTrue(ssa.getDiseaseSelection().equals("disorder"));
        assertTrue(ssa.getMultiSelectParamMap().get("externalIdAssigner.id").contains(1L));
        assertTrue(ssa.getMultiSelectParamMap().get("externalIdAssigner.id").contains(2L));
        assertNotNull(ssa.getMultiSelectParamMap().get("participant.gender"));
        assertFalse(ssa.getMultiSelectParamMap().get("participant.gender").isEmpty());
        assertTrue(ssa.getMultiSelectParamMap().get("participant.gender").contains(Gender.MALE));
        assertNull(ssa.getMultiSelectParamMap().get("participant.ethnicity"));
    }

    /**
     * Tests the prepare method.
     */
    @Test
    public void testPrepare() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.setSession(new HashMap<String, Object>());
        ssa.prepare();
        assertNotNull(ssa.getObject());
        assertNull(ssa.getObject().getId());
        assertNotNull(ssa.getObject().getParticipant());
        assertFalse(ssa.getSearchConfigs().isEmpty());
        assertTrue("0".equals(ssa.getNormalSampleId()));
        assertEquals(0, ssa.getMinimumAggregateResultsDisplayed());
        assertTrue(ssa.isDisplayUsageRestrictions());
        assertTrue(ssa.isDisplayRequestProcessSteps());

        // Confirm configs with required role are excluded
        SearchFieldConfigServiceLocal svc = getTestSearchFieldConfigService();
        assertEquals(ssa.getSearchConfigs().size(), svc.getSearchFieldConfigs(SearchSetType.ADVANCED).size());

        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        for (AbstractSearchFieldConfig config : svc.getSearchFieldConfigs(SearchSetType.ADVANCED)) {
            if (config.getRequiredRole() != null) {
                request.addUserRole(config.getRequiredRole().getName());
            }
        }

        ssa = getSpecimenSearchAction();
        ssa.prepare();
        assertEquals(ssa.getSearchConfigs().size(), svc.getSearchFieldConfigs(SearchSetType.ADVANCED).size());

        ssa = getSpecimenSearchAction();
        ssa.setSearchSetType(SearchSetType.SIMPLE);
        ssa.prepare();
        assertEquals(ssa.getSearchConfigs().size(), svc.getSearchFieldConfigs(SearchSetType.SIMPLE).size());
        assertTrue(ssa.getSearchConfigs().size() < svc.getSearchFieldConfigs(SearchSetType.ADVANCED).size());
        assertNotNull(ssa.getObjects());

        ssa = getSpecimenSearchAction();
        getTestAppSettingService().setSpecimenGrouping("EXTERNAL_ID_ASSIGNER");
        ssa.prepare();
        assertNotNull(ssa.getObjects());
    }

    /**
     * Test simple search.
     */
    @Test
    public void testSimpleSearch() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.prepare();
        assertEquals(ssa.getSearchSetType(), SearchSetType.ADVANCED);
        assertEquals(ssa.isSimpleSearch(), ssa.getSearchSetType().equals(SearchSetType.SIMPLE));

        ssa = getSpecimenSearchAction();
        ssa.prepare();
        ssa.setSearchSetType(null);
        assertEquals(ssa.getSearchSetType(), SearchSetType.ADVANCED);
        assertEquals(ssa.isSimpleSearch(), ssa.getSearchSetType().equals(SearchSetType.SIMPLE));

        ssa = getSpecimenSearchAction();
        ssa.setSimpleSearch(true);
        ssa.prepare();
        assertEquals(ssa.isSimpleSearch(), ssa.getSearchSetType().equals(SearchSetType.SIMPLE));

        ssa = getSpecimenSearchAction();
        ssa.setSimpleSearch(false);
        ssa.prepare();
        assertEquals(ssa.isSimpleSearch(), ssa.getSearchSetType().equals(SearchSetType.SIMPLE));

        ssa = getSpecimenSearchAction();
        ssa.setSearchSetType(SearchSetType.ADVANCED);
        ssa.prepare();
        assertFalse(ssa.isSimpleSearch());

        ssa = getSpecimenSearchAction();
        ssa.setSearchSetType(SearchSetType.SIMPLE);
        ssa.prepare();
        assertTrue(ssa.isSimpleSearch());
    }

    /**
     * Test tabbed search and sectioned categories views.
     */
    @Test
    public void testTabbedSearchAndSectionedCategoriesViews() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.prepare();
        assertEquals(getTestAppSettingService().isTabbedSearchAndSectionedCategoriesViewsEnabled(),
                ssa.isTabbedSearchAndSectionedCategoriesViewsEnabled());

        ssa.setIsTabbedSearchAndSectionedCategoriesViewsEnabled(false);
        assertFalse(ssa.isTabbedSearchAndSectionedCategoriesViewsEnabled());

        ssa.prepare();
        assertTrue(ssa.isTabbedSearchAndSectionedCategoriesViewsEnabled());

        ssa.setSelectedTab(SELECTED_TAB);
        ssa.filter();
        ssa.setSelectedTab(StringUtils.EMPTY);
        ssa.list();
        assertEquals(SELECTED_TAB, ssa.getSelectedTab());
    }

    /**
     * Tests the list method with a group by.
     */
    @Test
    @SuppressWarnings("rawtypes")
    public void testGroupByList() {
        getTestAppSettingService().setSpecimenGrouping("EXTERNAL_ID_ASSIGNER");

        SpecimenSearchServiceStub serviceStub = new SpecimenSearchServiceStub();
        SpecimenSearchAction ssa = getSpecimenSearchAction(serviceStub, getTestUiSearchFieldCategoryService(),
                getTestSearchFieldConfigService());
        ssa.prepare();

        String result = ssa.list();
        assertEquals("successGroup", result);

        result = ssa.filter();
        assertEquals("successGroup", result);

        assertEquals(2, ssa.getObjects().getList().size());
        assertTrue(serviceStub.isSearchCalled());
        Iterator results = ssa.getObjects().getList().iterator();
        SpecimenGroupedByInstitution searchResult = (SpecimenGroupedByInstitution) results.next();
        assertEquals("a", searchResult.getInstitution().getName());
        assertEquals(1, searchResult.getCount().intValue());
        searchResult = (SpecimenGroupedByInstitution) results.next();
        assertEquals("b", searchResult.getInstitution().getName());
        assertEquals(1, searchResult.getCount().intValue());

        getRequest().addParameter(TableTagParameters.PARAMETER_EXPORTING, "true");
        result = ssa.list();
        assertEquals("successGroup", result);
        PaginatedSearchHelper helper = TissueLocatorRequestHelper.getPaginatedSearchHelper(getRequest());
        assertNull(helper);
    }

    /**
     * Tests the input method with a group by.
     */
    @Test
    public void testGroupByInput() {
        getTestAppSettingService().setSpecimenGrouping("EXTERNAL_ID_ASSIGNER");

        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.prepare();

        String result = ssa.input();
        assertEquals("inputGroup", result);

    }

    /**
     * Test config value initialization.
     */
    @Test
    public void testConfigValues() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        populateAction(ssa);
        ssa.prepare();
        ssa.list();
        Map<AbstractSearchFieldConfig, Object> configValues = TissueLocatorRequestHelper
                .getSearchConfigValueMap(ServletActionContext.getRequest());
        assertFalse(configValues.isEmpty());
        SearchFieldConfigServiceLocal svc = getTestSearchFieldConfigService();
        assertEquals(svc.getSearchFieldConfigs(SearchSetType.ADVANCED).size()
                + SearchFieldConfigServiceStub.COMPOSITE_CONFIG_COUNT, configValues.size());
        Collection<Object> values = configValues.values();
        assertTrue(values.contains(ssa.getMultiSelectParamMap().get("participant.gender")));
        assertTrue(values.contains(ssa.getMultiSelectParamMap().get("participant.races")));
        assertTrue(values.contains(ssa.getMinimumAvailableQuantity()));
        ssa = getSpecimenSearchAction();
        ssa.prepare();
        ssa.filter();
        configValues = TissueLocatorRequestHelper.getSearchConfigValueMap(ServletActionContext.getRequest());
        assertFalse(configValues.isEmpty());
        assertEquals(svc.getSearchFieldConfigs(SearchSetType.ADVANCED).size()
                + SearchFieldConfigServiceStub.COMPOSITE_CONFIG_COUNT, configValues.size());
        values = configValues.values();
        assertTrue(values.contains(ssa.getMultiSelectParamMap().get("participant.gender")));
        assertTrue(values.contains(ssa.getMultiSelectParamMap().get("participant.races")));
        assertTrue(values.contains(ssa.getMinimumAvailableQuantity()));
    }

    /**
     * Tests the read only view of search criteria.
     */
    @Test
    public void testCriteriaString() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.prepare();
        assertFalse(StringUtils.isBlank(ssa.getCriteriaString()));
        assertEquals("specimen.search.noCriteria", ssa.getCriteriaString());
        populateAction(ssa);
        String criteriaString = ssa.getCriteriaString();
        assertTrue(StringUtils.isNotBlank(criteriaString));
        assertTrue(criteriaString.contains("Disease: Cancer"));
        assertTrue(criteriaString.contains("Collection Year: 1 - 2"));
        assertTrue(criteriaString.contains("Minimum Available Quantity: 123.45 milligrams"));
        assertTrue(criteriaString.contains("Patient Age At Collection: 1 - 2 years"));
        assertTrue(criteriaString.contains("Gender: Female"));
        assertTrue(criteriaString.contains("Race:"));
        assertTrue(criteriaString.contains("Asian"));
        assertTrue(criteriaString.contains("Black or African American"));
        assertTrue(StringUtils.split(criteriaString, ";").length > 1);
    }

    /**
     * Tests the read only view of the filtered search criteria.
     */
    @Test
    public void testFilteredCriteriaString() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.prepare();
        assertFalse(StringUtils.isBlank(ssa.getFilteredCriteriaString()));
        assertEquals("specimen.search.noCriteria", ssa.getFilteredCriteriaString());
        populateAction(ssa);
        String criteriaString = ssa.getFilteredCriteriaString();
        assertTrue(StringUtils.isNotBlank(criteriaString));
        assertTrue(criteriaString.contains("Disease: Cancer"));
        assertFalse(criteriaString.contains("Collection Year: 1 - 2"));
        assertTrue(criteriaString.contains("Minimum Available Quantity: 123.45 milligrams"));
        assertTrue(criteriaString.contains("Patient Age At Collection: 1 - 2 years"));
        assertTrue(StringUtils.split(criteriaString, ";").length > 1);
    }

    /**
     * Test invalid constructor.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testInvalidConstructor() {
        SpecimenSearchAction ssa = new SpecimenSearchAction() {

            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
        assertNull("UnsuportedOperationException should have been thrown", ssa);
    }

    private void populateAction(SpecimenSearchAction ssa) {
        ssa.getObject().setExternalId("external id");
        AdditionalPathologicFinding apf = new AdditionalPathologicFinding();
        apf.setId(1L);
        apf.setName("Cancer");
        apf.setValue("cancerCode");
        ssa.getObject().setPathologicalCharacteristic(apf);
        ssa.setMinimumAvailableQuantity(new BigDecimal("123.45"));
        ssa.getObject().setAvailableQuantityUnits(QuantityUnits.MG);
        ssa.getRangeSearchParameters().put("min_collectionYear", "1");
        ssa.getRangeSearchParameters().put("max_collectionYear", "2");
        ssa.getRangeSearchParameters().put("min_patientAge", "1");
        ssa.getRangeSearchParameters().put("max_patientAge", "2");
        ssa.getObject().setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        ssa.getMultiSelectParamMap().put("participant.gender", new ArrayList<Object>());
        ssa.getMultiSelectParamMap().get("participant.gender").add(Gender.FEMALE);
        ssa.getMultiSelectCollectionParamMap().put("participant.races", new ArrayList<Object>());
        ssa.getMultiSelectCollectionParamMap().get("participant.races").add(Race.ASIAN);
        ssa.getMultiSelectCollectionParamMap().get("participant.races").add(Race.BLACK_OR_AFRICAN_AMERICAN);
    }

    /**
     * Tests the range search parameters.
     */
    @Test
    public void testGetRangeSearchParameters() {
        Map<String, Object> rangeSearchParameters = new HashMap<String, Object>();
        rangeSearchParameters.put("min_collectionYear", new String[] {"1"});

        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.prepare();
        ssa.setRangeSearchParameters(rangeSearchParameters);

        assertEquals(1, ssa.getRangeSearchParameters().get("min_collectionYear"));
    }

    /**
     * Tests the action validation.
     */
    @Test
    public void testValidate() {
        String minParameterName = "min_collectionYear";
        SpecimenSearchAction ssa;
        Map<String, Object> rangeSearchParameters = new HashMap<String, Object>();
        // check successful validation
        ssa = getSpecimenSearchAction();
        rangeSearchParameters.put(minParameterName, new String[] {"1"});
        ssa.setRangeSearchParameters(rangeSearchParameters);
        ssa.prepare();
        ssa.validate();
        assertFalse(ssa.hasErrors());

        // check unsuccessful validation
        ssa = getSpecimenSearchAction();
        rangeSearchParameters.put(minParameterName, new String[] {"one"});
        ssa.setRangeSearchParameters(rangeSearchParameters);
        ssa.prepare();
        ssa.validate();
        ResourceBundle bundle = ssa.getTexts("ApplicationResources");
        Map<String, List<String>> fieldErrors = ssa.getFieldErrors();
        String resourceKey = fieldErrors.get(minParameterName).get(0);
        String errorMessage = String.format(bundle.getString(resourceKey), "Minimum Collection Year");
        assertEquals("Minimum Collection Year must be a whole number.", errorMessage);
        // test that the SearchConfigValueMap has actually been created
        assertNotNull(TissueLocatorRequestHelper.getSearchConfigValueMap(ServletActionContext.getRequest()));
    }

    /**
     * Test the getCountFilteredAggregateResults method.
     */
    @Test
    public void testCountFilteredAggregateResults() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        getTestAppSettingService().setSpecimenGrouping("EXTERNAL_ID_ASSIGNER");
        ssa.prepare();
        String result = ssa.filter();
        assertEquals("successGroup", result);
        assertEquals(0, ssa.getInstitutionsBelowMinimumResultsCount().size());
        getTestAppSettingService().setMinimumAggregateResultsDisplayed(2);
        ssa.prepare();
        result = ssa.filter();
        assertEquals("successGroup", result);
        assertEquals(2, ssa.getObjects().getList().size());
        assertEquals(2, ssa.getInstitutionsBelowMinimumResultsCount().size());
        SpecimenSearchServiceStub specStub = new SpecimenSearchServiceStub();
        specStub.setAggregateResultsCount(2L);
        ssa.prepare();
        result = ssa.filter();
        assertEquals("successGroup", result);
        assertEquals(2, ssa.getObjects().getList().size());
        assertEquals(2, ssa.getInstitutionsBelowMinimumResultsCount().size());
    }

    /**
     * Test the advanced search field user interface categories.
     */
    @Test
    public void testAdvancedUiSearchFieldCatagories() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.setSearchSetType(SearchSetType.ADVANCED);
        ssa.prepare();

        List<UiSearchFieldCategory> categories = ssa.getUiSearchFieldCategories();
        assertFalse(categories.isEmpty());
    }

    /**
     * Test the search field user interface categories.
     */
    @Test
    public void testSimpleUiSearchFieldCatagories() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.setSearchSetType(SearchSetType.SIMPLE);
        ssa.prepare();

        List<UiSearchFieldCategory> categories = ssa.getUiSearchFieldCategories();
        assertFalse(categories.isEmpty());
    }

    /**
     * Test the search field configuration validation decider.
     *
     * @throws Exception if error occurs
     */
    @Test
    public void testSearchFieldConfigurationValidationDecider() throws Exception {
        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        request.addUserRole(Role.ADMINISTRATOR.getName());
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.prepare();
        Decider decider = ssa.getSearchFieldConfigurationValidationDecider();
        TextConfig textConfig = new TextConfig();
        textConfig.setFieldConfig(new GenericFieldConfig());
        textConfig.getFieldConfig().setRequiredRole(null);
        assertTrue(decider.decide(textConfig));
        ApplicationRole role = new ApplicationRole();
        role.setName(Role.ADMINISTRATOR.getName());
        textConfig.getFieldConfig().setRequiredRole(role);
        assertTrue(decider.decide(textConfig));
        role.setName(Role.TISSUE_LOCATOR_USER.getName());
        textConfig.getFieldConfig().setRequiredRole(role);
        assertFalse(decider.decide(textConfig));
    }

    /**
     * Test the user interface section.
     */
    @Test
    public void testUiSections() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.prepare();

        UiSection returnedUiSection = ssa.getUiSections().get(0);
        assertEquals(UiSection.class.getSimpleName(), returnedUiSection.getSectionName());
        assertEquals(ssa.getObjectType().getName(), returnedUiSection.getEntityClassName());

        List<UiSearchFieldCategory> categories = new ArrayList<UiSearchFieldCategory>(
                returnedUiSection.getUiSearchFieldCategories());
        assertFalse(categories.isEmpty());
    }

    /**
     * Test search result display setting retrieval.
     */
    @Test
    public void testGetSearchResultFieldDisplaySettings() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        ssa.setSession(new HashMap<String, Object>());
        SearchResultDisplaySettingsServiceStub stub = getTestSearchResultDisplaySettingService();
        List<SearchResultFieldDisplaySetting> settings = createSearchResultFieldDisplaySettings();
        stub.setDefaultSearchResultDisplaySettings(settings);
        ssa.prepare();
        List<SearchResultFieldDisplaySetting> retrievedSettings = ssa.getSearchResultFieldDisplaySettings();
        assertEquals(settings.size(), retrievedSettings.size());
        for (int i = 0; i < settings.size(); i++) {
            assertEquals(settings.get(i).getFieldConfig().getSearchFieldName(),
                    retrievedSettings.get(i).getFieldConfig().getSearchFieldName());
        }

        int count = settings.size() - 1;
        Iterator<SearchResultFieldDisplaySetting> it = settings.iterator();
        for (int i = count; i >= 0; i--) {
            it.next().setId(new Long(i));
        }
        stub.setDefaultSearchResultDisplaySettings(settings);
        ssa = getSpecimenSearchAction();
        ssa.setSession(new HashMap<String, Object>());
        ssa.prepare();
        retrievedSettings = ssa.getSearchResultFieldDisplaySettings();
        assertEquals(settings.size(), retrievedSettings.size());
        for (int i = 0; i < settings.size(); i++) {
            assertEquals(new Long(i), retrievedSettings.get(i).getId());
        }

        Collections.shuffle(settings);
        stub.setDefaultSearchResultDisplaySettings(settings);
        ssa = getSpecimenSearchAction();
        ssa.setSession(new HashMap<String, Object>());
        ssa.prepare();
        retrievedSettings = ssa.getSearchResultFieldDisplaySettings();
        assertEquals(settings.size(), retrievedSettings.size());
        for (int i = 0; i < settings.size(); i++) {
            assertEquals(new Long(i), retrievedSettings.get(i).getId());
        }

        List<SearchResultFieldDisplaySetting> ids = new ArrayList<SearchResultFieldDisplaySetting>();
        it = settings.iterator();
        for (int i = 0; i < settings.size(); i++) {
            SearchResultFieldDisplaySetting setting = it.next();
            if (i % 2 == 0) {
                setting.setId(new Long(i));
                ids.add(setting);
            } else {
                setting.setId(null);
            }
        }
        stub.setDefaultSearchResultDisplaySettings(settings);
        ssa = getSpecimenSearchAction();
        ssa.setSession(new HashMap<String, Object>());
        ssa.prepare();
        retrievedSettings = ssa.getSearchResultFieldDisplaySettings();
        assertEquals(settings.size(), retrievedSettings.size());
        it = retrievedSettings.iterator();
        for (int i = 0; i < ids.size(); i++) {
            assertEquals(ids.get(i).getId(), it.next().getId());
        }
    }

    private List<SearchResultFieldDisplaySetting> createSearchResultFieldDisplaySettings() {
        List<SearchResultFieldDisplaySetting> settings = new ArrayList<SearchResultFieldDisplaySetting>();
        List<UiSearchFieldCategory> searchCategories = getTestUiSearchFieldCategoryService().getUiSearchFieldCategories(
                Specimen.class, SearchSetType.ADVANCED);
        for (UiSearchFieldCategory category : searchCategories) {
            for (UiSearchCategoryField field : category.getUiSearchCategoryFields()) {
                SearchResultFieldDisplaySetting setting = new SearchResultFieldDisplaySetting();
                setting.setFieldConfig(new GenericFieldConfig());
                setting.getFieldConfig().setSearchFieldName(field.getFieldConfig().getSearchFieldName());
                setting.setFieldCategory(category);
                setting.setDisplayFlag(DisplayOption.ENABLED);
                settings.add(setting);
            }
        }
        return settings;
    }

    /**
     * Test the retrieval of saved specimen search.
     */
    @Test
    public void testGetSavedSpecimenSearch() {
        SpecimenSearchAction ssa = getSpecimenSearchAction();
        getSession().setAttribute(SavedSpecimenSearch.class.getSimpleName(), Long.valueOf("1"));
        assertNotNull(ssa.getSavedSpecimenSearch());
        assertEquals(SavedSpecimenSearch.class.getSimpleName(), ssa.getSavedSpecimenSearch().getName());
    }

    /**
     * Test search config retrieval by category.
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testGetSearchConfigsByCategory() {
        SearchConfig.reset();
        UiSearchFieldCategoryServiceLocal categoryService = mock(UiSearchFieldCategoryServiceLocal.class);
        SearchFieldConfigServiceLocal searchConfigService = mock(SearchFieldConfigServiceLocal.class);

        List<AbstractSearchFieldConfig> configList = new ArrayList<AbstractSearchFieldConfig>();
        SelectConfig searchConfig = new SelectConfig();
        searchConfig.setFieldConfig(new GenericFieldConfig());
        searchConfig.getFieldConfig().setId(0L);
        configList.add(searchConfig);
        searchConfig = new SelectConfig();
        searchConfig.setFieldConfig(new GenericFieldConfig());
        searchConfig.getFieldConfig().setId(1L);
        configList.add(searchConfig);
        searchConfig = new SelectConfig();
        searchConfig.setFieldConfig(new GenericFieldConfig());
        searchConfig.getFieldConfig().setId(2L);
        configList.add(searchConfig);


        UiSearchFieldCategory category = new UiSearchFieldCategory();
        List<UiSearchFieldCategory> categoryList = new ArrayList<UiSearchFieldCategory>();
        categoryList.add(category);
        when(categoryService.getUiSearchFieldCategories(any(Class.class),
                any(SearchSetType.class))).thenReturn(categoryList);
        when(searchConfigService.getSearchFieldConfigs(any(SearchSetType.class))).thenReturn(configList);

        // unmatched configs
        SpecimenSearchAction action = getSpecimenSearchAction(new SpecimenSearchServiceStub(),
                categoryService, searchConfigService);
        action.prepare();
        assertTrue(action.getSearchFieldConfigsForCategory(category).isEmpty());

        action = getSpecimenSearchAction(new SpecimenSearchServiceStub(),
                categoryService, searchConfigService);
        action.prepare();
        assertTrue(action.getSearchFieldConfigsForCategory(category).isEmpty());

        category.setUiSearchCategoryFields(new ArrayList<UiSearchCategoryField>());
        UiSearchCategoryField field = new UiSearchCategoryField();
        field.setFieldConfig(new GenericFieldConfig());
        //CHECKSTYLE:OFF
        field.getFieldConfig().setId(3L);
        //CHECKSTYLE:ON
        category.getUiSearchCategoryFields().add(field);
        action = getSpecimenSearchAction(new SpecimenSearchServiceStub(),
                categoryService, searchConfigService);
        action.prepare();
        assertTrue(action.getSearchFieldConfigsForCategory(category).isEmpty());

        field.getFieldConfig().setId(0L);
        action = getSpecimenSearchAction(new SpecimenSearchServiceStub(),
                categoryService, searchConfigService);
        action.prepare();
        assertEquals(1, action.getSearchFieldConfigsForCategory(category).size());
        assertEquals(0, action.getSearchFieldConfigsForCategory(new UiSearchFieldCategory()).size());

        UiSearchFieldCategory category2 = new UiSearchFieldCategory();
        category2.setUiSearchCategoryFields(new ArrayList<UiSearchCategoryField>());
        UiSearchCategoryField field2 = new UiSearchCategoryField();
        field2.setFieldConfig(new GenericFieldConfig());
        //CHECKSTYLE:OFF
        field2.getFieldConfig().setId(3L);
        //CHECKSTYLE:ON
        category2.getUiSearchCategoryFields().add(field2);
        categoryList.add(category2);
        action = getSpecimenSearchAction(new SpecimenSearchServiceStub(),
                categoryService, searchConfigService);
        action.prepare();
        assertEquals(1, action.getSearchFieldConfigsForCategory(category).size());
        assertEquals(0, action.getSearchFieldConfigsForCategory(category2).size());
        assertEquals(0, action.getSearchFieldConfigsForCategory(new UiSearchFieldCategory()).size());

        field2.getFieldConfig().setId(1L);
        action = getSpecimenSearchAction(new SpecimenSearchServiceStub(),
                categoryService, searchConfigService);
        action.prepare();
        assertEquals(1, action.getSearchFieldConfigsForCategory(category).size());
        assertEquals(1, action.getSearchFieldConfigsForCategory(category2).size());
        assertEquals(0, action.getSearchFieldConfigsForCategory(new UiSearchFieldCategory()).size());

        category.getUiSearchCategoryFields().add(field2);
        action = getSpecimenSearchAction(new SpecimenSearchServiceStub(),
                categoryService, searchConfigService);
        action.prepare();
        assertEquals(2, action.getSearchFieldConfigsForCategory(category).size());
        assertEquals(1, action.getSearchFieldConfigsForCategory(category2).size());
        assertEquals(0, action.getSearchFieldConfigsForCategory(new UiSearchFieldCategory()).size());
        SearchConfig.reset();
    }

    private SpecimenSearchAction getSpecimenSearchAction() {
        return getSpecimenSearchAction(new SpecimenSearchServiceStub(),
                getTestUiSearchFieldCategoryService(), getTestSearchFieldConfigService());
    }

    private SavedSpecimenSearchServiceLocal getMockSavedSpecimenSearchService() {
        SavedSpecimenSearch savedSearch = new SavedSpecimenSearch();
        savedSearch.setName(SavedSpecimenSearch.class.getSimpleName());
        SavedSpecimenSearchServiceLocal mockSavedSpecimenSearchService = mock(SavedSpecimenSearchServiceLocal.class);
        when(mockSavedSpecimenSearchService.getSavedSpecimenSearchAndData(any(Long.class))).thenReturn(savedSearch);
        return mockSavedSpecimenSearchService;
    }

    private SpecimenSearchAction getSpecimenSearchAction(SpecimenSearchServiceStub stub,
            UiSearchFieldCategoryServiceLocal uiSearchFieldCategoryService,
            SearchFieldConfigServiceLocal searchFieldConfigService) {
        return new SpecimenSearchAction(stub,
                getTestUserService(),
                getTestAppSettingService(),
                getMockSavedSpecimenSearchService(), uiSearchFieldCategoryService,
                getTestSearchResultDisplaySettingService(), getTestUiSectionService(),
                searchFieldConfigService, getTestUiDynamicFieldCategoryService()) {

            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }

        };
    }

}