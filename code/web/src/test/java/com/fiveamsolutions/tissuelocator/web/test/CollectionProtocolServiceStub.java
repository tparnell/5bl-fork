/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;

/**
 * @author smiller
 *
 */
public class CollectionProtocolServiceStub extends GenericServiceStub<CollectionProtocol> implements
        CollectionProtocolServiceLocal {

    private int searchResultsSize = 1;

    /**
     * {@inheritDoc}
     */
    public Map<String, CollectionProtocol> getProtocols(Long institutionId, Collection<String> names) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public List<CollectionProtocol> getProtocolsByInstitution(Long institutionId) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CollectionProtocol> search(SearchCriteria<CollectionProtocol> criteria,
            PageSortParams<CollectionProtocol> params) {
        TissueLocatorAnnotatedBeanSearchCriteria<CollectionProtocol> crit =
            (TissueLocatorAnnotatedBeanSearchCriteria<CollectionProtocol>) criteria;
        List<CollectionProtocol> results = new ArrayList<CollectionProtocol>();
        if (StringUtils.isNotBlank(crit.getCriteria().getName())) {
            for (int i = 0; i < searchResultsSize; i++) {
                crit.getCriteria().setId(1L);
                results.add(crit.getCriteria());
            }
        }
        return results;
    }

    /**
     * @param searchResultsSize the searchResultsSize to set
     */
    public void setSearchResultsSize(int searchResultsSize) {
        this.searchResultsSize = searchResultsSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectionProtocol getDefaultProtocol(Long institutionId) {
        return new CollectionProtocol();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Long, CollectionProtocol> getDefaultProtocols() {
        return new HashMap<Long, CollectionProtocol>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectionProtocol createDefaultProtocol(Institution institution) {
        CollectionProtocol cp = new CollectionProtocol();
        cp.setName("default protocol");
        cp.setInstitution(institution);
        return cp;
    }
}