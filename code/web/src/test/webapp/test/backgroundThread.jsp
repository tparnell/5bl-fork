<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<%@page import="com.fiveamsolutions.tissuelocator.web.listener.SpecimenRequestVotingTask"%>
<%@page import="com.fiveamsolutions.tissuelocator.web.TissueLocatorContextParameterHelper"%>
<%@page import="com.fiveamsolutions.tissuelocator.web.listener.ScheduledRemindersTask"%>
<%@page import="com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal"%>
<%@page import="com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceBean"%>
<%@page import="com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry"%>
<%@page import="com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil"%>
<%@page import="com.fiveamsolutions.tissuelocator.inject.GuiceInjectorHolder"%>

<%
try {
    TissueLocatorHibernateUtil.getHibernateHelper().openAndBindSession();

    ApplicationSettingServiceLocal settingService =
        GuiceInjectorHolder.getInjector().getInstance(ApplicationSettingServiceBean.class);
    if (settingService.isVotingTaskEnabled()) {
        ServletContext context = request.getSession().getServletContext();
        int reviewPeriod = TissueLocatorContextParameterHelper.getReviewPeriod(context);
        int votingPeriod = TissueLocatorContextParameterHelper.getVotingPeriod(context);
        int minAvailable = TissueLocatorContextParameterHelper.getMinPercentAvailableVotes(context);
        int minWinning = TissueLocatorContextParameterHelper.getMinPercentWinningVotes(context);
        int reviewerAssignmentPeriod = TissueLocatorContextParameterHelper.getReviewerAssignmentPeriod(context);
        SpecimenRequestVotingTask task = new SpecimenRequestVotingTask(reviewPeriod, votingPeriod, minAvailable,
                minWinning, reviewerAssignmentPeriod);
        task.run();
    }

    try {
        TissueLocatorHibernateUtil.getHibernateHelper().openAndBindSession();

        int shipmentGracePeriod = settingService.getShipmentReceiptGracePeriod();
        int lineItemReviewGracePeriod = settingService.getLineItemReviewGracePeriod();
        int supportLetterRequestGracePeriod = settingService.getSupportLetterRequestReviewGracePeriod();
        int questionResponsePeriod = settingService.getQuestionResponseGracePeriod();
        ScheduledRemindersTask reminderTask = new ScheduledRemindersTask(shipmentGracePeriod, lineItemReviewGracePeriod, supportLetterRequestGracePeriod, questionResponsePeriod);
        reminderTask.run();
    } finally {
        TissueLocatorHibernateUtil.getHibernateHelper().unbindAndCleanupSession();
    }

} finally {
    TissueLocatorHibernateUtil.getHibernateHelper().unbindAndCleanupSession();
}
%>

<html>
<head>
    <title>Background threads complete</title>
</head>
<body>
    Background threads complete
</body>
</html>
