<#--
    Simplifying footer from xhtml theme to not include so many block level elements
-->
${parameters.after?if_exists}<#t/>
    </td><#lt/>
</tr>

<#if hasFieldErrors>
<#list fieldErrors[parameters.name] as error>
<tr errorFor="${parameters.id}">
<#if parameters.labelposition?default("") == 'top'>
    <td align="left" valign="top" colspan="2"><#rt/>
<#else>
    <td align="center" valign="top" colspan="2"><#rt/>
</#if>
        <span class="fielderror">${error?html}</span><#t/>
    </td><#lt/>
</tr>
</#list>
</#if>

