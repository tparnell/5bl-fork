<#--
    updating actionmessage in simple theme to not use ul's and li's
-->
<#if (actionMessages?? && actionMessages?size > 0)>
    <div>
        <#list actionMessages as message>
            <span
                <#if parameters.cssClass??>
                    class="${parameters.cssClass?html}"<#rt/>
                <#else>
                    class="actionMessage"<#rt/>
                </#if>
                <#if parameters.cssStyle??>
                    style="${parameters.cssStyle?html}"<#rt/>
                </#if>
            >${message!}</span><br/>
        </#list>
    </div>
</#if>
