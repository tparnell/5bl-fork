<#--
    Simplifying header from css_xhtml theme to not include so many block level elements
-->
<#--
  Only show message if errors are available.
  This will be done if ActionSupport is used.
-->
<#assign hasFieldErrors = parameters.name?? && fieldErrors?? && fieldErrors[parameters.name]??/>
<div <#rt/><#if parameters.id??>id="wwgrp_${parameters.id}"<#rt/></#if> class="wwgrp">

<#if parameters.label??>

    <label <#t/>
        <#if parameters.id??>
                for="${parameters.id?html}" <#t/>
        </#if>
        class="label" <#t/>
        style="font-style:normal;" <#t>
        ><#t/>

        <#if parameters.required?default(false)>
            <#if parameters.requiredposition?default("left") == 'left'>
                <span class="reqd">*</span><#t/>
            </#if>
        </#if>

        ${parameters.label?html}${parameters.labelseparator!":"?html}

        <#if parameters.required?default(false)>
            <#if parameters.requiredposition?default("left") == 'right'>
                <span class="reqd">*</span><#t/>
            </#if>
        </#if>
        <#include "/${parameters.templateDir}/xhtml/tooltip.ftl" />
    </label><#t/>

</#if>
