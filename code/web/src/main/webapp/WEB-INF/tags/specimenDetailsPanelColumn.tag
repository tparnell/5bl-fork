<%@ tag body-content="empty" %>
<%@ attribute name="columnLabel" required="true" type="java.lang.String" %>
<%@ attribute name="columnValue" required="true" type="java.lang.Object" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<fmt:message key="specimen.availableQuantity" var="quantityLabel"/>
<fmt:message key="specimen.patientAgeAtCollection" var="ageLabel"/>

<s:if test="%{((#attr.columnValue == null) || (#attr.columnValue.isEmpty()))}">
    <fmt:message key="unknown.value"/>
</s:if>
<s:elseif test="%{#attr.columnLabel.matches(#attr.quantityLabel)}">
    <fmt:formatNumber value="${object.availableQuantity}" maxFractionDigits="2"/>
    <fmt:message key="${object.availableQuantityUnits.resourceKey}"/>    
</s:elseif>
<s:elseif test="%{#attr.columnLabel.matches(#attr.ageLabel)}">
    ${object.patientAgeAtCollection}
    <s:if test="%{object.patientAgeAtCollectionAtHippaMax}">+ </s:if>
    <fmt:message key="${object.patientAgeAtCollectionUnits.resourceKey}"/>
</s:elseif>
<s:else>
    <s:property value="%{#attr.columnValue}"/>
</s:else>