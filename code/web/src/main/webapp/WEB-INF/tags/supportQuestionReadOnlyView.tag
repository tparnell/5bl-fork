<%@ tag body-content="empty" %>
<%@ attribute name="question" required="true" type="com.fiveamsolutions.tissuelocator.data.support.Question" rtexprvalue="true" %>
<%@ attribute name="backUrl" required="true" %>
<%@ attribute name="isAdmin" required="true" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<html>
<head>
    <title><fmt:message key="question.view.title"/></title>
</head>
<body>
    <c:url value="${backUrl}" var="listUrl"/>
    <div class="topbtns">
        <a href="${listUrl}" class="btn">
            &laquo; <fmt:message key="question.adminReview.back"/>
        </a>
    </div>

    <tissuelocator:supportQuestionDetails question="${question}"/>

    <div class="box pad10">
        <h2 class="formtop noline nobg"><fmt:message key="questionResponse.responses.title"/></h2>

        <div class="col250" style="padding-top:0px;">
            <b><fmt:message key="question.status"/></b><br />
            <c:choose>
                <c:when test="${isAdmin == 'true' }">
                    <fmt:message key="${question.status.administratorResourceKey}"/>
                </c:when>
                <c:otherwise>
                    <fmt:message key="${question.status.researcherResourceKey}"/>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="clear"></div>

        <table class="data">
            <thead>
                <tr>
                    <th><div><fmt:message key="questionResponse.institution"/></div></th>
                    <th><div><fmt:message key="questionResponse.responder"/></div></th>
                    <th><div><fmt:message key="questionResponse.updated"/></div></th>
                    <th><div><fmt:message key="questionResponse.status"/></div></th>
                    <th><div><fmt:message key="questionResponse.response"/></div></th>
                </tr>
            </thead>
            <tbody>
                <s:iterator value="%{#attr.question.responses}" status="responseStatus">
                    <c:set var="rowCssClass" value=""/>
                    <s:if test="%{#responseStatus.odd}">
                        <c:set var="rowCssClass" value="odd"/>
                    </s:if>
                    <tr class="${rowCssClass}">
                        <td class="title"><s:property value="%{institution.name}"/></td>
                        <td><s:property value="%{responder.displayName}"/></td>
                        <td>
                            <s:date name="%{lastUpdatedDate}" format="%{getText('default.date.format')}"/>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${isAdmin == 'true' }">
                                    <s:property value="%{getText(status.administratorResourceKey)}"/>
                                </c:when>
                                <c:otherwise>
                                    <s:property value="%{getText(status.researcherResourceKey)}"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td><s:property value="%{response}"/></td>
                    </tr>
                </s:iterator>
            </tbody>
        </table>
    </div>
</body>
</html>

