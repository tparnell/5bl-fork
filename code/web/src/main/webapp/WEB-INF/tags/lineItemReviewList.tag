<%@ tag body-content="empty" %>
<%@ attribute name="lineItems" required="true" type="com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem[]" rtexprvalue="true" %>
<%@ attribute name="lineItemPrefix" required="true" rtexprvalue="true" %>
<%@ attribute name="titleKey" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<tr id="${lineItemPrefix}_title">
    <td colspan="6">
        <h2 class="noline">
            <fmt:message key="${titleKey}"/>
        </h2>
    </td>
</tr>

<tr id="${lineItemPrefix}_header">
    <th><div><fmt:message key="shipment.reviewDetails.criteria"/></div></th>
    <th><div><fmt:message key="shipment.reviewDetails.quantity"/></div></th>
    <th><div><fmt:message key="shipment.reviewDetails.reviewer"/></div></th>
    <th><div><fmt:message key="shipment.reviewDetails.vote"/></div></th>
    <th><div><fmt:message key="shipment.reviewDetails.date"/></div></th>
    <th><div><fmt:message key="shipment.reviewDetails.comment"/></div></th>
</tr>

<c:forEach items="${lineItems}" var="lineItem" varStatus="lineItemStatus">
    <c:set var="rowClass" value=""/>
    <c:if test="${lineItemStatus.count % 2 == 1}">
        <c:set var="rowClass" value="odd"/>
    </c:if>

    <tr class="${rowClass}" id="${lineItemPrefix}_data${lineItemStatus.index}">

        <td>${lineItem.criteria}</td>
        <td><fmt:formatNumber value="${lineItem.quantity}"/></td>
        <td>${lineItem.vote.user.lastName}, ${lineItem.vote.user.firstName}</td>
        <td><fmt:message key="${lineItem.vote.vote.status.resourceKey}"/></td>
        <td>
            <fmt:message key="default.datetime.format" var="dateTimeFormat"/>
            <fmt:formatDate value="${lineItem.vote.date}" pattern="${dateTimeFormat}"/>
        </td>
        <td>${lineItem.vote.comment}</td>

    </tr>
</c:forEach>
