<%@ tag body-content="empty" %>
<%@ attribute name="showRadioButtons" required="true" rtexprvalue="true" %>
<%@ attribute name="listActionBase" required="true" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set name="pendingStatus" value="%{@com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus@PENDING_REVIEW}" />
<s:set name="approvedStatus" value="%{@com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus@APPROVED}" />
<s:set name="outOfDateStatus" value="%{@com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus@OUT_OF_DATE}" />

<s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
<fmt:message key="mta.info.institutionList.summary" var="institutionListSummary"/>
<c:url var="currentImageUrl" value="/images/ico_success.gif"/>
<fmt:message key="mta.info.current" var="currentImageAlt"/>
<display:table name="objects" requestURI="/${listActionBase}/list.action"
        uid="institution" pagesize="${tablePageSize}" sort="external"
        style="width:557px;" summary="${institutionListSummary}">
    <tissuelocator:displaytagNarrowProperties/>

    <s:set var="institutionSignedMta" scope="page" value="%{#attr.institution.currentSignedRecipientMta}"/>
    <c:set var="columnClassSuffix" value=""/>
    <s:if test="%{#attr.institutionSignedMta == null}">
        <c:set var="columnClassSuffix" value=" none"/>
    </s:if>
    <s:elseif test="%{#outOfDateStatus == #attr.institutionSignedMta.status}">
        <c:set var="columnClassSuffix" value=" outdated"/>
    </s:elseif>
    <s:elseif test="%{#pendingStatus == #attr.institutionSignedMta.status}">
        <c:set var="columnClassSuffix" value=" pending"/>
    </s:elseif>

    <fmt:message key="default.date.format" var="dateFormat"/>
    <c:choose>
        <c:when test="${showRadioButtons == 'true'}">
            <display:column titleKey="signedMta.review.institutionList.institution" class="inst_col${columnClassSuffix}"
                headerClass="inst_col${columnClassSuffix}" sortProperty="NAME" sortable="true">
                <c:url var="reviewUrl" value="/admin/signedMta/review/list.action">
                    <c:param name="signedMta.id"><s:property value="%{signedMta.id}"/></c:param>
                    <c:param name="institution.id"><s:property value="%{#attr.institution.id}"/></c:param>
                    <c:param name="correctInstitution" value="false"/>
                </c:url>
                <c:set var="checked" value=""/>
                <s:if test="%{#attr.institution.id == institution.id}">
                    <c:set var="checked" value="checked"/>
                </s:if>
                <input type="radio" name="inst_replacement" id="inst${institution.id}" ${checked}
                    onclick="window.location='${reviewUrl}'" />
                <label class="inline" for="inst${institution.id}">${institution.name}</label>
            </display:column>
        </c:when>
        <c:otherwise>
            <display:column titleKey="mta.info.institutionList.institution" class="inst_col${columnClassSuffix}"
                headerClass="inst_col${columnClassSuffix}" property="name" sortProperty="NAME" sortable="true"/>
        </c:otherwise>
    </c:choose>
    <display:column titleKey="mta.info.institutionList.version"
        class="vers_col${columnClassSuffix}" headerClass="vers_col${columnClassSuffix}">
        <c:choose>
            <c:when test="${empty institutionSignedMta}">
                -
            </c:when>
            <c:otherwise>
                <fmt:formatDate pattern="${dateFormat}" value="${institutionSignedMta.originalMta.version}"/>
            </c:otherwise>
        </c:choose>
    </display:column>

    <display:column titleKey="mta.info.institutionList.status"
        class="stat_col${columnClassSuffix}" headerClass="stat_col${columnClassSuffix}">
        <c:choose>
            <c:when test="${empty institutionSignedMta}">
                -
            </c:when>
            <c:otherwise>
                <s:if test="%{#approvedStatus == #attr.institutionSignedMta.status}">
                    <img src="${currentImageUrl}" class="active_icon"
                        alt="${currentImageAlt}" title="${currentImageAlt}"/>
                </s:if>
                <fmt:message key="${institutionSignedMta.status.resourceKey}"/>
            </c:otherwise>
        </c:choose>
    </display:column>
</display:table>
