<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<tissuelocator:displaytagCommonProperties/>

<fmt:message key="displaytag.page" var="pageLabel"/>
<fmt:message key="displaytag.first" var="firstLabel"/>
<fmt:message key="displaytag.prev" var="prevLabel"/>
<fmt:message key="displaytag.next" var="nextLabel"/>
<fmt:message key="displaytag.last" var="lastLabel"/>

<display:setProperty name="paging.banner.no_items_found">
    <div style="background:#eee; padding:10px;">
        <div class="resultscount">0 {0} <fmt:message key="displaytag.found"/></div>
</display:setProperty>
<display:setProperty name="paging.banner.one_item_found">
    <div style="background:#eee; padding:10px;">
        <div class="resultscount"><strong>1-1</strong> of <strong>1</strong> {0}</div>
        <div class="currentpage"><strong>1</strong> of 1 ${pageLabel}</div>
</display:setProperty>
<display:setProperty name="paging.banner.all_items_found">
    <div style="background:#eee; padding:10px;">
        <div class="resultscount"><strong>1-{0}</strong> of <strong>{0}</strong> {1}</div>
        <div class="currentpage"><strong>1</strong> of 1 ${pageLabel}</div>
</display:setProperty>
<display:setProperty name="paging.banner.some_items_found">
    <div style="background:#eee; padding:10px;">
        <div class="resultscount"><strong>{2}-{3}</strong> of <strong>{0}</strong> {1}</div>
        <div class="currentpage"><strong>{4}</strong> of {5} <fmt:message key="displaytag.pages"/></div>
</display:setProperty>

<display:setProperty name="paging.banner.full">
        <div class="pagenav">
            <a href="{1}">|&lt; ${firstLabel}</a>
            <a href="{2}">&lt;&lt; ${prevLabel}</a>
            {0}
            <a href="{3}">${nextLabel} &gt;&gt;</a>
            <a href="{4}">${lastLabel} &gt;|</a>
        </div>
        <div class="perpage">
            <s:select name="pageSize" id="pageSize" value="%{pageSize}"
                onchange="submit()" theme="simple"
                list="#{'10':'10', '20':'20', '50':'50', '100':'100'}"
                title="%{getText('displaytag.pageSize.select')}"/>
            <fmt:message key="displaytag.perPage"/>
        </div>
        <div class="clear"></div>
    </div>
</display:setProperty>
<display:setProperty name="paging.banner.first">
        <div class="pagenav">
            <a href="#" class="disabled">|&lt; ${firstLabel}</a>
            <a href="#" class="disabled">&lt;&lt; ${prevLabel}</a>
            {0}
            <a href="{3}">${nextLabel} &gt;&gt;</a>
            <a href="{4}">${lastLabel} &gt;|</a>
        </div>
        <div class="perpage">
            <s:select name="pageSize" id="pageSize" value="%{pageSize}"
                onchange="submit()" theme="simple"
                list="#{'10':'10', '20':'20', '50':'50', '100':'100'}"
                title="%{getText('displaytag.pageSize.select')}"/>
            <fmt:message key="displaytag.perPage"/>
        </div>
        <div class="clear"></div>
    </div>
</display:setProperty>
<display:setProperty name="paging.banner.last">
        <div class="pagenav">
            <a href="{1}">|&lt; ${firstLabel}</a>
            <a href="{2}">&lt;&lt; ${prevLabel}</a>
            {0}
            <a href="#" class="disabled">${nextLabel} &gt;&gt;</a>
            <a href="#" class="disabled">${lastLabel} &gt;|</a>
        </div>
        <div class="perpage">
            <s:select name="pageSize" id="pageSize" value="%{pageSize}"
                onchange="submit()" theme="simple"
                list="#{'10':'10', '20':'20', '50':'50', '100':'100'}"
                title="%{getText('displaytag.pageSize.select')}"/>
            <fmt:message key="displaytag.perPage"/>
        </div>
        <div class="clear"></div>
    </div>
</display:setProperty>
<display:setProperty name="paging.banner.onepage">
        <div class="pagenav">
            <a href="#" class="disabled">|&lt; ${firstLabel}</a>
            <a href="#" class="disabled">&lt;&lt; ${prevLabel}</a>
            {0}
            <a href="#" class="disabled">${nextLabel} &gt;&gt;</a>
            <a href="#" class="disabled">${lastLabel} &gt;|</a>
        </div>
        <div class="perpage">
            <s:select name="pageSize" id="pageSize" value="%{pageSize}"
                onchange="submit()" theme="simple"
                list="#{'10':'10', '20':'20', '50':'50', '100':'100'}"
                title="%{getText('displaytag.pageSize.select')}"/>
            <fmt:message key="displaytag.perPage"/>
        </div>
        <div class="clear"></div>
    </div>
</display:setProperty>
