<%@ tag body-content="empty" %>
<%@ attribute name="fieldName" required="true" rtexprvalue="true" %>
<%@ attribute name="fieldId" required="true" rtexprvalue="true" %>
<%@ attribute name="searchFieldName" required="true" rtexprvalue="true" %>
<%@ attribute name="autocompleteUrl" required="true" rtexprvalue="true" %>
<%@ attribute name="labelKey" rtexprvalue="true" %>
<%@ attribute name="noteKey" %>
<%@ attribute name="required" %>
<%@ attribute name="onSelect" %>
<%@ attribute name="cssStyle" %>
<%@ attribute name="cssClass" %>
<%@ attribute name="tabIndex" %>
<%@ attribute name="onSearchTopics" %>
<%@ attribute name="onSelectTopics" %>
<%@ attribute name="onChangeTopics" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="/struts-jquery-tags" prefix="sj" %>

<c:if test="${!empty labelKey}">
    <label for="${fieldId}" class="label" style="font-style:normal;" >
        <fmt:message key="${labelKey}"/>:
        <c:if test="${required}">
            <span class="reqd"><fmt:message key="required.indicator"/></span>
        </c:if>
    </label>
</c:if>
<s:hidden name="%{#attr.fieldName}" id="%{#attr.fieldId + 'Hidden'}"/>
<s:url var="ajaxUrl" action="%{#attr.autocompleteUrl}" namespace="/"/>
<sj:autocompleter name="%{#attr.searchFieldName}" id="%{#attr.fieldId}"
    href="%{ajaxUrl}" delay="50" loadMinimumCount="0" parentTheme="simple"
    maxlength="254" cssStyle="%{#attr.cssStyle}" cssClass="%{#attr.cssClass}" tabindex="%{#attr.tabIndex}"
    onChangeTopics="%{#attr.onChangeTopics}" 
    onSelectTopics="%{#attr.onSelectTopics}"
    onSearchTopics="%{#attr.onSearchTopics}"/>
<c:url value="/images/ico_arrow_autocomplete.gif" var="downArrowSrc"/>
<img src="${downArrowSrc}" id="${fieldId}Arrow" style="margin:0px 0px -3px -18px">
<script type="text/javascript">
    $('#${fieldId}').bind('autocompleteselect', function(event, ui) {
    	if (ui.item.id != -1) {
    	    $('#${fieldId}Hidden').get(0).value = ui.item.id;
    	    ${onSelect}
    	} else {
    		ui.item.value = '';
    	}
    });

    var ${fieldId}MenuDisplayed = false;

    $('#${fieldId}').bind('autocompleteopen', function(event, ui) {
        ${fieldId}MenuDisplayed = true;
        var menu = $(this).data('autocomplete').menu;
        var toActivate = null;
        for (var i = 0; i < menu.element.children().length; i++) {
        	var opt = $(menu.element.children()[i]);
        	if (opt.data('item.autocomplete').id == -1) {
        		opt.addClass('autocomplete_empty_option');
        	} else if (toActivate == null) {
        		toActivate = opt;
        	}
        }
        menu.activate($.Event({type: 'mouseenter'}), toActivate);
    });

    $('#${fieldId}').bind('autocompleteclose', function(event, ui) {
        ${fieldId}MenuDisplayed = false;
        var autocompleteMenuItems = $(this).data('autocomplete').menu.element.children();
        if (autocompleteMenuItems.length == 1) {
            var optionData = autocompleteMenuItems.first().data('item.autocomplete');
            if (!$('#${fieldId}Hidden').val() && $('#${fieldId}').val().toLowerCase() == optionData.value.toLowerCase()) {
                $('#${fieldId}').get(0).value = optionData.value;
                $('#${fieldId}Hidden').get(0).value = optionData.id;
            }
        }
    });

    $('#${fieldId}').keydown(function(event) {
        if (event.keyCode == $.ui.keyCode.DOWN && $(this).val().length == 0 && !${fieldId}MenuDisplayed) {
            //display options if the user presses the down arrow in the field before typing anything
            $(this).autocomplete('search', ' ');
        } else if (event.keyCode == $.ui.keyCode.DELETE || event.keyCode == $.ui.keyCode.BACKSPACE) {
            //clear the hidden field if the user deletes anything from the text field
            $('#${fieldId}Hidden').get(0).value = null;
        }
    });

    $('#${fieldId}Arrow').click(function() {
        if (${fieldId}MenuDisplayed) {
            $('#${fieldId}').autocomplete('close');
        } else {
            var searchTerm = $('#${fieldId}').val();
            if (!searchTerm) {
                searchTerm = ' ';
            }
            $('#${fieldId}').autocomplete('search', searchTerm);
        }
        $('#${fieldId}').focus();
        return false;
    });

</script>
<c:if test="${!empty noteKey}">
    <div class="note"><fmt:message key="${noteKey}"/></div>
</c:if>
<s:fielderror cssClass="fielderror">
    <s:param><s:property value="%{#attr.searchFieldName}"/></s:param>
</s:fielderror>
