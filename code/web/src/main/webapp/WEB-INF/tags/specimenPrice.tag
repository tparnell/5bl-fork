<%@ tag body-content="empty" %>
<%@ attribute name="priceNegotiable" %>
<%@ attribute name="minimumPrice" required="true" %>
<%@ attribute name="maximumPrice" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${empty priceNegotiable}">
    <c:set var="priceNegotiable" value="${false}"/>
</c:if>
<c:choose>
    <c:when test="${priceNegotiable}">
        <fmt:message key="specimen.price.negotiable"/>
    </c:when>
    <c:otherwise>
        <fmt:formatNumber value="${minimumPrice}" type="currency"/>
        <c:if test="${minimumPrice != maximumPrice}">
        -
        <fmt:formatNumber value="${maximumPrice}" type="currency"/>
        </c:if>
    </c:otherwise>
</c:choose>