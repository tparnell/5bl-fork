<%@ tag body-content="empty" %>
<%@ attribute name="specificLineItems" required="true" type="java.util.Collection" rtexprvalue="true" %>
<%@ attribute name="generalLineItems" required="true" type="java.util.Collection" rtexprvalue="true" %>
<%@ attribute name="showOrderFulfillmentForms" required="true" rtexprvalue="true" %>
<%@ attribute name="showFinalPriceColumn" required="true" rtexprvalue="true" %>
<%@ attribute name="showReceiptQuality" rtexprvalue="true" %>
<%@ attribute name="cartPrefix" rtexprvalue="true" %>
<%@ attribute name="order" type="com.fiveamsolutions.tissuelocator.data.Shipment" rtexprvalue="true" %>
<%@ attribute name="showEmpty" %>
<%@ attribute name="wrapped" %>
<%@ attribute name="displayVotingResults" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set var="showSpecific" value="%{(#attr.showEmpty == 'true') || (#attr.specificLineItems.size() > 0) || ((#attr.generalLineItems.size() == 0) && (#attr.specificLineItems.size() == 0))}"/>
<s:set var="showGeneral" value="%{(#attr.showEmpty == 'true') || (#attr.generalLineItems.size() > 0)}"/>


<c:set var="finalPriceSum" value="${0.0}" scope="request"/>
<c:if test="${empty wrapped || wrapped == 'true'}">
    <div id="${cartPrefix}cart">
    <table class="data" id="aggregateLineItemTable">
</c:if>

<c:if test="${showSpecific}">
    <tissuelocator:aggregateLineItemTable
        requestLineItems="${specificLineItems}"
        showFinalPriceColumn="${showFinalPriceColumn}"
        showOrderFulfillmentForms="${showOrderFulfillmentForms}"
        showReceiptQuality="${showReceiptQuality}"
        cartPrefix="${cartPrefix}specific"
        order="${order}"
        lineItemPrefix="specific"
        emptyKey="shipment.cart.specific.noneSelected"
        totalKey="shipment.cart.specific.total"
        titleKey="aggregateCartTable.specificCart.title"
        displayVotingResults="${displayVotingResults}"/>
</c:if>

<c:if test="${showGeneral}">
    <tissuelocator:aggregateLineItemTable
        requestLineItems="${generalLineItems}"
        showFinalPriceColumn="${showFinalPriceColumn}"
        showOrderFulfillmentForms="${showOrderFulfillmentForms}"
        showReceiptQuality="${showReceiptQuality}"
        cartPrefix="${cartPrefix}general"
        order="${order}"
        lineItemPrefix="general"
        emptyKey="shipment.cart.general.noneSelected"
        totalKey="shipment.cart.general.total"
        titleKey="aggregateCartTable.generalCart.title"
        displayVotingResults="${displayVotingResults}"/>
</c:if>

<tissuelocator:aggregateCartTableFooter order="${order}" showOrderForms="${showOrderFulfillmentForms}"
    showFinalPriceColumn="${showFinalPriceColumn}" finalPriceTotal="${finalPriceSum}"
    showReceiptQuality="${showReceiptQuality}"/>

<c:if test="${empty wrapped || wrapped == 'true'}">
    </table>
    </div>
</c:if>
