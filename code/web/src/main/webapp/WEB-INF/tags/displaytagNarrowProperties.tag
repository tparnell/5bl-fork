<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<tissuelocator:displaytagCommonProperties/>

<fmt:message key="displaytag.page" var="pageLabel"/>
<fmt:message key="displaytag.first" var="firstLabel"/>
<fmt:message key="displaytag.prev" var="prevLabel"/>
<fmt:message key="displaytag.next" var="nextLabel"/>
<fmt:message key="displaytag.last" var="lastLabel"/>

<display:setProperty name="paging.banner.no_items_found">
    <div style="width:560px; border-top:1px solid #ccc;">
        <div style="padding-top:10px;text-align:center;">
            0 {0} <fmt:message key="displaytag.found"/> |
</display:setProperty>
<display:setProperty name="paging.banner.one_item_found">
    <div style="width:560px; border-top:1px solid #ccc;">
        <div style="padding-top:10px;text-align:center;">
            <strong>1-1</strong> of <strong>1</strong> {0} |
            <strong>1</strong> of 1 ${pageLabel} |
</display:setProperty>
<display:setProperty name="paging.banner.all_items_found">
    <div style="width:560px; border-top:1px solid #ccc;">
        <div style="padding-top:10px;text-align:center;">
            <strong>1-{0}</strong> of <strong>{0}</strong> {1} |
            <strong>1</strong> of 1 ${pageLabel} |
</display:setProperty>
<display:setProperty name="paging.banner.some_items_found">
    <div style="width:560px; border-top:1px solid #ccc;">
        <div style="padding-top:10px;text-align:center;">
            <strong>{2}-{3}</strong> of <strong>{0}</strong> {1} |
            <strong>{4}</strong> of {5} <fmt:message key="displaytag.pages"/> |
</display:setProperty>

<display:setProperty name="paging.banner.full">
            <s:select name="pageSize" id="pageSize" value="%{pageSize}"
                onchange="submit()" theme="simple"
                list="#{'10':'10', '20':'20', '50':'50', '100':'100'}"
                title="%{getText('displaytag.pageSize.select')}"/>
            <fmt:message key="displaytag.perPage"/>
        </div>
        <div class="line"></div>
        <div class="pagenav" style="padding-bottom:10px;width:560px">
            <a href="{1}">|&lt; ${firstLabel}</a>
            <a href="{2}">&lt;&lt; ${prevLabel}</a>
            {0}
            <a href="{3}">${nextLabel} &gt;&gt;</a>
            <a href="{4}">${lastLabel} &gt;|</a>
        </div>
        <div class="clear"></div>
    </div>
</display:setProperty>
<display:setProperty name="paging.banner.first">
            <s:select name="pageSize" id="pageSize" value="%{pageSize}"
                onchange="submit()" theme="simple"
                list="#{'10':'10', '20':'20', '50':'50', '100':'100'}"
                title="%{getText('displaytag.pageSize.select')}"/>
            <fmt:message key="displaytag.perPage"/>
        </div>
        <div class="line"></div>
        <div class="pagenav" style="padding-bottom:10px;width:560px">
            <a href="#" class="disabled">|&lt; ${firstLabel}</a>
            <a href="#" class="disabled">&lt;&lt; ${prevLabel}</a>
            {0}
            <a href="{3}">${nextLabel} &gt;&gt;</a>
            <a href="{4}">${lastLabel} &gt;|</a>
        </div>
        <div class="clear"></div>
    </div>
</display:setProperty>
<display:setProperty name="paging.banner.last">
            <s:select name="pageSize" id="pageSize" value="%{pageSize}"
                onchange="submit()" theme="simple"
                list="#{'10':'10', '20':'20', '50':'50', '100':'100'}"
                title="%{getText('displaytag.pageSize.select')}"/>
            <fmt:message key="displaytag.perPage"/>
        </div>
        <div class="line"></div>
        <div class="pagenav" style="padding-bottom:10px;width:560px">
            <a href="{1}">|&lt; ${firstLabel}</a>
            <a href="{2}">&lt;&lt; ${prevLabel}</a>
            {0}
            <a href="#" class="disabled">${nextLabel} &gt;&gt;</a>
            <a href="#" class="disabled">${lastLabel} &gt;|</a>
        </div>
        <div class="clear"></div>
    </div>
</display:setProperty>
<display:setProperty name="paging.banner.onepage">
            <s:select name="pageSize" id="pageSize" value="%{pageSize}"
                onchange="submit()" theme="simple"
                list="#{'10':'10', '20':'20', '50':'50', '100':'100'}"
                title="%{getText('displaytag.pageSize.select')}"/>
            <fmt:message key="displaytag.perPage"/>
        </div>
        <div class="line"></div>
        <div class="pagenav" style="padding-bottom:10px;width:560px">
            <a href="#" class="disabled">|&lt; ${firstLabel}</a>
            <a href="#" class="disabled">&lt;&lt; ${prevLabel}</a>
            {0}
            <a href="#" class="disabled">${nextLabel} &gt;&gt;</a>
            <a href="#" class="disabled">${lastLabel} &gt;|</a>
        </div>
        <div class="clear"></div>
    </div>
</display:setProperty>
