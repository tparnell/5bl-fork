<%@ tag body-content="empty" %>
<%@ attribute name="value" required="true" rtexprvalue="true"%>
<%@ attribute name="cssMaxWidth" required="true" %>
<%@ attribute name="isTableCell" required="true" rtexprvalue="true"%>
<%@ attribute name="maxWordLength" %>
<%@ attribute name="manual" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
  prefix="fn" %>

<%-- If this is a table cell, individual words will wrap, so we
     need to check the length of individual words --%>
<c:set var="truncate" value="${true}"/>
<c:if test="${empty maxWordLength}">
    <c:set var="maxWordLength" value="20"/>
</c:if>
<c:if test="${isTableCell}">
    <c:set var="truncate" value="${false}"/>
    <c:forEach var="str" items="${fn:split(value,' ')}">
        <c:if test="${fn:length(str) > maxWordLength}">
            <c:set var="truncate" value="${true}"/>
        </c:if>
    </c:forEach>
</c:if>
<c:choose>
    <c:when test="${truncate}">
        <c:choose>
            <c:when test="${manual eq 'true'}">
                <c:set var="truncated" value="${value}"/>
                <fmt:message var="append" key="text.truncate"/>
                <c:if test="${fn:length(value) > maxWordLength}">
                    <c:set var="truncated" value="${fn:substring(value,0,maxWordLength)}${append}"/>
                </c:if>
                ${truncated}
            </c:when>
            <c:otherwise>
                <div style="overflow:hidden; text-overflow: ellipsis; max-width: ${cssMaxWidth}" class="truncate ${cssClass}" title="${value}">
                    ${value}
                </div>
           </c:otherwise>
       </c:choose>
    </c:when>
    <c:otherwise>
        ${value}
    </c:otherwise>
</c:choose>


