<%@ tag body-content="empty" %>
<%@ attribute name="requestLineItems" required="true" type="java.util.Collection" rtexprvalue="true" %>
<%@ attribute name="titleKey" required="true" rtexprvalue="true" %>
<%@ attribute name="lineItemPrefix" required="true" rtexprvalue="true" %>
<%@ attribute name="removeUrl" required="true" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<tr id="${lineItemPrefix}_title">
    <td colspan="5">
        <h2 class="noline">
            <fmt:message key="${titleKey}"/>
        </h2>
    </td>
</tr>

<tr id="${lineItemPrefix}_header">
    <th><div><fmt:message key="aggregateCart.institution"/></div></th>
    <th><div><fmt:message key="aggregateCart.quantity"/></div></th>
    <th><div><fmt:message key="aggregateCart.criteria"/></div></th>
    <th><div><fmt:message key="aggregateCart.note"/></div></th>
    <th class="action"><div><fmt:message key="column.action"/></div></th>
</tr>

<c:if test="${empty requestLineItems}">
    <tr class="empty" id="${lineItemPrefix}_empty">
        <td colspan="5">
            <fmt:message key="specimenRequest.noSpecimensSelected"/>
        </td>
    </tr>
</c:if>

<c:forEach items="${requestLineItems}" var="lineItem" varStatus="lineItemStatus">
    <c:set var="rowClass" value=""/>
    <c:if test="${lineItemStatus.count % 2 == 1}">
        <c:set var="rowClass" value="odd"/>
    </c:if>

    <tr class="${rowClass}" id="${lineItemPrefix}_data${lineItemStatus.index}">
        <td>
            <tissuelocator:institutionName institution="${lineItem.institution}"/>
            <s:hidden name="%{#attr.lineItemPrefix}LineItemsArray[%{#attr.lineItemStatus.index}].institution"
                value="%{#attr.lineItem.institution.id}"/>
        </td>

        <td>
            <fmt:formatNumber value="${lineItem.quantity}"/>
            <s:hidden name="%{#attr.lineItemPrefix}LineItemsArray[%{#attr.lineItemStatus.index}].quantity"
                value="%{#attr.lineItem.quantity}"/>
        </td>

        <td>
            ${lineItem.criteria}
            <s:hidden name="%{#attr.lineItemPrefix}LineItemsArray[%{#attr.lineItemStatus.index}].criteria"
                value="%{#attr.lineItem.criteria}"/>
        </td>

        <td>
            <s:textarea name="%{#attr.lineItemPrefix}LineItemsArray[%{#attr.lineItemStatus.index}].note"
                value="%{#attr.lineItem.note}" cssStyle="width:100%" rows="3" required="false" theme="simple"
                title="%{getText('aggregateCart.note.field')}"/>
        </td>

        <td class="action">
            <tissuelocator:determinePersistenceType urlPrefix="${removeUrl}" urlEnding=".action"/>
            <c:url value="${outputUrl}" var="fullRemoveUrl">
                <c:param name="selection" value="${lineItemStatus.index}"/>
            </c:url>
            <a href="${fullRemoveUrl}" class="btn_fw_action_col">
                <fmt:message key="cart.remove"/>
            </a>
        </td>
    </tr>
</c:forEach>
