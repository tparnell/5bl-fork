<%@ tag body-content="empty" %>
<%@ attribute name="order" required="true" type="com.fiveamsolutions.tissuelocator.data.Shipment" %>
<%@ attribute name="showLabel" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<s:set var="trackingUrl" scope="page"
    value="%{#attr.order.shippingMethod.getFormattedTrackingUrl(#attr.order.trackingNumber)}"/>
<c:choose>
    <c:when test="${!empty trackingUrl}">
        <c:if test="${showLabel}">
            <label for="tn"><fmt:message key="shipment.trackingLink"/></label>
        </c:if>
        <a href="${trackingUrl}" target="externalTracking">${order.trackingNumber}</a>
    </c:when>
    <c:otherwise>
        <c:if test="${showLabel}">
            <label for="tn"><fmt:message key="shipment.trackingNumber"/></label>
        </c:if>
        <c:choose>
            <c:when test="${!empty order.trackingNumber}">${order.trackingNumber}</c:when>
            <c:otherwise>-</c:otherwise>
        </c:choose>
    </c:otherwise>
</c:choose>
