<%@ tag body-content="empty" %>
<%@ attribute name="searchParams" required="true" %>
<%@ attribute name="refreshAction" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>

<!--Filters-->

<c:if test="${(!empty searchParams and !startPopupOnLoad) || simpleSearchEnabled}">
    <a href="#" onclick="toggleVisibility('filters');toggleVisibility('criteria');" class="toggle">
        <fmt:message key="specimen.search.hide"/>
    </a>
</c:if>
<c:set var="filtersDisplay" value=""/>
<c:set var="criteriaDisplay" value="display:none"/>
<s:set var="hasErrors" value="%{hasErrors()}" scope="page"/>
<c:if test="${!empty searchParams and !startPopupOnLoad and !hasErrors}">
    <c:set var="filtersDisplay" value="display:none"/>
    <c:set var="criteriaDisplay" value=""/>
</c:if>

<div id="filters" style="${filtersDisplay}">
    <s:if test="%{simpleSearchEnabled}">
        <c:url var="refreshUrl" value="${refreshAction}">
            <c:param name="simpleSearch">${!simpleSearch}</c:param>
        </c:url>
        <script type="text/javascript">
            function refreshSearch() {
                window.location = '${refreshUrl}';
            }
        </script>
        <div class="alignright" style="margin-top:-1.4em;">
            <c:url var="simpleSearchInfoUrl" value="/info/popup/simpleSearchInfo.action"/>
            <s:checkbox theme="simple" id="simpleSearch" name="simpleSearch" onclick="refreshSearch()" cssStyle="vertical-align:middle;"/>
            <label class="inline" for="search_type">
                <fmt:message key="specimen.search.simple">
                    <fmt:param value="${simpleSearchInfoUrl}"/>
                </fmt:message>
            </label>
        </div>
    </s:if>
    <tissuelocator:messages/>
    <s:hidden name="page" value="1"/>

    <s:if test="%{tabbedSearchAndSectionedCategoriesViewsEnabled}">
        <s:iterator var="uiSection" value="%{uiSections}" status="rowStatus">
            <s:set var="rowNumber" value="%{#rowStatus.index + 1}"/>
            <s:set var="tab_content_id" value="%{'tab_' + #rowNumber + '_content'}"/>
            <div id="${tab_content_id}">
                <div class="box">
                    <table class="data tabular-layout">
                        <s:iterator var="uiSearchFieldCategory" value="%{#uiSection.uiSearchFieldCategories}">
                            <s:set var="searchFieldConfigurations" value="%{getSearchFieldConfigsForCategory(#uiSearchFieldCategory)}"/>
                            <s:set var="shouldDisplayCategory" value="false"/>
                            <s:iterator var="config" value="%{getSearchFieldConfigsForCategory(#uiSearchFieldCategory)}">
                                <s:if test="%{#config.getRequiredRole() == null}">
                                    <s:set var="shouldDisplayCategory" value="true" />
                                </s:if>
                                <s:else>
                                    <s:if test="%{#attr.TissueLocatorUser.isInRoleName(#config.getRequiredRole().getName())}">
                                      <s:set var="shouldDisplayCategory" value="true" />
                                  </s:if>
                                </s:else>
                            </s:iterator>
                            <s:if test="%{#shouldDisplayCategory}">
                              <tr>
                                  <th colspan="2">
                                      <div class="padme5"><s:property value="%{#uiSearchFieldCategory.categoryName}"/></div>
                                  </th>
                              </tr>
                            </s:if>
                            <tissuelocator:searchFieldConfigurations searchFieldConfigs="${searchFieldConfigurations}" rowIndex="0"/>
                        </s:iterator>
                    </table>
                </div>
            </div>
        </s:iterator>
        <div class="clear"></div>
    </s:if>
    <s:else>
        <div class="box">
            <table class="data tabular-layout" title="Search" summary="This table shows a list of search parameters.">
                <s:iterator value="%{uiSearchFieldCategories}" var="uiSearchFieldCategory">
                    <tr>
                        <th colspan="2">
                            <div class="padme5">
                                <s:set var="categoryName" value="%{#uiSearchFieldCategory.categoryName}"/>
                                ${categoryName}
                            </div>
                        </th>
                   </tr>
                   <s:set var="searchFieldConfigurations" value="%{getSearchFieldConfigsForCategory(#uiSearchFieldCategory)}"/>
                   <tissuelocator:searchFieldConfigurations
                        searchFieldConfigs="${searchFieldConfigurations}"
                        rowIndex="0"/>
              </s:iterator>
            </table>
        </div>
    </s:else>

    <div class="btn_bar">
        <s:submit value="%{getText('btn.search')}" name="btn_search" id="btn_search" cssClass="btn" theme="simple" title="%{getText('btn.search.button')}"/>
    </div>

</div>
<!--/Filters-->

<div id="criteria" style="${criteriaDisplay}">
    <div><strong><s:property value="%{criteriaString}" escape="false" /></strong></div>
</div>
