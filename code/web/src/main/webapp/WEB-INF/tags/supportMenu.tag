<%@ tag body-content="empty" %>
<%@ attribute name="selectedPage" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div id="secnav">
    <div class="box">
        <h3><fmt:message key="menu.researchSupport"/></h3>

        <fmt:message key="menu.researchSupport.resources" var="resources"/>
        <fmt:message key="menu.researchSupport.questions" var="questions"/>
        <fmt:message key="menu.researchSupport.letters" var="letters"/>
        <c:url value="/protected/support/letter/input.action" var="lettersUrl"/>
        <c:url value="/protected/support/question/input.action" var="questionsUrl"/>
        <c:url value="/support/resources.action" var="resourcesUrl"/>

        <ul>
            <li <c:if test="${selectedPage == 'resources'}"> class="selected"</c:if>>
                <a href="${resourcesUrl}">
                    ${resources}
                </a>
            </li>
            <li <c:if test="${selectedPage == 'questions'}"> class="selected"</c:if>>
                <a href="${questionsUrl}">
                    ${questions}
                </a>
            </li>
            <li <c:if test="${selectedPage == 'letters'}"> class="selected"</c:if>>
                <a href="${lettersUrl}">
                    ${letters}
                </a>
            </li>
        </ul>
    </div>
</div>