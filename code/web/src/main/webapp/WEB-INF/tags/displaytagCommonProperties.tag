<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>

<display:setProperty name="css.table" value="data" />
<display:setProperty name="css.th.sortable" value="" />
<display:setProperty name="css.th.sorted" value="" />
<display:setProperty name="css.th.ascending" value="sort_ascending" />
<display:setProperty name="css.th.descending" value="sort_descending" />
<display:setProperty name="css.tr.odd" value="odd"/>
<display:setProperty name="css.tr.even" value=""/>

<display:setProperty name="paging.banner.placement" value="bottom"/>
<display:setProperty name="paging.banner.item_name" value="Result"/>
<display:setProperty name="paging.banner.items_name" value="Results"/>

<display:setProperty name="paging.banner.page.selected">
    <a href="#" class="selected">{0}</a>
</display:setProperty>
<display:setProperty name="paging.banner.page.link">
    <a href="{1}">{0}</a>
</display:setProperty>
<display:setProperty name="paging.banner.page.separator" value=""/>

<display:setProperty name="export.banner">
    <div class="exportlinks">{0}</div>
</display:setProperty>
<display:setProperty name="export.types" value="csv"/>
<display:setProperty name="export.csv" value="true"/>
<display:setProperty name="export.csv.label">
    <span class="export csv"><fmt:message key="specimen.search.exportAll"/></span>
</display:setProperty>
<display:setProperty name="export.xml" value="false"/>
<display:setProperty name="export.pdf" value="false"/>
<display:setProperty name="export.excel" value="false"/>
