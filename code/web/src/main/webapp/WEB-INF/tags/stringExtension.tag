<%@ tag body-content="empty" %>
<%@ attribute name="extendableEntity" required="true" type="com.fiveamsolutions.dynamicextensions.ExtendableEntity" %>
<%@ attribute name="fieldDefinition" required="true" type="com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition" %>
<%@ attribute name="layout" required="true" %>
<%@ attribute name="mode" required="true" %>
<%@ attribute name="propertyPath" %>
<%@ attribute name="wrapperClass" %>
<%@ attribute name="wrapperStyle" %>
<%@ attribute name="displayUnknown" %>
<%@ attribute name="helpUrlBase" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set var="dynamicFieldName" value="%{#attr.propertyPath + '.customProperties[\\'' + #attr.fieldDefinition.fieldName + '\\']'}"/>
<fmt:message key="unknown.value" var="unknownValue"/>

    <s:if test="%{#attr.layout == 'rowLayout'}">
        <tr>
            <td class="label" scope="row">
                <label>
                    <s:property value="%{#attr.fieldDefinition.fieldDisplayName}"/>
                    <s:if test="%{#attr.mode == 'edit'}">
                        <s:if test="%{!nullable || nullableExtensionDeciderClass != null}">
                            <span class="reqd">
                                <fmt:message key="required.indicator"/>
                            </span>
                        </s:if>                    
                    </s:if>
                </label>
            </td>
            <td class="value">
    </s:if>
    <s:else>
        <div class="${wrapperClass}" style="${wrapperStyle}">
    </s:else>
    <s:if test="%{#attr.mode == 'view'}">
        <s:if test="%{#attr.layout == 'boxLayout'}">
            <label>${fieldDefinition.fieldDisplayName}</label>
            <div class="value">
        </s:if>
        <s:if test="%{(#attr.displayUnknown == 'true') && (#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName) == null)}">
            ${unknownValue}
        </s:if>
        <s:else>
            <s:property value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName)}"/>
        </s:else>
        <s:if test="%{#attr.layout == 'boxLayout'}">
            </div>
        </s:if>        
    </s:if>
    <s:if test="%{#attr.mode == 'edit'}">
        <s:if test="%{#attr.layout == 'rowLayout'}">
            <s:textfield id="%{#dynamicFieldName}"
                name="%{#dynamicFieldName}"
                value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName)}"
                size="30"
                maxlength="%{maxLength}"
                cssStyle="width:280px"
                required="%{!nullable || nullableExtensionDeciderClass != null}"
                requiredposition="right"/>
        </s:if>
        <s:if test="%{#attr.layout == 'boxLayout'}">
            <tissuelocator:helpLabel helpUrlBase="${helpUrlBase}" labelFor="${dynamicFieldName}"
                fieldDisplayName="${fieldDefinition.fieldDisplayName}" fieldName="${fieldDefinition.fieldName}"
                required="${!fieldDefinition.nullable || fieldDefinition.nullableExtensionDeciderClass != null}"/>
            <s:textfield id="%{#dynamicFieldName}"
                name="%{#dynamicFieldName}"
                value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName)}"
                size="30"
                maxlength="%{maxLength}"
                cssStyle="width:280px"
                required="%{!nullable || nullableExtensionDeciderClass != null}"
                requiredposition="right"/>
        </s:if>         
        <s:fielderror cssClass="fielderror">
            <s:param value="%{#attr.propertyPath + '.' + #attr.fieldDefinition.fieldDisplayName}"/>
        </s:fielderror>
        <s:fielderror cssClass="fielderror">
            <s:param value="%{#attr.fieldDefinition.fieldDisplayName}"/>
        </s:fielderror>
    </s:if>
    <s:if test="%{#attr.layout == 'rowLayout'}">
            </td>
        </tr>
    </s:if>
    <s:else>
        </div>
    </s:else>
