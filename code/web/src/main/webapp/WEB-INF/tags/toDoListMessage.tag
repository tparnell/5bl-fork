<%@ tag body-content="empty" %>
<%@ attribute name="count" required="true" %>
<%@ attribute name="messageKeyBase" required="true" %>
<%@ attribute name="url" required="true" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<s:if test="%{#attr.count > 0}">
    <p class="info">
        <a href="${url}" class="ico_requests">
            <s:if test="%{#attr.count == 1}">
                <fmt:message key="${messageKeyBase}.single"/>
            </s:if>
            <s:else>
                <fmt:message key="${messageKeyBase}.multiple">
                    <fmt:param>
                        <s:property value="%{#attr.count}"/>
                    </fmt:param>
                </fmt:message>
            </s:else>
        </a>
    </p>
</s:if>

