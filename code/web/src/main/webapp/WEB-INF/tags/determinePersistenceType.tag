<%@ tag body-content="empty" %>
<%@ attribute name="urlPrefix" required="true" rtexprvalue="true" %>
<%@ attribute name="urlEnding" required="true" %>
<%@ attribute name="asUrl" required="false" %>
<%@ attribute name="outputUrl" required="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<c:choose>
    <c:when test="${not empty specimenCart && not empty specimenCart.id}">
        <c:set scope="request" var="outputUrl" value="${urlPrefix}${'Db'}${urlEnding}"></c:set>
    </c:when>
    <c:otherwise>
        <c:set scope="request" var="outputUrl" value="${urlPrefix}${'Session'}${urlEnding}"></c:set>
    </c:otherwise>
</c:choose>
<c:if test="${not empty asUrl && asUrl}">
    <c:url scope="request" var="outputUrl" value="${outputUrl}"/>
</c:if>
