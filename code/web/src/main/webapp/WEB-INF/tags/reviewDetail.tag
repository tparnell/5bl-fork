<%@ tag body-content="empty" %>
<%@ attribute name="vote" required="true" type="com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote" rtexprvalue="true"%>
<%@ attribute name="titleKey" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${!empty vote}">
    <h2 class="noline">
        <fmt:message key="${titleKey}"/>
    </h2>

    <c:if test="${!empty vote.user}">
        <div class="formcol">
            <label for="reviewer"><fmt:message key="shipment.reviewDetails.reviewer"/></label>
            ${vote.user.lastName}, ${vote.user.firstName}
        </div>
    </c:if>

    <c:if test="${!empty vote.vote}">
        <div class="formcol">
            <label for="reviewVote"><fmt:message key="shipment.reviewDetails.vote"/></label>
            <fmt:message key="${vote.vote.status.resourceKey}"/>
        </div>
    </c:if>

    <c:if test="${!empty vote.date}">
        <div class="formcol">
            <label for="reviewDate"><fmt:message key="shipment.reviewDetails.date"/></label>
            <fmt:message key="default.datetime.format" var="dateTimeFormat"/>
            <fmt:formatDate value="${vote.date}" pattern="${dateTimeFormat}"/>
        </div>
    </c:if>

    <c:if test="${!empty vote.comment}">
        <div class="formcol_wide">
            <label for="reviewComment"><fmt:message key="shipment.reviewDetails.comment"/></label>
            ${vote.comment}
        </div>
    </c:if>

    <div class="clear"></div>
</c:if>
