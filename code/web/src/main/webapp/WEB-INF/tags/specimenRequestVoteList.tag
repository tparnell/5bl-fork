<%@ tag body-content="empty" %>
<%@ attribute name="votes" required="true" type="java.util.Collection" %>
<%@ attribute name="titleKey" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<s:set name="approvedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.Vote@APPROVE}" />
<s:set name="deniedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.Vote@DENY}" />
<fmt:message key="default.date.format" var="dateTimeFormat"/>

<c:if test="${!empty votes}">
    <h2 class="formtop"><fmt:message key="${titleKey}"/></h2>

    <div class="topcomment">
        <c:forEach items="${votes}" var="vote">
            <c:if test="${not empty vote.vote}">
                <c:set var="decisionClass" value=""/>
                <s:if test="%{#approvedEnum == #attr.vote.vote}">
                    <c:set var="decisionClass" value="approved"/>
                </s:if>
                <s:elseif test="%{#deniedEnum == #attr.vote.vote}">
                    <c:set var="decisionClass" value="denied"/>
                </s:elseif>
                <h3 class="${decisionClass}">
                    ${vote.institution.name}:
                    <fmt:message key="specimenRequest.vote.vote.label"/>
                    <fmt:message key="${vote.vote.status.resourceKey}"/>
                </h3>
                <c:if test="${!empty vote.comment}">
                    <p class="comment">
                        <strong>
                            ${vote.institution.name}
                            <fmt:message key="specimenRequest.vote.comment.label"/>
                        </strong>
                        ${vote.comment}
                    </p>
                </c:if>
            </c:if>
        </c:forEach>
    </div>
    <br />
</c:if>
