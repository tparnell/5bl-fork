<%@ tag body-content="empty" %>
<%@ attribute name="showRequired" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<style type="text/css">
    OL.lower-alpha LI
    { list-style-type:lower-alpha; font-size:90%; padding:0 0 8px 0px; margin-left: -18px}
</style>

<label for="pi_cert">
    <fmt:message key="specimenRequest.investigator.certified.declare.text"/><c:if test="${showRequired}">&nbsp;<span class="reqd"><fmt:message key="required.indicator"/></span></c:if>
</label>

<%-- NOTE: Specifying style information for both the OL and the LI in this case results in buggy IE behavior,
and all style info for the LI will be ignored (regardless of whether the styles overlap/inherit). So, OL
style is specified in a DIV. --%>
<div style="max-width: 600px; margin-top: 8px;">
    <ol class="lower-alpha normal" id="pi_cert" style="margin-bottom: 0px">
        <li><fmt:message key="specimenRequest.investigator.certified.suspend.text"/></li>
        <li><fmt:message key="specimenRequest.investigator.certified.sanction.text"/></li>
        <li><fmt:message key="specimenRequest.investigator.certified.convict.text"/></li>
    </ol>
</div>

