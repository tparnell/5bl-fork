<%@ tag body-content="empty" %>
<%@ attribute name="fieldName" required="true" rtexprvalue="true" %>
<%@ attribute name="fieldDisplayName" required="true" rtexprvalue="true" %>
<%@ attribute name="helpUrlBase" required="true" rtexprvalue="true" %>
<%@ attribute name="labelFor" required="true" rtexprvalue="true" %>
<%@ attribute name="required" required="true" rtexprvalue="true" %>
<%@ attribute name="labelClass" %>
<%@ attribute name="displayAsLink" required="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:if test="%{helpConfig.getFieldHelpConfig(#attr.fieldName)}">
    <c:url var="helpUrl" value="${helpUrlBase}">
        <c:param name="helpFieldName">${fieldName}</c:param>
        <c:param name="helpFieldDisplayName">${fieldDisplayName}</c:param>
    </c:url>
    <c:if test="${not empty asUrl && asUrl}">
    <c:url scope="request" var="outputUrl" value="${outputUrl}"/>
    </c:if>
    
    <label for="${labelFor}" class="${labelClass}">
        <c:choose>
            <c:when test="${not empty displayAsLink && displayAsLink}">
                <a id="aid.${fieldName}" class="submodal-550-250" href="${helpUrl}">
                    <span class="info">${fieldDisplayName}</span>
                </a>
            </c:when>
            <c:otherwise>
                ${fieldDisplayName}
                <a id="aid.${fieldName}" class="submodal-550-250" href="${helpUrl}">
                    <span class="info icon_only_padding"></span>
                </a>
            </c:otherwise>
        </c:choose>
        <c:if test="${required}"><span class="reqd"><fmt:message key="required.indicator"/></span></c:if>
    </label>
</s:if>
<s:else>
    <label for="${labelFor}" class="${labelClass}">${fieldDisplayName}<c:if test="${required}"><span class="reqd"><fmt:message key="required.indicator"/></span></c:if></label>
</s:else>