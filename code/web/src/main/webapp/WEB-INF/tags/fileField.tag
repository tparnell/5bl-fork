<%@ tag body-content="empty" %>
<%@ attribute name="file" type="com.fiveamsolutions.tissuelocator.data.TissueLocatorFile" rtexprvalue="true" %>
<%@ attribute name="id" required="true" %>
<%@ attribute name="name" required="true" %>
<%@ attribute name="objectProperty" required="true" rtexprvalue="true" %>
<%@ attribute name="labelKey" required="true" rtexprvalue="true" %>
<%@ attribute name="required" required="true" rtexprvalue="true" %>
<%@ attribute name="fieldSize" required="true" %>
<%@ attribute name="unsavedUrlBase" rtexprvalue="true" %>
<%@ attribute name="helpFieldName" rtexprvalue="true" %>
<%@ attribute name="helpUrlBase" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<div class="attachments">
    <c:choose>
        <c:when test="${!empty helpFieldName}">
            <fmt:message key="${labelKey}" var="helpLabelDisplayName"/>
            <tissuelocator:helpLabel helpUrlBase="${helpUrlBase}" labelFor="${id}"
                fieldDisplayName="${helpLabelDisplayName}" fieldName="${helpFieldName}" required="${required}"/>
        </c:when>
        <c:otherwise>
            <label for="${id}" class="label" style="font-style:normal;">
                <fmt:message key="${labelKey}"/>
                <c:if test="${required}">
                    <span class="reqd"><fmt:message key="required.indicator"/></span>
                </c:if>
            </label>
        </c:otherwise>
    </c:choose>

    <div class="large_doc">
        <c:choose>
            <c:when test="${empty file}">
                <s:file name="%{#attr.name}" id="%{#attr.id}" size="%{#attr.fieldSize}"/>
                <s:fielderror cssClass="fielderror">
                    <s:param>${objectProperty}</s:param>
                </s:fielderror>

            </c:when>
            <c:otherwise>
                 <div id="new${id}" style="display:none">
                    <s:file name="%{#attr.name}" id="%{#attr.id}" size="%{#attr.fieldSize}" theme="simple"/>
                    <s:fielderror cssClass="fielderror">
                        <s:param>${objectProperty}</s:param>
                    </s:fielderror>
                    <fmt:message key="specimenRequest.keepAttachment" var="cancelLinkTitle"/>
                    <a class="delete" id="cancel${id}" title="${cancelLinkTitle}" href="#"
                        onclick="$('#new${id}').hide();$('#existing${id}').show();$('#${id}').val('');return false">
                        <fmt:message key="specimenRequest.cancel"/>
                    </a>
                </div>
                 <div id="existing${id}">
                    <c:choose>
                        <c:when test="${file.lob.id == null}">
                            <tissuelocator:determinePersistenceType urlPrefix="${unsavedUrlBase}" urlEnding=".action"/>
                            <c:url value="${outputUrl}" var="downloadFileUrl"/>
                        </c:when>
                        <c:otherwise>
                            <c:url value="/protected/downloadFile.action" var="downloadFileUrl">
                                <c:param name="file.id" value="${file.lob.id}"/>
                                <c:param name="fileName" value="${file.name}"/>
                                <c:param name="contentType" value="${file.contentType}"/>
                            </c:url>
                        </c:otherwise>
                     </c:choose>
                     <a href="${downloadFileUrl}" class="doc_download">
                        ${file.name}
                     </a>
                    <fmt:message key="specimenRequest.deleteAttachment" var="changeLinkTitle"/>
                    <a class="delete" id="change${id}" title="${changeLinkTitle}" href="#"
                        onclick="$('#new${id}').show();$('#existing${id}').hide();$('#${id}').val('');return false">
                        <fmt:message key="specimenRequest.change"/>
                    </a>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
