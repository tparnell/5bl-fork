<%@ tag body-content="empty" %>
<%@ attribute name="showCheckboxes" required="true" %>
<%@ attribute name="showEditButtons" required="true" %>
<%@ attribute name="exportEnabled" required="true" %>
<%@ attribute name="formId" %>
<%@ attribute name="checkboxProperty" %>
<%@ attribute name="listAction" %>
<%@ attribute name="viewAction" required="true" %>
<%@ attribute name="showStatus" required="true" %>
<%@ attribute name="showFee" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>

<script type="text/javascript">
    var institutionAmountFieldMap = {};
    var fieldCountMap = {};
    var allocationErrorMessages = {};
    allocationErrorMessages['INVALID_TYPE'] = '<fmt:message key="aggregateCart.add.error.invalid"/>';
    allocationErrorMessages['MAX'] = '<fmt:message key="aggregateCart.add.error.max"/>';
    $(document).ready(function () {
        for (var key in institutionAmountFieldMap) {
            $(institutionAmountFieldMap[key]).keyup(function() {
                updateInstitutionRequest(this, fieldCountMap, institutionAmountFieldMap, allocationErrorMessages);
            });
        }
    });
</script>

<c:if test="${not empty institutionsBelowMinimumResultsCount}">
    <div>
        <p class="important">
            <fmt:message key="aggregateSearch.results.belowMinimum.1"/><br/>
            <c:forEach var="institution" items="${institutionsBelowMinimumResultsCount}">
                <tissuelocator:institutionName institution="${institution}"/>
                <br/>
            </c:forEach>
            <fmt:message key="aggregateSearch.results.belowMinimum.2"/>
        </p>
    </div>
    <div class="clear"><br/></div>
</c:if>

<s:set var="criteriaString" value="%{filteredCriteriaString}"/>
<s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
<fmt:message key="default.datetime.format" var="datetimeFormat"/>
<display:table name="objects" requestURI="${listAction}"
               uid="specimen" pagesize="${tablePageSize}" sort="external" export="${exportEnabled}">

    <tissuelocator:displaytagProperties/>
    <display:setProperty name="export.csv.filename" value="specimens.csv"/>
    <display:setProperty name="export.csv.include_header" value="true"/>

    <c:if test="${showCheckboxes}">
        <fmt:message key="specimen.selectAll" var="selectAllCheckbox"/>
        <display:column title="<span style='padding:7px'><input type='checkbox' onclick='selectAllSpecimens()' title='${selectAllCheckbox}' id='selectall'></span>" media="html">
            <s:checkbox name="%{#attr.checkboxProperty}" theme="simple" fieldValue="%{#attr.specimen.id}" title="%{getText('specimen.search.select')}" />
        </display:column>
    </c:if>

    <display:column titleKey="specimen.storageInstitution" sortable="true"
            decorator="com.fiveamsolutions.tissuelocator.web.decorator.NewlineRemovalDecorator">
                <tissuelocator:institutionName institution="${specimen.institution}"/>
   </display:column>


    <display:column titleKey="specimen.availableQuantity" sortable="true"
        sortProperty="AVAILABLE_QUANTITY_UNITS,AVAILABLE_QUANTITY"
        decorator="com.fiveamsolutions.tissuelocator.web.decorator.NewlineRemovalDecorator">
        <c:if test="${!empty specimen.count}">
            <fmt:formatNumber value="${specimen.count}" maxFractionDigits="2"/>
        </c:if>
    </display:column>

    <c:if test="${not empty objects.list}">
        <fmt:message key="specimen.search.quantityRequested.label" var="quantityRequestedTitle"/>
        <display:column title="<div>${quantityRequestedTitle}</div>" media="html">
            <s:hidden name="aggregateSelections[%{#attr.specimen_rowNum - 1}].institution" value="%{#attr.specimen.institution.id}"/>
            <s:hidden name="aggregateSelections[%{#attr.specimen_rowNum - 1}].criteria" value="%{#criteriaString}"/>

            <s:textfield id="%{#attr.specimen.institution.id + '_amountRequested'}" theme="simple"
                name="aggregateSelections[%{#attr.specimen_rowNum - 1}].quantity"/>
        </display:column>

        <fmt:message key="aggregateCart.specimenUseRestrictions.title" var="restrictionsTitle"/>
        <display:column title="<div>${restrictionsTitle}</div>" media="html">
            <s:if test="%{displayUsageRestrictions}">
                <c:if test="${not empty specimen.institution.specimenUseRestrictions}">
                    <c:url value="/info/popup/specimenUseRestrictions.action" var="usageUrl">
                        <c:param name="object.id">${specimen.institution.id}</c:param>
                    </c:url>
                    <a class="submodal-550-250" href="${usageUrl}"><span class="info"><fmt:message key="aggregateCart.specimenUseRestrictions"/></span></a>
                </c:if>
            </s:if>
        </display:column>

    </c:if>

</display:table>

<c:forEach var="specimen" items="${objects.list}">
    <script type="text/javascript">
        institutionAmountFieldMap[${specimen.institution.id}] = $('#${specimen.institution.id}_amountRequested').get(0);
        fieldCountMap['${specimen.institution.id}_amountRequested'] = ${specimen.count};
    </script>
</c:forEach>

<c:if test="${showCheckboxes}">
    <script>
        function selectAllSpecimens() {
            selectAllCheckboxes('${formId}', 'selectall');
        }
    </script>
</c:if>
