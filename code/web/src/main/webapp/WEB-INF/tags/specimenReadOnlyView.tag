<%@ tag body-content="empty" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>

<s:if test="%{tabbedSearchAndSectionedCategoriesViewsEnabled}">
	<s:iterator var="uiSection" value="%{uiSections}">
	    <h2><s:property value="%{#uiSection.sectionName}"/></h2>
	    <s:iterator var="uiDynamicFieldCategory" value="%{#uiSection.uiDynamicFieldCategories}">
	        <table class="data tabular-layout">
	            <thead>
	                <tr>
	                    <th colspan="2">
	                        <div class="padme5">
	                            <s:property value="%{#uiDynamicFieldCategory.categoryName}"/>
	                        </div>
	                    </th>
	                </tr>
	            </thead>
	        </table>
	        <tissuelocator:displayUiDynamicCategoryFields uiDynamicFieldCategory="${#uiDynamicFieldCategory}"/>
	    </s:iterator> 
	</s:iterator>
</s:if>
<s:else>
    <s:iterator value="%{uiDynamicFieldCategories.values()}" var="category">
	    <h2>${category.categoryName}</h2>
	    <tissuelocator:displayUiDynamicCategoryFields uiDynamicFieldCategory="${category}"/>
	</s:iterator>
</s:else>

<req:isUserInRole role="specimenAdmin">
    <h2><fmt:message key="specimen.statusTransitionHistory"/></h2>
    <table class="data tabular-layout">
        <fmt:message key="default.date.format" var="dateFormat"/>
        <tr>
            <td class="label" scope="row">
                <label><fmt:message key="specimen.statusTransitionHistory"/></label>
            </td>
            <td class="value">
                <c:choose>
                    <c:when test="${empty object.statusTransitionHistory}">
                        <span><fmt:message key="${object.status.resourceKey}"/></span>
                        <span><fmt:formatDate pattern="${dateFormat}" value="${object.createdDate}"/></span>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${object.statusTransitionHistory}" var="transition" varStatus="loopStatus">
                            <c:if test="${loopStatus.first}">
                                <span><fmt:message key="${transition.previousStatus.resourceKey}"/></span>
                                <span><fmt:formatDate pattern="${dateFormat}" value="${object.createdDate}"/></span>
                                <br/>
                            </c:if>
                            <span><fmt:message key="${transition.newStatus.resourceKey}"/></span>
                            <span><fmt:formatDate pattern="${dateFormat}" value="${transition.transitionDate}"/></span>
                            <c:if test="${not loopStatus.last}"><br/></c:if>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>
</req:isUserInRole>

<h2><fmt:message key="specimen.specimenUseRestrictions"/></h2>
<tissuelocator:specimenUseRestrictions institution="${object.externalIdAssigner}"
    emptyResourceKey="specimen.specimenUseRestrictions.default"/>
