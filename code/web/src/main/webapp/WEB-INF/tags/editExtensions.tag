<%@ tag body-content="empty" %>
<%@ attribute name="dynamicFieldDefinitions" required="true" type="java.util.List" %>
<%@ attribute name="extendableEntity" required="true" type="com.fiveamsolutions.dynamicextensions.ExtendableEntity" %>
<%@ attribute name="layout" required="true" %>
<%@ attribute name="propertyPath" required="true" %>
<%@ attribute name="wrapperClass" %>
<%@ attribute name="wrapperStyle" %>
<%@ attribute name="helpUrlBase" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set var="booleanClass" value="%{@com.fiveamsolutions.dynamicextensions.BooleanDynamicFieldDefinition@class.name}" />
<s:set var="stringClass" value="%{@com.fiveamsolutions.dynamicextensions.StringDynamicFieldDefinition@class.name}" />
<s:set name="integerClass" value="%{@com.fiveamsolutions.dynamicextensions.IntegerDynamicFieldDefinition@class.name}" />
<s:set name="decimalClass" value="%{@com.fiveamsolutions.dynamicextensions.DecimalDynamicFieldDefinition@class.name}" />
<s:set name="controlledVocabularyClass" value="%{@com.fiveamsolutions.dynamicextensions.ControlledVocabularyDynamicFieldDefinition@class.name}" />
<s:set name="fileClass" value="%{@com.fiveamsolutions.tissuelocator.data.extension.FileDynamicFieldDefinition@class.name}" />

<s:iterator value="%{#attr.dynamicFieldDefinitions}">
    <s:set var="fieldDefinition" value="%{top}" scope="request"/>
    <s:if test="%{class.name == #booleanClass}">
        <tissuelocator:booleanExtension
            fieldDefinition="${fieldDefinition}"
            layout="${layout}"            
            mode="edit"
            extendableEntity="${extendableEntity}"
            propertyPath="${propertyPath}"            
            wrapperClass="${wrapperClass}"
            wrapperStyle="${wrapperStyle}"/>
    </s:if>
    <s:elseif test="%{class.name == #stringClass}">
        <tissuelocator:stringExtension
            fieldDefinition="${fieldDefinition}"
            layout="${layout}"
            mode="edit"
            helpUrlBase="${helpUrlBase}"
            extendableEntity="${extendableEntity}"
            propertyPath="${propertyPath}"
            wrapperClass="${wrapperClass}"
            wrapperStyle="${wrapperStyle}"/>
    </s:elseif>
    <s:elseif test="%{class.name == #integerClass}">
        <tissuelocator:integerExtension
            fieldDefinition="${fieldDefinition}"
            layout="${layout}"
            mode="edit"
            helpUrlBase="${helpUrlBase}"
            extendableEntity="${extendableEntity}"
            propertyPath="${propertyPath}"
            wrapperClass="${wrapperClass}"
            wrapperStyle="${wrapperStyle}"/>
    </s:elseif>
    <s:elseif test="%{class.name == #decimalClass}">
        <tissuelocator:decimalExtension
            fieldDefinition="${fieldDefinition}"
            layout="${layout}"
            mode="edit"
            helpUrlBase="${helpUrlBase}"
            extendableEntity="${extendableEntity}"
            propertyPath="${propertyPath}"
            wrapperClass="${wrapperClass}"
            wrapperStyle="${wrapperStyle}"/>
    </s:elseif>
    <s:elseif test="%{class.name == #controlledVocabularyClass}">
        <tissuelocator:controlledVocabularyExtension
            fieldDefinition="${fieldDefinition}"
            layout="${layout}"
            mode="edit"
            helpUrlBase="${helpUrlBase}"
            extendableEntity="${extendableEntity}"
            propertyPath="${propertyPath}"
            wrapperClass="${wrapperClass}"
            wrapperStyle="${wrapperStyle}"/>
    </s:elseif>
    <s:elseif test="%{class.name == #fileClass}">
        <tissuelocator:fileExtension
            fieldDefinition="${fieldDefinition}"
            layout="${layout}"
            mode="edit"
            helpUrlBase="${helpUrlBase}"
            extendableEntity="${extendableEntity}"
            propertyPath="${propertyPath}"
            wrapperClass="${wrapperClass}"
            wrapperStyle="${wrapperStyle}"/>
    </s:elseif>
</s:iterator>