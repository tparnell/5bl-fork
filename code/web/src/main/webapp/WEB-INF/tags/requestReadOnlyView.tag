<%@ tag body-content="empty" %>
<%@ attribute name="specimenRequest" required="true" type="com.fiveamsolutions.tissuelocator.data.SpecimenRequest" rtexprvalue="true" %>
<%@ attribute name="showCart" required="true" rtexprvalue="true" %>
<%@ attribute name="displayVotingResults" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set name="yesEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@YES}" />
<s:set name="noEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@NO}" />
<s:set name="naEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@NOT_APPLICABLE}" />
<s:set var="yesFundedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.FundingStatus@FUNDED}"/>
<s:set var="notFundedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.FundingStatus@NOT_FUNDED}"/>
<s:set var="pendingFundedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.FundingStatus@PENDING}"/>

<s:set var="yesFundedString" value="%{#yesFundedEnum.toString()}"/>
<s:set var="pendingFundedString" value="%{#pendingFundedEnum.toString()}"/>
<s:set var="notFundedString" value="%{#notFundedEnum.toString()}"/>

<c:if test="${showCart}">
    <s:if test="%{displayAggregateSearchResults}">

        <s:set var="specificLineItems" value="%{specificLineItems}"/>
        <s:set var="generalLineItems" value="%{generalLineItems}"/>
        <tissuelocator:aggregateCartTable specificLineItems="${specificLineItems}" generalLineItems="${generalLineItems}" showFinalPriceColumn="false" showOrderFulfillmentForms="false"
            cartPrefix="aggregate" displayVotingResults="${displayVotingResults}"/>
    </s:if>
    <s:else>
        <tissuelocator:cartTable showFinalPriceColumn="false" requestLineItems="${specimenRequest.lineItems}"
            showOrderFulfillmentForms="false" showSpecimenLinks="true"/>
    </s:else>
    <br />
</c:if>

<c:if test="${!empty specimenRequest.prospectiveCollectionNotes}">
    <h2 class="formtop">
        <a href="javascript:;" onclick="toggleVisibility('pcNotes');" class="toggle">
            <fmt:message key="specimenRequest.prospectiveCollectionNotes"/>
        </a>
    </h2>
    <div id="pcNotes">
        ${specimenRequest.prospectiveCollectionNotes}
        <br/>
        <br/>
    </div>
    <div class="clear"></div>
</c:if>

<h2 class="formtop">
    <a href="javascript:;" onclick="toggleVisibility('requestform');" class="toggle">
        <fmt:message key="specimenRequest.edit.title"/>
    </a>
</h2>

<div id="requestform">

    <c:if test="${!empty specimenRequest.revisionComment}">
        <h2 class="noline_top"><fmt:message key="specimenRequest.revisionComment"/></h2>
        <div class="revision_comments">
            <label for="revisionComment"><fmt:message key="specimenRequest.revisionComment"/></label>
            ${specimenRequest.revisionComment}
        </div>
    </c:if>

    <c:set var="sectionCount" value="${1}"/>
    <h2 class="noline_top">
        <a href="javascript:;" onclick="toggleVisibility('study_details');" class="toggle">
            <fmt:message key="specimenRequest.study.title">
                <fmt:param value="${sectionCount}"/>
            </fmt:message>
        </a>
    </h2>

    <div id="study_details">

        <div class="formcol" style="width:300px">
            <label for="pi"><fmt:message key="specimenRequest.study.protocolTitle"/></label>
            ${specimenRequest.study.protocolTitle}

            <div class="clear" style="height:5px;"></div>

            <label for="preliminary">
                <fmt:message key="specimenRequest.study.preliminary"/>
            </label>
            <c:set var="checkClass" value=""/>
            <s:set var="preliminaryLabelText" value="%{''}"/>
            <c:choose>
                <c:when test="${empty specimenRequest.study.preliminary}">
                    <s:set var="preliminaryLabelText" value="%{getText(#naEnum.resourceKey)}"/>
                </c:when>
                <c:when test="${specimenRequest.study.preliminary}">
                    <c:set var="checkClass" value="checked"/>
                    <s:set var="preliminaryLabelText" value="%{getText(#yesEnum.resourceKey)}"/>
                </c:when>
                <c:when test="${!specimenRequest.study.preliminary}">
                    <c:set var="checkClass" value="x_icon"/>
                    <s:set var="preliminaryLabelText" value="%{getText(#noEnum.resourceKey)}"/>
                </c:when>
            </c:choose>
            <div id="preliminaryLabel" class="${checkClass}">
                <s:property value="%{#preliminaryLabelText}"/>
            </div>
        </div>

        <div class="formcol" style="width:500px">
            <label for="file2"><fmt:message key="specimenRequest.study.protocolDocument"/></label>
            <c:choose>
                <c:when test="${!empty specimenRequest.study.protocolDocument}">
                    <c:choose>
                        <c:when test="${specimenRequest.study.protocolDocument.lob.id != null}">
                            <c:url value="/protected/downloadFile.action" var="downloadProtocolUrl">
                                <c:param name="file.id" value="${specimenRequest.study.protocolDocument.lob.id}" />
                                <c:param name="fileName" value="${specimenRequest.study.protocolDocument.name}" />
                                <c:param name="contentType" value="${specimenRequest.study.protocolDocument.contentType}" />
                            </c:url>
                        </c:when>
                        <c:otherwise>
                            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/downloadProtocolDocument" urlEnding=".action"/>
                            <c:url value="${outputUrl}" var="downloadProtocolUrl"/>
                        </c:otherwise>
                    </c:choose>
                    <a href="${downloadProtocolUrl}" class="file">
                        ${specimenRequest.study.protocolDocument.name}
                    </a>
                </c:when>
                <c:otherwise>
                    <fmt:message key="notSpecified.value"/>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="clear"></div>
        <div class="line"></div>

        <label for="fundingStatus">
            <fmt:message key="specimenRequest.study.fundingStatus"/>
        </label>
        <c:set var="checkClass" value=""/>
        <s:set var="fundingStatusLabelText" value="%{''}"/>
        <s:set var="fundingStatus" value="%{#attr.specimenRequest.study.fundingStatus.toString()}"/>
        <c:choose>
            <c:when test="${fundingStatus == yesFundedString}">
                <c:set var="checkClass" value="checked"/>
                <s:set var="fundingStatusLabelText" value="%{getText(#yesFundedEnum.resourceKey)}"/>
            </c:when>
            <c:when test="${fundingStatus == notFundedString}">
                <c:set var="checkClass" value="x_icon"/>
                <s:set var="fundingStatusLabelText" value="%{getText(#notFundedEnum.resourceKey)}"/>
            </c:when>
           <c:when test="${fundingStatus == pendingFundedString}">
                <c:set var="checkClass" value="x_icon"/>
                <s:set var="fundingStatusLabelText" value="%{getText(#pendingFundedEnum.resourceKey)}"/>
            </c:when>
        </c:choose>
        <div id="fundingStatusLabel" class="${checkClass}">
            <s:property value="%{#fundingStatusLabelText}"/>
        </div>

        <c:if test="${fundingStatus == yesFundedString}">
            <div class="formcol" style="width:300px;min-height: 0">
                <label for="fs"><fmt:message key="specimenRequest.study.fundingSource"/></label>
                <s:set var="fundingSourceString" value="%{''}" scope="request"/>
                <s:iterator value="%{#attr.specimenRequest.study.fundingSources}">
                    <c:choose>
                        <c:when test="${empty fundingSourceString}">
                            <s:set var="fundingSourceString" value="%{name}" scope="request"/>
                        </c:when>
                        <c:otherwise>
                            <s:set var="fundingSourceString" value="%{#attr.fundingSourceString + ', ' + name}"
                                scope="request"/>
                        </c:otherwise>
                    </c:choose>
                </s:iterator>
                <tissuelocator:displayValueOrUnknown value="${fundingSourceString}"
                    unknownResourceKey="notSpecified.value" useResourceKey="false"/>
            </div>

            <s:set var="uiDynamicFieldCategory" value="getUiDynamicFieldCategory('specimenRequest.study.fundingStatus')"/>
            <s:if test="%{#uiDynamicFieldCategory != null}">
                <s:set var="dynamicFieldDefinitionList"
                    value="%{getDynamicFieldDefinitionsByCategory(#uiDynamicFieldCategory)}"/>
                <s:if test="%{!#dynamicFieldDefinitionList.isEmpty()}">
                    <tissuelocator:viewExtensions
                        dynamicFieldDefinitions="${dynamicFieldDefinitionList}"
                        layout="boxLayout"
                        extendableEntity="${object}"
                        wrapperClass="formcol"
                        wrapperStyle="width:300px"
                        columnsOnStartRow="1"
                        displayUnknown="${true}"/>
                </s:if>
            </s:if>

        </c:if>


        <div class="clear"></div>
        <div class="line"></div>

        <label for="approved"><fmt:message key="specimenRequest.study.irb.approved.text"/></label>
        <div id="approvedLabel" class="value">
            <s:property value="%{getText(#attr.specimenRequest.study.irbApprovalStatus.resourceKey)}"/>
        </div>

        <c:if test="${!empty specimenRequest.study.irbName ||
                      !empty specimenRequest.study.irbExemptionLetter}">
            <div class="formcol" style="width:300px">

                <c:if test="${!empty specimenRequest.study.irbName}">
                    <label for="irbname"><fmt:message key="specimenRequest.study.irbName"/></label>
                    ${specimenRequest.study.irbName}
                </c:if>

                <c:if test="${!empty specimenRequest.study.irbApprovalExpirationDate}">
                    <label for="irbApprovalExpirationDate"><fmt:message key="specimenRequest.study.irbApprovalExpirationDate"/></label>
                    <fmt:message key="default.date.format" var="dateFormat"/>
                    <fmt:formatDate pattern="${dateFormat}" value="${specimenRequest.study.irbApprovalExpirationDate}"/>

                    <label for="letterOfApproval"><fmt:message key="specimenRequest.study.irbApprovalLetter"/></label>
                    <c:choose>
                        <c:when test="${!empty specimenRequest.study.irbApprovalLetter}">
                            <c:choose>
                                <c:when test="${specimenRequest.study.irbApprovalLetter.lob.id != null}">
                                    <c:url value="/protected/downloadFile.action" var="downloadIrbApprovalLetterUrl">
                                        <c:param name="file.id" value="${specimenRequest.study.irbApprovalLetter.lob.id}" />
                                        <c:param name="fileName" value="${specimenRequest.study.irbApprovalLetter.name}" />
                                        <c:param name="contentType" value="${specimenRequest.study.irbApprovalLetter.contentType}" />
                                    </c:url>
                                </c:when>
                                <c:otherwise>
                                    <tissuelocator:determinePersistenceType urlPrefix="/protected/request/downloadIrbApprovalLetter" urlEnding=".action"/>
                                    <c:url value="${outputUrl}" var="downloadIrbApprovalLetterUrl"/>
                                </c:otherwise>
                            </c:choose>
                            <a href="${downloadIrbApprovalLetterUrl}" class="file">
                                ${specimenRequest.study.irbApprovalLetter.name}
                            </a>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="notSpecified.value"/>
                        </c:otherwise>
                    </c:choose>
                </c:if>

                <c:if test="${!empty specimenRequest.study.irbExemptionLetter}">
                    <label for="file3"><fmt:message key="specimenRequest.study.irbExemptionLetter"/></label>
                    <c:choose>
                        <c:when test="${specimenRequest.study.irbExemptionLetter.lob.id != null}">
                            <c:url value="/protected/downloadFile.action" var="downloadIrbExemptionLetterUrl">
                                <c:param name="file.id" value="${specimenRequest.study.irbExemptionLetter.lob.id}" />
                                <c:param name="fileName" value="${specimenRequest.study.irbExemptionLetter.name}" />
                                <c:param name="contentType" value="${specimenRequest.study.irbExemptionLetter.contentType}" />
                            </c:url>
                        </c:when>
                        <c:otherwise>
                            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/downloadIrbExemptionLetter" urlEnding=".action"/>
                            <c:url value="${outputUrl}" var="downloadIrbExemptionLetterUrl"/>
                        </c:otherwise>
                    </c:choose>
                    <a href="${downloadIrbExemptionLetterUrl}" class="file">
                        ${specimenRequest.study.irbExemptionLetter.name}
                    </a>
                </c:if>
            </div>
        </c:if>

        <c:if test="${!empty specimenRequest.study.irbRegistrationNumber}">
            <div class="formcol" style="width:300px">
                <label for="irbRegNum"><fmt:message key="specimenRequest.study.irbRegistrationNumber"/></label>
                <tissuelocator:displayValueOrUnknown value="${specimenRequest.study.irbRegistrationNumber}"
                                                     unknownResourceKey="notSpecified.value"/>

                <c:if test="${!empty specimenRequest.study.irbApprovalExpirationDate}">
                    <label for="irbAppNum"><fmt:message key="specimenRequest.study.irbApprovalNumber"/></label>
                    <tissuelocator:displayValueOrUnknown value="${specimenRequest.study.irbApprovalNumber}"
                                                         unknownResourceKey="notSpecified.value"/>
                </c:if>
            </div>
        </c:if>

        <div class="clear"></div>
        <div class="line"></div>

        <label for="sa"><fmt:message key="specimenRequest.study.studyAbstract"/></label>
        <p>${specimenRequest.study.studyAbstract}</p>

        <div class="clear"></div>
        <div class="line"></div>
    </div>


    <c:set var="sectionCount" value="${sectionCount + 1}"/>
    <h2 class="noline">
        <a href="javascript:;" onclick="toggleVisibility('principal_investigator');" class="toggle">
            <fmt:message key="specimenRequest.investigator.title">
                <fmt:param value="${sectionCount}"/>
            </fmt:message>
        </a>
    </h2>

    <div id="principal_investigator">
        <tissuelocator:viewPerson person="${specimenRequest.investigator}" showInstitution="true"/>

        <div class="formcol_wide" style="width:300px">
            <label for="file"><fmt:message key="specimenRequest.investigator.resume"/></label>
            <c:choose>
                <c:when test="${specimenRequest.investigator.resume.lob.id != null}">
                    <c:url value="/protected/downloadFile.action" var="downloadResumeUrl">
                        <c:param name="file.id" value="${specimenRequest.investigator.resume.lob.id}" />
                        <c:param name="fileName" value="${specimenRequest.investigator.resume.name}" />
                        <c:param name="contentType" value="${specimenRequest.investigator.resume.contentType}" />
                    </c:url>
                </c:when>
                <c:otherwise>
                    <tissuelocator:determinePersistenceType urlPrefix="/protected/request/downloadResume" urlEnding=".action"/>
                    <c:url value="${outputUrl}" var="downloadResumeUrl"/>
                </c:otherwise>
            </c:choose>
            <a href="${downloadResumeUrl}" class="file">
                ${specimenRequest.investigator.resume.name}
            </a>
        </div>

        <div class="clear"></div>
        <div class="line"></div>

        <s:if test="%{displayPILegalField}">
            <h3>Principal Investigator Legal Questions</h3>

            <div id="pi_certified">
                <tissuelocator:investigatorCertificationText showRequired="false"/>
                <c:set var="checkClass" value="x_icon"/>
                    <c:if test="${specimenRequest.investigator.certified eq yesEnum}">
                        <c:set var="checkClass" value="checked"/>
                    </c:if>
                <div id="investigatorCertifiedLabel" class="${checkClass}" style="margin:-4px 0 15px 0;">
                    <s:property value="%{getText(#attr.specimenRequest.investigator.certified.resourceKey)}"/>
                </div>
            </div>

            <div id="pi_investigated">
                <label for="investigated_YES"><fmt:message key="specimenRequest.investigator.investigated.text"/></label>
                <c:set var="checkClass" value="x_icon"/>
                <c:if test="${specimenRequest.investigator.investigated == yesEnum}">
                    <c:set var="checkClass" value="checked"/>
                </c:if>
                <div id="investigatorInvestigatedLabel" class="${checkClass}">
                    <s:property value="%{getText(#attr.specimenRequest.investigator.investigated.resourceKey)}"/>
                </div>
                <c:if test="${!empty specimenRequest.investigator.investigationComment}">
                    <label>
                        <fmt:message key="specimenRequest.investigator.explanation"/>
                    </label>
                    ${specimenRequest.investigator.investigationComment}
                </c:if>
            </div>

            <div class="clear"></div>
            <div class="line"></div>
        </s:if>

    </div>

    <c:set var="sectionCount" value="${sectionCount + 1}"/>
    <h2 class="noline">
        <a href="javascript:;" onclick="toggleVisibility('shipping_details');" class="toggle">
            <fmt:message key="specimenRequest.shipment.title">
                <fmt:param value="${sectionCount}"/>
            </fmt:message>
        </a>
    </h2>

    <div id="shipping_details">
        <div class="formcol_wide" style="width:300px">
            <label for="si"><fmt:message key="specimenRequest.shipment.instructions"/></label>
            <p>${specimenRequest.shipment.instructions}</p>
        </div>

        <div class="clear"></div>

        <br />
        <div class="line"></div>
    </div>

    <c:set var="sectionCount" value="${sectionCount + 1}"/>
    <h2 class="noline">
        <a href="javascript:;" onclick="toggleVisibility('shipping_address');" class="toggle">
            <fmt:message key="specimenRequest.shipment.recipient.title">
                <fmt:param value="${sectionCount}"/>
            </fmt:message>
        </a>
    </h2>

    <div id="shipping_address">
        <tissuelocator:viewPerson person="${specimenRequest.shipment.recipient}" showInstitution="true"/>
        <div class="line"></div>
    </div>

    <c:if test="${displayShipmentBillingRecipient && (not empty specimenRequest.shipment.billingRecipient)}">
        <c:set var="sectionCount" value="${sectionCount + 1}"/>
        <h2 class="noline">
            <a href="javascript:;" onclick="toggleVisibility('billing_address');" class="toggle">
                <fmt:message key="specimenRequest.shipment.billingRecipient.title">
                    <fmt:param value="${sectionCount}"/>
                </fmt:message>
            </a>
        </h2>

        <div id="billing_address">
            <tissuelocator:viewPerson person="${specimenRequest.shipment.billingRecipient}" showInstitution="true"/>
            <div class="line"></div>
        </div>    
    </c:if>
    
    <c:if test="${not (empty specimenRequest.statusTransitionHistory && specimenRequest.status == null)}">
        <c:set var="sectionCount" value="${sectionCount + 1}"/>
        <h2 class="noline">
            <a href="javascript:;" onclick="toggleVisibility('status_details');" class="toggle">
                <fmt:message key="specimenRequest.statusHistory.title">
                    <fmt:param value="${sectionCount}"/>
                </fmt:message>
            </a>
        </h2>
        <fmt:message key="date.format.display" var="dateFormat"/>
        <div id="status_details">
            <c:choose>
                <c:when test="${empty specimenRequest.statusTransitionHistory && specimenRequest.status != null}">
                    <span><fmt:message key="${specimenRequest.status.resourceKey}"/></span>
                    <span><fmt:formatDate pattern="${dateFormat}" value="${specimenRequest.requestedDate}"/></span>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${specimenRequest.statusTransitionHistory}" var="transition" varStatus="loopStatus">
                        <c:if test="${loopStatus.first}">
                            <span><fmt:message key="${transition.previousStatus.resourceKey}"/></span>
                            <span><fmt:formatDate pattern="${dateFormat}" value="${specimenRequest.requestedDate}"/></span>
                            <br/>
                        </c:if>
                        <span><fmt:message key="${transition.newStatus.resourceKey}"/></span>
                        <span><fmt:formatDate pattern="${dateFormat}" value="${transition.transitionDate}"/></span>
                        <c:if test="${not loopStatus.last}"><br/></c:if>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
            <div class="line"></div>
        </div>
    </c:if>

    <s:set var="hasGeneralInformation" value="%{false}"/>
    <s:if test="%{!uiDynamicFieldCategories.isEmpty()}">
        <s:subset source="uiDynamicFieldCategories.values()" decider="uiDynamicFieldCategoryDecider">
            <s:iterator var="uiDynamicFieldCategory">
                <s:if test="%{!#uiDynamicFieldCategory.dynamicFieldDefinitions.isEmpty()}">
                    <s:set var="hasGeneralInformation" value="%{true}"/>
                </s:if>
            </s:iterator>
        </s:subset>
    </s:if>

    <c:if test="${!empty specimenRequest.generalComment}">
        <s:set var="hasGeneralInformation" value="%{true}"/>
    </c:if>

    <s:if test="%{#hasGeneralInformation}">
        <c:set var="sectionCount" value="${sectionCount + 1}"/>
        <h2 class="noline">
            <a href="javascript:;" onclick="toggleVisibility('general_info');" class="toggle">
                <fmt:message key="specimenRequest.generalInfo.title">
                    <fmt:param value="${sectionCount}"/>
                </fmt:message>
            </a>
        </h2>

        <div id="general_info">
            <s:if test="%{!uiDynamicFieldCategories.isEmpty()}">
                <s:subset source="uiDynamicFieldCategories.values()" decider="uiDynamicFieldCategoryDecider">
                    <s:iterator var="uiDynamicFieldCategory">
                        <s:set var="dynamicFieldDefinitions"
                               value="%{#uiDynamicFieldCategory.dynamicFieldDefinitions}"/>
                        <s:if test="%{!#dynamicFieldDefinitions.isEmpty()}">
                            <h2>${categoryName}</h2>
                            <div class="datacol">
                                <tissuelocator:viewExtensions
                                    dynamicFieldDefinitions="${dynamicFieldDefinitions}"
                                    layout="boxLayout"
                                    extendableEntity="${specimenRequest}"
                                    columnsOnStartRow="0" />
                            </div>
                        </s:if>
                    </s:iterator>
                </s:subset>
                <div class="clear"><br /></div>
            </s:if>
            <c:if test="${!empty specimenRequest.generalComment}">
                <label for="generalComment"><fmt:message key="specimenRequest.generalComent"/></label>
                <p>${specimenRequest.generalComment}</p>
            </c:if>
            <div class="line"></div>
        </div>
    </s:if>
</div>


