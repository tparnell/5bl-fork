<%@ tag body-content="empty" %>
<%@ attribute name="extension" required="true" rtexprvalue="true"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${not empty extension}">
    <fmt:message key="display.phoneExtension"/>&nbsp;${extension}
</c:if>
