<%@ attribute name="order"
	type="com.fiveamsolutions.tissuelocator.data.Shipment"%>
<%@ attribute name="finalPriceTotal"%>
<%@ attribute name="showInstitution" required="true"%>
<%@ attribute name="showOrderForms" required="true"%>
<%@ attribute name="showPriceRange" required="true"%>
<%@ attribute name="showReceiptQuality" %>
<%@ attribute name="showFinalPriceColumn" required="true"%>
<%@ attribute name="minPriceTotal"%>
<%@ attribute name="maxPriceTotal"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0"
	prefix="req"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator"%>
<c:if test="${empty finalPriceTotal}">
    <c:set var="finalPriceTotal" value="${0.0}"/>
</c:if>
<display:footer>
	<c:set var="footerColspanOne" value="${1}" />
	<c:if
		test="${showInstitution and showOrderForms != 'true'}">
		<c:set var="footerColspanOne" value="${footerColspanOne + 1}" />
	</c:if>
	<req:isUserInRole role="priceViewer">
		<c:if test="${empty showPriceRange || showPriceRange == 'true'}">
			<c:set var="footerColspanOne" value="${footerColspanOne + 1}" />
		</c:if>
		<c:if test="${showFinalPriceColumn == 'true'}">
			<c:set var="footerColspanOne" value="${footerColspanOne + 1}" />
		</c:if>
	</req:isUserInRole>
	<c:set var="footerColspanTwo" value="${1}" />
	<c:if test="${showReceiptQuality == 'true'}">
		<c:set var="footerColspanTwo" value="${footerColspanTwo + 1}" />
	</c:if>
	<c:if test="${showOrderForms == 'true'}">
		<c:set var="footerColspanTwo" value="${footerColspanTwo + 1}" />
	</c:if>
	<c:choose>
		<c:when test="${!empty order}">
			<req:isUserInRole role="priceViewer">
				<c:if test="${not empty order.shippingPrice}">
					<c:set var="finalPriceTotal"
						value="${finalPriceTotal + order.shippingPrice}" />
				</c:if>
				<c:if test="${not empty order.fees}">
					<c:set var="finalPriceTotal"
						value="${finalPriceTotal + order.fees}" />
				</c:if>
				<tr class="graybar" style="background: #efefef">
					<td class="alignleft" colspan="${footerColspanOne}"><strong><fmt:message
						key="shipment.othercharges" /></strong></td>
					<td class="alignright"><strong><fmt:message
						key="shipment.shippingPrice" /></strong></td>
					<td class="action" style="white-space: nowrap"><c:if
						test="${showFinalPriceColumn == 'true'}">
						<c:choose>
							<c:when test="${showOrderForms == 'true'}">
                                            &#36;&nbsp;<s:textfield
									size="5" name="object.shippingPrice" id="price"
									value="%{#attr.order.shippingPrice}" theme="simple" 
									title="%{getText('shipment.shippingPrice.field')}"/>
								<s:fielderror cssClass="fielderror">
									<s:param>object.shippingPrice</s:param>
								</s:fielderror>
							</c:when>
							<c:when test="${not empty order.shippingPrice}">
								<strong> <fmt:formatNumber
									value="${order.shippingPrice}" type="currency" /> </strong>
							</c:when>
							<c:otherwise>-</c:otherwise>
						</c:choose>
					</c:if></td>
					<td colspan="${footerColspanTwo}"></td>
				</tr>
				<tr class="graybar" style="background: #efefef">
					<td colspan="${footerColspanOne}"></td>
					<td class="alignright"><strong><fmt:message
						key="shipment.fees" /></strong></td>
					<td class="action" style="white-space: nowrap"><c:if
						test="${showFinalPriceColumn == 'true'}">
						<c:choose>
							<c:when test="${showOrderForms == 'true'}">
                                            &#36;&nbsp;<s:textfield
									size="5" name="object.fees" id="fees"
									value="%{#attr.order.fees}" theme="simple" 									
									title="%{getText('shipment.fees.field')}"/>
								<s:fielderror cssClass="fielderror">
									<s:param>object.fees</s:param>
								</s:fielderror>
							</c:when>
							<c:when test="${not empty order.fees}">
								<strong> <fmt:formatNumber value="${order.fees}"
									type="currency" /> </strong>
							</c:when>
							<c:otherwise>-</c:otherwise>
						</c:choose>
					</c:if></td>
					<td colspan="${footerColspanTwo}"></td>
				</tr>
			</req:isUserInRole>
			<c:set var="footerColspanThree" value="${footerColspanOne}" />
			<c:if test="${showOrderForms == 'true'}">
				<c:set var="footerColspanThree" value="${footerColspanThree - 1}" />
			</c:if>
			<tr class="graybar">
				<td colspan="${footerColspanThree}"></td>
				<td class="alignright"><req:isUserInRole role="priceViewer">
					<strong><fmt:message key="shipment.orderTotal" /></strong>
				</req:isUserInRole></td>
				<c:if test="${showOrderForms == 'true'}">
					<td><req:isUserInRole role="priceViewer">
						<s:submit value="%{getText('btn.saveOrderPricing')}" name="submit"
							id="btn_save" cssClass="btn" theme="simple" title="%{getText('btn.saveOrderPricing.button')}"/>
					</req:isUserInRole></td>
				</c:if>
				<td class="action" style="white-space: nowrap"><c:if
					test="${showFinalPriceColumn == 'true'}">
					<req:isUserInRole role="priceViewer">
						<c:choose>
							<c:when test="${not empty finalPriceTotal}">
								<strong> <fmt:formatNumber value="${finalPriceTotal}"
									type="currency" /> </strong>
							</c:when>
							<c:otherwise>-</c:otherwise>
						</c:choose>
					</req:isUserInRole>
				</c:if></td>
				<td colspan="${footerColspanTwo}"></td>
			</tr>
		</c:when>
		<c:otherwise>
			<c:if
				test="${(not empty minPriceTotal) and (not empty maxPriceTotal)}">
				<tr class="graybar">
					<td colspan="${footerColspanOne}"></td>
					<td class="alignright"><req:isUserInRole role="priceViewer">
						<strong> <fmt:message key="cart.subtotal" /> </strong>
					</req:isUserInRole></td>
					<td class="action" style="white-space: nowrap">
                    <req:isUserInRole role="priceViewer">
						<strong>
                            <tissuelocator:specimenPrice
                                minimumPrice="${minPriceTotal}" maximumPrice="${maxPriceTotal}"/>
                        </strong>
					</req:isUserInRole></td>
					<td colspan="${footerColspanTwo}"></td>
				</tr>
			</c:if>
		</c:otherwise>
	</c:choose>
</display:footer>