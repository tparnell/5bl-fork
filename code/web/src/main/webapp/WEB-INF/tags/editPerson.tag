<%@ tag body-content="empty" %>
<%@ attribute name="propertyPrefix" required="true" rtexprvalue="true"%>
<%@ attribute name="resourcePrefix" required="true" rtexprvalue="true"%>
<%@ attribute name="idPrefix" required="true" %>
<%@ attribute name="stateValue" required="true" rtexprvalue="true" %>
<%@ attribute name="countryValue" required="true" rtexprvalue="true" %>
<%@ attribute name="copyEnabled" required="true"  rtexprvalue="true"%>
<%@ attribute name="showInstitution" required="true"  %>
<%@ attribute name="showFax" required="true"  %>
<%@ attribute name="showResume" %>
<%@ attribute name="resumeFile" type="com.fiveamsolutions.tissuelocator.data.TissueLocatorFile" rtexprvalue="true" %>
<%@ attribute name="unsavedResumeDownloadUrl" %>
<%@ attribute name="helpUrlBase" rtexprvalue="true" %>
<%@ attribute name="resumeHelpFieldName" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<c:if test="${copyEnabled}">
<s:checkbox theme="simple" id="%{'copy' + #attr.idPrefix}" name="%{'copy' + #attr.idPrefix}"
    onclick="%{'copyTo' + #attr.idPrefix + '()'}"/>
<label class="inline" for="copy${idPrefix}">
    <fmt:message key="specimenRequest.copyAddress"/>
</label>
<br />
</c:if>

<div class="clear"></div>

<div class="formcol">
    <s:textfield name="%{#attr.propertyPrefix + '.firstName'}" id="%{#attr.idPrefix + 'FirstName'}"
            size="30" maxlength="254" cssStyle="width:20em" required="true" requiredposition="right"
            labelposition="top" label="%{getText(#attr.resourcePrefix + '.firstName')}" labelSeparator=""/>
</div>

<div class="formcol">
    <s:textfield name="%{#attr.propertyPrefix + '.lastName'}" id="%{#attr.idPrefix + 'LastName'}"
            size="30" maxlength="254" cssStyle="width:20em" required="true" requiredposition="right"
            labelposition="top" label="%{getText(#attr.resourcePrefix + '.lastName')}" labelSeparator=""/>
</div>

<div class="clear"></div>

<c:if test="${showInstitution == 'true'}">
    <s:set var="fieldName" value="%{#attr.propertyPrefix + '.organization'}" scope="page"/>
    <s:set var="searchFieldName" value="%{#attr.idPrefix + 'OrganizationName'}" scope="page"/>
    <s:set var="searchFieldId" value="%{#attr.idPrefix + 'Organization'}" scope="page"/>
    <tissuelocator:autocompleter fieldName="${fieldName}" fieldId="${searchFieldId}"
        searchFieldName="${searchFieldName}" cssStyle="width:27em"
        required="true" autocompleteUrl="protected/json/autocompleteInstitution.action"
        labelKey="${resourcePrefix}.organization" noteKey="autocompleter.note"/>
</c:if>

<s:textfield name="%{#attr.propertyPrefix + '.address.line1'}" id="%{#attr.idPrefix + 'Line1'}"
        size="30" maxlength="254" cssStyle="width:280px" required="true" requiredposition="right"
        labelposition="top" label="%{getText(#attr.resourcePrefix + '.address.line1')}" labelSeparator=""
        title="%{getText(#attr.resourcePrefix + '.address.line1.field')}"/>

<div class="pad5"></div>

<s:textfield name="%{#attr.propertyPrefix + '.address.line2'}" id="%{#attr.idPrefix + 'Line2'}"
    size="30" maxlength="254" cssStyle="width:280px" theme="simple"
    title="%{getText(#attr.resourcePrefix + '.address.line2.field')}"/>

<div class="clear"></div>

<div class="formcol" style="width:300px">
    <s:textfield name="%{#attr.propertyPrefix + '.address.city'}" id="%{#attr.idPrefix + 'City'}"
            size="30" maxlength="254" cssStyle="width:280px" required="true" requiredposition="right"
            labelposition="top" label="%{getText(#attr.resourcePrefix + '.address.city')}" labelSeparator=""/>
</div>

<div class="formcol_thin" id="${idPrefix}StateWrapper" style="display:none">
    <s:select name="%{#attr.propertyPrefix + '.address.state'}" id="%{#attr.idPrefix + 'State'}"
        value="%{#attr.stateValue}" required="true" requiredposition="right"
        list="%{@com.fiveamsolutions.tissuelocator.data.State@values()}" listValue="%{getText(resourceKey)}"
        headerKey="" headerValue="%{getText('select.emptyOption')}"
        labelposition="top" label="%{getText(#attr.resourcePrefix + '.address.state')}" labelSeparator=""/>
    <script type="text/javascript">
        $(function(){
            $("#${idPrefix}State").multiselect({
                multiple: false,
                header: false,
                classes: 'auto fixed',
                selectedList: 1,
                noneSelectedText: '<fmt:message key="select.emptyOption"/>',
                position: {
                    my: 'left top',
                    at: 'left bottom'
                }
             });
        });
    </script>
</div>

<div class="formcol_xthin" style="width:10em">
    <s:textfield name="%{#attr.propertyPrefix + '.address.zip'}" id="%{#attr.idPrefix + 'Zip'}"
        size="20" maxlength="20" cssStyle="width:10em" required="true" requiredposition="right"
        labelposition="top" label="%{getText(#attr.resourcePrefix + '.address.zip')}" labelSeparator=""/>
</div>

<div class="clear"></div>

<div class="formcol" id="${idPrefix}CountryWrapper" style="display:none">
    <s:select name="%{#attr.propertyPrefix + '.address.country'}" id="%{#attr.idPrefix + 'Country'}"
        value="%{#attr.countryValue}" required="true" requiredposition="right" cssStyle="width:280px"
        list="%{@com.fiveamsolutions.tissuelocator.data.Country@values()}" listValue="%{getText('country.' + name())}"
        labelposition="top" label="%{getText(#attr.resourcePrefix + '.address.country')}" labelSeparator=""/>
    <script type="text/javascript">
        $(function(){
            $("#${idPrefix}Country").multiselect({
                multiple: false,
                header: false,
                selectedList: 1,
                noneSelectedText: '<fmt:message key="select.emptyOption"/>',
                position: {
                    my: 'left top',
                    at: 'left bottom'
                }
             });
        });
    </script>
</div>

<div class="clear"></div>

<div class="formcol" style="width:300px">
    <s:textfield name="%{#attr.propertyPrefix + '.email'}" id="%{#attr.idPrefix + 'Email'}"
            size="30" maxlength="254" cssStyle="width:280px" required="true" requiredposition="right"
            labelposition="top" label="%{getText(#attr.resourcePrefix + '.email')}" labelSeparator=""/>
</div>

<div class="formcol">
    <s:textfield name="%{#attr.propertyPrefix + '.address.phone'}" id="%{#attr.idPrefix + 'Phone'}"
            size="10" maxlength="10" cssStyle="width:20em" required="true" requiredposition="right"
            labelposition="top" label="%{getText(#attr.resourcePrefix + '.address.phone')}" labelSeparator=""/>
</div>
<div class="formcol_xthin">
    <s:textfield name="%{#attr.propertyPrefix + '.address.phoneExtension'}" id="%{#attr.idPrefix + 'PhoneExtension'}"
            size="10" maxlength="10" cssStyle="width:6em"
            labelposition="top" label="%{getText(#attr.resourcePrefix + '.address.phoneExtension')}" labelSeparator=""/>
</div>

<div class="clear"></div>
<c:if test="${showFax == 'true'}">
    <div class="formcol_xthin" style="width:10em">
        <s:textfield name="%{#attr.propertyPrefix + '.address.fax'}" id="%{#attr.idPrefix + 'Fax'}"
                size="10" maxlength="10" cssStyle="width:15em"
                labelposition="top" label="%{getText(#attr.resourcePrefix + '.address.fax')}" labelSeparator=""/>
    </div>
</c:if>

<c:if test="${showResume == 'true'}">
    <tissuelocator:fileField file="${resumeFile}" fieldSize="30"
        id="resume" name="resume" objectProperty="${propertyPrefix}.resume"
        labelKey="${resourcePrefix}.resume" required="true"
        unsavedUrlBase="${unsavedResumeDownloadUrl}"
        helpFieldName="${resumeHelpFieldName}" helpUrlBase="${helpUrlBase}"/>
</c:if>

<script type="text/javascript">
    $(document).ready(function() {
      $('#${idPrefix}StateWrapper').show();
      $('#${idPrefix}CountryWrapper').show();
  });
</script>

<c:if test="${copyEnabled}">
    <script>
        var copyTo${idPrefix} = function() {
            <c:set var="quote" value="'"/>
            <c:set var="escapedQuote" value="\\'"/>
            if ($('#copy${idPrefix}').get(0).checked) {
                $('#${idPrefix}FirstName').get(0).value = '${fn:replace(TissueLocatorUser.firstName, quote, escapedQuote)}';
                $('#${idPrefix}LastName').get(0).value = '${fn:replace(TissueLocatorUser.lastName, quote, escapedQuote)}';
                $('#${idPrefix}Email').get(0).value = '${fn:replace(TissueLocatorUser.email, quote, escapedQuote)}';
                $('#${idPrefix}Line1').get(0).value = '${fn:replace(TissueLocatorUser.address.line1, quote, escapedQuote)}';
                $('#${idPrefix}Line2').get(0).value = '${fn:replace(TissueLocatorUser.address.line2, quote, escapedQuote)}';
                $('#${idPrefix}City').get(0).value = '${fn:replace(TissueLocatorUser.address.city, quote, escapedQuote)}';
                $('#${idPrefix}State').get(0).value = '${fn:replace(TissueLocatorUser.address.state, quote, escapedQuote)}';
                $('#${idPrefix}State').multiselect('refresh');
                $('#${idPrefix}Zip').get(0).value = '${fn:replace(TissueLocatorUser.address.zip, quote, escapedQuote)}';
                $('#${idPrefix}Country').get(0).value = '${fn:replace(TissueLocatorUser.address.country, quote, escapedQuote)}';
                $('#${idPrefix}Country').multiselect('refresh');
                $('#${idPrefix}Phone').get(0).value = '${fn:replace(TissueLocatorUser.address.phone, quote, escapedQuote)}';
                $('#${idPrefix}PhoneExtension').get(0).value = '${fn:replace(TissueLocatorUser.address.phoneExtension, quote, escapedQuote)}';
                <c:if test="${showFax == 'true'}">
                    $('#${idPrefix}Fax').get(0).value = '${fn:replace(TissueLocatorUser.address.fax, quote, escapedQuote)}';
                </c:if>
                <c:if test="${showInstitution == 'true'}">
                    $('#${idPrefix}Organization').get(0).value = '${fn:replace(TissueLocatorUser.institution.name, quote, escapedQuote)}';
                    $('#${idPrefix}OrganizationHidden').get(0).value = ${TissueLocatorUser.institution.id};
                </c:if>
                <c:if test="${showResume == 'true' && !empty resumeFile}">
                    $('#newresume').hide();
                    $('#existingresume').show();
                    $('#resume').val('');
                </c:if>
            }
        }
    </script>
</c:if>