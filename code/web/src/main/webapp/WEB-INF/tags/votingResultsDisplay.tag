<%@ tag body-content="empty" %>
<%@ attribute name="specimenRequest" required="true" type="com.fiveamsolutions.tissuelocator.data.SpecimenRequest" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@DRAFT}" name="draft" scope="page"/>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING}" name="pending" scope="page"/>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_FINAL_DECISION}"
    name="pendingDecision" scope="page"/>
<s:set name="approvedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.Vote@APPROVE}" />
<s:set name="deniedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.Vote@DENY}" />
<c:if test="${specimenRequest.status != draft && specimenRequest.status != pendingDecision}">
    <s:if test="%{displayRequestReviewVotes}">
        <tissuelocator:specimenRequestVoteList votes="${specimenRequest.consortiumReviews}"
            titleKey="specimenRequest.vote.consortiumDecision.label"/>
        <tissuelocator:specimenRequestVoteList votes="${specimenRequest.institutionalReviews}"
            titleKey="specimenRequest.vote.institutionDecision.label"/>
    </s:if>
</c:if>
<c:if test="${specimenRequest.status != draft && specimenRequest.status != pending && specimenRequest.status != pendingDecision}">
    <s:else>
        <c:if test="${not empty specimenRequest.finalVote}">
            <h2 class="noline huge"><fmt:message key="specimenRequest.review.finalComment.title"/></h2>
            <div class="box">
                <div class="pad10">
                    <fmt:message key="specimenRequest.review.finalComment.votingResults"/>:
                    <strong><fmt:message key="${specimenRequest.finalVote.resourceKey}"/></strong>
                </div>
            </div>
        </c:if>
        <c:if test="${not empty specimenRequest.externalComment}">
            <div class="box">
                <div class="pad10">
                    <label for="rc">
                        <fmt:message key="specimenRequest.review.externalComment.label"/>:
                    </label>
                    <p>${specimenRequest.externalComment}</p>
                </div>
            </div>
        </c:if>
    </s:else>
</c:if>
