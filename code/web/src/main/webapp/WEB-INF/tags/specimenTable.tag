<%@ tag body-content="empty" %>
<%@ attribute name="showCheckboxes" required="true" %>
<%@ attribute name="showEditButtons" required="true" %>
<%@ attribute name="exportEnabled" required="true" %>
<%@ attribute name="formId" %>
<%@ attribute name="checkboxProperty" %>
<%@ attribute name="listAction" %>
<%@ attribute name="viewAction" required="true" %>
<%@ attribute name="showStatus" required="true" %>
<%@ attribute name="showFee" required="true" %>
<%@ attribute name="showInstitution" required="true" %>
<%@ attribute name="columnFilteringEnabled" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>

<s:set value="%{pageSize}" name="tablePageSize" scope="page"/>

<fmt:message key="default.datetime.format" var="datetimeFormat"/>
<div id="specimenSearchResultsPanel" class="dataPanel">

<display:table name="objects" requestURI="${listAction}"
         uid="specimen" pagesize="${tablePageSize}" sort="external" export="${exportEnabled}">
    <tissuelocator:displaytagProperties/>
    <display:setProperty name="export.csv.filename" value="specimens.csv"/>
    <display:setProperty name="export.csv.include_header" value="true"/>
     
    <c:if test="${showCheckboxes}">
        <fmt:message key="specimen.selectAll" var="selectAllCheckbox"/>
        <display:column title="<span style='padding:7px'><input type='checkbox' onclick='selectAllSpecimens()' 
                                                                title='${selectAllCheckbox}' id='selectall'></span>"
                        media="html">
            <s:checkbox name="%{#attr.checkboxProperty}" theme="simple" fieldValue="%{#attr.specimen.id}" 
                        title="%{getText('specimen.search.select')}" />
        </display:column>
    </c:if>

    <display:column titleKey="specimen.id" class="title" sortProperty="ID" sortable="true" media="html">
        <c:url var="viewUrl" value="${viewAction}">
            <c:param name="object.id" value="${specimen.id}"/>
            <c:if test="${order.id != null}"><c:param name="orderId" value="${order.id}"/></c:if>
        </c:url>
        <a href="${viewUrl}">${specimen.id}</a>
    </display:column>

    <display:column titleKey="specimen.id" property="id" sortProperty="ID" sortable="true" media="csv" />

    <s:if test="%{#attr.columnFilteringEnabled != null && #attr.columnFilteringEnabled}">
        <%-- For search pages, use configured display settings --%>
        <s:set var="filtered" value="true"/>
        <s:set var="propertyList" value="%{searchResultFieldDisplaySettings}"/>
    </s:if>
    <s:else>
        <%-- Ohterwise, use pre-defined columns --%>
        <s:set var="filtered" value="false"/>
        <s:set var="propertyList" value="%{{'specimenType', 'pathologicalCharacteristic', 'availableQuantity', 'patientAgeAtCollection',
                    'collectionYear', 'externalId', 'externalIdAssigner.name', 'status'}}"/>
    </s:else>

		<s:iterator value="%{#propertyList}" var="property">

			<s:if test="%{#filtered}">
				<s:set var="propertyName" value="fieldConfig.fieldName" />
				<s:if
					test="%{#session['SpecimenDisplayOptions'] != null && #session['SpecimenDisplayOptions'][#propertyName].displayFlagAsBoolean == true}">
					<s:set var="displayColumnMediaValue" value="" />
				</s:if>
				<s:else>
					<s:set var="displayColumnMediaValue" value="%{'csv'}" />
				</s:else>
			</s:if>
			<s:else>
				<s:set var="propertyName" value="%{#property}" />
			</s:else>

			<s:if test="%{#propertyName eq 'specimenType'}">
				<display:column titleKey="specimen.specimenType" sortable="true"
					sortProperty="SPECIMEN_TYPE"
					decorator="com.fiveamsolutions.tissuelocator.web.decorator.NewlineRemovalDecorator"
					media="${displayColumnMediaValue}">
					<tissuelocator:codeDisplay code="${specimen.specimenType}" />
				</display:column>
			</s:if>
			<s:elseif
				test="%{#propertyName eq 'pathologicalCharacteristic'}">
				<display:column titleKey="specimen.pathologicalCharacteristic"
					sortable="true" sortProperty="PATHOLOGICAL_CHARACTERISTIC"
					decorator="com.fiveamsolutions.tissuelocator.web.decorator.NewlineRemovalDecorator"
					media="${displayColumnMediaValue}">
					<tissuelocator:codeDisplay
						code="${specimen.pathologicalCharacteristic}" />
				</display:column>
			</s:elseif>
			<s:elseif test="%{#propertyName eq 'availableQuantity'}">
				<display:column titleKey="specimen.availableQuantity"
					sortable="true"
					sortProperty="AVAILABLE_QUANTITY_UNITS,AVAILABLE_QUANTITY"
					decorator="com.fiveamsolutions.tissuelocator.web.decorator.NewlineRemovalDecorator"
					media="${displayColumnMediaValue}">
					<c:if test="${!empty specimen.availableQuantity}">
						<fmt:formatNumber value="${specimen.availableQuantity}"
							maxFractionDigits="2" />
						<fmt:message key="${specimen.availableQuantityUnits.resourceKey}" />
					</c:if>
				</display:column>
			</s:elseif>
			<s:elseif
				test="%{#propertyName eq 'patientAgeAtCollection'}">
				<display:column titleKey="specimen.patientAgeAtCollection.short"
					sortable="true"
					sortProperty="PATIENT_AGE_AT_COLLECTION_UNITS,PATIENT_AGE_AT_COLLECTION"
					decorator="com.fiveamsolutions.tissuelocator.web.decorator.NewlineRemovalDecorator"
					media="${displayColumnMediaValue}">
                    ${specimen.patientAgeAtCollection}<c:if
						test="${specimen.patientAgeAtCollectionAtHippaMax}">+ </c:if>
					<fmt:message
						key="${specimen.patientAgeAtCollectionUnits.resourceKey}" />
				</display:column>
			</s:elseif>
			<s:elseif test="%{#propertyName eq 'collectionYear'}">
				<display:column titleKey="specimen.collectionYear.short"
					property="collectionYear" sortable="true"
					sortProperty="COLLECTION_YEAR" media="${displayColumnMediaValue}" />
			</s:elseif>
			<s:elseif test="%{#propertyName eq 'externalId'}">
				<req:isUserInRole role="externalIdViewer">
					<display:column titleKey="specimen.externalId"
						property="externalId" sortProperty="EXTERNAL_ID" sortable="true"
						media="${displayColumnMediaValue}" />
				</req:isUserInRole>
			</s:elseif>
			<s:elseif
				test="%{#propertyName eq 'externalIdAssigner.name'}">
				<req:isUserInRole role="preApprovalSpecimenLocViewer">
					<c:set var="specimenLocViewer" value="true" />
				</req:isUserInRole>
				<req:isUserInRole role="postApprovalSpecimenLocViewer">
					<c:set var="specimenLocViewer" value="true" />
				</req:isUserInRole>
				<c:if test="${specimenLocViewer}">
					<c:if test="${showInstitution}">
						<display:column titleKey="specimen.storageInstitution"
							sortable="true"
							decorator="com.fiveamsolutions.tissuelocator.web.decorator.NewlineRemovalDecorator">
							<tissuelocator:specimenLocation specimen="${specimen}">
                            ${specimen.externalIdAssigner.name}
                        </tissuelocator:specimenLocation>
						</display:column>
					</c:if>
				</c:if>
			</s:elseif>
			<s:elseif test="%{#propertyName eq 'minimumPrice'}">
				<req:isUserInRole role="priceViewer">
					<c:if test="${showFee}">
						<display:column titleKey="specimen.price" sortable="true"
							sortProperty="PRICE_NEGOTIABLE,MINIMUM_PRICE,MAXIMUM_PRICE"
							decorator="com.fiveamsolutions.tissuelocator.web.decorator.NewlineRemovalDecorator"
							media="${displayColumnMediaValue}">
							<tissuelocator:specimenPrice
								priceNegotiable="${specimen.priceNegotiable}"
								minimumPrice="${specimen.minimumPrice}"
								maximumPrice="${specimen.maximumPrice}" />
						</display:column>
					</c:if>
				</req:isUserInRole>
			</s:elseif>
			<s:elseif test="%{#propertyName eq 'status'}">
				<c:if test="${showStatus}">
					<display:column titleKey="specimen.status" sortable="true"
						sortProperty="STATUS">
						<fmt:message key="${specimen.status.resourceKey}" />
					</display:column>
				</c:if>
			</s:elseif>
			<s:elseif test="%{#propertyName eq 'participant.gender'}">
				<fmt:message var="gender"
					key="${specimen.participant.gender.resourceKey}"></fmt:message>
				<display:column titleKey="participant.gender" value="${gender}"
					media="${displayColumnMediaValue}" />
			</s:elseif>
			<s:elseif
				test="%{#propertyName eq 'participant.ethnicity'}">
				<fmt:message var="ethnicity"
					key="${specimen.participant.ethnicity.resourceKey}"></fmt:message>
				<display:column titleKey="participant.ethnicity"
					value="${ethnicity}" media="${displayColumnMediaValue}" />
			</s:elseif>
			<s:elseif test="%{#propertyName eq 'participant.races'}">
				<c:set var="participantRaces" value=""></c:set>
				<c:forEach items="${specimen.participant.races}" var="curRace">
					<fmt:message var="race" key="${curRace.resourceKey}"></fmt:message>
					<c:choose>
						<c:when test="${empty participantRaces}">
							<c:set var="participantRaces" value="${race}"></c:set>
						</c:when>
						<c:otherwise>
							<c:set var="participantRaces"
								value="${participantRaces}, ${race}"></c:set>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<display:column titleKey="participant.races"
					value="${participantRaces}" media="${displayColumnMediaValue}" />
			</s:elseif>
			<s:elseif test="%{#filtered}">
				<s:set var="displayName" value="fieldConfig.fieldDisplayName" />
				<s:set var="colValue" value="%{getObjectProperty(#attr.specimen, #propertyName)}" />

				<display:column title="${displayName}" value="${colValue}"
							media="${displayColumnMediaValue}" />
			</s:elseif>

		</s:iterator>
		
		<%--If not using configured display settings, include custom properties in csv export --%>
		<s:if test="%{!#filtered}">
            <s:iterator value="%{dynamicFieldDefinitions}">
                <s:set var="displayName" value="%{fieldDisplayName}" />
                <s:set var="colFieldName" value="%{fieldName}" />
                <s:set var="colValue" value="%{#attr.specimen.getCustomProperty(#attr.colFieldName)}" />
                <display:column title="${displayName}" value="${colValue}"
                    media="csv" />
                </s:iterator>
        </s:if>
		<display:column titleKey="specimen.protocol.name" property="protocol.name" media="csv" />
    
    <c:if test="${showEditButtons}">
        <display:column titleKey="column.action" media="html" headerClass="action" class="action">
            <c:if test="${TissueLocatorUser.crossInstitution || 
                         TissueLocatorUser.institution.id == specimen.externalIdAssigner.id}">
                <c:url value="/admin/specimen/input.action" var="editUrl">
                    <c:param name="object.id" value="${specimen.id}"/>
                </c:url>
                <a href="${editUrl}" class="btn_fw_action_col">
                    <fmt:message key="btn.edit"/>
                </a>
            </c:if>
        </display:column>
    </c:if>
</display:table>
</div>
<script type="text/javascript" language="javascript" src="<c:url value="/jquery-ui/js/jquery.tablescroll.js"/>"></script>
<script>
    $(document).ready(function () {
        $('thead').addClass('dataheader');
        $('tbody').addClass('databody');
    });
</script>

<c:if test="${showCheckboxes}">
    <script>
        function selectAllSpecimens() {
            selectAllCheckboxes('${formId}', 'selectall');
        }
    </script>
</c:if>
