<%@ tag body-content="empty" %>
<%@ attribute name="billingRecipient" required="true" type="com.fiveamsolutions.tissuelocator.data.Person" rtexprvalue="true" %>
<%@ attribute name="propertyPrefix" required="true" %>
<%@ attribute name="resourcePrefix" required="true" %>
<%@ attribute name="formName" required="true" %>
<%@ attribute name="sectionTitle" required="true" rtexprvalue="true"%>
<%@ attribute name="copyEnabled" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<script type="text/javascript">
    var billingRecipientTextFields = [ 'billingRecipientFirstName', 'billingRecipientLastName', 'billingRecipientEmail', 'billingRecipientLine1',
                                       'billingRecipientLine2', 'billingRecipientCity', 'billingRecipientZip',
                                       'billingRecipientPhone', 'billingRecipientPhoneExtension', 'billingRecipientOrganization',
                                       'billingRecipientOrganizationHidden'];
    
    var billingRecipientSelectFields = ['billingRecipientState', 'billingRecipientCountry'];
   
    $(document).ready(function () {
        var billingRecipientTabLinkMap = {'noBillingRecipient' : 'emptyBillingRecipient',
            'includeBillingRecipient' : 'billingRecipient'};
        
        var inconsistentBillingRecipientFieldMap = {'noBillingRecipient' : billingRecipientTextFields,
              'includeBillingRecipient' : []};
        
        var inconsistentMultiSelectBillingRecipientFieldMap = {'noBillingRecipient' : billingRecipientSelectFields,
                'includeBillingRecipient' : []};
        createRadioTab('billingRecipientContainer', 'billingRecipientUl', billingRecipientTabLinkMap, 
                inconsistentBillingRecipientFieldMap, inconsistentMultiSelectBillingRecipientFieldMap);
    });

    function removeBillingRecipientFields() {
    	if ($('#noBillingRecipient').get(0) && $('#noBillingRecipient').get(0).checked) {
            removeFields(billingRecipientTextFields.concat(billingRecipientSelectFields));
    	}
    }
    
    $('#${formName}').submit(function() {
        removeBillingRecipientFields();
    });
</script>

<div id="billingRecipientContainer">
		<h2 class="noline">
			<a href="javascript:;" onclick="toggleVisibility('billing_address');" class="toggle">${sectionTitle}</a>
		</h2>

		<div id="billing_address">

			<ul id="billingRecipientUl" class="radio_tabs"
				style="margin: 10px 0px 0px -20px">
				<li class="tab" style="display: none;"><a href="#none"></a></li>
				<li class="tab" style="margin-right: 12px">
				    <c:set var="checked" value="" /> 
				    <c:if test="${!includeBillingRecipient}">
						<c:set var="checked" value="checked=\"checked\"" />
					</c:if> 
					<a href="#emptyBillingRecipient"></a> 
					<input type="radio" name="includeBillingRecipient" id="noBillingRecipient" value="false" ${checked}/> 
					<label for="noBillingRecipient" class="inline">
					   <fmt:message key="specimenRequest.shipment.hideBillingRecipient"/>
				    </label>
				</li>
				<li class="tab" style="margin-right: 12px">
				    <c:set var="checked" value="" /> 
				    <c:if test="${includeBillingRecipient}">
						<c:set var="checked" value="checked=\"checked\"" />
					</c:if> 
					<a href="#billingRecipient"></a> 
					<input type="radio" name="includeBillingRecipient" id="includeBillingRecipient" value="true" ${checked}/>
					<label for="billingRecipient" class="inline">
					   <fmt:message key="specimenRequest.shipment.showBillingRecipient" />
				    </label>
			   </li>
			</ul>

			<div id="emptyBillingRecipient"></div>
			<div id="billingRecipient">
				<tissuelocator:editPerson copyEnabled="${copyEnabled}"
					propertyPrefix="${propertyPrefix}"
					resourcePrefix="${resourcePrefix}"
					idPrefix="billingRecipient"
					countryValue="${billingRecipient.address.country}"
					stateValue="${billingRecipient.address.state}"
					showInstitution="true" showFax="false" />

				<div class="clear"></div>
				<br />
				<div class="line"></div>
			</div>

		</div>
	</div>