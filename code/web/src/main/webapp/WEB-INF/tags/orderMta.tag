<%@ tag body-content="empty" %>
<%@ attribute name="mode" required="true"  %>
<%@ attribute name="viewer" required="true"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<c:if test="${mode == 'edit'}">
    <div class="formcol" style="width:320px; margin-right:20px;">
</c:if>
<ul class="nobullet">
<fmt:message key="shipment.recipientMta" var="recipientMtaLabel">
  <fmt:param value="${object.request.requestor.institution.name}" />
</fmt:message>
<fmt:message key="shipment.recipientMta.invalid" var="recipientMtaInvalid">
  <fmt:param value="${object.request.requestor.institution.name}" />
</fmt:message>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus@APPROVED}"
  name="approvedStatus" />
<s:if test="%{signedRecipientMta == null || !#approvedStatus.equals(signedRecipientMta.status)}">
  <li><p class="important">${recipientMtaInvalid}</p></li>
</s:if>
<s:else>
  <c:url value="/protected/downloadFile.action"
    var="downloadRecipientUrl">
    <c:param name="file.id" value="${signedRecipientMta.document.lob.id}" />
    <c:param name="fileName" value="${signedRecipientMta.document.name}" />
    <c:param name="contentType"
      value="${signedRecipientMta.document.contentType}" />
  </c:url>
  <li><a href="${downloadRecipientUrl}" class="file">${recipientMtaLabel} </a></li>
</s:else>
<fmt:message key="shipment.senderMta" var="senderMtaLabel">
  <fmt:param value="${object.sendingInstitution.name}" />
</fmt:message>
<fmt:message key="shipment.senderMta.invalid" var="senderMtaInvalid">
  <fmt:param value="${object.sendingInstitution.name}" />
</fmt:message>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus@APPROVED}"
  name="approvedStatus" />
<s:if test="%{signedSenderMta == null || !#approvedStatus.equals(signedSenderMta.status)}">
  <li><p class="important">${senderMtaInvalid}</p></li>
</s:if>
<s:else>
  <c:url value="/protected/downloadFile.action" var="downloadSenderUrl">
    <c:param name="file.id" value="${signedSenderMta.document.lob.id}" />
    <c:param name="fileName" value="${signedSenderMta.document.name}" />
    <c:param name="contentType"
      value="${signedSenderMta.document.contentType}" />
  </c:url>
  <li><a href="${downloadSenderUrl}" class="file"> ${senderMtaLabel} </a></li>
</s:else>

<c:if test="${mode == 'view'}">
    <c:url value="/protected/shipment/print/invoice.action" var="invoiceUrl">
        <c:param name="object.id" value="${object.id}"/>
    </c:url>
    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@CANCELED}" name="canceled" scope="page"/>
    <c:if test="${object.status != canceled}">
     <li><a class="file"
      onclick="Popup=window.open('${invoiceUrl}','Popup','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=800,height=540,left=430,top=23'); return false;"><fmt:message
      key="invoice.print" /></a></li>
    </c:if>
</c:if>

<fmt:message key="shipment.mtaAmendment" var="mtaAmendmentLabel"/>
<c:if test="${not empty object.mtaAmendment}">
  <c:choose>
    <c:when test="${object.mtaAmendment.lob.id == null}">
      <c:url var="downloadMtaAmendmentUrl"
        value="/admin/shipment/downloadMtaAmendment" />
    </c:when>
    <c:otherwise>
      <c:url value="/protected/downloadFile.action"
        var="downloadMtaAmendmentUrl">
        <c:param name="file.id" value="${object.mtaAmendment.lob.id}" />
        <c:param name="fileName" value="${object.mtaAmendment.name}" />
        <c:param name="contentType"
          value="${object.mtaAmendment.contentType}" />
      </c:url>
    </c:otherwise>
  </c:choose>
  <li><a href="${downloadMtaAmendmentUrl}" class="file">${mtaAmendmentLabel} </a></li>
</c:if>
</ul>
<c:if test="${mode == 'edit'}">
    </div>

    <div class="formcol" style="width:300px; margin-top:10px; padding:0 10px 10px 20px; border-left:1px dotted #ccc;">
        <s:file name="mtaAmendment" id="mtaAmendment" size="28"
           label="%{getText('shipment.mtaAmendment.upload')}" labelposition="top"
           labelSeparator="" />
        <s:submit value="%{getText('btn.upload')}" name="btn_upload_mta" cssClass="btn" theme="simple" cssStyle="margin-top:8px;"
              title="%{getText('btn.upload.button')}" onclick="$('#shipmentForm').get(0).action=getFormActionSave(); $('#shipmentForm').get(0).submit();" />

    </div>

</c:if>
<div class="clear"></div>
<div class="line"></div>