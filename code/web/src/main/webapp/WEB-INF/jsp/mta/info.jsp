<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="mta.info.title" /></title>
</head>
<body>

    <p><fmt:message key="mta.info.title.note" /></p>
    <h2 class="formtop"><fmt:message key="mta.info.subtitle" /></h2>
    <br />

    <tissuelocator:messages/>

    <div id="mta_info">
        <div class="box mta_listbox">
            <h3><fmt:message key="mta.info.institutionList" /></h3>
            <s:if test="%{currentMta != null}">
                <div class="current_version_num">
                    <fmt:message key="mta.info.currentVersion" />
                    <fmt:message key="default.date.format" var="dateFormat"/>
                    <strong><fmt:formatDate pattern="${dateFormat}" value="${currentMta.version}"/></strong>
                </div>
            </s:if>
            <c:set var="filterFormActionBase" value="mtaInfo"/>
            <c:if test="${pageContext.request.remoteUser != null}">
                <c:set var="filterFormActionBase" value="protected/mtaSubmission"/>
            </c:if>
            <s:form id="filterForm" action="/%{#attr.filterFormActionBase}/filter.action">
                <div class="box_541">
                    <label class="inline" for="name">
                    	<strong><fmt:message key="mta.info.institutionList.filter"/></strong>
                    </label>
                    <s:textfield name="object.name" id="name" label="%{getText('institution.name')}" theme="simple"/>
                    <s:submit value="%{getText('btn.search')}" name="btn_search" cssClass="btn" theme="simple" title="%{getText('btn.search.button')}"/>
                </div>
                <tissuelocator:institutionMtaList showRadioButtons="false" listActionBase="${filterFormActionBase}"/>
            </s:form>
        </div>

        <div class="downloadbox">
            <div class="box subbox">
                <h3><fmt:message key="mta.info.download" /></h3>
                <div class="line"></div>
                <p><fmt:message key="mta.info.download.instructions" /></p>
                <div class="line"></div>
                <c:set var="currentMtaUrl" value="#"/>
                <s:if test="%{currentMta != null}">
                    <c:url value="/mtaDownload.action" var="currentMtaUrl"></c:url>
                </s:if>
                <div class="btn_bar" style="padding-bottom:10px; text-align:center;">
                    <a href="${currentMtaUrl}" class="btn">
                        <fmt:message key="mta.info.download.link" />
                    </a>
                </div>
                <p class="note"><fmt:message key="mta.info.download.note" /></p>
            </div>

            <s:if test="%{currentMta != null}">
                <c:if test="${pageContext.request.remoteUser != null}">
                    <div class="box subbox">
                        <h3><fmt:message key="mta.info.upload" /></h3>
                        <div class="line"></div>
                        <s:form id="mtaForm" action="/protected/mtaSubmission/saveMta.action"
                            enctype="multipart/form-data" method="POST">

                            <s:file name="document" id="document" size="25" cssStyle="width:16em"
                                required="true" requiredposition="right"
                                label="%{getText('signedMta.document')}" labelposition="top" labelSeparator=""/>
                            <div class="note"><fmt:message key="mta.info.upload.note"/></div>

                            <div class="btn_bar" style="padding-bottom:10px; text-align:center;">
                                <s:submit value="%{getText('mta.info.upload.button')}" name="btn_save"
                                    cssClass="btn" theme="simple" title="%{getText('mta.info.upload.buttonTitle')}"/>
                            </div>
                        </s:form>
                    </div>
                </c:if>
            </s:if>
        </div>

    </div>
    <div class="clear"><br /></div>

</body>
</html>
