<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url var="imagesBase" value="/images"/>
<c:url value="/notYetImplemented.jsp" var="notYetImplementedUrl"/>
<html>
    <head>
        <title>
            <tissuelocator:codeDisplay code="${object.pathologicalCharacteristic}" displayUnknown="true"/>
        </title>
    </head>
    <body>
        <div class="topbtns">
            <a href="#" onclick="history.back();return false" class="btn">
                <fmt:message key="btn.back"/>
            </a>
        </div>
        <div class="box_wrapper" id="bio-details">
            <s:action namespace="/protected" name="specimen/detailsPanel/view" executeResult="true">
                <s:param name="object.id" value="%{object.id}"/>
            </s:action>
            <div class="box_inner" id="table-format">
                <tissuelocator:specimenReadOnlyView />
                <!--Privilege Based Only-->
                <req:isUserInRole role="specimenAdmin">
                    <c:if test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == object.externalIdAssigner.id}">
                        <div class="btn_bar">
                            <c:url value="/admin/specimen/input.action" var="editUrl">
                                <c:param name="object.id" value="${object.id}"/>
                            </c:url>
                            <a href="${editUrl}" class="btn">
                                <fmt:message key="btn.edit"/>
                            </a>
                            &nbsp;&nbsp;&nbsp;|&nbsp;
                            <c:url value="/admin/specimen/list.action" var="listUrl"/>
                            <a href="${listUrl}">
                                <fmt:message key="btn.cancel"/>
                            </a>
                        </div>
                    </c:if>
                </req:isUserInRole>
                <!--Privilege Based Only-->
            </div>
        </div>
        <div id="rightcol">
            <s:if test="%{!aggregateResults}">
                <div class="roundpanel_210">
                    <h3><fmt:message key="specimen.request"/></h3>
                    <req:isUserInRole role="priceViewer">
                        <h4 id="huge">
                            <tissuelocator:specimenPrice priceNegotiable="${object.priceNegotiable}"
                                minimumPrice="${object.minimumPrice}" maximumPrice="${object.maximumPrice}"/>
                        </h4>
                    </req:isUserInRole>
                    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.SpecimenStatus@AVAILABLE}"
                        name="available" scope="page"/>
                    <c:choose>
                        <c:when test="${object.status == available}">
                            <fmt:message key="specimen.addToCart" var="addToCart"/>
                            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/addToCart"
                                urlEnding=".action"/>
                            <c:url value="${outputUrl}" var="addToCartUrl">
                                <c:param name="selection" value="${object.id}"/>
                            </c:url>
                            <a href="${addToCartUrl}">
                                <img src="${imagesBase}/btn_add_to_cart.gif" alt="${addToCart}" id="add_to_cart"/>
                            </a>
                        </c:when>
                        <c:when test="${object.status != available}">
                            <fmt:message key="specimen.addToCart" var="addToCart"/>
                            <img src="${imagesBase}/btn_add_to_cart_disabled.gif" alt="${addToCart}" id="add_to_cart_disabled"/>
                        </c:when>
                    </c:choose>
                </div>
            </s:if>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".data.tabular-layout > tbody > tr:odd").addClass('odd');
            });
        </script>
    </body>
</html>
