<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimen.returnedToSourceInstitutionDate.title" /></title>
</head>
<body>
    <req:isUserInRole role="specimenAdmin">
        <s:form id="returnForm" action="/admin/specimen/popup/returnToSourceInstitution.action" target="_self">
            <s:hidden key="object.id" />

            <fmt:message key="datepicker.format" var="dateFormat"/>
            <sj:datepicker id="statusTransitionDate" name="object.statusTransitionDate"
                value="%{object.statusTransitionDate == null ? new java.util.Date() : object.statusTransitionDate}"
                displayFormat="%{#attr.dateFormat}" cssStyle="width:160px"
                labelSeparator="" labelposition="top" label="%{getText('specimen.returnToSourceInstitutionDate')}" 
                buttonImageOnly="true" changeMonth="true" changeYear="true"/>

            <div class="clear"><br /></div>

            <button onclick="javascript: $('#returnForm').get(0).submit();" id="btn_returnToSource"><fmt:message key="btn.submit"/></button>
        </s:form>
    </req:isUserInRole>
</body>
</html>
