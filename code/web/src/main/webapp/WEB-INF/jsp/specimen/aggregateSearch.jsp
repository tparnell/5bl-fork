<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>
<c:url value="/protected/specimen/search/storeSearch.action" var="storeSearchAction"/>
<html>
    <head>
        <title>
            <s:if test="%{simpleSearch}">
                <fmt:message key="specimen.search.simple.title"/>
            </s:if>
            <s:else>
                <fmt:message key="specimen.search.title"/>
            </s:else>
        </title>
        <script type="text/javascript">
            function saveSearch(saveSearchUrl) {
                $("#forwardSavedSpecimenSearchUrl").val(saveSearchUrl);
                $("#filterForm").get(0).action = '${storeSearchAction}';
                $("#filterForm").submit();
            }
        </script>
    </head>
    <body onload="setFocusToFirstControl();">

        <c:choose>
            <c:when test="${empty SpecimenSearchAction_params or startPopupOnLoad}">
                <tissuelocator:requestProcessSteps selected="search"/>
            </c:when>
            <c:otherwise>
                <tissuelocator:requestProcessSteps selected="select"/>
            </c:otherwise>
        </c:choose>

        <h1>
            <s:if test="%{simpleSearch}">
                <fmt:message key="specimen.search.simple.title"/>
            </s:if>
            <s:else>
                <fmt:message key="specimen.search.title"/>
            </s:else>
        </h1>

        <span style="float: right; margin-top: -30px">
            <s:if test="%{savedSpecimenSearch != null}">
                <s:url var="createSavedSpecimenSearchUrl"
                    value="/protected/myAccount/savedSpecimenSearches/popup/createSavedSpecimenSearch.action"
                    includeContext="false">
                    <s:param name="object.id" value="%{savedSpecimenSearch.id}"/>
                </s:url>
                <s:url var="updateSavedSpecimenSearchUrl"
                    value="/protected/myAccount/savedSpecimenSearches/popup/updateSavedSpecimenSearch.action"
                    includeContext="false">
                    <s:param name="object.id" value="%{savedSpecimenSearch.id}"/>
                </s:url>
                <a href="javascript: saveSearch('${updateSavedSpecimenSearchUrl}');">
                    <s:property value="%{'Update ' + savedSpecimenSearch.name}"/>
                </a>
                &nbsp;|&nbsp;
            </s:if>
            <s:else>
                <s:url var="createSavedSpecimenSearchUrl"
                    value="/protected/myAccount/savedSpecimenSearches/popup/createSavedSpecimenSearch.action"
                    includeContext="false"/>
            </s:else>
            <a href="javascript: saveSearch('${createSavedSpecimenSearchUrl}');">
                <fmt:message key="savedSpecimenSearch.saveAs"/>
            </a>
            &nbsp;|&nbsp;
            <s:url var="manageSavedSpecimenSearchesUrl"
                value="/protected/myAccount/savedSpecimenSearches/popup/listSavedSpecimenSearches.action"/>
            <a href="javascript:showPopWin('${manageSavedSpecimenSearchesUrl}', 675, 525, null);">
                <fmt:message key="savedSpecimenSearch.manage"/>
            </a>
        </span>
        <s:form id="filterForm" namespace="/protected" action="specimen/search/filter.action">
            <tissuelocator:specimenTabbedSearchView>
                <div id="table-format" class="roundbox_gray_wrapper">
                    <div class="roundbox_gray">
                        <tissuelocator:specimenSearchForm searchParams="${SpecimenSearchAction_params}"
                            refreshAction="/protected/specimen/search/refreshList.action"/>
                    </div>
                </div>
                <s:set var="hasErrors" value="%{hasErrors()}" scope="page"/>
                <c:choose>
                    <c:when test="${!empty SpecimenSearchAction_params and !startPopupOnLoad and !hasErrors}">
                        <h2 class="noline"><fmt:message key="specimen.search.results.title"/></h2>
                        <tissuelocator:specimenGroupTable showEditButtons="false" showCheckboxes="false"
                            formId="filterForm" checkboxProperty="selection"
                            listAction="/protected/specimen/search/filter.action"
                            viewAction="/protected/specimen/view.action"
                            exportEnabled="false" showStatus="false" showFee="true"/>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                checkTextForDisable($('#btn_addtocart').get(0), institutionAmountFieldMap);
                                for (var key in institutionAmountFieldMap) {
                                    $(institutionAmountFieldMap[key]).keyup(function() {
                                        checkTextForDisable($('#btn_addtocart').get(0), institutionAmountFieldMap);
                                    });
                                }
                            });
                        </script>
                        <div class="note"><fmt:message key="specimen.search.disclosure.results"/></div>
                        <div class="clear"><br /></div>
                        <div class="btn_bar">
                            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/addToAggregateCart"
                                urlEnding=".action" asUrl="true"/>
                            <s:submit value="%{getText('specimen.search.addToCart')}"
                                title="%{getText('specimen.search.addToCart.button')}"
                                name="btn_addtocart" id="btn_addtocart" cssClass="btn" theme="simple"
                                onclick="$('#filterForm').get(0).action = '%{#attr.request.outputUrl}'"/>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="note"><fmt:message key="specimen.search.disclosure.conditions"/></div>
                        <div class="clear"><br /></div>
                    </c:otherwise>
                </c:choose>
            </tissuelocator:specimenTabbedSearchView>
            <s:hidden id="forwardSavedSpecimenSearchUrl" name="forwardSavedSpecimenSearchUrl"/>
        </s:form>
        <c:if test="${startPopupOnLoad}">
            <s:url var="savedSpecimenSearchUrl" value="%{#parameters['forwardSavedSpecimenSearchUrl']}"/>
            <script type="text/javascript">
                $(document).ready(function () {
                    showPopWin('${savedSpecimenSearchUrl}', 600, 400, null);
                });
            </script>
        </c:if>
    </body>
</html>

