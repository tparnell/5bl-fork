<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>
<s:url var="searchSaved"
    namespace="/protected/myAccount/savedSpecimenSearches" 
    action="savedSuccessfully.action"/>
<s:set var="isUpdate" value="false"/>
<s:set var="title" value="%{getText('savedSpecimenSearch.save')}"/>
<s:if test="%{object != null && object.id != null}">
    <s:set var="isUpdate" value="true"/>
    <s:set var="title" value="%{getText('savedSpecimenSearch.update')}"/>    
</s:if>
<html>
<head>
    <title><s:property value="title"/></title>
    <script type="text/javascript">
        function searchSaved() {
            window.parent.hidePopWin(false);
            $("#saveSearchForm").get(0).target = "_top";
            $("#saveSearchForm").get(0).action = '${searchSaved}';
            $("#saveSearchForm").submit();
        }
    </script>
</head>
<body>
    <tissuelocator:messages/>

    <s:form id="saveSearchForm"
        namespace="/protected/myAccount/savedSpecimenSearches"
        action="popup/persistSavedSpecimenSearch"
        target="_self">
        
        <s:if test="%{#isUpdate}">
            <label id="label"><fmt:message key="savedSpecimenSearch.name"/></label>
            <s:property value="object.name"/>
            <s:hidden id="id" name="object.id" value="%{object.id}"/>
            <s:hidden id="name" name="object.name" value="%{object.name}"/>
        </s:if>
        <s:else>
            <s:textfield id="name"
                name="object.name"
                label="%{getText('savedSpecimenSearch.name')}"
                labelposition="top"
                labelSeparator=""
                required="true"
                requiredposition="right"
                cssStyle="width:20em"/>
        </s:else>
        
        <s:textarea id="description"
            name="object.description"
            label="%{getText('savedSpecimenSearch.description')}" 
            labelposition="top"
            labelSeparator=""
            required="false"
            requiredposition="right"
            maxlength="254"
            cssStyle="width:30em"/>

        <s:hidden id="owner" name="object.owner" value="%{#session['TissueLocatorUser']}"/>

        <div class="btn_bar">
            <s:submit id="saveSearch_submitButton"
                value="%{getText('btn.save')}"
                name="btn_save"
                theme="simple"
                title="%{getText('btn.save.button')}"
                cssClass="btn"
                cssStyle="float:left"/>
            
            <a href="#"
                onclick="window.parent.hidePopWin(false);return false;" 
                class="btn"
                style="float:left; padding:0px 8px;">
                <s:text name="btn.cancel"/>
            </a>
        </div>
    </s:form>
    <s:if test="%{saveCompleted}">    
        <script type="text/javascript">
            searchSaved();
        </script>
    </s:if>
</body>
</html>
