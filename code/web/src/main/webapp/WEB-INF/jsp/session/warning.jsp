<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<c:set var="skipHeader" value="true" scope="request"/>
<html>
    <head>
        <title><fmt:message key="sessionWarning.title" /></title>
    </head>
    <body>
        <fmt:message key="sessionWarning.title" var="title" scope="request"/>
        <fmt:message key="sessionWarning.message.part1"/>&nbsp;<span id="sessionWarningTime"></span>&nbsp;<fmt:message key="sessionWarning.message.part2"/>
        <br/>
        <br/>
        <a href="#" class="btn" onclick="window.top.resetSession();return false;">
            <fmt:message key="sessionWarning.button"/>
        </a>
        <script type="text/javascript" language="javascript">
            countdown();
        </script>
    </body>
</html>
