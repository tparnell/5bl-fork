<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>
<html>
<head>
    <title><fmt:message key="researchSupport.resources.title" /></title>
</head>
<body>

    <tissuelocator:supportMenu selectedPage="resources"/>

    <div id="maincontent" style="padding-bottom:0px;">
        <h1><fmt:message key="researchSupport.resources.title"/></h1>

        <tissuelocator:supportResources/>
    </div>

</body>
</html>
