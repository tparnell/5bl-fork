<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="questionResponse.submit.title" /></title>
</head>
<body>
    <c:url value="/admin/support/question/response/list.action" var="listUrl"/>
    <div class="topbtns">
        <a href="${listUrl}" class="btn">
            &laquo; <fmt:message key="questionResponse.submit.back"/>
        </a>
    </div>

    <div class="important">
        <fmt:message key="questionResponse.submit.action.title"/>
        <ul style="margin-bottom:5px;">
            <li><b><fmt:message key="questionResponse.submit.action.respond"/></b></li>
        </ul>
    </div>

    <tissuelocator:messages/>

    <tissuelocator:supportQuestionDetails question="${object.question}"/>

    <div class="box pad10">
        <h2 class="formtop noline nobg"><fmt:message key="questionResponse.responses.title"/></h2>

        <s:form id="responseForm" namespace="/admin/support/question/response/" action="respond.action"
            enctype="multipart/form-data" method="POST">
            <s:hidden key="object.id" />
            <s:hidden key="object.status" id="responseStatus"/>
            <table class="data">
                <thead>
                    <tr>
                        <th><div><fmt:message key="questionResponse.institution"/></div></th>
                        <th><div><fmt:message key="questionResponse.responder"/></div></th>
                        <th><div><fmt:message key="questionResponse.updated"/></div></th>
                        <th><div><fmt:message key="questionResponse.status"/></div></th>
                        <th><div><fmt:message key="questionResponse.response"/></div></th>
                    </tr>
                </thead>
                <tbody>
                    <s:iterator value="%{object.question.responses}" status="responseStatus">
                        <c:set var="rowCssClass" value=""/>
                        <s:if test="%{#responseStatus.odd}">
                            <c:set var="rowCssClass" value="odd"/>
                        </s:if>
                        <tr class="${rowCssClass}">
                            <td class="title"><s:property value="%{institution.name}"/></td>
                            <td><s:property value="%{responder.displayName}"/></td>
                            <td>
                                <s:date name="%{lastUpdatedDate}" format="%{getText('default.date.format')}"/>
                            </td>
                            <td>
                                <s:property value="%{getText(previousStatus.administratorResourceKey)}"/>
                            </td>
                            <td>
                                <s:if test="%{id == object.id}">
                                    <s:textarea name="object.response" id="response"
                                        value="%{object.response}" theme="simple"
                                        style="width:100%;margin-bottom:6px;" cols="20" rows="5"
                                        title="%{getText('questionResponse.response')}"/>
                                    <s:fielderror fieldName="object.response" cssClass="fielderror"/>
                                    <s:fielderror fieldName="object.responseValid" cssClass="fielderror"/>
                                    <s:submit value="%{getText('questionResponse.submit.online')}"
                                        name="btn_save_online" cssClass="btn" theme="simple"
                                        onclick="$('#responseStatus').val('RESPONDED');"
                                        title="%{getText('questionResponse.submit.online')}"/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <s:submit value="%{getText('questionResponse.submit.offline')}"
                                        name="btn_save_offline" cssClass="btn" theme="simple"
                                        onclick="$('#responseStatus').val('RESPONDED_OFFLINE');"
                                        title="%{getText('questionResponse.submit.offline')}"/>
                                </s:if>
                                <s:else>
                                    <s:property value="%{response}"/>
                                </s:else>
                            </td>
                        </tr>
                    </s:iterator>
                </tbody>
            </table>
        </s:form>
    </div>
</body>
</html>
