<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.review.list.title" /></title>
</head>
<body>
    <tissuelocator:requestListTabs isAdmin="true" selectedTab="letter" subtitleKey="supportLetterRequest.list.title"/>
    <tissuelocator:supportLetterRequestList isAdmin="true"/>
</body>
</html>
