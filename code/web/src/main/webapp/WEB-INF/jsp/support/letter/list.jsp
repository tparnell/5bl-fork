<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.myrequests.title" /></title>
</head>
<body>
    <tissuelocator:requestListTabs isAdmin="false" selectedTab="letter"
        subtitleKey="supportLetterRequest.list.title"/>
    <div class="topbtns">
        <a href="<c:url value="/protected/support/letter/input.action"/>" class="btn">
            <fmt:message key="supportLetterRequest.list.add"/>
        </a>
    </div>
    <tissuelocator:supportLetterRequestList isAdmin="false"/>
</body>
</html>
