<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>
<html>
<head>
    <title><fmt:message key="supportLetterRequest.edit.title" /></title>
    <script type="text/javascript">
        $(document).ready(function () {
          // the following divs are hidden so as not to display on page load
          // the following code unhides them, and the remaining code shows/hides appropriately based on current selections
            var initialHiddenDivs = ['#letterTypeLink_GRANT', '#letterTypeLink_IRB', '#letterTypeLink_OTHER'];
            for (i = 0; i < initialHiddenDivs.length; i++) {
                $(initialHiddenDivs[i]).show();
            }

            // Map of radio button id to div that should appear when selected for funding status
            var tabLinkMap = {'letterType_GRANT' : 'letterTypeLink_GRANT',
                    'letterType_IRB' : 'letterTypeLink_IRB',
                    'letterType_OTHER' : 'letterTypeLink_OTHER'};

            // Map of form fields that should be cleard when the radio button is selected
            var grantFields = ['grantType'];
            var otherFields = ['otherType'];
            var inconsistentFieldMap = {
                    'letterType_GRANT' : otherFields,
                    'letterType_IRB' : grantFields.concat(otherFields),
                    'letterType_OTHER' : grantFields
            };
            var multiSelectFields = ['fundingSource'];
            var inconsistentMultiSelectFieldMap = {
                    'letterType_GRANT' : [],
                    'letterType_IRB' : multiSelectFields,
                    'letterType_OTHER' : multiSelectFields
            };
            createRadioTab('letterTypeContainer', 'letterTypeUl', tabLinkMap,
                    inconsistentFieldMap, inconsistentMultiSelectFieldMap);
        });
    </script>
    <style>
        .ui-tabs .ui-tabs-panel
        { padding: 0px; }
        li.ui-state-default, li.ui-widget-content .ui-state-default, li.ui-widget-header .ui-state-default,
        li.ui-state-hover, li.ui-widget-content .ui-state-hover, li.ui-widget-header .ui-state-hover,
        li.ui-state-focus, li.ui-widget-content .ui-state-focus, li.ui-widget-header .ui-state-focus,
        li.ui-state-active, li.ui-widget-content .ui-state-active, li.ui-widget-header .ui-state-active
        { border: none; background: none; }
    </style>
</head>
<body>
    <c:set var="helpUrlBase" value="/protected/support/letter/info/popup/fieldHelp.action"/>

    <tissuelocator:supportMenu selectedPage="letters"/>

    <div id="maincontent" style="padding-bottom:0px;">
        <h1><fmt:message key="supportLetterRequest.edit.title"/></h1>

        <div class="pad10 note">
            <c:url value="/images/arrow_right.gif" var="rightArrowUrl"/>
            <h4><fmt:message key="supportLetterRequest.reasons.title"/></h4>
            <div>
                <img src="${rightArrowUrl}" align="absmiddle" width="12" height="12" />
                <fmt:message key="supportLetterRequest.reasons.one"/>
            </div>
            <div>
                <img src="${rightArrowUrl}" align="absmiddle" width="12" height="12" />
                <fmt:message key="supportLetterRequest.reasons.two"/>
            </div>
            <br />

            <h4><fmt:message key="supportLetterRequest.howLong.title"/></h4>
            <div>
                <img src="${rightArrowUrl}" align="absmiddle" width="12" height="12" />
                <fmt:message key="supportLetterRequest.howLong.one"/>
            </div>
        </div>
        <br />

        <div class="box pad10">
            <s:form id="letterForm" namespace="/protected/support/letter" action="save.action"
                enctype="multipart/form-data" method="POST">
                <tissuelocator:messages/>

                <s:hidden key="object.id" />
                <s:hidden name="object.requestor" value="%{#attr.TissueLocatorUser.id}"/>
                <s:hidden name="object.status" value="PENDING"/>

                <div id="investigator_edit">
                    <h4 class="green_bar"><fmt:message key="supportLetterRequest.investigator.title"/></h4>

                    <div class="pad10 select_container">
                        <div class="note">
                            <strong><fmt:message key="supportLetterRequest.investigator.note.title"/></strong>
                            <fmt:message key="supportLetterRequest.investigator.note"/>
                        </div>
                        <br />

                        <c:set var="resumeFile" value="${object.resume}"/>
                        <s:if test="%{hasErrors()}">
                            <c:set var="resumeFile" value="${TissueLocatorUser.resume}"/>
                        </s:if>
                        <tissuelocator:editPerson copyEnabled="true" propertyPrefix="object.investigator"
                            resourcePrefix="supportLetterRequest.investigator" idPrefix="investigator"
                            countryValue="${object.investigator.address.country}"
                            stateValue="${object.investigator.address.state}"
                            showInstitution="true" showFax="false"
                            showResume="true" resumeFile="${resumeFile}"/>

                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                </div>

                <div class="line"></div>

                <div id="letter_of_support">
                    <h4 class="green_bar"><fmt:message key="supportLetterRequest.letter.title"/></h4>

                    <div class="pad10">

                        <s:set var="grantTypeEnum" value="%{@com.fiveamsolutions.tissuelocator.data.support.SupportLetterType@GRANT}"/>
                        <s:set var="irbTypeEnum" value="%{@com.fiveamsolutions.tissuelocator.data.support.SupportLetterType@IRB}"/>
                        <s:set var="otherTypeEnum" value="%{@com.fiveamsolutions.tissuelocator.data.support.SupportLetterType@OTHER}"/>
                        <div id="letterTypeContainer">
                            <label for="type">
                                <fmt:message key="supportLetterRequest.type"/>
                                <span class="reqd"><fmt:message key="required.indicator"/></span>
                            </label>

                            <ul id="letterTypeUl" class="radio_tabs" style="margin: 10px 0px 0px -20px">
                                <li class="tab" style="display:none;"><a href="#empty"></a></li>
                                <s:iterator value="%{{#grantTypeEnum, #irbTypeEnum, #otherTypeEnum}}">
                                    <s:set value="%{name()}" var="optionName"/>
                                    <s:set value="%{'letterTypeFor_' + #optionName}" var="ulId"/>

                                    <c:set var="checked" value=""/>
                                    <s:if test="%{#optionName == object.type.name()}">
                                        <c:set var="checked" value="checked"/>
                                    </s:if>

                                    <c:set var="optionLink" value="#letterTypeLink_${optionName}"/>
                                    <li class="tab" style="margin-right: 12px">
                                        <a href="${optionLink}"></a>
                                        <input type="radio" name="object.type"
                                            id="letterType_${optionName}" value="${optionName}" ${checked}/>
                                        <label class="inline" for="letterType_${optionName}">
                                            <s:property value="%{getText(resourceKey)}"/>
                                        </label>
                                    </li>
                                </s:iterator>
                            </ul>
                            <s:fielderror cssClass="fielderror" fieldName="object.type"/>


                            <div id="empty"></div>
                            <div id="letterTypeLink_GRANT" style="display: none">
                                <div class="formcol" style="width:300px">
                                    <fmt:message key="supportLetterRequest.grantType" var="grantTypeDisplayName"/>
                                    <tissuelocator:helpLabel helpUrlBase="${helpUrlBase}"
                                        labelFor="grantType"
                                        fieldDisplayName="${grantTypeDisplayName}"
                                        fieldName="grantType"
                                        required="true"/>
                                    <s:textfield name="object.grantType" id="grantType"
                                            size="30" maxlength="254" cssStyle="width:20em" theme="simple"/>
                                    <s:fielderror cssClass="fielderror" fieldName="object.grantType"/>
                                </div>

                                <div class="formcol select_container" style="width:300px; display:none" id="fundingSourceWrapper">
                                    <s:select name="object.fundingSources" id="fundingSource"
                                            value="%{fundingSourceIds}"
                                            list="%{fundingSources}" listKey="id" listValue="name"
                                            multiple="true" required="true" requiredposition="right"
                                            labelSeparator="" labelposition="top"
                                            label="%{getText('supportLetterRequest.fundingSources')}" />
                                    <script type="text/javascript">
                                        $(function(){
                                          $("#fundingSource").multiselect({
                                                selectedList: 1,
                                                minWidth: 280,
                                                noneSelectedText: '<fmt:message key="select.emptyOption"/>',
                                                position: {
                                                    my: 'left top',
                                                    at: 'left bottom'
                                                }
                                            });
                                        });
                                        $(document).ready(function() {
                                            $('#fundingSourceWrapper').show();
                                        });
                                    </script>
                                </div>
                            </div>
                            <div id="letterTypeLink_IRB" style="display: none"></div>
                            <div id="letterTypeLink_OTHER" style="display: none">
                                <div class="formcol" style="width:300px">
                                    <s:textfield name="object.otherType" id="otherType"
                                            size="30" maxlength="254" cssStyle="width:20em"
                                            required="true" requiredposition="right"
                                            labelposition="top" label="%{getText('supportLetterRequest.otherType.label')}"
                                            labelSeparator=""/>
                                </div>
                            </div>
                        </div>

                        <div class="line"></div>

                        <div class="formcol" style="width:320px">
                            <s:file name="draftProtocol" id="draftProtocol" size="28" required="true" requiredposition="right"
                                label="%{getText('supportLetterRequest.draftProtocol')}" labelposition="top" labelSeparator=""/>
                            <s:fielderror fieldName="object.draftProtocol" cssClass="fielderror"/>
                        </div>

                        <div class="formcol" style="width:320px">
                            <fmt:message key="supportLetterRequest.draftLetter" var="draftLetterName"/>
                            <tissuelocator:helpLabel helpUrlBase="${helpUrlBase}"
                                labelFor="draftLetter"
                                fieldDisplayName="${draftLetterName}"
                                fieldName="draftLetter"
                                required="false"/>
                            <s:file name="draftLetter" id="draftLetter" size="28" theme="simple"/>
                            <s:fielderror fieldName="object.draftLetter" cssClass="fielderror"/>
                        </div>

                        <div class="clear"></div>

                        <div class="formcol" style="width:320px">
                            <fmt:message key="supportLetterRequest.intendedSubmissionDate" var="submissionDateName"/>
                            <tissuelocator:helpLabel helpUrlBase="${helpUrlBase}"
                                labelFor="intendedSubmissionDate"
                                fieldDisplayName="${submissionDateName}"
                                fieldName="intendedSubmissionDate"
                                required="false"/>
                            <sj:datepicker id="intendedSubmissionDate" name="object.intendedSubmissionDate"
                                displayFormat="%{getText('datepicker.format')}"
                                buttonImageOnly="true" changeMonth="true" changeYear="true"/>
                        </div>

                        <div class="clear" style="height:10px;"></div>
                        <div class="line"></div>

                        <s:textarea name="object.comments" id="comments"
                            cols="50" rows="3" cssStyle="width:100%; height:100px"
                            labelposition="top" label="%{getText('supportLetterRequest.comments')}" labelSeparator=""/>

                        <div class="clear"><br /></div>
                        <div class="line"></div>

                        <div class="btn_bar">
                            <s:submit value="%{getText('supportLetterRequest.submit')}" name="btn_submit"
                                id="btn_submit" cssClass="btn" theme="simple" title="%{getText('supportLetterRequest.submit')}"/>
                        </div>

                    </div>
                </div>
            </s:form>
        </div>
    </div>

</body>
</html>
