<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="protocol.title" /></title>
</head>
<body>
    <div class="topbtns">
        <a href="<c:url value="/admin/protocol/input.action"/>" class="btn">
            <fmt:message key="protocol.add"/>
        </a>
    </div>

    <tissuelocator:messages/>

    <s:form id="filterForm" namespace="/admin" action="protocol/filter.action">
        <!--Filters-->
        <div class="roundbox_gray_wrapper">
            <div class="roundbox_gray">
                <div class="float_left">
                </div>

                <div class="float_right">
                    <label class="inline" for="name">
                    	<strong><fmt:message key="protocol.list.filter"/></strong>
                    </label>
                    <s:textfield name="object.name" label="%{getText('protocol.name')}" theme="simple" id="name"/>
                    <s:submit value="%{getText('btn.search')}" name="btn_search" cssClass="btn" theme="simple" title="%{getText('btn.search.button')}"/>
                </div>

                <div class="clear"></div>
            </div>
        </div>

        <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
        <fmt:message key="default.date.format" var="dateFormat"/>
        <display:table name="objects" requestURI="/admin/protocol/list.action"
                uid="protocol" pagesize="${tablePageSize}" sort="external" export="true">
                <tissuelocator:displaytagProperties/>
                <display:setProperty name="export.csv.filename" value="collectionprotocols.csv"/>

            <display:column titleKey="protocol.name" sortProperty="NAME" sortable="true" media="html">
                <c:choose>
                    <c:when test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == protocol.institution.id}">
                        <c:url var="editUrl" value="/admin/protocol/input.action">
                            <c:param name="object.id" value="${protocol.id}"/>
                        </c:url>
                        <a href="${editUrl}">${protocol.name}</a>
                    </c:when>
                    <c:otherwise>
                        ${protocol.name}
                    </c:otherwise>
                </c:choose>
            </display:column>
             <display:column titleKey="protocol.name" sortProperty="NAME" sortable="true" media="csv">
                ${protocol.name}
             </display:column>
            <display:column titleKey="protocol.institution.name" property="institution.name"
                sortProperty="INSTITUTION" sortable="true"/>
            <display:column titleKey="protocol.startDate" sortProperty="START_DATE" sortable="true">
                <fmt:formatDate pattern="${dateFormat}" value="${protocol.startDate}"/>
            </display:column>
            <display:column titleKey="protocol.endDate" sortProperty="END_DATE" sortable="true">
                <fmt:formatDate pattern="${dateFormat}" value="${protocol.endDate}"/>
            </display:column>
            <display:column titleKey="protocol.contact.firstName" property="contact.firstName"
                sortProperty="FIRST_NAME" sortable="true"/>
            <display:column titleKey="protocol.contact.lastName" property="contact.lastName"
                sortProperty="LAST_NAME" sortable="true"/>
            <display:column titleKey="protocol.contact.email" property="contact.email"
                sortProperty="EMAIL" sortable="true"/>
            <display:column titleKey="protocol.contact.phone"
                sortProperty="PHONE" sortable="true">
                ${protocol.contact.phone}
                <tissuelocator:phoneExtensionDisplay extension="${protocol.contact.phoneExtension}"/> 
            </display:column>
            <display:column titleKey="column.action" media="html" headerClass="action" class="action">
                <c:if test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == protocol.institution.id}">
                    <c:url var="editUrl" value="/admin/protocol/input.action">
                        <c:param name="object.id" value="${protocol.id}"/>
                    </c:url>
                    <a href="${editUrl}" class="btn_fw_action_col"><fmt:message key="btn.edit" /></a>
                </c:if>
            </display:column>
        </display:table>
    </s:form>
</body>
</html>
