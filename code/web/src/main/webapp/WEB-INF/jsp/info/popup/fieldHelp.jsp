<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<html>
    <head>
        <title>${helpFieldDisplayName}</title>
    </head>
    <body>
    
        <div>
            <p>
                <s:set var="fieldConfig" value="%{helpConfig.getFieldHelpConfig(helpFieldName)}"/>
                <s:set var="helpText" value="%{#attr.fieldConfig.helpText}"/>
                <s:if test="%{#attr.fieldConfig.helpTextLinks != null}">
                    <s:set var="replacements" value="%{new java.util.ArrayList()}"/>
                    <s:iterator  value="%{#attr.fieldConfig.helpTextLinks}" status="iterStatus">
                        <s:url var="helpHref" value="%{linkHref}"/>
                        <c:set var="linkReplacement"><a href="${helpHref}" target="_blank"><s:property value="%{linkText}"/></a></c:set>
                        <s:set var="replacement" value="%{#attr.replacements.add(#attr.linkReplacement)}"/>
                    </s:iterator>
                    <s:set var="helpText" value="%{#attr.fieldConfig.getHelpTextWithReplacements(#attr.replacements)}"/>
                </s:if>
               ${helpText}
            </p>
        </div>
            
        <div class="clear"></div>
        
        <div class="btn_bar">
            <a href="#" onclick="window.parent.hidePopWin(true);return false">Close</a>

        </div>
        
    </body>
</html>