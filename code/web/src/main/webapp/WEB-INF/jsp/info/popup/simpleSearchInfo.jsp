<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<html>
    <head>
        <title><fmt:message key="specimen.search.simple.info.title"/></title>
    </head>
    <body>
    
        <div>
            <p>
               <fmt:message key="specimen.search.simple.info"/> 
            </p>
        </div>
            
        <div class="clear"></div>
        
        <div class="btn_bar">
            <a href="#" onclick="window.parent.hidePopWin(true);return false">Close</a>

        </div>
        
    </body>
</html>