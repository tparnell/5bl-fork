<%@ page language="java" errorPage="/error.jsp" pageEncoding="UTF-8" contentType="text/html;charset=utf-8" %>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title><fmt:message key="tissuelocator.title" /> - <decorator:title default="Print" /></title>

        <style type="text/css">
            BODY {font-family:'lucida grande','segoe ui',verdana, arial, helvetica, sans-serif; color:#000; }
            #invoice { width:620px; margin:0 auto }
            TABLE { border-collapse:collapse; float:left; margin:0 20px 20px 0; }
            H1 { text-align:right; text-transform:uppercase }
            H2 { font-size:12pt }
            TH { vertical-align:bottom; background:#e7e7e7; text-align:left; font-size:8.5pt;  border-top:1px solid #ccc; border-bottom:1px solid #ccc; padding:5px }
            TD { vertical-align:top; text-align:left; font-size:8.5pt; border-top:1px solid #ccc; border-bottom:1px solid #ccc; padding:5px }
            TD.carttitle { font-weight:bold } 
            DIV.button { padding:10px; float:right; }
            .logo { margin-top:-70px; margin-bottom:10px; }
            .fullwidth { width:100%; margin-right:0; }
            .alignright { text-align:right }
            .clear { clear:both }
            /* --- Printer Output --- */
            @media print {
                DIV.noprint { display:none; }
            }
        </style>
        <decorator:head/>
    </head>

    <body onload="window.print()">
        <decorator:body/>
    </body>
</html>
