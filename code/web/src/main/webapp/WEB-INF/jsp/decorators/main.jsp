<%@ page language="java" errorPage="/error.jsp" pageEncoding="UTF-8" contentType="text/html;charset=utf-8" %>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title><fmt:message key="tissuelocator.title" /> - <decorator:title default="Welcome"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="X-UA-Compatible" content="IE=8" />
        <%@ include file="/WEB-INF/jsp/decorators/headIncludes.jsp" %>
        <link rel="address bar icon" href="<c:url value="/images/favicon.ico"/>" />
        <link rel="icon" href="<c:url value="/images/favicon.ico"/>" type="image/x-icon" />
        <link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/>" type="image/x-icon" />
        <decorator:head/>
        <script type="text/javascript">
            anylinkmenu.init("menuanchorclass");
            $(document).ready(function () {
                $('div.exportlinks a').addClass('btn');
                $('.truncate').textOverflow();
            });
        </script>
    </head>

    <body onkeypress="monitorActivity();" onclick="monitorActivity();">
        <a name="top"></a><a href="#content" id="navskip"><fmt:message key="main.skip"/></a>
        <div id="wrapper">
            <div id="wrapper_inner">


                <%@ include file="/WEB-INF/jsp/common/header.jsp" %>

                <!--Content-->

                <div id="content_wrapper">
                    <div id="content">

                        <c:if test="${skipHeader != true}">
                            <fmt:message key="tissuelocator.title" var="defaultTitle" />
                            <h1><decorator:title default="${defaultTitle}"/></h1>
                        </c:if>
                        <decorator:body/>
                        <div class="clear"></div>

                    </div>
                </div>
                <div id="content_footer"><br /></div>

                <!--/Content-->

            </div>

            <%@ include file="/WEB-INF/jsp/common/footer.jsp" %>

        </div>
        <c:if test="${!empty TissueLocatorUser}">
            <script type="text/javascript" language="javascript">
                $(document).ready(function () {
                    startSessionWarningTimer();
                });
            </script>
        </c:if>
    </body>
</html>
