<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url value="/admin/request/review/fixedConsortium/list.action" var="listUrl"/>
<c:url var="refreshUrl" value="/admin/request/review/fixedConsortium/input.action">
    <c:param name="object.id" value="${object.id}"/>
    <c:param name="commentSaved" value="true"/>
</c:url>
<fmt:message key="default.date.format" var="dateFormat"/>
<fmt:message key="default.datetime.format" var="datetimeFormat"/>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING}" name="pending"/>
<s:set value="%{#pending.equals(object.status)}" name="isPending"/>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_FINAL_DECISION}" name="pendingDecision"/>
<s:set value="%{#pendingDecision.equals(object.status)}" name="isPendingDecision" scope="page"/>
<html>
<head>
    <title><fmt:message key="specimenRequest.review.title" />: ${object.id} (<fmt:message key="${object.status.resourceKey}"/>)</title>
    <script type="text/javascript">
        function returnRefresh(returnVal) {
            window.location = '${refreshUrl}';
        }

        var numVotes = <s:property value="%{object.consortiumReviews.size()}"/>;
        var reviewerInstitutionMap = [
             <s:iterator value="%{reviewers}" status="reviewerIterStatus">
                 [<s:property value="%{id}"/>, <s:property value="%{institution.id}"/>]
                 <s:if test="%{!#reviewerIterStatus.last}"> , </s:if>
             </s:iterator>
         ];

        function setReviewerInstitutions() {
            for (var voteIndex = 1; voteIndex <= numVotes; voteIndex++) {
                var reviewer = $('#consortiumReviewUser' + voteIndex).get(0).value;
                var institutionField = $('#consortiumReviewInstitution' + voteIndex).get(0);
                for (var mapIndex = 0; mapIndex < reviewerInstitutionMap.length; mapIndex++) {
                    if (reviewer == reviewerInstitutionMap[mapIndex][0]) {
                        institutionField.value = reviewerInstitutionMap[mapIndex][1];
                    }
                }
            }
        }
    </script>
</head>
<body onload="setFocusToFirstControl();">
<div class="topbtns">
    <c:url var="summaryUrl" value="/admin/request/review/fixedConsortium/viewSummary.action" >
        <c:param name="object.id" value="${object.id}"/>
    </c:url>
    <a href="${listUrl}" class="btn"><fmt:message key="btn.backToList"/></a>
    <a href="${summaryUrl}" class="btn"><fmt:message key="specimenRequest.review.details"/></a>
</div>

<c:set var="needsReviewerAssignment" value="${false}"/>
<req:isUserInRole role="scientificReviewerAssigner">
    <s:if test="%{reviewProcess.userActionAnalyzer.needsReviewerAssignment(#attr.TissueLocatorUser, object)}">
        <c:set var="needsReviewerAssignment" value="${true}"/>
    </s:if>
</req:isUserInRole>
<req:isUserInRole role="consortiumReviewVoter">
    <s:if test="%{reviewProcess.userActionAnalyzer.needsConsortiumReview(#attr.TissueLocatorUser, object)}">
        <c:set var="needsConsortiumReview" value="${true}"/>
    </s:if>
</req:isUserInRole>
<c:set var="needsFinalComment" value="${false}"/>
<req:isUserInRole role="leadReviewer">
    <s:if test="%{reviewProcess.userActionAnalyzer.needsFinalComment(#attr.TissueLocatorUser, object)}">
        <c:set var="needsFinalComment" value="${true}"/>
    </s:if>
</req:isUserInRole>
<c:set var="needsResearcherNotification" value="${false}"/>
<req:isUserInRole role="reviewDecisionNotifier">
    <s:if test="%{reviewProcess.userActionAnalyzer.needsResearcherNotification(#attr.TissueLocatorUser, object)}">
        <c:set var="needsResearcherNotification" value="${true}"/>
    </s:if>
</req:isUserInRole>
<c:if test="${needsReviewerAssignment || needsConsortiumReview || needsFinalComment || needsResearcherNotification}">
    <div class="topimportant">
        <p><fmt:message key="specimenRequest.review.outstanding"/><br></p>
        <ul>
            <c:if test="${needsReviewerAssignment}">
                <li style="font-size:100%">
                    <fmt:message key="specimenRequest.review.outstanding.assignReviewer"/>
                </li>
            </c:if>
            <c:if test="${needsConsortiumReview}">
                <li style="font-size:100%">
                    <fmt:message key="specimenRequest.review.outstanding.consortiumReview"/>
                </li>
            </c:if>
            <c:if test="${needsFinalComment}">
                <li style="font-size:100%">
                    <fmt:message key="specimenRequest.review.outstanding.finalComment"/>
                </li>
            </c:if>
            <c:if test="${needsResearcherNotification}">
                <li style="font-size:100%">
                    <fmt:message key="specimenRequest.review.outstanding.researcherNotification"/>
                </li>
            </c:if>
        </ul>
    </div>
    <br/>
</c:if>

<tissuelocator:messages/>
<s:fielderror cssClass="fielderror">
    <s:param>consortiumReview.commentValid</s:param>
</s:fielderror>

<c:if test="${isPending && !empty object.externalComment}">
    <div class="topcomment">
        <h2><fmt:message key="specimenRequest.review.decision.header"/></h2>
        <p><fmt:message key="specimenRequest.review.decision.externalComment.header"/></p>
        <p class="comment">${object.externalComment}</p>
        <c:if test="${!empty object.revisionComment}">
            <br/>
            <p><fmt:message key="specimenRequest.review.decision.revisionComment.header"/></p>
            <p class="comment">${object.revisionComment}</p>
        </c:if>
    </div>
</c:if>

<c:if test="${isPendingDecision}">
    <req:isUserInRole role="leadReviewer">
        <h2 class="noline huge"><fmt:message key="specimenRequest.review.finalComment.title"/></h2>

        <s:form id="externalCommentForm" action="/admin/request/review/fixedConsortium/finalizeVote.action">
            <s:hidden key="object.id" />
            <div class="box">
                <div class="pad10">
                    <label for="finalVote" style="display:inline;">
                        <fmt:message key="specimenRequest.review.finalComment.votingResults"/>
                        <span class="reqd">
                            <fmt:message key="required.indicator"/>
                        </span>
                    </label>
                    <s:select name="finalVote" id="finalVote" value="%{finalVote}"
                        list="%{@com.fiveamsolutions.tissuelocator.data.Vote@values()}"
                        listValue="%{getText(resourceKey)}" theme="simple"
                        headerKey="" headerValue="%{getText('select.emptyOption')}"
                        required="true" requiredposition="right" />
                    <s:fielderror cssClass="fielderror">
                        <s:param>finalVote</s:param>
                    </s:fielderror>
                </div>
            </div>

            <div class="box">
                <div class="pad10">
                    <s:textarea name="object.externalComment" id="externalComment"
                                cols="20" rows="6" cssStyle="width:900px;"
                                labelposition="top" label="%{getText('specimenRequest.review.externalComment.label')}"
                                labelSeparator="" required="true" requiredposition="right"/>

                    <div class="btn_bar">
                        <s:submit value="%{getText('specimenRequest.review.submit')}" name="btn_save"
                            cssClass="btn" theme="simple" title="%{getText('specimenRequest.review.submit.button')}"/>
                    </div>
                </div>
            </div>
        </s:form>
    </req:isUserInRole>
    <req:isUserInRole role="reviewDecisionNotifier">
        <c:if test="${needsResearcherNotification}" >
            <div class="box">
                <div class="pad10">
                    <fmt:message key="specimenRequest.review.finalComment.votingResults" />:
                    <strong><fmt:message key="${finalVote.resourceKey}"/></strong>
                </div>
            </div>
            <div class="box">
                <div class="pad10">
                    <p><fmt:message key="specimenRequest.review.externalComment.label" />:</p>
                    <p class="comment">${object.externalComment}</p>
                </div>
            </div>
            <div class="box">
                <div class="pad10">
                    <div class="btn_bar">
                        <c:url value="/admin/request/review/fixedConsortium/notifyResearcherOfDecision.action" var="notifyUrl">
                            <c:param name="object.id" value="${object.id}"/>
                        </c:url>
                        <a href="${notifyUrl}" class="btn">
                            <fmt:message key="specimenRequest.review.outstanding.researcherNotification"/>
                        </a>
                    </div>
                </div>
            </div>
        </c:if>
    </req:isUserInRole>
</c:if>

<tissuelocator:votingResultsDisplay specimenRequest="${object}" />

<h2 class="noline huge"><fmt:message key="specimenRequest.review.consortium.title" /></h2>

<div class="box">
    <req:isUserInRole role="scientificReviewerAssigner">
        <s:set var="isScientificReviewerAssigner" value="true"/>
    </req:isUserInRole>
    <req:isUserInRole role="consortiumReviewVoter">
        <c:set var="isConsortiumReviewer" value="true" />
    </req:isUserInRole>

    <table class="data" title="<fmt:message key="specimenRequest.review.consortium.title" />" id="consortiumReviewTable">
        <tr>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.reviewer" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.date" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.vote" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.comment" /></th>
            <th style="padding: 8px;" scope="col" class="action"><fmt:message key="specimenRequest.review.columnHeader.action" /></th>
        </tr>

            <s:if test="%{#isScientificReviewerAssigner && #isPending}">
                <s:form id="consortiumReviewForm" action="/admin/request/review/fixedConsortium/assignScientificReviewer.action">
                    <s:hidden key="object.id" />
                    <c:forEach items="${object.consortiumReviews}" var="currentReview" varStatus="loopStatus">
                        <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if> >
                            <td class="hilite">
                                <s:select name="consortiumReviews[%{#attr.loopStatus.count - 1}].user"
                                            id="consortiumReviewUser%{#attr.loopStatus.count}"
                                            value="%{#attr.currentReview.user.id}" theme="simple"
                                            list="reviewers" listKey="id" listValue="%{firstName + ' ' + lastName}"
                                            headerKey="" headerValue="%{getText('select.emptyOption')}"
                                            title="%{getText('specimenRequest.review.reviewers.select')}" cssStyle="max-width:160px;"/>
                                <s:fielderror cssClass="fielderror">
                                    <s:param>
                                        <s:property value="%{consortiumReviews[%{#attr.loopStatus.count - 1}]}"/>
                                    </s:param>
                                </s:fielderror>
                                <s:hidden name="consortiumReviews[%{#attr.loopStatus.count - 1}].institution"
                                            id="consortiumReviewInstitution%{#attr.loopStatus.count}"
                                            value="%{#attr.currentReview.institution.id}"/>
                            </td>
                            <td class="hilite"><fmt:formatDate pattern="${dateFormat}" value="${currentReview.date}"/></td>
                            <td class="hilite">
                                <c:if test="${currentReview.vote != null}">
                                    <fmt:message key="${currentReview.vote.resourceKey}"  />
                                </c:if>
                            </td>
                            <td class="hilite">${currentReview.comment}</td>
                            <td class="hilite action"></td>
                        </tr>
                    </c:forEach>
                </s:form>
            </s:if>
            <s:else>
            <c:forEach items="${object.consortiumReviews}" var="currentReview" varStatus="loopStatus">
                <c:choose>
                <c:when test="${empty isScientificReviewerAssigner && isConsortiumReviewer == true && isPending && currentReview.id == consortiumReview.id}">
                <s:form id="consortiumReviewForm" action="/admin/request/review/fixedConsortium/saveConsortiumReview.action">
                    <s:hidden key="object.id" />
                    <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if>>
                        <td class="hilite"> <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${true}" value="${currentReview.user.firstName} ${currentReview.user.lastName}"
                                maxWordLength="20"/></td>
                        <td class="hilite"><fmt:formatDate pattern="${dateFormat}" value="${currentReview.date}" /></td>
                        <td class="hilite">
                            <s:select name="consortiumReview.vote" id="consortiumReviewVote" value="%{consortiumReview.vote}"
                                    list="%{@com.fiveamsolutions.tissuelocator.data.Vote@values()}"
                                    listValue="%{getText(resourceKey)}" headerKey=""
                                    headerValue="%{getText('select.emptyOption')}" theme="simple"
                                    title="%{getText('consortiumReview.vote.select')}" />
                            <s:fielderror cssClass="fielderror">
                                <s:param>consortiumReview.vote</s:param>
                            </s:fielderror>
                        </td>
                        <td class="hilite">
                            <s:textarea name="consortiumReview.comment" id="consortiumReviewComment"
                                cols="30" rows="3" required="false" theme="simple" title="%{getText('consortiumReview.comment.textarea')}"/>
                            <s:fielderror cssClass="fielderror">
                                <s:param>consortiumReview.comment</s:param>
                            </s:fielderror>
                        </td>
                        <td class="hilite action" style="white-space: nowrap">
                            <div class="btn_bar">
                                <s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn" theme="simple" title="%{getText('btn.save.button')}"/>
                                <c:url var="declineUrl" value="/admin/request/review/fixedConsortium/declineReview.action" >
                                    <c:param name="object.id" value="${object.id}"/>
                                </c:url>
                                <a href="${declineUrl}" class="btn_fw_action_col"><fmt:message key="btn.decline"/></a>
                            </div>
                        </td>
                    </tr>
                </s:form>
            </c:when>
            <c:otherwise>
                <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if>>
                    <td> <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${true}" value="${currentReview.user.firstName} ${currentReview.user.lastName}"
                            maxWordLength="20"/>
                    </td>
                    <td><fmt:formatDate pattern="${dateFormat}" value="${currentReview.date}" /></td>
                    <td><c:if test="${currentReview.vote != null}">
                        <fmt:message key="${currentReview.vote.resourceKey}" />
                    </c:if></td>
                    <td>${currentReview.comment}</td>
                    <td class="action"></td>
                </tr>
            </c:otherwise>
        </c:choose>
        </c:forEach>
        </s:else>
    </table>
    <c:if test="${isScientificReviewerAssigner == true && isPending}">
        <div class="btn_bar">
            <s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn" theme="simple"
                onclick="setReviewerInstitutions();$('#consortiumReviewForm').get(0).submit()" title="%{getText('btn.save.button')}"/>
        </div>
    </c:if>
</div>

<h2 class="noline huge"><fmt:message key="specimenRequest.review.comments.title" /></h2>

<c:if test="${isPending}">
    <req:isUserInRole role="reviewCommenter">
    <div class="topbtns">
        <c:url var="loadCommentFormUrl" value="/admin/request/review/fixedConsortium/popup/loadCommentForm.action" >
            <c:param name="object.id" value="${object.id}"/>
        </c:url>
        <a id="btn_addComment" class="btn" href="javascript: showPopWin('${loadCommentFormUrl}', 400, 225, returnRefresh);"><fmt:message key="btn.addComment"/></a>
    </div>
    </req:isUserInRole>
</c:if>

<div class="box">
    <c:choose>
    <c:when test="${empty object.comments}">
        <p>No comments have been entered.</p>
    </c:when>
    <c:otherwise>
    <table class="data" title="<fmt:message key="specimenRequest.review.comments.title" />">
        <tr>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.user"/></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.internalComment"/></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.date"/></th>
        </tr>
    <c:forEach items="${object.comments}" var="currentComment" varStatus="loopStatus">
        <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if> >
            <td>${currentComment.user.firstName} ${currentComment.user.lastName}</td>
            <td>${currentComment.comment}</td>
            <td><fmt:formatDate pattern="${datetimeFormat}" value="${currentComment.date}"/></td>
        </tr>
    </c:forEach>
    </table>
    </c:otherwise>
    </c:choose>
</div>
</body>
</html>
