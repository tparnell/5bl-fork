<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.lineItems" /></title>
</head>
<body>
    <s:set name="lineItems" value="%{getLineItemsByInstitution(#attr.institution)}"/>
    <tissuelocator:cartTable showFinalPriceColumn="false" showOrderFulfillmentForms="false"
        requestLineItems="${lineItems}" showSpecimenLinks="false" />
</body>
