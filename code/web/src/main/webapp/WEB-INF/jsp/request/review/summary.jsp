<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="false" scope="request"/>
<html>
<head>
    <title><fmt:message key="specimenRequest.review.title" />: ${object.id} (<fmt:message key="${object.status.resourceKey}"/>)</title>
</head>
<body onload="setFocusToFirstControl();">

    <div class="topbtns">
    <c:choose>
          <c:when test="${object.requestor.id != TissueLocatorUser.id}">
            <c:url var="backUrl" value="/admin/request/review/input.action">
                <c:param name="object.id" value="${object.id}"/>
            </c:url>
            <a href="${backUrl}" class="btn">
                <fmt:message key="specimenRequest.review.back"/>
            </a>
          </c:when>
          <c:otherwise>
            <a href="#" onclick="history.back();return false" class="btn">
                <fmt:message key="btn.back"/>
            </a>
          </c:otherwise>
        </c:choose>
    </div>

    <c:set var="displayVotingResults" value="false"/>
    <s:if test="%{displayRequestReviewVotes}">
        <c:set var="displayVotingResults" value="true"/>
    </s:if>

    <tissuelocator:aggregateLineItemDisplay displayVotingResults="${displayVotingResults}"/>

    <s:set var="hasOrderLineItems" value="%{!object.orderLineItems.isEmpty()}" scope="page"/>
    <c:if test="${hasOrderLineItems}">
        <h2 class="formtop">
            <a href="javascript:;" onclick="toggleVisibility('ordercart');" class="toggle">
               <fmt:message key="specimenRequest.lineItems"/>
            </a>
        </h2>

        <tissuelocator:cartTable showFinalPriceColumn="false" requestLineItems="${object.orderLineItems}"
            showOrderFulfillmentForms="false" showSpecimenLinks="true" cartPrefix="order"/>
        <br />
    </c:if>

    <s:set var="hasLineItems" value="%{!object.lineItems.isEmpty()}" scope="page"/>
    <c:if test="${hasLineItems}">
        <fmt:message key="specimenRequest.view.unavailable" var="lineItemsLabel"/>
        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING}" name="pending" scope="page"/>
        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_REVISION}"
            name="pendingRevision" scope="page"/>
        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_FINAL_DECISION}"
            name="pendingDecision" scope="page"/>
        <c:if test="${object.status == pending || object.status == pendingRevision || object.status == pendingDecision}">
            <fmt:message key="specimenRequest.lineItems" var="lineItemsLabel"/>
        </c:if>
        <h2 class="formtop">
            <a href="javascript:;" onclick="toggleVisibility('cart');" class="toggle">
               ${lineItemsLabel}
            </a>
        </h2>
    </c:if>

    <tissuelocator:requestReadOnlyView specimenRequest="${object}" showCart="${hasLineItems}"
        displayVotingResults="${displayVotingResults}"/>

    <div class="clear"><br/></div>
    <div class="btn_bar">
        <a href="#" onclick="history.back();return false" class="btn">
            <fmt:message key="btn.back"/>
        </a>
    </div>
    <div class="clear"></div>
</body>
</html>
