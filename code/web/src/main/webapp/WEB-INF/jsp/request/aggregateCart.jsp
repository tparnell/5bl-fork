<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>
<c:url var="imagesBase" value="/images"/>
<c:url value="/notYetImplemented.jsp" var="notYetImplementedUrl"/>
<html>
    <head>
        <title>
            <fmt:message key="cart.title"/>
        </title>
    </head>
    <body>

        <tissuelocator:requestProcessSteps selected="request"/>
        <h1><fmt:message key="cart.title"/></h1>

        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/updateAggregateCart" urlEnding=".action"/>
        <s:form theme="simple" action="%{#attr.request.outputUrl}">

        <p><fmt:message key="cart.message"/></p>
        <c:if test="${!empty specimenRequestError}">
            <div class="toperror">
                <p><fmt:message key="${specimenRequestError}"/></p>
            </div>
        </c:if>
        <tissuelocator:messages breakCount="1" escape="false"/>

        <tissuelocator:externalCommentDisplay specimenRequest="${object}"/>

        <c:url value="/protected/specimen/search/refreshList.action" var="specimenSearchUrl"/>

        <div class="clear"></div>

        <!--Data Table-->
        <table class="data" id="aggregateLineItemTable">

            <!--Specific Selections Line Item Data Table-->
            <s:if test="%{(specificLineItems.size() > 0) || ((generalLineItems.size() == 0) && (specificLineItems.size() == 0))}">
                <tissuelocator:editableAggregateLineItemTable
                    requestLineItems="${specificLineItems}"
                    lineItemPrefix="specific"
                    titleKey="aggregateCartTable.specificCart.title"
                    removeUrl="/protected/request/removeFromSpecificAggregateCart"/>
            </s:if>
            <!--/Specific Selections Line Item Data Table-->

            <!--General Population Request Data Table-->
            <s:if test="%{generalLineItems.size() > 0}">
                <tissuelocator:editableAggregateLineItemTable
                    requestLineItems="${generalLineItems}"
                    lineItemPrefix="general"
                    titleKey="aggregateCartTable.generalCart.title"
                    removeUrl="/protected/request/removeFromGeneralAggregateCart"/>
            </s:if>

        </table>
        <div class="clear"><br /></div>

        <div class="btn_bar">
            <s:submit value="%{getText('cart.request')}" name="submit"
                    id="btn_request" cssClass="btn" theme="simple" title="%{getText('cart.request.button')}"/>
            <s:submit value="%{getText('cart.update')}" name="submit"
                    id="btn_update" cssClass="btn" theme="simple" title="%{getText('cart.update.button')}"/>
            <s:submit value="%{getText('specimenRequest.edit.saveDraft')}" name="submit"
                    id="btn_saveDraft" cssClass="btn" theme="simple" title="%{getText('specimenRequest.edit.saveDraft.button')}"/>
            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/emptyAggregateCart" urlEnding=".action" asUrl="true"/>

            <a href="${specimenSearchUrl}" class="btn">
                <fmt:message key="cart.searchMore"/>
            </a>

            <a href="${outputUrl}" class="btn">
                <fmt:message key="cart.empty"/>
            </a>
            <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@DRAFT}"
                name="draft" scope="page"/>
            <c:if test="${object.status == draft}">
                <tissuelocator:determinePersistenceType urlPrefix="/protected/request/delete" urlEnding=".action"/>
                <c:url var="deleteRequestUrl" value="${outputUrl}">
                    <c:param name="object.id" value="${object.id}"/>
                </c:url>
                <fmt:message key="specimenRequest.delete.confirm" var="confirmDelete"/>
                <a href="#" class="btn" onclick="confirmAndContinue('${confirmDelete}','${deleteRequestUrl}'); return false;"><fmt:message key="btn.delete.draft" /></a>
            </c:if>
        </div>
        </s:form>
    </body>
</html>
