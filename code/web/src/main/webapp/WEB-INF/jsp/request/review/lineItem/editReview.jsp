<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url value="/admin/request/review/lineItem/list.action" var="listUrl"/>
<c:url var="refreshUrl" value="/admin/request/review/lineItem/input.action">
    <c:param name="object.id" value="${object.id}"/>
    <c:param name="commentSaved" value="true"/>
</c:url>
<fmt:message key="default.datetime.format" var="datetimeFormat"/>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING}" name="pending"/>
<s:set value="%{#pending.equals(object.status)}" name="isPending" scope="page"/>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_FINAL_DECISION}" name="pendingDecision"/>
<s:set value="%{#pendingDecision.equals(object.status)}" name="isPendingDecision" scope="page"/>
<html>
<head>
    <title><fmt:message key="specimenRequest.review.title" />: ${object.id} (<fmt:message key="${object.status.resourceKey}"/>)</title>
    <script type="text/javascript">
        function returnRefresh(returnVal) {
            window.location = '${refreshUrl}';
        }
    </script>
</head>
<body onload="setFocusToFirstControl();">
<div class="topbtns">
    <a href="${listUrl}" class="btn"><fmt:message key="btn.backToList"/></a>
</div>

<c:set var="needsLineItemReview" value="${false}"/>
<req:isUserInRole role="lineItemReviewVoter">
    <s:if test="%{reviewProcess.userActionAnalyzer.needsLineItemReview(#attr.TissueLocatorUser, object)}">
        <c:set var="needsLineItemReview" value="${true}"/>
    </s:if>
</req:isUserInRole>
<c:if test="${needsLineItemReview}">
    <div class="topimportant">
        <p><fmt:message key="specimenRequest.review.outstanding"/><br></p>
        <ul>
            <li style="font-size:100%">
                <fmt:message key="specimenRequest.review.outstanding.lineItemReview"/>
            </li>
        </ul>
    </div>
    <br/>
</c:if>

<div class="line"></div>

<h2 class="noline huge"><fmt:message key="specimenRequest.review.summary"/></h2>
<div class="box">
    <fmt:message key="specimenRequest.review.summary.title" var="requestSummaryTitle"/>
    <table class="data" title="${requestSummaryTitle}">
        <tr>
            <th scope="col"><div><fmt:message key="specimenRequest.review.summary.name"/></div></th>
            <th scope="col"><div><fmt:message key="specimenRequest.review.summary.contact"/></div></th>
            <th scope="col"><div><fmt:message key="specimenRequest.review.summary.resume"/></div></th>
            <th scope="col"><div><fmt:message key="specimenRequest.review.summary.protocol"/></div></th>
            <th scope="col" class="action"><div><fmt:message key="specimenRequest.review.summary.details"/></div></th>
        </tr>

        <tr>
            <td class="title">
                ${object.investigator.firstName} ${object.investigator.lastName}<br />
                ${object.investigator.organization.name}
            </td>
            <td>
                <a href="mailto:${object.investigator.email}">
                    ${object.investigator.email}
                </a>
                <br />

                ${object.investigator.address.phone}
                <tissuelocator:phoneExtensionDisplay extension="${object.investigator.address.phoneExtension}"/>
                <br />
                <c:if test="${!empty object.investigator.address.fax}">
                    ${object.investigator.address.fax}
                    <br />
                </c:if>

                ${object.investigator.address.line1}<br />
                <c:if test="${!empty object.investigator.address.line2}">
                    ${object.investigator.address.line2}<br />
                </c:if>

                ${object.investigator.address.city}
                <fmt:message key="${object.investigator.address.state.resourceKey}"/>,
                ${object.investigator.address.zip}<br />
                <fmt:message key="country.${object.investigator.address.country}"/><br />
            </td>
            <td>
                <c:url value="/protected/downloadFile.action" var="downloadResumeUrl">
                    <c:param name="file.id" value="${object.investigator.resume.lob.id}" />
                    <c:param name="fileName" value="${object.investigator.resume.name}" />
                    <c:param name="contentType" value="${object.investigator.resume.contentType}" />
                </c:url>
                <a href="${downloadResumeUrl}" class="file">
                    ${object.investigator.resume.name}
                </a>
            </td>
            <td>
                <c:url value="/protected/downloadFile.action" var="downloadProtocolUrl">
                    <c:param name="file.id" value="${object.study.protocolDocument.lob.id}" />
                    <c:param name="fileName" value="${object.study.protocolDocument.name}" />
                    <c:param name="contentType" value="${object.study.protocolDocument.contentType}" />
                </c:url>
                <a href="${downloadProtocolUrl}" class="file">
                    ${object.study.protocolDocument.name}
                </a>
            </td>
            <td class="action">
                <c:url var="summaryUrl" value="/admin/request/review/lineItem/viewSummary.action" >
                    <c:param name="object.id" value="${object.id}"/>
                </c:url>
                <a href="${summaryUrl}" class="btn"><fmt:message key="specimenRequest.review.details"/></a>
            </td>
        </tr>
    </table>
</div>

<tissuelocator:messages/>

<div class="line"></div>

<c:if test="${isPending && !empty object.externalComment}">
    <div class="topcomment">
        <h2><fmt:message key="specimenRequest.review.decision.header"/></h2>
        <p><fmt:message key="specimenRequest.review.decision.externalComment.header"/></p>
        <p class="comment">${object.externalComment}</p>
        <c:if test="${!empty object.revisionComment}">
            <br/>
            <p><fmt:message key="specimenRequest.review.decision.revisionComment.header"/></p>
            <p class="comment">${object.revisionComment}</p>
        </c:if>
    </div>
</c:if>

<h2 class="noline huge"><fmt:message key="specimenRequest.review.lineItem.title" /></h2>

<div class="box">
    <s:form id="lineItemReviewForm" action="/admin/request/review/lineItem/saveLineItemReview.action"
        onsubmit="return confirm('%{getText('specimenRequest.review.lineItem.confirm')}')">
        <s:hidden key="object.id" />
        <s:hidden key="validateRequest" value="true" />
        <fmt:message key="specimenRequest.review.lineItem.title" var="lineItemReviewTableTitle"/>
        <table class="data" title="${lineItemReviewTableTitle}" id="lineItemReviewTable">
            <s:if test="%{combinedSpecificLineItems.size() > 0}">
                <s:set var="combinedSpecificLineItems" value="%{combinedSpecificLineItems}"/>
                <tissuelocator:aggregateLineItemReviewTable
                    requestLineItems="${combinedSpecificLineItems}"
                    cartPrefix="combinedSpecific"
                    emptyKey="shipment.cart.specific.noneSelected"
                    titleKey="aggregateCartTable.specificCart.title"/>
            </s:if>

            <s:if test="%{combinedGeneralLineItems.size() > 0}">
                <s:set var="combinedGeneralLineItems" value="%{combinedGeneralLineItems}"/>
                <tissuelocator:aggregateLineItemReviewTable
                    requestLineItems="${combinedGeneralLineItems}"
                    cartPrefix="combinedGeneral"
                    emptyKey="shipment.cart.general.noneSelected"
                    titleKey="aggregateCartTable.generalCart.title"/>
            </s:if>
        </table>

        <div class="btn_bar">
            <s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn"
                theme="simple" title="%{getText('btn.save.button')}"/>
        </div>
    </s:form>
</div>

<h2 class="noline huge"><fmt:message key="specimenRequest.review.comments.title" /></h2>

<c:if test="${isPending}">
    <req:isUserInRole role="reviewCommenter">
    <div class="topbtns">
        <c:url var="loadCommentFormUrl" value="/admin/request/review/lineItem/popup/loadCommentForm.action" >
            <c:param name="object.id" value="${object.id}"/>
        </c:url>
        <a id="btn_addComment" class="btn" href="javascript: showPopWin('${loadCommentFormUrl}', 400, 225, returnRefresh);"><fmt:message key="btn.addComment"/></a>
    </div>
    </req:isUserInRole>
</c:if>

<div class="box">
    <c:choose>
    <c:when test="${empty object.comments}">
        <p><fmt:message key="specimenRequest.review.noComments"/></p>
    </c:when>
    <c:otherwise>
    <table class="data" title="<fmt:message key="specimenRequest.review.comments.title" />">
        <tr>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.user"/></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.internalComment"/></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.date"/></th>
        </tr>
    <c:forEach items="${object.comments}" var="currentComment" varStatus="loopStatus">
        <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if> >
            <td>${currentComment.user.firstName} ${currentComment.user.lastName}</td>
            <td>${currentComment.comment}</td>
            <td><fmt:formatDate pattern="${datetimeFormat}" value="${currentComment.date}"/></td>
        </tr>
    </c:forEach>
    </table>
    </c:otherwise>
    </c:choose>
</div>
</body>
</html>
