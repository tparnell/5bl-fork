<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url value="/admin/request/review/fullConsortium/list.action" var="listUrl"/>
<c:url var="refreshUrl" value="/admin/request/review/fullConsortium/input.action">
    <c:param name="object.id" value="${object.id}"/>
    <c:param name="commentSaved" value="true"/>
</c:url>
<fmt:message key="default.date.format" var="dateFormat"/>
<fmt:message key="default.datetime.format" var="datetimeFormat"/>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING}" name="pending"/>
<s:set value="%{#pending.equals(object.status)}" name="isPending" scope="page"/>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_FINAL_DECISION}" name="pendingDecision"/>
<s:set value="%{#pendingDecision.equals(object.status)}" name="isPendingDecision" scope="page"/>
<html>
<head>
    <title><fmt:message key="specimenRequest.review.title" />: ${object.id} (<fmt:message key="${object.status.resourceKey}"/>)</title>
    <script type="text/javascript">
        function returnRefresh(returnVal) {
            window.location = '${refreshUrl}';
        }
    </script>
</head>
<body onload="setFocusToFirstControl();">
<div class="topbtns">
    <c:url var="summaryUrl" value="/admin/request/review/fullConsortium/viewSummary.action" >
        <c:param name="object.id" value="${object.id}"/>
    </c:url>
    <a href="${listUrl}" class="btn"><fmt:message key="btn.backToList"/></a>
    <a href="${summaryUrl}" class="btn"><fmt:message key="specimenRequest.review.details"/></a>
</div>
<p><fmt:message key="specimenRequest.review.deadline">
<fmt:param><strong><fmt:formatDate pattern="${dateFormat}" value="${votingEndDate}"/></strong></fmt:param>
</fmt:message></p>

<p><fmt:message key="specimenRequest.review.rules" /></p>

<c:set var="needsReviewerAssignment" value="${false}"/>
<req:isUserInRole role="scientificReviewerAssigner">
    <s:if test="%{reviewProcess.userActionAnalyzer.needsReviewerAssignment(#attr.TissueLocatorUser, object)}">
        <c:set var="needsReviewerAssignment" value="${true}"/>
    </s:if>
</req:isUserInRole>
<c:set var="needsConsortiumReview" value="${false}"/>
<req:isUserInRole role="consortiumReviewVoter">
    <s:if test="%{reviewProcess.userActionAnalyzer.needsConsortiumReview(#attr.TissueLocatorUser, object)}">
        <c:set var="needsConsortiumReview" value="${true}"/>
    </s:if>
</req:isUserInRole>
<c:set var="needsInstitutionalReview" value="${false}"/>
<req:isUserInRole role="instiutionalReviewVoter">
    <s:if test="%{reviewProcess.userActionAnalyzer.needsInstitutionalReview(#attr.TissueLocatorUser, object)}">
        <c:set var="needsInstitutionalReview" value="${true}"/>
    </s:if>
</req:isUserInRole>
<c:set var="needsLeadReview" value="${false}"/>
<c:set var="needsFinalComment" value="${false}"/>
<req:isUserInRole role="leadReviewer">
    <s:if test="%{reviewProcess.userActionAnalyzer.needsLeadReview(#attr.TissueLocatorUser, object)}">
        <c:set var="needsLeadReview" value="${true}"/>
    </s:if>
    <s:if test="%{reviewProcess.userActionAnalyzer.needsFinalComment(#attr.TissueLocatorUser, object)}">
        <c:set var="needsFinalComment" value="${true}"/>
    </s:if>
</req:isUserInRole>
<c:if test="${needsReviewerAssignment || needsLeadReview || needsConsortiumReview || needsInstitutionalReview || needsFinalComment}">
    <div class="topimportant">
        <p><fmt:message key="specimenRequest.review.outstanding"/><br></p>
        <ul>
            <c:if test="${needsReviewerAssignment}">
                <li style="font-size:100%">
                    <fmt:message key="specimenRequest.review.outstanding.assignReviewer"/>
                </li>
            </c:if>
            <c:if test="${needsLeadReview}">
                <li style="font-size:100%">
                    <fmt:message key="specimenRequest.review.outstanding.leadReview"/>
                </li>
            </c:if>
            <c:if test="${needsConsortiumReview}">
                <li style="font-size:100%">
                    <fmt:message key="specimenRequest.review.outstanding.consortiumReview"/>
                </li>
            </c:if>
            <c:if test="${needsInstitutionalReview}">
                <li style="font-size:100%">
                    <fmt:message key="specimenRequest.review.outstanding.institutionalReview"/>
                </li>
            </c:if>
            <c:if test="${needsFinalComment}">
                <li style="font-size:100%">
                    <fmt:message key="specimenRequest.review.outstanding.finalComment"/>
                </li>
            </c:if>
        </ul>
    </div>
    <br/>
</c:if>

<tissuelocator:messages/>
<s:fielderror cssClass="fielderror">
    <s:param>consortiumReview.commentValid</s:param>
    <s:param>institutionalReview.commentValid</s:param>
</s:fielderror>

<div class="line"></div>

<c:if test="${isPending && !empty object.externalComment}">
    <div class="topcomment">
        <h2><fmt:message key="specimenRequest.review.decision.header"/></h2>
        <p><fmt:message key="specimenRequest.review.decision.externalComment.header"/></p>
        <p class="comment">${object.externalComment}</p>
        <c:if test="${!empty object.revisionComment}">
            <br/>
            <p><fmt:message key="specimenRequest.review.decision.revisionComment.header"/></p>
            <p class="comment">${object.revisionComment}</p>
        </c:if>
    </div>
</c:if>

<c:if test="${isPendingDecision}">
    <s:if test="%{#attr.TissueLocatorUser.id == consortiumReview.user.id && object.reviewers.contains(consortiumReview.institution)}">
        <h2 class="noline huge"><fmt:message key="specimenRequest.review.finalComment.title"/></h2>

        <s:form id="externalCommentForm" action="/admin/request/review/fullConsortium/finalizeVote.action">
            <s:hidden key="object.id" />
            <div class="box">
                <div class="pad10">
                    <fmt:message key="specimenRequest.review.finalComment.votingResults"/>:
                    <strong><fmt:message key="${object.finalVote.resourceKey}"/></strong>
                </div>
            </div>

            <div class="box">
                <div class="pad10">
                    <p>
                        <strong><fmt:message key="specimenRequest.review.leadReviewer"/></strong>:
                        <fmt:message key="specimenRequest.review.finalComment.instructions"/>
                    </p>
                    <s:textarea name="object.externalComment" id="externalComment"
                                cols="20" rows="6" cssStyle="width:900px;"
                                labelposition="top" label="%{getText('specimenRequest.review.externalComment.label')}"
                                labelSeparator="" required="true" requiredposition="right"/>

                    <div class="btn_bar">
                        <s:submit value="%{getText('specimenRequest.review.submit')}" name="btn_save"
                            cssClass="btn" theme="simple" title="%{getText('specimenRequest.review.submit.button')}"/>
                    </div>
                </div>
            </div>
        </s:form>
    </s:if>
</c:if>

<tissuelocator:votingResultsDisplay specimenRequest="${object}" />


    <h2 class="noline huge"><fmt:message key="specimenRequest.review.consortium.title" />
    &nbsp;
    <span class="small">
    <c:url var="consortimReviewGuidelines" value="/documents/ABL_ScientificPeerReviewForm.pdf"/>
    <a href="${consortimReviewGuidelines}" id="consortiumReviewGuidelines" class="doc_check">
        <fmt:message key="specimenRequest.review.guidelines"/>
    </a></span></h2>

<div class="box">
    <req:isUserInRole role="consortiumReviewVoter">
        <c:if test="${TissueLocatorUser.id == consortiumReview.user.id}">
            <c:set var="isConsortiumReviewer" value="true" />
        </c:if>
    </req:isUserInRole>
    <req:isUserInRole role="scientificReviewerAssigner">
        <c:set var="isScientificReviewerAssigner" value="true" />
    </req:isUserInRole>

    <c:url value="/images/ico_star.gif" var="leadReviewIconUrl"/>
    <table class="data" title="<fmt:message key="specimenRequest.review.consortium.title" />" id="consortiumReviewTable">
        <tr>
            <th style="padding: 8px;"><fmt:message key="specimenRequest.review.columnHeader.lead" /></th>
            <th style="padding: 8px;" colspan="2" scope="col"><fmt:message key="specimenRequest.review.columnHeader.institution" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.reviewer" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.date" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.vote" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.comment" /></th>
            <th style="padding: 8px;" scope="col" class="action"><fmt:message key="specimenRequest.review.columnHeader.action" /></th>
        </tr>
    <c:forEach items="${object.consortiumReviews}" var="currentReview" varStatus="loopStatus">
        <s:set var="isLeadReviewer" value="%{object.reviewers.contains(#attr.currentReview.institution)}"/>
        <s:set var="assignable" scope="page" value="%{#attr.isPending || (#attr.isPendingDecision && #isLeadReviewer)}"/>
        <c:choose>
            <c:when test="${isScientificReviewerAssigner == true && currentReview.id == consortiumReview.id && assignable}">
                <s:form id="consortiumReviewForm" action="/admin/request/review/fullConsortium/assignScientificReviewer.action">
                    <s:hidden key="object.id" />
                    <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if> >
                        <td class="hilite">
                            <s:if test="%{#isLeadReviewer}">
                                <img src="${leadReviewIconUrl}" alt="ico_star" />
                            </s:if>
                        </td>
                        <td class="hilite"><img src="<c:url value="/images/arrow_right.gif" />" alt="arrow_right" /></td>
                        <td class="hilite">${currentReview.institution.name}</td>
                        <td class="hilite">
                            <s:select name="consortiumReview.user" id="consortiumReviewUser"
                                    value="%{consortiumReview.user.id}" theme="simple"
                                    list="reviewers" listKey="id" listValue="%{firstName + ' ' + lastName}"
                                    headerKey="" headerValue="%{getText('select.emptyOption')}"
                                    title="%{getText('specimenRequest.review.reviewers.select')}" cssStyle="max-width:160px;"/>
                            <s:fielderror cssClass="fielderror">
                                <s:param>consortiumReview.user</s:param>
                            </s:fielderror>
                        </td>
                        <td class="hilite"><fmt:formatDate pattern="${dateFormat}" value="${currentReview.date}"/></td>
                        <td class="hilite">
                            <c:if test="${currentReview.vote != null}">
                                <fmt:message key="${currentReview.vote.resourceKey}"  />
                            </c:if>
                        </td>
                        <td class="hilite">${currentReview.comment}</td>
                        <td class="hilite action"><s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn" theme="simple" title="%{getText('btn.save.buton')}"/></td>
                    </tr>
                </s:form>
            </c:when>
            <c:when test="${isConsortiumReviewer == true && currentReview.id == consortiumReview.id && isPending}">
                <s:form id="consortiumReviewForm" action="/admin/request/review/fullConsortium/saveConsortiumReview.action">
                    <s:hidden key="object.id" />
                    <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if> >
                        <td class="hilite">
                            <s:if test="%{#isLeadReviewer}">
                                <img src="${leadReviewIconUrl}" alt="ico_star" />
                            </s:if>
                        </td>
                        <td class="hilite"><img src="<c:url value="/images/arrow_right.gif" />" alt="arrow_right" /></td>
                        <td class="hilite">${currentReview.institution.name}</td>
                        <td class="hilite">
                            <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${true}" value="${currentReview.user.firstName} ${currentReview.user.lastName}"
                                maxWordLength="20"/>
                        </td>
                        <td class="hilite"><fmt:formatDate pattern="${dateFormat}" value="${currentReview.date}"/></td>
                        <td class="hilite">
                            <s:select name="consortiumReview.vote" id="consortiumReviewVote"
                                            value="%{consortiumReview.vote}"
                                            list="%{@com.fiveamsolutions.tissuelocator.data.Vote@values()}"
                                            listValue="%{getText(resourceKey)}"
                                            headerKey="" headerValue="%{getText('select.emptyOption')}"
                                            theme="simple" title="%{getText('consortiumReview.vote.select')}" />
                            <s:fielderror cssClass="fielderror">
                                <s:param>consortiumReview.vote</s:param>
                            </s:fielderror>
                        </td>
                        <td class="hilite">
                            <s:textarea name="consortiumReview.comment" id="consortiumReviewComment" cols="30" rows="3"
                                        required="false"  theme="simple" title="%{getText('consortiumReview.comment.textarea')}"/>
                            <s:fielderror cssClass="fielderror">
                                <s:param>consortiumReview.comment</s:param>
                            </s:fielderror>
                        </td>
                        <td class="hilite action"><s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn" theme="simple" title="%{getText('btn.save.button')}"/></td>
                    </tr>
                </s:form>
            </c:when>
            <c:otherwise>
                <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if> >
                    <td>
                        <s:if test="%{#isLeadReviewer}">
                            <img src="${leadReviewIconUrl}" alt="ico_star" />
                        </s:if>
                    </td>
                    <td colspan="2">${currentReview.institution.name}</td>
                    <td>
                        <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${true}" value="${currentReview.user.firstName} ${currentReview.user.lastName}"
                            maxWordLength="20"/>
                    </td>
                    <td><fmt:formatDate pattern="${dateFormat}" value="${currentReview.date}"/></td>
                    <td>
                        <c:if test="${currentReview.vote != null}">
                            <fmt:message key="${currentReview.vote.resourceKey}"  />
                        </c:if>
                    </td>
                    <td>${currentReview.comment}</td>
                    <td class="action"></td>
                </tr>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    </table>
</div>

    <h2 class="noline huge"><fmt:message key="specimenRequest.review.institutional.title" />
    &nbsp;
    <span class="small">
      <c:url var="institutionalReviewGuidelines" value="/documents/ABL_InstitutionalAdministratorReviewForm.pdf"/>
      <a href="${institutionalReviewGuidelines}" id="institutionalReviewGuidelines" class="doc_check">
          <fmt:message key="specimenRequest.review.guidelines"/>
    </a></span></h2>

<div class="box">
    <req:isUserInRole role="instiutionalReviewVoter">
        <c:set var="isInstiutionalReviewer" value="true" />
    </req:isUserInRole>

    <table class="data" title="<fmt:message key="specimenRequest.review.institutional.title" />" id="instReviewTable">
        <tr>
            <th style="padding: 8px;" colspan="2" scope="col"><fmt:message key="specimenRequest.review.columnHeader.institution" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.reviewer" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.biospecimens" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.date" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.vote" /></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.comment" /></th>
            <th style="padding: 8px;" scope="col" class="action"><fmt:message key="specimenRequest.review.columnHeader.action" /></th>
        </tr>
    <c:forEach items="${object.institutionalReviews}" var="currentReview" varStatus="loopStatus">
        <c:choose>
            <c:when test="${isInstiutionalReviewer == true && currentReview.id == institutionalReview.id && isPending}">
            <s:form id="instReviewForm" action="/admin/request/review/fullConsortium/saveInstitutionalReview.action">
                <s:hidden key="object.id" />
                <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if> >
                    <td class="hilite"><img src="<c:url value="/images/arrow_right.gif" />" alt="arrow_right" /></td>
                    <td class="hilite">${currentReview.institution.name}</td>
                    <td class="hilite">${currentReview.user.firstName} ${currentReview.user.lastName}</td>
                    <td class="hilite">
                        <c:url var="viewLineItemsUrl" value="/admin/request/review/fullConsortium/popup/viewLineItems.action">
                            <c:param name="object.id" value="${object.id}"/>
                            <c:param name="institution.id" value="${currentReview.institution.id}"/>
                        </c:url>
                        <a href="javascript: showPopWin('${viewLineItemsUrl}', 700, 400, null);" id="specimenPopupLink">
                            <s:property default="0" value="%{getLineItemsByInstitution(#attr.currentReview.institution).size}"/>
                        </a>
                    </td>
                    <td class="hilite"><fmt:formatDate pattern="${dateFormat}" value="${currentReview.date}"/></td>
                    <td class="hilite">
                        <s:select name="institutionalReview.vote" id="institutionalReviewVote"
                                        value="%{institutionalReview.vote}"
                                        list="%{instiutionalReviewAllowableVotes}"
                                        listValue="%{getText(resourceKey)}"
                                        headerKey="" headerValue="%{getText('select.emptyOption')}"
                                        theme="simple" title="%{getText('institutionalReview.vote.select')}"/>
                        <s:fielderror cssClass="fielderror">
                            <s:param>institutionalReview.vote</s:param>
                        </s:fielderror>
                    </td>
                    <td class="hilite">
                        <s:textarea name="institutionalReview.comment" id="institutionalReviewComment" cols="30" rows="3"
                                    required="false"  theme="simple" title="%{getText('institutionalReview.comment.textarea')}"/>
                        <s:fielderror cssClass="fielderror">
                            <s:param>institutionalReview.comment</s:param>
                        </s:fielderror>
                    </td>
                    <td class="hilite action"><s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn" theme="simple" title="%{getText('btn.save.button')}"/></td>
                </tr>
            </s:form>
            </c:when>
            <c:otherwise>
                <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if> >
                    <td colspan="2">${currentReview.institution.name}</td>
                    <td>${currentReview.user.firstName} ${currentReview.user.lastName}</td>
                    <td>
                        <c:url var="viewLineItemsUrl" value="/admin/request/review/fullConsortium/popup/viewLineItems.action">
                            <c:param name="object.id" value="${object.id}"/>
                            <c:param name="institution.id" value="${currentReview.institution.id}"/>
                        </c:url>
                        <a href="javascript: showPopWin('${viewLineItemsUrl}', 700, 400, null);" id="specimenPopupLink" >
                            <s:property default="0" value="%{getLineItemsByInstitution(#attr.currentReview.institution).size}"/>
                        </a>
                    </td>
                    <td><fmt:formatDate pattern="${dateFormat}" value="${currentReview.date}"/></td>
                    <td>
                        <c:if test="${currentReview.vote != null}">
                            <fmt:message key="${currentReview.vote.resourceKey}"  />
                        </c:if>
                    </td>
                    <td>${currentReview.comment}</td>
                    <td class="action"></td>
                </tr>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    </table>
</div>

<h2 class="noline huge"><fmt:message key="specimenRequest.review.comments.title" /></h2>

<c:if test="${isPending}">
    <req:isUserInRole role="reviewCommenter">
    <div class="topbtns">
        <c:url var="loadCommentFormUrl" value="/admin/request/review/fullConsortium/popup/loadCommentForm.action" >
            <c:param name="object.id" value="${object.id}"/>
        </c:url>
        <a id="btn_addComment" class="btn" href="javascript: showPopWin('${loadCommentFormUrl}', 400, 225, returnRefresh);"><fmt:message key="btn.addComment"/></a>
    </div>
    </req:isUserInRole>
</c:if>

<div class="box">
    <c:choose>
    <c:when test="${empty object.comments}">
        <p>No comments have been entered.</p>
    </c:when>
    <c:otherwise>
    <table class="data" title="<fmt:message key="specimenRequest.review.comments.title" />">
        <tr>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.user"/></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.internalComment"/></th>
            <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.date"/></th>
        </tr>
    <c:forEach items="${object.comments}" var="currentComment" varStatus="loopStatus">
        <tr <c:if test="${loopStatus.count % 2 == 1}">class="odd"</c:if> >
            <td>${currentComment.user.firstName} ${currentComment.user.lastName}</td>
            <td>${currentComment.comment}</td>
            <td><fmt:formatDate pattern="${datetimeFormat}" value="${currentComment.date}"/></td>
        </tr>
    </c:forEach>
    </table>
    </c:otherwise>
    </c:choose>
</div>
</body>
</html>
