<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.lineItems" /></title>
</head>
<body>
    <c:if test="${not empty object.lineItems}">
        <tissuelocator:cartTable showFinalPriceColumn="false" showOrderFulfillmentForms="false" requestLineItems="${object.lineItems}"
            showSpecimenLinks="false" />
   </c:if>
   <s:set var="specificLineItems" value="%{specificLineItemList}"/>
   <s:set var="generalLineItems" value="%{generalLineItemList}"/>
   <c:if test="${not empty object.aggregateLineItems}">
        <tissuelocator:aggregateCartTable specificLineItems="${specificLineItems}" generalLineItems="${generalLineItems}" showFinalPriceColumn="false" showOrderFulfillmentForms="false"/>
   </c:if>
</body>
