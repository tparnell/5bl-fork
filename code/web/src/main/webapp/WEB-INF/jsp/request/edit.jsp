<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>

<s:set var="yesEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@YES}" />
<s:set var="noEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@NO}" />
<s:set var="naEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@NOT_APPLICABLE}" />
<s:set var="yesFundedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.FundingStatus@FUNDED}"/>
<s:set var="notFundedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.FundingStatus@NOT_FUNDED}"/>
<s:set var="pendingFundedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.FundingStatus@PENDING}"/>
<s:set var="minFundingValue" value="%{minimumFundingRequired}"/>

<s:set var="yesFundedString" value="%{#yesFundedEnum.toString()}"/>
<s:set var="notFundedString" value="%{#notFundedEnum.toString()}"/>
<s:set var="pendingFundedString" value="%{#pendingFundedEnum.toString()}"/>
<s:set var="minFundingString" value="%{#minFundingValue.toString()}"/>


<html>
    <head>
        <title><fmt:message key="specimenRequest.edit.title" /></title>
        <script type="text/javascript">
        $(document).ready(function () {
          // the following divs are hidden so as not to display on page load
          // the following code unhides them, and the remaining code shows/hides appropriately based on current selections
          var initialHiddenDivs = ['#fundingStatusLink_FUNDED', '#fundingStatusLink_PENDING', '#fundingStatusLink_NOT_FUNDED',
                                   '#irbApprovalStatusLink_APPROVED', '#irbApprovalStatusLink_EXEMPT', '#irbApprovalStatusLink_NOT_APPROVED'];

          for (i = 0; i < initialHiddenDivs.length; i++) {
            $(initialHiddenDivs[i]).show();
          }
            var misconductClearFields = {'investigated_NO' : ['investigationComment']};
            createYesNoRadioTab('pi_investigated', 'pi_research_misconduct','investigated_','pi_research_misconduct_',misconductClearFields);

            var exemptFields = ['exempt_irbName','exempt_irbRegistrationNumber','exempt_irbExemptionLetter'];
            var approvedFields = ['approved_irbName','approved_irbRegistrationNumber','approved_irbApprovalLetter',
                                  'approved_irbApprovalExpirationDate','approved_irbApprovalNumber'];

            // Map of radio button id to div that should appear when selected
            var tabLinkMap = {
                    'irbApprovalStatusValue_APPROVED' : 'irbApprovalStatusLink_APPROVED',
                    'irbApprovalStatusValue_EXEMPT' : 'irbApprovalStatusLink_EXEMPT',
                    'irbApprovalStatusValue_NOT_APPROVED' : 'irbApprovalStatusLink_NOT_APPROVED'
            };
            // Map of form fields that should be cleard when the radio button is selected
            var inconsistentFieldMap = {
                    'irbApprovalStatusValue_APPROVED' : exemptFields,
                    'irbApprovalStatusValue_EXEMPT' : approvedFields,
                    'irbApprovalStatusValue_NOT_APPROVED' : exemptFields.concat(approvedFields)
            };
            createRadioTab('irbApprovalStatusContainer', 'irbApprovalStatus', tabLinkMap, inconsistentFieldMap, null);

            // Map of radio button id to div that should appear when selected for funding status
            var fundingStatusTabLinkMap = {'fundingStatus_PENDING' : 'fundingStatusLink_PENDING',
                    'fundingStatus_NOT_FUNDED' : 'fundingStatusLink_NOT_FUNDED',
                    'fundingStatus_FUNDED' : 'fundingStatusLink_FUNDED'};
            var fundedFields = [];
            <s:iterator value="%{getUiDynamicFieldCategory('specimenRequest.study.fundingStatus').customPropertyFields}">
                <s:set var="dynamicFieldId" value="%{fieldConfig.fieldName}"/>
                fundedFields.push('object.customProperties[\'${dynamicFieldId}\']');
            </s:iterator>
            var inconsistentFundingStatusFieldMap = {'fundingStatus_FUNDED' : [],
                'fundingStatus_NOT_FUNDED' : fundedFields, 'fundingStatus_PENDING' : fundedFields};
            var multiSelectFundedFields = ['fundingSource'];
            var inconsistentMultiSelectFundingStatusFieldMap = {'fundingStatus_FUNDED' : [],
                    'fundingStatus_NOT_FUNDED' : multiSelectFundedFields,
                    'fundingStatus_PENDING' : multiSelectFundedFields};
            createRadioTab('fundingStatusContainer', 'fundingStatusUl', fundingStatusTabLinkMap,
                    inconsistentFundingStatusFieldMap, inconsistentMultiSelectFundingStatusFieldMap);

            // IRB Name & registration number are duplicated, duplicates must be removed on submit
            $('#requestForm').submit(function() {
                if (!$('#irbApprovalStatusValue_EXEMPT').get(0).checked) {
                    removeFields(['exempt_irbName', 'exempt_irbRegistrationNumber']);
                }
                if (!$('#irbApprovalStatusValue_APPROVED').get(0).checked) {
                    removeFields(['approved_irbName', 'approved_irbRegistrationNumber']);
                }
            });

        });
        var disableIfSelected = [];
        var listenForDisable = [];
    </script>

    <c:choose>
        <c:when test="${minFundingString == yesFundedString}">
            <script type="text/javascript">
                $(document).ready(function () {
                    disableIfSelected = disableIfSelected.concat([$('#fundingStatus_NOT_FUNDED').get(0), $('#fundingStatus_PENDING').get(0)]);
                    listenForDisable = listenForDisable.concat(['fundingStatus_FUNDED']);
                });
            </script>
        </c:when>
        <c:when test="${minFundingString == pendingFundedString}">
            <script type="text/javascript">
                $(document).ready(function () {
                    disableIfSelected = disableIfSelected.concat([$('#fundingStatus_NOT_FUNDED').get(0)]);
                    listenForDisable = listenForDisable.concat(['fundingStatus_FUNDED', 'fundingStatus_PENDING']);
                });
            </script>
        </c:when>
    </c:choose>
    <c:if test="${irbApprovalRequired}">
        <script type="text/javascript">
            $(document).ready(function () {
                disableIfSelected = disableIfSelected.concat([$('#irbApprovalStatusValue_NOT_APPROVED').get(0)]);
                listenForDisable = listenForDisable.concat(['irbApprovalStatusValue_APPROVED', 'irbApprovalStatusValue_EXEMPT', 'irbApprovalStatusValue_NON_HUMAN_SUBJECT']);
            });
        </script>
    </c:if>


    <script type="text/javascript">
        $(document).ready(function () {
            disableOnSelection($('#btn_save').get(0), disableIfSelected, listenForDisable, true);
        });
    </script>
    <style>
        .ui-tabs .ui-tabs-panel
        { padding: 0px; }
        li.ui-state-default, li.ui-widget-content .ui-state-default, li.ui-widget-header .ui-state-default,
        li.ui-state-hover, li.ui-widget-content .ui-state-hover, li.ui-widget-header .ui-state-hover,
        li.ui-state-focus, li.ui-widget-content .ui-state-focus, li.ui-widget-header .ui-state-focus,
        li.ui-state-active, li.ui-widget-content .ui-state-active, li.ui-widget-header .ui-state-active
        { border: none; background: none; }
    </style>
    </head>
    <body>

        <tissuelocator:requestProcessSteps selected="proposal"/>
        <h1><fmt:message key="specimenRequest.edit.title" /></h1>

        <p><fmt:message key="specimenRequest.edit.text"/></p>
        <fmt:message key="tissuelocator.baseUrl" var="baseurl" />
        <p><fmt:message key="specimenRequest.policies"><fmt:param value="${baseurl}"/></fmt:message></p>

        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/info/popup/fieldHelp" urlEnding=".action"/>
        <c:set var="helpUrlBase" value="${outputUrl}"/>
        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/viewCart" urlEnding=".action" asUrl="true"/>
        <c:set var="cartUrl" value="${outputUrl}"/>
        <c:choose>
            <c:when test="${empty object.id}">
                <div class="line"></div>
            </c:when>
            <c:otherwise>
                <tissuelocator:externalCommentDisplay specimenRequest="${object}"/>
                <div class="box">
                    <a href="${cartUrl}" class="btn"><fmt:message key="specimenRequest.edit.reviseCart"/></a>
                    <span class="note"><fmt:message key="specimenRequest.edit.reviseCart.note"/></span>
                </div>
            </c:otherwise>
        </c:choose>
        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/saveDraftOrConfirm" urlEnding=".action"/>
        <s:form id="requestForm" action="%{#attr.request.outputUrl}" enctype="multipart/form-data" method="POST">
            <tissuelocator:messages breakCount="2" escape="false"/>
            <s:hidden key="object.id" />
            <s:hidden name="object.requestor" value="%{#attr.TissueLocatorUser.id}"/>
            <s:hidden name="previousStatus" value="%{object.status}"/>

            <div id="requestform">
                <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@DRAFT}" name="draftStatus"/>
                <s:set value="%{#draftStatus.equals(object.status)}" name="isDraft" scope="page"/>
                <c:if test="${!isDraft and !empty object.id}">
                    <h2 class="noline_top"><fmt:message key="specimenRequest.revisionComment.title"/></h2>
                    <div class="revision_comments">
                        <s:textarea name="object.revisionComment" id="revisionComment"
                            cols="20" rows="3" cssStyle="width:630px;"
                            labelposition="top" label="%{getText('specimenRequest.revisionComment.instructions')}"/>
                    </div>
                </c:if>

                <c:set var="sectionCount" value="${1}"/>
                <h2 class="noline_top">
                    <a href="javascript:;" onclick="toggleVisibility('study_details');" class="toggle">
                        <fmt:message key="specimenRequest.study.title">
                            <fmt:param value="${sectionCount}"/>
                        </fmt:message>
                    </a>
                </h2>

                <div id="study_details">
                    <div class="formcol_wide" style="width:300px">
                        <s:textfield name="object.study.protocolTitle" id="protocolTitle"
                                    size="30" maxlength="254" cssStyle="width:280px" required="true" requiredposition="right"
                                    labelposition="top" label="%{getText('specimenRequest.study.protocolTitle')}"
                                    labelSeparator=""/>

                        <label for="preliminary">
                            <fmt:message key="specimenRequest.study.preliminary"/>
                            <span class="reqd"><fmt:message key="required.indicator"/></span>
                        </label>
                        <ul id="preliminaryUl" class="radio_tabs" style="margin-left: -20px">
                            <li class="tab" style="display:none;"><a href="#empty"></a></li>
                            <s:iterator value="%{#{#yesEnum : true, #noEnum : false, #naEnum : null}}">
                                <s:set value="%{key.name()}" var="optionName"/>
                                <s:set value="%{value}" var="optionValue"/>

                                <c:set var="checked" value=""/>
                                <s:if test="%{#optionValue == object.study.preliminary}">
                                    <c:set var="checked" value="checked"/>
                                </s:if>

                                <li class="tab" style="margin-right: 12px">
                                    <input type="radio" name="object.study.preliminary"
                                        id="preliminary_${optionName}" value="${optionValue}" ${checked}/>
                                    <label class="inline" for="preliminary_${optionName}">
                                        <s:property value="%{getText(key.resourceKey)}"/>
                                    </label>
                                </li>
                            </s:iterator>
                        </ul>
                        <s:fielderror fieldName="object.study.preliminary" cssClass="fielderror"/>
                    </div>

                    <div class="formcol_wide" style="width:500px">
                        <tissuelocator:fileField file="${object.study.protocolDocument}"
                            id="protocolDocument" name="protocolDocument"
                            objectProperty="object.study.protocolDocument"
                            labelKey="specimenRequest.study.protocolDocument" required="${protocolDocumentRequired}"
                            unsavedUrlBase="/protected/request/downloadProtocolDocument" fieldSize="28"/>
                    </div>

                    <div class="clear"></div>
                    <div class="line"></div>

                    <div id="fundingStatusContainer">
                        <label for="fundingStatus">
                            <fmt:message key="specimenRequest.study.fundingStatus"/>
                            <span class="reqd"><fmt:message key="required.indicator"/></span>
                        </label>

                        <s:set var="showPendingStatus" value="%{displayFundingStatusPending}"/>
                        <ul id="fundingStatusUl" class="radio_tabs" style="margin: 10px 0px 0px -20px">
                            <li class="tab" style="display:none;"><a href="#empty"></a></li>
                            <s:iterator value="%{{#yesFundedEnum, #notFundedEnum, #pendingFundedEnum}}">
                                <s:if test="%{#showPendingStatus || !name().equals(#pendingFundedEnum.name())}">
                                    <s:set value="%{name()}" var="optionName"/>
                                    <s:set value="%{'fundingStatusFor_' + #optionName}" var="ulId"/>

                                    <c:set var="checked" value=""/>
                                    <s:if test="%{#optionName == object.study.fundingStatus.name()}">
                                        <c:set var="checked" value="checked"/>
                                    </s:if>

                                    <c:set var="optionLink" value="#fundingStatusLink_${optionName}"/>
                                    <li class="tab" style="margin-right: 12px">
                                        <a href="${optionLink}"></a>
                                        <input type="radio" name="object.study.fundingStatus"
                                            id="fundingStatus_${optionName}" value="${optionName}" ${checked}/>
                                        <label class="inline" for="fundingStatus_${optionName}">
                                            <s:property value="%{getText(resourceKey)}"/>
                                        </label>
                                    </li>
                                </s:if>
                            </s:iterator>
                        </ul>
                        <s:fielderror cssClass="fielderror" fieldName="object.study.fundingStatus"/>


                        <div id="empty"></div>
                        <div id="fundingStatusLink_FUNDED" style="display: none">
                            <div class="formcol" style="width:300px; display:none" id="fundingSourceWrapper">
                                <s:select name="object.study.fundingSources" id="fundingSource"
                                        value="%{fundingSourceIds}"
                                        list="%{fundingSources}" listKey="id" listValue="name"
                                        multiple="true" required="true" requiredposition="right"
                                        labelSeparator="" labelposition="top"
                                        label="%{getText('specimenRequest.study.fundingSource')}" />
                                <script type="text/javascript">
                                    $(function(){
                                      $("#fundingSource").multiselect({
                                            selectedList: 1,
                                            minWidth: 280,
                                            noneSelectedText: '<fmt:message key="select.emptyOption"/>',
                                            position: {
                                                my: 'left top',
                                                at: 'left bottom'
                                            }
                                        });
                                    });
                                    $(document).ready(function() {
                                        $('#fundingSourceWrapper').show();
                                    });
                                </script>
                            </div>

                            <s:set var="uiDynamicFieldCategory" value="getUiDynamicFieldCategory('specimenRequest.study.fundingStatus')"/>
                            <s:if test="%{#uiDynamicFieldCategory != null}">
                                <s:set var="dynamicFieldDefinitionList"
                                    value="%{getDynamicFieldDefinitionsByCategory(#uiDynamicFieldCategory)}"/>
                                <s:if test="%{!#dynamicFieldDefinitionList.isEmpty()}">
                                    <tissuelocator:editExtensions
                                        dynamicFieldDefinitions="${dynamicFieldDefinitionList}"
                                        layout="boxLayout"
                                        extendableEntity="${object}"
                                        propertyPath="object"
                                        wrapperClass="formcol" wrapperStyle="width:300px"
                                        helpUrlBase="${helpUrlBase}"/>
                                </s:if>
                            </s:if>
                        </div>
                        <c:if test="${showPendingStatus}">
                            <div id="fundingStatusLink_PENDING" style="display: none">
                                <s:if test="%{#minFundingValue == #yesFundedEnum}">
                                    <p class="important">
                                        <fmt:message key="specimenRequest.study.noMinFunding"/>
                                    </p>
                                </s:if>
                            </div>
                        </c:if>
                        <fmt:message key="specimenRequest.study.notFunded.warning" var="notFundedMessage"/>
                        <s:if test="%{#minFundingValue != #notFundedEnum}">
                            <fmt:message key="specimenRequest.study.noMinFunding" var="notFundedMessage"/>
                        </s:if>
                        <div id="fundingStatusLink_NOT_FUNDED" style="display: none">
                            <c:if test="${not empty notFundedMessage}">
                                <p class="important">
                                    ${notFundedMessage}
                                </p>
                            </c:if>
                        </div>
                    </div>

                    <div class="clear"></div>
                    <div class="line"></div>

                    <div id="irbApprovalStatusContainer">
                        <h3><fmt:message key="specimenRequest.study.irb.title"/></h3>
                        <label for="irbApprovalStatus">
                                <fmt:message key="specimenRequest.study.irb.approved.text"/>
                                <span class="reqd"><fmt:message key="required.indicator"/></span>
                        </label>
                        <ul id="irbApprovalStatus" class="radio_tabs" style="margin: 10px 0px 0px -20px">
                            <li class="tab" style="display:none;"><a href="#none"></a></li>
                            <s:iterator value="%{@com.fiveamsolutions.tissuelocator.data.IrbApprovalStatus@values()}">
                                <s:set value="%{name()}" var="optionName"/>
                                <s:set var="ulId" value="%{'irbApprovalStatus_' + #optionName}"/>
                                <c:set var="checked" value=""/>
                                <s:if test="%{#optionName == object.study.irbApprovalStatus.name()}">
                                  <c:set var="checked" value="checked"/>
                                </s:if>
                                <c:set var="optionLink" value="#irbApprovalStatusLink_${optionName}"/>
                                <li class="tab" style="margin-right: 12px">
                                    <a href="${optionLink}"></a>
                                    <input type="radio" name="object.study.irbApprovalStatus"
                                           id="irbApprovalStatusValue_${optionName}" value="${optionName}" ${checked}/>
                                    <s:set var="statusKey" value="%{resourceKey}"/>
                                    <fmt:message key="${statusKey}" var="statusDisplayName"/>
                                    <tissuelocator:helpLabel helpUrlBase="${helpUrlBase}"
                                                             labelFor="irbApprovalStatusValue_${optionName}"
                                                             fieldDisplayName="${statusDisplayName}"
                                                             fieldName="study.irbApprovalStatus.${optionName}"
                                                             required="false" labelClass="inline"/>
                                </li>
                            </s:iterator>
                        </ul>
                        <s:fielderror cssClass="fielderror" fieldName="object.study.irbApprovalStatus"/>

                        <div id="none"></div>
                        <div id="irbApprovalStatusLink_APPROVED" style="display: none">

                            <div class="formcol" style="width:300px">
                                <s:textfield name="object.study.irbName" id="approved_irbName"
                                    size="30" maxlength="254" cssStyle="width:280px" required="true" requiredposition="right"
                                    labelposition="top" label="%{getText('specimenRequest.study.irbName')}"
                                    labelSeparator=""/>
                            </div>

                            <div class="formcol" style="width:300px">
                                <s:textfield name="object.study.irbRegistrationNumber" id="approved_irbRegistrationNumber"
                                    size="30" maxlength="254" cssStyle="width:280px" labelSeparator=""
                                    labelposition="top" label="%{getText('specimenRequest.study.irbRegistrationNumber')}" />
                            </div>

                            <div class="formcol" style="width:310px">
                                <fmt:message key="datepicker.format" var="dateFormat"/>
                                <sj:datepicker id="approved_irbApprovalExpirationDate" name="object.study.irbApprovalExpirationDate"
                                    displayFormat="%{#attr.dateFormat}" cssStyle="width:260px"
                                    required="true" requiredposition="right" labelSeparator="" labelposition="top"
                                    label="%{getText('specimenRequest.study.irbApprovalExpirationDate')}"
                                    buttonImageOnly="true" changeMonth="true" changeYear="true"/>
                            </div>
                            <div class="clear"></div>
                            <s:fielderror cssClass="fielderror" fieldName="object.studyIrbApprovalExpirationDateValid"/>

                            <label for="approvedNote">
                                <fmt:message key="specimenRequest.study.irbApproval.include"/>
                                <span class="reqd"><fmt:message key="required.indicator"/></span>
                            </label>

                            <div class="formcol" style="width:300px">
                                <s:textfield name="object.study.irbApprovalNumber" id="approved_irbApprovalNumber"
                                    size="30" maxlength="254" cssStyle="width:280px"
                                    labelposition="top" label="%{getText('specimenRequest.study.irbApprovalNumber')}"
                                    labelSeparator=""/>
                            </div>

                            <div class="formcol" style="width:300px">
                                <tissuelocator:fileField file="${object.study.irbApprovalLetter}"
                                    id="approved_irbApprovalLetter" name="irbApprovalLetter"
                                    objectProperty="object.study.irbApprovalLetter"
                                    labelKey="specimenRequest.study.irbApprovalLetter" required="false"
                                    unsavedUrlBase="/protected/request/downloadIrbApprovalLetter" fieldSize="24"/>
                            </div>

                        </div>

                        <div id="irbApprovalStatusLink_EXEMPT" style="display: none">

                            <div class="formcol" style="width:300px">
                                <s:textfield name="object.study.irbName" id="exempt_irbName"
                                    size="30" maxlength="254" cssStyle="width:280px" required="true" requiredposition="right"
                                    labelposition="top" label="%{getText('specimenRequest.study.irbName')}"
                                    labelSeparator=""/>
                            </div>

                            <div class="formcol" style="width:300px">
                                <s:textfield name="object.study.irbRegistrationNumber" id="exempt_irbRegistrationNumber"
                                    size="30" maxlength="254" cssStyle="width:280px" labelSeparator=""
                                    labelposition="top" label="%{getText('specimenRequest.study.irbRegistrationNumber')}" />
                            </div>

                            <div class="clear"></div>

                            <div class="formcol" style="width:300px">
                                <tissuelocator:fileField file="${object.study.irbExemptionLetter}"
                                    id="exempt_irbExemptionLetter" name="irbExemptionLetter"
                                    objectProperty="object.study.irbExemptionLetter"
                                    labelKey="specimenRequest.study.irbExemptionLetter" required="true"
                                    unsavedUrlBase="/protected/request/downloadIrbExemptionLetter" fieldSize="24"/>
                            </div>
                        </div>
                        <div id="irbApprovalStatusLink_NON_HUMAN_SUBJECT">
                        </div>

                        <div id="irbApprovalStatusLink_NOT_APPROVED" style="display: none">
                            <p class="important">
                                <strong><fmt:message key="specimenRequest.study.irb.notApproved.note"/></strong>&nbsp;
                                <fmt:message key="specimenRequest.study.irb.notApproved.note.review"/>
                            </p>
                        </div>
                    </div>

                    <div class="clear"></div>
                    <br />
                    <div class="line"></div>

                    <s:textarea name="object.study.studyAbstract" id="abstract"
                            rows="6" cols="20" cssStyle="width:630px;"
                            labelposition="top" label="%{getText('specimenRequest.study.studyAbstract')}"
                            labelSeparator="" required="true" requiredposition="right"/>
                    <span class="note"><fmt:message key="specimenRequest.study.studyAbstract.note"/></span>

                    <div class="clear"></div>
                    <div class="line"></div>

                </div>

                <c:set var="sectionCount" value="${sectionCount + 1}"/>
                <h2 class="noline">
                    <a href="javascript:;" onclick="toggleVisibility('principal_investigator');" class="toggle">
                        <fmt:message key="specimenRequest.investigator.title">
                            <fmt:param value="${sectionCount}"/>
                        </fmt:message>
                    </a>
                </h2>

                <div id="principal_investigator">

                    <span class="note"><fmt:message key="specimenRequest.investigator.note"/></span>
                    <div class="clear"></div>
                    <br />

                    <tissuelocator:editPerson copyEnabled="true" propertyPrefix="object.investigator"
                        resourcePrefix="specimenRequest.investigator" idPrefix="investigator"
                        countryValue="${object.investigator.address.country}"
                        stateValue="${object.investigator.address.state}"
                        showInstitution="true" showFax="false"
                        showResume="true" resumeFile="${object.investigator.resume}"
                        unsavedResumeDownloadUrl="/protected/request/downloadResume"
                        resumeHelpFieldName="principalInvestigator.resume" helpUrlBase="${helpUrlBase}"/>

                    <div class="clear"></div>
                    <br />
                    <div class="line"></div>


                    <s:if test="%{displayPILegalField}">
                        <h3><fmt:message key="specimenRequest.investigator.investigated.title"/></h3>

                        <div id="pi_certified">
                            <tissuelocator:investigatorCertificationText showRequired="true"/>

                            <s:iterator value="%{{#yesEnum, #noEnum}}">
                                <s:set value="%{name()}" var="optionName"/>
                                <c:set var="checked" value=""/>
                                <s:if test="%{#optionName == object.investigator.certified.name()}">
                                    <c:set var="checked" value="checked"/>
                                </s:if>
                                <input type="radio" name="object.investigator.certified"
                                    id="certified_${optionName}" value="${optionName}" ${checked}/>
                                <label class="inline" for="certified_${optionName}">
                                    <s:property value="%{getText(resourceKey)}"/>
                                </label>
                                &nbsp;&nbsp;
                            </s:iterator>

                        </div>
                        <div class="clear"></div>
                        <s:fielderror cssClass="fielderror" fieldName="object.investigator.certified"/>
                        <br/>

                        <div id="pi_investigated">

                            <label for="pi_research_misconduct">
                                <fmt:message key="specimenRequest.investigator.investigated.text"/>
                                <span class="reqd"><fmt:message key="required.indicator"/></span>
                            </label>

                            <ul id="pi_research_misconduct" class="radio_tabs" style="margin: 10px 0px 0px -20px;">
                                <%-- default list element that initializes empty tab as active --%>
                                <li class="tab" style="display:none;"><a href="#empty_default"></a></li>
                                <s:iterator value="%{{#yesEnum, #noEnum}}">
                                    <s:set value="%{name()}" var="optionName"/>
                                    <c:set var="checked" value=""/>
                                    <s:if test="%{#optionName == object.investigator.investigated.name()}">
                                        <c:set var="checked" value="checked"/>
                                    </s:if>

                                    <c:set var="misconductLink" value="#pi_research_misconduct_${optionName}"/>
                                    <li class="tab" style="margin-right: 1.3em">
                                        <a href="${misconductLink}"></a>
                                        <input type="radio" name="object.investigator.investigated"
                                            id="investigated_${optionName}" value="${optionName}" ${checked}/>
                                        <label class="inline" for="investigated_${optionName}">
                                            <s:property value="%{getText(resourceKey)}"/>
                                        </label>
                                    </li>
                                </s:iterator>
                            </ul>
                            <s:fielderror cssClass="fielderror" fieldName="object.investigator.investigated"/>

                            <div id="empty_default"></div>

                            <!--/Yes Research Misconduct -->
                            <div id="pi_research_misconduct_YES">
                                <s:textarea name="object.investigator.investigationComment" id="investigationComment"
                                          rows="3" cols="20" cssStyle="width:630px;"
                                          labelSeparator="" required="true" requiredposition="right"
                                          labelposition="top" label="%{getText('specimenRequest.investigator.explain')}"/>
                            </div>

                            <!-- No Research Misconduct -->
                            <div id="pi_research_misconduct_NO"></div>
                            <s:fielderror cssClass="fielderror">
                                <s:param>object.investigator.investigationCommentValid</s:param>
                                <s:param>object.investigator.investigationCommentAllowed</s:param>
                            </s:fielderror>

                            <div class="line"></div>

                        </div>
                    </s:if>
                    <s:else>
                        <s:set name="notApplicableEnum"
                            value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@NOT_APPLICABLE}" />
                        <s:hidden name="object.investigator.certified" value="%{#notApplicableEnum.name()}"/>
                        <s:hidden name="object.investigator.investigated" value="%{#notApplicableEnum.name()}"/>
                    </s:else>

                </div>

                <c:set var="sectionCount" value="${sectionCount + 1}"/>
                <h2 class="noline">
                    <a href="javascript:;" onclick="toggleVisibility('shipping_details');" class="toggle">
                        <fmt:message key="specimenRequest.shipment.title">
                            <fmt:param value="${sectionCount}"/>
                        </fmt:message>
                    </a>
                </h2>

                <div id="shipping_details">
                    <div class="formcol_wide" style="width:300px">
                        <s:textarea name="object.shipment.instructions" id="shippingInstructions"
                                rows="3" cols="20" cssStyle="width:280px; height:5.9em;" labelSeparator=""
                                labelposition="top" label="%{getText('specimenRequest.shipment.instructions')}"/>
                    </div>

                    <div class="clear"></div>
                    <br />
                    <div class="line"></div>

                </div>

                <c:set var="sectionCount" value="${sectionCount + 1}"/>
                <h2 class="noline">
                    <a href="javascript:;" onclick="toggleVisibility('shipping_address');" class="toggle">
                        <fmt:message key="specimenRequest.shipment.recipient.title">
                            <fmt:param value="${sectionCount}"/>
                        </fmt:message>
                    </a>
                </h2>

                <div id="shipping_address">

                    <tissuelocator:editPerson copyEnabled="true" propertyPrefix="object.shipment.recipient"
                        resourcePrefix="specimenRequest.shipment.recipient" idPrefix="recipient"
                        countryValue="${object.shipment.recipient.address.country}"
                        stateValue="${object.shipment.recipient.address.state}"
                        showInstitution="true" showFax="false"/>

                    <div class="clear"></div>
                    <br />
                    <div class="line"></div>

                </div>

                <c:if test="${displayShipmentBillingRecipient}">
                    <c:set var="sectionCount" value="${sectionCount + 1}"/>
                    <fmt:message key="specimenRequest.shipment.billingRecipient.title" var="billingSectionTitle">
                         <fmt:param value="${sectionCount}"/>
                    </fmt:message>
                    <tissuelocator:editBillingRecipient sectionTitle="${billingSectionTitle}" propertyPrefix="object.shipment.billingRecipient"
                     billingRecipient="${object.shipment.billingRecipient}" copyEnabled="true" resourcePrefix="specimenRequest.shipment.billingRecipient"
                     formName="requestForm"/>
                </c:if>

                <c:set var="sectionCount" value="${sectionCount + 1}"/>
                <h2 class="noline">
                    <a href="javascript:;" onclick="toggleVisibility('general_info');" class="toggle">
                        <fmt:message key="specimenRequest.generalInfo.title">
                            <fmt:param value="${sectionCount}"/>
                        </fmt:message>
                    </a>
                </h2>

                <div id="general_info">
                    <s:if test="%{!uiDynamicFieldCategories.isEmpty()}">
                        <s:subset source="uiDynamicFieldCategories.values()" decider="uiDynamicFieldCategoryDecider">
                            <s:iterator var="uiDynamicFieldCategory">
                                <s:set var="dynamicFieldDefinitions"
                                       value="%{getDynamicFieldDefinitionsByCategory(#uiDynamicFieldCategory)}"/>
                                <s:if test="%{!#dynamicFieldDefinitions.isEmpty()}">
                                    <div class="clear"><br /></div>
                                    <s:set var="categoryName" value="%{#uiDynamicFieldCategory.categoryName}"/>
                                    <h2>${categoryName}</h2>
                                    <div class="datacol">
                                        <tissuelocator:editExtensions
                                            dynamicFieldDefinitions="${dynamicFieldDefinitions}"
                                            layout="boxLayout"
                                            extendableEntity="${object}"
                                            propertyPath="object"
                                            helpUrlBase="${helpUrlBase}"/>
                                    </div>
                                </s:if>
                            </s:iterator>
                        </s:subset>
                        <div class="clear"><br /></div>
                    </s:if>

                    <s:textarea name="object.generalComment" id="generalComment"
                            rows="6" cols="20" cssStyle="width:630px;" labelSeparator=""
                            labelposition="top" label="%{getText('specimenRequest.generalComent')}"/>
                </div>

            </div>

            <s:set var="specificLineItems" value="%{specificLineItems}"/>
            <s:set var="generalLineItems" value="%{generalLineItems}"/>
            <s:if test="%{displayAggregateSearchResults}">
                <tissuelocator:aggregateCartTable specificLineItems="${specificLineItems}" generalLineItems="${generalLineItems}" cartPrefix="aggregate" showFinalPriceColumn="false"
                showOrderFulfillmentForms="false"/>
            </s:if>
            <s:else>
                <tissuelocator:cartTable showFinalPriceColumn="false" requestLineItems="${object.lineItems}"
                        showOrderFulfillmentForms="false" showSpecimenLinks="true" />
            </s:else>

            <div class="clear"><br /></div>

            <c:if test="${!empty object.prospectiveCollectionNotes}">
                <label><fmt:message key="specimenRequest.prospectiveCollectionNotes"/></label>
                ${object.prospectiveCollectionNotes}
            </c:if>
            <div class="clear"><br /></div>

            <div class="btn_bar">
                <s:submit value="%{getText('specimenRequest.edit.continue')}" name="submit"
                    id="btn_save" cssClass="btn" theme="simple" title="%{getText('specimenRequest.edit.continue.button')}"/>
                <a href="${cartUrl}" class="btn">
                    <c:choose>
                        <c:when test="${empty object.id}">
                            <fmt:message key="specimenRequest.edit.back"/>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="specimenRequest.edit.reviseCart"/>
                        </c:otherwise>
                    </c:choose>
                </a>
                <s:submit value="%{getText('specimenRequest.edit.saveDraft')}" name="submit"
                    id="btn_saveDraft" cssClass="btn" theme="simple" title="%{getText('specimenRequest.edit.saveDraft.button')}" />
                <c:if test="${isDraft}">
                    <tissuelocator:determinePersistenceType urlPrefix="/protected/request/delete" urlEnding=".action"/>
                    <c:url var="deleteRequestUrl" value="${outputUrl}">
                        <c:param name="object.id" value="${object.id}"/>
                    </c:url>
                    <fmt:message key="specimenRequest.delete.confirm" var="confirmDelete"/>
                    <a href="#" class="btn" onclick="confirmAndContinue('${confirmDelete}','${deleteRequestUrl}'); return false;"><fmt:message key="btn.delete.draft" /></a>
                </c:if>
            </div>

        </s:form>
    </body>
</html>
