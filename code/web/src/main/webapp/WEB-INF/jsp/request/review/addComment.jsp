<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.review.addComment.title" /></title>
</head>
<body>
    <req:isUserInRole role="reviewCommenter">
        <s:form id="reviewForm" action="%{'/admin/request/review/' + reviewProcess.namespace + '/popup/addComment.action'}" target="_self">
            <s:hidden key="object.id" />
            <s:textarea name="comment.comment" id="comment" cols="40" rows="3" required="true" requiredposition="right"
                    labelposition="top" label="%{getText('comment.comment')}"
                    labelSeparator=""/>

            <div class="clear"><br /></div>

            <s:submit name="btn_saveComment"
                            cssClass="btn" theme="simple" title="%{getText('btn.submit')}"/>
        </s:form>
    </req:isUserInRole>
</body>
</html>
