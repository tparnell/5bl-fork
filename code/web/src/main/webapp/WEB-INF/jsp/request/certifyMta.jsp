<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<html>
    <head>
        <title><fmt:message key="specimenRequest.certifyMta.title" /></title>
    </head>
    <body>
    <p><fmt:message key="specimenRequest.edit.text"/></p>

    <tissuelocator:determinePersistenceType urlPrefix="/protected/request/certifyMta" urlEnding=".action"/>
    <s:form id="requestForm" action="%{#attr.request.outputUrl}" enctype="multipart/form-data" method="POST">
        <tissuelocator:messages breakCount="2" escape="false"/>

        <div id="requestform">
            <s:hidden name="previousStatus" value="%{object.status}"/>

            <h2 class="noline_top">
                <a href="javascript:;" onclick="toggleVisibility('mta');" class="toggle">
                    <fmt:message key="specimenRequest.mta.title"/>
                </a>
            </h2>

            <div id="mta">

                <p><fmt:message key="specimenRequest.mtaCertificationRequired.text"/></p>

                <c:set var="signedMtaInvalid" value="false"/>
                <s:if test="%{signedRecipientMtaValid}">
                    <fmt:message key="specimenRequest.signedRecipientMTA.valid" var="signedMtaMessage"/>
                </s:if>
                <s:elseif test="%{signedRecipientMtaPending}">
                    <fmt:message key="specimenRequest.signedRecipientMTA.pending" var="signedMtaMessage"/>
                </s:elseif>
                <s:else>
                    <c:set var="signedMtaInvalid" value="true"/>
                    <fmt:message key="specimenRequest.signedRecipientMTA.invalid" var="signedMtaMessage"/>
                    <fmt:message key="specimenRequest.signedRecipientMTA.invalid.note.faq" var="mtaInvalidMessage"/>
                </s:else>

                <c:url value="/images/arrow_right.gif" var="arrowUrl"/>
                <p>
                    <img src="${arrowUrl}" alt="" style="vertical-align:middle; margin:0 0 .25em 0;" /> <strong>${signedMtaMessage}</strong>
                    <c:if test="${signedMtaInvalid}">
                        <c:url value="/static/mtaFaq.jsp" var="mtaFaqUrl"/>
                        <fmt:message key="specimenRequest.signedRecipientMTA.invalid.faq.url" var="mtaFaqUrlText"/>
                        <br><span style="margin-left:20px;">${mtaInvalidMessage} <a href="${mtaFaqUrl}" target="_blank"><strong>${mtaFaqUrlText}</strong></a></span>
                    </c:if>


                <s:set var="mtaUploadRequired" value="%{!signedRecipientMtaValid && !signedRecipientMtaPending}"/>
                <s:if test="%{#mtaUploadRequired}">
                    <fmt:message key="specimenRequest.mta.downloadAndSign" var="mtaDownloadMessage"/>
                </s:if>
                <s:else>
                    <fmt:message key="specimenRequest.mta.downloadAndReview" var="mtaDownloadMessage"/>
                </s:else>

                <h3>${mtaDownloadMessage}</h3>

                <s:if test="%{currentMta != null}">
                    <c:url value="/protected/downloadFile.action" var="downloadMtaUrl">
                        <c:param name="file.id" value="${currentMta.document.lob.id}"/>
                        <c:param name="fileName" value="${currentMta.document.name}"/>
                        <c:param name="contentType" value="${currentMta.document.contentType}"/>
                    </c:url>
                    <a href="${downloadMtaUrl}" class="btn"><fmt:message key="specimenRequest.mta.download" /></a>
                </s:if>
                <s:else>
                    <p class="important"><fmt:message key="specimenRequest.currentMta.unavailable"/></p>
                </s:else>


                <br />
                <br />
                <div class="line"></div>

                <s:if test="%{#mtaUploadRequired && currentMta != null}">
                    <h3><fmt:message key="specimenRequest.mta.signAndUpload"/></h3>

                    <div id="mta_upb" class="wwgrp">

                        <s:file name="submittedMtaDocument" id="submittedMtaDocument" size="28"
                            label="%{getText('specimenRequest.submittedMtaDocument')}" labelposition="top"
                            labelSeparator=""/>
                        <div class="note">Uploaded files cannot be deleted.</div>
                        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/uploadMta" urlEnding=".action" asUrl="true"/>
                        <s:submit value="%{getText('specimenRequest.mta.upload')}" name="btn_upload_mta" cssClass="btn" theme="simple"
                            cssStyle="margin-top:8px;" onclick="$('#requestForm').get(0).action='%{#attr.request.outputUrl}';" title="%{getText('specimenRequest.mta.upload.button')}"/>

                    </div>


                    <br />
                    <br />
                    <div class="line"></div>

                </s:if>

                <h3><fmt:message key="specimenRequest.certifyMta.confirm"/></h3>

                <p><fmt:message key="specimenRequest.certifyMta.confirm.text"/></p>
                <s:textfield name="object.mtaCertification" id="mtaCertification" size="4" maxlength="254" required="true" requiredposition="right"
                    labelposition="top" label="%{getText('specimenRequest.mtaCertification.initial')}" labelSeparator=""/>

            </div>

            <div class="clear"></div>
            <br />
            <br />
        </div>

        <div class="clear"><br /></div>

        <div class="btn_bar">

            <s:submit value="%{getText('specimenRequest.edit.continue')}" name="submit" id="btn_save" cssClass="btn" theme="simple" title="%{getText('specimenRequest.edit.continue.button')}"/>

            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/updateCart" urlEnding=".action" asUrl="true"/>
            <a href="${outputUrl}" class="btn tiny"><fmt:message key="specimenRequest.edit.back"/></a>

            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/saveDraftOrConfirm" urlEnding=".action" asUrl="true"/>
            <s:submit value="%{getText('specimenRequest.edit.saveDraft')}" name="submit" title="%{getText('specimenRequest.edit.saveDraft.button')}"
                id="btn_saveDraft" cssClass="btn tiny" theme="simple" onclick="$('#requestForm').get(0).action='%{#attr.request.outputUrl}';" />

       </div>

   </s:form>

    </body>
</html>