<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>

<html>
    <head>
        <title><fmt:message key="specimenRequest.confirm.title" /></title>
    </head>
    <body>

        <tissuelocator:requestProcessSteps selected="confirm"/>
        <h1><fmt:message key="specimenRequest.confirm.title" /></h1>

        <p><fmt:message key="specimenRequest.confirm.text"/></p>
        <tissuelocator:externalCommentDisplay specimenRequest="${object}"/>

        <c:set var="cartDivId" value="cart"/>
        <s:if test="%{displayAggregateSearchResults}">
            <c:set var="cartDivId" value="aggregatecart"/>
        </s:if>
        <h2 class="formtop">
            <a href="javascript:;" onclick="toggleVisibility('${cartDivId}');" class="toggle">
               <fmt:message key="specimenRequest.lineItems" />
            </a>
        </h2>

        <tissuelocator:requestReadOnlyView specimenRequest="${object}" showCart="true"/>

        <div class="clear"><br /></div>

        <div class="btn_bar">
            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/save" urlEnding=".action"/>
            <s:form theme="simple" action="%{#attr.request.outputUrl}">
                <s:token/>
                <s:hidden name="previousStatus" value="%{previousStatus}"/>
                <s:submit value="%{getText('specimenRequest.confirm.save')}" name="submit" id="btn_save_request" cssClass="btn" theme="simple"
                      title="%{getText('specimenRequest.confirm.save.button')}"/>
            </s:form>
            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/input" urlEnding=".action" asUrl="true"/>
            <a href="${outputUrl}" class="btn">
                <fmt:message key="specimenRequest.confirm.back"/>
            </a>
        </div>

    </body>
</html>

