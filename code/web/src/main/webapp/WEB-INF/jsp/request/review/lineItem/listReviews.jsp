<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.review.list.title" /></title>
</head>
<body>
    <tissuelocator:requestListTabs isAdmin="true" selectedTab="specimen"
        subtitleKey="specimenRequest.myrequests.subtitle"/>
    <tissuelocator:specimenRequestList filterActionBase="/admin/request/review/lineItem"
        viewAction="viewSummary" exportEnabled="true" dualPersistencePossible="false"
        includeUnsubmittedRequests="false"/>
</body>
</html>
