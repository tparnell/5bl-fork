<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<s:set var="minPropName" value="%{searchFieldConfig.minSearchFieldName}"/>
<s:set var="maxPropName" value="%{searchFieldConfig.maxSearchFieldName}"/>

<s:set var="isObjectProperty" value="%{searchFieldConfig.objectProperty}"/>
<s:set var="helpBaseUrl" value="searchFieldConfig.helpBaseUrl"/>

<s:if test="%{isObjectProperty}">
    <s:set var="minPropName" value="%{'object.' + #minPropName}"/>
    <s:set var="maxPropName" value="%{'object.' + #maxPropName}"/>
</s:if>

<s:set var="label">${searchFieldConfig.searchFieldDisplayName}</s:set>

<td scope="col" class="label" nowrap="nowrap">
    <tissuelocator:helpLabel helpUrlBase="${helpBaseUrl}"
                             labelFor="${searchFieldConfig.searchFieldName}"
                             fieldDisplayName="${label}"
                             fieldName="${searchFieldConfig.searchFieldName}"
                             required="false"/>
</td>
<td scope="col" class="value">
    <s:textfield name="%{'rangeSearchParameters[\\'' + #minPropName + '\\']'}"
        id="%{minPropName}"
        theme="simple"
        size="15"
        title="%{getText('specimen.search.min.field')} %{searchFieldConfig.searchFieldDisplayName}"
        cssClass="%{searchFieldConfig.cssClasses}"/>
    <span class="${searchFieldConfig.cssClasses}">&ndash;</span>
    <s:textfield name="%{'rangeSearchParameters[\\'' + #maxPropName + '\\']'}"
        id="%{maxPropName}" theme="simple"
        size="15"
        title="%{getText('specimen.search.max.field')} %{searchFieldConfig.searchFieldDisplayName}"
        cssClass="%{searchFieldConfig.cssClasses}"/>
    <s:if test="%{searchFieldConfig.note != null}">
        ${searchFieldConfig.note}
    </s:if>
    <s:fielderror cssClass="fielderror">
        <s:param value="%{minPropName}"/>
    </s:fielderror>
    <s:fielderror cssClass="fielderror">
        <s:param value="%{maxPropName}"/>
    </s:fielderror>
    <s:if test="%{searchFieldConfig.errorProperty != null}">
        <s:fielderror cssClass="fielderror">
            <s:param value="%{searchFieldConfig.errorProperty}"/>
        </s:fielderror>
    </s:if>
</td>