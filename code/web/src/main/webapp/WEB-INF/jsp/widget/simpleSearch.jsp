<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<div id="search_home">
    <h2><fmt:message key="home.search"/></h2>

    <s:form id="filterForm" action="specimen/search/filter.action" namespace="/protected">
        <table class="data" id="simplesearchtable">
            <tr>
                <td scope="col" class="value" colspan="2">
                    <s:iterator value="%{@com.fiveamsolutions.tissuelocator.data.SpecimenClass@values()}">
                        <s:set value="%{name()}" var="specimenClassName"/>
                        <input type="radio" name="object.specimenType.specimenClass"
                            id="specimenClass_${specimenClassName}" value="${specimenClassName}" />
                        <label class="inline" for="specimenClass_${specimenClassName}">
                            <s:property value="%{getText(resourceKey)}"/>
                        </label>
                        &nbsp;&nbsp;
                    </s:iterator>
                </td>
            </tr>
            <tr>
                <td scope="col" class="label"><label for="pathologicalCharacteristic"><s:property value="%{getText('home.pathologicalCharacteristic.empty')}"/></label></td>
                <td scope="col" class="value">
                    <s:textfield name="object.pathologicalCharacteristic.name" id="pathologicalCharacteristic"
                        size="20" maxlength="50" cssStyle="width:12em" theme="simple" />
                </td>
            </tr>
            <s:set var="bean" scope="request"/>
            <s:iterator value="simpleSearchConfigs" var="config" status="status">
                <tr>
                    <s:set var="actionName" value="%{'get' + #config.class.simpleName + 'Widget'}"/>
                    <s:action namespace="/widget" name="search/%{#actionName}" executeResult="true"
                        ignoreContextParams="true" >
                        <s:param name="searchFieldConfig.id" value="%{#config.id}"/>
                        <s:param name="searchSetType" value="WIDGET"/>
                    </s:action>
                </tr>
            </s:iterator>
        </table>

        <div class="vpad5" style="padding-left: 10px;">
            <s:submit value="%{getText('home.search.button')}" name="submit" title="%{getText('home.search.button.button')}"
                id="btn_search" theme="simple" cssClass="btn"/>
        </div>

    </s:form>

    <p class="advanced" style="padding-left: 10px;">
        <c:url value="/protected/specimen/search/refreshList.action" var="listUrl"/>
        <a href="${listUrl}">
            <fmt:message key="home.search.advanced"/>
        </a>
    </p>
</div>
