<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<div id="type">
    <h2><fmt:message key="home.browse.type"/></h2>
    <ul>
        <s:iterator value="%{@com.fiveamsolutions.tissuelocator.data.SpecimenClass@values()}" var="specClass">
            <s:url namespace="/protected" action="specimen/search/filter.action" var="classUrl">
                <s:param name="object.specimenType.specimenClass">
                    <s:property value="%{#specClass.name()}"/>
                </s:param>
            </s:url>
            <li>
                <s:set value="%{getText(#specClass.resourceKey + '.cssClass')}" var="classClass"/>
                <a href="${classUrl}" class="${classClass}">
                    <s:property value="%{getText(#specClass.resourceKey)}"/>
                    <div class="count">
                        (<s:property value="%{countsByClass.get(#specClass)}" default="0"/>)
                    </div>
                </a>
            </li>
        </s:iterator>
    </ul>
</div>