<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<s:set var="textPropName" value="searchFieldConfig.textConfig.searchFieldName"/>
<s:set var="selectPropName" value="selectConfig.searchFieldName"/>
<s:set var="isTextObjectProperty" value="%{searchFieldConfig.textConfig.objectProperty}"/>
<s:set var="isSelectObjectProperty" value="%{selectConfig.objectProperty}"/>
<s:set var="helpBaseUrl" value="searchFieldConfig.helpBaseUrl"/>

<s:if test="%{isTextObjectProperty}">
 <s:set var="textPropName" value="%{'object.' + #textPropName}"/>
</s:if>
<s:if test="%{isSelectObjectProperty}">
 <s:set var="selectPropName" value="%{'object.' + #selectPropName}"/>
</s:if>

<s:set var="label">${searchFieldConfig.textConfig.searchFieldDisplayName}</s:set>

<td scope="col" class="label" nowrap="nowrap">
    <tissuelocator:helpLabel helpUrlBase="${helpBaseUrl}"
                             labelFor="${textPropName}"
                             fieldDisplayName="${label}"
                             fieldName="${textPropName}"
                             required="false"/>
</td>
<td scope="col" class="value">
    <s:textfield name="%{textPropName}" id="%{searchFieldConfig.textConfig.searchFieldName}"
        size="15" theme="simple"
        title="%{getText('specimen.search.property.field')} %{searchFieldConfig.textConfig.searchFieldDisplayName}"/>
    <tissuelocator:searchSelect name="${selectPropName}" selectSearchConfig="${selectConfig}"
        optionList="${selectOptions}"/>

    <s:fielderror cssClass="fielderror">
        <s:param value="%{textPropName}"/>
    </s:fielderror>
    <s:if test="%{searchFieldConfig.textConfig.errorProperty != null}">
        <s:fielderror cssClass="fielderror">
            <s:param value="%{searchFieldConfig.textConfig.errorProperty}"/>
        </s:fielderror>
    </s:if>
</td>