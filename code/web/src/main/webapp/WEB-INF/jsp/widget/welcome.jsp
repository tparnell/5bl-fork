<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<c:url value="/notYetImplemented.jsp" var="notYetImplementedUrl"/>
<c:url value="/register/input.action" var="registerUrl"/>
<fmt:message var="aboutLinkUrl" key="home.welcome.learnMore.url" />
<div id="search_home">
    <h2><fmt:message key="login.text.title"/></h2>
    <p class="intro">
        <fmt:message key="login.text.text"/>
        <br /><br />
        <c:choose>
          <c:when test='${not fn:startsWith(aboutLinkUrl, "??")}'>
            <a href="${aboutLinkUrl}" target="_blank"><fmt:message key="login.link.about"/></a>
          </c:when>
          <c:otherwise>
            <a href="${notYetImplementedUrl}"><fmt:message key="login.link.about"/></a>
          </c:otherwise>
        </c:choose>
        |
        <a href="${registerUrl}">
            <fmt:message key="login.link.register"/>
        </a>
    </p>
</div>