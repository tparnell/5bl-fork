<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="reports.specimensByInstitutionReport.title"/></title>
</head>
<body>
    <fmt:message key="default.date.format" var="dateFormat"/>
    <div id="reports">
        <div id="requestform" style="background: none repeat scroll 0 0 transparent;">
            <div id="reportcontent"><s:property value="%{reportContent}" escapeHtml="false"/></div>
        </div>
    </div>
</body>
</html>
