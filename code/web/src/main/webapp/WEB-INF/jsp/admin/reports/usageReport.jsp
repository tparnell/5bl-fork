<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="reports.usageReport.title" /></title>
    <style>
    </style>
</head>
<body>
    <fmt:message key="default.date.format" var="dateFormat"/>
    <div id="reports">

        <tissuelocator:reportDateRange formAction="/admin/reports/usageReport.action"/>

        <div id="requestform">

            <h2 class="nobg noline">
                <fmt:message key="reports.dateRange.label">
                    <fmt:param><s:date name="%{startDate}" format="%{#attr.dateFormat}"/></fmt:param>
                    <fmt:param><s:date name="%{endDate}" format="%{#attr.dateFormat}"/></fmt:param>
                </fmt:message>
            </h2>

            <div class="col">
                <div class="box">
                    <h3><fmt:message key="reports.usageReport.usageStatistics.title"/></h3>
                    <table class="data report">
                        <tr>
                            <th scope="col" colspan="2">
                                <div><fmt:message key="reports.usageReport.usageStatistics.subtitle"/></div>
                            </th>
                        </tr>
                        <tr class="odd">
                            <td class="label"><fmt:message key="reports.usageReport.usageStatistics.registered"/></td>
                            <td class="value">
                                <c:url value="/admin/reports/newUserReport.action" var="newUserReportUrl">
                                    <c:param name="startDate">
                                        <s:date name="%{startDate}" format="%{#attr.dateFormat}"/>
                                    </c:param>
                                    <c:param name="endDate">
                                        <s:date name="%{endDate}" format="%{#attr.dateFormat}"/>
                                    </c:param>
                                </c:url>
                                <a href="${newUserReportUrl}">
                                    <s:property value="%{usageReport.newUserCount}"/>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><fmt:message key="reports.usageReport.usageStatistics.requests.restricted"/></td>
                            <td class="value"><s:property value="%{usageReport.restrictedNewRequestCount}"/></td>
                        </tr>
                        <tr class="odd">
                            <td class="label"><fmt:message key="reports.usageReport.usageStatistics.requests.remaining"/></td>
                            <td class="value">
                                <s:property value="%{usageReport.unrestrictedNewRequestCount - usageReport.restrictedNewRequestCount}"/>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="box">
                    <h3><fmt:message key="reports.usageReport.shipmentsAndReturns.title"/></h3>
                    <table class="data report">
                        <tr>
                            <th scope="col" colspan="2">
                                <div><fmt:message key="reports.usageReport.shipmentsAndReturns.subtitle"/></div>
                            </th>
                        </tr>
                        <tr class="odd">
                            <td class="label"><fmt:message key="reports.usageReport.shipmentsAndReturns.shipments"/></td>
                            <td class="value"><s:property value="%{usageReport.shippedOrderCount}"/></td>
                        </tr>
                        <tr>
                            <td class="label"><fmt:message key="reports.usageReport.shipmentsAndReturns.returnsRequested"/></td>
                            <td class="value"><s:property value="%{usageReport.totalReturnsRequested}"/></td>
                        </tr>
                        <tr class="odd">
                            <td class="label"><fmt:message key="reports.usageReport.shipmentsAndReturns.returnsFulfilled"/></td>
                            <td class="value"><s:property value="%{usageReport.totalReturnsFulfilled}"/></td>
                        </tr>
                        <tr>
                            <td class="label"><fmt:message key="reports.usageReport.shipmentsAndReturns.unfulfilledReturns"/></td>
                            <td class="value"><s:property value="%{usageReport.totalUnfulfilledReturnRequests}"/></td>
                        </tr>
                    </table>
                    
                    <c:if test="${usageReport.totalReturnsRequested > 0}">
                        <table class="data report">
                            <tr>
                                <th scope="col" colspan="2">
                                    <div><fmt:message key="reports.usageReport.returnRequests.participant.subtitle"/></div>
                                </th>
                            </tr>
                            <c:set var="pIndex" value="${0}"/>
                            <c:set var="rowClass" value="class=\"odd\""/>
                            <c:forEach items="${usageReport.participantReturnRequestMap}" var="participantEntry">
                            <c:if test="${participantEntry.value.returnsRequested > 0}">
                                <c:choose>
                                    <c:when test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == participantEntry.value.externalIdAssignerId}">
                                        <c:url var="participantUrl" value="/admin/participant/input.action">
                                            <c:param name="object.id" value="${participantEntry.key}"/>
                                        </c:url>
                                        <fmt:message key="reports.usageReport.shipmentsAndReturns.participant.link" var="participantLabel">
                                            <fmt:param value="${participantUrl}"/>
                                            <fmt:param value="${participantEntry.value.externalId}"/>
                                        </fmt:message>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="reports.usageReport.shipmentsAndReturns.participant.participant" var="participantLabel">
                                            <fmt:param value="${participantEntry.value.externalId}"/>
                                        </fmt:message>
                                    </c:otherwise>
                                </c:choose>
                                <tr ${rowClass}>
                                    <td class="label">${participantLabel}</td>
                                    <td class="value">${participantEntry.value.returnsRequested}</td>
                                </tr>
                                <c:choose>
                                    <c:when test="${pIndex % 2 == 0}">
                                        <c:set var="rowClass" value=""/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="rowClass" value="class=\"odd\""/>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="pIndex" value="${pIndex + 1}"/>
                            </c:if>
                            </c:forEach>
                        </table>
                    </c:if>
                    
                    <c:if test="${usageReport.totalReturnsFulfilled > 0}">
                        <table class="data report">
                            <tr>
                                <th scope="col" colspan="2">
                                    <div><fmt:message key="reports.usageReport.returnsFulfilled.participant.subtitle"/></div>
                                </th>
                            </tr>
                            <c:set var="pIndex" value="${0}"/>
                            <c:set var="rowClass" value="class=\"odd\""/>
                            <c:forEach items="${usageReport.participantReturnRequestMap}" var="participantEntry">
                            <c:if test="${participantEntry.value.returnsFulfilled > 0}">
                                <c:choose>
                                    <c:when test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == participantEntry.value.externalIdAssignerId}">
                                        <c:url var="participantUrl" value="/admin/participant/input.action">
                                            <c:param name="object.id" value="${participantEntry.key}"/>
                                        </c:url>
                                        <fmt:message key="reports.usageReport.shipmentsAndReturns.participant.link" var="participantLabel">
                                            <fmt:param value="${participantUrl}"/>
                                            <fmt:param value="${participantEntry.value.externalId}"/>
                                        </fmt:message>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="reports.usageReport.shipmentsAndReturns.participant.participant" var="participantLabel">
                                            <fmt:param value="${participantEntry.value.externalId}"/>
                                        </fmt:message>
                                    </c:otherwise>
                                </c:choose>
                                <tr ${rowClass}>
                                    <td class="label">${participantLabel}</td>
                                    <td class="value">${participantEntry.value.returnsFulfilled}</td>
                                </tr>
                                <c:choose>
                                    <c:when test="${pIndex % 2 == 0}">
                                        <c:set var="rowClass" value=""/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="rowClass" value="class=\"odd\""/>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="pIndex" value="${pIndex + 1}"/>
                            </c:if>
                            </c:forEach>
                        </table>
                    </c:if>
                    
                    <c:if test="${usageReport.totalUnfulfilledReturnRequests > 0}">
                        <table class="data report">
                            <tr>
                                <th scope="col" colspan="2">
                                    <div><fmt:message key="reports.usageReport.unfulfilledReturns.participant.subtitle"/></div>
                                </th>
                            </tr>
                            <c:set var="pIndex" value="${0}"/>
                            <c:set var="rowClass" value="class=\"odd\""/>
                            <c:forEach items="${usageReport.participantReturnRequestMap}" var="participantEntry">
                            <c:if test="${participantEntry.value.unfulfilledReturnRequests > 0}">
                                <c:choose>
                                    <c:when test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == participantEntry.value.externalIdAssignerId}">
                                        <c:url var="participantUrl" value="/admin/participant/input.action">
                                            <c:param name="object.id" value="${participantEntry.key}"/>
                                        </c:url>
                                        <fmt:message key="reports.usageReport.shipmentsAndReturns.participant.link" var="participantLabel">
                                            <fmt:param value="${participantUrl}"/>
                                            <fmt:param value="${participantEntry.value.externalId}"/>
                                        </fmt:message>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="reports.usageReport.shipmentsAndReturns.participant.participant" var="participantLabel">
                                            <fmt:param value="${participantEntry.value.externalId}"/>
                                        </fmt:message>
                                    </c:otherwise>
                                </c:choose>
                                <tr ${rowClass}>
                                    <td class="label">${participantLabel}</td>
                                    <td class="value">${participantEntry.value.unfulfilledReturnRequests}</td>
                                </tr>
                                <c:choose>
                                    <c:when test="${pIndex % 2 == 0}">
                                        <c:set var="rowClass" value=""/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="rowClass" value="class=\"odd\""/>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="pIndex" value="${pIndex + 1}"/>
                            </c:if>
                            </c:forEach>
                        </table>
                    </c:if>
                    
                </div>
            </div>

            <div class="col">

                <div class="box">
                    <h3><fmt:message key="reports.usageReport.tissueTypes.title"/></h3>
                    <table class="data report">
                        <tr>
                            <th scope="col" colspan="2">
                                <div><fmt:message key="reports.usageReport.tissueTypes.subtitle"/></div>
                            </th>
                        </tr>
                        <s:iterator value="%{usageReport.topSpecimenTypes}" status="topSpecimenTypesStatus"
                            var="topSpecimenType">
                            <c:set var="rowClass" value=""/>
                            <s:if test="%{#topSpecimenTypesStatus.index % 2 == 0}">
                                <c:set var="rowClass" value="odd"/>
                            </s:if>
                            <tr class="${rowClass}">
                                <td class="label"><s:property value="%{name}"/></td>
                                <td class="value"><s:property value="%{count}"/></td>
                            </tr>
                        </s:iterator>
                    </table>
                </div>

            </div>
            <div class="clear"></div>
        </div>
    </div>
</body>
</html>
