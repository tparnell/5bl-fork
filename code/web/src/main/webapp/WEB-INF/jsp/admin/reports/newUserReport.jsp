<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="reports.newUserReport.title" /></title>
</head>
<body>
    <fmt:message key="default.date.format" var="dateFormat"/>
    <div id="reports">
        <tissuelocator:reportDateRange formAction="/admin/reports/newUserReport.action"/>

        <div id="requestform" style="background: none repeat scroll 0 0 transparent;">
            <h2 class="nobg noline">
                <fmt:message key="reports.dateRange.label">
                    <fmt:param><s:date name="%{startDate}" format="%{#attr.dateFormat}"/></fmt:param>
                    <fmt:param><s:date name="%{endDate}" format="%{#attr.dateFormat}"/></fmt:param>
                </fmt:message>
            </h2>

            <div id="reportcontent"><s:property value="%{reportContent}" escapeHtml="false"/></div>

            <s:if test="%{lastPage > 0}">
                <s:set var="firstPageIndex" value="%{page - 4}" scope="page"/>
                <s:if test="%{#attr.firstPageIndex < 0}">
                    <s:set var="firstPageIndex" value="%{0}" scope="page"/>
                </s:if>
                <s:set var="lastPageIndex" value="%{#attr.firstPageIndex + 7}" scope="page"/>
                <s:if test="%{#attr.lastPageIndex >= lastPage}">
                    <s:set var="lastPageIndex" value="%{lastPage - 1}" scope="page"/>
                </s:if>
                <s:if test="%{#attr.lastPageIndex - #attr.firstPageIndex < 7}">
                    <s:set var="firstPageIndex" value="%{#attr.lastPageIndex - 7}" scope="page"/>
                    <s:if test="%{#attr.firstPageIndex < 0}">
                        <s:set var="firstPageIndex" value="%{0}" scope="page"/>
                    </s:if>
                </s:if>

                <c:url value="/admin/reports/newUserReport.action" var="firstPageUrl">
                    <c:param name="page"><s:property value="%{0}"/></c:param>
                    <c:param name="startDate">
                        <s:date name="%{startDate}" format="%{#attr.dateFormat}"/>
                    </c:param>
                    <c:param name="endDate">
                        <s:date name="%{endDate}" format="%{#attr.dateFormat}"/>
                    </c:param>
                </c:url>
                <c:url value="/admin/reports/newUserReport.action" var="prevPageUrl">
                    <c:param name="page"><s:property value="%{page - 1}"/></c:param>
                    <c:param name="startDate">
                        <s:date name="%{startDate}" format="%{#attr.dateFormat}"/>
                    </c:param>
                    <c:param name="endDate">
                        <s:date name="%{endDate}" format="%{#attr.dateFormat}"/>
                    </c:param>
                </c:url>
                <c:url value="/admin/reports/newUserReport.action" var="nextPageUrl">
                    <c:param name="page"><s:property value="%{page + 1}"/></c:param>
                    <c:param name="startDate">
                        <s:date name="%{startDate}" format="%{#attr.dateFormat}"/>
                    </c:param>
                    <c:param name="endDate">
                        <s:date name="%{endDate}" format="%{#attr.dateFormat}"/>
                    </c:param>
                </c:url>
                <c:url value="/admin/reports/newUserReport.action" var="lastPageUrl">
                    <c:param name="page"><s:property value="%{lastPage - 1}"/></c:param>
                    <c:param name="startDate">
                        <s:date name="%{startDate}" format="%{#attr.dateFormat}"/>
                    </c:param>
                    <c:param name="endDate">
                        <s:date name="%{endDate}" format="%{#attr.dateFormat}"/>
                    </c:param>
                </c:url>
                <fmt:message key="displaytag.first" var="firstLabel"/>
                <fmt:message key="displaytag.prev" var="prevLabel"/>
                <fmt:message key="displaytag.next" var="nextLabel"/>
                <fmt:message key="displaytag.last" var="lastLabel"/>
                <fmt:message key="displaytag.page" var="pageLabel"/>
                <s:if test="%{lastPage > 1}">
                    <fmt:message key="displaytag.pages" var="pageLabel"/>
                </s:if>
                <div class="currentpage">
                    <strong><s:property value="%{page + 1}"/></strong>
                    of <s:property value="%{lastPage}"/> ${pageLabel}
                </div>
                <div class="pagenav" style="float:right;text-align:right;">
                    <s:if test="%{page > 0}">
                        <a href="${firstPageUrl}">|&lt; ${firstLabel}</a>
                        <a href="${prevPageUrl}">&lt;&lt; ${prevLabel}</a>
                    </s:if>
                    <s:else>
                        <a href="#" class="disabled">|&lt; ${firstLabel}</a>
                        <a href="#" class="disabled">&lt;&lt; ${prevLabel}</a>
                    </s:else>
                    <c:forEach var="pageLinkIndex" begin="${firstPageIndex}" end="${lastPageIndex}" step="1">
                        <s:if test="%{page == #attr.pageLinkIndex}">
                            <a href="#" class="selected">${pageLinkIndex + 1}</a>
                        </s:if>
                        <s:else>
                            <c:url value="/admin/reports/newUserReport.action" var="pageLinkUrl">
                                <c:param name="page" value="${pageLinkIndex}"/>
                                <c:param name="startDate">
                                    <s:date name="%{startDate}" format="%{#attr.dateFormat}"/>
                                </c:param>
                                <c:param name="endDate">
                                    <s:date name="%{endDate}" format="%{#attr.dateFormat}"/>
                                </c:param>
                            </c:url>
                            <a href="${pageLinkUrl}">${pageLinkIndex + 1}</a>
                        </s:else>
                    </c:forEach>
                    <s:if test="%{lastPage - 1 > page}">
                        <a href="${nextPageUrl}">${nextLabel} &gt;&gt;</a>
                        <a href="${lastPageUrl}">${lastLabel} &gt;|</a>
                    </s:if>
                    <s:else>
                        <a href="#" class="disabled">${nextLabel} &gt;&gt;</a>
                        <a href="#" class="disabled">${lastLabel} &gt;|</a>
                    </s:else>
                </div>
                <div class="clear"></div>
            </s:if>
        </div>
    </div>
</body>
</html>
