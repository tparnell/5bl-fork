<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<c:set var="skipHeader" value="true" scope="request"/>
<c:set var="extraBodyTags" scope="request">style="margin:5px 15px"</c:set>

<html>
    <head><title><fmt:message key="reports.requestReviewReport.rejectionComments.title"/></title></head>
    <body class="submodal">
    
        <h2 class="noline"><fmt:message key="reports.requestReviewReport.rejectionComments.subtitle"/></h2>
        
        <div class="line"></div>
        <c:forEach items="${rejectionComments}" var="rejectionEntry">
            
            
            <c:url var="requestUrl" value="/protected/request/viewDb.action">
                <c:param name="object.id" value="${rejectionEntry.key}"/>
            </c:url>
            <p>
                <strong><a href="${requestUrl}" target="_blank">
                    <fmt:message key="reports.requestReviewReport.rejectionComments.request"><fmt:param>${rejectionEntry.key}</fmt:param></fmt:message>
                </a></strong><br />
                
                <c:forEach items="${rejectionEntry.value}" var="comment">
                <em>${comment}</em>
                <div class="clear"></div>
                </c:forEach>
            </p>            
            
         <div class="line"></div>   
        </c:forEach>
        
    </body>
</html>