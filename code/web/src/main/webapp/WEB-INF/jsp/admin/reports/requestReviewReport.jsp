<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="reports.requestReviewReport.title" /></title>
</head>
<body>
    <fmt:message key="default.date.format" var="dateFormat"/>
    <s:set name="approvedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.Vote@APPROVE}" />
    <s:set name="deniedEnum" value="%{@com.fiveamsolutions.tissuelocator.data.Vote@DENY}" />
    <s:set name="reviseEnum" value="%{@com.fiveamsolutions.tissuelocator.data.Vote@REVISE}" />
    <div id="reports">

        <tissuelocator:reportDateRange formAction="/admin/reports/requestReviewReport.action"/>

        <div id="requestform">

            <h2 class="nobg noline">
                <fmt:message key="reports.dateRange.label">
                    <fmt:param><s:date name="%{startDate}" format="%{#attr.dateFormat}"/></fmt:param>
                    <fmt:param><s:date name="%{endDate}" format="%{#attr.dateFormat}"/></fmt:param>
                </fmt:message>
            </h2>

            <s:iterator value="%{requestReviewReport.scientificReviewMap}">
                <div class="col">
                    <div class="box">
                        <h3>
                            <fmt:message key="reports.requestReviewReport.reviews.title"/>
                            <s:property value="%{key}"/>
                        </h3>
                        <table class="data report">
                            <tr>
                                <th scope="col" colspan="2">
                                    <div><fmt:message key="reports.requestReviewReport.reviews.subtitle"/></div>
                                </th>
                            </tr>
                            <tr class="odd">
                                <td class="label">
                                    <fmt:message key="reports.requestReviewReport.reviews.approved"/>
                                </td>
                                <td class="value"><s:property value="%{value.get(#approvedEnum)}"/></td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <fmt:message key="reports.requestReviewReport.reviews.revisionRequested"/>
                                </td>
                                <td class="value"><s:property value="%{value.get(#reviseEnum)}"/></td>
                            </tr>
                            <tr class="odd">
                                <td class="label">
                                    <fmt:message key="reports.requestReviewReport.reviews.rejected"/>
                                </td>
                                <c:url value="/admin/reports/popup/viewRejectionComments.action" var="commentsUrl">
                                    <c:param name="institutionName" value="${key}"/>
                                    <c:param name="startDate"><s:date name="%{startDate}" format="%{#attr.dateFormat}"/></c:param>
                                    <c:param name="endDate"><s:date name="%{endDate}" format="%{#attr.dateFormat}"/></c:param>
                                </c:url>
                                <td class="value">
                                    <s:set var="deniedCount" value="%{value.get(#deniedEnum)}"/>
                                    ${deniedCount}
                                    <s:if test="%{#deniedCount > 0}">
                                        <a class="submodal-400-500" href="${commentsUrl}">
                                            <fmt:message key="reports.requestReviewReport.reviews.rejected.comments"/></a>
                                    </s:if>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <fmt:message key="reports.requestReviewReport.reviews.institutionalVeto"/>
                                </td>
                                <td class="value">
                                    <s:property value="%{requestReviewReport.institutionalReviewMap.get(key).get(#deniedEnum)}"/>
                                </td>
                            </tr>
                            <tr class="odd">
                                <td class="label">
                                    <fmt:message key="reports.requestReviewReport.reviews.scientificLate"/>
                                </td>
                                <td class="value">
                                    <s:property value="%{requestReviewReport.lateScientificReviewMap.get(key)}"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <fmt:message key="reports.requestReviewReport.reviews.institutionalLate"/>
                                </td>
                                <td class="value">
                                    <s:property value="%{requestReviewReport.lateInstitutionalReviewMap.get(key)}"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </s:iterator>
            <div class="clear"></div>
        </div>
    </div>
</body>
</html>
