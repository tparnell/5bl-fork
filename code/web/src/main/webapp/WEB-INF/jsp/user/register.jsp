<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<s:set var="isUserAdmin" value="false"/>
<req:isUserInRole role="userAdmin">
    <s:set var="isUserAdmin" value="true"/>
</req:isUserInRole>
<html>
<head>
    <title>
        <s:if test="%{object.id == null}">
            <s:if test="%{#isUserAdmin}">
                <fmt:message key="user.create.title" />
            </s:if>
            <s:else>
                <fmt:message key="user.register.head.title" />
            </s:else>
        </s:if>
        <s:else>
            <fmt:message key="user.update.title" />
            <s:property value="%{object.firstName}"/>
            <s:property value="%{object.lastName}"/>
        </s:else>
    </title>
</head>
<body>

<s:if test="%{object.id == null && !#isUserAdmin}">
    <p>
        <strong><fmt:message key="user.register.why.title"/></strong>
        <fmt:message key="user.register.why.text"/>
    </p>
</s:if>

<c:set var="isRegister" value="${true}"/>
<s:set var="formAction" value="%{'register/save'}"/>
<s:if test="%{#isUserAdmin}">
    <s:set var="formAction" value="%{'user/save'}"/>
    <c:set var="isRegister" value="${false}"/>
</s:if>
<s:if test="%{#request['javax.servlet.forward.request_uri'].contains('myAccount')}">
    <s:set var="formAction" value="%{'myAccount/save'}"/>
    <c:set var="isRegister" value="${false}"/>
</s:if>
<s:form id="registerForm" action="%{#formAction}" onsubmit="return updateHiddenFields()"
    enctype="multipart/form-data" method="POST">

    <s:hidden name="object.id"/>

    <tissuelocator:messages/>

    <h2 class="formtop">
        <s:if test="%{object.id == null && !#isUserAdmin}">
            <fmt:message key="user.register.start"/>
        </s:if>
        <s:else>
            <fmt:message key="user.update.personalInfo"/>
        </s:else>
        - (<span class="reqd"><fmt:message key="required.indicator"/></span>
        <fmt:message key="required.indicator.label"/>)
    </h2>

    <div class="formcol">
        <s:textfield name="object.firstName" id="firstName" size="30" maxlength="254" cssStyle="width:20em"
                labelposition="top" label="%{getText('user.register.firstName')}" labelSeparator=""
                required="true" requiredposition="right" />
    </div>

    <div class="formcol">
        <s:textfield name="object.lastName" id="lastName" size="30" maxlength="254" cssStyle="width:20em"
                labelposition="top" label="%{getText('user.register.lastName')}" labelSeparator=""
                required="true" requiredposition="right"/>
    </div>

    <div class="clear"><br /></div>

    <s:textfield name="object.address.line1" id="address1" size="30" maxlength="254" cssStyle="width:20em"
            labelposition="top" label="%{getText('user.register.address.line1')}" labelSeparator=""
            required="true" requiredposition="right" title="%{getText('user.register.address.line1.field')}"/>

    <div class="pad5"></div>

    <s:textfield name="object.address.line2" id="address2" size="30" maxlength="254" cssStyle="width:20em"
           title="%{getText('user.register.address.line2.field')}"/>

    <div class="formcol">
        <s:textfield name="object.address.city" id="city" size="30" maxlength="254" cssStyle="width:20em"
                labelposition="top" label="%{getText('user.register.address.city')}" labelSeparator=""
                required="true" requiredposition="right"/>
    </div>

    <div class="formcol">
        <s:select name="object.address.state" id="state" value="%{object.address.state}"
            list="%{@com.fiveamsolutions.tissuelocator.data.State@values()}"
            listValue="%{getText(resourceKey)}"
            headerKey="" headerValue="%{getText('select.emptyOption')}"
            labelposition="top" label="%{getText('user.register.address.state')}" labelSeparator=""
            required="true" requiredposition="right" cssStyle="width:20em"/>
    </div>

    <div class="formcol_thin">
        <s:textfield name="object.address.zip" id="zip" size="20" maxlength="20" cssStyle="width:15em"
            labelposition="top" label="%{getText('user.register.address.zip')}" labelSeparator=""
            required="true" requiredposition="right"/>
    </div>

    <div class="formcol">
        <s:select name="object.address.country" id="country" value="%{object.address.country}"
            list="%{@com.fiveamsolutions.tissuelocator.data.Country@values()}"
            listValue="%{getText('country.' + name())}"
            labelposition="top" label="%{getText('user.register.address.country')}" labelSeparator=""
            required="true" requiredposition="right" cssStyle="width:20em"/>
    </div>

    <div class="clear"><br /></div>

    <div class="formcol">
        <s:textfield name="object.address.phone" id="phone" size="50" maxlength="50" cssStyle="width:20em"
            labelposition="top" label="%{getText('user.register.address.phone')}" labelSeparator=""
            required="true" requiredposition="right"/>
    </div>
    <div class="formcol">
        <s:textfield name="object.address.phoneExtension" id="phoneExtension" size="10" maxlength="10" cssStyle="width:6em"
            labelposition="top" label="%{getText('user.register.address.phoneExtension')}" labelSeparator=""/>
    </div>

    <div class="clear"><br /></div>
    <s:textfield name="object.address.fax" id="fax" size="50" maxlength="50" cssStyle="width:20em"
            labelposition="top" label="%{getText('user.register.address.fax')}" labelSeparator=""/>

    <div class="clear"><br /></div>

    <h2><fmt:message key="user.register.education"/></h2>

    <s:textfield name="object.title" id="title" size="20" cssStyle="width:29em"
            labelposition="top" label="%{getText('user.register.title')}" labelSeparator=""
            required="true" requiredposition="right"/>

    <s:textfield name="object.degree" id="degree" size="20" cssStyle="width:29em"
            labelposition="top" label="%{getText('user.register.degree')}" labelSeparator=""
            required="true" requiredposition="right"/>
    <div class="note"><fmt:message key="user.register.degree.note"/></div>

    <s:if test="%{userResumeRequired}">
        <fmt:message key="user.register.resume" var="resumeDisplayName"/>
        <tissuelocator:helpLabel helpUrlBase="/register/info/popup/fieldHelp.action"
            labelFor="resume" fieldDisplayName="${resumeDisplayName}"
            fieldName="resume" required="true"/>
        <s:file name="resume" id="resume" size="30" />
        <s:fielderror fieldName="object.resume" cssClass="fielderror"/>
        <s:set var="fileId" value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName).id}"/>
        <s:set var="fileName" value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fileNameFieldName)}"/>
        <s:set var="contentType" value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.contentTypeFieldName)}"/>
        <s:if test="%{object.resume.lob.id != null}">
            <div class="value">
                <c:url value="/protected/downloadFile.action" var="resumeDownloadUrl">
                    <c:param name="file.id"><s:property value="%{object.resume.lob.id}"/></c:param>
                    <c:param name="fileName"><s:property value="%{object.resume.name}"/></c:param>
                    <c:param name="contentType"><s:property value="%{object.resume.contentType}"/></c:param>
                </c:url>
                <a href="${resumeDownloadUrl}" class="file">
                    <s:property value="%{object.resume.name}"/>
                </a>
            </div>
        </s:if>
    </s:if>

    <s:if test="%{dynamicFieldDefinitions.size() > 0}">
        <h2><fmt:message key="user.register.additionalInformation"/></h2>

        <div id="extensions">
            <s:set var="dynamicFieldDefinitions" value="%{dynamicFieldDefinitions}"/>
            <tissuelocator:editExtensions
                dynamicFieldDefinitions="${dynamicFieldDefinitions}"
                layout="boxLayout"
                extendableEntity="${object}"
                propertyPath="object"
                helpUrlBase="/register/info/popup/fieldHelp.action"/>
        </div>
    </s:if>

    <h2><fmt:message key="user.register.institution"/></h2>

    <c:choose>
        <c:when test="${empty TissueLocatorUser || TissueLocatorUser.crossInstitution}">

            <c:choose>
                <c:when test="${isRegister}">
                    <div class="formcol_wide">
                        <tissuelocator:autocompleter fieldName="object.institution" fieldId="institution"
                            searchFieldName="object.institution.name" required="true"
                            autocompleteUrl="json/register/autocompleteInstitution.action"
                            labelKey="user.register.institution.name" noteKey="autocompleter.note"
                            cssStyle="width:27em" onSelect="selectType();"/>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="formcol_wide">
                        <tissuelocator:autocompleter fieldName="object.institution" fieldId="institution"
                            searchFieldName="object.institution.name" required="true"
                            autocompleteUrl="protected/json/autocompleteInstitution.action"
                            labelKey="user.register.institution.name" noteKey="autocompleter.note"
                            cssStyle="width:27em" onSelect="selectType();"/>
                    </div>
                </c:otherwise>
            </c:choose>


            <div class="formcol_wide">
                <s:select name="object.institution.type" id="institutionType" value="%{object.institution.type.id}"
                    list="types" listKey="id" listValue="name"
                    headerKey="" headerValue="%{getText('select.emptyOption')}"
                    labelposition="top" label="%{getText('user.register.institution.type')}" labelSeparator=""
                    required="true" requiredposition="right" cssStyle="width:29em"/>
            </div>
        </c:when>
        <c:otherwise>
            <label><s:property value="%{getText('user.register.institution.name')}"/></label>
            ${TissueLocatorUser.institution.name}
            <s:hidden name="object.institution" value="%{#attr.TissueLocatorUser.institution.id}"/>
            <s:hidden name="object.institution.name" value="%{#attr.TissueLocatorUser.institution.name}"/>
            <s:hidden name="object.institution.type" value="%{#attr.TissueLocatorUser.institution.type.id}"/>
        </c:otherwise>
    </c:choose>

    <div class="clear"></div>

    <div class="formcol_wide">
        <s:textfield name="object.department" id="department" size="30" maxlength="254" cssStyle="width:29em"
                labelposition="top" label="%{getText('user.register.department')}" labelSeparator=""
                required="true" requiredposition="right"/>
    </div>

    <div class="formcol_wide">
        <s:textfield name="object.researchArea" id="researchArea" size="30" maxlength="254" cssStyle="width:29em"
                labelposition="top" label="%{getText('user.register.researchArea')}" labelSeparator=""
                required="true" requiredposition="right"/>
    </div>

    <div class="clear"><br /></div>

    <h2>
        <s:if test="%{object.id == null && !#isUserAdmin}">
            <fmt:message key="user.register.login" />
        </s:if>
        <s:else>
            <fmt:message key="user.update.login" />
        </s:else>
    </h2>

    <div class="formcol">
        <s:textfield name="object.email" id="email1" size="30" maxlength="254" cssStyle="width:20em"
                labelposition="top" label="%{getText('user.register.email')}" labelSeparator=""
                required="true" requiredposition="right"/>

        <div class="note"><ul><fmt:message key="user.register.email.note.list.items"/></ul></div>
    </div>

    <div class="formcol">
        <s:textfield name="object.username" id="email2" size="30" maxlength="254" cssStyle="width:20em"
                labelposition="top" label="%{getText('user.register.email.confirm')}" labelSeparator=""
                required="true" requiredposition="right"/>
    </div>

    <div class="clear"><br /></div>

    <s:if test="%{object.id != null || !#isUserAdmin}">
        <div class="formcol">
            <s:set var="passwordLabel" value="%{'user.register.password.label'}"/>
            <s:if test="%{object.id != null}">
                <s:set var="passwordLabel" value="%{'user.update.password.label'}"/>
            </s:if>
            <s:password name="password" id="password" size="30" maxlength="254" cssStyle="width:20em"
                    labelposition="top" label="%{getText(#passwordLabel)}" labelSeparator=""
                    required="%{object.id == null}" requiredposition="right" autocomplete="off"/>
            <s:fielderror fieldName="object.password" cssClass="fielderror"/>

            <div class="note">
                <ul>
                    <li><fmt:message key="user.register.password.note1"/></li>
                    <li><fmt:message key="user.register.password.note2"/></li>
                </ul>
            </div>
        </div>

        <div class="formcol">
            <s:set var="confirmLabel" value="%{'user.register.password.confirm'}"/>
            <s:if test="%{object.id != null}">
                <s:set var="confirmLabel" value="%{'user.update.password.confirm'}"/>
            </s:if>
            <s:password name="confirmPassword" id="confirmPassword" size="30" maxlength="254" cssStyle="width:20em"
                    labelposition="top" label="%{getText(#confirmLabel)}" labelSeparator=""
                    required="%{object.id == null}" requiredposition="right" autocomplete="off"/>
        </div>

        <div class="clear"><br /></div>
    </s:if>

    <s:if test="%{#isUserAdmin}">
        <h2><fmt:message key="user.update.accessPrivileges"/></h2>
        <s:checkboxlist name="object.groups" id="groups" cssClass="inline"
            list="groups" listKey="id" listValue="name" value="objectGroupIds"
            labelposition="top" label="%{getText('user.register.groups')}" labelSeparator=""
            required="true" requiredposition="right"/>
        <s:if test="%{object.id == null}">
            <s:set var="statusList"
                value="%{@com.fiveamsolutions.nci.commons.data.security.AccountStatus@ACTIVE.allowedTransitions}"/>
        </s:if>
        <s:else>
            <s:set var="statusList"
                value="%{object.previousStatus.allowedTransitions}"/>
        </s:else>
        <s:select name="object.status" id="status"
            onchange="displayStatusTransitionComment()"
            list="%{#statusList}"
            listValue="%{getText('AccountStatus.' + name())}"
            labelposition="top" label="%{getText('user.register.status')}" labelSeparator=""
            required="true" requiredposition="right"/>
        <s:textarea name="object.statusTransitionComment" id="statusTransitionComment"
            rows="10" cols="20"
            cssStyle="width:600px;"
            labelposition="top"
            label="%{getText('user.register.statusTransitionComment')}"
            required="true"
            requiredposition="right"/>
        <s:fielderror cssClass="fielderror">
            <s:param>object.deactivationStatusTransitionCommentValid</s:param>
            <s:param>object.denialStatusTransitionCommentValid</s:param>
        </s:fielderror>
        <div class="clear"><br /></div>
    </s:if>
    <s:else>
        <s:hidden name="object.status" value="ACTIVE"/>
    </s:else>

    <c:if test="${isRegister}">
        <tissuelocator:registerTermsAndConditions/>
    </c:if>

    <div class="btn_bar">
        <s:set var="submitLabel" value="%{'user.register.register'}"/>
        <s:if test="%{object.id != null}">
            <s:set var="submitLabel" value="%{'user.update.save'}"/>
            <s:set var="submitButtonLabel" value="%{'user.update.save.button'}"/>
        </s:if>
        <s:elseif test="%{#isUserAdmin}">
            <s:set var="submitLabel" value="%{'user.create.save'}"/>
            <s:set var="submitButtonLabel" value="%{'user.update.save.button'}"/>
        </s:elseif>
        <s:submit value="%{getText(#submitLabel)}" name="btn_register"
            id="btn_register" cssClass="btn" theme="simple" title="%{getText(#submitButtonLabel)}"/>
        &nbsp;&nbsp;&nbsp;|&nbsp;
        <c:url value="/" var="homeUrl"/>
        <a href="${homeUrl}"><fmt:message key="user.register.cancel"/></a>
    </div>

</s:form>

<script>
    var instArray = [
        <s:iterator value="%{institutions}" status="instIterStatus">
            [<s:property value="%{id}"/>, "<s:property value="%{name}"/>", <s:property value="%{type.id}"/>]
            <s:if test="%{!#instIterStatus.last}"> , </s:if>
        </s:iterator>
    ];

    var selectType = function() {
        var instId = -1;
        var formElements = $('#registerForm').get(0).elements;
        for (var i = 0; i < formElements.length; i++) {
            if (formElements[i].name == 'object.institution') {
                instId = formElements[i].value;
            }
        }

        var typeField = $('#wwgrp_institutionType').get(0);
        if (typeField) {
            if (instId > 0) {
                typeField.style.display = 'none';
            } else {
                typeField.style.display = '';
            }
        }

        var typeSelect = $('#institutionType').get(0);
        if (typeSelect) {
            var typeId = typeSelect.options[typeSelect.selectedIndex].value;
            for (var i = 0; i < instArray.length; i++) {
                if (instArray[i][0] == instId) {
                    typeId = instArray[i][2];
                }
            }
            // if an existing institution is selected, but no type was autoselected,
            // prevent overriding the type by removing the field
            if ((instId != null && instId != '') && (typeId == null || typeId == '')) {
              removeFields(['institutionType']);
            } else {
              for (var i = 0; i < typeSelect.options.length; i++) {
                    if (typeSelect.options[i].value == typeId) {
                        typeSelect.selectedIndex = i;
                    }
                }
            }
        }
    }

    var updateHiddenFields = function() {
        // Fix the case when the user types in the value and clicks away from the field.
        var idField = null;
        var nameField = null;
        var formElements = $('#registerForm').get(0).elements;
        for (var i = 0; i < formElements.length; i++) {
            if (formElements[i].name == 'object.institution') {
                idField = formElements[i];
            } else if (formElements[i].name == 'object.institution.name') {
                nameField = formElements[i];
            }
        }
        var idValue = '';
        for (var i = 0; i < instArray.length; i++) {
            if (nameField.value.toLowerCase() == instArray[i][1].toLowerCase()) {
                nameField.value = instArray[i][1];
                idValue = instArray[i][0];
            }
        }
        idField.value = idValue;
        selectType();

        return confirmIfNeeded();
    }

    function init() {
        selectType();
        displayStatusTransitionComment();
    }

    var confirmIfNeeded = function() {
        var loggedInUserId = '${TissueLocatorUser.id}';
        var currentUserId = '${object.id}';
        var originalUsername = '${TissueLocatorUser.username}';
        var currentUsername = $('#email1').get(0).value;

        if ('' != loggedInUserId && loggedInUserId == currentUserId && currentUsername != originalUsername) {
            return confirm("Changing your username will cause you to be logged out after your profile is saved.  Are you sure you want to continue?");
        }

        return true;
    }

    var displayStatusTransitionComment = function() {
        var status = $('#status').get(0);
        if (status) {
          var selectedStatus = status.options[status.selectedIndex].value.toLowerCase()
            var previousStatus = '${object.previousStatus}'.toLowerCase()
            var denialTextArea = $('#wwgrp_statusTransitionComment').get(0);
            if ((previousStatus == 'pending' || previousStatus == 'active') && selectedStatus == 'inactive') {
                denialTextArea.style.display = '';
            } else {
                denialTextArea.style.display = 'none';
            }
        }
    }

    $(document).ready(function () {
        init();
    });
</script>
</body>
</html>
