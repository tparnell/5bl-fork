<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
    <head>
        <title><fmt:message key="forgotPassword.success.title"/></title>
    </head>
    <body>
        <tissuelocator:messages/>
        <fmt:message key="forgotPassword.success.banner"/></p>
        <a href="#" onclick="window.parent.hidePopWin(true);return false">
            <fmt:message key="forgotPassword.success.returnLinkText"/>
        </a>
    </body>
</html>
