<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
    <head>
        <title><fmt:message key="forgotPassword.title"/></title>
    </head>
    <body>
        <tissuelocator:messages/>
        <div>
            <p><fmt:message key="forgotPassword.banner"/></p>

            <s:form action="forgotPassword/popup/execute" id="forgotPasswordForm" target="_self">
                <div class="formcol">
                    <s:textfield name="username" id="username"  size="30" maxlength="254"
                            labelposition="left" label="%{getText('forgotPassword.username')}"
                            required="true" requiredposition="right" />
                </div>

                <div class="clear"></div>

				<div class="btn_bar">
					<fmt:message key="forgotPassword.submit" var="forgotPasswordSubmit"/>
					<fmt:message key="form.submit.button" var="submitButton"/>
					<input id="forgotPasswordSubmit" class="btn" type="submit" name="submit"
						   value="${forgotPasswordSubmit}" title="${submitButton}" />
					<fmt:message key="forgotPassword.cancel" var="forgotPasswordCancel"/>
					<fmt:message key="form.cancel.button" var="cancelButton"/>	   
					<input id="forgotPasswordCancel" class="btn tiny" type="submit" onclick="window.parent.hidePopWin(true);return false" 
						   name="btn_cancel" value="${forgotPasswordCancel}" title="${cancelButton}"/>						   
				</div>
            </s:form>
        </div>
    </body>
</html>
