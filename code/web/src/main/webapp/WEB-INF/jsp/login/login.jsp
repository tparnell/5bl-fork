<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<page:applyDecorator name="main">
<html>
<head>
    <title><fmt:message key="login.signIn" /></title>
</head>
<body onload="setFocusToFirstControl();">

    <tissuelocator:messages/>

    <c:url value="/register/input.action" var="registerUrl"/>
    <div class="loginfloat">
        <div class="box">

            <h2><fmt:message key="login.newUser"/></h2>
            <p><fmt:message key="login.newUser.text"/></p>
            <h3>
                <a href="${registerUrl}">
                    &raquo; <fmt:message key="login.newUser.link"/>
                </a>
            </h3>

        </div>
    </div>

    <div class="loginfloat" style="border-left:1px solid #eee; padding-left:30px">
        <div class="box">

            <h2><fmt:message key="login.signIn.title"/></h2>

            <form action="j_security_check" method="post" id="loginForm">
                <c:if test="${not empty param.failedLogin}">
                    <div class="messages fielderror">
                        <fmt:message key="errors.password.mismatch"/>
                    </div>
                </c:if>

                <label for="username"><fmt:message key="login.username"/></label>
                <input type="text" name="j_username" id="username" size="15" maxlength="100" style="width:175px" />

                <label for="password"><fmt:message key="login.password"/></label>
                <input type="password" name="j_password" id="password" size="15" maxlength="100" style="width:175px" />

                <br />
                <fmt:message key="login.submit" var="submitLabel"/>
                <fmt:message key="login.submit.button" var="submitButton"/>
                <input type="submit" value="${submitLabel}" name="submitLogin" id="submitLogin" class="btn" title="${submitButton}"/>

                <p class="reminders">
                    <c:url value="/forgotPassword/popup/input.action" var="forgotUrl"/>
                    <a href="javascript: showPopWin('${forgotUrl}', 600, 375, null);">
                        <fmt:message key="login.forgotPassword"/>
                    </a>
                </p>
            </form>
        </div>
    </div>
    <div class="clear"></div>


</body>
</html>
</page:applyDecorator>
