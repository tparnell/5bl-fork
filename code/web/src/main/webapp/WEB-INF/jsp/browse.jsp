<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="login.title" /></title>
</head>
<body onload="setFocusToFirstControl();">

<c:url value="/notYetImplemented.jsp" var="notYetImplementedUrl"/>
<c:url value="/register/input.action" var="registerUrl"/>
<div id="focus">

    <tissuelocator:splashImage showRegisterButton="true"/>

    <div id="focus_right_wrapper">
        <div id="focus_right">

            <!--Sign In-->
            <h2><fmt:message key="login.title"/></h2>


            <script type="text/javascript">
                function startLogin() {
                    $.ajax({
                        url: '<c:url value="/protected/home.action"/>',
                        success: function(){
                            $('#loginForm').get(0).submit();
                        }
                    });
                }
            </script>

            <form action="j_security_check" method="post" id="loginForm" onsubmit="startLogin(); return false;">

                <label for="username"><fmt:message key="login.username"/></label>
                <input type="text" name="j_username" id="username" size="15" maxlength="100" style="width:175px" />

                <label for="password"><fmt:message key="login.password"/></label>
                <input type="password" name="j_password" id="password" size="15" maxlength="100" style="width:175px" />

                <fmt:message key="login.submit" var="submitLabel"/>
                <fmt:message key="login.submit.button" var="submitButton"/>
                <input type="submit" value="${submitLabel}" name="submitLogin" id="submitLogin" class="btn" title="${submitButton}" />

                <p class="reminders">
                    <c:url value="/forgotPassword/popup/input.action" var="forgotUrl"/>
                    <a href="javascript: showPopWin('${forgotUrl}', 600, 375, null);">
                        <fmt:message key="login.forgotPassword"/>
                    </a>
                    |
                    <a href="${registerUrl}">
                        <fmt:message key="login.register"/>
                    </a>
                    <br />
                </p>

            </form>
            <!--/Sign In-->

        </div>
    </div>

    <div class="clear"></div>

</div>

<div id="subfocus">

    <tissuelocator:biospecimenBrowse/>
    <div class="clear"></div>

</div>

</body>
</html>
