<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url value="/admin/institution/list.action" var="listUrl"/>
<html>
<head>
    <title><fmt:message key="institution.title" /></title>
    <script type="text/javascript">
        function toggleMtaDiv(show) {
          if (!$('#mtaContactDiv').is(':visible') && show) {
              toggleVisibility('mtaContactDiv');
          } else if ($('#mtaContactDiv').is(':visible') && !show) {
              toggleVisibility('mtaContactDiv');
          }
        }
    </script>
</head>
<body onload="setFocusToFirstControl();">

<s:set name="pendingStatus" value="%{@com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus@PENDING_REVIEW}" />
<s:set name="approvedStatus" value="%{@com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus@APPROVED}" />
<s:set name="rejectedStatus" value="%{@com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus@REJECTED}" />
<s:set name="outOfDateStatus" value="%{@com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus@OUT_OF_DATE}" />

<div class="topbtns">
    <a href="${listUrl}" class="btn">
        <fmt:message key="btn.backToList"/>
    </a>
</div>
<s:form id="institutionForm" namespace="/admin" action="institution/save.action"
        enctype="multipart/form-data" method="POST">
    <tissuelocator:messages/>

    <s:hidden key="object.id" />

    <h2 class="formtop"><fmt:message key="institution.sectionHeader"/></h2>

    <div class="formcol">
        <s:textfield name="object.name" label="%{getText('institution.name')}" size="30" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" required="true" requiredposition="right" id="name" />
    </div>

    <div class="formcol">
        <s:select key="object.type" value="%{object.type.id}"
            list="types"
            listValue="name"
            listKey="id"
            headerKey="" headerValue="%{getText('select.emptyOption')}"
            labelposition="top" labelSeparator="" label="%{getText('institution.type')}"
            required="true" requiredposition="right" cssStyle="width:20em"/>
    </div>

    <div class="clear"><br /></div>

    <div class="formcol">
        <s:textfield name="object.homepage" size="30" maxlength="254" cssStyle="width:20em" id="homepage"
            labelposition="top" label="%{getText('institution.homepage')}" labelSeparator="" />
    </div>

    <req:isUserInRole role="institutionProfileUrlAdministrator">
        <div class="formcol">
            <s:textfield name="object.institutionProfileUrl" id="institutionProfileUrl"
                size="30" maxlength="254" cssStyle="width:20em"
                labelposition="top" label="%{getText('institution.institutionProfileUrl')}" labelSeparator="" />
        </div>
    </req:isUserInRole>

    <req:isUserInRole role="institutionProfileUrlAdministrator" value="false">
        <c:if test="${!empty object.institutionProfileUrl}">
            <div class="formcol_wide">
                <label class="label"><fmt:message key="institution.institutionProfileUrl"/></label>
                <a href="${object.institutionProfileUrl}" target="_blank" id="institutionProfileLink">
                    ${object.institutionProfileUrl}
                </a>
            </div>
        </c:if>
    </req:isUserInRole>

    <div class="clear"><br /></div>
    <div class="pad5"></div>

    <s:checkbox theme="simple" id="consortiumMember" name="object.consortiumMember"
        label="%{getText('institution.consortiumMember')}" labelposition="left"
        onclick="toggleVisibility('billingInfoDiv');toggleVisibility('consortiumMtaDiv');"/>
    <label class="inline" for="consortiumMember">
        <fmt:message key="institution.consortiumMember"/>
    </label>
    <s:fielderror fieldName="object.consortiumMember" cssClass="fielderror"/>

    <div class="pad5"></div>

    <h2><fmt:message key="institution.contact.info.heading"/></h2>

    <div class="formcol">
        <s:textfield name="object.contact.firstName" label="%{getText('institution.contact.firstName')}"
            size="30" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" id="firstName" />
    </div>

    <div class="formcol">
        <s:textfield name="object.contact.lastName" label="%{getText('institution.contact.lastName')}"
            size="30" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" id="lastName" />
    </div>

    <div class="clear"><br /></div>

    <div class="formcol">
        <s:textfield name="object.contact.email" label="%{getText('institution.contact.email')}"
            size="30" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" id="email" />
    </div>

    <div class="formcol">
        <s:textfield name="object.contact.phone" label="%{getText('institution.contact.phone')}"
            size="50" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" id="phone" />
    </div>

    <div class="formcol">
        <s:textfield name="object.contact.phoneExtension" label="%{getText('institution.contact.phoneExtension')}"
            size="10" maxlength="254" cssStyle="width:6em"
            labelposition="top" labelSeparator="" id="phoneExtension" />
    </div>

    <div class="clear"><br /></div>

    <c:set var="displayState" value="block" />
    <c:if test="${!(object.consortiumMember == 'true')}">
        <c:set var="displayState" value="none" />
    </c:if>

    <div id="billingInfoDiv" style="display: ${displayState};">

        <div class="pad5"></div>

        <h2><fmt:message key="institution.specimenUseRestrictions"/></h2>

        <s:textarea name="object.specimenUseRestrictions" id="restrictions" cols="40" rows="6" cssStyle="width:750px;"
              title="%{getText('institution.specimenUseRestrictions.textarea')}" />

        <div class="pad5"></div>

        <h2><fmt:message key="institution.billing.info.heading"/></h2>

        <s:textfield name="object.billingAddress.line1" id="billingAddressLine1"
            size="30" maxlength="254" cssStyle="width:20em;" required="true" requiredposition="right"
            labelposition="top" label="%{getText('institution.billingAddress.line1')}" labelSeparator=""
            title="%{getText('institution.billingAddress.line1.field')}"/>

        <div class="pad5"></div>

        <s:textfield name="object.billingAddress.line2" id="billingAddressLine2"
            size="30" maxlength="254" cssStyle="width:20em;" theme="simple"
            title="%{getText('institution.billingAddress.line2.field')}"/>

        <div class="pad5"></div>

        <div class="formcol">
            <s:textfield name="object.billingAddress.city" id="billingAddressCity"
                size="30" maxlength="254" cssStyle="width:20em;" required="true" requiredposition="right"
                labelposition="top" label="%{getText('institution.billingAddress.city')}" labelSeparator=""/>
        </div>

        <div class="formcol">
            <s:select name="object.billingAddress.state" id="billingAddressState"
                required="true" requiredposition="right" cssStyle="width:20em;"
                list="%{@com.fiveamsolutions.tissuelocator.data.State@values()}" listValue="%{getText(resourceKey)}"
                headerKey="" headerValue="%{getText('select.emptyOption')}"
                labelposition="top" label="%{getText('institution.billingAddress.state')}" labelSeparator=""/>
        </div>

        <div class="formcol_thin">
            <s:textfield name="object.billingAddress.zip" id="billingAddressZip"
                size="20" maxlength="20" cssStyle="width:15em;" required="true" requiredposition="right"
                labelposition="top" label="%{getText('institution.billingAddress.zip')}" labelSeparator=""/>
        </div>

        <div class="clear"></div>

        <div class="formcol">
            <s:select name="object.billingAddress.country" id="billingAddressCountry"
                required="true" requiredposition="right" cssStyle="width:20em;"
                list="%{@com.fiveamsolutions.tissuelocator.data.Country@values()}" listValue="%{getText('country.' + name())}"
                labelposition="top" label="%{getText('institution.billingAddress.country')}" labelSeparator=""/>
        </div>

        <div class="clear"><br /></div>

        <div class="formcol">
            <s:textfield name="object.billingAddress.phone" id="billingAddressPhone"
                size="50" maxlength="50" cssStyle="width:20em;" required="true" requiredposition="right"
                labelposition="top" label="%{getText('institution.billingAddress.phone')}" labelSeparator=""/>
        </div>
        <div class="formcol">
            <s:textfield name="object.billingAddress.phoneExtension" id="billingAddressPhoneExtension"
                size="10" maxlength="10" cssStyle="width:6em;"
                labelposition="top" label="%{getText('institution.billingAddress.phoneExtension')}" labelSeparator=""/>
        </div>
        <div class="clear"><br /></div>

      <div class="pad5"></div>

        <h2><fmt:message key="institution.dunsEIN.heading"/></h2>

        <div class="formcol">
            <s:select name="object.dunsEINType" id="dunsEINType"
                required="false" requiredposition="right" cssStyle="width:20em;"
                list="%{@com.fiveamsolutions.tissuelocator.data.DunsEin@values()}"
                listValue="%{getText(resourceKey)}"
                headerKey="" headerValue="%{getText('select.emptyOption')}"
                labelposition="top" label="%{getText('institution.dunsEIN.type')}" labelSeparator="">
            </s:select>
        </div>

        <div class="formcol">
            <s:textfield name="object.dunsEINNumber" label="%{getText('institution.dunsEIN.number')}"
                size="30" maxlength="9" cssStyle="width:20em"
                labelposition="top" labelSeparator="" id="dunsEINNumber" />
        </div>

        <div class="clear"><br /></div>

        <s:fielderror fieldName="object.dunsEINValid" cssClass="fielderror"/>

        <div class="pad5"></div>

        <h2><fmt:message key="institution.paymentInstructions.heading"/></h2>

        <div id="information">
            <label for="paymentInstructions"><fmt:message key="institution.paymentInstructions.label" /></label>

            <s:textarea name="object.paymentInstructions" id="paymentInstructions" cols="40" rows="6" cssStyle="width:750px;"
                labelposition="top" label="%{getText('institution.paymentInstructions')}" labelSeparator="" theme="simple"/>

            <div class="note"></div>
        </div>

        <div class="clear"></div>

    </div>

    <req:isUserInRole role="institutionMtaAdmin">

            <div class="pad5"></div>

            <h2><fmt:message key="institution.mtaAdministration"/></h2>

            <div id="mta_admin">

                <br />

                <req:isUserInRole role="mtaAdministrator">
                    <div class="box uploadbox">
                        <h3><fmt:message key="institution.uploadSignedMta"/></h3>
                        <div class="line"></div>

                        <s:file name="document" id="document" size="30" cssStyle="width:20em"
                            required="true" requiredposition="right"
                            label="%{getText('signedMta.document')}" labelposition="top" labelSeparator=""/>
                        <div class="note"><fmt:message key="mta.info.upload.note"/></div>

                        <div id="consortiumMtaDiv" style="display: ${displayState};">
                            <c:set var="sendingChecked" value=""/>
                            <c:set var="receivingChecked" value="checked"/>
                            <c:set var="displayMtaContact" value="block" />
                            <s:if test="%{sendingMta}">
                                <c:set var="sendingChecked" value="checked"/>
                                <c:set var="receivingChecked" value=""/>
                                <c:set var="displayMtaContact" value="none" />
                            </s:if>
                            <div id="mta_type_radios" class="wwgrp">
                                <label for="mta_type1" class="label" style="font-style:normal;">
                                    <fmt:message key="institution.sendingMta"/>
                                    <span class="reqd"><fmt:message key="required.indicator"/></span>
                                </label>
                                <input type="radio" name="sendingMta" id="mta_type1" ${sendingChecked} value="true"
                                     onclick="toggleMtaDiv(false)"/>
                                <label for="mta_type1" class="inline"><fmt:message key="institution.sendingMta.yes"/></label>
                                <br />
                                <input type="radio" name="sendingMta" id="mta_type2" ${receivingChecked} value="false"
                                       onclick="toggleMtaDiv(true)"/>
                                <label for="mta_type2" class="inline"><fmt:message key="institution.sendingMta.no"/></label>
                            </div>
                        </div>

                    </div>
                </req:isUserInRole>

                <c:set var="downloadBoxStyle" value=""/>
                <req:isUserInRole role="mtaAdministrator" value="false">
                    <c:set var="downloadBoxStyle" value="float:none"/>
                </req:isUserInRole>
                <div class="box downloadbox" style="${downloadBoxStyle}">
                    <h3><fmt:message key="institution.downloadSignedMta"/></h3>
                    <div class="line"></div>

                    <fmt:message key="institution.downloadSignedMta.list" var="mtaListTitle"/>
                    <fmt:message key="institution.downloadSignedMta.summary" var="mtaListSummary"/>
                    <fmt:message key="institution.downloadSignedMta.version" var="versionTitle"/>
                    <fmt:message key="institution.downloadSignedMta.type" var="typeTitle"/>
                    <fmt:message key="institution.downloadSignedMta.uploaded" var="uploadedTitle"/>
                    <fmt:message key="institution.downloadSignedMta.status" var="statusTitle"/>
                    <fmt:message key="column.action" var="actionTitle"/>
                    <c:url var="currentImageUrl" value="/images/ico_success.gif"/>
                    <fmt:message key="mta.info.current" var="currentImageAlt"/>
                    <table style="width:552px;" class="data" title="${mtaListTitle}" summary="${mtaListSummary}">
                        <tr>
                            <th scope="col"><div class="nosort">${versionTitle}</div></th>
                            <c:if test="${object.consortiumMember == 'true'}">
                                <th scope="col"><div class="nosort">${typeTitle}</div></th>
                            </c:if>
                            <th scope="col"><div class="nosort">${uploadedTitle}</div></th>
                            <th scope="col"><div class="nosort">${statusTitle}</div></th>
                            <th scope="col" class="action"><div class="nosort">${actionTitle}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></th>
                        </tr>
                    </table>
                    <div class="scrolling_downloads">
                        <table class="data" title="${mtaListTitle}" summary="${mtaListSummary}" id="signedMtaList">
                            <s:iterator value="%{signedMtas}" status="signedMtaStatus" var="savedSignedMta">
                                <c:set var="rowClass" value=""/>
                                <c:set var="cellClass" value=""/>
                                <s:if test="%{#signedMtaStatus.index % 2 == 0}">
                                    <c:set var="rowClass" value="odd"/>
                                </s:if>

                                <s:if test="%{#approvedStatus == #savedSignedMta.status}">
                                    <c:set var="rowClass" value="${rowClass} current"/>
                                    <c:set var="cellClass" value="hilite current"/>
                                </s:if>
                                <s:elseif test="%{#rejectedStatus == #savedSignedMta.status}">
                                    <c:set var="cellClass" value="none"/>
                                </s:elseif>
                                <s:elseif test="%{#outOfDateStatus == #savedSignedMta.status}">
                                    <c:set var="cellClass" value="outdated"/>
                                </s:elseif>
                                <s:elseif test="%{#pendingStatus == #savedSignedMta.status}">
                                    <c:set var="cellClass" value="pending"/>
                                </s:elseif>

                                <fmt:message key="default.date.format" var="dateFormat"/>
                                <tr class="${rowClass}">
                                    <td title="${versionTitle}" class="${cellClass}">
                                        <fmt:formatDate pattern="${dateFormat}" value="${savedSignedMta.originalMta.version}"/>
                                    </td>
                                    <c:if test="${object.consortiumMember == 'true'}">
                                        <td title="${typeTitle}" class="${cellClass}">
                                            <s:if test="%{#savedSignedMta.sendingInstitution != null}">
                                                <fmt:message key="institution.sendingMta.yes"/>
                                            </s:if>
                                            <s:else>
                                                <fmt:message key="institution.sendingMta.no"/>
                                            </s:else>
                                        </td>
                                    </c:if>
                                    <td title="${uploadedTitle}" class="${cellClass}">
                                        <s:date name="%{#savedSignedMta.uploadTime}"
                                            format="%{getText('default.date.format')}" />
                                    </td>
                                    <td title="${statusTitle}" class="${cellClass}">
                                        <s:if test="%{#approvedStatus == #savedSignedMta.status}">
                                            <img src="${currentImageUrl}" class="active_icon"
                                                alt="${currentImageAlt}" title="${currentImageAlt}"/>
                                        </s:if>
                                        <s:property value="%{getText(#savedSignedMta.status.resourceKey)}"/>
                                    </td>
                                    <td class="action ${cellClass}">
                                        <c:url value="/protected/downloadFile.action" var="signedMtaUrl">
                                            <c:param name="file.id"><s:property value="%{#savedSignedMta.document.lob.id}"/></c:param>
                                            <c:param name="fileName"><s:property value="%{#savedSignedMta.document.name}"/></c:param>
                                            <c:param name="contentType"><s:property value="%{#savedSignedMta.document.contentType}"/></c:param>
                                        </c:url>
                                        <fmt:message key="institution.downloadSignedMta.downloadAlt" var="downloadAlt"/>
                                        <fmt:message key="institution.downloadSignedMta.download" var="downloadLink"/>
                                        <a class="btn_fw_action_col" href="${signedMtaUrl}" title="${downloadAlt}">
                                            ${downloadLink}
                                        </a>
                                    </td>
                                </tr>

                            </s:iterator>
                        </table>
                    </div>
                </div>

                <div id="mtaContactDiv" style="display: ${displayMtaContact};">
                  <div class="line"></div>
                  <h3><fmt:message key="institution.mtaContact"/></h3>
                  <req:isUserInRole role="mtaAdministrator">
                      <div class="note"><fmt:message key="institution.mtaContact.note"/></div>
                      <tissuelocator:editPerson copyEnabled="false" propertyPrefix="object.mtaContact"
                              resourcePrefix="institution.mtaContact"  idPrefix="mtaContact"
                              countryValue="${object.mtaContact.address.country}"
                              stateValue="${object.mtaContact.address.state}"
                              showInstitution="false" showFax="true"/>
                      <s:hidden name="object.mtaContact.organization" value="%{object.id}"/>
                  </req:isUserInRole>
                  <req:isUserInRole role="mtaAdministrator" value="false">
                      <c:choose>
                          <c:when test="${empty object.mtaContact}">
                              <div class="note"><fmt:message key="institution.mtaContact.empty"/></div>
                          </c:when>
                          <c:otherwise>
                              <tissuelocator:viewPerson showInstitution="false" person="${object.mtaContact}"/>
                          </c:otherwise>
                      </c:choose>
                  </req:isUserInRole>
                </div>
            </div>

    </req:isUserInRole>

    <div class="clear"><br /></div>

    <div class="btn_bar">
        <s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn" theme="simple" title="%{getText('btn.save.button')}"/>
        &nbsp;&nbsp;&nbsp;|&nbsp;
        <a href="${listUrl}"><fmt:message key="btn.cancel"/></a>
    </div>
</s:form>
</body>
</html>
