<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="institution.title" /></title>
</head>
<body>
    <req:isUserInRole role="crossinstitution">
    <div class="topbtns">
        <a href="<c:url value="/admin/institution/input.action"/>" class="btn">
            <fmt:message key="institution.add"/>
        </a>
    </div>
    </req:isUserInRole>

    <tissuelocator:messages/>

    <s:form id="filterForm" namespace="/admin" action="institution/filter.action">
        <!--Filters-->
        <div class="roundbox_gray_wrapper">
            <div class="roundbox_gray">
                <div class="float_left">
                </div>

                <div class="float_right">
                    <label class="inline" for="consortiumMembers">
                    	<strong><fmt:message key="institution.list.consortiumMember"/></strong>
                    </label>
                    <s:checkbox theme="simple" name="consortiumMembersOnly" onclick="submit()" id="consortiumMembers"/>
                    <label class="inline" for="name">
                    	<strong><fmt:message key="institution.list.filter"/></strong>
                    </label>
                    <s:textfield name="object.name" label="%{getText('institution.name')}" theme="simple" id="name" />
                    <s:submit value="%{getText('btn.search')}" name="btn_search" cssClass="btn" theme="simple" title="%{getText('btn.search.button')}"/>
                </div>

                <div class="clear"></div>
            </div>
        </div>

        <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
        <fmt:message key="default.datetime.format" var="datetimeFormat"/>
        <display:table name="objects" requestURI="/admin/institution/list.action"
                uid="institution" pagesize="${tablePageSize}" sort="external" export="true">
                <tissuelocator:displaytagProperties/>
                <display:setProperty name="export.csv.filename" value="institutions.csv"/>

            <display:column titleKey="institution.name" class="title" sortProperty="NAME" sortable="true" media="html">
                <c:choose>
                    <c:when test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == institution.id}">
                        <c:url var="editUrl" value="/admin/institution/input.action">
                            <c:param name="object.id" value="${institution.id}"/>
                        </c:url>
                        <a href="${editUrl}">${institution.name}</a>
                    </c:when>
                    <c:otherwise>
                        ${institution.name}
                    </c:otherwise>
                </c:choose>
            </display:column>
            <display:column titleKey="institution.name" class="title" sortProperty="NAME" sortable="true" media="csv">
                ${institution.name}
            </display:column>

            <display:column titleKey="institution.homepage"
                sortProperty="HOMEPAGE" sortable="true" media="html">
                <a href="${institution.homepage}" target="_blank">${institution.homepage}</a>
            </display:column>
            <display:column titleKey="institution.homepage" property="homepage" sortProperty="HOMEPAGE" sortable="true"
                media="csv"/>
            <display:column titleKey="institution.type" property="type.name" sortProperty="TYPE" sortable="true"/>
            <display:column titleKey="column.action" media="html" headerClass="action" class="action">
                <c:if test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == institution.id}">
                    <c:url var="editUrl" value="/admin/institution/input.action">
                        <c:param name="object.id" value="${institution.id}"/>
                    </c:url>
                    <a href="${editUrl}" class="btn_fw_action_col"><fmt:message key="btn.edit" /></a>
                </c:if>
            </display:column>
        </display:table>
    </s:form>
</body>
</html>
