<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>
<c:set var="extraBodyTags" scope="request">style="margin:5px 15px"</c:set>
<html>
    <head>
        <title>
            <fmt:message key="specimen.search.title"/>
        </title>
    </head>
    <body onload="setFocusToFirstControl();">
        <s:form id="filterForm" action="/admin/shipment/popup/specimenSearch/filter.action">
            <tissuelocator:specimenTabbedSearchView>
                <s:hidden name="object.externalIdAssigner" value="%{#attr.TissueLocatorUser.institution.id}" />
                <s:hidden name="order" value="%{order.id}" />
                <div id="table-format" class="roundbox_gray_wrapper">
                    <div class="roundbox_gray">
                        <tissuelocator:specimenSearchForm searchParams="${SpecimenSearchPopupAction_params}" refreshAction="/admin/shipment/popup/specimenSearch/refreshList.action"/>
                    </div>
                </div>
                <c:if test="${!empty SpecimenSearchPopupAction_params}">
                    <h2 class="noline"><fmt:message key="specimen.search.results.title"/></h2>
                    <tissuelocator:specimenGroupTable showEditButtons="false" showCheckboxes="false"
                        formId="filterForm" checkboxProperty="selection"
                        listAction="/admin/shipment/popup/specimenSearch/filter.action"
                        viewAction="/admin/shipment/popup/specimen/view.action" exportEnabled="false" showStatus="false" showFee="true" />
                    <div class="clear"><br /></div>
                    <div class="btn_bar">
                        <c:url var="addToOrderUrl" value="/admin/shipment/popup/addAggregatesToOrder.action" />
                        <s:submit value="%{getText('specimen.addToOrder')}"
                            name="btn_addtoorder" id="btn_addtoorder" cssClass="btn" theme="simple"
                            onclick="$('#filterForm').get(0).action = '%{#attr.addToOrderUrl}'"
                            title="%{getText('form.submit.button')}"/>
                        <fmt:message key="form.cancel.button" var="cancelButton"/>
                        <input type="submit" value="Cancel" name="btn_cancel" id="btn_cancel" class="btn tiny"
                            onclick="javascript:$('#filterForm').get(0).target='_self'; window.parent.hidePopWin()";
                            title="${cancelButton}"/>
                    </div>
                </c:if>
            </tissuelocator:specimenTabbedSearchView>
        </s:form>
    </body>
</html>

