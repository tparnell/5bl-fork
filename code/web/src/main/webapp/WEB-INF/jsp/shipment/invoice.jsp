<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<fmt:message key="default.date.format" var="dateFormat"/>
<html>
<head>
    <title><fmt:message key="invoice.title" /></title>
</head>
<body>
    <div id="invoice">
        <div class="button noprint ">
            <fmt:message key="invoice.print" var="invoicePrint"/>
            <fmt:message key="invoice.print.button" var="invoicePrintButton"/>
            <form><input type="button" value="${invoicePrint}" title="${invoicePrintButton}"
                   onclick="window.print()"></form>
        </div>

        <h1><fmt:message key="invoice.title" /></h1>

        <c:url value="/images/logo_invoice.gif" var="invoiceLogoUrl"/>
        <fmt:message key="header.alt" var="invoiceLogoAlt" />
        <div class="logo"><img src="${invoiceLogoUrl}" alt="${invoiceLogoAlt}" title="${invoiceLogoAlt}" /></div>

        <table class="fullwidth">

            <tr>
                <th><fmt:message key="shipment.request.id" /></th>
                <th><fmt:message key="shipment.id" /></th>
                <th><fmt:message key="invoice.request.date" /></th>
                <th><fmt:message key="invoice.processed.by" /></th>
            </tr>
            <tr>
                <td>${object.request.id}</td>
                <td>${object.id}</td>
                <td><fmt:formatDate value="${object.request.requestedDate}" pattern="${dateFormat}"/></td>
                <td>
                    <c:if test="${not empty object.processingUser}">
                        ${object.processingUser.firstName} ${object.processingUser.lastName}<br />${object.processingUser.email}
                    </c:if>
                </td>
            </tr>
        </table>

        <div class="clear"></div>

        <table align="center" width="100%">
            <tr>
                <td style="width: 200px;">
                    <table style="width: 100%;">
                        <tr>
                            <th><fmt:message key="invoice.requested.by" /></th>
                        </tr>
                        <tr>
                            <td>
                                ${object.request.investigator.firstName} ${object.request.investigator.lastName}<br />
                                ${object.request.investigator.organization.name}<br />
                                ${object.request.investigator.address.line1}<br />
                                <c:if test="${!empty object.request.investigator.address.line2}">
                                    ${object.request.investigator.address.line2}<br />
                                </c:if>
                                ${object.request.investigator.address.city}, <fmt:message key="${object.request.investigator.address.state.resourceKey}" /> ${object.request.investigator.address.zip}<br />
                                ${object.request.investigator.address.phone}
                                <tissuelocator:phoneExtensionDisplay extension="${object.request.investigator.address.phoneExtension}"/>
                                ${object.request.study.irbName}<br />
                                <c:if test="${!empty object.request.study.irbApprovalNumber}">
                                    <fmt:message key="invoice.approvalNumber"/>: ${object.request.study.irbApprovalNumber}<br/>
                                </c:if>
                                <c:if test="${!empty object.request.study.irbApprovalLetter}">
                                    <fmt:message key="invoice.irbApprovalLetter.onFile"/><br/>
                                </c:if>
                                <c:if test="${!empty object.request.study.irbExemptionLetter}">
                                    <fmt:message key="invoice.irbExemptionLetter.onFile"/><br/>
                                </c:if>
                            </td>

                        </tr>
                    </table>
                </td>
                <td style="width: 200px;">
                    <table style="width: 100%;">
                        <tr>
                            <th><fmt:message key="invoice.ship.to" /></th>
                        </tr>
                        <tr>
                            <td>
                                ${object.recipient.firstName} ${object.recipient.lastName}<br />
                                ${object.recipient.address.line1}<br />
                                <c:if test="${!empty object.recipient.address.line2}">
                                    ${object.recipient.address.line2}<br />
                                </c:if>
                                ${object.recipient.address.city}, <fmt:message key="${object.recipient.address.state.resourceKey}" /> ${object.recipient.address.zip}<br />
                                ${object.recipient.address.phone}
                                <tissuelocator:phoneExtensionDisplay extension="${object.recipient.address.phoneExtension}"/>
                                <br />
                                ${object.recipient.email}<br />
                            </td>
                        </tr>
                    </table>
                </td>
                <c:if test="${not empty object.billingRecipient}">
                    <td style="width: 200px;">
                    <table style="width: 100%;">
                        <tr>
                            <th><fmt:message key="invoice.billingAddress" /></th>
                        </tr>
                        <tr>
                            <td>
                                ${object.billingRecipient.firstName} ${object.billingRecipient.lastName}<br />
                                ${object.billingRecipient.address.line1}<br />
                                <c:if test="${!empty object.billingRecipient.address.line2}">
                                    ${object.billingRecipient.address.line2}<br />
                                </c:if>
                                ${object.billingRecipient.address.city}, <fmt:message key="${object.billingRecipient.address.state.resourceKey}" /> ${object.billingRecipient.address.zip}<br />
                                ${object.billingRecipient.address.phone}
                                <tissuelocator:phoneExtensionDisplay extension="${object.billingRecipient.address.phoneExtension}"/>
                                <br />
                                ${object.billingRecipient.email}<br />
                            </td>
                        </tr>
                    </table>
                </td>
                </c:if>
                <td style="width:200px;">
                    <table style="width: 100%;">
                        <tr>
                            <th><fmt:message key="invoice.ship.via" /></th>
                        </tr>
                        <tr>
                            <td>
                                <c:choose>
                                    <c:when test="${!empty object.shippingMethod}"><fmt:message key="${object.shippingMethod.resourceKey}" /></c:when>
                                    <c:otherwise><fmt:message key="invoice.unkownMethod"/></c:otherwise>
                                </c:choose>
                                <br />
                                <fmt:message key="invoice.tracking" />: ${object.trackingNumber}<br />
                                <fmt:message key="invoice.shipping.instructions" />: ${object.shippingInstructions}<br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <div class="clear"></div>

        <s:if test="%{displayAggregateSearchResults}">
            <s:set var="specificLineItems" value="%{specificLineItemList}"/>
            <s:set var="generalLineItems" value="%{generalLineItemList}"/>

            <c:if test="${not empty specificLineItems || not empty generalLineItems}">
                <table class="fullwidth" id="aggregateLineItemTable">
                    <c:if test="${not empty specificLineItems}">
                        <tissuelocator:invoiceAggregateLineItemTable orderLineItems="${specificLineItems}"
                            titleKey="aggregateCartTable.specificCart.title" prefix="specific"/>
                    </c:if>

                    <c:if test="${not empty generalLineItems}">
                          <tissuelocator:invoiceAggregateLineItemTable orderLineItems="${generalLineItems}"
                            titleKey="aggregateCartTable.generalCart.title" prefix="general"/>
                    </c:if>
                </table>
            </c:if>
        </s:if>
        <s:else>
            <display:table name="object.lineItems" uid="lineItem" class="fullwidth">
                <fmt:message key="specimenRequest.lineItems" var="lineItemsTitle"/>
                <display:column title="<div>${lineItemsTitle}</div>" sortProperty="specimen.id" class="carttitle">
                    ${lineItem.specimen.id} <req:isUserInRole role="externalIdViewer">(External Id: ${lineItem.specimen.externalId})</req:isUserInRole><br />
                    <tissuelocator:codeDisplay code="${lineItem.specimen.pathologicalCharacteristic}"/>
                </display:column>

                <fmt:message key="specimenRequest.amountRequested" var="amountRequestedTitle"/>
                <display:column title="<div>${amountRequestedTitle}</div>">
                    ${lineItem.quantity}
                    <fmt:message key="${lineItem.quantityUnits.resourceKey}"/>
                </display:column>

                <req:isUserInRole role="priceViewer">
                    <fmt:message key="specimenRequest.costRecoveryFee" var="costRecoveryFeeTitle"/>
                    <display:column title="<div>${costRecoveryFeeTitle}</div>">
                        <fmt:formatNumber value="${lineItem.finalPrice}" type="currency"/>
                        <c:if test="${not empty lineItem.finalPrice}">
                            <c:set var="finalPriceTotal" value="${finalPriceTotal + lineItem.finalPrice}" scope="request"/>
                        </c:if>
                    </display:column>
                </req:isUserInRole>

            </display:table>
        </s:else>

        <req:isUserInRole role="priceViewer">
            <h2><fmt:message key="invoice.payment" /></h2>

            <table class="fullwidth">
                <tr>
                    <th><fmt:message key="invoice.payment.address" /></th>
                    <th><fmt:message key="invoice.payment.instructions" /></th>
                    <th><fmt:message key="invoice.notes" /></th>
                    <th><fmt:message key="invoice.amount" /></th>
                </tr>
                <tr>
                    <td nowrap="nowrap">
                        ${object.sendingInstitution.name}<br />
                        ${object.sendingInstitution.billingAddress.line1}<br />
                        <c:if test="${!empty object.sendingInstitution.billingAddress.line2}">
                            ${object.sendingInstitution.billingAddress.line2}<br />
                        </c:if>
                        ${object.sendingInstitution.billingAddress.city}, <fmt:message key="${object.sendingInstitution.billingAddress.state.resourceKey}" /> ${object.sendingInstitution.billingAddress.zip}<br />
                        Phone:  ${object.sendingInstitution.billingAddress.phone}
                        <c:if test="${not empty object.sendingInstitution.billingAddress.phoneExtension}">
                            &nbsp;<fmt:message key="specimenRequest.investigator.address.phoneExtension"/>&nbsp;${object.sendingInstitution.billingAddress.phoneExtension}
                        </c:if><br />
                        <c:if test="${!empty object.sendingInstitution.dunsEINType}">
                            <s:property value="%{getText(object.sendingInstitution.dunsEINType.resourceKey)}"/>
                        </c:if>
                        <c:if test="${!empty object.sendingInstitution.dunsEINNumber}">
                            ${object.sendingInstitution.dunsEINNumber}<br />
                        </c:if>
                    </td>
                    <td>${object.sendingInstitution.paymentInstructions}</td>
                    <td>${object.notes}</td>
                    <td class="alignright">
                        <table>
                            <tr>
                                <td class="alignright" style="border: 0;">Biospecimen Total:</td>
                                <td class="alignright" style="border: 0;"><fmt:formatNumber value="${finalPriceTotal}" type="currency"/></td>
                            </tr>
                            <tr>
                                <td class="alignright" style="border: 0;">Handling Fee:</td>
                                <td class="alignright" style="border: 0;"><fmt:formatNumber value="${object.fees}" type="currency"/></td>
                            </tr>
                            <tr>
                                <td class="alignright" style="border: 0;">Shipping Cost:</td>
                                <td class="alignright" style="border: 0;"><fmt:formatNumber value="${object.shippingPrice}" type="currency"/></td>
                            </tr>
                            <tr>
                                <td class="alignright"><b>Order Total:</b></td>
                                <td class="alignright"><b><fmt:formatNumber value="${finalPriceTotal + object.fees + object.shippingPrice}" type="currency"/></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </req:isUserInRole>

        <s:if test="%{displayUsageRestrictions}">
            <h2><fmt:message key="invoice.specimenUseRestrictions" /></h2>
            <tissuelocator:specimenUseRestrictions institution="${object.sendingInstitution}"
                emptyResourceKey="invoice.noContent"/>
        </s:if>

        <req:isUserInRole role="priceViewer" value="false">
            <h2><fmt:message key="invoice.notes" />:</h2>
            <c:choose>
                <c:when test="${!empty object.notes}">
                    <p>${object.notes}</p>
                </c:when>
                <c:otherwise>
                    <fmt:message key="invoice.noContent" />
                </c:otherwise>
            </c:choose>
        </req:isUserInRole>

        <h2>Important:</h2>

        <fmt:message key="tissuelocator.baseUrl" var="baseurl" />
        <p><fmt:message key="invoice.footer"><fmt:param value="${baseurl}" /></fmt:message></p>

    </div>

</body>
</html>
