<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<fmt:message key="ajax.loading.title" var="loadingText"/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>${loadingText}</title>
    </head>
    <body>
        <c:url value="/images/indicator.gif" var="loadingImageUrl"/>
        <img src="${loadingImageUrl}" alt="${loadingText}" />
    </body>
</html>