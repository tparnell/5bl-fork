/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.data.LobHolder;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;

/**
 * @author ddasgupta
 *
 */
public class TissueLocatorFileHelper {

    private static final Logger LOG = Logger.getLogger(TissueLocatorFileHelper.class);

    /**
     * get the bytes in a file.
     * @param file the file to read
     * @return the bytes in the file
     */
    public static byte[] getFileContents(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            int fileLength = (int) file.length();
            byte[] fileContents = new byte[fileLength];
            fis.read(fileContents, 0, fileLength);
            fis.close();
            return fileContents;
        } catch (IOException e) {
            LOG.error("error reading file: " + file.getPath(), e);
        }
        return new byte[0];
    }

    /**
     * Get the file to use from the default value or a file upload.
     * @param upload the uploaded file
     * @param uploadName the uploaded file name
     * @param uploadContentType the uploaded file content type
     * @param defaultFile the default file
     * @param lobHolderService the lob holder service
     * @return the resume file to use
     */
    public static TissueLocatorFile getFile(File upload, String uploadName, String uploadContentType,
            TissueLocatorFile defaultFile, LobHolderServiceLocal lobHolderService) {
        if (upload != null) {
            byte[] fileContents = getFileContents(upload);
            return new TissueLocatorFile(fileContents, uploadName, uploadContentType);
        } else if (defaultFile != null && defaultFile.getLob().getId() != null
                && LobHolder.class.equals(defaultFile.getLob().getClass())) {
            //Load the lob data only if the lob currently in the request is not a hibernate proxy object
            //that has been saved before.
            LobHolder lob = lobHolderService.getLob(defaultFile.getLob().getId());
            return new TissueLocatorFile(lob.getData(), defaultFile.getName(), defaultFile.getContentType());
        }
        return null;
    }
}
