/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.convention;

import java.util.HashSet;
import java.util.Set;

import org.apache.struts2.convention.PackageBasedActionConfigBuilder;

import com.fiveamsolutions.tissuelocator.web.struts2.action.FileDownloadAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.SavedSpecimenSearchAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.mta.MtaDownloadAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.mta.MtaReviewAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.mta.SignedMaterialTransferAgreementAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.report.NewUserReportAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.report.RequestReviewReportAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.report.SpecimensByConditionReportAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.report.SpecimensByInstitutionReportAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.report.UsageReportAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.support.QuestionAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.support.QuestionAdministrationAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.support.QuestionResponseAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.support.SupportLetterRequestAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.support.SupportLetterRequestAdministrationAction;
import com.opensymphony.xwork2.ObjectFactory;
import com.opensymphony.xwork2.config.Configuration;
import com.opensymphony.xwork2.inject.Container;
import com.opensymphony.xwork2.inject.Inject;

/**
 * @author ddasgupta
 *
 */
public class TissueLocatorActionConfigBuilder extends PackageBasedActionConfigBuilder {

    /**
     * Builds the config builder.
     * @param configuration the config
     * @param container the container
     * @param objectFactory the factory
     * @param redirectToSlash redirect to slash?
     * @param defaultParentPackage the default parent package.
     */
    @Inject
    public TissueLocatorActionConfigBuilder(Configuration configuration, Container container,
            ObjectFactory objectFactory,
            @Inject("struts.convention.redirect.to.slash") String redirectToSlash,
            @Inject("struts.convention.default.parent.package") String defaultParentPackage) {
        super(configuration, container, objectFactory, redirectToSlash, defaultParentPackage);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("rawtypes")
    protected Set<Class> findActions() {
        Set<Class> classes = new HashSet<Class>();
        classes.add(SignedMaterialTransferAgreementAction.class);
        classes.add(MtaReviewAction.class);
        classes.add(UsageReportAction.class);
        classes.add(RequestReviewReportAction.class);
        classes.add(NewUserReportAction.class);
        classes.add(SpecimensByInstitutionReportAction.class);
        classes.add(SpecimensByConditionReportAction.class);
        classes.add(SavedSpecimenSearchAction.class);
        classes.add(MtaDownloadAction.class);
        classes.add(FileDownloadAction.class);
        classes.add(SupportLetterRequestAction.class);
        classes.add(SupportLetterRequestAdministrationAction.class);
        classes.add(QuestionAction.class);
        classes.add(QuestionAdministrationAction.class);
        classes.add(QuestionResponseAction.class);
        return classes;
    }
}
