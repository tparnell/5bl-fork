/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiSectionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.google.inject.Inject;

/**
 * @author jstephens
 */
@SuppressWarnings({ "PMD.SingularField", "PMD.UnusedPrivateField" })
public class SpecimenDetailsPanelAction extends SpecimenAction {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SpecimenDetailsPanelAction.class);

    /**
     * default constructor.
     */
    public SpecimenDetailsPanelAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * The constructor.
     * @param specimenSearchService the search service.
     * @param userService the user service
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     * @param uiSectionService ui section service.
     */
    @Inject
    public SpecimenDetailsPanelAction(SpecimenSearchServiceLocal specimenSearchService,
            TissueLocatorUserServiceLocal userService, ApplicationSettingServiceLocal appSettingService, 
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService, UiSectionServiceLocal uiSectionService) {
        super(specimenSearchService, userService, appSettingService, uiDynamicFieldCategoryService, uiSectionService);
    }

    /**
     * Returns the label for column 1.
     *
     * @return the label for column 1
     */
    public String getColumn1Label() {
        return getAppSettingService().getSpecimenDetailsPanelColumn1Label();
    }

    /**
     * Returns the value for column 1.
     *
     * @return the value for column 1
     */
    public Object getColumn1Value() {
        return getProperty(getAppSettingService().getSpecimenDetailsPanelColumn1Value());
    }

    /**
     * Returns the label for column 2.
     *
     * @return the label for column 2
     */
    public String getColumn2Label() {
        return getAppSettingService().getSpecimenDetailsPanelColumn2Label();
    }

    /**
     * Returns the value for column 2.
     *
     * @return the value for column 2
     */
    public Object getColumn2Value() {
        return getProperty(getAppSettingService().getSpecimenDetailsPanelColumn2Value());
    }

    /**
     * Returns the label for column 3.
     *
     * @return the label for column 3
     */
    public String getColumn3Label() {
        return getAppSettingService().getSpecimenDetailsPanelColumn3Label();
    }

    /**
     * Returns the value for column 3.
     *
     * @return the value for column 3
     */
    public Object getColumn3Value() {
        return getProperty(getAppSettingService().getSpecimenDetailsPanelColumn3Value());
    }

    /**
     * Returns the label for column 4.
     *
     * @return the label for column 4
     */
    public String getColumn4Label() {
        return getAppSettingService().getSpecimenDetailsPanelColumn4Label();
    }

    /**
     * Returns the value for column 4.
     *
     * @return the value for column 4
     */
    public Object getColumn4Value() {
        return getProperty(getAppSettingService().getSpecimenDetailsPanelColumn4Value());
    }

    private Object getProperty(String columnValue) {
        Object property = null;
        try {
            property = PropertyUtils.getProperty(getObject(), columnValue);
        } catch (Exception e) {
            LOG.error(e);
        }
        return property;
    }

}
