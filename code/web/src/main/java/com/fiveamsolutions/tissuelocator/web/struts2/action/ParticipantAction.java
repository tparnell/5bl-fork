/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.displaytag.properties.SortOrderEnum;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.ParticipantSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSortCriterion;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author smiller
 *
 */
public class ParticipantAction
    extends AbstractPersistentObjectAction<Participant, ParticipantSortCriterion> {

    private static final long serialVersionUID = 1L;
    private static final String SPECIMEN_RETURNED_PARAM = "specimenReturned";
    private PaginatedList<Specimen> specimens;
    private Specimen exampleSpecimen = new Specimen();

    /**
     * Default constructor.
     */
    public ParticipantAction() {
        setObject(new Participant());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String input() {
        if (getObject().getExternalIdAssigner() != null) {
            setInstitutionSelection(getObject().getExternalIdAssigner().getName());
        }

        if (getObject().getId() != null) {
            locateParticipantSpecimens();
        }
        
        if (StringUtils.isNotBlank(ServletActionContext.getRequest().getParameter(SPECIMEN_RETURNED_PARAM))) {
            addActionMessage(getText("specimen.returnToSourceInstitution.success"));
        }
        return super.input();
    }

    private void locateParticipantSpecimens() {
        SpecimenServiceLocal specimenService = TissueLocatorRegistry.getServiceLocator().getSpecimenService();

        Specimen s = getExampleSpecimen();
        Participant p = new Participant();
        p.setId(getObject().getId());
        s.setParticipant(p);

        SpecimenSearchCriteria criteria = new SpecimenSearchCriteria(s, null, null, null, null);
        PaginatedList<Specimen> list = new SortablePaginatedList<Specimen, SpecimenSortCriterion>(1,
                SpecimenSortCriterion.EXTERNAL_ID.name(), SpecimenSortCriterion.class);

        if (!ServletActionContext.getRequest().getParameterMap().containsKey("pageSize")) {
            String objectsPerPage = ServletActionContext.getServletContext().getInitParameter(DEFAULT_PAGE_SIZE);
            setPageSize(Integer.parseInt(objectsPerPage));
        }

        list.setObjectsPerPage(getPageSize());
        list.setPageNumber(getPage());
        if (StringUtils.isNotBlank(getSort())) {
            list.setSortCriterion(getSort());
        }
        list.setSortDirection(SortOrderEnum.fromName(getDir()));

        //set up the PageSortParams
        int index = list.getObjectsPerPage() * (list.getPageNumber() - 1);
        boolean desc = SortOrderEnum.DESCENDING.equals(list.getSortDirection());
        List<SpecimenSortCriterion> typedSortCriteria = new ArrayList<SpecimenSortCriterion>();
        String[] sortCriteria = StringUtils.split(list.getSortCriterion(), ",");
        for (String crit : sortCriteria) {
            typedSortCriteria.add(Enum.valueOf(SpecimenSortCriterion.class, crit));
        }
        PageSortParams<Specimen> pageSortParams =
            new PageSortParams<Specimen>(getPageSize(), index, typedSortCriteria, desc);

        list.setList(specimenService.search(criteria, pageSortParams));
        list.setFullListSize(specimenService.count(criteria));
        setSpecimens(list);
    }

    /**
     * action to save an institution.
     * @return the forward to go to.
     */
    @Validations(
            customValidators = { @CustomValidator(type = "hibernate", fieldName = "object" ,
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "participant") })
            },
            fieldExpressions = {
                @FieldExpressionValidator(fieldName = "institutionSelection",
                        key = "participant.externalIdAssigner.required",
                        expression = "institutionSelection == object.externalIdAssigner.name")
            }
    )
    public String save() {
        ParticipantServiceLocal service = getService();
        service.savePersistentObject(getObject());
        addActionMessage(getText("participant.success"));
        return list();
    }

    /**
     * Action to withdraw consent for all eligible specimens
     * associated with this participant.
     * @return struts forward.
     */
    @SkipValidation
    public String withdrawConsent() {
        SpecimenServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        service.withdrawConsentForParticipant(getObject());
        addActionMessage(getText("participant.withdrawConsent.success"));
        return input();
    }
    
    /**
     * Tests to see whether or not a participant specimen is eligible to view more details depending on
     * whether the specimen is in a shipment or request status and current user roles.
     * @param specimen the specimen
     * @return true if the specimen has more details to view
     */
    public boolean hasMoreDetails(Specimen specimen) {
        HttpServletRequest request = ServletActionContext.getRequest();
        boolean results = false;
        if (request.isUserInRole(Role.SHIPMENT_ADMIN.getName())) {
            Shipment shipment =
                TissueLocatorRegistry.getServiceLocator().getShipmentService().getShipmentWithSpecimen(specimen);
            if (shipment != null) {
                results = true;
            }
        } else if (request.isUserInRole(Role.TISSUE_REQUEST_ADMIN.getName())) {
            SpecimenRequest req =
                TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService().getRequestWithSpecimen(specimen);
            if (req != null) {
                results = true;
            }
        }

        return results;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public PaginatedList<Participant> getPaginatedList() {
        return new SortablePaginatedList<Participant, ParticipantSortCriterion>(1,
                ParticipantSortCriterion.EXTERNAL_ID.name(), ParticipantSortCriterion.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ParticipantServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getParticipantService();
    }

    /**
     * @return A participants specimens
     */
    public PaginatedList<Specimen> getSpecimens() {
        return specimens;
}

    /**
     * @param specimens the participant specimens to set
     */
    public void setSpecimens(PaginatedList<Specimen> specimens) {
        this.specimens = specimens;
    }

    /**
     * @return the specimen to use as an example in search
     */
    public Specimen getExampleSpecimen() {
        return exampleSpecimen;
    }

    /**
     * @param exampleSpecimen the specimen
     */
    public void setExampleSpecimen(Specimen exampleSpecimen) {
        this.exampleSpecimen = exampleSpecimen;
    }
}
