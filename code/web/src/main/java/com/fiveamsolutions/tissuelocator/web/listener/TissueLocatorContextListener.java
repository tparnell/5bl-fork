/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.listener;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorContextParameterHelper;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * @author ddasgupta
 *
 */
public class TissueLocatorContextListener implements ServletContextListener {

    private static final int MILLIS_PER_DAY = 24 * 60 * 60 * 1000;
    private static final int INITIAL_DELAY = 1000;
    private static final Logger LOG = Logger.getLogger(TissueLocatorContextListener.class);
    private static Timer votingTask = new Timer();
    private static Timer reminderThread = new Timer();
    private static Timer homePageCacheThread = new Timer();
    @Inject
    private static Provider<ApplicationSettingServiceLocal> appSettingServiceProvider;

    /**
     * {@inheritDoc}
     */
    public void contextInitialized(ServletContextEvent context) {
        ServletContext sc = context.getServletContext();

        try {
            initializeHibernateSession();

            if (appSettingServiceProvider.get().isVotingTaskEnabled()) {
                initializeRequestVotingThread(sc);
            }

            initializeScheduledReminderThread();

            initializeHomePageDataCacheRefreshThread(sc);
        } finally {
            closeHibernateSession();
        }
    }

    /**
     * @param sc
     */
    private void initializeScheduledReminderThread() {
        try {
            initializeHibernateSession();

            Date startTime = appSettingServiceProvider.get().getScheduledReminderThreadStartTime();
            int shipmentGracePeriod = appSettingServiceProvider.get().getShipmentReceiptGracePeriod();
            int lineItemReviewGracePeriod = appSettingServiceProvider.get().getLineItemReviewGracePeriod();
            int suppoertLetterRequestReviewGracePeriod =
                appSettingServiceProvider.get().getSupportLetterRequestReviewGracePeriod();
            int questionResponseGracePeriod = appSettingServiceProvider.get().getQuestionResponseGracePeriod();

            TimerTask task = new ScheduledRemindersTask(shipmentGracePeriod, lineItemReviewGracePeriod,
                    suppoertLetterRequestReviewGracePeriod, questionResponseGracePeriod);
            reminderThread.scheduleAtFixedRate(task, startTime, MILLIS_PER_DAY);
        } finally {
            closeHibernateSession();
        }
    }

    /**
     * @param sc
     */
    private void initializeRequestVotingThread(ServletContext sc) {
        String startTimeString = TissueLocatorContextParameterHelper.getVotingTime(sc);
        int startHour = Integer.parseInt(startTimeString.substring(0, 2));
        int startMinute = Integer.parseInt(startTimeString.substring(2));
        Calendar cal = Calendar.getInstance();
        cal = DateUtils.truncate(cal, Calendar.DATE);
        cal.set(Calendar.HOUR_OF_DAY, startHour);
        cal.set(Calendar.MINUTE, startMinute);
        if (!cal.getTime().after(new Date())) {
            cal.add(Calendar.DATE, 1);
        }
        LOG.info("fist run of specimen request voting task will be at " + cal.getTime().toString());

        int reviewPeriod = TissueLocatorContextParameterHelper.getReviewPeriod(sc);
        int votingPeriod = TissueLocatorContextParameterHelper.getVotingPeriod(sc);
        int minPercentAvailableVotes = TissueLocatorContextParameterHelper.getMinPercentAvailableVotes(sc);
        int minPercentWinningVotes = TissueLocatorContextParameterHelper.getMinPercentWinningVotes(sc);
        int reviewerAssignmentPeriod = TissueLocatorContextParameterHelper.getReviewerAssignmentPeriod(sc);
        LOG.trace("with parameters :");
        LOG.trace("review period: " + reviewPeriod);
        LOG.trace("voting period: " + votingPeriod);
        LOG.trace("required percent available votes: " + minPercentAvailableVotes);
        LOG.trace("required percent winning votes: " + minPercentWinningVotes);
        LOG.trace("reviewer assignment period: " + reviewerAssignmentPeriod);
        TimerTask task = new SpecimenRequestVotingTask(reviewPeriod, votingPeriod, minPercentAvailableVotes,
                minPercentWinningVotes, reviewerAssignmentPeriod);
        votingTask.scheduleAtFixedRate(task, cal.getTime(), MILLIS_PER_DAY);
    }

    private void initializeHomePageDataCacheRefreshThread(ServletContext sc) {
        try {
            initializeHibernateSession();
            int period = appSettingServiceProvider.get().getHomePageDataCacheRefreshInterval();
            Set<String> widgets = new HashSet<String>();
            widgets.add(appSettingServiceProvider.get().getLeftBrowseWidget());
            widgets.add(appSettingServiceProvider.get().getCenterBrowseWidget());
            widgets.add(appSettingServiceProvider.get().getRightBrowseWidget());
            widgets.add(appSettingServiceProvider.get().getLeftHomeWidget());
            widgets.add(appSettingServiceProvider.get().getCenterHomeWidget());
            widgets.add(appSettingServiceProvider.get().getRightHomeWidget());
            TimerTask task = new HomePageDataCacheRefreshTask(sc, widgets);
            homePageCacheThread.scheduleAtFixedRate(task, INITIAL_DELAY, period);
        } finally {
            closeHibernateSession();
        }
    }

    /**
     * init hibernate.
     */
    protected void initializeHibernateSession() {
        TissueLocatorHibernateUtil.getHibernateHelper().openAndBindSession();
    }

    /**
     * unbind hibernate.
     */
    protected void closeHibernateSession() {
        TissueLocatorHibernateUtil.getHibernateHelper().unbindAndCleanupSession();
    }

    /**
     * {@inheritDoc}
     */
    public void contextDestroyed(ServletContextEvent context) {
        votingTask.cancel();
        reminderThread.cancel();
        homePageCacheThread.cancel();
    }

    /**
     * @param provider the application setting service provider to set
     */
    @Inject
    public void setAppSettingServiceProvider(Provider<ApplicationSettingServiceLocal> provider) {
        appSettingServiceProvider = provider;
    }
}
