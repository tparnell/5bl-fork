/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.interceptor;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.struts2.dispatcher.ServletActionRedirectResult;
import org.apache.struts2.dispatcher.ServletRedirectResult;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.Result;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

/**
 * An Interceptor to preserve an actions ValidationAware messages across a redirect result.
 *
 * It makes the assumption that you always want to preserve messages across a redirect and restore them to the next
 * action if they exist.
 *
 * The way this works is it looks at the result type after a action has executed and if the result was a redirect
 * (ServletRedirectResult) or a redirectAction (ServletActionRedirectResult) and there were any errors, messages, or
 * fieldErrors they are stored in the session. Before the next action executes it will check if there are any messages
 * stored in the session and add them to the next action.
 *
 * <br>
 * This code was adapted from:
 * http://glindholm.wordpress.com/2008/07/02/preserving-messages-across-a-redirect-in-struts-2/
 */
@SuppressWarnings({ "PMD.CyclomaticComplexity", "PMD.SignatureDeclareThrowsException", "unchecked" })
public class RedirectMessageInterceptor extends MethodFilterInterceptor {
    private static final long serialVersionUID = -1847557437429753540L;

    /**
     * Field errors.
     */
    public static final String FIELD_ERRORS_KEY = "RedirectMessageInterceptor_FieldErrors";
    /**
     * Action errors.
     */
    public static final String ACTION_ERRORS_KEY = "RedirectMessageInterceptor_ActionErrors";
    /**
     * Action messages.
     */
    public static final String ACTION_MESSAGES_KEY = "RedirectMessageInterceptor_ActionMessages";

    /**
     * {@inheritDoc}
     */
    @Override
    public String doIntercept(ActionInvocation invocation) throws Exception {
        Object action = invocation.getAction();
        if (action instanceof ValidationAware) {
            before(invocation, (ValidationAware) action);
        }

        String result = invocation.invoke();

        if (action instanceof ValidationAware) {
            after(invocation, (ValidationAware) action);
        }
        return result;
    }

    @SuppressWarnings("rawtypes")
    private void before(ActionInvocation invocation, ValidationAware validationAware) throws Exception {
        Map<String, ?> session = invocation.getInvocationContext().getSession();

        Collection<String> actionErrors = (Collection) session.remove(ACTION_ERRORS_KEY);
        if (actionErrors != null) {
            for (String error : actionErrors) {
                validationAware.addActionError(error);
            }
        }

        Collection<String> actionMessages = (Collection) session.remove(ACTION_MESSAGES_KEY);
        if (actionMessages != null) {
            for (String message : actionMessages) {
                validationAware.addActionMessage(message);
            }
        }

        Map<String, Collection<String>> fieldErrors = (Map) session.remove(FIELD_ERRORS_KEY);
        if (fieldErrors != null) {
            for (Map.Entry<String, Collection<String>> fieldError : fieldErrors.entrySet()) {
                for (String message : fieldError.getValue()) {
                    validationAware.addFieldError(fieldError.getKey(), message);
                }
            }
        }
    }

    private void after(ActionInvocation invocation, ValidationAware validationAware) throws Exception {
        Result result = invocation.getResult();

        if (result instanceof ServletRedirectResult || result instanceof ServletActionRedirectResult) {
            Map<String, Object> session = invocation.getInvocationContext().getSession();

            Collection<String> actionErrors = validationAware.getActionErrors();
            if (actionErrors != null) {
                session.put(ACTION_ERRORS_KEY, actionErrors);
            }

            Collection<String> actionMessages = validationAware.getActionMessages();
            if (actionMessages != null) {
                session.put(ACTION_MESSAGES_KEY, actionMessages);
            }

            Map<String, List<String>> fieldErrors = validationAware.getFieldErrors();
            if (fieldErrors != null) {
                session.put(FIELD_ERRORS_KEY, fieldErrors);
            }
        }
    }
}