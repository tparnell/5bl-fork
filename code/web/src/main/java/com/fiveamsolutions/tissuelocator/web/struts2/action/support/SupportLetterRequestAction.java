/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.support;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.code.FundingSource;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.CodeSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.SupportLetterRequestSortCriterion;
import com.fiveamsolutions.tissuelocator.service.support.SupportLetterRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorFileHelper;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractPersistentObjectAction;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
@Namespace("/protected/support/letter")
@Results(value = {
        @Result(name = "success", location = "/WEB-INF/jsp/support/letter/list.jsp"),
        @Result(name = "input", location = "/WEB-INF/jsp/support/letter/edit.jsp"),
        @Result(name = "view", location = "/WEB-INF/jsp/support/letter/view.jsp"),
        @Result(name = "help", location = "/WEB-INF/jsp/info/popup/fieldHelp.jsp")
})
public class SupportLetterRequestAction
    extends AbstractPersistentObjectAction<SupportLetterRequest, SupportLetterRequestSortCriterion> {

    private static final long serialVersionUID = 6667518586825775474L;

    private File resume;
    private String resumeFileName;
    private String resumeContentType;

    private File draftProtocol;
    private String draftProtocolFileName;
    private String draftProtocolContentType;

    private File draftLetter;
    private String draftLetterFileName;
    private String draftLetterContentType;

    private List<FundingSource> fundingSources;
    private String investigatorOrganizationName;
    private boolean copyinvestigator;

    private final SupportLetterRequestServiceLocal supportLetterRequestService;
    private final LobHolderServiceLocal lobHolderService;

    /**
     * Default constructor.
     */
    public SupportLetterRequestAction() {
        throw new UnsupportedOperationException();
    }

    /**
     * @param supportLetterRequestService support letter request service
     * @param lobHolderService the lob holder service
     */
    @Inject
    public SupportLetterRequestAction(SupportLetterRequestServiceLocal supportLetterRequestService,
            LobHolderServiceLocal lobHolderService) {
        super();
        this.supportLetterRequestService = supportLetterRequestService;
        this.lobHolderService = lobHolderService;
        setObject(new SupportLetterRequest());
        HttpSession session = ServletActionContext.getRequest().getSession();
        TissueLocatorUser user = TissueLocatorSessionHelper.getLoggedInUser(session);
        getObject().setInvestigator(new Person(user));
        if (user.getResume() != null) {
            getObject().setResume(new TissueLocatorFile(user.getResume()));
        }
        copyinvestigator = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        convertFiles();

        CodeServiceLocal csl = TissueLocatorRegistry.getServiceLocator().getCodeService();
        setFundingSources(csl.getActiveCodes(FundingSource.class, CodeSortCriterion.ID));

        if (getObject().getInvestigator().getOrganization() != null) {
            setInvestigatorOrganizationName(getObject().getInvestigator().getOrganization().getName());
        }
    }

    private void convertFiles() {
        TissueLocatorFile resumeFile = TissueLocatorFileHelper.getFile(getResume(), getResumeFileName(),
                getResumeContentType(), getObject().getResume(), lobHolderService);
        if (resumeFile != null) {
            getObject().setResume(resumeFile);
        }

        if (getDraftProtocol() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getDraftProtocol());
            getObject().setDraftProtocol(new TissueLocatorFile(fileContents,
                    getDraftProtocolFileName(), getDraftProtocolContentType()));
        }

        if (getDraftLetter() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getDraftLetter());
            getObject().setDraftLetter(new TissueLocatorFile(fileContents,
                    getDraftLetterFileName(), getDraftLetterContentType()));
        }
    }

    /**
     * action to save a request for a letter of support.
     * @return the forward to go to.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "supportLetterRequest")
                })
            },
            fieldExpressions = {
                @FieldExpressionValidator(fieldName = "investigatorOrganizationName",
                        key = "supportLetterRequest.investigator.organization.required",
                        expression = "investigatorOrganizationName == object.investigator.organization.name")
            })
    @Action(value = "save")
    public String save() {
        getService().saveSupportLetterRequest(getObject());
        addActionMessage(getText("supportLetterRequest.save.success"));
        return list();
    }

    /**
     * Action for viewing a support letter request.
     * @return a struts forward.
     */
    @SkipValidation
    @Action(value = "view")
    public String view() {
        return "view";
    }

    /**
     * Action for viewing support letter request help text.
     * @return a struts forward.
     */
    @SkipValidation
    @Action(value = "info/popup/fieldHelp")
    public String help() {
        return "help";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PaginatedList<SupportLetterRequest> getPaginatedList() {
        return new SortablePaginatedList<SupportLetterRequest, SupportLetterRequestSortCriterion>(1,
                SupportLetterRequestSortCriterion.ID.name(), SupportLetterRequestSortCriterion.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SupportLetterRequestServiceLocal getService() {
        return supportLetterRequestService;
    }

    /**
     * @return the resume
     */
    public File getResume() {
        return resume;
    }

    /**
     * @param resume the resume to set
     */
    public void setResume(File resume) {
        this.resume = resume;
    }

    /**
     * @return the resumeFileName
     */
    public String getResumeFileName() {
        return resumeFileName;
    }

    /**
     * @param resumeFileName the resumeFileName to set
     */
    public void setResumeFileName(String resumeFileName) {
        this.resumeFileName = resumeFileName;
    }

    /**
     * @return the resumeContentType
     */
    public String getResumeContentType() {
        return resumeContentType;
    }

    /**
     * @param resumeContentType the resumeContentType to set
     */
    public void setResumeContentType(String resumeContentType) {
        this.resumeContentType = resumeContentType;
    }

    /**
     * @return the draftProtocolocol
     */
    public File getDraftProtocol() {
        return draftProtocol;
    }

    /**
     * @param draftProtocol the draftProtocol to set
     */
    public void setDraftProtocol(File draftProtocol) {
        this.draftProtocol = draftProtocol;
    }

    /**
     * @return the draftProtocolFileName
     */
    public String getDraftProtocolFileName() {
        return draftProtocolFileName;
    }

    /**
     * @param draftProtocolFileName the draftProtocolFileName to set
     */
    public void setDraftProtocolFileName(String draftProtocolFileName) {
        this.draftProtocolFileName = draftProtocolFileName;
    }

    /**
     * @return the draftProtocolContentType
     */
    public String getDraftProtocolContentType() {
        return draftProtocolContentType;
    }

    /**
     * @param draftProtocolContentType the draftProtocolContentType to set
     */
    public void setDraftProtocolContentType(String draftProtocolContentType) {
        this.draftProtocolContentType = draftProtocolContentType;
    }

    /**
     * @return the draftLetter
     */
    public File getDraftLetter() {
        return draftLetter;
    }

    /**
     * @param draftLetter the draftLetter to set
     */
    public void setDraftLetter(File draftLetter) {
        this.draftLetter = draftLetter;
    }

    /**
     * @return the draftLetterFileName
     */
    public String getDraftLetterFileName() {
        return draftLetterFileName;
    }

    /**
     * @param draftLetterFileName the draftLetterFileName to set
     */
    public void setDraftLetterFileName(String draftLetterFileName) {
        this.draftLetterFileName = draftLetterFileName;
    }

    /**
     * @return the draftLetterContentType
     */
    public String getDraftLetterContentType() {
        return draftLetterContentType;
    }

    /**
     * @param draftLetterContentType the draftLetterContentType to set
     */
    public void setDraftLetterContentType(String draftLetterContentType) {
        this.draftLetterContentType = draftLetterContentType;
    }

    /**
     * @return the fundingSources
     */
    public List<FundingSource> getFundingSources() {
        return fundingSources;
    }

    /**
     * @param fundingSources the fundingSources to set
     */
    public void setFundingSources(List<FundingSource> fundingSources) {
        this.fundingSources = fundingSources;
    }

    /**
     * @return the ids of the funding sources for this request.
     */
    public List<Long> getFundingSourceIds() {
        List<Long> sourceIds = new ArrayList<Long>();
        for (FundingSource fs : getObject().getFundingSources()) {
            sourceIds.add(fs.getId());
        }
        return sourceIds;
    }

    /**
     * @return the investigatorOrganizationName
     */
    public String getInvestigatorOrganizationName() {
        return investigatorOrganizationName;
    }

    /**
     * @param investigatorOrganizationName the investigatorOrganizationName to set
     */
    public void setInvestigatorOrganizationName(String investigatorOrganizationName) {
        this.investigatorOrganizationName = investigatorOrganizationName;
    }

    /**
     * @return the copyinvestigator
     */
    public boolean isCopyinvestigator() {
        return copyinvestigator;
    }

    /**
     * @param copyinvestigator the copyinvestigator to set
     */
    public void setCopyinvestigator(boolean copyinvestigator) {
        this.copyinvestigator = copyinvestigator;
    }
}
