/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.support;

import java.io.File;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.SupportLetterRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorFileHelper;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
@Namespace("/admin/support/letter")
@Results(value = {
        @Result(name = "success", location = "/WEB-INF/jsp/support/letter/adminList.jsp"),
        @Result(name = "input", location = "/WEB-INF/jsp/support/letter/review.jsp"),
        @Result(name = "view", location = "/WEB-INF/jsp/support/letter/adminView.jsp")
})
public class SupportLetterRequestAdministrationAction extends SupportLetterRequestAction {

    private static final long serialVersionUID = -2478466619261683353L;

    /**
     * Default constructor.
     */
    public SupportLetterRequestAdministrationAction() {
        throw new UnsupportedOperationException();
    }

    /**
     * @param supportLetterRequestService support letter request service
     * @param lobHolderService the lob holder service
     */
    @Inject
    public SupportLetterRequestAdministrationAction(SupportLetterRequestServiceLocal supportLetterRequestService,
            LobHolderServiceLocal lobHolderService) {
        super(supportLetterRequestService, lobHolderService);
    }

    private File letter;
    private String letterFileName;
    private String letterContentType;

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();

        if (getLetter() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getLetter());
            getObject().setLetter(new TissueLocatorFile(fileContents,
                    getLetterFileName(), getLetterContentType()));
        }
    }

    /**
     * action to review a request for a letter of support.
     * @return the forward to go to.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "supportLetterRequest")
                })
            }
        )
    @Action(value = "review")
    public String review() {
        getService().reviewSupportLetterRequest(getObject());
        addActionMessage(getText("supportLetterRequest.review.success"));
        return list();
    }

    /**
     * @return the letter
     */
    public File getLetter() {
        return letter;
    }

    /**
     * @param letter the letter to set
     */
    public void setLetter(File letter) {
        this.letter = letter;
    }

    /**
     * @return the letterFileName
     */
    public String getLetterFileName() {
        return letterFileName;
    }

    /**
     * @param letterFileName the letterFileName to set
     */
    public void setLetterFileName(String letterFileName) {
        this.letterFileName = letterFileName;
    }

    /**
     * @return the letterContentType
     */
    public String getLetterContentType() {
        return letterContentType;
    }

    /**
     * @param letterContentType the letterContentType to set
     */
    public void setLetterContentType(String letterContentType) {
        this.letterContentType = letterContentType;
    }
}
