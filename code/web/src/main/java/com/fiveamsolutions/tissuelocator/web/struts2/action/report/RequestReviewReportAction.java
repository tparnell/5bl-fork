/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.report;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ReportServiceLocal;
import com.fiveamsolutions.tissuelocator.util.RequestReviewReport;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorContextParameterHelper;
import com.opensymphony.xwork2.Preparable;

/**
 * @author ddasgupta
 *
 */
@Namespace("/admin/reports")
@Results(value = { @Result(name = "success", location = "requestReviewReport.jsp") })
@SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
public class RequestReviewReportAction extends AbstractReportAction implements Preparable {

    private static final long serialVersionUID = -3209214081701037097L;

    private RequestReviewReport requestReviewReport;
    private String institutionName;
    private Map<Long, List<String>> rejectionComments;

    /**
     * {@inheritDoc}
     */
    public void prepare() {
        ServletContext context = ServletActionContext.getServletContext();
        int votingPeriod = TissueLocatorContextParameterHelper.getVotingPeriod(context);
        ReportServiceLocal service = TissueLocatorRegistry.getServiceLocator().getReportService();
        setRequestReviewReport(service.getRequestReviewReport(getStartDate(), getEndDate(), votingPeriod));
    }

    /**
     * display the report.
     * @return a struts forward
     */
    @Action("requestReviewReport")
    public String requestReviewReport() {
        return SUCCESS;
    }
    
    /**
     * View scientific review rejection comments for an institution.
     * @return a struts forward
     */
    @Action(value = "popup/viewRejectionComments",
            results = { @Result(name = "success", location = "/WEB-INF/jsp/admin/reports/rejectionComments.jsp") })
    @SkipValidation
    public String viewRejectionComments() {
        InstitutionServiceLocal service = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        setRejectionComments(service.getScientificReviewRejectionComments(getStartDate(), 
                getEndDate(), getInstitutionName()));
        return SUCCESS;
    }

    /**
     * @return the report
     */
    public RequestReviewReport getRequestReviewReport() {
        return requestReviewReport;
    }

    /**
     * @param report the report to set
     */
    public void setRequestReviewReport(RequestReviewReport report) {
        requestReviewReport = report;
    }

    /**
     * @return the institutionName
     */
    public String getInstitutionName() {
        return institutionName;
    }

    /**
     * @param institutionName the institutionName to set
     */
    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    /**
     * @return the rejectionComments
     */
    public Map<Long, List<String>> getRejectionComments() {
        return rejectionComments;
    }

    /**
     * @param rejectionComments the rejectionComments to set
     */
    public void setRejectionComments(Map<Long, List<String>> rejectionComments) {
        this.rejectionComments = rejectionComments;
    }
    
}
