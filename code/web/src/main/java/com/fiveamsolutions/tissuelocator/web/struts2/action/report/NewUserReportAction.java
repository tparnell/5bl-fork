/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.report;

import java.io.IOException;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.fiveamsolutions.tissuelocator.service.ReportServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
@Namespace("/admin/reports")
@Results(value = { @Result(name = "success", location = "newUserReport.jsp") })
public class NewUserReportAction extends AbstractReportAction {

    private static final long serialVersionUID = -3209214081701037097L;
    private static final String REPORT_FILE_NAME = "newUsers";

    /**
     * display the report.
     * @return a struts forward
     * @throws JRException on error
     * @throws IOException on error
     */
    @Action("newUserReport")
    public String newUserReport() throws JRException, IOException {
        return runJasperReport(REPORT_FILE_NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected JasperPrint runReport(String fileName, Map<String, Object> parameters) throws JRException {
        ReportServiceLocal service = TissueLocatorRegistry.getServiceLocator().getReportService();
        return service.runNewUserReport(getReportPath(fileName), parameters);
    }
}
