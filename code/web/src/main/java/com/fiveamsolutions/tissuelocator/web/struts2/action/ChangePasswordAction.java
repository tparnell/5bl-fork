/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import javax.mail.MessagingException;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.util.SecurityUtils;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.ForgotPasswordStatus;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.google.inject.Inject;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
public class ChangePasswordAction extends ActionSupport {

    private static final long serialVersionUID = 3068118807336308261L;

    private String username;
    private String requested;
    private String retyped;
    private String nonce;
    private final TissueLocatorUserServiceLocal userService;

    /**
     * Default constructor.
     */
    public ChangePasswordAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * @param userService user service
     */
    @Inject
    public ChangePasswordAction(TissueLocatorUserServiceLocal userService) {
        super();
        this.userService = userService;
    }

    /**
     * {@inheritDoc}
     */
    @Validations(
            requiredFields = {
                    @RequiredFieldValidator(fieldName = "username", message = "",
                            key = "errors.changepassword.noUsername"),
                    @RequiredFieldValidator(fieldName = "requested", message = "",
                            key = "errors.changepassword.noRequested"),
                    @RequiredFieldValidator(fieldName = "retyped", message = "",
                            key = "errors.changepassword.noRetyped"),
                    @RequiredFieldValidator(fieldName = "nonce", message = "",
                            key = "errors.changepassword.noNonce")
            }
    )
    @Override
    public String execute() throws MessagingException {
        userService.updatePassword(getUser(), getRequested(), StringUtils.trimToNull(getNonce()));
        addActionMessage(getText("changePassword.success"));
        return Action.SUCCESS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void validate() {
        super.validate();
        if (!ObjectUtils.equals(getRequested(), getRetyped())) {
            addActionError(getText("errors.changepassword.notSame"));
        }

        if (!SecurityUtils.isAcceptablePassword(getRequested())) {
            addActionError(getText("errors.changepassword.badRequested"));
        }

        TissueLocatorUser user = getUser();
        if (user == null || !user.checkNonce(getNonce())) {
            addActionError(getText(ForgotPasswordStatus.UNKNOWN_USERNAME.getResourceKey()));
        }
    }

    private TissueLocatorUser getUser() {
        return userService.getByUsername(getUsername());
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the requested
     */
    public String getRequested() {
        return this.requested;
    }

    /**
     * @param requested the requested to set
     */
    public void setRequested(String requested) {
        this.requested = requested;
    }

    /**
     * @return the retyped
     */
    public String getRetyped() {
        return this.retyped;
    }

    /**
     * @param retyped the retyped to set
     */
    public void setRetyped(String retyped) {
        this.retyped = retyped;
    }

    /**
     * @return the nonce
     */
    public String getNonce() {
        return this.nonce;
    }

    /**
     * @param nonce the nonce to set
     */
    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
