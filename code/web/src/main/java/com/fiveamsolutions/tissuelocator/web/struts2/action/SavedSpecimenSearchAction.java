/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsConfigurator;
import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.SavedSpecimenSearch;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SavedSpecimenSearchServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SavedSpecimenSearchSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchCriteria;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.google.inject.Inject;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author smiller
 */
@Namespace("/protected/myAccount/savedSpecimenSearches")
@Results(value = {
    @Result(name = "specimenSearchPage",
            type = "redirectAction",
            params = { "actionName", "specimen/search/list.action",
                       "namespace", "/protected"
            })
})
public class SavedSpecimenSearchAction
    extends AbstractPersistentObjectAction<SavedSpecimenSearch, SavedSpecimenSearchSortCriterion>
    implements Preparable {

    private static final long serialVersionUID = 1L;
    private static final String EXAMPLE_ATTR = SpecimenSearchAction.class.getSimpleName() + "_example";
    private static final String SPECIMEN_SEARCH_PAGE = "specimenSearchPage";
    private static final String EDIT_SAVED_SEARCH_PAGE = "/WEB-INF/jsp/specimen/editSavedSearch.jsp";

    private final SavedSpecimenSearchServiceLocal savedSpecimenSearchService;
    private final SearchFieldConfigServiceLocal searchFieldConfigService;
    private final SpecimenSearchActionHelper specimenSearchActionHelper;
    private List<SavedSpecimenSearch> savedSpecimenSearches;
    private boolean saveCompleted;

    /**
     * Constructor.
     *
     * @param configurator the configurator
     * @param savedSpecimenSearchService the service that handles saving search criteria
     * @param searchFieldConfigService search field config service.
     */
    @Inject
    public SavedSpecimenSearchAction(DynamicExtensionsConfigurator configurator,
            SavedSpecimenSearchServiceLocal savedSpecimenSearchService, 
            SearchFieldConfigServiceLocal searchFieldConfigService) {
        specimenSearchActionHelper =
            new SpecimenSearchActionHelper(
                configurator.getDynamicFieldDefinitionsForClass(Specimen.class.getName()),
                SpecimenSearchAction.class.getSimpleName());
        this.savedSpecimenSearchService = savedSpecimenSearchService;
        this.searchFieldConfigService = searchFieldConfigService;
        setObject(new SavedSpecimenSearch());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        specimenSearchActionHelper.initSearchConfigs(SearchSetType.ADVANCED, searchFieldConfigService);
    }

    /**
     * Creates a new saved specimen search.
     *
     * @return the struts forward to the edit save specimen search page
     */
    @Action(value = "popup/createSavedSpecimenSearch", results = {
            @Result(name = SUCCESS, location = EDIT_SAVED_SEARCH_PAGE)
    })
    public String createSavedSpecimenSearch() {
        if (getObject() != null) {
            getObject().setId(null);
        }
        return SUCCESS;
    }

    /**
     * Updates the saved specimen search.
     *
     * @return the struts forward to the edit save specimen search page
     */
    @Action(value = "popup/updateSavedSpecimenSearch", results = {
            @Result(name = SUCCESS, location = EDIT_SAVED_SEARCH_PAGE)
    })
    public String updateSavedSpecimenSearch() {
        return SUCCESS;
    }

    /**
     * Deletes the saved specimen search.
     *
     * @return the struts forward
     */
    @Action(value = "popup/deleteSavedSpecimenSearch", results = {
            @Result(name = SUCCESS, type = "redirectAction",
                    params = {
                        "actionName", "popup/listSavedSpecimenSearches",
                        "namespace", "/protected/myAccount/savedSpecimenSearches"
                    })
    })
    public String deleteSavedSpecimenSearch() {
        SavedSpecimenSearch existingSavedSpecimenSearch =
            savedSpecimenSearchService.getPersistentObject(
                SavedSpecimenSearch.class, getObject().getId());

        if (existingSavedSpecimenSearch.getId()
                == specimenSearchActionHelper.getSavedSpecimenSearchIdFromSession()) {
            specimenSearchActionHelper.setSavedSpecimenSearchIdInSession(null);
        }

        savedSpecimenSearchService.deletePersistentObject(existingSavedSpecimenSearch);
        addActionMessage(getText("savedSpecimenSearch.deleteSuccess"));
        return SUCCESS;
    }

    /**
     * Persists the saved specimen search.
     *
     * @return the struts forward
     * @throws IOException on error
     */
    @Action(value = "popup/persistSavedSpecimenSearch", results = {
            @Result(name = SPECIMEN_SEARCH_PAGE, location = EDIT_SAVED_SEARCH_PAGE),
            @Result(name = INPUT, location = EDIT_SAVED_SEARCH_PAGE)
    })
    @Validations(customValidators = {
            @CustomValidator(type = "hibernate", fieldName = "object", parameters = {
                    @ValidationParameter(name = "resourceKeyBase", value = "savedSpecimenSearch"),
                    @ValidationParameter(name = "excludes", value = "criteriaData")
            })
    })
    public String persistSavedSpecimenSearch() throws IOException {
        specimenSearchActionHelper.loadSearchParamsFromSession();
        SpecimenSearchCriteria criteria = specimenSearchActionHelper.getSearchCriteria(getExample());
        getObject().setOwner(getLoggedInUser());
        Long id = savedSpecimenSearchService.persistSavedSpecimenSearch(getObject(), criteria);
        specimenSearchActionHelper.setSavedSpecimenSearchIdInSession(id);
        setSaveCompleted(true);
        return SPECIMEN_SEARCH_PAGE;
    }

    /**
     * The specimen search criteria was saved successfully.
     *
     * @return struts result
     */
    @Action("savedSuccessfully")
    @SkipValidation
    public String savedSuccessfully() {
        addActionMessage(getText("savedSpecimenSearch.saveSuccess"));
        setSaveCompleted(false);
        return SPECIMEN_SEARCH_PAGE;
    }

    /**
     * Loads the saved specimen search.
     *
     * @return the struts forward
     * @throws IOException on error
     * @throws ClassNotFoundException on error
     */
    @Action("loadSavedSpecimenSearch")
    public String loadSavedSpecimenSearch() throws IOException, ClassNotFoundException {
        setObject(savedSpecimenSearchService.getSavedSpecimenSearchAndData(getObject().getId()));
        SpecimenSearchCriteria criteria = getObject().getSearchCriteria();
        specimenSearchActionHelper.setMinimumAvailableQuantity(criteria.getMinimumAvailableQuantity());
        specimenSearchActionHelper.setMultiSelectParamMap(criteria.getMultiSelectParamValues());
        specimenSearchActionHelper.setMultiSelectCollectionParamMap(criteria.getMultiSelectCollectionParamValues());
        if (!criteria.getSearchConditions().isEmpty()) {
            specimenSearchActionHelper.setRangeSearchParameters(
                    criteria.getSearchConditions().iterator().next().getConditionParameters());
        }
        specimenSearchActionHelper.storeSearchParamsInSession(criteria.getCriteria());
        specimenSearchActionHelper.setSavedSpecimenSearchIdInSession(getObject().getId());
        ServletActionContext.getRequest().getSession().setAttribute(EXAMPLE_ATTR, criteria.getCriteria());
        addActionMessage(getText("savedSpecimenSearch.loadSuccess"));
        return SPECIMEN_SEARCH_PAGE;
    }

    /**
     * Go to the page that lists the saved specimen searches.
     *
     * @return the struts forward that indicates the list page
     */
    @Action(value = "popup/listSavedSpecimenSearches", results = {
            @Result(name = SUCCESS, location = "/WEB-INF/jsp/specimen/listSavedSearches.jsp")
    })
    @SkipValidation
    public String listSavedSpecimenSearches() {
        savedSpecimenSearches = savedSpecimenSearchService.getSavedSpecimenSearchesByOwner(getLoggedInUser());
        return SUCCESS;
    }

    private TissueLocatorUser getLoggedInUser() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        return TissueLocatorSessionHelper.getLoggedInUser(session);
    }

    /**
     * Gets a list of the saved specimen searches.
     *
     * @return a list of saved specimen searches
     */
    public List<SavedSpecimenSearch> getSavedSpecimenSearches() {
        return savedSpecimenSearches;
    }

    /**
     * Sets the list of the saved specimen searches.
     *
     * @param savedSearches the list of saved specimen searches to set
     */
    public void setSavedSpecimenSearches(List<SavedSpecimenSearch> savedSearches) {
        this.savedSpecimenSearches = savedSearches;
    }

    private Specimen getExample() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        return (Specimen) session.getAttribute(EXAMPLE_ATTR);
    }

    /**
     * Indicates whether the persistence of a specimen search has completed.
     *
     * @return true if the save has complete, false otherwise
     */
    public boolean isSaveCompleted() {
        return saveCompleted;
    }

    /**
     * Marks the completion of the persistence of a saved specimen search.
     *
     * @param saveCompleted if true, the persistence has completed
     */
    public void setSaveCompleted(boolean saveCompleted) {
        this.saveCompleted = saveCompleted;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected GenericServiceLocal<SavedSpecimenSearch> getService() {
        return savedSpecimenSearchService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PaginatedList<SavedSpecimenSearch> getPaginatedList() {
        return new SortablePaginatedList<SavedSpecimenSearch, SavedSpecimenSearchSortCriterion>(
                1, SavedSpecimenSearchSortCriterion.NAME.name(), SavedSpecimenSearchSortCriterion.class);
    }

}