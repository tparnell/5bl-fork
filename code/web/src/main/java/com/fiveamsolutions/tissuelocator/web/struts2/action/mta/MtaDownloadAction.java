/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.mta;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.fiveamsolutions.tissuelocator.data.LobHolder;
import com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.google.inject.Inject;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * MTA file download action.
 *
 * @author jstephens
 */
@Namespace("/")
@Results(value = {
        @Result(name = "success",
                type = "stream",
                params = { "contentType", "${currentMta.document.contentType}",
                           "contentDisposition", "attachment; filename=${currentMta.document.name}",
                           "inputName", "inputStream"
                })
})
public class MtaDownloadAction extends ActionSupport implements Preparable {

    private static final long serialVersionUID = -4721124087724918652L;

    private MaterialTransferAgreement currentMta;
    private final LobHolderServiceLocal lobHolderService;
    
    /**
     * Constructor.
     * @param lobHolderService lob holder service.
     */
    @Inject
    public MtaDownloadAction(LobHolderServiceLocal lobHolderService) {
        this.lobHolderService = lobHolderService;
    }
    
    /**
     * Prepare for download by retrieving the current MTA.
     */
    public void prepare() {
        MaterialTransferAgreementServiceLocal service =
            TissueLocatorRegistry.getServiceLocator()
                .getMaterialTransferAgreementService();

        currentMta = service.getCurrentMta();
    }

    /**
     * Execute the download of the current MTA.
     *
     * @return successful action execution.
     */
    @Override
    @Action(value = "mtaDownload")
    public String execute() {
        if (currentMta == null) {
            return INPUT;
        }

        currentMta.getDocument().setLob(getLobData());
        return SUCCESS;
    }

     /**
     * Returns the current MTA.
     *
     * @return the current MTA.
     */
    public MaterialTransferAgreement getCurrentMta() {
        return currentMta;
    }

    /**
     * Sets the current MTA.
     *
     * @param mta the MTA that will be set to current.
     */
    public void setCurrentMta(MaterialTransferAgreement mta) {
        currentMta = mta;
    }

    /**
     * Returns the stream used to download the MTA.
     *
     * @return the stream used to download the MTA.
     */
    public InputStream getInputStream() {
        LobHolder lobHolder = currentMta.getDocument().getLob();

        return new ByteArrayInputStream(lobHolder.getData());
    }

    private LobHolder getLobData() {
        Long id = currentMta.getDocument().getLob().getId();
        return lobHolderService.getLob(id);
    }

}