/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.nci.commons.data.security.AccountStatus;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.ShipmentStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.data.support.QuestionResponse;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SignedMaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenRequestSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.QuestionResponseServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.SupportLetterRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.google.inject.Inject;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * @author ddasgupta
 */
@SuppressWarnings("PMD.TooManyFields")
public class HomeAction extends ActionSupport implements Preparable {

    private static final long serialVersionUID = -292548573610575735L;

    private int approvedRequestCount = -1;
    private int assignReviewerCount = -1;
    private int leadReviewerCount = -1;
    private int consortiumReviewCount = -1;
    private int institutionalReviewCount = -1;
    private int lineItemReviewCount = -1;
    private int finalCommentCount = -1;
    private int researcherDecisionNotificationCount = -1;
    private int requestorActionRequiredCount = -1;
    private int registrationReviewCount = -1;
    private int mtaReviewCount = -1;
    private int supportLetterRequestCount = -1;
    private int questionResponseCount = -1;

    private String leftBrowseWidget;
    private String centerBrowseWidget;
    private String rightBrowseWidget;
    private final TissueLocatorUserServiceLocal userService;
    private final ApplicationSettingServiceLocal appSettingService;
    private final SupportLetterRequestServiceLocal supportLetterRequestService;
    private final QuestionResponseServiceLocal questionResponseService;

    /**
     * Default constructor.
     */
    public HomeAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     * @param userService the user service.
     * @param appSettingService the application setting service
     * @param supportLetterRequestService the support letter request service
     * @param questionResponseService the question response service
     */
    @Inject
    public HomeAction(TissueLocatorUserServiceLocal userService,
            ApplicationSettingServiceLocal appSettingService,
            SupportLetterRequestServiceLocal supportLetterRequestService,
            QuestionResponseServiceLocal questionResponseService) {
        super();
        this.userService = userService;
        this.appSettingService = appSettingService;
        this.supportLetterRequestService = supportLetterRequestService;
        this.questionResponseService = questionResponseService;
    }

    /**
     * {@inheritDoc}
     */
    public void prepare() {
        if (StringUtils.isNotBlank(ServletActionContext.getRequest().getRemoteUser())) {
            populateRequestToDoList();
            populateSupportRequestToDoList();

            if (ServletActionContext.getRequest().isUserInRole(Role.SHIPMENT_ADMIN.getName())) {
                Shipment example = new Shipment();
                example.setStatus(ShipmentStatus.PENDING);
                HttpSession session = ServletActionContext.getRequest().getSession();
                Institution institution = TissueLocatorSessionHelper.getLoggedInUser(session).getInstitution();
                example.setSendingInstitution(institution);
                SearchCriteria<Shipment> shipmentCriteria = new TissueLocatorAnnotatedBeanSearchCriteria<Shipment>(
                        example);
                ShipmentServiceLocal shipmentService = TissueLocatorRegistry.getServiceLocator().getShipmentService();
                setApprovedRequestCount(shipmentService.count(shipmentCriteria));
            }

            if (ServletActionContext.getRequest().isUserInRole(Role.USER_ACCOUNT_APPROVER.getName())) {
                TissueLocatorUser example = new TissueLocatorUser();
                example.setStatus(AccountStatus.PENDING);
                SearchCriteria<TissueLocatorUser> userCriteria =
                    new TissueLocatorAnnotatedBeanSearchCriteria<TissueLocatorUser>(example);
                setRegistrationReviewCount(userService.count(userCriteria));
            }

            if (ServletActionContext.getRequest().isUserInRole(Role.MTA_ADMINISTRATOR.getName())) {
                SignedMaterialTransferAgreement example = new SignedMaterialTransferAgreement();
                example.setStatus(SignedMaterialTransferAgreementStatus.PENDING_REVIEW);
                SearchCriteria<SignedMaterialTransferAgreement> mtaCriteria =
                    new TissueLocatorAnnotatedBeanSearchCriteria<SignedMaterialTransferAgreement>(example);
                SignedMaterialTransferAgreementServiceLocal mtaService =
                    TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
                setMtaReviewCount(mtaService.count(mtaCriteria));
            }

            leftBrowseWidget = appSettingService.getLeftHomeWidget();
            centerBrowseWidget = appSettingService.getCenterHomeWidget();
            rightBrowseWidget = appSettingService.getRightHomeWidget();

        } else {

            leftBrowseWidget = appSettingService.getLeftBrowseWidget();
            centerBrowseWidget = appSettingService.getCenterBrowseWidget();
            rightBrowseWidget = appSettingService.getRightBrowseWidget();

        }
    }

    private void populateRequestToDoList() {
        SpecimenRequest sr = new SpecimenRequest();
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        SearchCriteria<SpecimenRequest> criteria;
        TissueLocatorUser currentUser = TissueLocatorSessionHelper.getLoggedInUser(ServletActionContext.getRequest()
                .getSession());

        ReviewProcess reviewProcess = appSettingService.getReviewProcess();
        populateRequestVotingToDoList(currentUser, reviewProcess);
        if (ServletActionContext.getRequest().isUserInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName())) {
            criteria = new SpecimenRequestSearchCriteria(sr, true, false, false, false, false, false, false, false,
                    false, currentUser, reviewProcess);
            setAssignReviewerCount(service.count(criteria));
        }
        if (ServletActionContext.getRequest().isUserInRole(Role.LEAD_REVIEWER.getName())) {
            criteria = new SpecimenRequestSearchCriteria(sr, false, true, false, false, false, false, false, false,
                    false, currentUser, reviewProcess);
            setLeadReviewerCount(service.count(criteria));
            criteria = new SpecimenRequestSearchCriteria(sr, false, false, false, false, false, true, false, false,
                    false, currentUser, reviewProcess);
            setFinalCommentCount(service.count(criteria));
        }
        if (ServletActionContext.getRequest().isUserInRole(Role.REVIEW_DECISION_NOTIFIER.getName())) {
            criteria = new SpecimenRequestSearchCriteria(sr, false, false, false, false, false, false, true, false,
                    false, currentUser, reviewProcess);
            setResearcherDecisionNotificationCount(service.count(criteria));
        }

        sr.setRequestor(TissueLocatorSessionHelper.getLoggedInUser(ServletActionContext.getRequest().getSession()));
        criteria = new SpecimenRequestSearchCriteria(sr, false, false, false, false, false, false, false, true,
                false, currentUser, reviewProcess);
        setRequestorActionRequiredCount(service.count(criteria));
    }

    private void populateRequestVotingToDoList(TissueLocatorUser currentUser, ReviewProcess reviewProcess) {
        SpecimenRequest sr = new SpecimenRequest();
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        SearchCriteria<SpecimenRequest> criteria;
        if (ServletActionContext.getRequest().isUserInRole(Role.INSTITUTIONAL_REVIEW_VOTER.getName())) {
            criteria = new SpecimenRequestSearchCriteria(sr, false, false, false, true, false, false, false, false,
                    false, currentUser, reviewProcess);
            setInstitutionalReviewCount(service.count(criteria));
        }
        if (ServletActionContext.getRequest().isUserInRole(Role.CONSORTIUM_REVIEW_VOTER.getName())) {
            criteria = new SpecimenRequestSearchCriteria(sr, false, false, true, false, false, false, false, false,
                    false, currentUser, reviewProcess);
            setConsortiumReviewCount(service.count(criteria));
        }
        if (ServletActionContext.getRequest().isUserInRole(Role.LINE_ITEM_REVIEW_VOTER.getName())) {
            criteria = new SpecimenRequestSearchCriteria(sr, false, false, false, false, true, false, false, false,
                    false, currentUser, reviewProcess);
            setLineItemReviewCount(service.count(criteria));
        }
    }

    private void populateSupportRequestToDoList() {
        if (ServletActionContext.getRequest().isUserInRole(Role.SUPPORT_LETTER_REQUEST_REVIEWER.getName())) {
            SupportLetterRequest example = new SupportLetterRequest();
            example.setStatus(SupportRequestStatus.PENDING);
            SearchCriteria<SupportLetterRequest> supportLetterRequestCriteria =
                new TissueLocatorAnnotatedBeanSearchCriteria<SupportLetterRequest>(example);
            setSupportLetterRequestCount(supportLetterRequestService.count(supportLetterRequestCriteria));
        }

        if (ServletActionContext.getRequest().isUserInRole(Role.QUESTION_RESPONDER.getName())) {
            QuestionResponse example = new QuestionResponse();
            example.setStatus(SupportRequestStatus.PENDING);
            HttpSession session = ServletActionContext.getRequest().getSession();
            Institution institution = TissueLocatorSessionHelper.getLoggedInUser(session).getInstitution();
            example.setInstitution(institution);
            SearchCriteria<QuestionResponse> questionResponseCriteria =
                new TissueLocatorAnnotatedBeanSearchCriteria<QuestionResponse>(example);
            setQuestionResponseCount(questionResponseService.count(questionResponseCriteria));
        }
    }

    /**
     * @return the approvedRequestCount
     */
    public int getApprovedRequestCount() {
        return approvedRequestCount;
    }

    /**
     * @param approvedRequestCount the approvedRequestCount to set
     */
    public void setApprovedRequestCount(int approvedRequestCount) {
        this.approvedRequestCount = approvedRequestCount;
    }

    /**
     * @return the assignReviewerCount
     */
    public int getAssignReviewerCount() {
        return assignReviewerCount;
    }

    /**
     * @param assignReviewerCount the assignReviewerCount to set
     */
    public void setAssignReviewerCount(int assignReviewerCount) {
        this.assignReviewerCount = assignReviewerCount;
    }

    /**
     * @return the leadReviewerCount
     */
    public int getLeadReviewerCount() {
        return leadReviewerCount;
    }

    /**
     * @param leadReviewerCount the leadReviewerCount to set
     */
    public void setLeadReviewerCount(int leadReviewerCount) {
        this.leadReviewerCount = leadReviewerCount;
    }

    /**
     * @return the consortiumReviewCount
     */
    public int getConsortiumReviewCount() {
        return consortiumReviewCount;
    }

    /**
     * @param consortiumReviewCount the consortiumReviewCount to set
     */
    public void setConsortiumReviewCount(int consortiumReviewCount) {
        this.consortiumReviewCount = consortiumReviewCount;
    }

    /**
     * @return the institutionalReviewCount
     */
    public int getInstitutionalReviewCount() {
        return institutionalReviewCount;
    }

    /**
     * @param institutionalReviewCount the institutionalReviewCount to set
     */
    public void setInstitutionalReviewCount(int institutionalReviewCount) {
        this.institutionalReviewCount = institutionalReviewCount;
    }

    /**
     * @return the lineItemReviewCount
     */
    public int getLineItemReviewCount() {
        return lineItemReviewCount;
    }

    /**
     * @param lineItemReviewCount the lineItemReviewCount to set
     */
    public void setLineItemReviewCount(int lineItemReviewCount) {
        this.lineItemReviewCount = lineItemReviewCount;
    }

    /**
     * @return the finalCommentCount
     */
    public int getFinalCommentCount() {
        return finalCommentCount;
    }

    /**
     * @param finalCommentCount the finalCommentCount to set
     */
    public void setFinalCommentCount(int finalCommentCount) {
        this.finalCommentCount = finalCommentCount;
    }

    /**
     * @return the researcherDecisionNotificationCount
     */
    public int getResearcherDecisionNotificationCount() {
        return researcherDecisionNotificationCount;
    }

    /**
     * @param researcherDecisionNotificationCount the researcherDecisionNotificationCount to set
     */
    public void setResearcherDecisionNotificationCount(int researcherDecisionNotificationCount) {
        this.researcherDecisionNotificationCount = researcherDecisionNotificationCount;
    }

    /**
     * @return the requestorActionRequiredCount
     */
    public int getRequestorActionRequiredCount() {
        return requestorActionRequiredCount;
    }

    /**
     * @param requestorActionRequiredCount the requestorActionRequiredCount to set
     */
    public void setRequestorActionRequiredCount(int requestorActionRequiredCount) {
        this.requestorActionRequiredCount = requestorActionRequiredCount;
    }

    /**
     * @return the registrationReviewCount
     */
    public int getRegistrationReviewCount() {
        return registrationReviewCount;
    }

    /**
     * @param registrationReviewCount the registrationReviewCount to set
     */
    public void setRegistrationReviewCount(int registrationReviewCount) {
        this.registrationReviewCount = registrationReviewCount;
    }

    /**
     * @return the mtaReviewCount
     */
    public int getMtaReviewCount() {
        return mtaReviewCount;
    }

    /**
     * @param mtaReviewCount the mtaReviewCount to set
     */
    public void setMtaReviewCount(int mtaReviewCount) {
        this.mtaReviewCount = mtaReviewCount;
    }

    /**
     * @return the supportLetterRequestCount
     */
    public int getSupportLetterRequestCount() {
        return supportLetterRequestCount;
    }

    /**
     * @param supportLetterRequestCount the supportLetterRequestCount to set
     */
    public void setSupportLetterRequestCount(int supportLetterRequestCount) {
        this.supportLetterRequestCount = supportLetterRequestCount;
    }

    /**
     * @return the questionResponseCount
     */
    public int getQuestionResponseCount() {
        return questionResponseCount;
    }

    /**
     * @param questionResponseCount the questionResponseCount to set
     */
    public void setQuestionResponseCount(int questionResponseCount) {
        this.questionResponseCount = questionResponseCount;
    }

    /**
     * @return the leftBrowseWidget
     */
    public String getLeftBrowseWidget() {
        return leftBrowseWidget;
    }

    /**
     * @return the centerBrowseWidget
     */
    public String getCenterBrowseWidget() {
        return centerBrowseWidget;
    }

    /**
     * @return the rightBrowseWidget
     */
    public String getRightBrowseWidget() {
        return rightBrowseWidget;
    }
}
