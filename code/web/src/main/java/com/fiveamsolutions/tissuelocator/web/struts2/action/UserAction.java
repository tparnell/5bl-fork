/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.data.security.AccountStatus;
import com.fiveamsolutions.nci.commons.data.security.Password;
import com.fiveamsolutions.nci.commons.data.security.PasswordType;
import com.fiveamsolutions.nci.commons.data.security.UserGroup;
import com.fiveamsolutions.nci.commons.util.SecurityUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.Country;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Institution.InstitutionComparator;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorUserSortCriterion;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorFileHelper;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.util.ExtendableEntitySection;
import com.fiveamsolutions.tissuelocator.web.util.SelectOption;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 */
@SuppressWarnings({ "PMD.TooManyMethods" })
public class UserAction extends AbstractDynamicExtensionAction<TissueLocatorUser, TissueLocatorUserSortCriterion> {

    private static final long serialVersionUID = 1178441556890029360L;
    private static final String DEFAULT_PASSWORD = "changeMe!";

    private String password;
    private String confirmPassword;

    private final List<InstitutionType> types = new ArrayList<InstitutionType>();
    private final List<Institution> institutions = new ArrayList<Institution>();
    private final List<UserGroup> groups = new ArrayList<UserGroup>();
    private final TissueLocatorUserServiceLocal userService;
    private final ApplicationSettingServiceLocal appSettingService;
    private boolean accountApprovalActive;

    private boolean userResumeRequired;
    private File resume;
    private String resumeFileName;
    private String resumeContentType;

    /**
     * Default constructor.
     */
    public UserAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * @param userService user service
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     */
    @Inject
    public UserAction(TissueLocatorUserServiceLocal userService, ApplicationSettingServiceLocal appSettingService,
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
        super(uiDynamicFieldCategoryService);
        this.userService = userService;
        this.appSettingService = appSettingService;
        setObject(new TissueLocatorUser());
        getObject().setStatus(null);
        getObject().setInstitution(new Institution());
        getObject().getAddress().setCountry(Country.UNITED_STATES);
        Password p = new Password();
        p.setType(PasswordType.PLAINTEXT);
        getObject().setPassword(p);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        TissueLocatorUser loggedInUser =
            TissueLocatorSessionHelper.getLoggedInUser(ServletActionContext.getRequest().getSession());
        if (loggedInUser != null && !loggedInUser.isCrossInstitution()) {
            getObject().getAddress().setLine1(loggedInUser.getAddress().getLine1());
            getObject().getAddress().setLine2(loggedInUser.getAddress().getLine2());
            getObject().getAddress().setCity(loggedInUser.getAddress().getCity());
            getObject().getAddress().setState(loggedInUser.getAddress().getState());
            getObject().getAddress().setZip(loggedInUser.getAddress().getZip());
            getObject().getAddress().setCountry(loggedInUser.getAddress().getCountry());
        }

        super.prepare();

        // get the groups that can be assigned to a user by this admin
        // and add these groups the the list that will appear in the ui
        if (!UsernameHolder.ANONYMOUS_USERNAME.equals(UsernameHolder.getUser())) {
            groups.addAll(getAdministratableGroups());
        }

        GenericServiceLocal<PersistentObject> service =
            TissueLocatorRegistry.getServiceLocator().getGenericService();
        getInstitutions().addAll(service.getAll(Institution.class));
        getTypes().addAll(service.getAll(InstitutionType.class));
        preparePassword();

        setAccountApprovalActive(appSettingService.isAccountApprovalActive());

        setUserResumeRequired(appSettingService.isUserResumeRequired());
        if (getResume() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getResume());
            getObject().setResume(new TissueLocatorFile(fileContents,
                    getResumeFileName(), getResumeContentType()));
        }
    }

    /**
     * Perform user validation.
     */
    @Override
    public void validate() {
        super.validate();
        if (!validatePasswordField()) {
            addFieldError("password", getText("errors.changepassword.badRequested"));
        }
    }

    /**
     * Validates the password field.
     *
     * @return true if password field is valid, false otherwise
     */
    private boolean validatePasswordField() {
        // Only validate field if the created user is not new.  If it is, then the object's
        // password field has already been set and validation occurs on the data side.
        boolean isNew = getObject().getId() == null;
        if (!isNew && StringUtils.isNotBlank(getPassword())
                && !SecurityUtils.isAcceptablePassword(getPassword())) {
            return false;
        }
        return true;
    }

    /**
     * @return the institutions for the autocompleter on the registration page.
     */
    public SelectOption[] getRegistrationInstitutionsMap() {
        Boolean showConsortiumMembers = Boolean.FALSE;
        if (appSettingService.isDisplayConsortiumRegistration()) {
            showConsortiumMembers = null;
        }
        return searchInstitutions(showConsortiumMembers);
    }

    /**
     * Sets the default password to the object when the prepare interceptor is called.
     * Only relevant when a new user is created by the administrator.
     */
    private void preparePassword() {
        boolean isNew = getObject().getId() == null;
        boolean isAdmin = ServletActionContext.getRequest().isUserInRole(Role.USER_ADMIN.getName());
        if (isNew && isAdmin) {
            setPassword(DEFAULT_PASSWORD);
            setConfirmPassword(DEFAULT_PASSWORD);
        }
        // Only set the object's password if it is new.  If not, then the password
        // ends up bypassing validation and gets saved to the DB.
        if (isNew) {
            Password p = new Password();
            p.setType(PasswordType.PLAINTEXT);
            p.setValue(getPassword());
            getObject().setPassword(p);
        }
    }

    /**
     * Get the groups the logged in user can assign to another user.
     * @return the list of groups/
     */
    private List<UserGroup> getAdministratableGroups() {
        // get the roles the current user has
        TissueLocatorUser loggedInUser = userService.getByUsername(UsernameHolder.getUser());
        return userService.getAdministratableGroups(loggedInUser);
    }

    /**
     * registers a user.
     * @return struts forward
     * @throws IOException on error
     * @throws MessagingException on error
     */
    @Validations(
        customValidators = {
            @CustomValidator(type = "hibernate", fieldName = "object",
                parameters = @ValidationParameter(name = "resourceKeyBase", value = "user.register"))
        },
        fieldExpressions = {
            @FieldExpressionValidator(fieldName = "password",
                    expression = "(object.id != null) || (password != null)",
                    key = "user.register.password.required", message = ""),
            @FieldExpressionValidator(fieldName = "confirmPassword",
                    expression = "(object.id != null) || (confirmPassword != null)",
                    key = "user.register.confirmPassword.required", message = ""),
            @FieldExpressionValidator(fieldName = "confirmPassword",
                    expression = "password == confirmPassword",
                    key = "user.register.password.mismatch", message = "")
        })
    public String save() throws IOException, MessagingException {
        if (getObject().getInstitution().getConsortiumMember() == null) {
            getObject().getInstitution().setConsortiumMember(false);
        }

        generatePassword();
        clearUserGroups();
        registerUser();
        resetSessionUser();
        if (hasCurrentUsernameChanged()) {
            // changed name of current user, log the user out.
            return "logout";
        }
        return "list";
    }

    /**
     * Encrypt the password and save it to the object if
     * it has been modified/not blank.
     */
    private void generatePassword() {
        if (StringUtils.isNotBlank(getPassword())) {
            getObject().setPassword(SecurityUtils.create(getPassword()));
        }
    }

    private void clearUserGroups() {
        //If no user groups are checked, no request parameters identifying user groups are included.
        //As a result, the original group selection is maintained, when actually it should be cleared.
        //To get around this, clear the selection manually.
        List<String> paramNames = new ArrayList<String>();
        CollectionUtils.addAll(paramNames, ServletActionContext.getRequest().getParameterNames());
        if (!paramNames.contains("object.groups")) {
            getObject().getGroups().clear();
        }
    }

    private void registerUser() throws MessagingException {
        TissueLocatorUser user = getObject();
        if (isUserDenied(user)) {
            getService().denyUser(user);
        } else if (isUserDeactivated(user)) {
            getService().deactivateUser(user);
        } else {
            boolean isAdmin =
                ServletActionContext.getRequest()
                    .isUserInRole(Role.USER_ADMIN.getName());

            getService().saveUser(user, isAdmin, getPassword());
        }
        addActionMessage(getText("user.success"));
    }

    private boolean isUserDenied(TissueLocatorUser user) {
        return accountApprovalActive && AccountStatus.PENDING.equals(user.getPreviousStatus())
            && AccountStatus.INACTIVE.equals(user.getStatus());
    }

    private boolean isUserDeactivated(TissueLocatorUser user) {
        return accountApprovalActive && AccountStatus.ACTIVE.equals(user.getPreviousStatus())
            && AccountStatus.INACTIVE.equals(user.getStatus());
    }

    private void resetSessionUser() {
        if (isCurrentUserBeingEdited()) {
            TissueLocatorUser u = getService().getPersistentObject(TissueLocatorUser.class, getObject().getId());
            u.isCrossInstitution();
            TissueLocatorSessionHelper.setLoggedInUser(ServletActionContext.getRequest().getSession(), u);
        }
    }

    private boolean hasCurrentUsernameChanged() {
        return isCurrentUserBeingEdited()
            && !UsernameHolder.getUser().equals(getObject().getUsername());
    }

    private boolean isCurrentUserBeingEdited() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        TissueLocatorUser sessionUser = TissueLocatorSessionHelper.getLoggedInUser(session);
        return getObject().getId() != null && sessionUser != null && getObject().getId().equals(sessionUser.getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaginatedList<TissueLocatorUser> getPaginatedList() {
        List<String> criteria = new ArrayList<String>();
        criteria.add(TissueLocatorUserSortCriterion.LAST_NAME.name());
        criteria.add(TissueLocatorUserSortCriterion.FIRST_NAME.name());
        return new SortablePaginatedList<TissueLocatorUser, TissueLocatorUserSortCriterion>(1,
                StringUtils.join(criteria, ","), TissueLocatorUserSortCriterion.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected TissueLocatorUserServiceLocal getService() {
        return userService;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the confirmPassword
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * @param confirmPassword the confirmPassword to set
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * @return the types
     */
    public List<InstitutionType> getTypes() {
        return types;
    }

    /**
     * @return the institution
     */
    public List<Institution> getInstitutions() {
        Collections.sort(institutions, new InstitutionComparator());
        return institutions;
    }

    /**
     * @return the groups
     */
    public List<UserGroup> getGroups() {
        return groups;
     }

    /**
     * @return the ids of the group that the user belogs to.
     */
    public List<Long> getObjectGroupIds() {
        List<Long> groupIds = new ArrayList<Long>();
        for (UserGroup ug : getObject().getGroups()) {
            groupIds.add(ug.getId());
        }
        return groupIds;
    }

    /**
     * @return the accountApprovalActive
     */
    public boolean isAccountApprovalActive() {
        return accountApprovalActive;
    }

    /**
     * @param accountApprovalActive the accountApprovalActive to set
     */
    public void setAccountApprovalActive(boolean accountApprovalActive) {
        this.accountApprovalActive = accountApprovalActive;
    }

    /**
     * @return the userResumeRequired
     */
    public boolean isUserResumeRequired() {
        return userResumeRequired;
    }

    /**
     * @param userResumeRequired the userResumeRequired to set
     */
    public void setUserResumeRequired(boolean userResumeRequired) {
        this.userResumeRequired = userResumeRequired;
    }

    /**
     * @return the resume
     */
    public File getResume() {
        return resume;
    }

    /**
     * @param resume the resume to set
     */
    public void setResume(File resume) {
        this.resume = resume;
    }

    /**
     * @return the resumeFileName
     */
    public String getResumeFileName() {
        return resumeFileName;
    }

    /**
     * @param resumeFileName the resumeFileName to set
     */
    public void setResumeFileName(String resumeFileName) {
        this.resumeFileName = resumeFileName;
    }

    /**
     * @return the resumeContentType
     */
    public String getResumeContentType() {
        return resumeContentType;
    }

    /**
     * @param resumeContentType the resumeContentType to set
     */
    public void setResumeContentType(String resumeContentType) {
        this.resumeContentType = resumeContentType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Collection<? extends ExtendableEntitySection> getAllSections() {
        return Collections.emptyList();
    }
}
