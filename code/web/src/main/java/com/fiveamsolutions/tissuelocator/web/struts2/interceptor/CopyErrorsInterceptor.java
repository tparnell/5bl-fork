/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.interceptor;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.opensymphony.xwork2.util.CompoundRoot;

/**
 * This interceptor will copy errors to the child / chained action when one action uses
 * s:action to call a second.  This allows form elements conversion and validation errors
 * to be displayed under the called action's view if needed.
 * @author smiller
 */
public class CopyErrorsInterceptor extends AbstractInterceptor {

    private static final long serialVersionUID = 1L;

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("PMD.SignatureDeclareThrowsException")
    public String intercept(ActionInvocation invocation) throws Exception {
        CompoundRoot root = invocation.getStack().getRoot();
        Action currentAction = (Action) invocation.getAction();
        
        if (root.size() > 1 && isValidationAware(currentAction)) {
            Action callerAction = getFirstActionBelow(root, currentAction);
            if (callerAction != null) {
                ValidationAware currentActionVal = (ValidationAware) currentAction;
                ValidationAware callerActionVal = (ValidationAware) callerAction;

                currentActionVal.setActionErrors(callerActionVal.getActionErrors());
                currentActionVal.setFieldErrors(callerActionVal.getFieldErrors());
            }
        }

        return invocation.invoke();
    }

    /**
     * Gets the first action below the passed action.
     * @param root the stack to find the action
     * @param current is the current action.
     * @return
     */
    private Action getFirstActionBelow(CompoundRoot root, Action current) {
        boolean foundCurrentAction = false;
        for (Object obj : root) {
            if (obj == current) {
                foundCurrentAction = true;
            } else {
                if (obj instanceof Action && foundCurrentAction && isValidationAware((Action) obj)) {
                    return (Action) obj;
                }
            }
        }
        return null;
    }

    private boolean isValidationAware(Action action) {
        return action instanceof ValidationAware;
    }
}
