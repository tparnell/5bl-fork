/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web;

import javax.servlet.http.HttpSession;

import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.PrincipalInvestigator;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;

/**
 * @author smiller
 *
 */
public class TissueLocatorSessionHelper {
    private static final String CART_ATTRIBUTE_NAME = "specimenCart";
    private static final String USER_ATTRIBUTE_NAME = TissueLocatorUser.class.getSimpleName();

    /**
     * Get the shopping cart from the session.
     * @param session the session
     * @return the cart
     */
    public static final SpecimenRequest getCart(HttpSession session) {
        SpecimenRequest sessionCart = (SpecimenRequest) session.getAttribute(CART_ATTRIBUTE_NAME);
        if (sessionCart == null) {
            TissueLocatorUser user = getLoggedInUser(session);
            sessionCart = new SpecimenRequest();
            sessionCart.setRequestor(user);
            sessionCart.setInvestigator(new PrincipalInvestigator(user));
            sessionCart.getShipment().setRecipient(new Person(user));
            setCart(session, sessionCart);
        }
        return sessionCart;
    }

    /**
     * Set the shopping cart in the session.
     * @param session the session
     * @param cart the cart
     */
    public static final void setCart(HttpSession session, SpecimenRequest cart) {
        session.setAttribute(CART_ATTRIBUTE_NAME, cart);
    }

    /**
     * Get the logged in user.
     * @param session the session
     * @return the user.
     */
    public static final TissueLocatorUser getLoggedInUser(HttpSession session) {
        return (TissueLocatorUser) session.getAttribute(USER_ATTRIBUTE_NAME);
    }

    /**
     * Set the logged in user.
     * @param session the session
     * @param u the user.
     */
    public static final void setLoggedInUser(HttpSession session, TissueLocatorUser u) {
        session.setAttribute(USER_ATTRIBUTE_NAME, u);
    }

}
