/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.support;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.support.QuestionResponse;
import com.fiveamsolutions.tissuelocator.service.search.QuestionResponseSortCriterion;
import com.fiveamsolutions.tissuelocator.service.support.QuestionResponseServiceLocal;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractPersistentObjectAction;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
@Namespace("/admin/support/question/response")
@Results(value = {
        @Result(name = "success", location = "/WEB-INF/jsp/support/questionResponse/list.jsp"),
        @Result(name = "input", location = "/WEB-INF/jsp/support/questionResponse/edit.jsp"),
        @Result(name = "view", location = "/WEB-INF/jsp/support/questionResponse/view.jsp")
})
public class QuestionResponseAction
    extends AbstractPersistentObjectAction<QuestionResponse, QuestionResponseSortCriterion> {

    private static final long serialVersionUID = -4513214699302347246L;

    private final QuestionResponseServiceLocal questionResponseService;

    /**
     * Default constructor.
     */
    public QuestionResponseAction() {
        throw new UnsupportedOperationException();
    }

    /**
     * @param questionResponseService support letter request service
     */
    @Inject
    public QuestionResponseAction(QuestionResponseServiceLocal questionResponseService) {
        super();
        this.questionResponseService = questionResponseService;
        setObject(new QuestionResponse());
    }

    /**
     * action to submit a response to a question.
     * @return the forward to go to.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "questionResponse")
                })
            }
        )
    @Action(value = "respond")
    public String respond() {
        getService().submitQuestionResponse(getObject());
        addActionMessage(getText("questionResponse.submit.success"));
        return list();
    }

    /**
     * Action for viewing a question response.
     * @return a struts forward.
     */
    @SkipValidation
    @Action(value = "view")
    public String view() {
        return "view";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PaginatedList<QuestionResponse> getPaginatedList() {
        return new SortablePaginatedList<QuestionResponse, QuestionResponseSortCriterion>(1,
                QuestionResponseSortCriterion.ID.name(), QuestionResponseSortCriterion.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected QuestionResponseServiceLocal getService() {
        return questionResponseService;
    }
}
