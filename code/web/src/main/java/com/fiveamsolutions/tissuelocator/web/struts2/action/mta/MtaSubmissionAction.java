/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action.mta;

import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
public class MtaSubmissionAction extends MtaInformationAction {

    private static final long serialVersionUID = 6167004619732713297L;

    /**
     * Default constructor.
     */
    public MtaSubmissionAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Injected constructor.
     * 
     * @param userService user service
     */
    @Inject
    public MtaSubmissionAction(TissueLocatorUserServiceLocal userService) {
        super(userService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        getSignedMta().setReceivingInstitution(getSignedMta().getUploader().getInstitution());
        getSignedMta().setSendingInstitution(null);
        getSignedMta().setStatus(SignedMaterialTransferAgreementStatus.PENDING_REVIEW);
    }

    /**
     * action to save an institution.
     * @return the forward to go to.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "signedMta",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "signedMta")
                })
            },
            fieldExpressions = {
                @FieldExpressionValidator(fieldName = "document", key = "signedMta.document.required",
                    expression = "document != null || signedMta.document != null")
            }
        )
    public String saveMta() {
        TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService()
                .saveSignedMaterialTransferAgreement(getSignedMta());
        addActionMessage(getText("signedMta.success"));
        setSignedMta(new SignedMaterialTransferAgreement());
        return list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate() {
        super.validate();
        if (hasErrors()) {
            list();
        }
    }
}
