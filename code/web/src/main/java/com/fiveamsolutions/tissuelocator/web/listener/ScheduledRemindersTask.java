/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.listener;

import java.util.TimerTask;

import com.fiveamsolutions.tissuelocator.service.reminder.ScheduledReminderServiceLocal;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Timer task to control sending reminder emails.
 * @author smiller
 */
public class ScheduledRemindersTask extends TimerTask {

    private final int shipmentReceiptGracePeriod;
    private final int lineItemReviewGracePeriod;
    private final int supportLetterRequestReviewGracePeriod;
    private final int questionResponseGracePeriod;
    @Inject
    private static Provider<ScheduledReminderServiceLocal> scheduledReminderServiceProvider;


    /**
     * Constructor.
     * @param shipmentReceiptGracePeriod the number of days before an email reminder
     * should be sent reminding people to mark the shipment as received. 0 means never send a reminder.
     * @param lineItemReviewGracePeriod number of days before a reminder should be sent to lineItem
     * reviewers. 0 means never send a reminder.
     * @param supportLetterRequestReviewGracePeriod number of days before a reminder should be sent to
     * support letter request reviewers. 0 means never send a reminder.
     * @param questionResponseGracePeriod number of days before a reminder should be sent to
     * support question responders. 0 means never send a reminder.
     */
    public ScheduledRemindersTask(int shipmentReceiptGracePeriod, int lineItemReviewGracePeriod,
            int supportLetterRequestReviewGracePeriod, int questionResponseGracePeriod) {
        this.shipmentReceiptGracePeriod = shipmentReceiptGracePeriod;
        this.lineItemReviewGracePeriod = lineItemReviewGracePeriod;
        this.supportLetterRequestReviewGracePeriod = supportLetterRequestReviewGracePeriod;
        this.questionResponseGracePeriod = questionResponseGracePeriod;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        scheduledReminderServiceProvider.get().sendReminderEmails(shipmentReceiptGracePeriod,
                lineItemReviewGracePeriod, supportLetterRequestReviewGracePeriod, questionResponseGracePeriod);
    }

    /**
     * @param provider the application setting service provider to set
     */
    @Inject
    public void setScheduledReminderServiceProvider(Provider<ScheduledReminderServiceLocal> provider) {
        scheduledReminderServiceProvider = provider;
    }
}