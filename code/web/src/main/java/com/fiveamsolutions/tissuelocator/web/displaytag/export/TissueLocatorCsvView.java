/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.displaytag.export;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.displaytag.exception.DecoratorException;
import org.displaytag.exception.ObjectLookupException;
import org.displaytag.export.TextExportView;
import org.displaytag.model.Column;
import org.displaytag.model.ColumnIterator;
import org.displaytag.model.HeaderCell;
import org.displaytag.model.Row;
import org.displaytag.model.RowIterator;
import org.displaytag.model.TableModel;

import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorRequestHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractPersistentObjectAction.PaginatedSearchHelper;

/**
 * This class is largely copied from org.displaytag.export.BaseExportView and
 * org.displaytag.export.CsvView, with the only difference being the doExport() method.
 * Displaytag enables the use of custom export handlers, but the functionality we
 * need to override is not accessible by subclassing existing classes, hence the copying.
 *
 * @author gvaughn
 *
 */

@SuppressWarnings("PMD")
public class TissueLocatorCsvView implements TextExportView {

    private static final int BUFFER_SIZE = 1000;
    private static final int MAX_EXPORT_ROWS = 1048576; //Max # of rows allowed in Excel 2007

    /**
     * TableModel to render.
     */
    private TableModel model;

    /**
     * export full list?
     */
    private boolean exportFull;

    /**
     * include header in export?
     */
    private boolean header;

    /**
     * decorate export?
     */
    private boolean decorated;

    /**
     *
     * {@inheritDoc}
     */
    public void setParameters(TableModel tableModel, boolean exportFullList,
            boolean includeHeader, boolean decorateValues) {
        model = tableModel;
        exportFull = exportFullList;
        header = includeHeader;
        decorated = decorateValues;
    }

    /**
     * Write table header.
     *
     * @return String rendered header
     */
    @SuppressWarnings("rawtypes")
    protected String doHeaders() {

        final String rowEnd = getRowEnd();
        final String cellEnd = getCellEnd();

        StringBuffer buffer = new StringBuffer(BUFFER_SIZE);

        Iterator iterator = model.getHeaderCellList().iterator();

        while (iterator.hasNext()) {
            HeaderCell headerCell = (HeaderCell) iterator.next();

            String columnHeader = headerCell.getTitle();

            columnHeader = escapeColumnValue(columnHeader);

            buffer.append(columnHeader);

            if (iterator.hasNext()) {
                buffer.append(cellEnd);
            }
        }

        buffer.append(rowEnd);

        return buffer.toString();

    }

    /**
     * Writes table values to a csv file. Large search result sets are retrieved
     * incrementally to prevent memory errors and to mitigate performance problems.
     * @param out writer
     * @throws IOException on error
     * @throws JspException on error
     */
    @SuppressWarnings("rawtypes")
    public void doExport(Writer out) throws IOException, JspException {

        if (header) {
            write(out, doHeaders());
        }

        // get the correct iterator (full or partial list according to the
        // exportFull field)
        RowIterator rowIterator = model.getRowIterator(exportFull);
        doRowIteration(rowIterator, out);

        if (exportFull) {
            PaginatedSearchHelper helper = TissueLocatorRequestHelper
                .getPaginatedSearchHelper(ServletActionContext.getRequest());
            int rowNumber = model.getRowListFull().size() + 1;
            while (helper != null && helper.hasMoreResults() && rowNumber < MAX_EXPORT_ROWS) {
                List results = helper.getNextResults();
                model.getRowListFull().clear();
                Iterator iter = results.iterator();
                while (iter.hasNext() && rowNumber < MAX_EXPORT_ROWS) {
                    model.addRow(new Row(iter.next(), rowNumber++));
                }
                doRowIteration(model.getRowIterator(exportFull), out);
                clearResults();
            }
        }
    }

    private void doRowIteration(RowIterator rowIterator, Writer out) throws ObjectLookupException,
        DecoratorException, IOException {

        final String rowEnd = getRowEnd();
        final String cellEnd = getCellEnd();

        // iterator on rows
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            if (model.getTableDecorator() != null) {

                String stringStartRow = model.getTableDecorator()
                        .startRow();
                write(out, stringStartRow);
            }

            // iterator on columns
            ColumnIterator columnIterator = row.getColumnIterator(model
                    .getHeaderCellList());

            while (columnIterator.hasNext()) {
                Column column = columnIterator.nextColumn();

                // Get the value to be displayed for the column
                String value = escapeColumnValue(column
                        .getValue(decorated));

               write(out, value);

                if (columnIterator.hasNext()) {
                    write(out, cellEnd);
                }

            }
            write(out, rowEnd);
        }
    }

    /**
     * Performs any clean-up work necessary to limit memory usage.
     */
    protected void clearResults() {
        TissueLocatorHibernateUtil.getCurrentSession().clear();
    }

    /**
     * Write a String, checking for nulls value.
     *
     * @param out
     *            output writer
     * @param string
     *            String to be written
     * @throws IOException
     *             thrown by out.write
     */
    private void write(Writer out, String string) throws IOException {
        if (string != null) {
            out.write(string);
        }
    }

    /**
     *
     * @return return
     */
    public boolean outputPage() {
        return false;
    }

    /**
     *
     * @return return
     */
    protected String getRowEnd() {
        return "\n"; //$NON-NLS-1$
    }

    /**
     *
     * @return return
     */
    protected String getCellEnd() {
        return ","; //$NON-NLS-1$
    }

    /**
     *
     * {@inheritDoc}
     */
    public String getMimeType() {
        return "text/csv"; //$NON-NLS-1$
    }

    /**
     *
     * @param value value
     * @return return val
     */
    protected String escapeColumnValue(Object value) {
        String stringValue = StringUtils.trim(value.toString());
        if (!StringUtils.containsNone(stringValue, new char[] {'\n', ','})) {
            return "\"" + //$NON-NLS-1$
                    StringUtils.replace(stringValue, "\"", "\\\"") + "\""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        }

        return stringValue;
    }


}
