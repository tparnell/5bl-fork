/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.EJBException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.code.FundingSource;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableUtil;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceBean;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.CodeSortCriterion;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractSpecimenRequestAction;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author vsemenov
 *
 */
@SuppressWarnings({"PMD.AvoidDuplicateLiterals", "PMD.TooManyMethods" })
public abstract class AbstractCartSpecimenAction extends AbstractSpecimenRequestAction {

    private static final long serialVersionUID = 1163885603497231856L;

    private static final String HIBERNATE = "hibernate";
    private static final String RESOURCE_KEY_BASE = "resourceKeyBase";
    private static final String CONDITIONAL_EXPRESSION = "conditionalExpression";
    private static final String OBJECT_FIELD_NAME = "object";

    private SpecimenRequest originalRequest;
    private String investigatorOrganizationName;
    private String recipientOrganizationName;
    private String billingRecipientOrganizationName;
    private String specimenRequestError;
    private List<FundingSource> fundingSources;
    private boolean displayRequestProcessSteps;
    private boolean includeBillingRecipient;
    private boolean copyinvestigator;
    private boolean copyrecipient;


    /**
     * Default constructor.
     */
    public AbstractCartSpecimenAction() {
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category serivce.
     */
    @Inject
    public AbstractCartSpecimenAction(ApplicationSettingServiceLocal appSettingService,
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
        super(appSettingService, uiDynamicFieldCategoryService);
        setObject(new SpecimenRequest());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        syncObjectAndSession();
        setCopyinvestigator(getObject().getId() == null);
        setCopyrecipient(getObject().getId() == null);
        initLineItems();
        getObject().setUpdatedDate(new Date());
        prepareStatus();
        if (getObject().getInvestigator().getOrganization() != null) {
            setInvestigatorOrganizationName(getObject().getInvestigator().getOrganization().getName());
        }
        if (getObject().getShipment().getRecipient().getOrganization() != null) {
            setRecipientOrganizationName(getObject().getShipment().getRecipient().getOrganization().getName());
        }
        if (getObject().getShipment().getBillingRecipient() != null
                && getObject().getShipment().getBillingRecipient().getOrganization() != null) {
            setBillingRecipientOrganizationName(getObject().getShipment()
                    .getBillingRecipient().getOrganization().getName());
        }
        CodeServiceLocal csl = TissueLocatorRegistry.getServiceLocator().getCodeService();
        setFundingSources(csl.getActiveCodes(FundingSource.class, CodeSortCriterion.ID));

        setDisplayRequestProcessSteps(getAppSettingService().isDisplayRequestProcessSteps());
        setIncludeBillingRecipient(getObject().getShipment().getBillingRecipient() != null);
    }

    /**
     * Modifies the status of the request depending on how it was submitted.
     */
    private void prepareStatus() {
        if (getText("specimenRequest.edit.saveDraft").equals(getSubmit())
                && getObject().getStatus() != RequestStatus.PENDING_REVISION) {
            getObject().setStatus(RequestStatus.DRAFT);
        }
        // remove the draft status if it is being edited
        if (getText("specimenRequest.edit.continue").equals(getSubmit())
                && getObject().getStatus() == RequestStatus.DRAFT) {
            getObject().setStatus(null);
        }
    }

    /**
     * action to decide between saveDraft or Confirm action.
     *
     * @return the forward to go to.
     */
    @Validations(customValidators = {
            @CustomValidator(type = HIBERNATE, fieldName = OBJECT_FIELD_NAME, parameters = {
                    @ValidationParameter(name = RESOURCE_KEY_BASE, value = "specimenRequest"),
                    @ValidationParameter(name = "excludes", value = "status, mtaCertification"),
                    @ValidationParameter(name = CONDITIONAL_EXPRESSION,
                            value = "(object.study.irbApprovalStatus == null "
                                + "|| object.study.irbApprovalStatus.toString() eq 'APPROVED'.toString())"
                                + "&& includeBillingRecipient") }),
            @CustomValidator(type = HIBERNATE, fieldName = OBJECT_FIELD_NAME, parameters = {
                    @ValidationParameter(name = RESOURCE_KEY_BASE, value = "specimenRequest"),
                    @ValidationParameter(name = "excludes", value = "study.irbApprovalExpirationDate, "
                            + "status, mtaCertification"),
                    @ValidationParameter(name = CONDITIONAL_EXPRESSION,
                            value = "(object.study.irbApprovalStatus != null "
                                + " && !(object.study.irbApprovalStatus.toString() eq 'APPROVED'.toString()))"
                                + " && includeBillingRecipient") }),
            @CustomValidator(type = HIBERNATE, fieldName = OBJECT_FIELD_NAME, parameters = {
                    @ValidationParameter(name = RESOURCE_KEY_BASE, value = "specimenRequest"),
                    @ValidationParameter(name = "excludes", value = "status, mtaCertification, "
                                + "billingRecipient.address.line1, "
                                + "billingRecipient.address.city, "
                                + "billingRecipient.address.state, billingRecipient.address.zip, "
                                + "billingRecipient.address.country, billingRecipient.address.phone, "
                                + "billingRecipient.firstName, billingRecipient.lastName, "
                                + "billingRecipient.email, billingRecipient.organization"),
                    @ValidationParameter(name = CONDITIONAL_EXPRESSION,
                                         value = "!includeBillingRecipient && "
                                + "(object.study.irbApprovalStatus == null "
                                + "|| object.study.irbApprovalStatus.toString() eq 'APPROVED'.toString())") }),
            @CustomValidator(type = HIBERNATE, fieldName = OBJECT_FIELD_NAME, parameters = {
                    @ValidationParameter(name = RESOURCE_KEY_BASE, value = "specimenRequest"),
                    @ValidationParameter(name = "excludes", value = "study.irbApprovalExpirationDate, "
                                + "status, mtaCertification, billingRecipient.address.line1, "
                                + "billingRecipient.address.city, "
                                + "billingRecipient.address.state, billingRecipient.address.zip, "
                                + "billingRecipient.address.country, billingRecipient.address.phone, "
                                + "billingRecipient.firstName, billingRecipient.lastName, "
                                + "billingRecipient.email, billingRecipient.organization"),
                   @ValidationParameter(name = CONDITIONAL_EXPRESSION, value = "!includeBillingRecipient && "
                                + "(object.study.irbApprovalStatus != null "
                                + " && !(object.study.irbApprovalStatus.toString() eq 'APPROVED'.toString()))") }) },
                            fieldExpressions = {
            @FieldExpressionValidator(fieldName = "resume", key = "specimenRequest.investigator.resume.required",
                    expression = "(object.status != null && object.status.toString() eq 'DRAFT'.toString())"
                    + " || resume != null || object.investigator.resume != null"),
            @FieldExpressionValidator(fieldName = "investigatorOrganizationName",
                    key = "specimenRequest.investigator.organization.required",
                    expression = "(object.status != null && object.status.toString() eq 'DRAFT'.toString())"
                    + " || investigatorOrganizationName == object.investigator.organization.name"),
            @FieldExpressionValidator(fieldName = "recipientOrganizationName",
                    key = "specimenRequest.shipment.recipient.organization.required",
                    expression = "(object.status != null && object.status.toString() eq 'DRAFT'.toString())"
                    + " || recipientOrganizationName == object.shipment.recipient.organization.name"),
            @FieldExpressionValidator(fieldName = "billingRecipientOrganizationName",
                    key = "specimenRequest.shipment.billingRecipient.organization.required",
                    expression = "(includeBillingRecipient == false)"
                    + " || (object.status != null && object.status.toString() eq 'DRAFT'.toString())"
                    + " || (billingRecipientOrganizationName == object.shipment.billingRecipient.organization.name)") })
    public String saveDraftOrConfirm() {
        if (!isIncludeBillingRecipient()) {
            getObject().getShipment().setBillingRecipient(null);
        }
        if (getText("specimenRequest.edit.saveDraft").equals(getSubmit())) {
            return saveDraft();
        } else {
            return confirm();
        }
    }

    /**
     * saves a draft request.
     *
     * @return - "input"
     */
    protected String saveDraft() {
        getService().saveDraftRequest(getObject());

        Long requestId = getObject().getId();
        Long requestorId = getObject().getRequestor().getId();
        String contextPath = ServletActionContext.getRequest().getContextPath();
        String[] args = new String[] {contextPath, Long.toString(requestorId), Long.toString(requestId)};
        addActionMessage(getText("specimenRequest.edit.saveDraft.message", args));
        TissueLocatorSessionHelper.setCart(ServletActionContext.getRequest().getSession(), getObject());
        return "input";
    }

    /**
     * syncs object and session.
     */
    protected void syncObjectAndSession() {
        originalRequest = TissueLocatorSessionHelper.getCart(ServletActionContext.getRequest().getSession());
        if (getObject().getId() != null
                && (originalRequest.getId() == null || !getObject().getId().equals(
                originalRequest.getId()))) {
            TissueLocatorSessionHelper.setCart(ServletActionContext.getRequest().getSession(), getObject());
        } else if (TissueLocatorSessionHelper.getCart(ServletActionContext.getRequest().getSession()) != null) {
            setObject(originalRequest);
            DisableableUtil.setValidationDisabled(RequestStatus.DRAFT.equals(originalRequest.getStatus()));
        }
    }

    /**
     * Action for viewing a specimen.
     *
     * @return a struts forward.
     */
    @SkipValidation
    public String view() {
        if (originalRequest != null) {
            TissueLocatorSessionHelper.setCart(ServletActionContext.getRequest().getSession(),
                    originalRequest);
        }
        return "view";
    }

    /**
     * action for the cart.
     * @return a struts forward
     */
    @SkipValidation
    public String viewCart() {
        if (isDisplayAggregateSearchResults()) {
            return "aggregateCart";
        }
        return "cart";
    }

    /**
     * @return the investigatorOrganizationName
     */
    public String getInvestigatorOrganizationName() {
        return investigatorOrganizationName;
    }

    /**
     * @param investigatorOrganizationName the investigatorOrganizationName to set
     */
    public void setInvestigatorOrganizationName(String investigatorOrganizationName) {
        this.investigatorOrganizationName = investigatorOrganizationName;
    }

    /**
     * @return the recipientOrganizationName
     */
    public String getRecipientOrganizationName() {
        return recipientOrganizationName;
    }

    /**
     * @param recipientOrganizationName the recipientOrganizationName to set
     */
    public void setRecipientOrganizationName(String recipientOrganizationName) {
        this.recipientOrganizationName = recipientOrganizationName;
    }

    /**
     * Get the ids of the specimens in the cart.
     * @return the set of specimens
     */
    @SuppressWarnings("unchecked")
    public Collection<Long> getSpecimenIdsInCart() {
        return CollectionUtils.collect(getObject().getLineItems(), new Transformer() {
            /**
             * {@inheritDoc}
             */
            public Object transform(Object input) {
                SpecimenRequestLineItem li = (SpecimenRequestLineItem) input;
                return li.getSpecimen().getId();
            }
        });
    }

    /**
     * Delete a specimen request.
     * @return struts forward.
     */
    @SkipValidation
    public String delete() {
        try {
            Long requestId = getObject().getId();
            getService().deleteRequest(getObject());
            setObject(null);
            TissueLocatorSessionHelper.setCart(ServletActionContext.getRequest().getSession(), null);
            addActionMessage(getText("specimenRequest.delete.message", new String[]{requestId.toString()}));
        } catch (EJBException e) {
            if (e.getCause() != null
                    && e.getCause().getMessage().equals(SpecimenRequestServiceBean.CURRENTLY_NOT_DELETABLE)) {
                setSpecimenRequestError("specimenRequest.error.delete");
                return "deleteSuccess";
            }
            throw e;
        }
        return "deleteSuccess";
    }

    /**
     * @return the specimenRequestError
     */
    public String getSpecimenRequestError() {
        return specimenRequestError;
    }

    /**
     * @param specimenRequestError the specimenRequestError to set
     */
    public void setSpecimenRequestError(String specimenRequestError) {
        this.specimenRequestError = specimenRequestError;
    }

    /**
     * @return the fundingSources
     */
    public List<FundingSource> getFundingSources() {
        return fundingSources;
    }

    /**
     * @param fundingSources the fundingSources to set
     */
    public void setFundingSources(List<FundingSource> fundingSources) {
        this.fundingSources = fundingSources;
    }


    /**
     * @return the ids of the funding sources for this request.
     */
    public List<Long> getFundingSourceIds() {
        List<Long> sourceIds = new ArrayList<Long>();
        for (FundingSource fs : getObject().getStudy().getFundingSources()) {
            sourceIds.add(fs.getId());
        }
        return sourceIds;
    }

    /**
     * @return the displayRequestProcessSteps
     */
    public boolean isDisplayRequestProcessSteps() {
        return displayRequestProcessSteps;
    }

    /**
     * @param displayRequestProcessSteps the displayRequestProcessSteps to set
     */
    public void setDisplayRequestProcessSteps(boolean displayRequestProcessSteps) {
        this.displayRequestProcessSteps = displayRequestProcessSteps;
    }

    /**
     * @return the includeBillingRecipient
     */
    public boolean isIncludeBillingRecipient() {
        return includeBillingRecipient;
    }

    /**
     * @param includeBillingRecipient the includeBillingRecipient to set
     */
    public void setIncludeBillingRecipient(boolean includeBillingRecipient) {
        this.includeBillingRecipient = includeBillingRecipient;
    }

    /**
     * @return the billingRecipientOrganizationName
     */
    public String getBillingRecipientOrganizationName() {
        return billingRecipientOrganizationName;
    }

    /**
     * @param billingRecipientOrganizationName the billingRecipientOrganizationName to set
     */
    public void setBillingRecipientOrganizationName(
            String billingRecipientOrganizationName) {
        this.billingRecipientOrganizationName = billingRecipientOrganizationName;
    }

    /**
     * @return the copyinvestigator
     */
    public boolean isCopyinvestigator() {
        return copyinvestigator;
    }

    /**
     * @param copyinvestigator the copyinvestigator to set
     */
    public void setCopyinvestigator(boolean copyinvestigator) {
        this.copyinvestigator = copyinvestigator;
    }

    /**
     * @return the copyrecipient
     */
    public boolean isCopyrecipient() {
        return copyrecipient;
    }

    /**
     * @param copyrecipient the copyrecipient to set
     */
    public void setCopyrecipient(boolean copyrecipient) {
        this.copyrecipient = copyrecipient;
    }
}
