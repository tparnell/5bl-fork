/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.config.search.DataType;
import com.fiveamsolutions.tissuelocator.data.config.search.DataType.DataRetriever;
import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorRequestHelper;
import com.fiveamsolutions.tissuelocator.web.util.SelectOption;
import com.google.inject.Inject;

/**
 * Base class for processing selectable value search fields.
 * @author gvaughn
 * @param <T> AbstractSearchFieldConfig type.
 */
public abstract class AbstractSelectSearchFieldWidgetAction<T extends AbstractSearchFieldConfig> extends
        AbstractSearchFieldWidgetAction<T> {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(AbstractSelectSearchFieldWidgetAction.class);
    private final List<SelectOption> options = new ArrayList<SelectOption>();

    /**
     * Default Constructor.
     */
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public AbstractSelectSearchFieldWidgetAction() {
        throw new UnsupportedOperationException();
    }
    
    /**
     * 
     * @param searchFieldConfigService search field config service.
     */
    @Inject
    public AbstractSelectSearchFieldWidgetAction(SearchFieldConfigServiceLocal searchFieldConfigService) {
        super(searchFieldConfigService);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        initPropertyList();
    }

    private void initPropertyList() {
        options.clear();
        addEmptyOptionIfNeeded();

        Class<?> dataClass;
        try {
            dataClass = Class.forName(getSelectConfig().getListClass());
            DataRetriever r = getSelectConfig().getListType().getDataRetriever();

            for (Object o : getSelectConfig().getListType().getData(dataClass)) {
                String label = r.getValueString(o);
                String value = label;
                if (getSelectConfig().getListType().equals(DataType.ENUM)) {
                    value = o.toString();
                } else if (getSelectConfig().getListType().equals(DataType.INSTITUTION)) {
                    value = ((Institution) o).getId().toString();
                }
                options.add(new SelectOption(null, label, value));
            }
        } catch (ClassNotFoundException e) {
            LOG.error("Error retrieving property data: ", e);
        }
    }

    private void addEmptyOptionIfNeeded() {
        if (getSelectConfig().isShowEmptyOption() && !getSelectConfig().isMultiSelect()) {
            options.add(new SelectOption(null, getText("search.emptyOption"), " "));
        }
    }

    /**
     * Return the list of select options.
     * @return the list.
     */
    public List<SelectOption> getSelectOptions() {
        return options;
    }

    /**
     * Return the select field configuration.
     * @return the select field configuration.
     */
    protected abstract SelectConfig getSelectConfig();

    /**
     * @return The value of the select config.
     */
    public Object getValue() {
        Map<AbstractSearchFieldConfig, Object> configValueMap =
            TissueLocatorRequestHelper.getSearchConfigValueMap(ServletActionContext.getRequest());
        Object result = configValueMap.get(getSelectConfig());
        return convertValueIfNeeded(result);
    }

    /**
     * convert the value if needed.
     * @param value the value to covert
     * @return the converted val
     */
    @SuppressWarnings("rawtypes")
    public Object convertValueIfNeeded(Object value) {
        if (value instanceof Collection) {
            List<String> results = new ArrayList<String>();
            for (Object cur : (Collection) value) {
                results.add(cur.toString());
            }
            return results;
        }
        if (value instanceof PersistentObject) {
            return ((PersistentObject) value).getId();
        }
        return value;
    }
}
