/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.google.inject.Inject;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author vsemenov
 *
 */
@SuppressWarnings({ "PMD.AvoidDuplicateLiterals", "PMD.TooManyMethods" })
public abstract class AbstractCartAction extends AbstractCartSpecimenAction {

    private static final long serialVersionUID = -4319336276559330091L;

    private Set<Long> selection = new HashSet<Long>();
    private AggregateSpecimenRequestLineItem[] aggregateSelections = new AggregateSpecimenRequestLineItem[0];

    /**
     * Default constructor.
     */
    public AbstractCartAction() {
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     */
    @Inject
    public AbstractCartAction(ApplicationSettingServiceLocal appSettingService,
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
        super(appSettingService, uiDynamicFieldCategoryService);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void prepare() {
        super.prepare();
        int aggregateSelectionCount = 0;
        Enumeration<String> paramNames = ServletActionContext.getRequest().getParameterNames();
        while (paramNames.hasMoreElements()) {
            String name = paramNames.nextElement();
            if (name.startsWith("aggregateSelections")) {
                String indexString = StringUtils.substringBefore(StringUtils.substringAfter(name, "["), "]");
                int index = Integer.valueOf(indexString);
                if (index > aggregateSelectionCount) {
                    aggregateSelectionCount = index;
                }
            }
        }
        setAggregateSelections(new AggregateSpecimenRequestLineItem[aggregateSelectionCount + 1]);
        for (int i = 0; i < aggregateSelections.length; i++) {
            aggregateSelections[i] = new AggregateSpecimenRequestLineItem();
        }
    }

    /**
     * action for adding specimens to the cart.
     * @return a struts forward
     */
    @SkipValidation
    public String addToCart() {
        int oldCount = getObject().getLineItems().size();
        Collection<Long> specimenIdsInCart = getSpecimenIdsInCart();
        for (Long currentId : selection) {
            addSpecimenToCart(specimenIdsInCart, currentId);
        }
        int newCount = getObject().getLineItems().size();
        initLineItems();
        addActionMessage(getText("cart.add.message", new String[] {String.valueOf(newCount - oldCount)}));
        return viewCart();
    }

    private void addSpecimenToCart(Collection<Long> specimenIdsInCart, Long specimenId) {
        if (!specimenIdsInCart.contains(specimenId)) {
            SpecimenRequestLineItem li = new SpecimenRequestLineItem();
            li.setSpecimen(TissueLocatorRegistry.getServiceLocator().getSpecimenService().
                    getPersistentObject(Specimen.class, specimenId));
            li.setQuantity(li.getSpecimen().getAvailableQuantity());
            li.setQuantityUnits(li.getSpecimen().getAvailableQuantityUnits());
            getObject().getLineItems().add(li);
        }
    }

    /**
     * action for adding specimens to the aggregate cart.
     * @return a struts forward
     */
    @SkipValidation
    public String addToAggregateCart() {
        int newCount = 0;
        for (AggregateSpecimenRequestLineItem aggregateSelection : getAggregateSelections()) {
            if (aggregateSelection.getQuantity() > 0) {
                boolean quantityUpdated = updateQuantity(aggregateSelection);
                if (!quantityUpdated) {
                    getObject().getAggregateLineItems().add(aggregateSelection);
                    newCount++;
                }
            }
        }
        initLineItems();
        addActionMessage(getText("aggregateCart.add.message", new String[] {String.valueOf(newCount)}));
        return viewCart();
    }

    private boolean updateQuantity(AggregateSpecimenRequestLineItem aggregateSelection) {
        for (AggregateSpecimenRequestLineItem lineItem : getObject().getAggregateLineItems()) {
            if (StringUtils.equals(aggregateSelection.getCriteria(), lineItem.getCriteria())
                    && ObjectUtils.equals(aggregateSelection.getInstitution(), lineItem.getInstitution())) {
                lineItem.setQuantity(aggregateSelection.getQuantity());
                return true;
            }
        }
        return false;
    }

    /**
     * Action for adding prospective collection data to the cart.
     * @return a struts forward.
     */
    @SkipValidation
    public String addProspectiveCollectionToCart() {
        addActionMessage(getText("cart.add.prospectiveCollection.message"));
        return viewCart();
    }

    /**
     * action for removing specimens from the cart.
     * @return a struts forward
     */
    @SkipValidation
    public String removeFromCart() {
        Set<SpecimenRequestLineItem> liToRemove = new HashSet<SpecimenRequestLineItem>();
        for (SpecimenRequestLineItem li : getObject().getLineItems()) {
            if (selection.contains(li.getSpecimen().getId())) {
                liToRemove.add(li);
            }
        }
        getObject().getLineItems().removeAll(liToRemove);
        addActionMessage(getText("cart.remove.message"));
        return viewCart();
    }

    /**
     * action for removing specimens from the cart.
     *
     * @return a struts forward
     */
    @SkipValidation
    public String removeFromSpecificAggregateCart() {
        return removeFromAggregateCart(getSpecificLineItemsArray());
    }

    /**
     * action for removing specimens from the cart.
     *
     * @return a struts forward
     */
    @SkipValidation
    public String removeFromGeneralAggregateCart() {
        return removeFromAggregateCart(getGeneralLineItemsArray());
    }

    /**
     * Action for removing specimens from the cart.
     * @param lineItemArray the appropriate array to remove from.
     * @return a struts forward
     */
    @SkipValidation
    protected String removeFromAggregateCart(AggregateSpecimenRequestLineItem[] lineItemArray) {
        int removeIndex = getSelection().iterator().next().intValue();
        getObject().getAggregateLineItems().remove(lineItemArray[removeIndex]);
        addActionMessage(getText("aggregateCart.remove.message"));
        generateSeparateArrays();
        return viewCart();
    }


    /**
     * Action for removing the prospective collection data from
     * the cart.
     * @return a struts forward.
     */
    @SkipValidation
    public String removeProspectiveCollectionFromCart() {
        removeProspectiveCollection(true);
        return viewCart();
    }

    private void removeProspectiveCollection(boolean alwaysRemove) {
        if (getObject().getProspectiveCollectionNotes() != null || alwaysRemove) {
            getObject().setProspectiveCollectionNotes(null);
            addActionMessage(getText("cart.remove.prospectiveCollection.message"));
        }
    }

    /**
     * action for emptying the cart.
     * @return a struts forward
     */
    @SkipValidation
    public String emptyCart() {
        int oldCount = getObject().getLineItems().size();
        getObject().getLineItems().clear();
        addActionMessage(getText("cart.empty.message", new String[] {String.valueOf(oldCount)}));
        removeProspectiveCollection(false);
        return viewCart();
    }

    /**
     * action for emptying the aggregate cart.
     * @return a struts forward
     */
    @SkipValidation
    public String emptyAggregateCart() {
        int oldCount = getObject().getAggregateLineItems().size();
        getObject().getAggregateLineItems().clear();
        addActionMessage(getText("aggregateCart.empty.message", new String[] {String.valueOf(oldCount)}));
        removeProspectiveCollection(false);
        generateSeparateArrays();
        return viewCart();
    }

    /**
     * Edit the current prospective collection data.
     * @return struts forward.
     */
    @SkipValidation
    public String editProspectiveCollection() {
        return "prospectiveCollection";
    }

    /**
     * action to update the cart.
     * @return a struts forward.
     */
    @Validations(
        customValidators = {
            @CustomValidator(type = "hibernate", fieldName = "lineItems",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "lineItems") })
        }
    )
    public String updateCart() {
        return updateCartHelper();
    }

    /**
     * action to update the cart.
     * @return a struts forward.
     */
    @Validations(
        customValidators = {
            @CustomValidator(type = "hibernate", fieldName = "aggregateLineItems",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "aggregateLineItems") })
        }
    )
    public String updateAggregateCart() {
        return updateCartHelper();
    }

    private String updateCartHelper() {
        if (getText("cart.update").equals(getSubmit())) {
            addActionMessage(getText("cart.update.success"));
            return Action.SUCCESS;
        } else if (getText("specimenRequest.edit.saveDraft").equals(getSubmit())) {
            saveDraft();
            return Action.SUCCESS;
        } else if (getObject().getStatus() == null || getObject().getStatus() == RequestStatus.PENDING_REVISION
                || getObject().getStatus() == RequestStatus.DRAFT) {
            return "request";
        } else {
            throw new IllegalArgumentException("Selected specimen request is not editable");
        }
    }

    /**
     * @return the selection
     */
    public Set<Long> getSelection() {
        return selection;
    }

    /**
     * @param selection the selection to set
     */
    public void setSelection(Set<Long> selection) {
        this.selection = selection;
    }

    /**
     * @return the aggregateSelections
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public AggregateSpecimenRequestLineItem[] getAggregateSelections() {
        return aggregateSelections;
    }

    /**
     * @param aggregateSelections the aggregateSelections to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setAggregateSelections(AggregateSpecimenRequestLineItem[] aggregateSelections) {
        this.aggregateSelections = aggregateSelections;
    }
}
