/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.interceptor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * Derived from org.apache.struts2.interceptor.CheckboxInterceptor, with the additional
 * ability to ignore specified checkbox parameters.
 * 
 * From CheckboxInterceptor:
 * "Looks for a hidden identification field that specifies the original value of the checkbox.
 * If the checkbox isn't submitted, insert it into the parameters as if it was with the value
 * of 'false'."
 * 
 * Any single-valued checkbox property that has not been checked will be set to 'false', with
 * the exception of the specified excluded parameters. This is useful for, for example, excluding
 * checkbox parameters associated with search results, where a single result (and a single checkbox)
 * may be submitted but unselected, in which case the value would be set to 'false' if not excluded.
 * 
 * 
 * @author gvaughn
 *
 */
public class TissueLocatorCheckboxInterceptor extends AbstractInterceptor {

    private static final long serialVersionUID = -3743445557164683751L;
    private static final String UNCHECKED_VALUE = Boolean.FALSE.toString();
    private static final String CHECKBOX_PREFIX = "__checkbox_";
    
    private Set<String> excludeParams = new HashSet<String>();
    
    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings({ "rawtypes", "unchecked", "PMD.SignatureDeclareThrowsException" })
    public String intercept(ActionInvocation invocation) throws Exception {
        Map parameters = invocation.getInvocationContext().getParameters();
        Map<String, String[]> newParams = new HashMap<String, String[]>();
        Set<Map.Entry> entries = parameters.entrySet();
        for (Iterator<Map.Entry> iterator = entries.iterator(); iterator.hasNext();) {
            Map.Entry entry = iterator.next();
            String key = (String) entry.getKey();

            if (key.startsWith(CHECKBOX_PREFIX)) {
                String name = key.substring(CHECKBOX_PREFIX.length());

                Object values = entry.getValue();
                iterator.remove();
                if (ignoreField(name, values, parameters)) {
                    continue;
                }
                
                newParams.put(name, new String[]{UNCHECKED_VALUE});
            }
        }

        parameters.putAll(newParams);

        return invocation.invoke();
    }
    
    @SuppressWarnings("rawtypes")
    private boolean ignoreField(String name, Object values, Map parameters) {
        if (excludeParams.contains(name)) {
            return true;
        }
        if (values instanceof String[] && ((String[]) values).length > 1) {
            return true;
        }
        // is this checkbox checked/submitted?
        return parameters.containsKey(name);
    }

    /**
     * Set the parameters to be excluded from this interceptor's check,
     * as a comma-delimited list of parameter names.
     * @param commaDelimitedParams a comma-delimited list of parameter names to be excluded.
     */
    public void setExcludeParams(String commaDelimitedParams) {
        excludeParams = new HashSet<String>();
        String trimmedString = StringUtils.trimToNull(commaDelimitedParams);
        if (trimmedString != null) {
            excludeParams.addAll(Arrays.asList(StringUtils
                    .stripAll(StringUtils.split(trimmedString, ','))));
        }
    }
}
