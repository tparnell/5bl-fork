/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.tissuelocator.data.LobHolder;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.google.inject.Inject;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * @author smiller
 *
 */
@Namespace("/protected")
@Results(value = {
        @Result(name = "success",
                type = "stream",
                params = { "contentType", "${contentType}",
                           "contentDisposition", "attachment; filename=${fileName}",
                           "inputName", "fileStream"
                })
})
public class FileDownloadAction extends ActionSupport implements Preparable {
    
    private static final long serialVersionUID = 1L;
    private LobHolder file;
    private String contentType;
    private String fileName;
    private final LobHolderServiceLocal lobHolderService;

    /**
     * @param lobHolderService Lob holder service.
     */
    @Inject
    public FileDownloadAction(LobHolderServiceLocal lobHolderService) {
        this.file = new LobHolder();
        this.lobHolderService = lobHolderService;
    }

    /**
     * {@inheritDoc}
     */
    public void prepare() {
        if (getFile().getId() != null) {
            setFile(lobHolderService.
                getLob(getFile().getId()));
        }
    }
    
    /**
     * action for downloading the file contents.
     * @return the struts forward.
     */
    @Action(value = "downloadFile")
    @SkipValidation
    public String downloadFile() {
        return SUCCESS;
    }
    
    /**
     * @return the input stream needed for file download.
     */
    public InputStream getFileStream() {
        return new ByteArrayInputStream(getFile().getData());
    }

    /**
     * @return the file
     */
    public LobHolder getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(LobHolder file) {
        this.file = file;
    }

    /**
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}