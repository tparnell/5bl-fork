/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSection;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceBean;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiSectionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.CodeSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.CollectionProtocolSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.ParticipantSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.UseCountedCodeSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.util.ExtendableEntitySection;
import com.fiveamsolutions.tissuelocator.web.util.SelectOption;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * Action for managing specimens.
 * @author smiller
 */
@SuppressWarnings({ "PMD.TooManyFields", "PMD.TooManyMethods", "PMD.ExcessiveClassLength" })
public class SpecimenAction extends AbstractDynamicExtensionAction<Specimen, SpecimenSortCriterion> {

    private static final long serialVersionUID = 1L;
    private static final String SHIPMENT_FORWARD = "shipment";
    private static final String REQUEST_FORWARD = "request";
    private static final String ATTRIBUTE_SUFFIX = "_base_params";

    private static final int MAX_DISEASE_COUNT = 100;
    private static final int MAX_PARTICIPANT_COUNT = 100;
    private static final int MAX_PROTOCOL_COUNT = 100;

    private final SpecimenSearchServiceLocal specimenSearchService;
    private final TissueLocatorUserServiceLocal userService;
    private final ApplicationSettingServiceLocal appSettingService;
    private List<Institution> institutions;
    private List<SpecimenType> specimenTypes;

    private String diseaseSelection;
    private String participantSelection;
    private String protocolSelection;

    private Long redirectId;
    private String normalCodeId;
    private boolean normalSample;

    private boolean displayCollectionProtocol;
    private CollectionProtocol defaultProtocol;
    private Map<Long, CollectionProtocol> defaultProtocolMap = new HashMap<Long, CollectionProtocol>();

    private boolean aggregateResults;
    private boolean filterByInstitution;

    private UiSectionServiceLocal uiSectionService;
    private List<UiSection> uiSections;
    
    private boolean tabbedSearchAndSectionedCategoriesViewsEnabled;

    /**
     * Default constructor.
     */
    public SpecimenAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     * @param specimenSearchService the search service.
     * @param userService the user service
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     * @param uiSectionService ui section service.
     */
    @Inject
    public SpecimenAction(SpecimenSearchServiceLocal specimenSearchService,
            TissueLocatorUserServiceLocal userService, ApplicationSettingServiceLocal appSettingService, 
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService, UiSectionServiceLocal uiSectionService) {
        super(uiDynamicFieldCategoryService);
        this.specimenSearchService = specimenSearchService;
        this.userService = userService;
        this.appSettingService = appSettingService;
        this.uiSectionService = uiSectionService;
        Specimen s = new Specimen();
        setObject(s);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String input() {
        defaultExternalIdField();
        defaultCollectionProtocol();
        setDiseaseName();
        setParticipantName();
        setProtocolName();
        return super.input();
    }

    private void defaultExternalIdField() {
        if (getObject() != null && getObject().getExternalIdAssigner() == null
                && institutions != null && !institutions.isEmpty()) {
            getObject().setExternalIdAssigner(institutions.get(0));
        }
    }

    private void defaultCollectionProtocol() {
        CollectionProtocolServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();
        TissueLocatorUser user = userService.getByUsername(UsernameHolder.getUser());
        if (!user.isCrossInstitution()) {
            setDefaultProtocol(service.getDefaultProtocol(user.getInstitution().getId()));
            if (getObject() != null && getObject().getProtocol() == null) {
                getObject().setProtocol(getDefaultProtocol());
            }
        } else {
            setDefaultProtocolMap(service.getDefaultProtocols());
        }
    }

    private void setDiseaseName() {
        if (getObject() != null && getObject().getPathologicalCharacteristic() != null) {
            setDiseaseSelection(getObject().getPathologicalCharacteristic().getName());
        }
    }

    private void setParticipantName() {
        if (getObject() != null && getObject().getParticipant() != null) {
            setParticipantSelection(getObject().getParticipant().getExternalId());
        }
    }

    private void setProtocolName() {
        if (getObject() != null && getObject().getProtocol() != null) {
            setProtocolSelection(getObject().getProtocol().getName());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SpecimenSearchServiceLocal getSearchService() {
        return specimenSearchService;
    }

    /**
     * Action for viewing a specimen.
     * @return a struts forward.
     */
    @SkipValidation
    public String view() {
        return "view";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();

        institutions =
            TissueLocatorRegistry.getServiceLocator().getInstitutionService().getAll(Institution.class);

        CodeServiceLocal codeService = TissueLocatorRegistry.getServiceLocator().getCodeService();
        specimenTypes = codeService.getActiveCodes(SpecimenType.class);

        normalCodeId = getSpecimenKeyByName(getText("specimen.search.normal.name"));
        if (getObject() != null && getObject().getPathologicalCharacteristic() != null
                && getObject().getPathologicalCharacteristic().getId() != null) {
            setNormalSample(ObjectUtils.equals(getObject().getPathologicalCharacteristic().getId().toString(),
                    getNormalSampleId()));
        }

        setDisplayCollectionProtocol(appSettingService.isDisplayCollectionProtocol());
        setAggregateResults(StringUtils.isNotBlank(appSettingService.getSpecimenGrouping()));
        setIsTabbedSearchAndSectionedCategoriesViewsEnabled(
                getAppSettingService().isTabbedSearchAndSectionedCategoriesViewsEnabled());
        initializeUiSections();
    }

    /**
     * action to save an specimen.
     * @return the forward to go to.
     */
    @Validations(
            customValidators = { @CustomValidator(type = "hibernate", fieldName = "object" ,
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "specimen") })
            },
            fieldExpressions = {
                @FieldExpressionValidator(fieldName = "diseaseSelection",
                        key = "specimen.pathologicalCharacteristic.required",
                        expression = "diseaseSelection == object.pathologicalCharacteristic.name"),
                @FieldExpressionValidator(fieldName = "participantSelection",
                                key = "specimen.participant.required",
                                expression = "participantSelection == object.participant.externalId"),
                @FieldExpressionValidator(fieldName = "protocolSelection",
                                key = "specimen.protocol.required",
                                expression = "protocolSelection == object.protocol.name")
            }
    )
    public String save() {
        SpecimenServiceLocal service = getService();
        service.savePersistentObject(getObject());
        addActionMessage(getText("specimen.success"));
        return list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String list() {
        String attributeName = getClass().getSimpleName() + ATTRIBUTE_SUFFIX;
        HttpSession session = ServletActionContext.getRequest().getSession();
        Boolean instFilter = (Boolean) session.getAttribute(attributeName);
        if (instFilter != null) {
            setFilterByInstitution(instFilter.booleanValue());
        }
        return super.list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String filter() {
        String attributeName = getClass().getSimpleName() + ATTRIBUTE_SUFFIX;
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.setAttribute(attributeName, Boolean.valueOf(isFilterByInstitution()));
        Institution userInst = TissueLocatorSessionHelper.getLoggedInUser(session).getInstitution();
        getObject().setExternalIdAssigner(isFilterByInstitution() ? userInst : null);
        return super.filter();
    }

    /**
     * action to redirect to either shipment or request depending on role.
     * @return the forward to go to
     */
    @SkipValidation
    public String moreDetails() {
        Shipment shipment =
            TissueLocatorRegistry.getServiceLocator().getShipmentService().getShipmentWithSpecimen(getObject());
        if (shipment != null) {
            setRedirectId(shipment.getId());
            return SHIPMENT_FORWARD;
        } else {
            SpecimenRequest request = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService()
                                         .getRequestWithSpecimen(getObject());
            setRedirectId(request.getId());
            return REQUEST_FORWARD;
        }
    }

    /**
     * Withdraw consent for the current specimen.
     * @return struts forward.
     */
    @SkipValidation
    public String withdrawConsent() {
        try {
            getService().withdrawConsent(getObject());
        } catch (EJBException e) {
            if (e.getCause().getMessage().equals(SpecimenServiceBean.CONSENT_NOT_WITHDRAWABLE)) {
                addActionError(getText("specimen.error.consentNotWithdrawable"));
                return SUCCESS;
            }
            throw e;
        }
        addActionMessage(getText("specimen.consentWithdrawn.success"));
        return SUCCESS;
    }

    /**
     * Mark the specimen as returned to the source institution.
     * @return struts forward.
     */
    @SkipValidation
    public String returnToSourceInstitution() {
        try {
            getService().returnToSourceInstitution(getObject());
        } catch (EJBException e) {
            if (e.getCause().getMessage().equals(SpecimenServiceBean.SPECIMEN_NOT_RETURNABLE)) {
                addActionError(getText("specimen.error.notReturnable"));
                return SUCCESS;
            }
            throw e;
        }
        addActionMessage(getText("specimen.returnToSourceInstitution.success"));
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaginatedList<Specimen> getPaginatedList() {
        return new SortablePaginatedList<Specimen, SpecimenSortCriterion>(1,
                SpecimenSortCriterion.ID.name(), SpecimenSortCriterion.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SpecimenServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getSpecimenService();
    }

    /**
     * Action for the auto completer.
     * @return SUCCESS
     */
    @SkipValidation
    public String autocompleteDisease() {
        return SUCCESS;
    }

    /**
     * Action for the in-use auto completer.
     * @return SUCCESS
     */
    @SkipValidation
    public String autocompleteInUseDisease() {
        return SUCCESS;
    }

    /**
     * @return the institutions
     */
    public SelectOption[] getDiseaseMap() {
        TissueLocatorAnnotatedBeanSearchCriteria<Code> sc =
            new TissueLocatorAnnotatedBeanSearchCriteria<Code>(getDiseaseExample());
        return searchDiseases(sc);
    }

    /**
     * @return the institutions
     */
    public SelectOption[] getInUseDiseaseMap() {
        UseCountedCodeSearchCriteria sc =
            new UseCountedCodeSearchCriteria(getDiseaseExample(), 1L);
        SelectOption[] results = searchDiseases(sc);
        String normalName = getText("specimen.search.normal.name");
        if (getTerm() != null && !StringUtils.containsIgnoreCase(normalName, getTerm())) {
            return results;
        }
        if (!containsNormalSelectOption(results)) {
            SelectOption[] newResults = new SelectOption[results.length + 1];
            int normalIndex = 0;
            int arrayLength = results.length;
            int srcIndex = 0;
            int destIndex = 1;
            if (results.length > 0 && results[0].getId().equals(EMPTY_OPTION_ID)) {
                newResults[0] = results[0];
                normalIndex = 1;
                arrayLength = results.length - 1;
                srcIndex = 1;
                destIndex = 2;
            }
            newResults[normalIndex] = new SelectOption(getNormalSampleId(), normalName, normalName);
            System.arraycopy(results, srcIndex, newResults, destIndex, arrayLength);
            results = newResults;
        }
        return results;
    }

    private boolean containsNormalSelectOption(SelectOption[] options) {
        for (SelectOption option : options) {
            if (option.getId().equals(getNormalSampleId())) {
                return true;
            }
        }
        return false;
    }

    private AdditionalPathologicFinding getDiseaseExample() {
        AdditionalPathologicFinding example = new AdditionalPathologicFinding();
        example.setName(getTerm());
        example.setActive(true);
        return example;
    }

    private SelectOption[] searchDiseases(SearchCriteria<Code> sc) {
        CodeServiceLocal service = TissueLocatorRegistry.getServiceLocator().getCodeService();
        PageSortParams<Code> psp = new PageSortParams<Code>(MAX_DISEASE_COUNT, 0, CodeSortCriterion.NAME, false);
        List<Code> diseases = service.search(sc, psp);

        boolean additionalResults = diseases.size() == MAX_DISEASE_COUNT;
        int resultsLength = diseases.size() + (additionalResults ? 1 : 0);
        SelectOption[] results = new SelectOption[resultsLength];
        for (int index = 0; index < diseases.size(); index++) {
            Code current = diseases.get(index);
            results[additionalResults ? index + 1 : index] =
                new SelectOption(current.getId().toString(), current.getName(), current.getName());
        }
        if (additionalResults) {
            addAdditionalOptionsMessage(results);
        }
        return results;
    }

    /**
     * Action for the participant auto completer.
     * @return SUCCESS
     */
    @SkipValidation
    public String autocompleteParticipant() {
        return SUCCESS;
    }

    /**
     * @return the participants
     */
    public SelectOption[] getParticipantMap() {
        ParticipantServiceLocal service = TissueLocatorRegistry.getServiceLocator().getParticipantService();

        Institution institution = new Institution();
        institution.setName(getInstitutionSelection());

        Participant participant = new Participant();
        participant.setExternalId(getTerm());
        participant.setExternalIdAssigner(institution);

        SearchCriteria<Participant> sc = new TissueLocatorAnnotatedBeanSearchCriteria<Participant>(participant);
        PageSortParams<Participant> psp =
            new PageSortParams<Participant>(MAX_PARTICIPANT_COUNT, 0, ParticipantSortCriterion.EXTERNAL_ID, false);
        List<Participant> participants = service.search(sc, psp);

        boolean additionalResults = participants.size() == MAX_PARTICIPANT_COUNT;
        int resultsLength = participants.size() + (additionalResults ? 1 : 0);
        SelectOption[] results = new SelectOption[resultsLength];
        for (int index = 0; index < participants.size(); index++) {
            Participant current = participants.get(index);
            results[additionalResults ? index + 1 : index] = new SelectOption(current.getId().toString(),
                    current.getExternalId(), current.getExternalId());
        }
        if (additionalResults) {
            addAdditionalOptionsMessage(results);
        }
        return results;
    }

    /**
     * Action for the collection protocol auto completer.
     * @return SUCCESS
     */
    @SkipValidation
    public String autocompleteProtocol() {
        return SUCCESS;
    }

    /**
     * @return the participants
     */
    public SelectOption[] getProtocolMap() {
        CollectionProtocolServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();

        Institution institution = new Institution();
        institution.setName(getInstitutionSelection());

        CollectionProtocol protocol = new CollectionProtocol();
        protocol.setName(getTerm());
        protocol.setInstitution(institution);

        SearchCriteria<CollectionProtocol> sc =
            new TissueLocatorAnnotatedBeanSearchCriteria<CollectionProtocol>(protocol);
        PageSortParams<CollectionProtocol> psp =
            new PageSortParams<CollectionProtocol>(MAX_PROTOCOL_COUNT, 0, CollectionProtocolSortCriterion.NAME, false);
        List<CollectionProtocol> protocols = service.search(sc, psp);

        boolean additionalResults = protocols.size() == MAX_PROTOCOL_COUNT;
        int resultsLength = protocols.size() + (additionalResults ? 1 : 0);
        SelectOption[] results = new SelectOption[resultsLength];
        for (int index = 0; index < protocols.size(); index++) {
            CollectionProtocol current = protocols.get(index);
            results[additionalResults ? index + 1 : index] =
                new SelectOption(current.getId().toString(), current.getName(), current.getName());
        }
        if (additionalResults) {
            addAdditionalOptionsMessage(results);
        }
        return results;
    }

    /**
     * @return the institutions
     */
    public List<Institution> getInstitutions() {
        return institutions;
    }

    /**
     * @return the specimenTypes
     */
    public List<SpecimenType> getSpecimenTypes() {
        return specimenTypes;
    }

    /**
     * @return the diseaseSelection
     */
    public String getDiseaseSelection() {
        return diseaseSelection;
    }

    /**
     * @param diseaseSelection the diseaseSelection to set
     */
    public void setDiseaseSelection(String diseaseSelection) {
        this.diseaseSelection = diseaseSelection;
    }

    /**
     * @return the participantSelection
     */
    public String getParticipantSelection() {
        return participantSelection;
    }

    /**
     * @param participantSelection the participantSelection to set
     */
    public void setParticipantSelection(String participantSelection) {
        this.participantSelection = participantSelection;
    }

    /**
     * @return the protocolSelection
     */
    public String getProtocolSelection() {
        return protocolSelection;
    }

    /**
     * @param protocolSelection the protocolSelection to set
     */
    public void setProtocolSelection(String protocolSelection) {
        this.protocolSelection = protocolSelection;
    }

    /**
     * @return the id of the object that will be redirected to
     */
    public Long getRedirectId() {
        return redirectId;
    }

    /**
     * @param redirectId the id to set
     */
    public void setRedirectId(Long redirectId) {
        this.redirectId = redirectId;
    }


    /**
     * @return the list of legal statuses that the specimen can be put in to.
     */
    public List<SpecimenStatus> getLegalStatuses() {
        List<SpecimenStatus> statuses = new LinkedList<SpecimenStatus>();
        if (getObject() != null && getObject().getStatus() != null) {
            statuses = getObject().getStatus().geUserAssignableTransitions();
        } else {
            statuses.add(SpecimenStatus.AVAILABLE);
        }
        return statuses;
    }

    /**
     * This method is called in the prepare method.
     *
     * @param name the name of the specimen whose key is desired.
     * @return the Hibernate key of the specimen.
     */
    public String getSpecimenKeyByName(String name) {
        return TissueLocatorRegistry.getServiceLocator().getCodeService()
                .getCode(name, AdditionalPathologicFinding.class).getId().toString();
    }

    /**
     * @return the hibernate key of the 'normal' specimen
     */
    public String getNormalSampleId() {
        return normalCodeId;
    }

    /**
     * @return the normalSample
     */
    public boolean isNormalSample() {
        return normalSample;
    }

    /**
     * @param normalSample the normalSample to set
     */
    public void setNormalSample(boolean normalSample) {
        this.normalSample = normalSample;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Collection<SpecimenHeaderKey> getAllSections() {
        return Arrays.asList(SpecimenHeaderKey.values());
    }

    /**
     * @return whether the collection protocol should be displayed.
     */
    public boolean isDisplayCollectionProtocol() {
        return displayCollectionProtocol;
    }

    /**
     * @param displayCollectionProtocol whether the collection protocol should be displayed.
     */
    public void setDisplayCollectionProtocol(boolean displayCollectionProtocol) {
        this.displayCollectionProtocol = displayCollectionProtocol;
    }

    /**
     * @return the default collection protocol for the current specimen, or null if none exists.
     */
    public CollectionProtocol getDefaultProtocol() {
        return defaultProtocol;
    }

    /**
     * @param defaultProtocol the default collection protocol for the current specimen.
     */
    public void setDefaultProtocol(CollectionProtocol defaultProtocol) {
        this.defaultProtocol = defaultProtocol;
    }

    /**
     * @param defaultProtocolMap mapping of institution id to default collection protocol for the institution.
     */
    public void setDefaultProtocolMap(
            Map<Long, CollectionProtocol> defaultProtocolMap) {
        this.defaultProtocolMap = defaultProtocolMap;
    }

    /**
     * @return mapping of institution id to default collection protocol for the institution.
     */
    public Map<Long, CollectionProtocol> getDefaultProtocolMap() {
        return defaultProtocolMap;
    }

    /**
     * @return the aggregateResults
     */
    public boolean isAggregateResults() {
        return aggregateResults;
    }

    /**
     * @param aggregateResults the aggregateResults to set
     */
    public void setAggregateResults(boolean aggregateResults) {
        this.aggregateResults = aggregateResults;
    }

    /**
     * @return the filterByInstitution
     */
    public boolean isFilterByInstitution() {
        return filterByInstitution;
    }

    /**
     * @param filterByInstitution the filterByInstitution to set
     */
    public void setFilterByInstitution(boolean filterByInstitution) {
        this.filterByInstitution = filterByInstitution;
    }

    /**
     * Gets the user interface sections.
     *
     * @return list of user interface sections
     */
    public List<UiSection> getUiSections() {
        return uiSections;
    }

    /**
     * Returns whether tabbed view should be displayed on the specimen search page, and section categories on the
     * specimen view/edit pages.
     *
     * @return true if tabbed view should be displayed on the specimen search page, and section categories on the
     * specimen view/edit pages; false otherwise
     */
    public boolean isTabbedSearchAndSectionedCategoriesViewsEnabled() {
        return tabbedSearchAndSectionedCategoriesViewsEnabled;
    }

    /**
     * Enables or disables tabbed search and sectioned category views on the specimen search and view/edit pages.
     *
     * @param isTabbedSearchAndSectionedCategoriesViewsEnabled true if tabbed and sectioned category views should be
     * displayed
     */
    public void setIsTabbedSearchAndSectionedCategoriesViewsEnabled(
            boolean isTabbedSearchAndSectionedCategoriesViewsEnabled) {
        this.tabbedSearchAndSectionedCategoriesViewsEnabled = isTabbedSearchAndSectionedCategoriesViewsEnabled;
    }

    private void initializeUiSections() {
        uiSections = uiSectionService.getUiSections(getObjectType());
    }

    /**
     * @return the appSettingService
     */
    protected ApplicationSettingServiceLocal getAppSettingService() {
        return appSettingService;
    }

    /**
     * Keys of the specimen header properties defined in the application resources.
     */
    enum SpecimenHeaderKey implements ExtendableEntitySection {

        /**
         * Specimen characteristics header key.
         */
        BIOSPECIMEN_CHARACTERISTICS("specimen.biospecimenCharacteristics"),

        /**
         * Participant header key.
         */
        PARTICIPANT("specimen.participantHeader"),

        /**
         * Collection protocol header key.
         */
        COLLECTION_PROTOCOL("specimen.collectionProtocol");

        private String resourceKey;

        /**
         * Creates a specimen header key.
         *
         * @param resourceKey the resource key value
         */
        private SpecimenHeaderKey(String resourceKey) {
            this.resourceKey = resourceKey;
        }

        /**
         * Returns the value of the resource key.
         *
         * @return the resource key
         */
        public String getResourceKey() {
            return resourceKey;
        }
    }

}
