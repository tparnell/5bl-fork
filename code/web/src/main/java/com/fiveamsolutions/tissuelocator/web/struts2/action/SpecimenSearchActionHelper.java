/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.tissuelocator.data.SavedSpecimenSearch;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.AggregateSpecimenSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.RangeSearchCondition;
import com.fiveamsolutions.tissuelocator.service.search.SearchCondition;
import com.fiveamsolutions.tissuelocator.service.search.SearchParameterTypeConverter;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchCriteria;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorPropertyUtil;

/**
 * This class is used to share code between the specimen search action and the saved specimens earch action.
 * @author smiller
 */
public class SpecimenSearchActionHelper {

    private static final Logger LOG = Logger.getLogger(SpecimenSearchActionHelper.class);
    private final SearchParameterTypeConverter typeConverter;
    private final Map<Long, AbstractSearchFieldConfig> fieldConfigSearchConfigMap 
        = new HashMap<Long, AbstractSearchFieldConfig>();

    private static final String ATTRIBUTE_SUFFIX = "_params";

    // indexes for storing search in session
    private static final int MIN_QUANTITY_INDEX = 0;
    private static final int MULTI_SELECT_COLLECTION_PARAMS_INDEX = 1;
    private static final int MULTI_SELECT_PARAMS_INDEX = 2;
    private static final int RANGE_PARAMS_INDEX = 3;
    private static final int SELECTED_TAB_INDEX = 4;
    private static final int PARAMS_SIZE = 5;

    // data for search
    private final String attributeNameForParams;
    private boolean aggregateResults;
    private int minimumAggregateResultsDisplayed;
    private BigDecimal minimumAvailableQuantity;
    private Map<String, Collection<Object>> multiSelectParamMap = new HashMap<String, Collection<Object>>();
    private Map<String, Collection<Object>> multiSelectCollectionParamMap = new HashMap<String, Collection<Object>>();
    private Map<String, Object> rangeSearchParameters = new HashMap<String, Object>();
    private String selectedTab;


    /**
     * Constructor for the search action helper.
     * @param deFieldDefinitions the de field definitions to use
     * @param attributeDiscriminator the name to use as the start of the session attribute
     */
    public SpecimenSearchActionHelper(List<AbstractDynamicFieldDefinition> deFieldDefinitions,
            String attributeDiscriminator) {
        typeConverter = new SearchParameterTypeConverter(Specimen.class, deFieldDefinitions);
        attributeNameForParams = attributeDiscriminator + ATTRIBUTE_SUFFIX;
    }

    /**
     * Store the data in the helper in to the session so it will be the users next search.
     * @param object the specimen that is the 'example' in the search by example.
     */
    public void storeSearchParamsInSession(Specimen object) {
        Object[] params = new Object[PARAMS_SIZE];
        params[MIN_QUANTITY_INDEX] = getMinimumAvailableQuantity();
        params[MULTI_SELECT_COLLECTION_PARAMS_INDEX] = getMultiSelectCollectionParamMap(object);
        params[MULTI_SELECT_PARAMS_INDEX] = getMultiSelectParamMap(object);
        params[RANGE_PARAMS_INDEX] = getRangeSearchParameters();
        params[SELECTED_TAB_INDEX] = getSelectedTab();
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.setAttribute(attributeNameForParams, params);
    }

    /**
     * Remove the search params from the session.
     */
    public void removeSearchParamsFromSession() {
        ServletActionContext.getRequest().getSession().removeAttribute(attributeNameForParams);
    }

    /**
     * load the search params from the session.
     * @return true if any were found.
     */
    @SuppressWarnings("unchecked")
    public boolean loadSearchParamsFromSession() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        Object[] params = (Object[]) session.getAttribute(attributeNameForParams);
        if (params != null) {
            setMinimumAvailableQuantity((BigDecimal) params[MIN_QUANTITY_INDEX]);
            setMultiSelectCollectionParamMap((Map<String, Collection<Object>>) 
                    params[MULTI_SELECT_COLLECTION_PARAMS_INDEX]);
            setMultiSelectParamMap((Map<String, Collection<Object>>) params[MULTI_SELECT_PARAMS_INDEX]);
            setRangeSearchParameters((Map<String, Object>) params[RANGE_PARAMS_INDEX]);
            setSelectedTab((String) params[SELECTED_TAB_INDEX]);
            return true;
        }
        return false;
    }

    /**
     * @return the searchConfigs
     */
    public Collection<AbstractSearchFieldConfig> getSearchConfigs() {
        return fieldConfigSearchConfigMap.values();
    }

    /**
     * Generate the set of search conditions.
     * @return the conditions.
     */
    public Collection<SearchCondition> getSearchConditions() {
        Collection<SearchCondition> searchConditions = new ArrayList<SearchCondition>();
        for (AbstractSearchFieldConfig config : getSearchConfigs()) {
            String className = config.getSearchConditionClass();
            if (StringUtils.isNotBlank(className)) {
                SearchCondition searchCondition = createSearchCondition(className);
                searchCondition.setPropertyName(config.getSearchFieldName());
                searchCondition.setConditionParameters(getRangeSearchParameters());
                searchConditions.add(searchCondition);
            }
        }
        return searchConditions;
    }

    /**
     * Init the search configs.
     * @param type the type of the search
     * @param searchFieldConfigService search field config service.
     */
    public void initSearchConfigs(SearchSetType type, SearchFieldConfigServiceLocal searchFieldConfigService) {
        SearchConfig specimenSearchConfig = new SearchConfig(type, searchFieldConfigService);
        for (AbstractSearchFieldConfig config : specimenSearchConfig
                .getSearchFieldConfigs()) {
            fieldConfigSearchConfigMap.put(config.getFieldConfig().getId(), config);
        }
    }
    
    /**
     * Returns the search field configurations associated with the given category, ordered
     * according to the ordering of the categorized search fields.
     * @param category Search field category.
     * @return The search field configurations associated with the given category.
     */
    public List<AbstractSearchFieldConfig> getSearchFieldConfigs(UiSearchFieldCategory category) {
        List<AbstractSearchFieldConfig> configs = new ArrayList<AbstractSearchFieldConfig>();
        for (UiSearchCategoryField field : category.getUiSearchCategoryFields()) {
            if (fieldConfigSearchConfigMap.containsKey(field.getFieldConfig().getId())) {
                configs.add(fieldConfigSearchConfigMap.get(field.getFieldConfig().getId()));
            }            
        }
        return configs;
    }

    private SearchCondition createSearchCondition(String searchConditionClassName) {
        SearchCondition searchCondition = null;
        try {
            Class<?> searchConditionClass = Class.forName(searchConditionClassName);
            searchCondition = (SearchCondition) searchConditionClass.newInstance();
        } catch (Exception e) {
            LOG.error("Unable to create search condition class", e);
        }
        return searchCondition;
    }

    /**
     * create the search criteria.
     * @param object the object that is the example.
     * @return the criteria.
     */
    public SpecimenSearchCriteria getSearchCriteria(Specimen object) {
        if (isAggregateResults()) {
            AggregateSpecimenSearchCriteria criteria
                = new AggregateSpecimenSearchCriteria(object,
                    getMinimumAvailableQuantity(),
                    getMultiSelectCollectionParamMap(object),
                    getMultiSelectParamMap(object),
                    getSearchConditions());

            if (getMinimumAggregateResultsDisplayed() > 0) {
                criteria.setMinimumCountThreshold(getMinimumAggregateResultsDisplayed());
            }

            return criteria;
        } else {
            return new SpecimenSearchCriteria(object,
                    getMinimumAvailableQuantity(),
                    getMultiSelectCollectionParamMap(object),
                    getMultiSelectParamMap(object),
                    getSearchConditions());
        }
    }

    /**
     * @param object the specimen that is the 'example' in the search by example.
     * @return the multiSelectParamMap the map.
     */
    public Map<String, Collection<Object>> getMultiSelectParamMap(Specimen object) {
        convertMultiSelectParamValues(multiSelectParamMap, getMultiSelectParamMapConfigs(), object);
        return multiSelectParamMap;
    }
    
    private List<SelectConfig> getMultiSelectParamMapConfigs() {
        List<SelectConfig> selectConfigs = new ArrayList<SelectConfig>();
        for (AbstractSearchFieldConfig config : getSearchConfigs()) {
            if (isMultiSelectParamMapConfig(config)) {
                selectConfigs.add((SelectConfig) config);
            }
        }
        return selectConfigs;
    }
    
    private boolean isMultiSelectParamMapConfig(AbstractSearchFieldConfig config) {
        return config instanceof SelectConfig && ((SelectConfig) config).isMultiSelect() 
            && !((SelectConfig) config).isMultiSelectCollection();
    }
    
    private List<SelectConfig> getMultiSelectCollectionParamMapConfigs() {
        List<SelectConfig> selectConfigs = new ArrayList<SelectConfig>();
        for (AbstractSearchFieldConfig config : getSearchConfigs()) {
            if (isMultiSelectCollectionParamMapConfig(config)) {
                selectConfigs.add((SelectConfig) config);
            }
        }
        return selectConfigs;
    }
    
    private boolean isMultiSelectCollectionParamMapConfig(AbstractSearchFieldConfig config) {
        return config instanceof SelectConfig && ((SelectConfig) config).isMultiSelectCollection();
    }

    /**
     * @param multiSelectParamMap the multiSelectParamMap to set
     */
    public void setMultiSelectParamMap(Map<String, Collection<Object>> multiSelectParamMap) {
        this.multiSelectParamMap = multiSelectParamMap;
    }

    /**
     * @param object example specimen.
     * @return the multiSelectCollectionParamMap
     */
    public Map<String, Collection<Object>> getMultiSelectCollectionParamMap(Specimen object) {
        convertMultiSelectParamValues(multiSelectCollectionParamMap, getMultiSelectCollectionParamMapConfigs(), object);
        return multiSelectCollectionParamMap;
    }

    /**
     * @param multiSelectCollectionParamMap the multiSelectCollectionParamMap to set
     */
    public void setMultiSelectCollectionParamMap(
            Map<String, Collection<Object>> multiSelectCollectionParamMap) {
        this.multiSelectCollectionParamMap = multiSelectCollectionParamMap;
    }

    private void convertMultiSelectParamValues(Map<String, Collection<Object>> map, 
            List<SelectConfig> selectConfigs, Specimen object) {
        for (SelectConfig config : selectConfigs) {
            String key = config.getSearchFieldName();
            Collection<Object> values = new HashSet<Object>();
            Collection<Object> srcVals = map.get(key);
            if (srcVals != null) {
                values.addAll(config.getMultiSelectValueType().getValues(srcVals));
            }

            map.put(key, values);
            try {
                Object value = TissueLocatorPropertyUtil.getObjectProperty(object, key);
                if (value != null) {
                    map.get(key).add(value);
                }
            } catch (Exception e) {
                LOG.warn(e.getMessage());
            }
        }
    }
    
    /**
     * @return the aggregateResults
     */
    public boolean isAggregateResults() {
        return aggregateResults;
    }

    /**
     * @param aggregateResults the aggregateResults to set
     */
    public void setAggregateResults(boolean aggregateResults) {
        this.aggregateResults = aggregateResults;
    }

    /**
     * @return the minimumAggregateResultsDisplayed
     */
    public int getMinimumAggregateResultsDisplayed() {
        return minimumAggregateResultsDisplayed;
    }

    /**
     * @param minimumAggregateResultsDisplayed the minimumAggregateResultsDisplayed to set
     */
    public void setMinimumAggregateResultsDisplayed(int minimumAggregateResultsDisplayed) {
        this.minimumAggregateResultsDisplayed = minimumAggregateResultsDisplayed;
    }

    /**
     * @return the minimumAvailableQuantity
     */
    public BigDecimal getMinimumAvailableQuantity() {
        return minimumAvailableQuantity;
    }

    /**
     * @param minimumAvailableQuantity the minimumAvailableQuantity to set
     */
    public void setMinimumAvailableQuantity(BigDecimal minimumAvailableQuantity) {
        this.minimumAvailableQuantity = minimumAvailableQuantity;
    }

    /**
     * Gets the map of range search parameters.
     *
     * @return a map of range search parameters.
     */
    public Map<String, Object> getRangeSearchParameters() {
        for (Map.Entry<String, Object> entry : rangeSearchParameters.entrySet()) {
            if (entry.getValue() instanceof String[]) {
                Object value = ((String[]) entry.getValue())[0];
                String parameterName = entry.getKey().split(RangeSearchCondition.RANGE_PREFIX_REGEX)[1];
                try {
                    rangeSearchParameters.put(entry.getKey(), typeConverter.convertValue(parameterName, value));
                } catch (Exception e) {
                    LOG.error("An error occurred while converting range search parameters", e);
                }
            }
        }
        return rangeSearchParameters;
    }

    /**
     * Sets the map of range search parameters.
     *
     * @param rangeSearchParameters a map of range search parameters.
     */
    public void setRangeSearchParameters(Map<String, Object> rangeSearchParameters) {
        this.rangeSearchParameters = rangeSearchParameters;
    }

    /**
     * Gets the HTML anchor of the selected tab in the tabbed search view.
     *
     * @return HTML anchor or an empty string if no tab is selected
     */
    public String getSelectedTab() {
        return selectedTab;
    }

    /**
     * Sets the HTML anchor of the selected tab in the tabbed search view.
     *
     * @param selectedTab HTML anchor of the selected tab
     */
    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    /**
     * Gets the saved specimen search id from the session.
     *
     * @return the saved specimen search id stored in the session
     */
    public Long getSavedSpecimenSearchIdFromSession() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        return (Long) session.getAttribute(SavedSpecimenSearch.class.getSimpleName());
    }

    /**
     * Sets the saved specimen search id in the session.
     *
     * @param id the saved specimen search id to be stored
     */
    public void setSavedSpecimenSearchIdInSession(Long id) {
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.setAttribute(SavedSpecimenSearch.class.getSimpleName(), id);
    }
}