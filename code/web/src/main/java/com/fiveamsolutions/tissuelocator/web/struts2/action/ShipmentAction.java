/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.ShipmentStatus;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenReceiptQuality;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.CodeSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.ShipmentSortCriterion;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.util.AggregateLineItemSeparator;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorFileHelper;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
@SuppressWarnings({"PMD.TooManyMethods", "PMD.TooManyFields", "PMD.AvoidDuplicateLiterals",
    "PMD.ExcessiveClassLength" })
public class ShipmentAction extends AbstractPersistentObjectAction<Shipment, ShipmentSortCriterion> {

    private static final long serialVersionUID = 5708224480550988546L;
    private static final String EDIT_FORWARD = "edit";
    private static final String QUALITY_FORWARD = "quality";
    private static final String VIEW_FORWARD = "view";

    private String recipientOrganizationName;
    private String billingRecipientOrganizationName;

    private SpecimenRequestLineItem lineItem;
    private AggregateSpecimenRequestLineItem aggregateLineItem;

    private SpecimenRequestLineItem[] lineItems;
    private AggregateSpecimenRequestLineItem[] specificLineItems =
        new AggregateSpecimenRequestLineItem[0];
    private AggregateSpecimenRequestLineItem[] generalLineItems =
        new AggregateSpecimenRequestLineItem[0];
    private int cartSize = -1;
    private boolean displayPILegalField;
    private boolean displayAggregateSearchResults;
    private boolean displayUsageRestrictions;
    private boolean displayShipmentBillingRecipient;
    private boolean includeBillingRecipient;

    private SignedMaterialTransferAgreement signedSenderMta;
    private SignedMaterialTransferAgreement signedRecipientMta;
    private File mtaAmendment;
    private String mtaAmendmentFileName;
    private String mtaAmendmentContentType;
    private List<SpecimenReceiptQuality> receiptQualities;

    private final ApplicationSettingServiceLocal appSettingService;

    /**
     * Default constructor.
     */
    public ShipmentAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     *
     * @param appSettingService the application setting service
     */
    @Inject
    public ShipmentAction(ApplicationSettingServiceLocal appSettingService) {
        Shipment example = new Shipment();
        example.setStatus(ShipmentStatus.PENDING);
        HttpSession session = ServletActionContext.getRequest().getSession();
        Institution institution = TissueLocatorSessionHelper.getLoggedInUser(session).getInstitution();
        example.setSendingInstitution(institution);
        setObject(example);
        this.appSettingService = appSettingService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        initLineItems();
        initSignedMtas();
        convertFiles();
        if (getObject().getRecipient() != null && getObject().getRecipient().getOrganization() != null) {
            setRecipientOrganizationName(getObject().getRecipient().getOrganization().getName());
        }
        if (getObject().getBillingRecipient() != null && getObject().getBillingRecipient().getOrganization() != null) {
            setBillingRecipientOrganizationName(getObject().getBillingRecipient().getOrganization().getName());
        }
        setDisplayPILegalField(appSettingService.isDisplayPiLegalFields());
        setDisplayAggregateSearchResults(StringUtils.isNotBlank(appSettingService.getSpecimenGrouping()));
        setDisplayUsageRestrictions(appSettingService.isDisplayUsageRestrictions());
        setDisplayShipmentBillingRecipient(appSettingService.isDisplayShipmentBillingRecipient());
        setIncludeBillingRecipient(getObject().getBillingRecipient() != null);

        CodeServiceLocal csl = TissueLocatorRegistry.getServiceLocator().getCodeService();
        setReceiptQualities(csl.getActiveCodes(SpecimenReceiptQuality.class, CodeSortCriterion.ID));
    }

    private void initLineItems() {
        if (getObject().getLineItems() != null) {
            setLineItems(new SpecimenRequestLineItem[getObject().getLineItems().size()]);
            getObject().getLineItems().toArray(lineItems);
        }
        if (getObject().getAggregateLineItems() != null) {
            AggregateLineItemSeparator separator = new AggregateLineItemSeparator(getObject().getAggregateLineItems(),
                    appSettingService);
            setSpecificLineItems(new AggregateSpecimenRequestLineItem[separator.getSpecificLineItems().size()]);
            separator.getSpecificLineItems().toArray(specificLineItems);
            setGeneralLineItems(new AggregateSpecimenRequestLineItem[separator.getGeneralLineItems().size()]);
            separator.getGeneralLineItems().toArray(generalLineItems);
        }
    }

    private void initSignedMtas() {
        ShipmentServiceLocal service = TissueLocatorRegistry.getServiceLocator().getShipmentService();
        setSignedSenderMta(service.getSignedSenderMta(getObject()));
        setSignedRecipientMta(service.getSignedRecipientMta(getObject()));
    }

    private void convertFiles() {
        if (getMtaAmendment() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getMtaAmendment());
            getObject().setMtaAmendment(new TissueLocatorFile(fileContents,
                    getMtaAmendmentFileName(), getMtaAmendmentContentType()));
        }
    }

    /**
     * View the shipment.
     * @return the struts forward
     */
    @SkipValidation
    public String edit() {
        if (cartSize >= 0) {
            String [] args = new String[] {String.valueOf(getObject().getLineItems().size()
                    + getObject().getAggregateLineItems().size() - getCartSize())};
            addActionMessage(getText("shipment.cart.add.message", args));
        }
        return EDIT_FORWARD;
    }

    /**
     * View the shipment.
     * @return the struts forward
     */
    @SkipValidation
    public String invoice() {
        return SUCCESS;
    }

    /**
     * Empty the order.
     * @return the struts forward
     */
    @SkipValidation
    public String removeLineItem() {
        getObject().setProcessingUser(TissueLocatorSessionHelper.
                getLoggedInUser(ServletActionContext.getRequest().getSession()));
        getService().removeLineItem(getObject(), getLineItem());
        addActionMessage(getText("shipment.cart.remove.message"));
        return EDIT_FORWARD;
    }

    /**
     * Remove an aggregate line item.
     * @return the struts forward
     */
    @SkipValidation
    public String removeAggregateLineItem() {
        getObject().setProcessingUser(TissueLocatorSessionHelper.
                getLoggedInUser(ServletActionContext.getRequest().getSession()));
        getService().removeAggregateLineItem(getObject(), getAggregateLineItem());
        addActionMessage(getText("shipment.cart.aggregate.remove.message"));
        initLineItems();
        return EDIT_FORWARD;
    }

    /**
     * Shows the shipment specimen quality.
     * @return the struts forward
     */
    @SkipValidation
    public String viewShipmentQuality() {
        return QUALITY_FORWARD;
    }

    /**
     * Shows shipment specimen quality.
     * @return the struts forward
     */
    @SkipValidation
    public String updateShipmentQuality() {
        getService().updateLineItemDispostitions(getObject());
        addActionMessage(getText("shipment.updateSpecimenQuality.success"));
        return SUCCESS;
    }

    /**
     * Remove all specimen line items from the order.
     * @return the struts forward
     */
    @SkipValidation
    public String emptyOrder() {
        getObject().setProcessingUser(TissueLocatorSessionHelper.
                getLoggedInUser(ServletActionContext.getRequest().getSession()));
        String [] args = new String[] {String.valueOf(getObject().getLineItems().size())};
        addActionMessage(getText("shipment.cart.empty.message", args));
        getService().removeAllLineItems(getObject());
        return EDIT_FORWARD;
    }

    /**
     * Remove all aggregate line items from the order.
     * @return the struts forward
     */
    @SkipValidation
    public String emptyAggregateOrder() {
        String[] args = new String[] {String.valueOf(getObject().getAggregateLineItems().size())};
        getService().removeAllAggregateLineItems(getObject());
        addActionMessage(getText("shipment.cart.aggregate.empty.message", args));
        initLineItems();
        return EDIT_FORWARD;
    }


    /**
     * Updates the order status to 'SHIPPED' and emails the requestor.
     * @return the strus forward
     * @throws MessagingException on error.
     */
    @SkipValidation
    public String markAsShipped() throws MessagingException {
        if (!isIncludeBillingRecipient()) {
            getObject().setBillingRecipient(null);
        }
        getObject().setProcessingUser(TissueLocatorSessionHelper.
                getLoggedInUser(ServletActionContext.getRequest().getSession()));
        getService().markOrderAsShipped(getObject());
        addActionMessage(getText("shipment.shipped.success"));
        return VIEW_FORWARD;
    }

    /**
     * Updates the order status to 'CANCELED' and emails the requestor.
     * @return the strus forward
     * @throws MessagingException on error.
     */
    @SkipValidation
    public String cancel() throws MessagingException {
        getObject().setProcessingUser(TissueLocatorSessionHelper.
                getLoggedInUser(ServletActionContext.getRequest().getSession()));
        getService().cancelOrder(getObject());
        addActionMessage(getText("shipment.cancel.success"));
        return VIEW_FORWARD;
    }

    /**
     * save the shipment.
     * @return the struts forward.
     * @throws MessagingException on error.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "shipment"),
                        @ValidationParameter(name = "excludes", value = "billingRecipient.address.line1, "
                            + "billingRecipient.address.city, "
                            + "billingRecipient.address.state, billingRecipient.address.zip, "
                            + "billingRecipient.address.country, billingRecipient.address.phone, "
                            + "billingRecipient.firstName, billingRecipient.lastName, "
                            + "billingRecipient.email, billingRecipient.organization"),
                        @ValidationParameter(name = "conditionalExpression", value = "!includeBillingRecipient") }),
                @CustomValidator(type = "hibernate", fieldName = "object",
                     parameters = { @ValidationParameter(name = "resourceKeyBase", value = "shipment"),
                        @ValidationParameter(name = "conditionalExpression", value = "includeBillingRecipient") })
            },
            fieldExpressions = {
                    @FieldExpressionValidator(fieldName = "recipientOrganizationName",
                        key = "specimenRequest.shipment.recipient.organization.required",
                        expression = "recipientOrganizationName == object.recipient.organization.name"),
                    @FieldExpressionValidator(fieldName = "billingRecipientOrganizationName",
                        key = "specimenRequest.shipment.billingRecipient.organization.required",
                        expression = "(includeBillingRecipient == false)"
                            + " || (billingRecipientOrganizationName =="
                            + " object.billingRecipient.organization.name)")
                }
    )
    public String save() throws MessagingException {
        if (!isIncludeBillingRecipient()) {
            getObject().setBillingRecipient(null);
        }
        getService().saveOrder(getObject());
        addActionMessage(getText("shipment.success"));
        return EDIT_FORWARD;
    }

    /**
     * @return the strus forward
     */
    @SkipValidation
    public String view() {
       return VIEW_FORWARD;
    }

    /**
     * action to update the pricing.
     * @return a struts forward.
     */
    @Validations(
        customValidators = {
            @CustomValidator(type = "hibernate", fieldName = "lineItems",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "lineItems") }),
            @CustomValidator(type = "hibernate", fieldName = "specificLineItems",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "specificLineItems") }),
            @CustomValidator(type = "hibernate", fieldName = "generalLineItems",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "generalLineItems") }),
            @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "shipment") })
        }
    )
    public String updatePricing() {
        getObject().setProcessingUser(TissueLocatorSessionHelper.
                getLoggedInUser(ServletActionContext.getRequest().getSession()));
        getService().savePersistentObject(getObject());
        addActionMessage(getText("shipment.price.success"));
        return EDIT_FORWARD;
    }

    /**
     * Action to set the shipped date prior to marking as shipped.
     * @return struts forward.
     * @throws MessagingException on error.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "shipment") })
            })
    public String setShipmentDate() throws MessagingException {
        getService().saveOrder(getObject());
        return SUCCESS;
    }

    /**
     * Action to go to the request details page.
     * @return the request.
     */
    public String viewRequestDetails() {
        return "viewRequestDetails";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PaginatedList<Shipment> getPaginatedList() {
        return new SortablePaginatedList<Shipment, ShipmentSortCriterion>(1,
                ShipmentSortCriterion.ID.name(), ShipmentSortCriterion.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ShipmentServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getShipmentService();
    }

    /**
     * @return the lineItem
     */
    public SpecimenRequestLineItem getLineItem() {
        return lineItem;
    }

    /**
     * @param lineItem the lineItem to set
     */
    public void setLineItem(SpecimenRequestLineItem lineItem) {
        this.lineItem = lineItem;
    }

    /**
     * @return the aggregate lineItem
     */
    public AggregateSpecimenRequestLineItem getAggregateLineItem() {
        return aggregateLineItem;
    }

    /**
     * @param aggregateLineItem the aggregate line item to set
     */
    public void setAggregateLineItem(AggregateSpecimenRequestLineItem aggregateLineItem) {
        this.aggregateLineItem = aggregateLineItem;
    }

    /**
     * @return the lineItems
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public SpecimenRequestLineItem[] getLineItems() {
        return lineItems;
    }

    /**
     * @param lineItems the lineItems to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setLineItems(SpecimenRequestLineItem[] lineItems) {
        this.lineItems = lineItems;
    }

    /**
     * @return the specificLineItems
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public AggregateSpecimenRequestLineItem[] getSpecificLineItems() {
        return specificLineItems;
    }

    /**
     * @param specificLineItems the specificLineItems to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setSpecificLineItems(
            AggregateSpecimenRequestLineItem[] specificLineItems) {
        this.specificLineItems = specificLineItems;
    }

    /**
     * @return the specific line items as a list.
     */
    public List<AggregateSpecimenRequestLineItem> getSpecificLineItemList() {
        List<AggregateSpecimenRequestLineItem> list = new ArrayList<AggregateSpecimenRequestLineItem>();
        list.addAll(Arrays.asList(getSpecificLineItems()));
        return list;
    }

    /**
     * @return the generalLineItems
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public AggregateSpecimenRequestLineItem[] getGeneralLineItems() {
        return generalLineItems;
    }

    /**
     * @param generalLineItems the generalLineItems to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setGeneralLineItems(
            AggregateSpecimenRequestLineItem[] generalLineItems) {
        this.generalLineItems = generalLineItems;
    }

    /**
     * @return the general line items as a list.
     */
    public List<AggregateSpecimenRequestLineItem> getGeneralLineItemList() {
        List<AggregateSpecimenRequestLineItem> list = new ArrayList<AggregateSpecimenRequestLineItem>();
        list.addAll(Arrays.asList(getGeneralLineItems()));
        return list;
    }

    /**
     * @return the recipientOrganizationName
     */
    public String getRecipientOrganizationName() {
        return recipientOrganizationName;
    }

    /**
     * @param recipientOrganizationName the recipientOrganizationName to set
     */
    public void setRecipientOrganizationName(String recipientOrganizationName) {
        this.recipientOrganizationName = recipientOrganizationName;
    }

    /**
     * @return the billingRecipientOrganizationName
     */
    public String getBillingRecipientOrganizationName() {
        return billingRecipientOrganizationName;
    }

    /**
     * @param billingRecipientOrganizationName the billingRecipientOrganizationName to set
     */
    public void setBillingRecipientOrganizationName(
            String billingRecipientOrganizationName) {
        this.billingRecipientOrganizationName = billingRecipientOrganizationName;
    }

    /**
     * @return the cartSize
     */
    public int getCartSize() {
        return cartSize;
    }

    /**
     * @param cartSize the cartSize to set
     */
    public void setCartSize(int cartSize) {
        this.cartSize = cartSize;
    }

    /**
     * @return the displayPILegalField
     */
    public boolean isDisplayPILegalField() {
        return displayPILegalField;
    }

    /**
     * @param displayPILegalField the displayPILegalField to set
     */
    public void setDisplayPILegalField(boolean displayPILegalField) {
        this.displayPILegalField = displayPILegalField;
    }

    /**
     * @return Whether line items are aggregate.
     */
    public boolean isDisplayAggregateSearchResults() {
        return displayAggregateSearchResults;
    }

    /**
     * @param displayAggregateSearchResults Whether line items are aggregate.
     */
    public void setDisplayAggregateSearchResults(
            boolean displayAggregateSearchResults) {
        this.displayAggregateSearchResults = displayAggregateSearchResults;
    }

    /**
     * @return the displayUsageRestrictions
     */
    public boolean isDisplayUsageRestrictions() {
        return displayUsageRestrictions;
    }

    /**
     * @param displayUsageRestrictions the displayUsageRestrictions to set
     */
    public void setDisplayUsageRestrictions(boolean displayUsageRestrictions) {
        this.displayUsageRestrictions = displayUsageRestrictions;
    }

    /**
     * @return the displayShipmentBillingRecipient
     */
    public boolean isDisplayShipmentBillingRecipient() {
        return displayShipmentBillingRecipient;
    }

    /**
     * @param displayShipmentBillingRecipient the displayShipmentBillingRecipient to set
     */
    public void setDisplayShipmentBillingRecipient(
            boolean displayShipmentBillingRecipient) {
        this.displayShipmentBillingRecipient = displayShipmentBillingRecipient;
    }

    /**
     * @return the signedSenderMta
     */
    public SignedMaterialTransferAgreement getSignedSenderMta() {
        return signedSenderMta;
    }

    /**
     * @param signedSenderMta the signedSenderMta to set
     */
    public void setSignedSenderMta(SignedMaterialTransferAgreement signedSenderMta) {
        this.signedSenderMta = signedSenderMta;
    }

    /**
     * @return the signedRecipientMta
     */
    public SignedMaterialTransferAgreement getSignedRecipientMta() {
        return signedRecipientMta;
    }

    /**
     * @param signedRecipientMta the signedRecipientMta to set
     */
    public void setSignedRecipientMta(
            SignedMaterialTransferAgreement signedRecipientMta) {
        this.signedRecipientMta = signedRecipientMta;
    }

    /**
     * @return the mtaAmendment
     */
    public File getMtaAmendment() {
        return mtaAmendment;
    }

    /**
     * @param mtaAmendment the mtaAmendment to set
     */
    public void setMtaAmendment(File mtaAmendment) {
        this.mtaAmendment = mtaAmendment;
    }

    /**
     * @return the mtaAmendmentFileName
     */
    public String getMtaAmendmentFileName() {
        return mtaAmendmentFileName;
    }

    /**
     * @param mtaAmendmentFileName the mtaAmendmentFileName to set
     */
    public void setMtaAmendmentFileName(String mtaAmendmentFileName) {
        this.mtaAmendmentFileName = mtaAmendmentFileName;
    }

    /**
     * @return the mtaAmendmentContentType
     */
    public String getMtaAmendmentContentType() {
        return mtaAmendmentContentType;
    }

    /**
     * @param mtaAmendmentContentType the mtaAmendmentContentType to set
     */
    public void setMtaAmendmentContentType(String mtaAmendmentContentType) {
        this.mtaAmendmentContentType = mtaAmendmentContentType;
    }

    /**
     * @return the includeBillingRecipient
     */
    public boolean isIncludeBillingRecipient() {
        return includeBillingRecipient;
    }

    /**
     * @param includeBillingRecipient the includeBillingRecipient to set
     */
    public void setIncludeBillingRecipient(boolean includeBillingRecipient) {
        this.includeBillingRecipient = includeBillingRecipient;
    }

    /**
     * Download the mta amendment.
     * @return struts forward.
     */
    public String downloadMtaAmendment() {
        return "downloadMtaAmendment";
    }

    /**
     * Get an input stream for the mta amendment.
     * @return struts forward.
     */
    public InputStream getMtaAmendmentStream() {
        return new ByteArrayInputStream(getObject().getMtaAmendment()
               .getLob().getData());
    }

    /**
     * View review details for the shipment.
     * @return the struts forward
     */
    @SkipValidation
    public String reviewDetails() {
        return SUCCESS;
    }

    /**
     * @return the consortium review vote relevant to this shipment
     */
    public SpecimenRequestReviewVote getConsortiumReviewVote() {
        return getVote(getObject().getRequest().getConsortiumReviews());
    }

    /**
     * @return the institutional review vote relevant to this shipment
     */
    public SpecimenRequestReviewVote getInstitutionalReviewVote() {
        return getVote(getObject().getRequest().getInstitutionalReviews());
    }

    private SpecimenRequestReviewVote getVote(Set<SpecimenRequestReviewVote> votes) {
        for (SpecimenRequestReviewVote vote : votes) {
            if (ObjectUtils.equals(getObject().getSendingInstitution(), vote.getInstitution())) {
                return vote;
            }
        }
        return null;
    }

    /**
     * @return the receiptQualities
     */
    public List<SpecimenReceiptQuality> getReceiptQualities() {
        return receiptQualities;
    }

    /**
     * @param receiptQualities the receiptQualities to set
     */
    public void setReceiptQualities(List<SpecimenReceiptQuality> receiptQualities) {
        this.receiptQualities = receiptQualities;
    }

}

