/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.support;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.support.Question;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.QuestionSortCriterion;
import com.fiveamsolutions.tissuelocator.service.support.QuestionServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorFileHelper;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractPersistentObjectAction;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
@Namespace("/protected/support/question")
@Results(value = {
        @Result(name = "success", location = "/WEB-INF/jsp/support/question/list.jsp"),
        @Result(name = "input", location = "/WEB-INF/jsp/support/question/edit.jsp"),
        @Result(name = "view", location = "/WEB-INF/jsp/support/question/view.jsp")
})
public class QuestionAction extends AbstractPersistentObjectAction<Question, QuestionSortCriterion> {

    private static final long serialVersionUID = -4513214699302347246L;

    private File resume;
    private String resumeContentType;
    private String resumeFileName;

    private File protocolDocument;
    private String protocolDocumentContentType;
    private String protocolDocumentFileName;

    private List<Institution> institutions;
    private List<Institution> selectedInstitutions;
    private String investigatorOrganizationName;
    private boolean copyinvestigator;

    private final QuestionServiceLocal questionService;
    private final LobHolderServiceLocal lobHolderService;

    /**
     * Default constructor.
     */
    public QuestionAction() {
        throw new UnsupportedOperationException();
    }

    /**
     * @param questionService support letter request service
     * @param lobHolderService the lob holder service
     */
    @Inject
    public QuestionAction(QuestionServiceLocal questionService, LobHolderServiceLocal lobHolderService) {
        super();
        this.questionService = questionService;
        this.lobHolderService = lobHolderService;
        setObject(new Question());
        HttpSession session = ServletActionContext.getRequest().getSession();
        TissueLocatorUser user = TissueLocatorSessionHelper.getLoggedInUser(session);
        getObject().setInvestigator(new Person(user));
        if (user.getResume() != null) {
            getObject().setResume(new TissueLocatorFile(user.getResume()));
        }
        copyinvestigator = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        convertFiles();

        InstitutionServiceLocal service = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        setInstitutions(service.getConsortiumMembers());

        if (getObject().getInvestigator().getOrganization() != null) {
            setInvestigatorOrganizationName(getObject().getInvestigator().getOrganization().getName());
        }
    }

    private void convertFiles() {
        TissueLocatorFile resumeFile = TissueLocatorFileHelper.getFile(getResume(), getResumeFileName(),
                getResumeContentType(), getObject().getResume(), lobHolderService);
        if (resumeFile != null) {
            getObject().setResume(resumeFile);
        }

        if (getProtocolDocument() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getProtocolDocument());
            getObject().setProtocolDocument(new TissueLocatorFile(fileContents,
                    getProtocolDocumentFileName(), getProtocolDocumentContentType()));
        }
    }

    /**
     * action to save a request for a letter of support.
     * @return the forward to go to.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "question")
                })
            },
            fieldExpressions = {
                @FieldExpressionValidator(fieldName = "selectedInstitutions",
                        key = "question.selectedInstitutions.required",
                        expression = "selectedInstitutions != null && !selectedInstitutions.isEmpty()"),
                @FieldExpressionValidator(fieldName = "investigatorOrganizationName",
                        key = "question.investigator.organization.required",
                        expression = "investigatorOrganizationName == object.investigator.organization.name")

            })
    @Action(value = "save")
    public String save() {
        getService().saveQuestion(getObject(), getSelectedInstitutions());
        addActionMessage(getText("question.save.success"));
        return list();
    }

    /**
     * Action for viewing a question.
     * @return a struts forward.
     */
    @SkipValidation
    @Action(value = "view")
    public String view() {
        return "view";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected PaginatedList<Question> getPaginatedList() {
        return new SortablePaginatedList<Question, QuestionSortCriterion>(1,
                QuestionSortCriterion.ID.name(), QuestionSortCriterion.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected QuestionServiceLocal getService() {
        return questionService;
    }

    /**
     * @return the resume
     */
    public File getResume() {
        return resume;
    }

    /**
     * @param resume the resume to set
     */
    public void setResume(File resume) {
        this.resume = resume;
    }

    /**
     * @return the resumeContentType
     */
    public String getResumeContentType() {
        return resumeContentType;
    }

    /**
     * @param resumeContentType the resumeContentType to set
     */
    public void setResumeContentType(String resumeContentType) {
        this.resumeContentType = resumeContentType;
    }

    /**
     * @return the resumeFileName
     */
    public String getResumeFileName() {
        return resumeFileName;
    }

    /**
     * @param resumeFileName the resumeFileName to set
     */
    public void setResumeFileName(String resumeFileName) {
        this.resumeFileName = resumeFileName;
    }

    /**
     * @return the protocolDocument
     */
    public File getProtocolDocument() {
        return protocolDocument;
    }

    /**
     * @param protocolDocument the protocolDocument to set
     */
    public void setProtocolDocument(File protocolDocument) {
        this.protocolDocument = protocolDocument;
    }

    /**
     * @return the protocolDocumentContentType
     */
    public String getProtocolDocumentContentType() {
        return protocolDocumentContentType;
    }

    /**
     * @param protocolDocumentContentType the protocolDocumentContentType to set
     */
    public void setProtocolDocumentContentType(String protocolDocumentContentType) {
        this.protocolDocumentContentType = protocolDocumentContentType;
    }

    /**
     * @return the protocolDocumentFileName
     */
    public String getProtocolDocumentFileName() {
        return protocolDocumentFileName;
    }

    /**
     * @param protocolDocumentFileName the protocolDocumentFileName to set
     */
    public void setProtocolDocumentFileName(String protocolDocumentFileName) {
        this.protocolDocumentFileName = protocolDocumentFileName;
    }

    /**
     * @return the institutions
     */
    public List<Institution> getInstitutions() {
        return institutions;
    }

    /**
     * @param institutions the institutions to set
     */
    public void setInstitutions(List<Institution> institutions) {
        this.institutions = institutions;
    }

    /**
     * @return the selectedInstitutions
     */
    public List<Institution> getSelectedInstitutions() {
        return selectedInstitutions;
    }

    /**
     * @param selectedInstitutions the selectedInstitutions to set
     */
    public void setSelectedInstitutions(List<Institution> selectedInstitutions) {
        this.selectedInstitutions = selectedInstitutions;
    }

    /**
     * @return the ids of the institutions selected for this question.
     */
    public List<Long> getSelectedInstitutionIds() {
        List<Long> instIds = new ArrayList<Long>();
        for (Institution i : getSelectedInstitutions()) {
            instIds.add(i.getId());
        }
        return instIds;
    }

    /**
     * @return the investigatorOrganizationName
     */
    public String getInvestigatorOrganizationName() {
        return investigatorOrganizationName;
    }

    /**
     * @param investigatorOrganizationName the investigatorOrganizationName to set
     */
    public void setInvestigatorOrganizationName(String investigatorOrganizationName) {
        this.investigatorOrganizationName = investigatorOrganizationName;
    }

    /**
     * @return the copyinvestigator
     */
    public boolean isCopyinvestigator() {
        return copyinvestigator;
    }

    /**
     * @param copyinvestigator the copyinvestigator to set
     */
    public void setCopyinvestigator(boolean copyinvestigator) {
        this.copyinvestigator = copyinvestigator;
    }
}
