/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.StringValueTransformer;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.displaytag.properties.SortOrderEnum;
import org.displaytag.tags.TableTagParameters;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.data.search.SortCriterion;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.service.GenericSearchService;
import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.config.help.HelpConfig;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.InstitutionSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorPropertyUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorRequestHelper;
import com.fiveamsolutions.tissuelocator.web.util.SelectOption;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * Abstract action class for management of persistent objects.
 * @author ddasgupta
 * @param <T> the type of the object to manage
 * @param <E> the type of the sort criterion
 */
@SuppressWarnings({ "PMD.ExcessiveClassLength", "PMD.TooManyMethods" })
public abstract class AbstractPersistentObjectAction<T extends PersistentObject, E extends Enum<E> & SortCriterion<T>>
    extends ActionSupport implements Preparable {

    /**
     * Max number of institutions returned.
     */
    public static final int MAX_INSTITUTION_COUNT = 100;

    private static final String UNCHECKED = "unchecked";

    /**
     * Id used for for empty select options.
     */
    protected static final String EMPTY_OPTION_ID = "-1";
    private static final long serialVersionUID = 1052102727156968834L;

    /**
     * Default page size.
     */
    protected static final String DEFAULT_PAGE_SIZE = "defaultPageSize";
    private static final int EXPORT_PAGE_SIZE = 2000;
    
    /**
     * location of the example.
     */
    protected static final String EXAMPLE_SUFFIX = "_example";
    private static final String PAGING_SUFFIX = "_paging";

    private T object;
    private PaginatedList<T> objects = new PaginatedList<T>();

    private String sort;
    private String dir = SortOrderEnum.ASCENDING.getName();
    private int page = 1;
    private int pageSize;

    private final Class<E> sortCriterionTypeArgument;
    private final Class<T> objectType;

    private String institutionSelection;
    private String term;

    private HelpConfig helpConfig;
    private String helpFieldName;
    private String helpFieldDisplayName;

    /**
     * Default constructor.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public AbstractPersistentObjectAction() {
        Type t = getClass().getGenericSuperclass();
        Class current = getClass();
        while (!(t instanceof ParameterizedType)) {
            current = current.getSuperclass();
            t = current.getGenericSuperclass();
        }
        ParameterizedType parameterizedType = (ParameterizedType) t;
        objectType = (Class) parameterizedType.getActualTypeArguments()[0];
        sortCriterionTypeArgument = (Class) parameterizedType.getActualTypeArguments()[1];
    }

    /**
     * @return a struts forward
     */
    @SkipValidation
    @SuppressWarnings(UNCHECKED)
    @Action("list")
    public String list() {
        String attributeName = getClass().getSimpleName() + EXAMPLE_SUFFIX;
        HttpSession session = ServletActionContext.getRequest().getSession();
        T example = (T) session.getAttribute(attributeName);
        if (example != null) {
            setObject(example);
        }
        return filter();
    }

    /**
     * @return a struts forward
     */
    @Action("filter")
    public String filter() {

        SearchCriteria<T> criteria = getSearchCriteria();
        GenericSearchService<T, SearchCriteria<T>> service = getSearchService();
        PaginatedList<T> l = getPaginatedList();

        PageSortParams<T> pageSortParams = setupPageSortParams(criteria, service, l);

        l.setList(service.search(criteria, pageSortParams));
        l.setFullListSize(service.count(criteria));
        setObjects(l);
        return SUCCESS;
    }

    /**
     * @param criteria the search criteria
     * @param service the generic search service
     * @param l the paginated list
     * @return the page sort params
     */
    @SkipValidation
    @SuppressWarnings(UNCHECKED)
    protected PageSortParams<T> setupPageSortParams(SearchCriteria<T> criteria,
            GenericSearchService<T, SearchCriteria<T>> service, PaginatedList<?> l) {
        String attributeName = getClass().getSimpleName() + EXAMPLE_SUFFIX;
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.setAttribute(attributeName, getObject());
        String pagingAttributeName = getClass().getSimpleName() + PAGING_SUFFIX;
        PageSortParams<T> pagingParams = (PageSortParams<T>) session.getAttribute(pagingAttributeName);
        //if no paging params stored in session, let them be computed from defaults and request params
        if (pagingParams != null) {
            processParams(pagingParams);
        } else if (!ServletActionContext.getRequest().getParameterMap().containsKey("pageSize")) {
            String objectsPerPage = ServletActionContext.getServletContext().getInitParameter(DEFAULT_PAGE_SIZE);
            setPageSize(Integer.parseInt(objectsPerPage));
        }
        int count = service.count(criteria);
        correctPageNumberIfNeeded(pagingParams, count);

        setUpPaginatedList(l);

        //set up the PageSortParams
        int index = l.getObjectsPerPage() * (l.getPageNumber() - 1);
        boolean desc = SortOrderEnum.DESCENDING.equals(l.getSortDirection());
        List<E> typedSortCriteria = new ArrayList<E>();
        String[] sortCriteria = StringUtils.split(l.getSortCriterion(), ",");
        if (sortCriteria != null) {
            for (String crit : sortCriteria) {
                typedSortCriteria.add(Enum.valueOf(sortCriterionTypeArgument, crit));
            }
        }

        PageSortParams<T> pageSortParams;
        if (ServletActionContext.getRequest().getParameter(TableTagParameters.PARAMETER_EXPORTING) == null) {
            pageSortParams = new PageSortParams<T>(getPageSize(), index, typedSortCriteria, desc);
            session.setAttribute(pagingAttributeName, pageSortParams);
        } else {
            l.setObjectsPerPage(EXPORT_PAGE_SIZE);
            pageSortParams = new PageSortParams<T>(l.getObjectsPerPage(), 0, typedSortCriteria, desc);
            setPaginatedSearchHelper(pageSortParams, criteria, count);
        }
        return pageSortParams;
    }

    /**
     * Set a paginated search helper in the request.
     * @param pageSortParams Page sort params.
     * @param criteria Search criteria.
     * @param count full results count.
     */
    protected void setPaginatedSearchHelper(PageSortParams<T> pageSortParams, SearchCriteria<T> criteria, int count) {
        TissueLocatorRequestHelper.setPaginatedSearchHelper(ServletActionContext.getRequest(),
                new PaginatedSearchHelper(pageSortParams, criteria, count));
    }

    private void setUpPaginatedList(PaginatedList<?> l) {
        //set up the PaginatedList
        l.setObjectsPerPage(getPageSize());
        l.setPageNumber(getPage());
        if (StringUtils.isNotBlank(getSort())) {
            l.setSortCriterion(getSort());
        }
        l.setSortDirection(SortOrderEnum.fromName(getDir()));
    }

    private void correctPageNumberIfNeeded(PageSortParams<T> pagingParams, int count) {
        if (pagingParams != null && getPageSize() != pagingParams.getPageSize()) {
            setPage(1);
        } else if (getPage() > 1 && count <= getPageSize() * (getPage() - 1)) {
            setPage(1);
        }
    }

    @SuppressWarnings({"rawtypes", UNCHECKED })
    private void processParams(PageSortParams<?> pagingParams) {
        Map parameters = ServletActionContext.getRequest().getParameterMap();
        //if any paging params are included in the request params, use those instead of what's in the session
        //otherwise, reset the paging params with what's stored in the session
        if (!parameters.containsKey("pageSize")) {
            setPageSize(pagingParams.getPageSize());
        }

        if (!parameters.containsKey("page")) {
            setPage(pagingParams.getIndex() / pagingParams.getPageSize() + 1);
        }

        if (!parameters.containsKey("dir")) {
            SortOrderEnum direction = SortOrderEnum.ASCENDING;
            if (pagingParams.isDesc()) {
                direction = SortOrderEnum.DESCENDING;
            }
            setDir(direction.getName());
        }

        if (!parameters.containsKey("sort")) {
            Collection<String> sortStrings = CollectionUtils.transformedCollection(pagingParams.getSortCriteria(),
                    StringValueTransformer.getInstance());
            setSort(StringUtils.join(sortStrings, ","));
        }
    }

    /**
     * Returns search results incrementally based on PageSortParams
     * and SearchCriteria settings. Assumes results have already been
     * retrieved for the current index.
     * @author gvaughn
     *
     */
    public class PaginatedSearchHelper {

        private final PageSortParams<T> pageSortParams;
        private final SearchCriteria<T> searchCriteria;
        private final int fullResultsCount;

        /**
         * Helper for retrieving search results incrementally.
         * @param pageSortParams paging parameters used in the search.
         * @param searchCriteria search criteria.
         * @param fullResultsCount total number of results.
         */
        public PaginatedSearchHelper(PageSortParams<T> pageSortParams, SearchCriteria<T> searchCriteria,
                int fullResultsCount) {
            this.pageSortParams = pageSortParams;
            this.searchCriteria = searchCriteria;
            this.fullResultsCount = fullResultsCount;
        }

        /**
         * Returns true more search results can be retrieved by incrementing
         * the current index by the page size, false otherwise.
         * @return if another page of results is available.
         */
        public boolean hasMoreResults() {
            return pageSortParams.getIndex() + pageSortParams.getPageSize() < fullResultsCount;
        }

        /**
         * Returns the search results retrieved by incrementing the current
         * index by the page size.
         * @return the next set of results.
         */
        public List<T> getNextResults() {
            pageSortParams.setIndex(pageSortParams.getIndex() + pageSortParams.getPageSize());
            return getSearchService().search(searchCriteria, pageSortParams);
        }
    }

    /**
     * @return the search criteria
     */
    protected SearchCriteria<T> getSearchCriteria() {
        return new TissueLocatorAnnotatedBeanSearchCriteria<T>(getObject());
    }

    /**
     * Entry action.
     * @return a struts forward
     */
    @Override
    @SkipValidation
    @Action("input")
    public String input() {
        return getInputResult();
    }

    /**
     * allows this to be overridden.
     * @return input result
     */
    protected String getInputResult() {
        return INPUT;
    }

    /**
     * {@inheritDoc}
     */
    public void prepare() {
        GenericServiceLocal<T> service = getService();
        if (getObject().getId() != null) {
            setObject(service.getPersistentObject(objectType, getObject().getId()));
        }
        helpConfig = new HelpConfig(getObjectType().getName());
    }

    /**
     * @return the service used to perform the search
     */
    protected abstract GenericServiceLocal<T> getService();

    /**
     * @return the service used to perform the search
     */
    protected GenericSearchService<T, SearchCriteria<T>> getSearchService() {
        // providing a default useless implementation so I don't have to muck with all subclasses
        // of this.
        return getService();
    }


    /**
     * @return get the paginated list to be displayed
     */
    protected abstract PaginatedList<T> getPaginatedList();

    /**
     * @return the object
     */
    public T getObject() {
        return object;
    }

    /**
     * @param object the object to set
     */
    public void setObject(T object) {
        this.object = object;
    }

    /**
     * @return the objects
     */
    public PaginatedList<?> getObjects() {
        return objects;
    }

    /**
     * @param objects the objects to set
     */
    public void setObjects(PaginatedList<T> objects) {
        this.objects = objects;
    }

    /**
     * @return the sort
     */
    public String getSort() {
        return this.sort;
    }

    /**
     * @param sort the sort to set
     */
    public void setSort(String sort) {
        this.sort = sort;
    }

    /**
     * @return the dir
     */
    public String getDir() {
        return this.dir;
    }

    /**
     * @param dir the dir to set
     */
    public void setDir(String dir) {
        this.dir = dir;
    }

    /**
     * @return the page
     */
    public int getPage() {
        return this.page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * @return the pageSize
     */
    public int getPageSize() {
        return this.pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Action for the auto completer.
     * @return SUCCESS
     */
    @SkipValidation
    @Action("autocompleteInstitution")
    public String autocompleteInstitution() {
        return SUCCESS;
    }

    /**
     * @return the institutions for the autocompleter.
     */
    public SelectOption[] getInstitutionsMap() {
        return searchInstitutions(null);
    }

    /**
     * @param consortiumMember whether consortium members should be included
     * @return the autocomplete results.
     */
    protected SelectOption[] searchInstitutions(Boolean consortiumMember) {
        InstitutionServiceLocal service = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        Institution example = new Institution();
        example.setName(getTerm());
        example.setConsortiumMember(consortiumMember);
        TissueLocatorAnnotatedBeanSearchCriteria<Institution> sc =
            new TissueLocatorAnnotatedBeanSearchCriteria<Institution>(example);
        PageSortParams<Institution> psp = new PageSortParams<Institution>(MAX_INSTITUTION_COUNT,
                0, InstitutionSortCriterion.NAME, false);
        List<Institution> institutions = service.search(sc, psp);

        boolean additionalResults = institutions.size() == MAX_INSTITUTION_COUNT;
        int resultsLength = institutions.size() + (additionalResults ? 1 : 0);
        SelectOption[] results = new SelectOption[resultsLength];
        for (int index = 0; index < institutions.size(); index++) {
            Institution current = institutions.get(index);
            results[additionalResults ? index + 1 : index] = 
                new SelectOption(current.getId().toString(), current.getName(), current.getName());
        }
        if (additionalResults) {
            addAdditionalOptionsMessage(results);
        }
        return results;
    }

    /**
     * Add an select option to indicate more results are available than were returned.
     * @param results Select options to which the indication will be added.
     */
    protected void addAdditionalOptionsMessage(SelectOption[] results) {
        String additionalOptionsText = getText("autocompleter.additionalOptions.label");
        results[0] = new SelectOption(EMPTY_OPTION_ID, 
                additionalOptionsText, additionalOptionsText);
    }
    /**
     * @return the institutionSelection
     */
    public String getInstitutionSelection() {
        return institutionSelection;
    }

    /**
     * @param institutionSelection the institutionSelection to set
     */
    public void setInstitutionSelection(String institutionSelection) {
        this.institutionSelection = institutionSelection;
    }

    /**
     * @return the term
     */
    public String getTerm() {
        return term;
    }

    /**
     * @param term the term to set
     */
    public void setTerm(String term) {
        this.term = term;
    }

    /**
     * @return the objectType
     */
    protected Class<T> getObjectType() {
        return this.objectType;
    }

    /**
     * @return the helpConfig
     */
    public HelpConfig getHelpConfig() {
        return helpConfig;
    }

    /**
     * @return the helpFieldName
     */
    public String getHelpFieldName() {
        return helpFieldName;
    }

    /**
     * @param helpFieldName the helpFieldName to set
     */
    public void setHelpFieldName(String helpFieldName) {
        this.helpFieldName = helpFieldName;
    }

    /**
     * @return the helpFieldDisplayName
     */
    public String getHelpFieldDisplayName() {
        return helpFieldDisplayName;
    }

    /**
     * @param helpFieldDisplayName the helpFieldDisplayName to set
     */
    public void setHelpFieldDisplayName(String helpFieldDisplayName) {
        this.helpFieldDisplayName = helpFieldDisplayName;
    }

    /**
     * Return a property value from this action's object. 
     * @param property Property name.
     * @return The object property value.
     */
    public Object getObjectProperty(String property) {
        return getObjectProperty(getObject(), property);
    }
    
    /**
     * Return a property value from an object. 
     * @param obj Object from which the property will be retrieved.
     * @param propertyName Property name.
     * @return The object property value.
     */
    public Object getObjectProperty(Object obj, String propertyName) {
        Object value = null;
        try {
            value = TissueLocatorPropertyUtil.getObjectProperty(obj, propertyName);
        } catch (Exception e) {
            LOG.warn(e.getMessage());
        }
        return value;
    }
}
